--18-NOV-2019
ALTER TABLE `company`
	ADD COLUMN `company_uname` VARCHAR(500) NULL DEFAULT NULL COLLATE 'utf8_general_ci' AFTER `Companycol1`;
--19-Nov-2019
CREATE TABLE `finanicalyear` (
	`fn_id` INT(11) NOT NULL AUTO_INCREMENT,
	`fname` VARCHAR(100) NULL DEFAULT NULL,
	`start_date` DATETIME NULL DEFAULT NULL,
	`end_date` DATETIME NULL DEFAULT NULL,
	`remarks` VARCHAR(255) NULL DEFAULT NULL,
	`company_id` INT(11) NULL DEFAULT '1',
	PRIMARY KEY (`fn_id`)
);
ALTER TABLE `rolegroup`
	ADD COLUMN `isadmin` TINYINT NULL DEFAULT '0' AFTER `desc`;
--22-Nov
ALTER TABLE `stockmain`
	ADD UNIQUE INDEX `vrnoa_etype_company_id` (`vrnoa`, `etype`, `company_id`);	
	ALTER TABLE `ordermain`
	ADD UNIQUE INDEX `vrnoa_etype_company_id` (`vrnoa`, `etype`, `company_id`);


