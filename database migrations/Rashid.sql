CREATE TABLE `imagestock` (
	`img_id` INT(11) NOT NULL AUTO_INCREMENT,
	`vrnoa` INT(11) NULL DEFAULT NULL,
	`etype` VARCHAR(50) NULL DEFAULT NULL,
	`photo` TEXT NULL,
	`remarks` TEXT NULL,
	`datetime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`img_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

 "uploadFiles":{ 
         "uploadFiles":1,
         "insert":1,
         "update":1,
         "delete":1
      },