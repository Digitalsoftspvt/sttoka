$(document).ready(function(){
	fetchfiltertablename();
	FetchReportGroups();
	fetchAllForComponents();
});
function fetchAllForComponents(name)
{
	var rname = $('.report_title').text().split('Report')[0].trim().toLowerCase();
	if (rname === '')
		{

		}else{
			$.ajax({
				url: base_url + "index.php/filter/fetchAllForComponents",
				data : {'rname' : rname},
				type: 'POST',
				dataType: 'JSON',
				async: false,
				success : function (data)
				{
				var sdate = new Date();
				var edate = new Date();
				var from = "";
				var to = "";
				var Month = "";
				var days = "";
				var advancedgenenral = '';
				var advanceditem = '';
				var advancedaccount = '';
				var Today = '';
				var till = '';
				var general = "General";
				from += "<div class='input-group'>";
				from += "<span class='input-group-addon'>From</span>";
				from += "<input type='text' class='form-control ts_datepicker' id='from22' >" +"</div>";
				till += "<div class='input-group'>";
				till += "<span class='input-group-addon'>Till Date</span>";
				to += "<div class='input-group'>";
				to += "<span class='input-group-addon'>To</span>";
				to += "<input type='text'  class='form-control ts_datepicker' id='to22' >" +"</div>";

				days += "<div class='input-group'>";
				days += "<span class='input-group-addon'>Days</span>";
				days += "<input type='text'  class='form-control' id='from22' >" +"</div>";

				advancedgenenral += '<div class="panel panel-default">';
				advancedgenenral += '<div class="panel-heading" role="tab" id="headingOne">';
				advancedgenenral += '<h4 class="panel-title">';
				advancedgenenral += '<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne"> General';
				advancedgenenral += '</a>';
				advancedgenenral += '</h4>';
				advancedgenenral += '</div>';
				advancedgenenral += '<div id="collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">';
				advancedgenenral += '<div class="panel-body">';
				advancedgenenral += '<form class="form-group">';
				advancedgenenral += '<div class="row">';
				advancedgenenral += '<div class="col-lg-12">';
				advancedgenenral += '<div class="general">';
				advancedgenenral += '</div>';
				advancedgenenral += '</div>';
				advancedgenenral += '</div>';
				advancedgenenral += '</form>';
				advancedgenenral += '</div>';
				advancedgenenral += '</div>';
				advancedgenenral += '</div>';

				advanceditem  += '<div class="panel panel-default">';
				advanceditem  +='<div class="panel-heading" role="tab" id="headingOne">';
				advanceditem  += '<h4 class="panel-title">';
				advanceditem  += '<a data-toggle="collapse" data-parent="#accordion" href="#collapsetwo" aria-expanded="false" aria-controls="collapsetwo"> Item'
				advanceditem  += '</a>';
				advanceditem  += '</h4>';
				advanceditem  += '</div>';
				advanceditem  += '<div id="collapsetwo" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">';
				advanceditem  += '<div class="panel-body">';
				advanceditem  += '<form class="form-group">';
				advanceditem  += '<div class="row">';
				advanceditem  += '<div class="col-lg-12">';
				advanceditem  += '<div class="item">';
				advanceditem  += '</div></div></div></form></div></div></div>';

				advancedaccount += '<div class="panel panel-default">';
				advancedaccount +='<div class="panel-heading" role="tab" id="headingOne">';
				advancedaccount += '<h4 class="panel-title">';
				advancedaccount += '<a data-toggle="collapse" data-parent="#accordion" href="#collapsethree" aria-expanded="false" aria-controls="collapsethree"> Account';
				advancedaccount += '</a>';
				advancedaccount += '</h4>';
				advancedaccount += '</div>';
				advancedaccount += '<div id="collapsethree" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">';
				advancedaccount += '<div class="panel-body">';
				advancedaccount += '<form class="form-group">'
				advancedaccount += '<div class="row">';
				advancedaccount += '<div class="col-lg-12">';
				advancedaccount += '<div class="account">';
				advancedaccount += '</div>'
				advancedaccount += '</div></div></form></div></div></div>';
				Today += '<div class="row">'+
							'<div class="col-lg-12">'+
							'<div class="col-lg-6">'+
							'<div class="form-group">'+
							'<label class="radio-inline">'+
							'<input type="radio" name="durType" value="today" checked="checked">'+
							'Today'+
							'</label>'+
							'<label class="radio-inline">'+
							'<input type="radio" name="durType" value="week">'+
							'This Week'+
							'</label>'+
							'<label class="radio-inline">'+
							'<input type="radio" name="durType" value="month">'+
							'This Month'+
							'</label>'+
							'<label class="radio-inline">'+
							'<input type="radio" name="durType" value="year">'+
							'This Year'+
							'</label>'+
							'</div>'+
							'</div>'+
							'</div>'+
						'</div>';
						if (data ==='false') {
						console.log('components Does Not Have This Report Title');
					}else{
						$.each(data,function(ind,elem){
							if (elem.from_date === '' && elem.to_date) {

							}
							else{
								if (elem.from_date === 'from' && elem.to_date === 'to') {
									$(from).appendTo('.from');
									$(to).appendTo('.to');
									$(Today).appendTo('.Today');
									$(".ts_datepicker").datepicker({ 
										format: 'yyyy/mm/dd'
									});
									$('#from22').datepicker('update',sdate);
									$('#to22').datepicker('update',edate);

								}else if (elem.from_date === 'till'){
									till += "<input type='text' class='form-control ts_datepicker' id='from22' "+ elem.attr +"='ture' >" +"</div>";
									$(till).appendTo('.from');
									$(".ts_datepicker").datepicker({ 
										format: 'yyyy/mm/dd'
									});
									$('#from22').datepicker('update', new Date());
								}
								else if (elem.from_date === 'days')
								{
									$(days).appendTo('.from');
									
								}else if (elem.from_date === 'Month'){
									Month += "<div class='input-group'>";
									Month += "<span class='input-group-addon'>Month</span>";
									Month += "<input type='text' class='form-control month_year_picker' id='from22' >" +"</div>";
									$('.month_year_picker').datepicker({
										format: "mm-yyyy",
										viewMode: "month",
										minViewMode: "month"
									});
									$('#from22').datepicker('update',sdate);
									$(Month).appendTo('.from');
								}else if (elem.from_date === 'Years'){
									Years += "<div class='input-group'>";
									Years += "<span class='input-group-addon'>Years</span>";
									Years += "<input type='text' class='form-control month_year_picker' id='from22' >" +"</div>";
									$('.month_year_picker').datepicker({
									format: "yyyy-mm",
									viewMode: "years",
									minViewMode: "years"
								});
									$('.month_year_picker').datepicker('update', new Date());
									$(Years).appendTo('.from');
								}

								if (elem.advanced !==null && elem.general !==null && elem.item !==null && elem.account !==null) {
									$(advancedgenenral).appendTo('.advancedgenenral');
									$(advanceditem).appendTo('.advanceditem');
									$(advancedaccount).appendTo('.advancedaccount');
								}
								else if (elem.advanced !==null){
									if (elem.general !==null){
										console.log('general');
										$(advancedgenenral).appendTo('.advancedgenenral');
									}else{
										console.log(elem.general);
									}
									if (elem.item !==null){
										$(advanceditem).appendTo('.advanceditem');
									}else{
										console.log(elem.item);
									}
									if (elem.account !==null){
										$(advancedaccount).appendTo('.advancedaccount');
									}else{
										console.log(elem.account);
									}
								}
								else{
									$('.btnAdvaced').hide();
								}
							}
							});
					}
				

				
	
				},

				error: function (xhr, statusText, err) {
					alert("Error:"+xhr.status);
				}
			});
		}
}


function fetchfiltertablename(name)
{
	var rname = $('.report_title').text().split('Report')[0].trim().toLowerCase();
	if (rname === '')
		{

		}else{
			$.ajax({
				url: base_url + "index.php/filter/fetchtablename",
				data : {'rname' : rname},
				type: 'POST',
				dataType: 'JSON',
				success : function (data)
				{
					console.log(data);
					var FilterType = "";
					var select2 = '';
					var Class='';
					$.each(data,function(ind,elem)
						{
						select2='';
						FilterType = elem.filtertype;
						Class = elem.classtype;
						select2 += '<div class="col-lg-2">';
						select2 += '<label>'+ elem.placeholder + '</label>';
						select2 += '<select  class="'+Class+'" multiple="true"   data-id='+ elem.classid +' data-placeholder="'+elem.placeholder+'">';

						$.each(elem.filterdata,function(ind2,elem2)
							{
								select2 += '<option data-displayvalue="'+components.titleCase(elem2.displayvalue)+'" data-crit_id="'+elem2.id +'"  value="'+elem2.id+'">' +components.titleCase(elem2.displayvalue) + '</option>';
							});
						select2 += '</select>';
						select2 += '</div>';
				if (FilterType === 'general')
					{
					$(select2).appendTo('.general');
				    }
				else if (FilterType === 'item')
					{
						$(select2).appendTo('.item');
					}
				else if (FilterType === 'account')
					{
						$(select2).appendTo('.account');
					}
					$('select.select2').select2();
				});
				},

				error: function (xhr, statusText, err) {
					alert("Error:"+xhr.status);
				}
			});
		}
}

function FetchReportGroups()
{
	var rname = $('.report_title').text().split('Report')[0].trim().toLowerCase();
	if (rname === '')
	{

	}
	else{
		$.ajax({
			url: base_url + "index.php/filter/getreportdatafromdatabase",
			data: { 'name' : rname},
			type: 'POST',
			dataType: 'JSON',
			success: function (result) {
				if (result === false || result===undefined) {
					console.log(result);
				}else{
					var a = "";
				var div = '';
				var div2 = '';
				var str = result[0]['reports_group'];
				var str_array = str.split(',' || '');
				div += '<div class="row">';
				div += '<div class="col-lg-1" style="margin-top:5px !important;"><div class="form-group"><div class="input-group"><span class=""></span><input type="checkbox" checked="" class="bs_switch" id="includegroup"></div></div></div>';
				div += '<div class="col-lg-11">';

				div2 += '<div class="row">';
				div2 += '<div class="col-lg-1" style="margin-top:5px !important;"><div class="form-group"><div class="input-group"><span class=""></span><input type="checkbox" checked="" class="bs_switch" id="includegroup1"></div></div></div>';
				for(var i = 0; i < str_array.length; i++)
					{
						if (i === 0)
							{
								a = 'btn-primary';
							}else{
								a  = 'btn-default ';
			  				}
							  div += '<a   class="btn ' + a +' btn-sm btnSelCre" style="margin: 5px !important;"> ' + str_array[i]  + " Wise " +'</a>';
							 
			  			}
			  			div += '</div>'+
						  '</div>';
						  
						$(div).appendTo('.AllGroups');
						

						$('#includegroup').bootstrapSwitch();
						$("#includegroup").bootstrapSwitch('offText', 'No');
						$("#includegroup").bootstrapSwitch('onText', 'Yes');

						$('#includegroup1').bootstrapSwitch();
						$("#includegroup1").bootstrapSwitch('offText', 'No');
						$("#includegroup1").bootstrapSwitch('onText', 'Yes');
						$('.btnSelCre').on('click', function(e) {
							e.preventDefault();
							$(this).siblings('.btnSelCre').removeClass('btn-primary');
							$(this).addClass('btn-primary');
						});
						$('.btnSelCre').on('click',function(e){
							e.preventDefault();
							setTimeout(function(){ $('#btnSearch').trigger('click'); }, 300);
						});
									
						$('.btnSelCre2').on('click', function(e) {
							e.preventDefault();
							$(this).siblings('.btnSelCre2').removeClass('btn-primary');
							$(this).addClass('btn-primary');
						});
						$('.btnSelCre2').on('click',function(e){
							e.preventDefault();
							setTimeout(function(){ $('#btnSearch').trigger('click'); }, 300);
						});

			  			    
								if (result[0]['report_types']!=='' || result[0]['report_types']!==null)
								{
			  
							  var types = result[0]['report_types'];
							  if (types!==null) 
							  {
			  
							  var types_array = types.split(',' || '');
							  var divtype =  '';
							  var checked = '';
							  divtype += '<div class="row">';
							  divtype += '<div class="col-lg-4">';
							  for(var j = 0; j < types_array.length; j++)
								  {
									  if (j==0) {
										   checked = 'checked="checked"';
									  }else{
										  checked = '';
									  }
									  divtype += '<label class="radio-inline">';
									  divtype +='<input type="radio" name="types" value="'+types_array[j]+'" '+checked+'>';
									  divtype += ' '+ types_array[j] +' </label>';
									}
									  divtype += '</div>';
									  divtype += '</div>';
									  $(divtype).appendTo('.reporttypes')
							  }
								}
				}
			},
			error: function (result) {
				$("#loading").hide();
				alert("Error:" + result);
			}
		});
	}}


var components = {

	hideLoader : false,

	init 	: function (){
				components.bindUI();
			  },

	bindUI 	: function (){
		

	},
	Components : function(etype) {
		if (etype !== 'inhouse') {
			console.log(etype);
		}
		return etype; 
	},
	isNumber : function(n) { return /^-?[\d.]+(?:e-?\d+)?$/.test(n); }, 
	getCurrentView : function() {
        var what = $('.btnSelCre.btn-primary').text().split('Wise')[0].trim().toLowerCase();
        return what;
	},
	getCurrentView2 : function() {
        var what = $('.btnSelCre2.btn-primary').text().split('Wise')[0].trim().toLowerCase();
        return what;
    },
		   // get Crit Fro Report
    getcrit : function (etype){
        var crit    = {};
        var filters = [];
        $('.filter').each(function(ind, elem) {
            if ($(this).is('select')) {
           var checkexisting = $(this).data("id");
           	if (crit.hasOwnProperty(checkexisting)===false)
           		{
           		crit[$(this).data("id")] = $.trim($(this).select2("val"));	
           		} 
            }
        });
        //console.log(crit)
        return crit;

   },
   getcrittxt : function (etype){
	var crit    = {};
	var results = [];
	$('.filter').each(function(ind, elem) {
		if ($(this).is('select')) {
	   var checkexisting = $(this).data("id");
		   if (crit.hasOwnProperty(checkexisting)===false)
			   {
				   var selected = $(this).find('option:selected', this);
				   console.log(selected)
				   
				   selected.each(function()
					   {
						   var res = $(this).data('displayvalue').toString();
						   res = res.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
						   results.push(res);
					   });
			   } 
		}

	});
	return results;

},
     titleCase : function (str) {
    	var splitStr = str.toLowerCase().split(' ');
    	for (var i = 0; i < splitStr.length; i++) {
    		splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1); 
    	}
    	// Directly return the joined string
    	return splitStr.join(' ');
    },
    // Checking for Values
    checkvaluetype : function(str){
    	var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
    	if(numberRegex.test(str)){
    		return 'text-right';
    	}else{
    		return 'text-left'
    	}
    },
    sortByKeyAsc : function (array, key) {
        return array.sort(function (a, b) {
			var x = a[key]; 
			var y = b[key];
			
			var check = components.isNumber(a[key]);
			if (check === true)
			{
				return ((parseInt(x) < parseInt(y)) ? -1 : ((parseInt(x) > parseInt(y)) ? 1 : 0));
			}else{
				return ((x < y) ? -1 : ((x > y) ? 1 : 0));
			}
            
        });
    },
}

$(function() {
    $(window).on("load", function() {
	components.init();
});
});

