var moulding = function() {
	var settings = {
		// basic information section
		switchPreBal : $('#switchPreBal')

	};
	var saveItem = function( item ) {
		$.ajax({
			url : base_url + 'index.php/item/save',
			type : 'POST',
			data : item,
			// processData : false,
			// contentType : false,
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					alert('Item saved successfully.');
					$('#ItemAddModel').modal('hide');
					fetchItems();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var saveAccount = function( accountObj ) {
		$.ajax({
			url : base_url + 'index.php/account/save',
			type : 'POST',
			data : { 'accountDetail' : accountObj },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'false') {
					alert('An internal error occured while saving account. Please try again.');
				} else {
					alert('Account saved successfully.');
					$('#AccountAddModel').modal('hide');
					fetchAccount();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

var fetchAccount = function() {

		$.ajax({
			url : base_url + 'index.php/account/fetchAll',
			type : 'POST',
			data : { 'active' : 1 },
			dataType : 'JSON',
			success : function(data) {
				if (data === 'false') {
					alert('No data found');
				} else {
					populateDataAccount(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var fetchItems = function() {
		$.ajax({
			url : base_url + 'index.php/item/fetchAll',
			type : 'POST',
			data : { 'active' : 1 },
			dataType : 'JSON',
			success : function(data) {
				if (data === 'false') {
					alert('No data found');
				} else {
					populateDataItem(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateDataAccount = function(data) {
		$("#party_dropdown").empty();
		
		$.each(data, function(index, elem){
			var opt="<option value='"+elem.party_id+"' >" +  elem.name + "</option>";
			$(opt).appendTo('#party_dropdown');
		});
	}
	var populateDataItem = function(data) {
		$("#itemid_dropdown").empty();
		$("#item_dropdown").empty();

		$.each(data, function(index, elem){
			var opt="<option value='"+elem.item_id+"' data-prate= '"+ elem.cost_price +"' data-uom_item= '"+ elem.uom +"' data-grweight= '"+ elem.grweight +"' >" +  elem.item_des + "</option>";
			 // var = "<option value='" + $item['item_id'] + "' data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>"><?php echo $item['item_des']; ?></option>";
			$(opt).appendTo('#item_dropdown');
			var opt1="<option value='"+elem.item_id+"' data-prate= '"+ elem.cost_price +"' data-uom_item= '"+ elem.uom +"' data-grweight= '"+ elem.grweight +"' >" +  elem.item_id + "</option>";
			 // var = "<option value='" + $item['item_id'] + "' data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>"><?php echo $item['item_des']; ?></option>";
			$(opt1).appendTo('#itemid_dropdown');

		});
	}
	var getSaveObjectAccount = function() {

		var obj = {
			pid : '20000',
			active : '1',
			name : $.trim($('#txtAccountName').val()),
			level3 : $.trim($('#txtLevel3').val()),
			dcno : $('#txtVrnoa').val(),
			etype : 'purchase',
			uid : $.trim($('#uid').val()),
			company_id : $.trim($('#cid').val()),
		};

		return obj;
	}
var getSaveObjectItem = function() {
		
		var itemObj = {
			item_id : 20000,
			active : '1',
			open_date : $.trim($('#current_date').val()),
			catid : $('#category_dropdown').val(),
			subcatid : $.trim($('#subcategory_dropdown').val()),
			bid : $.trim($('#brand_dropdown').val()),
			barcode : $.trim($('#txtBarcode').val()),
			description : $.trim($('#txtItemName').val()),
			item_des : $.trim($('#txtItemName').val()),
			cost_price : $.trim($('#txtPurPrice').val()),
			srate : $.trim($('#txtSalePrice').val()),
			uid : $.trim($('#uid').val()),
			company_id : $.trim($('#cid').val()),
		};
		return itemObj;
	}

	var validateSaveItem = function() {

		var errorFlag = false;
		// var _barcode = $('#txtBarcode').val();
		var _desc = $.trim($('#txtItemName').val());
		var cat = $.trim($('#category_dropdown').val());
		var subcat = $('#subcategory_dropdown').val();
		var brand = $.trim($('#brand_dropdown').val());

		// remove the error class first
		$('.inputerror').removeClass('inputerror');
		if ( _desc === '' || _desc === null ) {
			$('#txtItemName').addClass('inputerror');
			errorFlag = true;
		}
		if ( !cat ) {
			$('#category_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		if ( !subcat ) {
			$('#subcategory_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		if ( !brand ) {
			$('#brand_dropdown').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}
	var validateSaveAccount = function() {

		var errorFlag = false;
		var partyEl = $('#txtAccountName');
		var deptEl = $('#txtLevel3');

		// remove the error class first
		$('.inputerror').removeClass('inputerror');

		if ( !partyEl.val() ) {
			$('#txtAccountName').addClass('inputerror');
			errorFlag = true;
		}
		if ( !deptEl.val() ) {
			deptEl.addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}
	var save = function(moulding) {
		general.disableSave();
		
		$.ajax({
			url : base_url + 'index.php/moulding/save',
			type : 'POST',
			data : {'vrdate' : $('#vrdate').val(), 'stockmain' : moulding.stockmain, 'stockdetail' : moulding.stockdetail, 'vrnoa' : moulding.vrnoa, 'ledger' : moulding.ledger ,'voucher_type_hidden':$('#voucher_type_hidden').val() },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'date close') {
					alert('Sorry! you can not insert update in close date................');
				}else if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					var printConfirmation = confirm('Voucher saved!\nWould you like to print the invoice as well?');
					if (printConfirmation === true) {
						Print_Voucher();
					} else {
						general.reloadWindow();
					}
				}
				general.enableSave();
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var Print_Voucher = function( ) {
		var etype=  'moulding';
		var vrnoa = $('#txtVrnoa').val();
		var company_id = $('#cid').val();
		var user = $('#uname').val();
		var pre_bal_print = ($(settings.switchPreBal).bootstrapSwitch('state') === true) ? '1' : '0';
		
		var url = base_url + 'index.php/doc/Print_Voucher/' + etype + '/' + vrnoa + '/' + company_id + '/' + '-1' + '/' + user + '/' + pre_bal_print;
		// var url = base_url + 'index.php/doc/CashVocuherPrintPdf/' + etype + '/' + dcno   + '/' + companyid + '/' + '-1' + '/' + user;
		window.open(url);
	}

	var fetch = function(vrnoa) {

		$.ajax({

			url : base_url + 'index.php/moulding/fetch',
			type : 'POST',
			data : { 'vrnoa' : vrnoa , 'company_id': $('#cid').val()},
			dataType : 'JSON',
			success : function(data) {

				resetFields();
				$('#txtOrderNo').val('');
				if (data === 'false') {
					alert('No data found.');
					getMaxVrnoa();
				} else {
					populateData(data);
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateData = function(data) {
		// alert(data[0]['discount']);
		$('#txtVrno').val(data[0]['vrno']);
		$('#txtVrnoHidden').val(data[0]['vrno']);
		$('#txtVrnoaHidden').val(data[0]['vrnoa']);
		$('#txtIdImg').val(data[0]['vrnoa']);
		$('#current_date').datepicker('update', data[0]['vrdate'].substr(0, 10));
		$('#vrdate').datepicker('update', data[0]['vrdate'].substr(0, 10));
		$('#Memployee_dropdown').select2('val', data[0]['party_id']);
		$('#Demployee_dropdown').select2('val', data[0]['employee_id']);
		//$('#txtInvNo').val(data[0]['inv_no']);
		// $('#due_date').datepicker('update', data[0]['due_date'].substr(0, 10));
		$('#receivers_list').val(data[0]['received_by']);
		// $('#transporter_dropdown').select2('val', data[0]['transporter_id']);
		$('#txtRemarks').val(data[0]['remarks']);
		$('#txtNetAmount').val(data[0]['namount']);
		// $('#txtOrderNo').val(data[0]['order_no']);
		
		$('#txtMouldBonus').val(data[0]['discp']);
		$('#txtDharyDed').val(data[0]['exppercent']);
		$('#txtMouldDed').val(data[0]['expense']);
		$('#txtDharyBonus').val(data[0]['taxpercent']);
		$('#txtDharyAmount').val(data[0]['tax']);
		$('#txtMouldAmount').val(data[0]['discount']);
		$('#user_dropdown').val(data[0]['uid']);
		$('#cash_dropdown').select2('val', data[0]['currency_id']);
		$('#exp_dropdown').select2('val', data[0]['party_id_co']);
		$('#txtMPaid').val(data[0]['paid']);
		$('#txtDPaid').val(data[0]['dharrypaid']);
		$('#dept_dropdown').select2('val', data[0]['godown_id']);
		$('#voucher_type_hidden').val('edit');		
		
		$.each(data, function(index, elem) {
			if (elem.s_qty>0)
        {
            elem.s_amount=parseFloat(elem.s_amount).toFixed(2);
            elem.s_damount=parseFloat(elem.s_damount).toFixed(2);

            appendToTable('', elem.item_name, elem.item_id, elem.s_qty, elem.s_rate, elem.s_amount, elem.weight, elem.s_discount, elem.s_damount, elem.uom);
            calculateLowerTotal(elem.s_qty, elem.weight, elem.s_damount,0);
        }

		});
	}
	

	// gets the max id of the voucher
	var getMaxVrno = function() {
		$.ajax({
			url : base_url + 'index.php/moulding/getMaxVrno',
			type : 'POST',
			data : {'company_id': $('#cid').val()},
			dataType : 'JSON',
			success : function(data) {
				$('#txtVrno').val(data);
				$('#txtMaxVrnoHidden').val(data);
				$('#txtVrnoHidden').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var getMaxVrnoa = function() {

		$.ajax({

			url : base_url + 'index.php/moulding/getMaxVrnoa',
			type : 'POST',
			data : {'company_id': $('#cid').val()},
			dataType : 'JSON',
			success : function(data) {

				$('#txtVrnoa').val(data);
				$('#txtMaxVrnoaHidden').val(data);
				$('#txtVrnoaHidden').val(data);
				$('#txtIdImg').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var validateSingleProductAdd = function() {


		var errorFlag = false;
		var itemEl = $('#hfItemId');
		var qtyEl = $('#txtQty');
		var rateEl = $('#txtPRate');

		// remove the error class first
		$('.inputerror').removeClass('inputerror');

		if ( !itemEl.val() ) {
			$('#txtItemId').addClass('inputerror');
			errorFlag = true;
		}
		if ( !qtyEl.val() ) {
			qtyEl.addClass('inputerror');
			errorFlag = true;
		}
		if ( !rateEl.val() ) {
			rateEl.addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var appendToTable = function(srno, item_desc, item_id, qty, rate, amount, weight,discount, damount, uom) {
        amount=parseFloat(amount).toFixed(2);
        damount=parseFloat(damount).toFixed(2);
		var srno = $('#moulding_table tbody tr').length + 1;
		var row = 	"<tr>" +
						"<td class='srno'> "+ srno +"</td>" +
				 		"<td class='item_desc' data-item_id='"+ item_id +"'> "+ item_desc +"</td>" +
				 		"<td class='uom'> "+ uom +"</td>" +
				 		"<td class='qty'> "+ qty +"</td>" +
					 	"<td class='weight'> "+ weight +"</td>" +
					 	"<td class='rate'> "+ rate +"</td>" +
					 	"<td class='amount'> "+ amount +"</td>" +
					 	"<td class='discount'> "+ discount +"</td>" +
					 	"<td class='damount'> "+ damount +"</td>" +
					 	"<td><a href='' class='btn btn-primary btnRowEdit'><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td>" +
				 	"</tr>";
		$(row).appendTo('#moulding_table');
	}

	var getPartyId = function(partyName) {
		var pid = "";
		$('#employee_dropdown option').each(function() { if ($(this).text().trim().toLowerCase() == partyName) pid = $(this).val();  });
		return pid;
	}

	var getSaveObject = function() {

		var ledgers = [];
		var stockmain = {};
		var stockdetail = [];

		stockmain.vrno = $('#txtVrnoHidden').val();
		stockmain.vrnoa = $('#txtVrnoaHidden').val();
		stockmain.vrdate = $('#current_date').val();
		stockmain.party_id = $('#Memployee_dropdown').val();
		stockmain.employee_id = $('#Demployee_dropdown').val();
		// stockmain.godown_id = $('#dept_dropdown').val();
		// stockmain.bilty_no = $('#txtInvNo').val();
		// stockmain.bilty_date = $('#due_date').val();
		stockmain.received_by = $('#receivers_list').val();
		// stockmain.transporter_id = $('#transporter_dropdown').val();
		stockmain.remarks = $('#txtRemarks').val();
		stockmain.etype = 'moulding';
		stockmain.namount = $('#txtNetAmount').val();
		// stockmain.order_vrno = $('#txtOrderNo').val();
		stockmain.discp = $('#txtMouldBonus').val();
		stockmain.discount = $('#txtMouldAmount').val();
		// alert(stockmain.discount);
		stockmain.expense =$('#txtMouldDed').val();
		stockmain.exppercent = $('#txtDharyDed').val();
		stockmain.tax = $('#txtDharyAmount').val();
		stockmain.taxpercent = $('#txtDharyBonus').val();
		stockmain.paid = $('#txtMPaid').val();
		stockmain.dharrypaid = $('#txtDPaid').val();
		stockmain.currency_id = $('#cash_dropdown').val();
		stockmain.party_id_co = $('#exp_dropdown').val();

		stockmain.uid = $('#uid').val();
		stockmain.company_id = $('#cid').val();



		$('#moulding_table').find('tbody tr').each(function( index, elem ) {
			var sd = {};
			sd.stid = '';
			sd.item_id = $.trim($(elem).find('td.item_desc').data('item_id'));
			sd.godown_id = $('#dept_dropdown').val();
			sd.qty = $.trim($(elem).find('td.qty').text());
			sd.weight = $.trim($(elem).find('td.weight').text());
			sd.rate = $.trim($(elem).find('td.rate').text());
			sd.discount = $.trim($(elem).find('td.discount').text());
			sd.damount = $.trim($(elem).find('td.damount').text());
			sd.amount = $.trim($(elem).find('td.amount').text());
			sd.netamount = $.trim($(elem).find('td.amount').text());
			stockdetail.push(sd);
		});

		///////////////////////////////////////////////////////////////
		//// For Over All Voucher
		///////////////////////////////////////////////////////////////
		var net_mould = parseFloat(getNumVal($('#txtMouldAmount')))+parseFloat(getNumVal($('#txtMouldBonus')))- parseFloat(getNumVal($('#txtMouldDed')));
		var pledger = {};
		pledger.pledid = '';
		pledger.pid = $('#Memployee_dropdown').val();
		pledger.description =  $('#txtRemarks').val();
		pledger.remarks =  $('#txtRemarks').val();
		pledger.date = $('#current_date').val();
		pledger.debit = 0;
		pledger.credit = net_mould;
		pledger.dcno = $('#txtVrnoaHidden').val();
		pledger.invoice = $('#txtVrnoaHidden').val();
		pledger.etype = 'moulding';
		pledger.pid_key = $('#exp_dropdown').val();
		pledger.uid = $('#uid').val();
		pledger.company_id = $('#cid').val();
		pledger.isFinal = 0;	
		ledgers.push(pledger);

		var pledger = {};
		pledger.pledid = '';
		pledger.pid = $('#exp_dropdown').val();
		pledger.description = $('#employee_dropdown').find('option:selected').text();
		pledger.remarks =  $('#txtRemarks').val();
		pledger.date = $('#current_date').val();
		pledger.debit = net_mould;
		pledger.credit = 0;
		pledger.dcno = $('#txtVrnoaHidden').val();
		pledger.invoice = $('#txtInvNo').val();
		pledger.etype = 'moulding';
		pledger.pid_key = $('#Memployee_dropdown').val();
		pledger.uid = $('#uid').val();
		pledger.company_id = $('#cid').val();	
		pledger.isFinal = 0;
		ledgers.push(pledger);

		var net_dharry = parseFloat(getNumVal($('#txtDharyGAmount')))+parseFloat(getNumVal($('#txtDharyBonus')))- parseFloat(getNumVal($('#txtDharyDed')));
		var pledger = {};
		pledger.pledid = '';
		pledger.pid = $('#Demployee_dropdown').val();
		pledger.description =  $('#txtRemarks').val();
		pledger.remarks =  $('#txtRemarks').val();
		pledger.date = $('#current_date').val();
		pledger.debit = 0;
		pledger.credit = net_dharry;
		pledger.dcno = $('#txtVrnoaHidden').val();
		pledger.invoice = $('#txtVrnoaHidden').val();
		pledger.etype = 'moulding';
		pledger.pid_key = $('#exp_dropdown').val();
		pledger.uid = $('#uid').val();
		pledger.company_id = $('#cid').val();
		pledger.isFinal = 0;	
		ledgers.push(pledger);

		var pledger = {};
		pledger.pledid = '';
		pledger.pid = $('#exp_dropdown').val();
		pledger.description = $('#employee_dropdown').find('option:selected').text();
		pledger.remarks =  $('#txtRemarks').val();
		pledger.date = $('#current_date').val();
		pledger.debit = net_dharry;
		pledger.credit = 0;
		pledger.dcno = $('#txtVrnoaHidden').val();
		pledger.invoice = $('#txtInvNo').val();
		pledger.etype = 'moulding';
		pledger.pid_key = $('#Demployee_dropdown').val();
		pledger.uid = $('#uid').val();
		pledger.company_id = $('#cid').val();	
		pledger.isFinal = 0;
		ledgers.push(pledger);

		if ($('#txtMPaid').val() != 0 ) {
			pledger = undefined;
			var pledger = {};
			pledger.etype = 'moulding';
			pledger.description = $('#Memployee_dropdown option:selected').text() + '.';
			pledger.remarks =  $('#txtRemarks').val();
			// pledger.description = 'moulding Head';
			pledger.dcno = $('#txtVrnoaHidden').val();
			pledger.invoice = $('#txtVrnoaHidden').val();
			pledger.pid = $('#cash_dropdown').val();
			pledger.date = $('#current_date').val();
			pledger.debit = 0;
			pledger.credit = $('#txtMPaid').val();
			pledger.isFinal = 0;
			pledger.uid = $('#uid').val();
			pledger.company_id = $('#cid').val();	
			pledger.pid_key = $('#Memployee_dropdown').val();
			ledgers.push(pledger);

			pledger = undefined;
			var pledger = {};
			pledger.etype = 'moulding';
			pledger.description =  'Cash Paid';
			pledger.remarks =  $('#txtRemarks').val();
			// pledger.description = 'moulding Head';
			pledger.dcno = $('#txtVrnoaHidden').val();
			pledger.invoice = $('#txtVrnoaHidden').val();
			pledger.pid = $('#Memployee_dropdown').val();
			pledger.date = $('#current_date').val();
			pledger.debit = $('#txtMPaid').val();
			pledger.credit = 0;
			pledger.isFinal = 0;
			pledger.uid = $('#uid').val();
			pledger.company_id = $('#cid').val();	
			pledger.pid_key = $('#cash_dropdown').val();
			ledgers.push(pledger);
		}
		if ($('#txtDPaid').val() != 0 ) {
			pledger = undefined;
			var pledger = {};
			pledger.etype = 'moulding';
			pledger.description = $('#Demployee_dropdown option:selected').text() + '.';
			pledger.remarks =  $('#txtRemarks').val();
			// pledger.description = 'moulding Head';
			pledger.dcno = $('#txtVrnoaHidden').val();
			pledger.invoice = $('#txtVrnoaHidden').val();
			pledger.pid = $('#cash_dropdown').val();
			pledger.date = $('#current_date').val();
			pledger.debit = 0;
			pledger.credit = $('#txtDPaid').val();
			pledger.isFinal = 0;
			pledger.uid = $('#uid').val();
			pledger.company_id = $('#cid').val();	
			pledger.pid_key = $('#Demployee_dropdown').val();
			ledgers.push(pledger);

			pledger = undefined;
			var pledger = {};
			pledger.etype = 'moulding';
			pledger.description =  'Cash Paid';
			pledger.remarks =  $('#txtRemarks').val();
			// pledger.description = 'moulding Head';
			pledger.dcno = $('#txtVrnoaHidden').val();
			pledger.invoice = $('#txtVrnoaHidden').val();
			pledger.pid = $('#Demployee_dropdown').val();
			pledger.date = $('#current_date').val();
			pledger.debit = $('#txtDPaid').val();
			pledger.credit = 0;
			pledger.isFinal = 0;
			pledger.uid = $('#uid').val();
			pledger.company_id = $('#cid').val();	
			pledger.pid_key = $('#cash_dropdown').val();
			ledgers.push(pledger);
		}
		var data = {};
		data.stockmain = stockmain;
		data.stockdetail = stockdetail;
		// data.orderdetail = orderdetail;
		data.ledger = ledgers;
		data.vrnoa = $('#txtVrnoaHidden').val();
		return data;
	}

	// checks for the empty fields
	 var validateSave = function() {

		var errorFlag = false;
		var employeeE1 = $('#Memployee_dropdown');
		var employeeE2 = $('#Demployee_dropdown');
		var deptEl = $('#dept_dropdown');
		var CashEl = $('#cash_dropdown');
		var ExpEl = $('#exp_dropdown');


		// remove the error class first
		$('.inputerror').removeClass('inputerror');

		if ( !employeeE1.val() ) {
			employeeE1.addClass('inputerror');
			errorFlag = true;
		}
		if ( !employeeE2.val() ) {
			employeeE2.addClass('inputerror');
			errorFlag = true;
		}
		if ( !deptEl.val() ) {
			deptEl.addClass('inputerror');
			errorFlag = true;
		}
		
		if ( !((CashEl.val() != '' && CashEl.val() != null) && (ExpEl.val() != '' && ExpEl.val() != null)) ) {
			CashEl.addClass('inputerror');
			ExpEl.addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}
	var deleteVoucher = function(vrnoa) {

		$.ajax({
			url : base_url + 'index.php/moulding/delete',
			type : 'POST',
			data : {'chk_date' : $('#current_date').val(), 'vrdate' : $('#vrdate').val(), 'vrnoa' : vrnoa , 'etype':'moulding','company_id':$('#cid').val() },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'date close') {
					alert('Sorry! you can not delete in close date................');
				}else if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	///////////////////////////////////////////////////////////////
	/// calculations related to the overall voucher
	////////////////////////////////////////////////////////////////
	var calculateLowerTotal = function(qty, weight,DharyAmount,MouldAmount) {

		var _qty = getNumVal($('#txtTotalQty'));
		var _weight = getNumVal($('#txtTotalWeight'));

		var _dharyamount = getNumVal($('#txtDharyGAmount'));
		var _mouldamount = getNumVal($('#txtMouldAmount'));

		var _mouldbonus = getNumVal($('#txtMouldBonus'));
		var _dharybonus = getNumVal($('#txtDharyBonus'));
		
		var _mouldded = getNumVal($('#txtMouldDed'));
		var _dharyded = getNumVal($('#txtDharyDed'));


		var tempQty = parseFloat(_qty) + parseFloat(qty);
		$('#txtTotalQty').val(parseFloat(tempQty).toFixed(2));
		var totalWeight = parseFloat(_weight) + parseFloat(weight);
		$('#txtTotalWeight').val(parseFloat(totalWeight).toFixed(2));

		var tempDharyAmount = parseFloat(_dharyamount) + parseFloat(DharyAmount);
		$('#txtDharyGAmount').val(parseFloat(tempDharyAmount).toFixed(2));
		
		var tempMoulAmount = parseFloat(_mouldamount) + parseFloat(MouldAmount);
		$('#txtMouldAmount').val(parseFloat(tempMoulAmount).toFixed(2));
		
		tempNetAmount= parseFloat(tempMoulAmount) + parseFloat(tempDharyAmount) - parseFloat(_mouldded) - parseFloat(_dharyded) + parseFloat(_mouldbonus) + parseFloat(_dharybonus);
		$('#txtNetAmount').val(parseFloat(tempNetAmount).toFixed(2));
	}

	var getNumVal = function(el){
		return isNaN(parseFloat(el.val())) ? 0 : parseFloat(el.val());
	}

	///////////////////////////////////////////////////////////////
	/// calculations related to the single product calculation
	///////////////////////////////////////////////////////////////
	var calculateUpperSum = function() {

		var _qty = getNumVal($('#txtQty'));
		var _amnt = getNumVal($('#txtAmount'));
		var _net = getNumVal($('#txtNet'));
		var _prate = getNumVal($('#txtPRate'));
		var _gw = getNumVal($('#txtGWeight'));
		var _weight=getNumVal($('#txtWeight'));
		var _dharyrate=getNumVal($('#txtDharyRate'));
		

		var _uom=$('#txtUom').val();
		// alert('uom_item ' + _uom);
		kg=-1;
		gram=-1;
		var kg = _uom.search("KG");
		var gram = _uom.search("GRAM");
		// if (kg ==-1 && gram ==-1 ){
		// 	var _tempAmnt = parseFloat(_qty) * parseFloat(_prate);			
		// }else{
		// 	var _tempAmnt = parseFloat(_weight) * parseFloat(_prate);			
		// }
		var _tempAmnt = parseFloat(_qty) * parseFloat(_prate);
		var _tempAmntDhary = parseFloat(_qty) * parseFloat(_dharyrate);
		if(_gw != 0 && _gw != null){
			$('#txtWeight').val(parseFloat(_gw) * parseFloat(_qty));
		}
		$('#txtAmount').val(_tempAmnt);
		$('#txtDharyAmount').val(_tempAmntDhary);

	}

	var fetchThroughPO = function(po) {

		$.ajax({

			url : base_url + 'index.php/mouldingorder/fetch',
			type : 'POST',
			data : { 'vrnoa' : po },
			dataType : 'JSON',
			success : function(data) {

				resetFields();
				if (data === 'false') {
					alert('No data found.');
				} else {
					populatePOData(data);
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var calculateUpperSumL = function() {

		var _qty = getNumVal($('#txtQtyL'));
		var _amnt = getNumVal($('#txtAmountL'));
		var _net = getNumVal($('#txtNet'));
		var _prate = getNumVal($('#txtPRateL'));
		var _gw = getNumVal($('#txtGWeightL'));
		var _weight=getNumVal($('#txtWeightL'));
		var _dharyrate=getNumVal($('#txtDharyRateL'));
		

		var _uom=$('#txtUom').val();
		// alert('uom_item ' + _uom);
		kg=-1;
		gram=-1;
		var kg = _uom.search("KG");
		var gram = _uom.search("GRAM");
		// if (kg ==-1 && gram ==-1 ){
		// 	var _tempAmnt = parseFloat(_qty) * parseFloat(_prate);			
		// }else{
		// 	var _tempAmnt = parseFloat(_weight) * parseFloat(_prate);			
		// }
		var _tempAmnt = parseFloat(_qty) * parseFloat(_prate);
		var _tempAmntDhary = parseFloat(_qty) * parseFloat(_dharyrate);
		if(_gw != 0 && _gw != null){
			$('#txtWeightL').val(parseFloat(_gw) * parseFloat(_qty));
		}
		$('#txtAmountL').val(_tempAmnt);
		$('#txtDharyAmountL').val(_tempAmntDhary);

	}

	var populatePOData = function(data) {

		$('#employee_dropdown').select2('val', data[0]['party_id']);
		$('#txtRemarks').val(data[0]['remarks']);
		$('#txtMouldBonus').val(data[0]['discp']);
		$('#txtDharyDed').val(data[0]['exppercent']);
		$('#txtMouldDed').val(data[0]['expense']);
		$('#txtDharyBonus').val(data[0]['taxpercent']);
		// $('#txtDharyAmount').val(data[0]['tax']);
		// $('#txtMouldAmount').val(data[0]['discount']);

		$('#dept_dropdown').select2('val', data[0]['godown_id']);
		$('#txtNetAmount').val(data[0]['namount']);
		$('#voucher_type_hidden').val('edit');

		$.each(data, function(index, elem) {
			appendToTable('', elem.item_name, elem.item_id, elem.item_qty, elem.item_rate, elem.item_amount, elem.weight);
			calculateLowerTotal(elem.item_qty, elem.item_amount, elem.weight);
		});
	}

	var resetFields = function() {

		$('#current_date').datepicker('update', new Date());
		$('#Memployee_dropdown').select2('val', '');
		$('#Demployee_dropdown').select2('val', '');
		$('#txtInvNo').val('');
		$('#due_date').datepicker('update', new Date());
		$('#receivers_list').val('');
		$('#transporter_dropdown').select2('val', '');
		$('#txtRemarks').val('');
		$('#txtNetAmount').val('');		
		$('#txtMouldBonus').text('');
		$('#txtDharyDed').text('');
		$('#txtMouldDed').text('');
		$('#txtDharyBonus').text('');
		$('#txtDharyAmount').text('');
		$('#txtMouldAmount').text('');
		$('#txtMouldAmount').val('');
		$('#txtTotalAmount').val('');
		$('#txtTotalQty').val('');
		$('#txtTotalWeight').val('');
		$('#dept_dropdown').select2('val', '');
		$('#voucher_type_hidden').val('new');
		$('#txtNetAmount').val('');
		$('#txtMouldBonus').val('');
		$('#txtMouldAmount').val('');
		$('#txtMouldDed').val('');
		$('#txtDharyDed').val('');
		$('#txtDharyAmount').val('');
		$('#txtDharyGAmount').val('');
		$('#txtDharyBonus').val('');
		$('#txtPaid').val('');
		$('#cash_dropdown').val('');
		$('#exp_dropdown').val('');
		$('#voucher_type_hidden').val('new');
		$('table tbody tr').remove();
	}

	var clearItemData = function (){
		
		$("#hfItemId").val("");
		$("#hfItemSize").val("");
		$("#hfItemBid").val("");
		$("#hfItemUom").val("");
		$("#hfItemPrate").val("");
		$("#hfItemGrWeight").val("");
		$("#hfItemStQty").val("");
		$("#hfItemStWeight").val("");
		$("#hfItemLength").val("");
		$("#hfItemCatId").val("");
		$("#hfItemSubCatId").val("");
		$("#hfItemDesc").val("");
		$("#hfItemShortCode").val("");

        //$("#txtItemId").val("");
    }
    
	return {

		init : function() {
			this.bindUI();
			this.bindModalPartyGrid();
			this.bindModalItemGrid();
		},

		bindUI : function() {
			var self = this;


			$('#employee_dropdown').focus().css('border', '1px solid #368EE0');

			$('.btn').focus(function(){
				$(this).css('border', '2px solid black');
			}
			);
			$('.btn').blur(function(){
				$(this).css('border', '');
			}
			);


			$('#txtLevel3').on('change', function() {
				var level3 = $('#txtLevel3').val();
				$('#txtselectedLevel1').text('');
				$('#txtselectedLevel2').text('');
				if (level3 !== "" && level3 !== null) {
					// alert('enter' + $(this).find('option:selected').data('level2') );	
					$('#txtselectedLevel2').text(' ' + $(this).find('option:selected').data('level2'));
					$('#txtselectedLevel1').text(' ' + $(this).find('option:selected').data('level1'));
				}
			});
			// $('#txtLevel3').select2();
			$('.btnSaveM').on('click',function(e){
				if ( $('.btnSave').data('saveaccountbtn')==0 ){
					alert('Sorry! you have not save accounts rights..........');
				}else{
					e.preventDefault();
					self.initSaveAccount();
				}
			});
			$('.btnResetM').on('click',function(){
				
				$('#txtAccountName').val('');
				$('#txtselectedLevel2').text('');
				$('#txtselectedLevel1').text('');
				$('#txtLevel3').select2('val','');
			});
			$('#AccountAddModel').on('shown.bs.modal',function(e){
				$('#txtAccountName').focus();
			});
			shortcut.add("F3", function() {
    			$('#AccountAddModel').modal('show');
			});

			$('.btnSaveMItem').on('click',function(e){
				if ( $('.btnSave').data('saveitembtn')==0 ){
					alert('Sorry! you have not save item rights..........');
				}else{
					e.preventDefault();
					self.initSaveItem();
				}
			});

			$("#switchPreBal").bootstrapSwitch('offText', 'No');
			$("#switchPreBal").bootstrapSwitch('onText', 'Yes');
			$('#voucher_type_hidden').val('new');
			$('.modal-lookup .populateAccount').on('click', function(){
				var party_id = $(this).closest('tr').find('input[name=hfModalPartyId]').val();
				$("#employee_dropdown").select2("val", party_id); 				
			});
			$('.modal-lookup .populateItem').on('click', function(){
				// alert('dfsfsdf');
				var item_id = $(this).closest('tr').find('input[name=hfModalitemId]').val();
				$("#item_dropdown").select2("val", item_id); //set the value
				$("#itemid_dropdown").select2("val", item_id);
				$('#txtQty').focus();				
			});

			$('#voucher_type_hidden').val('new');
			$('#txtVrnoa').on('change', function() {
				fetch($(this).val());
			});

			$('.btnSave').on('click',  function(e) {
				if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('updatebtn')==0 ){
					alert('Sorry! you have not update rights..........');
				}else if($('#voucher_type_hidden').val()=='new' && $('.btnSave').data('insertbtn')==0){
					alert('Sorry! you have not insert rights..........');
				}else{
					e.preventDefault();
					self.initSave();
				}
			});

			$('.btnPrint').on('click',  function(e) {
				e.preventDefault();
				Print_Voucher();
			});

			$('.btnReset').on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			$('.btnDelete').on('click', function(e){
				e.preventDefault();
				var vrnoa = $('#txtVrnoaHidden').val();
				if (vrnoa !== '') {
					if ($('.btnSave').data('deletebtn')==0)
					{
						alert('Sorry! you have no Delete rights..........');
					}
					else
					{
					if (confirm('Are you sure to delete this voucher?'))
						deleteVoucher(vrnoa);
					}
				}
			});

			$('#txtOrderNo').on('keypress', function(e) {
				if (e.keyCode === 13) {
					e.preventDefault();
					if ($(this).val() != '') {
						fetchThroughPO($(this).val());
					}
				}
			});

			/////////////////////////////////////////////////////////////////
			/// setting calculations for the single product
			/////////////////////////////////////////////////////////////////

			$('#txtWeight').on('input', function() {
				//calculateUpperSum();
			});
			$('#txtDharyRate').on('input', function() {
				calculateUpperSum();
			});

			$('#itemid_dropdown').on('change', function() {
				var item_id = $(this).val();
				var prate = $(this).find('option:selected').data('prate');
				var grweight = $(this).find('option:selected').data('grweight');
				var uom_item = $(this).find('option:selected').data('uom_item');
				var mould = $(this).find('option:selected').data('mould');
				var dharry = $(this).find('option:selected').data('dharry');
				// $('#txtQty').val('1');
				//$('#txtPRate').val(parseFloat(prate).toFixed(2));
				$('#txtPRate').val(parseFloat(mould).toFixed(2));
				$('#txtDharyRate').val(parseFloat(dharry).toFixed(2));
				$('#item_dropdown').select2('val', item_id);
				$('#txtGWeight').val(parseFloat(grweight).toFixed(2));
				$('#txtUom').val(uom_item);

				// calculateUpperSum();
				// $('#txtQty').focus();
			});
			$('#item_dropdown').on('change', function() {
				var item_id = $(this).val();
				var prate = $(this).find('option:selected').data('prate');
				var grweight = $(this).find('option:selected').data('grweight');
				var uom_item = $(this).find('option:selected').data('uom_item');
				var mould = $(this).find('option:selected').data('mould');
				var dharry = $(this).find('option:selected').data('dharry');
				// $('#txtQty').val('1');
				//$('#txtPRate').val(parseFloat(prate).toFixed(2));
				$('#txtPRate').val(parseFloat(mould).toFixed(2));
				$('#txtDharyRate').val(parseFloat(dharry).toFixed(2));
				$('#itemid_dropdown').select2('val', item_id);
				$('#txtGWeight').val(parseFloat(grweight).toFixed(2));
				$('#txtUom').val(uom_item);
				// calculateUpperSum();
				// $('#txtQty').focus();
			});
			$('#txtQty').on('input', function() {
				calculateUpperSum();
			});
			$('#txtPRate').on('input', function() {
				calculateUpperSum();
			});


			$('#btnAdd').on('click', function(e) {
				e.preventDefault();

				var error = validateSingleProductAdd();
				if (!error) {

					var item_desc = $('#itemDesc').val();
					var uitem_des = $('#hfItemUname').val();
					var uom_item = $('#hfItemUom').val();
					var item_code = $('#hfItemShortCode').val();
					var item_id = $('#hfItemId').val();
					var qty = $('#txtQty').val();
					var rate = $('#txtPRate').val();
					var weight = $('#txtWeight').val();
					var amount = $('#txtAmount').val();
					var uom = $('#txtUom').val();

					var dharyrate = $('#txtDharyRate').val();
					var dharyamount = $('#txtDharyAmount').val();

					// reset the values of the annoying fields
					$('#itemDesc').val('');
					$('#txtItemId').val('');
					$('#txtQty').val('');
					$('#txtPRate').val('');
					$('#txtWeight').val('');
					$('#txtAmount').val('');
					$('#txtGWeight').val('');
					$('#txtDharyRate').val('');
					$('#txtDharyAmount').val('');

					appendToTable('', item_desc, item_id, qty, rate, amount, weight, dharyrate, dharyamount,uom);
					calculateLowerTotal(qty, weight,dharyamount, amount);
					$("#txtItemId").focus();
					$('#itemid_dropdown').val('opne');
				} else {
					alert('Correct the errors!');
				}

			});

			$('#less_btnAdd').on('click', function(e) {
				e.preventDefault();

				var error = validateSingleProductAdd_less();
				if (!error) {

					var item_desc = $('#item_dropdownless').find('option:selected').text();
					var item_id = $('#item_dropdownless').val();
					var qty = $('#txtQtyL').val();
					// var rate = $('#txtWeightL').val();
					var weight = $('#txtWeightL').val();
					var amount = $('#txtTotWeightL').val();

					// reset the values of the annoying fields
					$('#itemid_dropdownL').select2('val', '');
					$('#item_dropdownless').select2('val', '');
					$('#txtQtyL').val('');
					$('#txtWeightL').val('');
					$('#txtTotWeightL').val('');
					// $('#less_txtAmount').val('');
					// $('#less_txtGWeight').val('');

					appendToTable('', item_desc, item_id, weight, tweight, "less");
					//calculateLowerTotal(0, 0, 0,amount);
					$('#less_item_dropdown').focus();
				} else {
					alert('Correct the errors!');
				}


			});

			var countItem = 0;
			$('input[id="txtItemId"]').autoComplete({

				minChars: 1,
				cache: false,
				menuClass: '',
				source: function(search, response)
				{
					try { xhr.abort(); } catch(e){}
					$('#txtItemId').removeClass('inputerror');
					$("#imgItemLoader").hide();
					if(search != "")
					{
						xhr = $.ajax({
							url: base_url + 'index.php/item/searchitem',
							type: 'POST',
							data: {

								search: search
							},
							dataType: 'JSON',
							beforeSend: function (data) {

								$(".loader").hide();
								$("#imgItemLoader").show();
								countItem = 0;
							},
							success: function (data) {

								if(data == '')
								{
									$('#txtItemId').addClass('inputerror');
									clearItemData();
									$('#itemDesc').val('');
									$('#txtQty').val('');
									$('#txtPRate').val('');
									$('#txtBundle').val('');
									$('#txtGBundle').val('');
									$('#txtWeight').val('');
									$('#txtAmount').val('');
									$('#txtGWeight').val('');
									$('#txtDiscp').val('');
									$('#txtDiscount1_tbl').val('');
								}
								else
								{
									$('#txtItemId').removeClass('inputerror');
									response(data);
									$("#imgItemLoader").hide();
								}
							}
						});
					}
					else
					{
						clearItemData();
					}
				},
				renderItem: function (item, search)
				{
					var sea = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
					var re = new RegExp("(" + sea.split(' ').join('|') + ")", "gi");

					var selected = "";
					if((search.toLowerCase() == (item.item_des).toLowerCase() && countItem == 0) || (search.toLowerCase() != (item.item_des).toLowerCase() && countItem == 0))
					{
						selected = "selected";
					}
					countItem++;
					clearItemData();

					return '<div class="autocomplete-suggestion ' + selected + '" data-val="' + search + '" data-item_id="' + item.item_id + '" data-size="' + item.pack + '" data-bid="' + item.bid +
					'" data-uom_item="'+ item.uom + '" data-srate="' + item.srate + '" data-grweight="' + item.grweight + '" data-stqty="' + item.stqty +
					'" data-stweight="' + item.stweight + '" data-length="' + item.length  + '" data-catid="' + item.catid +
					'" data-subcatid="' + item.subcatid + '" data-desc="' + item.item_des + '" data-cost_price="' + item.cost_price + '" data-code="' + item.item_code + '" data-self="' + item.self_freight_rate + '" data-bilty="' + item.bilty_freight_rate + 
					'" data-gari="' + item.gari_freight_rate + '" data-dharry="' + item.srate4 + '" data-mould="' + item.srate3 + '" >' + item.item_des.replace(re, "<b>$1</b>") + '</div>';
				},
				onSelect: function(e, term, item)
				{

					$("#imgItemLoader").hide();
					$("#hfItemId").val(item.data('item_id'));
					$("#hfItemSize").val(item.data('size'));
					$("#hfItemBid").val(item.data('bid'));
					$("#hfItemUom").val(item.data('uom_item'));
					$("#hfItemPrate").val(item.data('cost_price'));
					$("#hfItemGrWeight").val(item.data('grweight'));
					$("#hfItemStQty").val(item.data('stqty'));
					$("#hfItemStWeight").val(item.data('stweight'));
					$("#hfItemLength").val(item.data('length'));
					$("#hfItemCatId").val(item.data('catid'));
					$("#hfItemSubCatId").val(item.data('subcatid'));
					$("#hfItemDesc").val(item.data('desc'));
					$("#hfItemShortCode").val(item.data('code'));
					$("#hfItemUname").val(item.data('uname'));
                    $("#hfItemMouldRate").val(item.data('mould'));
                    $("#hfItemDharyRate").val(item.data('dharry'));


					var freightStatus = $('input[name=inlineRadioOptions]:checked').val();

					if(freightStatus == 'self'){
						$('#txtFRate').val(item.data('self'));	
					}else if(freightStatus == 'bilty'){
						$('#txtFRate').val(item.data('bilty'));
					}else if(freightStatus == 'gari'){
						$('#txtFRate').val(item.data('gari'));
					}

					$("#txtItemId").val(item.data('code'));

					var itemId = item.data('item_id');
					var itemDesc = item.data('desc');
					var pRate = item.data('cost_price');
					var grWeight = item.data('grweight');
					var uomItem = item.data('uom_item');
					var stQty = item.data('stqty');
					var stWeight = item.data('stweight');
					var size = item.data('size');
					var brandId = item.data('bid');
                    var mouldrate = item.data('mould');
                    var dharyrate = item.data('dharry');
                    $('#stqty_lbl').text('Item,     Uom:' + uomItem);
					$('#itemDesc').val(itemDesc);
         			$('#txtPRate').val(parseFloat(pRate).toFixed(2));
					$('#txtGWeight').val(parseFloat(grWeight).toFixed(2));
					$('#txtUom').val(uomItem);
					$('#txtGWeight').trigger('input');
                    $('#txtPRate').val(mouldrate);
                    $('#txtDharyRate').val(dharyrate);
				}
			});
	
			$("form").submit(function() { return false; });
			// when btnRowRemove is clicked
			$('#moulding_table').on('click', '.btnRowRemove', function(e) {
				e.preventDefault();
				var qty = $.trim($(this).closest('tr').find('td.qty').text());
				var amount = $.trim($(this).closest('tr').find('td.amount').text());
				var weight = $.trim($(this).closest('tr').find('td.weight').text());
				var damount = $.trim($(this).closest('tr').find('td.damount').text());
				calculateLowerTotal("-"+qty, '-'+weight, "-"+damount, "-"+amount);
				$(this).closest('tr').remove();
			});
			$('#moulding_table').on('click', '.btnRowEdit', function(e) {
				e.preventDefault();

				// getting values of the cruel row
				var item_id = $.trim($(this).closest('tr').find('td.item_desc').data('item_id'));
				var qty = $.trim($(this).closest('tr').find('td.qty').text());
				var weight = $.trim($(this).closest('tr').find('td.weight').text());
				var rate = $.trim($(this).closest('tr').find('td.rate').text());
				var amount = $.trim($(this).closest('tr').find('td.amount').text());
				var discount = $.trim($(this).closest('tr').find('td.discount').text())
				var damount = $.trim($(this).closest('tr').find('td.damount').text());


				$.ajax({
					type: "POST",
					url: base_url + 'index.php/item/getiteminfobyid',
					data: {
						item_id: item_id
					}
				}).done(function (result) {

					$("#imgItemLoader").hide();
					var item = $.parseJSON(result);

					if (item != false)
					{
						$("#hfItemId").val(item[0].item_id);
						$("#hfItemSize").val(item[0].size);
						$("#hfItemBid").val(item[0].bid);
						$("#hfItemUom").val(item[0].uom);
						$("#hfItemPrate").val(item[0].cost_price);
						$("#hfItemGrWeight").val(item[0].grweight);
						$("#hfItemStQty").val(item[0].stqty);
						$("#hfItemStWeight").val(item[0].stweight);
						$("#hfItemLength").val(item[0].length);
						$("#hfItemCatId").val(item[0].catid);
						$("#hfItemSubCatId").val(item[0].subcatid);
						$("#hfItemDesc").val(item[0].item_des);
						$("#hfItemShortCode").val(item[0].short_code);
						$("#hfItemUname").val(item[0].uname);


						$("#txtItemId").val(item[0].item_code);
						$('#itemDesc').val(item[0].item_des);
						$('#txtUom').val(item[0].uom);
						
						$('#txtGWeight').val(parseFloat(item[0].grweight).toFixed());
						$('#txtQty').val(qty);
						$('#txtPRate').val(rate);
						$('#txtWeight').val(weight);
						$('#txtAmount').val(amount);
						$('#txtDharyRate').val(discount);
						$('#txtDharyAmount').val(damount);

						calculateLowerTotal("-"+qty, '-'+weight, "-"+damount, "-"+amount);
						
						
						$('#stqty_lbl').text('Item,     Uom:' + item[0].uom);
                        // now we have get all the value of the row that is being deleted. so remove that cruel row

                    }
                });
				// now we have get all the value of the row that is being deleted. so remove that cruel row
				$(this).closest('tr').remove();	// yahoo removed
			});

			

			$('#txtDharyDed').on('input', function() {
				calculateLowerTotal(0, 0, 0,0);
			});

			$('#txtMouldDed').on('input', function() {
				calculateLowerTotal(0, 0, 0,0);
			});

			$('#txtDharyBonus').on('input', function() {
				calculateLowerTotal(0, 0, 0,0);
			});
			$('#txtMouldBonus').on('input', function() {
				calculateLowerTotal(0, 0, 0,0);
			});

			$('#txtDharyAmount').on('input', function() {
				calculateLowerTotal(0, 0, 0,0);
			});


			shortcut.add("F10", function() {
    			self.initSave();
			});
			shortcut.add("F1", function() {
				$('a[href="#party-lookup"]').trigger('click');
			});
			shortcut.add("F2", function() {
				$('a[href="#item-lookup"]').trigger('click');
			});
			shortcut.add("F9", function() {
				Print_Voucher();
			});
			shortcut.add("F6", function() {
    			$('#txtVrnoa').focus();
    			// alert('focus');
			});
			shortcut.add("F5", function() {
    			self.resetVoucher();
			});

			shortcut.add("F12", function() {
    			var vrnoa = $('#txtVrnoaHidden').val();
				if (vrnoa !== '') {
					if (confirm('Are you sure to delete this voucher?'))
						deleteVoucher(vrnoa);
				}
			});


			$('#txtVrnoa').on('keypress', function(e) {
				if (e.keyCode === 13) {
					e.preventDefault();
					var vrnoa = $('#txtVrnoa').val();
					if (vrnoa !== '') {
						fetch(vrnoa);
					}
				}
			});

			moulding.fetchRequestedVr();
		},

		// prepares the data to save it into the database
		initSave : function() {
			var saveObj = getSaveObject();
			var error = validateSave();
			if (!error) {
				var rowsCount = $('#moulding_table').find('tbody tr').length;
				if (rowsCount > 0 ) {
					save(saveObj);
				} else {
					alert('No date found to save!');
				}
			} else {
				alert('Please enter into red empty fields..............');
			}
		},
		initSaveAccount : function() {
			var saveObjAccount = getSaveObjectAccount();
			var error = validateSaveAccount();
			if (!error) {
					saveAccount(saveObjAccount);
			} else {
				alert('Correct the errors...');
			}
		},
		initSaveItem : function() {
			var saveObjItem = getSaveObjectItem();
			var error = validateSaveItem();

			if (!error) {
					saveItem(saveObjItem);
			} else {
				alert('Correct the errors...');
			}
		},
		bindModalPartyGrid : function() {
            var dontSort = [];
            $('#party-lookup table thead th').each(function () {
                if ($(this).hasClass('no_sort')) {
                    dontSort.push({ "bSortable": false });
                } else {
                    dontSort.push(null);
                }
            });
            moulding.pdTable = $('#party-lookup table').dataTable({
                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
                "aaSorting": [[0, "asc"]],
                "bPaginate": true,
                "sPaginationType": "full_numbers",
                "bJQueryUI": false,
                "aoColumns": dontSort
            });
            $.extend($.fn.dataTableExt.oStdClasses, {
                "s`": "dataTables_wrapper form-inline"
            });
		},
		fetchRequestedVr : function () {
			var vrnoa = general.getQueryStringVal('vrnoa');
			vrnoa = parseInt( vrnoa );
			$('#txtVrnoa').val(vrnoa);
			$('#txtVrnoaHidden').val(vrnoa);
			if ( !isNaN(vrnoa) ) {
				fetch(vrnoa);
			}else{
				getMaxVrno();
				getMaxVrnoa();
			}
		},
		bindModalItemGrid : function() {
            var dontSort = [];
            $('#item-lookup table thead th').each(function () {
                if ($(this).hasClass('no_sort')) {
                    dontSort.push({ "bSortable": false });
                } else {
                    dontSort.push(null);
                }
            });
            moulding.pdTable = $('#item-lookup table').dataTable({
                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
                "aaSorting": [[0, "asc"]],
                "bPaginate": true,
                "sPaginationType": "full_numbers",
                "bJQueryUI": false,
                "aoColumns": dontSort
            });
            $.extend($.fn.dataTableExt.oStdClasses, {
                "s`": "dataTables_wrapper form-inline"
            });
		},
		// instead of reseting values reload the page because its cruel to write to much code to simply do that
		resetVoucher : function() {
			general.reloadWindow();
		}
	}
};

var moulding = new moulding();
moulding.init();