var Purchase = function() {
	var settings = {
		// basic information section
		switchPreBal : $('#switchPreBal')

	};

	var saveItem = function( item ) {
		$.ajax({
			url : base_url + 'index.php/item/save',
			type : 'POST',
			data : item,
			// processData : false,
			// contentType : false,
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					alert('Item saved successfully.');
					$('#ItemAddModel').modal('hide');
					fetchItems();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var saveAccount = function( accountObj ) {
		$.ajax({
			url : base_url + 'index.php/account/save',
			type : 'POST',
			data : { 'accountDetail' : accountObj },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'false') {
					alert('An internal error occured while saving account. Please try again.');
				} else {
					alert('Account saved successfully.');
					$('#AccountAddModel').modal('hide');
					fetchAccount();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

var fetchAccount = function() {

		$.ajax({
			url : base_url + 'index.php/account/fetchAll',
			type : 'POST',
			data : { 'active' : 1,'typee':'purchase'},
			dataType : 'JSON',
			success : function(data) {
				if (data === 'false') {
					alert('No data found');
				} else {
					populateDataAccount(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var fetchItems = function() {
		$.ajax({
			url : base_url + 'index.php/item/fetchAll',
			type : 'POST',
			data : { 'active' : 1 },
			dataType : 'JSON',
			success : function(data) {
				if (data === 'false') {
					alert('No data found');
				} else {
					populateDataItem(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateDataAccount = function(data) {
		$("#party_dropdown11").empty();
		
		$.each(data, function(index, elem){
			var opt="<option value='"+elem.party_id+"' >" +  elem.name + "</option>";
			$(opt).appendTo('#party_dropdown11');
		});
	}
	var populateDataItem = function(data) {
		$("#itemid_dropdown").empty();
		$("#item_dropdown").empty();

		$.each(data, function(index, elem){
			var opt="<option value='"+elem.item_id+"' data-prate= '"+ elem.cost_price +"' data-uom_item= '"+ elem.uom +"' data-grweight= '"+ elem.grweight +"' >" +  elem.item_des + "</option>";
			 // var = "<option value='" + $item['item_id'] + "' data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>"><?php echo $item['item_des']; ?></option>";
			$(opt).appendTo('#item_dropdown');
			var opt1="<option value='"+elem.item_id+"' data-prate= '"+ elem.cost_price +"' data-uom_item= '"+ elem.uom +"' data-grweight= '"+ elem.grweight +"' >" +  elem.item_id + "</option>";
			 // var = "<option value='" + $item['item_id'] + "' data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>"><?php echo $item['item_des']; ?></option>";
			$(opt1).appendTo('#itemid_dropdown');

		});
	}
	var getSaveObjectAccount = function() {

		var obj = {
			pid : '20000',
			active : '1',
			name : $.trim($('#txtAccountName').val()),
			level3 : $.trim($('#txtLevel3').val()),
			dcno : $('#txtVrnoa').val(),
			etype : 'purchase',
			uid : $.trim($('#uid').val()),
			company_id : $.trim($('#cid').val()),
		};

		return obj;
	}
var getSaveObjectItem = function() {
		
		var itemObj = {
			item_id : 20000,
			active : '1',
			open_date : $.trim($('#current_date').val()),
			catid : $('#category_dropdown').val(),
			subcatid : $.trim($('#subcategory_dropdown').val()),
			bid : $.trim($('#brand_dropdown').val()),
			barcode : $.trim($('#txtBarcode').val()),
			description : $.trim($('#txtItemName').val()),
			item_des : $.trim($('#txtItemName').val()),
			cost_price : $.trim($('#txtPurPrice').val()),
			srate : $.trim($('#txtSalePrice').val()),
			uid : $.trim($('#uid').val()),
			company_id : $.trim($('#cid').val()),
			uom : $.trim($('#uom_dropdown').val()),
		};
		return itemObj;
	}
var populateDataGodowns = function(data) {
        $("#dept_dropdown").empty();
        $.each(data, function(index, elem){
            var opt1="<option value=" + elem.did + ">" +  elem.name + "</option>";
            $(opt1).appendTo('#dept_dropdown');
        });
    }
    var fetchGodowns = function() {
        $.ajax({
            url : base_url + 'index.php/department/fetchAllDepartments',
            type : 'POST',
            dataType : 'JSON',
            success : function(data) {
                if (data === 'false') {
                    alert('No data found');
                } else {
                    populateDataGodowns(data);
                }
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }
    var getSaveObjectGodown = function() {
        var obj = {};
        obj.did = 20000;
        obj.name = $.trim($('#txtNameGodownAdd').val());
        obj.description = $.trim($('.page_title').val());
        return obj;
    }
    var saveGodown = function( department ) {
        $.ajax({
            url : base_url + 'index.php/department/saveDepartment',
            type : 'POST',
            data : { 'department' : department },
            dataType : 'JSON',
            success : function(data) {

                if (data.error === 'false') {
                    alert('An internal error occured while saving department. Please try again.');
                } else {
                    alert('Department saved successfully.');
                    $('#GodownAddModel').modal('hide');
                    fetchGodowns();
                }
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }
    var validateSaveGodown = function() {
        var errorFlag = false;
        var _desc = $.trim($('#txtNameGodownAdd').val());
        $('.inputerror').removeClass('inputerror');
        if ( !_desc ) {
            $('#txtNameGodownAdd').addClass('inputerror');
            errorFlag = true;
        }
        return errorFlag;
    }
	var validateSaveItem = function() {

		var errorFlag = false;
		// var _barcode = $('#txtBarcode').val();
		var _desc = $.trim($('#txtItemName').val());
		var cat = $.trim($('#category_dropdown').val());
		var subcat = $('#subcategory_dropdown').val();
		var brand = $.trim($('#brand_dropdown').val());
		var uom_ = $.trim($('#uom_dropdown').val());

		// remove the error class first
		
		$('.inputerror').removeClass('inputerror');
		if ( !uom_ ) {
			$('#uom_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		if ( _desc === '' || _desc === null ) {
			$('#txtItemName').addClass('inputerror');
			errorFlag = true;
		}
		if ( !cat ) {
			$('#category_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		if ( !subcat ) {
			$('#subcategory_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		if ( !brand ) {
			$('#brand_dropdown').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}
	var validateSaveAccount = function() {

		var errorFlag = false;
		var partyEl = $('#txtAccountName');
		var deptEl = $('#txtLevel3');

		// remove the error class first
		$('.inputerror').removeClass('inputerror');

		if ( !partyEl.val() ) {
			$('#txtAccountName').addClass('inputerror');
			errorFlag = true;
		}
		if ( !deptEl.val() ) {
			deptEl.addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}
	


	var save = function(purchase) {
		general.disableSave();

		$.ajax({
			url : base_url + 'index.php/purchase/save',
			type : 'POST',
			data : { 'stockmain' : JSON.stringify(purchase.stockmain), 'stockdetail' : JSON.stringify(purchase.stockdetail), 'vrnoa' : purchase.vrnoa, 'ledger' : JSON.stringify(purchase.ledger) ,'voucher_type_hidden':$('#voucher_type_hidden').val() , 'vrdate' : $('#vrdate').val()},
			dataType : 'JSON',
			success : function(data) {
				// console.log(data);
				if (data.error === 'date close') {
					alert('Sorry! you can not insert update in close date................');
				}else if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					// alert('Voucher saved successfully.');
					// general.reloadWindow();
					// general.ShowAlert('Save');
					var printConfirmation = confirm('Voucher saved!\nWould you like to print the invoice as well?');
					if (printConfirmation === true) {
						//Print_Voucher(0,'lg','');
						window.open(base_url + 'application/views/reportprints/gatepassinward.php', "Inward Return", 'width=1210, height=842');
						setTimeout(function(){ 
							resetFields();
							getMaxVrnoa();
							getMaxVrno();
						}, 4000);

						//general.reloadWindow();
					} else {
						general.reloadWindow();
					}
				}
				general.enableSave();
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var Print_Voucher = function(hd,prnt,wrate) {
		if ( $('.btnSave').data('printbtn')==0 ){
				alert('Sorry! you have not print rights..........');
		}else{
			var etype=  'purchase';
			var vrnoa = $('#txtVrnoa').val();
			var company_id = $('#cid').val();
			var user = $('#uname').val();
			// var hd = $('#hd').val();
			var pre_bal_print = ($(settings.switchPreBal).bootstrapSwitch('state') === true) ? '1' : '0';
			alert(wrate);
			var url = base_url + 'index.php/doc/Print_Voucher/' + etype + '/' + vrnoa + '/' + company_id + '/' + '-1' + '/' + user + '/' + pre_bal_print + '/' + hd + '/' + prnt + '/' + wrate;
			// var url = base_url + 'index.php/doc/CashVocuherPrintPdf/' + etype + '/' + dcno   + '/' + companyid + '/' + '-1' + '/' + user;
			window.open(url);
		}
	}

	var fetch = function(vrnoa) {

		$.ajax({
			url : base_url + 'index.php/purchase/fetch',
			type : 'POST',
			data : { 'vrnoa' : vrnoa , 'company_id': $('#cid').val()},
			dataType : 'JSON',
			success : function(data) {

				resetFields();
				$('#txtOrderNo').val('');
				if (data === 'false') {
					alert('No data found.');
					getMaxVrnoa();
                    $(".btnPrints").attr("disabled","disabled").next().attr("disabled","disabled")
				} else {
					$(".btnPrints").removeAttr("disabled").next().removeAttr("disabled");

					populateData(data);
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	


	var populateData = function(data) {

		$('#txtVrno').val(data[0]['vrno']);
		$('#txtVrnoHidden').val(data[0]['vrno']);
		$('#txtVrnoaHidden').val(data[0]['vrnoa']);
		$('#txtIdImg').val(data[0]['vrnoa']);
		$('#current_date').datepicker('update', data[0]['vrdate'].substr(0, 10));
		$('#vrdate').datepicker('update', data[0]['vrdate'].substr(0, 10));
		$('#party_dropdown11').select2('val', data[0]['party_id']);
		$('#expense_dropdown').select2('val', data[0]['party_id_co']);

		$('#txtInvNo').val(data[0]['inv_no']);
		$('#due_date').datepicker('update', data[0]['due_date'].substr(0, 10));
		$('#receivers_list').val(data[0]['received_by']);
		$('#transporter_dropdown').select2('val', data[0]['transporter_id']);
		$('#txtRemarks').val(data[0]['remarks']);
		$('#txtNetAmount').val(data[0]['namount']);
		$('#txtOrderNo').val(data[0]['order_no']);
		
		$('#txtDiscount').val(data[0]['discp']);
		$('#txtExpense').val(data[0]['exppercent']);
		$('#txtExpAmount').val(data[0]['expense']);
		$('#txtTax').val(data[0]['taxpercent']);
		$('#txtTaxAmount').val(data[0]['tax']);
		$('#txtDiscAmount').val(data[0]['discount']);
		$('#user_dropdown').val(data[0]['uid']);
		$('#txtPaid').val(data[0]['paid']);
		$('#txtFRate').val(data[0]['f_rate']);
		$('#txtFAmount').val(data[0]['f_amount']);
		$('#txtLRate').val(data[0]['l_rate']);
		$('#txtLAmount').val(data[0]['l_amount']);
		$('#txtULRate').val(data[0]['ul_rate']);
		$('#txtULAmount').val(data[0]['ul_amount']);
		$('#txtScaleCharges').val(data[0]['scale_charges']);
		$('#msgStatus').val(data[0]['msg_status']);

		//$('#dept_dropdown').select2('val', data[0]['godown_id']);
		$('#voucher_type_hidden').val('edit');		
		$('#user_dropdown').val(data[0]['uid']);
		$.each(data, function(index, elem) {
            elem.s_amount=parseFloat(elem.s_amount).toFixed(2)
			appendToTable('', elem.item_name, elem.item_id, elem.godownname, elem.godownid,elem.s_qty, elem.s_rate, elem.s_amount, elem.weight,elem.item_code,elem.uitem_des,elem.inv_no,elem.bal_qty_weight,elem.bal_qty_weight_hide);
			calculateLowerTotal(elem.s_qty, elem.s_amount, elem.weight);
		});
	}

	// gets the max id of the voucher
	var getMaxVrno = function() {

		$.ajax({

			url : base_url + 'index.php/purchase/getMaxVrno',
			type : 'POST',
			data : {'company_id': $('#cid').val()},
			dataType : 'JSON',
			success : function(data) {

				$('#txtVrno').val(data.vrno);
				$('#txtMaxVrnoHidden').val(data.vrno);
				$('#txtVrnoHidden').val(data.vrno);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var getAllItems = function() {
		alert('');
		// $.ajax({

		// 	url : base_url + 'index.php/item/getAllItems',
		// 	type : 'POST',
		// 	data : {},
		// 	dataType : 'JSON',
		// 	success : function(data) {
		// 		appendItemOption(data);
		// 	}, error : function(xhr, status, error) {
		// 		console.log(xhr.responseText);
		// 	}
		// });
		
	}

	var appendItemOption = function(data){
  	var html='';
  	var html1='';
		$.each(data, function(index, elem) {
			html += "<option value='"+elem.item_id+"' data-uom_item='"+elem.uom+"' data-prate='"+elem.cost_price+"' data-grweight='"+elem.grweight+"' data-stqty='"+ elem.stqty+"' data-stweight='"+elem.stweight+"' data-item_code='"+elem.item_code+"'>"+elem.item_id+"</option>";
			html1 += "<option value='"+elem.item_id+"' data-uom_item='"+elem.uom+"' data-prate='"+elem.cost_price+"' data-grweight='"+elem.grweight+"' data-stqty='"+ elem.stqty+"' data-stweight='"+elem.stweight+"' data-item_code='"+elem.item_code+"'>"+elem.item_des+"</option>";
		});
		$("#itemid_dropdown").append(html);
		$("#item_dropdown").append(html1);
		//$('.selectpicker').selectpicker('refresh');
	}

	var getMaxVrnoa = function() {

		$.ajax({

			url : base_url + 'index.php/purchase/getMaxVrnoa',
			type : 'POST',
			data : {'company_id': $('#cid').val()},
			dataType : 'JSON',
			success : function(data) {

				$('#txtVrnoa').val(data);
				$('#txtMaxVrnoaHidden').val(data);
				$('#txtVrnoaHidden').val(data);
				$('#txtIdImg').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var validateSingleProductAdd = function() {


		var errorFlag = false;
		var itemEl = $('#hfItemId');
		var qtyEl = $('#txtQty');
		var rateEl = $('#txtPRate');
		var deptEl = $('#dept_dropdown');

		// remove the error class first
		$('.inputerror').removeClass('inputerror');
		if ( !deptEl.val() ) {
			deptEl.addClass('inputerror');
			errorFlag = true;
		}
		if ( !itemEl.val() ) {
			$('#txtItemId').addClass('inputerror');
			errorFlag = true;
		}
		if ( !qtyEl.val() ) {
			qtyEl.addClass('inputerror');
			errorFlag = true;
		}
		if ( !rateEl.val() ) {
			rateEl.addClass('inputerror');
			errorFlag = true;
		}

		var contractNo = $('#contractNo').val();
		if(contractNo != '' && contractNo != null){
			var _uom= $('#txtUom').val().toUpperCase();
			kg=-1;
			gram=-1;
			var kg = _uom.search("KG");
			var gram = _uom.search("GRAM");
			if (kg ==-1 && gram ==-1 ){

				var balQty = getNumVal($('#txtHideBalWQ'));
				var qty = getNumVal($('#txtQty'));
				if(qty > balQty){
					$('#txtQty').addClass('inputerror');
					errorFlag = true;
				}
			}else{

				var balWeight = getNumVal($('#txtHideBalWQ'));
				var weight = getNumVal($('#txtWeight'));
				if(weight > balWeight){
					$('#txtWeight').addClass('inputerror');
					errorFlag = true;
				}
			}
		}

		return errorFlag;
	}

	var appendToTable = function(srno, item_desc, item_id, godown , godown_id, qty, rate, amount, weight,item_code,uitem_des,contract,balQtyWeight,hideBalQtyWeight) {
        amount=parseFloat(amount).toFixed(2)
		var srno = $('#purchase_table tbody tr').length + 1;
		var row = 	"<tr>" +
						"<td class='srno numeric' data-title='Sr#' > "+ srno +"</td>" +
						"<td class='contract numeric' data-title='Contract' > "+ contract +"</td>" +
				 		"<td class='item_desc' data-title='Description' data-item_id='"+ item_id +"' data-uitem_des='"+ uitem_des +"' data-item_code='"+ item_code +"'> "+ item_desc +"</td>" +
				 		"<td class='godown' data-title='Warehouse' data-godown_id='"+ godown_id +"' data-godown_id_uname='"+ godown_id +"'> "+ godown +"</td>" +
				 		"<td class='qty numeric' data-title='Qty'>  "+ qty +"</td>" +
					 	"<td class='weight numeric' data-title='Weigh' > "+ weight +"</td>" +
					 	"<td class='balqtyweight numeric' data-title='Weigh' > "+ balQtyWeight +"</td>" +
					 	"<td class='hidebalqtyweight hide numeric' data-title='Weigh' > "+ hideBalQtyWeight +"</td>" +
					 	"<td class='rate numeric' data-title='Rate'> "+ rate +"</td>" +
					 	"<td class='amount numeric' data-title='Amount' > "+ amount +"</td>" +
					 	"<td><a href='' class='btn btn-primary btnRowEdit'><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td>" +
				 	"</tr>";
		$(row).appendTo('#purchase_table');
	}

	var getPartyId = function(partyName) {
		var pid = "";
		$('#party_dropdown11 option').each(function() { if ($(this).text().trim().toLowerCase() == partyName) pid = $(this).val();  });
		return pid;
	}



	var getSaveObject = function() {

		var ledgers = [];
		var stockmain = {};
		var stockdetail = [];

		stockmain.vrno = $('#txtVrnoHidden').val();
		stockmain.vrnoa = $('#txtVrnoaHidden').val();
		stockmain.vrdate = $('#current_date').val();
		stockmain.party_id = $('#party_dropdown11').val();
		stockmain.bilty_no = $('#txtInvNo').val();
		stockmain.bilty_date = $('#due_date').val();
		stockmain.received_by = $('#receivers_list').val();
		stockmain.transporter_id = ($('#transporter_dropdown').val() && $('#transporter_dropdown').val() != '') ? $('#transporter_dropdown').val() : '';
		stockmain.remarks = $('#txtRemarks').val();
		stockmain.etype = 'purchase';
		stockmain.namount = $('#txtNetAmount').val();
		stockmain.order_vrno = $('#txtOrderNo').val();
		stockmain.discp = $('#txtDiscount').val();
		stockmain.discount = $('#txtDiscAmount').val();
		stockmain.expense =$('#txtExpAmount').val();
		stockmain.exppercent = $('#txtExpense').val();
		stockmain.tax = $('#txtTaxAmount').val();
		stockmain.taxpercent = $('#txtTax').val();
		stockmain.paid = $('#txtPaid').val();
		stockmain.f_rate = $('#txtFRate').val();
		stockmain.f_amount = $('#txtFAmount').val();
		stockmain.l_rate = $('#txtLRate').val();
		stockmain.l_amount = $('#txtLAmount').val();
		stockmain.ul_rate = $('#txtULRate').val();
		stockmain.ul_amount = $('#txtULAmount').val();
		stockmain.scale_charges = $('#txtScaleCharges').val();
		stockmain.msg_status = $('#msgStatus').val();
		stockmain.party_id_co = getNumVal($('#expense_dropdown'));

		stockmain.uid = $('#uid').val();
		stockmain.company_id = $('#cid').val();


		$('#purchase_table').find('tbody tr').each(function( index, elem ) {
			var sd = {};
			sd.stid = '';
			sd.item_id = $.trim($(elem).find('td.item_desc').data('item_id'));
			sd.godown_id = $.trim($(elem).find('td.godown').data('godown_id'));
			sd.qty = $.trim($(elem).find('td.qty').text());
			sd.weight = $.trim($(elem).find('td.weight').text());
			sd.bal_qty_weight = $.trim($(elem).find('td.balqtyweight').text());
			sd.bal_qty_weight_hide = $.trim($(elem).find('td.hidebalqtyweight').text());
			sd.rate = $.trim($(elem).find('td.rate').text());
			sd.amount = $.trim($(elem).find('td.amount').text());
			sd.netamount = $.trim($(elem).find('td.amount').text());
			sd.inv_no = $.trim($(elem).find('td.contract').text());
			stockdetail.push(sd);
		});

		///////////////////////////////////////////////////////////////
		//// for over all voucher
		///////////////////////////////////////////////////////////////
		
		var pledger = {};
		pledger.pledid = '';
		pledger.pid = $('#party_dropdown11').val();
		pledger.description =  $('#txtRemarks').val();
		pledger.remarks =  $('#txtRemarks').val();
		pledger.date = $('#current_date').val();
		pledger.debit = 0;
		pledger.credit = $('#txtNetAmount').val();
		pledger.dcno = $('#txtVrnoaHidden').val();
		pledger.invoice = $('#txtVrnoaHidden').val();
		pledger.etype = 'purchase';
		pledger.pid_key = $('#purchaseid').val();
		pledger.uid = $('#uid').val();
		pledger.company_id = $('#cid').val();
		pledger.isFinal = 0;	
		ledgers.push(pledger);

		var pledger = {};
		pledger.pledid = '';
		pledger.pid = $('#purchaseid').val();
		pledger.description = $('#party_dropdown11').find('option:selected').text();
		pledger.remarks =  $('#txtRemarks').val();
		pledger.date = $('#current_date').val();
		pledger.debit = $('#txtTotalAmount').val();
		pledger.credit = 0;
		pledger.dcno = $('#txtVrnoaHidden').val();
		pledger.invoice = $('#txtInvNo').val();
		pledger.etype = 'purchase';
		pledger.pid_key = $('#party_dropdown11').val();
		pledger.uid = $('#uid').val();
		pledger.company_id = $('#cid').val();	
		pledger.isFinal = 0;
		ledgers.push(pledger);

		///////////////////////////////////////////////////////////////
		//// for Discount
		///////////////////////////////////////////////////////////////
		if ($('#txtDiscAmount').val() != 0 ) {
			pledger = undefined;
			var pledger = {};
			pledger.etype = 'purchase';
			pledger.description = $('#party_dropdown11 option:selected').text() + '.';
			pledger.remarks =  $('#txtRemarks').val();
			// pledger.description = 'Purchase Head';
			pledger.dcno = $('#txtVrnoaHidden').val();
			pledger.invoice = $('#txtVrnoaHidden').val();
			pledger.pid = $('#discountid').val();
			pledger.date = $('#current_date').val();
			pledger.debit = 0;
			pledger.credit = $('#txtDiscAmount').val();
			pledger.isFinal = 0;
			pledger.uid = $('#uid').val();
			pledger.company_id = $('#cid').val();	
			pledger.pid_key = $('#party_dropdown11').val();								

			ledgers.push(pledger);
		}		
		

		if ($('#txtTaxAmount').val() != 0 ) {
			pledger = undefined;
			var pledger = {};
			pledger.etype = 'purchase';
			pledger.description = $('#party_dropdown11 option:selected').text() + '.';
			pledger.remarks =  $('#txtRemarks').val();
			// pledger.description = 'Purchase Head';
			pledger.dcno = $('#txtVrnoaHidden').val();
			pledger.invoice = $('#txtVrnoaHidden').val();
			pledger.pid = $('#taxid').val();
			pledger.date = $('#current_date').val();
			pledger.debit = $('#txtTaxAmount').val();
			pledger.credit = 0;
			pledger.isFinal = 0;
			pledger.uid = $('#uid').val();
			pledger.company_id = $('#cid').val();	
			pledger.pid_key = $('#party_dropdown11').val();
			ledgers.push(pledger);
		}

		var expenseAmount = 0;
		expenseAmount =  (getNumVal($('#txtFAmount')) + getNumVal($('#txtLAmount'))+ getNumVal($('#txtULAmount')) + getNumVal($('#txtScaleCharges'))).toFixed(0);

		if (expenseAmount != 0 && $('#expense_dropdown').val() != null) {
			pledger = undefined;
			var pledger = {};
			pledger.etype = 'purchase';
			pledger.description = $('#party_dropdown11 option:selected').text() + '.';
			pledger.remarks =  $('#txtRemarks').val();
			// pledger.description = 'Purchase Head';
			pledger.dcno = $('#txtVrnoaHidden').val();
			pledger.invoice = $('#txtVrnoaHidden').val();
			pledger.pid = $('#expense_dropdown').val();
			pledger.date = $('#current_date').val();
			pledger.credit = expenseAmount;
			pledger.debit = 0;
			pledger.isFinal = 0;
			pledger.uid = $('#uid').val();
			pledger.company_id = $('#cid').val();	
			pledger.pid_key = $('#party_dropdown11').val();
			ledgers.push(pledger);
		}
		if ($('#txtPaid').val() != 0 ) {
			pledger = undefined;
			var pledger = {};
			pledger.etype = 'purchase';
			pledger.description = $('#party_dropdown11 option:selected').text() + '.';
			pledger.remarks =  $('#txtRemarks').val();
			// pledger.description = 'Purchase Head';
			pledger.dcno = $('#txtVrnoaHidden').val();
			pledger.invoice = $('#txtVrnoaHidden').val();
			pledger.pid = $('#cashid').val();
			pledger.date = $('#current_date').val();
			pledger.debit = 0;
			pledger.credit = $('#txtPaid').val();
			pledger.isFinal = 0;
			pledger.uid = $('#uid').val();
			pledger.company_id = $('#cid').val();	
			pledger.pid_key = $('#party_dropdown11').val();
			ledgers.push(pledger);

			pledger = undefined;
			var pledger = {};
			pledger.etype = 'purchase';
			pledger.description =  'Cash Paid';
			pledger.remarks =  $('#txtRemarks').val();
			// pledger.description = 'Purchase Head';
			pledger.dcno = $('#txtVrnoaHidden').val();
			pledger.invoice = $('#txtVrnoaHidden').val();
			pledger.pid = $('#party_dropdown11').val();
			pledger.date = $('#current_date').val();
			pledger.debit = $('#txtPaid').val();
			pledger.credit = 0;
			pledger.isFinal = 0;
			pledger.uid = $('#uid').val();
			pledger.company_id = $('#cid').val();	
			pledger.pid_key = $('#cashid').val();
			ledgers.push(pledger);

		}
		var data = {};
		data.stockmain = stockmain;
		data.stockdetail = stockdetail;
		data.ledger = ledgers;
		data.vrnoa = $('#txtVrnoaHidden').val();

		return data;
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		var partyEl = $('#party_dropdown11');
		var expenseEl = $('#expense_dropdown');

		//var deptEl = $('#dept_dropdown');
		var transportEl = $('#transporter_dropdown');
		var expenseAmount = 0;
		expenseAmount =  (getNumVal($('#txtFAmount')) + getNumVal($('#txtLAmount'))+ getNumVal($('#txtULAmount')) + getNumVal($('#txtScaleCharges'))).toFixed(0);

		
		// remove the error class first
		$('.inputerror').removeClass('inputerror');

		// if ( !deptEl.val() ) {
		// 	deptEl.addClass('inputerror');
		// 	errorFlag = true;
		// }
		if ( !partyEl.val() ) {
			
			$('#party_dropdown11').addClass('inputerror');
			errorFlag = true;
		}
		//alert(!expenseEl);
		if ( parseFloat(expenseAmount) != 0  && !expenseEl) {
			
			$('#expense_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		/*if ( !transportEl.val() ) {
			
			$('#transporter_dropdown').addClass('inputerror');
			errorFlag = true;
		}*/
		

		return errorFlag;
	}
	

	var deleteVoucher = function(vrnoa) {

		$.ajax({
			url : base_url + 'index.php/purchase/delete',
			type : 'POST',
			data : {'chk_date' : $('#current_date').val(), 'vrdate' : $('#vrdate').val(), 'vrnoa' : vrnoa , 'etype':'purchase','company_id':$('#cid').val() },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'date close') {
					alert('Sorry! you can not delete in close date................');
				}else if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	///////////////////////////////////////////////////////////////
	/// calculations related to the overall voucher
	////////////////////////////////////////////////////////////////
	var calculateLowerTotal = function(qty, amount, weight) {

		var _qty = getNumVal($('#txtTotalQty'));
		var _weight = getNumVal($('#txtTotalWeight'));
		var _amnt = getNumVal($('#txtTotalAmount'));

		var _discp = getNumVal($('#txtDiscount'));
		var _disc = getNumVal($('#txtDiscAmount'));
		var _tax = getNumVal($('#txtTax'));
		var _taxamount = getNumVal($('#txtTaxAmount'));
		var _expense = getNumVal($('#txtExpAmount'));
		var _exppercent = getNumVal($('#txtExpense'));
		var fAmount = getNumVal($('#txtFAmount'));
		var lAmount = getNumVal($('#txtLAmount'));
		var uLAmount = getNumVal($('#txtULAmount'));
		var scaleCharges = getNumVal($('#txtScaleCharges'));


		var tempQty = parseFloat(_qty) + parseFloat(qty);
		$('#txtTotalQty').val(tempQty);
		var tempAmnt = parseFloat(_amnt) + parseFloat(amount);
		$('#txtTotalAmount').val(tempAmnt);

		var totalWeight = parseFloat(parseFloat(_weight) + parseFloat(weight)).toFixed(2);
		$('#txtTotalWeight').val(totalWeight);

		var net = parseFloat(tempAmnt) - parseFloat(_disc) + parseFloat(_taxamount) + parseFloat(_expense) + parseFloat(fAmount) + parseFloat(lAmount) + parseFloat(uLAmount) + parseFloat(scaleCharges);
		$('#txtNetAmount').val(net);
	}

	var getNumVal = function(el){
		return isNaN(parseFloat(el.val())) ? 0 : parseFloat(el.val());
	}

	///////////////////////////////////////////////////////////////
	/// calculations related to the single product calculation
	///////////////////////////////////////////////////////////////
	var calculateUpperSum = function() {

		var _qty = getNumVal($('#txtQty'));
		var _amnt = getNumVal($('#txtAmount'));
		var _net = getNumVal($('#txtNet'));
		var _prate = getNumVal($('#txtPRate'));
		var _gw = getNumVal($('#txtGWeight'));
		var _weight=getNumVal($('#txtWeight'));
		var _uom=$('#txtUom').val().toUpperCase();
		// alert('uom_item ' + _uom);
		kg=-1;
		gram=-1;
		var kg = _uom.search("KG");
		var gram = _uom.search("GRAM");
		if (kg ==-1 && gram ==-1 ){
			var _tempAmnt = parseFloat(_qty) * parseFloat(_prate);			
		}else{
			var _tempAmnt = parseFloat(_weight) * parseFloat(_prate);			
		}
		
		//$('#txtWeight').val(parseFloat(_gw) * parseFloat(_qty));
		$('#txtAmount').val(parseFloat(_tempAmnt).toFixed(2));
	}

	var fetchThroughPO = function(po) {

		$.ajax({

			url : base_url + 'index.php/purchaseorder/fetchthroughPO',
			type : 'POST',
			data : { 'vrnoa' : po , 'company_id': $('#cid').val()},
			dataType : 'JSON',
			success : function(data) {

				resetFields();
				if (data === 'false') {
					alert('No data found.');
				} else {
					populatePOData(data);
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populatePOData = function(data) {

		$('#current_date').datepicker('update', data[0]['vrdate'].substr(0, 10));
		$('#party_dropdown11').select2('val', data[0]['party_id']);
		$('#txtInvNo').val(data[0]['inv_no']);
		// $('#due_date').datepicker('update', data[0]['due_date'].substr(0, 10));
		$('#receivers_list').val(data[0]['received_by']);
		$('#transporter_dropdown').select2('val', data[0]['transporter_id']);
		$('#txtRemarks').val(data[0]['remarks']);
		$('#txtNetAmount').val(data[0]['namount']);
		// $('#txtOrderNo').val(data[0]['ordno']);
		
		$('#txtDiscount').val(data[0]['discp']);
		$('#txtExpense').val(data[0]['exppercent']);
		$('#txtExpAmount').val(data[0]['expense']);
		$('#txtTax').val(data[0]['taxpercent']);
		$('#txtTaxAmount').val(data[0]['tax']);
		$('#txtDiscAmount').val(data[0]['discount']);
		$('#user_dropdown').val(data[0]['uid']);
		$('#txtPaid').val(data[0]['paid']);

		$('#dept_dropdown').select2('val', data[0]['godown_id']);
		$('#voucher_type_hidden').val('edit');		
		$('#user_dropdown').val(data[0]['uid']);
		$.each(data, function(index, elem) {
			appendToTable('', elem.item_name, elem.item_id, elem.qty, elem.rate, elem.amount, elem.weight,elem.item_code,elem.uitem_des);
			calculateLowerTotal(elem.qty, elem.amount, elem.weight);
		});
	}

	var resetFields = function() {

		$('#current_date').datepicker('update', new Date());
		$('#party_dropdown11').select2('val', '');
		$('#txtInvNo').val('');
		$('#due_date').datepicker('update', new Date());
		$('#receivers_list').val('');
		$('#transporter_dropdown').select2('val', '');
		$('#txtRemarks').val('');
		$('#txtNetAmount').val('');		
		$('#txtDiscount').val('');
		// $('#txtIdImg').val('');
		$(".dz-preview").empty();
		$('#txtExpense').val('');
		$('#txtExpAmount').val('');
		$('#txtTax').val('');
		$('#txtTaxAmount').val('');
		$('#txtDiscAmount').val('');
		$('#txtPaid').val('');

		$('#txtTotalAmount').val('');
		$('#txtTotalQty').val('');
		$('#txtTotalWeight').val('');
		$('#dept_dropdown').select2('val', '');
		$('#voucher_type_hidden').val('new');
		$('#purchase_table tbody tr').remove();
	}

    var paging = function (total)
	{
    	$("#paging").smartpaginator({
        	totalrecords: parseInt(total),
        	recordsperpage: 10,
        	datacontainer: 'tbody',
        	dataelement: 'tr',
        	theme: 'custom',
        	onchange: onChange
    	});
	}


	var last_stockLocatons = function(item_id) {
		$.ajax({

			url : base_url + 'index.php/saleorder/last_stocklocatons',
			type : 'POST',
			data : { 'item_id' : item_id , 'company_id': $('#cid').val(),'etype':'sale'},
			dataType : 'JSON',
			
			success : function(data) {
				console.log(data);

				$('#laststockLocation_table tbody').html('');
				var total_qty = 0;
				if(data && data != 'false'){

					$.each(data, function(index, elem) {
						total_qty += parseInt(elem.qty);
						appendTo_last_stockLocatons(elem.location, elem.qty);
					});
				}
				
				last_stockLocatons_total(total_qty);

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}    

	var appendTo_last_stockLocatons = function(location, qty) {

		var row = 	"<tr>" +
						"<td class='location numeric' data-title='location' > "+ location +"</td>" +
					 	"<td class='qty numeric' data-title='qty'> "+ qty +"</td>" +
				 	"</tr>";
		
			$(row).appendTo('#laststockLocation_table tbody');
		
	}

	var last_stockLocatons_total = function(total_qty){
		var row = "<tr>" +
					"<td class='location numeric' data-title='location' >Total</td>" +
				 	"<td class='qty numeric' data-title='qty'> "+ total_qty +"</td>" +
				"</tr>";
		$(row).appendTo('#laststockLocation_table tbody');
	}




    var onChange = function (newPageValue)
    {
        $.ajax({
            type: "POST",
            url: base_url + 'index.php/item/paging',
            data: {
                page_number: newPageValue,
                search: $.trim($("#txtItemSearch").val())
            }
        }).done(function (result) {

            var total = "";
            var jsonR = JSON.stringify($.parseJSON(result)).replace(/\\n/g, "\\n")
                                        .replace(/\\'/g, "\\'")
                                        .replace(/\\"/g, '\\"')
                                        .replace(/\\&/g, "\\&")
                                        .replace(/\\r/g, "\\r")
                                        .replace(/\\t/g, "\\t")
                                        .replace(/\\b/g, "\\b")
                                        .replace(/\\f/g, "\\f");
            var json = $.parseJSON(jsonR);
            total = json.total;

            if (json.records != false) {
                var html = "";
                $.each(json.records, function (index, value) {
                    html += "<tr id='" + value.item_id + "'>";
                    html += "<td>"+value.item_id+"<input type='hidden' name='hfModalitemId' value='" + value.item_id + "'></td>";
                    html += "<td class='tdItemCode'><b>" + value.item_code + "<b></td>";
                    html += "<td class='tdDescription'><b>" + value.item_des + "</b></td>";
                    html += "<td class='tdItemUom'>" + value.uom + "</td>";
                    html += "<td class='tdPRate hide'>" + value.cost_price + "</td>";
                    html += "<td class='tdGrWeight hide'>" + value.grweight + "</td>";
                    html += "<td class='tdStQty'><b>" + value.stqty + "</b></td>";
                    html += "<td class='tdStWeight hide'>" + value.stweight + "</td>";
                    html += "<td class='tdUItem hide'>" + value.uitem_des + "</td>";
                    html += "<td><a href='#' data-dismiss='modal' class='btn btn-primary populateItem'><i class='fa fa-search'></i></a></td>"
                    html += "</tr>";
                });

                $("#divNoRecord").removeClass('divNoRecord');
                $("#divNoRecord").html('');
                $("#tbItems > tbody").html('');
                $("#tbItems > tbody").append(html);

                $('.modal-lookup .populateItem').on('click', function()
                {
                    var itemId = $(this).closest('tr').find('input[name=hfModalitemId]').val();
                    var itemDescription = $(this).closest('tr').find('.tdDescription').text();
                    var uom = $(this).closest('tr').find('.tdItemUom').text();
                    var pRate = $(this).closest('tr').find('.tdPRate').text();
                    var grWeight = $(this).closest('tr').find('.tdGrWeight').text();
                    var stQty = $(this).closest('tr').find('.tdStQty').text();
                    var stWeight = $(this).closest('tr').find('.tdStWeight').text();
                    var uItemName = $(this).closest('tr').find('.tdUItem').text();
                    var itemCode = $(this).closest('tr').find('.tdItemCode').text();


                    var itemCodeOption = "<option value='" + itemId + "' data-uom_item='" + uom + "' data-prate='" + pRate + "' data-grweight='" + grWeight + "' data-stqty='" + stQty + "' data-stweight='" + stWeight + "' data-uitem_des='" + uItemName + "' >" + itemCode + "</option>";
                    var itemOption = "<option value='" + itemId + "'  data-uom_item='" + uom + "' data-prate='" + pRate + "' data-grweight='" + grWeight + "' data-stqty='" + stQty + "' data-stweight='" + stWeight + "'  data-uitem_des='" + uItemName + "' >" + itemDescription + "</option>";
                    $('#itemid_dropdown').append(itemCodeOption);
                    $('#item_dropdown').append(itemOption);

                    $('#itemid_dropdown').val(itemId).trigger('change');
                    $('#item_dropdown').val(itemId).trigger('change');

                    $('#txtPRate').val(parseFloat(pRate).toFixed(2));

                    $('#txtQty').focus();
                });

                $("#paging").show();
                $("#tbItems").show();
                if (newPageValue == "1") {
                    paging(total);
                }
            }
            else {
                $("#tbItems > tbody").html('');
                $("#divNoRecord").show();
                $("#divNoRecord").addClass('divNoRecord');
                $("#paging").hide();
                $("#divNoRecord").html('No Record Found');
            }
        });
    }

    var getItemInfo = function(itemId)
    {
        $.ajax({
            type: "POST",
            url: base_url + 'index.php/item/getiteminfo',
            data: {
                item_id: itemId
            }
        }).done(function (result) {

            var jsonR = JSON.stringify($.parseJSON(result)).replace(/\\n/g, "\\n")
                                    .replace(/\\'/g, "\\'")
                                    .replace(/\\"/g, '\\"')
                                    .replace(/\\&/g, "\\&")
                                    .replace(/\\r/g, "\\r")
                                    .replace(/\\t/g, "\\t")
                                    .replace(/\\b/g, "\\b")
                                    .replace(/\\f/g, "\\f");
            var json = $.parseJSON(jsonR);

            if (json.records != false)
            {
                $.each(json.records, function (index, value) {
                    var itemCodeOption = "<option value='" + value.item_id + "' data-uom_item='" + value.uom + "' data-prate='" + value.cost_price + "' data-grweight='" + value.grweight + "' data-stqty='" + value.stqty + "' data-stweight='" + value.stweight + "' >" + value.item_code + "</option>";
                    var itemOption = "<option value='" + value.item_id + "'  data-uom_item='" + value.uom + "' data-prate='" + value.cost_price + "' data-grweight='" + value.grweight + "' data-stqty='" + value.stqty + "' data-stweight='" + value.stweight + "' >" + value.item_des + "</option>";
                    $('#itemid_dropdown').append(itemCodeOption);
                    $('#item_dropdown').append(itemOption);

                    $('#itemid_dropdown').select2('val', itemId);
                    $('#item_dropdown').select2('val', itemId);
                    $('#txtUom').val(value.uom);

                    var grweight = $('#item_dropdown').find('option:selected').data('grweight');
                    // var prate = $('#item_dropdown').find('option:selected').data('prate');
                    $('#txtGWeight').val(parseFloat(grweight).toFixed());
                    //$('#txtPRate').val(parseFloat(prate).toFixed(2));
                });
            }
        });
    }

    var fetchRunningTotal = function(_pid , _to) {
		$.ajax({
			url : base_url + 'index.php/account/fetchRunningTotal',
			type : 'POST',
			data : {'to': _to, 'pid' : _pid},
			dataType : 'JSON',
			success : function(data) {
				var opbal=data[0]['RTotal'];

				$('#balance_lbl').text('Party Name,     Balance: '+parseFloat(opbal).toFixed(2));
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var fetchPreviousBalance = function() {

		$.ajax({
			url : base_url + 'index.php/account/fetchPreviousBalance',
			type : 'POST',
			data : { 'vrnoa' : $('#txtVrnoa').val() , 'etype': 'purchase' , 'party_id' : $('#party_dropdown11').val(), 'vrdate' : $('#current_date').val() },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found.');
				} else {
					$('#hfPreviousBalnce').val(data);
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var clearItemData = function (){
		$("#hfItemId").val("");
		$("#hfItemSize").val("");
		$("#hfItemBid").val("");
		$("#hfItemUom").val("");
		$("#hfItemPrate").val("");
		$("#hfItemGrWeight").val("");
		$("#hfItemStQty").val("");
		$("#hfItemStWeight").val("");
		$("#hfItemLength").val("");
		$("#hfItemCatId").val("");
		$("#hfItemSubCatId").val("");
		$("#hfItemDesc").val("");
		$("#hfItemShortCode").val("");

         //$("#txtItemId").val("");
     }


	fetchContract = function(vrnoa) {

		$.ajax({

			url : base_url + 'index.php/purchase/fetchRemaining',
			type : 'POST',
			data : { 'vrnoa' : vrnoa , 'company_id': $('#cid').val()},
			dataType : 'JSON',
			success : function(data) {

				//resetFields();
				$('#contractNo').val(vrnoa);
				if (data === 'false') {
					alert('No data found.');
				} else {
					populateDataPO(data);
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateDataPO = function(data) {
		
		var _uom= data[0]['uom'].toUpperCase();
		kg=-1;
		gram=-1;
		var kg = _uom.search("KG");
		var gram = _uom.search("GRAM");
		if (kg ==-1 && gram ==-1 ){

			if(data[0]['balance_qty']  > 0.00 ){
				getItemInfo(data[0]['item_id']);
				$('#txtPRate').val(data[0]['rate']);
				$('#txtBalWQ').val(data[0]['balance_qty']);		
				$('#txtHideBalWQ').val(data[0]['balance_qty']);	
				$('#party_dropdown11').select2('val',data[0]['party_id']);	
			}else{
				alert('Contract already complete');
			}
		}else{

			if(data[0]['balance_weight']  > 0.00 ){
				getItemInfo(data[0]['item_id']);
				$('#txtPRate').val(data[0]['rate']);
				$('#txtBalWQ').val(data[0]['balance_weight']);		
				$('#txtHideBalWQ').val(data[0]['balance_weight']);	
				$('#party_dropdown11').select2('val',data[0]['party_id']);	
			}else{
				alert('Contract already complete');
			}
		}
		
		
		// $('#party_dropdown').select2('val', data[0]['party_id']);
		// $('#txtInvNo').val(data[0]['bilty_no']);
		// $('#txtVehicle').val(data[0]['prepared_by']);
		// $('#rgp_dropdown').val(data[0]['etype2']);
		
		// $('#txtRemarks').val(data[0]['remarks']);
		// $('#txtNetAmount').val(data[0]['namount']);
		// $('#txtOrderNo').val(data[0]['workorderno']);
		
		// $('#txtDiscount').val(data[0]['discp']);
		// $('#txtExpense').val(data[0]['exppercent']);
		// $('#txtExpAmount').val(data[0]['expense']);
		// $('#txtTax').val(data[0]['taxpercent']);
		// $('#txtTaxAmount').val(data[0]['tax']);
		// $('#txtDiscAmount').val(data[0]['discount']);
		// $('#user_dropdown').val(data[0]['uid']);
		// // $('#txtPaid').val(data[0]['paid']);
		// $('#dept_dropdown').select2('val', data[0]['godown_id']);
		// $('#voucher_type_hidden').val('new');		
		// $('#user_dropdown').val(data[0]['uid']);

		// $.each(data, function(index, elem) {
		// 	//appendToTable('', elem.item_name, elem.item_id, elem.godownname, elem.godownid,elem.s_qty, elem.s_rate, elem.s_amount, elem.weight,elem.item_code,elem.uitem_des,elem.inv_no);
		// 	appendToTable('', elem.item_name, elem.item_id,'','', elem.st_qty, elem.s_rate,elem.s_amount ,elem.st_weight, elem.item_code,elem.uitem_des,elem.vrnoa);
			
		// });
	}

	var fetchPartyPurRate = function(itemId, pRate){

		if($('#party_dropdown11').val()){

			$.ajax({

				url : base_url + 'index.php/updaterate/fetchPartyPurRate',
				type : 'POST',
				data : { item_id: itemId, party_id : $('#party_dropdown11').val()},
				dataType : 'JSON',
				success : function(partyPurRate) {

					if (partyPurRate === 'false') {
						
						$('#txtPRate').val(parseFloat(pRate).toFixed(2));
					} else {
						
						$('#txtPRate').val(parseFloat(partyPurRate).toFixed(2));
					}
				}, error : function(xhr, status, error) {
					console.log(xhr.responseText);
				}
			});
		}
	}


	return {

		init : function() {
			this.bindUI();
			this.bindModalPartyGrid();

			// this.bindModalOrderGrid();

		},

		bindUI : function() {
			
			var self = this;
            $(".btnPrints").attr("disabled","disabled").next().attr("disabled","disabled")

			
			$('#party_dropdown11').focus().css('border', '1px solid #368EE0');

			
			
			
			
			$('#party_dropdown').focus().css('border', '1px solid #368EE0');

			$('.btn').focus(function(){
				$(this).css('border', '2px solid black');
			}
			);
			$('.btn').blur(function(){
				$(this).css('border', '');
			}
			);
			
			
			$('#GodownAddModel').on('shown.bs.modal',function(e){
                $('#txtNameGodownAdd').focus();
            });
            $('.btnSaveMGodown').on('click',function(e){
                if ( $('.btnSave').data('savegodownbtn')==0 ){
                    alert('Sorry! you have not save departments rights..........');
                }else{
                    e.preventDefault();
                    self.initSaveGodown();
                }
            });
            $('.btnResetMGodown').on('click',function(){
                
                $('#txtNameGodownAdd').val('');
                
            });

			$('#txtLevel3').on('change', function() {
				
				var level3 = $('#txtLevel3').val();
				$('#txtselectedLevel1').text('');
				$('#txtselectedLevel2').text('');
				if (level3 !== "" && level3 !== null) {
					// alert('enter' + $(this).find('option:selected').data('level2') );	
					$('#txtselectedLevel2').text(' ' + $(this).find('option:selected').data('level2'));
					$('#txtselectedLevel1').text(' ' + $(this).find('option:selected').data('level1'));
				}
			});
			// $('#txtLevel3').select2();
			$('.btnSaveM').on('click',function(e){
				if ( $('.btnSave').data('saveaccountbtn')==0 ){
					alert('Sorry! you have not save accounts rights..........');
				}else{
					e.preventDefault();
					self.initSaveAccount();
				}
			});
			$('.btnResetM').on('click',function(){
				
				$('#txtAccountName').val('');
				$('#txtselectedLevel2').text('');
				$('#txtselectedLevel1').text('');
				$('#txtLevel3').select2('val','');
			});
			$('#AccountAddModel').on('shown.bs.modal',function(e){
				$('#txtAccountName').focus();
			});
			// shortcut.add("F3", function() {
   //  			$('#AccountAddModel').modal('show');
			// });

			$('.btnSaveMItem').on('click',function(e){
				if ( $('.btnSave').data('saveitembtn')==0 ){
					alert('Sorry! you have not save item rights..........');
				}else{
					e.preventDefault();
					self.initSaveItem();
				}
			});

			$('#contractNo').on('keypress', function(e) {
				
				if (e.keyCode === 13) {
					
					if ($(this).val() != '') {

						e.preventDefault();
						fetchContract($(this).val());
					}
				}
			});

			$('.btnResetMItem').on('click',function(){
				
				$('#txtItemName').val('');
				$('#category_dropdown').select2('val','');
				$('#subcategory_dropdown').select2('val','');
				$('#brand_dropdown').select2('val','');
				$('#txtBarcode').val('');
			});
			
			$('#ItemAddModel').on('shown.bs.modal',function(e){
				$('#txtItemName').focus();
			});
			shortcut.add("F7", function() {
    			$('#ItemAddModel').modal('show');
			});
			$("#switchPreBal").bootstrapSwitch('offText', 'No');
			$("#switchPreBal").bootstrapSwitch('onText', 'Yes');
			$('#voucher_type_hidden').val('new');
			$('.modal-lookup .populateAccount').on('click', function(){
				// alert('dfsfsdf');
				var party_id = $(this).closest('tr').find('input[name=hfModalPartyId]').val();
				$("#party_dropdown11").select2("val", party_id); 				
			});

			$('.populateContract').on('click', function(){
				// alert('dfsfsdf');
				var contractNo = $(this).closest('tr').find('td.contract').text();
				$('#contractNo').val(contractNo);
				fetchContract(contractNo);
			});
			/*$('.modal-lookup .populateItem').on('click', function(){

				var item_id = $(this).closest('tr').find('input[name=hfModalitemId]').val();
                var itemDescription = $(this).closest('tr').find('.tdDescription').text();

                var itemCodeOption = "<option>" + itemDescription + "</option>";
                var itemOption = "<option></option>";
                $('#itemid_dropdown').append(itemCodeOption);
                $('#item_dropdown').append(itemOption);
				$('#txtQty').focus();				
			});*/

			$('#txtVrnoa').on('change', function() {
				fetch($(this).val());
			});

			$('.btnSave').on('click',  function(e) {
				
				if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('updatebtn')==0 ){
					alert('Sorry! you have not update rights..........');
				}else if($('#voucher_type_hidden').val()=='new' && $('.btnSave').data('insertbtn')==0){
					alert('Sorry! you have not insert rights..........');
				}else{
					e.preventDefault();
					self.initSave();
				}
			});

			$('#item-lookup').on('shown.bs.modal', function () {
			  $('#txtItemSearch').focus()
			});

			$('#txtItemId').on('change', function(){
				var item_id = $('#hfItemId').val();
				last_stockLocatons(item_id);
			});

			$("form").submit(function() { return false; });

			/*$('.btnPrint').on('click',  function(e) {
				e.preventDefault();
				Print_Voucher(1,'lg','');
			});*/
			$('.btnprint_sm').on('click', function(e){
				e.preventDefault();
				Print_Voucher(1,'sm','');
			});
			$('.btnprint_sm_withOutHeader').on('click', function(e) {
				e.preventDefault();
				Print_Voucher(0,'sm');
			});
			$('.btnprint_sm_rate').on('click', function(e){
				e.preventDefault();
				Print_Voucher(1,'sm','wrate');
			});
			$('.btnprint_sm_withOutHeader_rate').on('click', function(e) {
				e.preventDefault();
				Print_Voucher(0,'sm','wrate');
			});

			$('.btnPrints').on('click', function(ev) {
				fetchPreviousBalance();
                window.open(base_url + 'application/views/reportprints/purchaseinvoice.php', "Purchase Invoice", 'width=1210, height=842');
            });
			$('.btnPrints_urdu').on('click', function(ev) {
                 window.open(base_url + 'application/views/reportprints/gatepassinward.php', "Inward Return", 'width=1210, height=842');
            });

			$('.btnReset').on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			$('.btnDelete').on('click', function(e){
				if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('deletebtn')==0 ){
					alert('Sorry! you have not delete rights..........');
				}else{

					// alert($('#voucher_type_hidden').val() +' - '+ $('.btnSave').data('deletebtn') );
					e.preventDefault();
					var vrnoa = $('#txtVrnoaHidden').val();
					if (vrnoa !== '') {
						if (confirm('Are you sure to delete this voucher?'))
							deleteVoucher(vrnoa);
					}
				}

			});

			$('#txtOrderNo').on('keypress', function(e) {
				if (e.keyCode === 13) {
					if ($(this).val() != '') {
						e.preventDefault();
						fetchThroughPO($(this).val());
					}
				}
			});
			
			$('.modal-lookup .populateOrder').on('click', function(){
				
				var order_id = $(this).closest('tr').find('input[name=orderid]').val();
				if(order_id!=0){
					$('#txtOrderNo').val(order_id);
					fetchThroughPO(order_id);
				}
			});

			/////////////////////////////////////////////////////////////////
			/// setting calculations for the single product
			/////////////////////////////////////////////////////////////////

			$('#txtWeight').on('input', function() {
				// var _gw = getNumVal($('#txtGWeight'));
				// if (_gw!=0) {
				// var w = parseInt(parseFloat($(this).val())/parseFloat(_gw));
				// $('#txtQty').val(w);	
				// }
				var contractNo = $('#contractNo').val();
				if(contractNo != '' && contractNo != null){
					var _uom= $('#txtUom').val().toUpperCase();
					kg=-1;
					gram=-1;
					var kg = _uom.search("KG");
					var gram = _uom.search("GRAM");
					if (kg ==-1 && gram ==-1 ){

						var balQty = getNumVal($('#txtHideBalWQ'));
						var qty = getNumVal($('#txtQty'));
						var balance = parseFloat(balQty) - parseFloat(qty);
						$('#txtBalWQ').val(parseFloat(balance).toFixed(2));
					}else{

						var balWeight = getNumVal($('#txtHideBalWQ'));
						var weight = getNumVal($('#txtWeight'));
						var balance = parseFloat(balWeight) - parseFloat(weight);
						$('#txtBalWQ').val(parseFloat(balance).toFixed(2));
					}
				}
				calculateUpperSum();
				
			});

			$('#itemid_dropdown').on('change', function() {
				var item_id = $(this).val();
				var prate = $(this).find('option:selected').data('prate');
				var grweight = $(this).find('option:selected').data('grweight');
				var uom_item = $(this).find('option:selected').data('uom_item');
				// $('#txtQty').val('1');
				var stqty = $(this).find('option:selected').data('stqty');
				var stweight = $(this).find('option:selected').data('stweight');
				$('#stqty_lbl').text('Item,     Qty:' + stqty + ', Weight ' + stweight);

				$('#txtPRate').val(parseFloat(prate).toFixed(2));
				$('#item_dropdown').select2('val',item_id);
				//$('.selectpicker').selectpicker('refresh');
				$('#txtGWeight').val(parseFloat(grweight).toFixed());
				$('#txtUom').val(uom_item);

				// calculateUpperSum();
				// $('#txtQty').focus();
			});
			$('#item_dropdown').on('change', function() {
				var item_id = $(this).val();
				var prate = $(this).find('option:selected').data('prate');
				var grweight = $(this).find('option:selected').data('grweight');
				var uom_item = $(this).find('option:selected').data('uom_item');
				// $('#txtQty').val('1');
				var stqty = $(this).find('option:selected').data('stqty');
				var stweight = $(this).find('option:selected').data('stweight');
				$('#stqty_lbl').text('Item,     Qty:' + stqty + ', Weight ' + stweight);

				$('#txtPRate').val(parseFloat(prate).toFixed(2));
				$('#itemid_dropdown').select2('val', item_id);
				$('#txtGWeight').val(parseFloat(grweight).toFixed(2));
				$('#txtUom').val(uom_item);
				// calculateUpperSum();
				// $('#txtQty').focus();
			});

			var countItem = 0;
			$('input[id="txtItemId"]').autoComplete({

				minChars: 1,
				cache: false,
				menuClass: '',
				source: function(search, response)
				{
					try { xhr.abort(); } catch(e){}
					$('#txtItemId').removeClass('inputerror');
					$("#imgItemLoader").hide();
					if(search != "")
					{
						xhr = $.ajax({
							url: base_url + 'index.php/item/searchitem',
							type: 'POST',
							data: {

								search: search
							},
							dataType: 'JSON',
							beforeSend: function (data) {

								$(".loader").hide();
								$("#imgItemLoader").show();
								countItem = 0;
							},
							success: function (data) {

								if(data == '')
								{
									$('#txtItemId').addClass('inputerror');
									clearItemData();
									$('#itemDesc').val('');
									$('#txtQty').val('');
									$('#txtPRate').val('');
									$('#txtBundle').val('');
									$('#txtGBundle').val('');
									$('#txtWeight').val('');
									$('#txtAmount').val('');
									$('#txtGWeight').val('');
									$('#txtDiscp').val('');
									$('#txtDiscount1_tbl').val('');
								}
								else
								{
									$('#txtItemId').removeClass('inputerror');
									response(data);
									$("#imgItemLoader").hide();
								}
							}
						});
					}
					else
					{
						clearItemData();
					}
				},
				renderItem: function (item, search)
				{
					var sea = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
					var re = new RegExp("(" + sea.split(' ').join('|') + ")", "gi");

					var selected = "";
					if((search.toLowerCase() == (item.item_des).toLowerCase() && countItem == 0) || (search.toLowerCase() != (item.item_des).toLowerCase() && countItem == 0))
					{
						selected = "selected";
					}
					countItem++;
					clearItemData();

					return '<div class="autocomplete-suggestion ' + selected + '" data-val="' + search + '" data-item_id="' + item.item_id + '" data-size="' + item.pack + '" data-bid="' + item.bid +
					'" data-uom_item="'+ item.uom + '" data-srate="' + item.srate + '" data-grweight="' + item.grweight + '" data-stqty="' + item.stqty +
					'" data-stweight="' + item.stweight + '" data-length="' + item.length  + '"  data-uname="' + item.uitem_des  + '" data-catid="' + item.catid +
					'" data-subcatid="' + item.subcatid + '" data-desc="' + item.item_des + '" data-cost_price="' + item.cost_price + '" data-code="' + item.item_code + '" data-self="' + item.self_freight_rate + '" data-bilty="' + item.bilty_freight_rate + 
					'" data-gari="' + item.gari_freight_rate + '">' + item.item_des.replace(re, "<b>$1</b>") + '</div>';
				},
				onSelect: function(e, term, item)
				{

					$("#imgItemLoader").hide();
					$("#hfItemId").val(item.data('item_id'));
					$("#hfItemSize").val(item.data('size'));
					$("#hfItemBid").val(item.data('bid'));
					$("#hfItemUom").val(item.data('uom_item'));
					$("#hfItemPrate").val(item.data('cost_price'));
					$("#hfItemGrWeight").val(item.data('grweight'));
					$("#hfItemStQty").val(item.data('stqty'));
					$("#hfItemStWeight").val(item.data('stweight'));
					$("#hfItemLength").val(item.data('length'));
					$("#hfItemCatId").val(item.data('catid'));
					$("#hfItemSubCatId").val(item.data('subcatid'));
					$("#hfItemDesc").val(item.data('desc'));
					$("#hfItemShortCode").val(item.data('code'));
					$("#hfItemUname").val(item.data('uname'));

					var freightStatus = $('input[name=inlineRadioOptions]:checked').val();

					if(freightStatus == 'self'){
						$('#txtFRate').val(item.data('self'));	
					}else if(freightStatus == 'bilty'){
						$('#txtFRate').val(item.data('bilty'));
					}else if(freightStatus == 'gari'){
						$('#txtFRate').val(item.data('gari'));
					}

					$("#txtItemId").val(item.data('code'));

					var itemId = item.data('item_id');
					var itemDesc = item.data('desc');
					var pRate = item.data('cost_price');
					var grWeight = item.data('grweight');
					var uomItem = item.data('uom_item');
					var stQty = item.data('stqty');
					var stWeight = item.data('stweight');
					var size = item.data('size');
					var brandId = item.data('bid');

					$('#stqty_lbl').text('Item,     Uom:' + uomItem);
					$('#itemDesc').val(itemDesc);
					$('#txtPRate').val(parseFloat(pRate).toFixed(2));
					$('#txtGWeight').val(parseFloat(grWeight).toFixed(2));
					$('#txtUom').val(uomItem);
					$('#txtGWeight').trigger('input');
					fetchPartyPurRate(itemId, pRate);	
				}
			});

			$('#txtQty').on('input', function() {
				var contractNo = $('#contractNo').val();
				if(contractNo != '' && contractNo != null){
					var _uom= $('#txtUom').val().toUpperCase();
					kg=-1;
					gram=-1;
					var kg = _uom.search("KG");
					var gram = _uom.search("GRAM");
					if (kg ==-1 && gram ==-1 ){

						var balQty = getNumVal($('#txtHideBalWQ'));
						var qty = getNumVal($('#txtQty'));
						var balance = parseFloat(balQty) - parseFloat(qty);
						$('#txtBalWQ').val(parseFloat(balance).toFixed(2));
					}else{

						var balWeight = getNumVal($('#txtHideBalWQ'));
						var weight = getNumVal($('#txtWeight'));
						var balance = parseFloat(balWeight) - parseFloat(weight);
						$('#txtBalWQ').val(parseFloat(balance).toFixed(2));
					}
				}
				calculateUpperSum();
			});
			$('#txtPRate').on('input', function() {
				calculateUpperSum();
			});


			$('#btnAdd').on('click', function(e) {
				e.preventDefault();

				var error = validateSingleProductAdd();
				if (!error) {

					var item_desc = $('#itemDesc').val();
					var uitem_des = $('#hfItemUname').val();
					var uom_item = $('#hfItemUom').val();
					var item_code = $('#hfItemShortCode').val();
					var item_id = $('#hfItemId').val();
					var godown = $('#dept_dropdown').find('option:selected').text();
					var godown_id = $('#dept_dropdown').val();
					var qty = $('#txtQty').val();
					var rate = $('#txtPRate').val();
					var weight = $('#txtWeight').val();
					var balQtyWeight = $('#txtBalWQ').val();
					var hideBalQtyWeight = $('#txtHideBalWQ').val();
					var amount = $('#txtAmount').val();
					var contract = $('#contractNo').val();

					// reset the values of the annoying fields
					
					$('#itemDesc').val('');
					$('#txtItemId').val('');
					$('#dept_dropdown').select2('val', '');
					$('#txtQty').val('');
					$('#txtPRate').val('');
					$('#txtWeight').val('');
					$('#txtAmount').val('');
					$('#txtGWeight').val('');
					$('#txtBalWQ').val('');
					$('#txtHideBalWQ').val('');
					$('#contractNo').val('');


					appendToTable('', item_desc, item_id, godown , godown_id , qty, rate, amount, weight,item_code,uitem_des,contract,balQtyWeight,hideBalQtyWeight);
					calculateLowerTotal(qty, amount, weight);
					$('#stqty_lbl').text('Item');
					$('#txtItemId').focus();
				} else {
					alert('Correct the errors!');
				}

			});

			// when btnRowRemove is clicked
			$('#purchase_table').on('click', '.btnRowRemove', function(e) {
				e.preventDefault();
				var qty = $.trim($(this).closest('tr').find('td.qty').text());
				var amount = $.trim($(this).closest('tr').find('td.amount').text());
				var weight = $.trim($(this).closest('tr').find('td.weight').text());
				calculateLowerTotal("-"+qty, "-"+amount, '-'+weight);
				$(this).closest('tr').remove();
			});
			$('#purchase_table').on('click', '.btnRowEdit', function(e) {
				e.preventDefault();

				// getting values of the cruel row
				var item_id = $.trim($(this).closest('tr').find('td.item_desc').data('item_id'));
				var godown_id = $.trim($(this).closest('tr').find('td.godown').data('godown_id'));
				var qty = $.trim($(this).closest('tr').find('td.qty').text());
				var weight = $.trim($(this).closest('tr').find('td.weight').text());
				var balQtyWeight = $.trim($(this).closest('tr').find('td.balqtyweight').text());
				var hideBalQtyWeight = $.trim($(this).closest('tr').find('td.hidebalqtyweight').text());
				var rate = $.trim($(this).closest('tr').find('td.rate').text());
				var amount = $.trim($(this).closest('tr').find('td.amount').text());
				var contract = $.trim($(this).closest('tr').find('td.contract').text());

                $.ajax({
					type: "POST",
					url: base_url + 'index.php/item/getiteminfobyid',
					data: {
						item_id: item_id
					}
				}).done(function (result) {

					$("#imgItemLoader").hide();
					var item = $.parseJSON(result);

					if (item != false)
					{
						$("#hfItemId").val(item[0].item_id);
						$("#hfItemSize").val(item[0].size);
						$("#hfItemBid").val(item[0].bid);
						$("#hfItemUom").val(item[0].uom);
						$("#hfItemPrate").val(item[0].cost_price);
						$("#hfItemGrWeight").val(item[0].grweight);
						$("#hfItemStQty").val(item[0].stqty);
						$("#hfItemStWeight").val(item[0].stweight);
						$("#hfItemLength").val(item[0].length);
						$("#hfItemCatId").val(item[0].catid);
						$("#hfItemSubCatId").val(item[0].subcatid);
						$("#hfItemDesc").val(item[0].item_des);
						$("#hfItemShortCode").val(item[0].short_code);
						$("#hfItemUname").val(item[0].uitem_des);


						$("#txtItemId").val(item[0].item_code);
						$('#itemDesc').val(item[0].item_des);
						$('#txtUom').val(item[0].uom);
						$('#txtGWeight').val(item[0].grweight);

						$('#contractNo').val(contract);
						$('#txtBalWQ').val(balQtyWeight);
						$('#txtHideBalWQ').val(hideBalQtyWeight);
						$('#dept_dropdown').select2('val',godown_id);
						$('#txtQty').val(qty);
						$('#txtPRate').val(rate);
						$('#txtWeight').val(weight);
						$('#txtAmount').val(amount);
						$('#stqty_lbl').text('Item,     Uom:' + item[0].uom);


						calculateLowerTotal("-"+qty, "-"+amount, '-'+weight);
                        // now we have get all the value of the row that is being deleted. so remove that cruel row

                    }
                });
				
				// now we have get all the value of the row that is being deleted. so remove that cruel row
				$(this).closest('tr').remove();	// yahoo removed
			});

			$('#txtDiscount').on('input', function() {
				var _disc= $('#txtDiscount').val();
				var _totalAmount= $('#txtTotalAmount').val();
				var _discamount=0;
				if (_disc!=0 && _totalAmount!=0){
					_discamount=_totalAmount*_disc/100;
				}
				$('#txtDiscAmount').val(_discamount);
				calculateLowerTotal(0, 0, 0);
			});

			$('#txtDiscAmount').on('input', function() {
				var _discamount= $('#txtDiscAmount').val();
				var _totalAmount= $('#txtTotalAmount').val();
				var _discp=0;
				if (_discamount!=0 && _totalAmount!=0){
					_discp=_discamount*100/_totalAmount;
				}
				$('#txtDiscount').val(parseFloat(_discp).toFixed(2));
				calculateLowerTotal(0, 0, 0);
			});

			$('#txtExpense').on('input', function() {
				var _exppercent= $('#txtExpense').val();
				var _totalAmount= $('#txtTotalAmount').val();
				var _expamount=0;
				if (_exppercent!=0 && _totalAmount!=0){
					_expamount=_totalAmount*_exppercent/100;
				}
				$('#txtExpAmount').val(_expamount);
				calculateLowerTotal(0, 0, 0);
			});

			$('#txtExpAmount').on('input', function() {
				var _expamount= $('#txtExpAmount').val();
				var _totalAmount= $('#txtTotalAmount').val();
				var _exppercent=0;
				if (_expamount!=0 && _totalAmount!=0){
					_exppercent=_expamount*100/_totalAmount;
				}
				$('#txtExpense').val(parseFloat(_exppercent).toFixed(2));
				calculateLowerTotal(0, 0, 0);
			});

			$('#txtTax').on('input', function() {
				var _taxpercent= $('#txtTax').val();
				var _totalAmount= $('#txtTotalAmount').val();
				var _taxamount=0;
				if (_taxpercent!=0 && _totalAmount!=0){
					_taxamount=_totalAmount*_taxpercent/100;
				}
				$('#txtTaxAmount').val(_taxamount);
				calculateLowerTotal(0, 0, 0);
			});

			$('#txtTaxAmount').on('input', function() {
				var _taxamount= $('#txtTaxAmount').val();
				var _totalAmount= $('#txtTotalAmount').val();
				var _taxpercent=0;
				if (_taxamount!=0 && _totalAmount!=0){
					_taxpercent=_taxamount*100/_totalAmount;
				}
				$('#txtTax').val(parseFloat(_taxpercent).toFixed(2));
				calculateLowerTotal(0, 0, 0);
			});

			$('#txtFRate').on('input', function() {
				var fRate = getNumVal($('#txtFRate'));
				var totalWeight = getNumVal($('#txtTotalWeight'));
				var	fAmount = fRate * totalWeight;

				$('#txtFAmount').val(parseFloat(fAmount).toFixed(2));
				calculateLowerTotal(0, 0, 0);
			});

			$('#txtLRate').on('input', function() {
				var lRate = getNumVal($('#txtLRate'));
				var totalWeight = getNumVal($('#txtTotalWeight'));
				var	LAmount = lRate * totalWeight;
				
				$('#txtLAmount').val(parseFloat(LAmount).toFixed(2));
				calculateLowerTotal(0, 0, 0);
			});

			$('#txtULRate').on('input', function() {
				var uLRate = getNumVal($('#txtULRate'));
				var totalWeight = getNumVal($('#txtTotalWeight'));
				var	uLAmount = uLRate * totalWeight;
				
				$('#txtULAmount').val(parseFloat(uLAmount).toFixed(2));
				calculateLowerTotal(0, 0, 0);
			});

			$('#txtScaleCharges').on('input',function(){
				calculateLowerTotal(0, 0, 0);
			});


			$('#party_dropdown11').on('change',function(){

				var partyId = $(this).val();
				var vrDate = $('#current_date').val();
				
				fetchRunningTotal(partyId, vrDate);
				fetchPartyPurRate($('#hfItemId').val(), $('#txtPRate').val());
			});

			
			$('#item_dropdown').on('change', function(){
				var item_id = $(this).find('option:selected').val();
				last_stockLocatons(item_id);
			});

			$('.btnSave1').on('click', function(){

				$('.btnSave').click();
			});

			// alert('load');

			shortcut.add("F10", function() {
    			$('.btnSave').trigger('click');
			});
			shortcut.add("F1", function() {
				$('a[href="#party-lookup"]').trigger('click');
			});
			shortcut.add("F2", function() {
				$('a[href="#item-lookup"]').trigger('click');
			});
			shortcut.add("F3", function() {
				$('a[href="#contract-lookup"]').trigger('click');
			});
			shortcut.add("F9", function() {
				Print_Voucher(1,'lg','');
			});
			shortcut.add("F6", function() {
    			$('#txtVrnoa').focus();
    			// alert('focus');
			});
			shortcut.add("F5", function() {
    			self.resetVoucher();
			});

			shortcut.add("F12", function() {
    			$('.btnDelete').trigger('click');
			});


			$('#txtVrnoa').on('keypress', function(e) {
				if (e.keyCode === 13) {
					e.preventDefault();
					var vrnoa = $('#txtVrnoa').val();
					if (vrnoa !== '') {
						fetch(vrnoa);
					}
				}
			});
			$('.btnprintHeader').on('click', function(e) {
				e.preventDefault();
				Print_Voucher(1,'lg','');

			});
			$('.btnprintwithOutHeader').on('click', function(e) {
				e.preventDefault();
				Print_Voucher(0,'lg','amount');
			});

			//$('.selectpicker').selectpicker();
			
			onChange(1);
            $("input[id$='txtItemSearch']").keydown(function(event){
                var keyCode = event.keyCode;
                if(keyCode == 13)
                {
                    onChange(1);
                    event.preventDefault();
                    return false;
                }
                else if(keyCode == 8 || keyCode == 46)
                {
                    var txt = $("input[id$='txtItemSearch']").val();
                    if(txt.length <= 1)
                    {
                        $("input[id$='txtItemSearch']").val('');
                        onChange(1);
                    }
                }
            });

			purchase.fetchRequestedVr();
		},

		// prepares the data to save it into the database
		initSave : function() {

			var saveObj = getSaveObject();
			var error = validateSave();

			if (!error) {
				var rowsCount = $('#purchase_table').find('tbody tr').length;
				if (rowsCount > 0 ) {
					save(saveObj);
				} else {
					alert('No date found to save!');
				}
			} else {
				alert('Correct the errors...');
			}
		},
		initSaveAccount : function() {

			var saveObjAccount = getSaveObjectAccount();
			var error = validateSaveAccount();

			if (!error) {
					saveAccount(saveObjAccount);
			} else {
				alert('Correct the errors...');
			}
		},
		initSaveItem : function() {

			var saveObjItem = getSaveObjectItem();
			var error = validateSaveItem();

			if (!error) {
					saveItem(saveObjItem);
			} else {
				alert('Correct the errors...');
			}
		},
		initSaveGodown : function() {

            var saveObjGodown = getSaveObjectGodown();
            var error = validateSaveGodown();

            if (!error) {
                    saveGodown(saveObjGodown);
            } else {
                alert('Correct the errors...');
            }
        },
		fetchRequestedVr : function () {

		var vrnoa = general.getQueryStringVal('vrnoa');
		vrnoa = parseInt( vrnoa );
		$('#txtVrnoa').val(vrnoa);
		$('#txtVrnoaHidden').val(vrnoa);
		if ( !isNaN(vrnoa) ) {
			fetch(vrnoa);
		}else{
			getMaxVrno();
			getMaxVrnoa();
		}
	},

				bindModalPartyGrid : function() {

			
				            var dontSort = [];
				            $('#party-lookup table thead th').each(function () {
				                if ($(this).hasClass('no_sort')) {
				                    dontSort.push({ "bSortable": false });
				                } else {
				                    dontSort.push(null);
				                }
				            });
				            purchase.pdTable = $('#party-lookup table').dataTable({
				                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
				                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
				                "aaSorting": [[0, "asc"]],
				                "bPaginate": true,
				                "sPaginationType": "full_numbers",
				                "bJQueryUI": false,
				                "aoColumns": dontSort

				            });
				            $.extend($.fn.dataTableExt.oStdClasses, {
				                "s`": "dataTables_wrapper form-inline"
				            });
},

		// instead of reseting values reload the page because its cruel to write to much code to simply do that
		resetVoucher : function() {
			 general.reloadWindow();
		}
	}

};

var purchase = new Purchase();
purchase.init();
