var Attachments = function()
{
	let fetchImages = function(vrnoa,etype)
	{
		$.ajax({
			url : base_url + 'index.php/fileuploader/fetchImages',
			type : 'POST',
			data : { 'vrnoa' : vrnoa, 'etype' : etype },
			dataType : 'JSON',
			success : function(data)
			{
				// console.log(data);
				if (data === 'false')
				{
					alert('No Attachments found');
				} 
				else
				{
					populateImages(data);
				}

			}, 
			error : function(xhr, status, error)
			{
				console.log(xhr.responseText);
			}
		});
	}

	let updateRemarks = function(data,etype)
	{

		$.ajax({
			url : base_url + 'index.php/fileuploader/updateFilesRemarks',
			type : 'POST',
			data : { 'vrnoa' : $('#txtIdImg').val(), 'etype' : etype, 'data' : data },
			dataType : 'JSON',
			beforeSend : function()
			{
				$(".loader").show();
			},
			success : function(data) 
			{
				if($.inArray( false, data ) == -1)
				{
					alert('Files Uploaded successfully With Remarks!');
					general.reloadWindow();
				}
				$('.loader').show();		
			}, 
			error : function(xhr, status, error)
			{
				console.log(xhr.responseText);
				$('.loader').hide();
			}
		});
	}

	let populateImages = function(data)
	{
		$(".dz-preview").empty();

		$.each(data, function(i, elem)
		{
			let re 	= /(?:\.([^.]+))?$/;
			let ext = re.exec(data[i]['photo'])[1];

			ext = ext.toLowerCase();
			// Create the mock file:

			let mockFile = { name: data[i]['photo'], size: 12345 };
			if (data[i]['photo'] != "")
			{
				var src = base_url + 'assets/uploads/' + data[0]['etype'] + '/';
				if(ext == 'jpg' || ext == 'jpeg' || ext == 'bmp' || ext == 'png' || ext == 'gif')
				{
					// Call the default addedfile event handler
					imageUpload.emit("addedfile", mockFile);

					// And optionally show the thumbnail of the file:
					imageUpload.emit("thumbnail", mockFile, src + data[i]['photo']);
				} 
				else
				{

					// Call the default addedfile event handler
					imageUpload.emit("addedfile", mockFile);

					// And optionally show the thumbnail of the file:
					imageUpload.emit("thumbnail", mockFile, base_url + '/assets/uploads/icons/doc_icon.png');
				}

				$('.dz-remove').each(function(index, elem)
				{
					if(index == i)
					{
						$(this).after("<a class='btn btn-primary downloadBtn' href='" + src + data[i]['photo'] + "' target='_blank'>Download <i class='fa fa-download'></i></a>");
						var imageName = document.createElement('span');
						$(imageName).addClass('text-success');
						if(data[i]['photo'].length > 23)
						{
							var newName = data[i]['photo'].slice(0,23);
							imageName.innerHTML = newName + '...';

						} 
						else
						{
							imageName.innerHTML = data[i]['photo'];
						}

						$(this).before(imageName);
						let remarks = data[i]['remarks'];

						$(this).siblings('.attachRemarks').val(remarks);
					}
				});
				
				// // Or if the file on your server is not yet in the right
				// // size, you can let Dropzone download and resize it
				// // callback and crossOrigin are optional.
				// // imageUpload.createThumbnailFromUrl(file, imageUrl, callback, crossOrigin);
				// imageUpload.files.push( mockFile ); 
				// Make sure that there is no progress bar, etc...
				imageUpload.emit("complete", mockFile);
				// If you use the maxFiles option, make sure you adjust it to the
				// correct amount:
				var existingFileCount = 1; // The number of files already uploaded
				imageUpload.options.maxFiles = imageUpload.options.maxFiles - existingFileCount;
			}
		});
	}

	let Attachmentspanel =  function()
	{
		// var panel = confirm('Would you like to Attachments Panel???');
		// if (panel === true)
		// {
			var div  = '';
			div += '<div class="row">' +
                            '<div class="panel panel-default">'+
                                '<div class="panel-body">' +
                                    '<div class="row">' +
                                        '<div class="col-md-3">' +
                                            '<input type="number" class="form-control" id="txtIdImg" style="background: #fff;" readonly />' +
                                        '</div>' +
                                        '<div class="col-md-6">' +
                                            '<button type="button" class="btn btn-primary" id="viewImages">View Attachments <i class="fa fa-eye"></i></button>' +
                                            '<button type="button" class="btn btn-success" id="uploadFiles">Update Files <i class="fa fa-upload"></i></button>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="row">' +
                                        '<div class="col-md-12">' +
                                            '<form enctype="multipart/form-data" class="dropzone" id="image-upload" method="POST">' +
                                                '<div>' +
                                                    '<h3>Attach Files to Upload</h3>' +
                                                    '<div class="fallback">' +
                                                        '<input name="file" type="file" />' +
                                                    '</div>' +
                                                '</div>' +
                                            '</form>' +
                                            '<small style="color: #555;""> Supported Files: (.bmp, .gif, .jpg, .jpeg, .png, .pdf, .doc, .docx, .txt, .xls, .xlsx, .rtf)</small>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' ;
            $(div).appendTo('.Attachmentspanel');
		// }
	}

	return {
		init : function()
		{
			this.bindUI();
			// Attachmentspanel();
		},
		bindUI : function()
		{
			let self 	= this;
			let etype 	= $('#txtetype').val(); 
			console.log(etype);
			Dropzone.options.imageUpload = {
				url: base_url + "index.php/fileuploader/uploadFiles",
				maxFilesize: 1,
				acceptedFiles: "image/*,application/pdf,.docx,.doc,.xls,.xlsx,.txt,.rtf",
				parallelUploads: 100,
		        maxFiles: 100,
		        thumbnailWidth:"180",
           		thumbnailHeight:"180",
           		autoProcessQueue: false,
		        dictRemoveFileConfirmation: "Are you Sure to Delete this File?",
		        dictDefaultMessage: "Drop Files here to upload",
		        addRemoveLinks: true,
		        dictResponseError: 'Server not Configured',
		        dictRemoveFile: "X (remove)",
		        accept: function(file, done)
		        {
		        	let src 		= base_url + '/assets/uploads/icons/';
		        	let thumbnail 	= $('.dropzone .dz-preview.dz-file-preview .dz-image');

		        	switch (file.type)
		        	{
		        		case 'application/pdf':
		        		thumbnail.css({'background': 'url('+ src +'doc_icon.png', 'background-size': '100% 100%'});
		        		break;
		        		case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
		        		thumbnail.css({'background': 'url('+ src +'doc_icon.png', 'background-size': '100% 100%'});
		        		break;
		        		case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
				       	thumbnail.css({'background': 'url('+ src +'doc_icon.png', 'background-size': '100% 100%'});
				       	break;
				       	case 'application/vnd.ms-excel':
				       	thumbnail.css({'background': 'url('+ src +'doc_icon.png', 'background-size': '100% 100%'});
				        break;
				    }
				    done();
				},
		        
		        init : function()
		        {
		        	imageUpload = this;
			        //Restore initial message when queue has been completed]
			        this.on("addedfile", function(file, dataUrl)
			        {
			        	// console.log(file.previewTemplate);
			        	let elem = "<input type='text' placeholder='Enter Remarks' class='form-control attachRemarks' id='attachRemark' />";
			        	$(file.previewTemplate).append(elem);
			    	});
			    	this.on('sending', function(file, xhr, formData)
			    	{
			    		let remarks = $(file.previewTemplate).find('.attachRemarks').val();
			    		formData.append('vrnoa', 	$("#txtIdImg").val());
			    		formData.append('imgName', 	file.name);
			    		formData.append('etype', 	etype);
			    		formData.append('remarks', 	remarks);
			    		formData.append('table', 	$('#tablename').val());					    
					});

					this.on('success', function(file, response)
					{
						if (response===true) 
						{
							alert('File Upload successfully!!!');
							general.reloadWindow();
						}
						else
						{	
							alert('This Voucher Not Saved!!!');
							general.reloadWindow();
						}
					});

					this.on('error', function(file, errorMessage, xhr)
					{
						$(file.previewElement).find('.dz-error-message').text(errorMessage['message']);
					});
						
						this.on('removedfile', function(file)
						{
							if ($('#voucher_type_hidden').val()=='edit' && $('#uploadFiles').data('delete')==0 ){
								alert('Sorry! you have not delete rights..........');
							}
							else if($('#voucher_type_hidden').val()=='new' && $('#uploadFiles').data('delete')==0)
							{
								alert('Sorry! you have not delete rights..........');
							} 
							else 
							{
								$.ajax({
									url : base_url + "index.php/fileuploader/deleteImage",
									type : 'POST',
									data : {'imgName' : file.name, 'item_id': $("#txtIdImg").val(), 'etype' : etype},
									success : function(response)
									{
										alert(response + "  Removed");
									   },
									   error : function(xhr, status, error)
									   {
										   alert('File Not Found on Server!');
										console.log(xhr.responseText);
									}
								   });
							}
							
						});
			       

			     },
		    };
		    $('#uploadFiles').on('click', function(e)
		    {
				e.preventDefault();
				if ($('#voucher_type_hidden').val()=='edit' && $('#uploadFiles').data('update')==0 ){
					alert('Sorry! you have not update  rights..........');
				}else if($('#voucher_type_hidden').val()=='new' && $('#uploadFiles').data('insert')==0){
					alert('Sorry! you have not insert rights..........');
				}
				else
				{
					let files = [];
					if($('.dz-preview').length > 0)
					{
						$('.dz-preview').each(function(e)
						{
							let obj 	= {};
							obj.photo 	= $(this).find('.dz-filename span').text();
							obj.remarks = $(this).find('#attachRemark').val();
							files.push(obj);
						});
						updateRemarks(files,etype);
						imageUpload.processQueue();
					}
					else
					{
						alert('No Files Found to Upload');
					}	
				}
			});
			
		    $("#viewImages").on('click', function(e){
				e.preventDefault();
				var vrnoa = $('#txtIdImg').val().trim();
				$('.dz-preview').remove();
				$('.dz-default.dz-message').css('display', 'block');
				// var etype = ($('#cpv').is(':checked') === true) ? 'cpv' : 'crv';
				fetchImages(vrnoa,etype);
			});
		},
	};
};

var Attachments = new Attachments();
Attachments.init();
