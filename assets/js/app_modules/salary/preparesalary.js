var PrepareSalary = function() {


var oTable = $('#salary_table').dataTable({
            "bPaginate":false,
            "bDestroy": true
        });
	// saves the data into the database
	var save = function( saveObj, dcno ) {

		$.ajax({
			url : base_url + 'index.php/staff/saveSalarySheet',
			type : 'POST',
			data : { 'pledgers' : JSON.stringify(saveObj.p), 'salarysheet' : JSON.stringify(saveObj.s), 'dcno' : dcno, 'etype' : 'salary','voucher_type_hidden':$('#voucher_type_hidden').val() },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					alert('Salarysheet saved successfully.');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var search = function(from, to, warehouse) {

		$.ajax({
			url : base_url + 'index.php/staff/getSalary',
			type : 'POST',
			data : { 'from' : from, 'to' : to , 'warehouse' : warehouse},
			dataType : 'JSON',
			success : function(data) {
                oTable.fnClearTable();
				if (data === 'false') {
					alert('No data found');
				} else {
					resettotals();
					populateSearchData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateSearchData = function(data) {

		// removes all rows
		//$('#salary_table').find('tbody tr :not(.dataTables_empty)').remove();
		$('#voucher_type_hidden').val('edit');

         resettotals();
        oTable.fnClearTable();
        var counter = 1;
		$.each(data, function(index, elem) {

            var grossSalary = calGrossTotal(elem.paid_days, elem.bsalary);

            oTable.fnAddData( [
				counter++,

                    "<span class='dept_name' data-spid='"+ elem.salary_account_id +"' data-did='"+ elem.did +"'>"+ elem.department_name +"</span>",
                    "<span class='name' data-staid='"+ elem.staid +"' data-pid='"+ elem.pid +"' data-shid='"+ elem.shid +"'>"+ elem.name +"</span>",
                    "<span class='designation'>"+ elem.designation +"</span>",
                    "<span class='bsalary'>"+ elem.bsalary +"</span>",
                    "<span class='absent'>"+ elem.absent +"</span>",
                 	"<span class='rest_days'>"+ elem.rest_days +"</span>",
                    "<span class='work_days'>"+ elem.work_days +"</span>",
                    "<span class='paid_days'>"+ parseFloat(elem.paid_days) +"</span>",
                    "<span class='gross_salary'>"+ grossSalary +"</span>",
                    "<span class='othour'>"+ elem.othour +"</span>",
                    "<span class='otrate'>"+ elem.otrate +"</span>",
                    "<span class='overtime'>"+ parseFloat(elem.overtime).toFixed(2) +"</span>",
                    "<span class='advance'>"+ parseFloat(elem.advance).toFixed(2) +"</span>",
                    "<span class='loan_deduction'>"+ parseFloat(elem.loan_deduction).toFixed(2) +"</span>",
                    "<span class='balance'>"+ elem.balance +"</span>",
                    "<span class='incentive'>"+ parseFloat(elem.incentive).toFixed(2) +"</span>",
                    "<span class='penalty'>"+ parseFloat(elem.penalty).toFixed(2) +"</span>",
                    "<span class='eobi'>"+ parseFloat( elem.eobi).toFixed(2) +"</span>",
                    // "<span class='insurance'>"+ elem.insurance +"</span>",
                    // "<span class='socialsec'>"+ elem.socialsec +"</span>",
                    "<span class='net_salary'>"+ calNetSalary(grossSalary, elem.penalty, elem.advance, elem.loan_deduction, elem.overtime, elem.incentive, elem.eobi, elem.insurance, elem.socialsec) +"</span>" ]
			);

					var netSal=calNetSalary(grossSalary, elem.penalty, elem.advance, elem.loan_deduction, elem.overtime, elem.incentive, elem.eobi, elem.insurance, elem.socialsec);
					CalculateTotals(elem.bsalary,elem.absent,elem.rest_days,elem.work_days,parseFloat(elem.paid_days),grossSalary,elem.othour,elem.overtime,elem.advance,elem.loan_deduction,elem.balance,elem.incentive,elem.penalty,elem.eobi,netSal);
		});
	}

    var calGrossTotal = function(paidDays, basisSalary) {

        var salaryPlane = $('#hfSalaryPlane').val();
        var totalSalary = "0";
        if(salaryPlane == "monthday")
        {
            var date = new Date($.trim($('#to_date').val()));
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            lastDay = lastDay.getDate();
            totalSalary = (basisSalary / lastDay) * paidDays;
        }
        else
        {
            totalSalary = (basisSalary / 30) * paidDays;
        }
        return parseInt(totalSalary);

    }

    var calNetSalary = function(grossSalary, penalty, advance, loanDeduction, overtime, incentive, eobi, insurance, socialSec) {

        var netSalary = parseFloat(grossSalary) - parseFloat(penalty) - parseFloat(advance) - parseFloat(loanDeduction) + parseFloat(overtime) + parseFloat(incentive) - parseFloat(eobi) - parseFloat(insurance) - parseFloat(socialSec);
        return parseInt(netSalary);
    }

    var calBalance = function(grossSalary, penalty, advance, loanDeduction, overtime, incentive, eobi, insurance, socialSec) {

        var balance = parseFloat(grossSalary) - parseFloat(penalty) - parseFloat(advance) - parseFloat(loanDeduction) + parseFloat(overtime) + parseFloat(incentive) - parseFloat(eobi) - parseFloat(insurance) - parseFloat(socialSec);
        return parseInt(balance);
    }

	var fetch = function(dcno) {

		$.ajax({
			url : base_url + 'index.php/staff/fetchSalarySheet',
			type : 'POST',
			data : { 'dcno' : dcno,'etype' : 'salary' },
			dataType : 'JSON',
			success : function(data) {
                  oTable.fnClearTable();

				if (data === 'false') {
					alert('No data found');
					
				} else {
					resettotals();
					populateHeadData(data);
					populateSearchData(data);
					$('.btnSave').attr('disabled', false);
					general.setUpdatePrivillage();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// generates the view
	var populateHeadData = function(data) {

		$('#txtId').val(data[0]['dcno']);
		$('#txtIdHidden').val(data[0]['dcno']);
		$('#voucher_type_hidden').val('edit');

		$('#from_date').val( data[0]['dts'].substr(0, 10));
		$('#to_date').val( data[0]['dte'].substr(0, 10));
		$('#warehouse').val(data[0]['did']);

	}

	// gets the maxid of the voucher
	var getMaxId = function() {

		$.ajax({
			url : base_url + 'index.php/staff/getMaxSalaryId',
			type : 'POST',
			data : {'etype': 'salary'},
			dataType : 'JSON',
			success : function(data) {

				$('#txtId').val(data);
				$('#txtIdHidden').val(data);
				$('#txtMaxIdHidden').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var validateSearch = function() {

		var errorFlag = false;
        var warehouse = $.trim($('#warehouse').val());
		var from = $.trim($('#from_date').val());
		var to = $.trim($('#to_date').val());

		// remove the error class first
		$('#from_date').removeClass('inputerror');
		$('#to_date').removeClass('inputerror');
        $('#warehouse').removeClass('inputerror');
		if ( from === '' ) {
			$('#from_date').addClass('inputerror');
			errorFlag = true;
		}

		if ( to === '' ) {
			$('#to_date').addClass('inputerror');
			errorFlag = true;
		}
		if ( warehouse === '' ) {
			$('#warehouse').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var isFieldValid = function() {

		var errorFlag = false;
		var name = '#txtSectionName';		// get the current fee category name entered by the user
		var secid = '#txtSectionIdHidden';		// hidden secid
		var maxId = '#txtMaxSectionIdHidden';		// hidden max secid
		var txtnameHidden = '#txtSectionNameHidden';		// hidden fee category name

		var sectionNames = new Array();
		// get all branch names from the hidden list
		$("#allSections option").each(function(){
			sectionNames.push($(this).text().trim().toLowerCase());
		});

		// if both values are not equal then we are in update mode
		if (secid.val() !== maxId.val()) {

			$.each(sectionNames, function(index, elem){

				if (txtnameHidden.val().toLowerCase() !== elem.toLowerCase() && name.val().toLowerCase() === elem.toLowerCase()) {
					name.addClass('inputerror');
					errorFlag = true;
				}
			});

		} else {	// if both are equal then we are in save mode

			$.each(sectionNames, function(index, elem){

				if (name.val().trim().toLowerCase() === elem) {
					name.addClass('inputerror');
					errorFlag = true;
				}
			});
		}

		return errorFlag;
	}
	var getNumVal = function(el){
		return isNaN(parseFloat(el.val())) ? 0 : parseFloat(el.val());
	}
	  var getNumText = function (el) {
        return isNaN(parseFloat(el.text())) ? 0 : parseFloat(el.text());
    }
var CalculateTotals= function(basicsalary,absent,restday,workdays,paiddays,grosssalary,overtime,otamount,advance,loanDeduction,remainingbalance,incentive,penalty,eobi,netsalary)
{
	console.log(absent);
var tempbasicsalary=parseFloat(getNumText($('#txtBasicSalary')))+parseFloat(basicsalary);
$('#txtBasicSalary').text(parseFloat(tempbasicsalary).toFixed(2));

var tempabsent=parseFloat(getNumText($('#txtAbsent')))+parseFloat(absent);
$('#txtAbsent').text(parseFloat(tempabsent).toFixed(2));

var temprestday=parseFloat(getNumText($('#txtRestDay')))+parseFloat(restday);
$('#txtRestDay').text(parseFloat(temprestday).toFixed(2));

var tempworkday=parseFloat(getNumText($('#txtWorkDay')))+parseFloat(workdays);
$('#txtWorkDay').text(parseFloat(tempworkday).toFixed(2));

var temppaidday=parseFloat(getNumText($('#txtPaidDay')))+parseFloat(paiddays);
$('#txtPaidDay').text(parseFloat(temppaidday).toFixed(2));

var tempgrosssalary=parseFloat(getNumText($('#txtGrossSalary')))+parseFloat(grosssalary);
$('#txtGrossSalary').text(parseFloat(tempgrosssalary).toFixed(2));

var tempovertime=parseFloat(getNumText($('#txtOverTime')))+parseFloat(overtime);
$('#txtOverTime').text(parseFloat(tempovertime).toFixed(2));

var tempotamount=parseFloat(getNumText($('#txtOtAmount')))+parseFloat(otamount);
$('#txtOtAmount').text(parseFloat(tempotamount).toFixed(2));

var tempadvance=parseFloat(getNumText($('#txtAdvance')))+parseFloat(advance);
$('#txtAdvance').text(parseFloat(tempadvance).toFixed(2));

var temploandeduction=parseFloat(getNumText($('#txtLoanDeduction')))+parseFloat(loanDeduction);
$('#txtLoanDeduction').text(parseFloat(temploandeduction).toFixed(2));

var tempremainingbalance=parseFloat(getNumText($('#txtRemainingBalance')))+parseFloat(remainingbalance);
$('#txtRemainingBalance').text(parseFloat(tempremainingbalance).toFixed(2));

var tempincentive=parseFloat(getNumText($('#txtIncentive')))+parseFloat(incentive);
$('#txtIncentive').text(parseFloat(tempincentive).toFixed(2));

var temppenalty=parseFloat(getNumText($('#txtPenalty')))+parseFloat(penalty);
$('#txtPenalty').text(parseFloat(temppenalty).toFixed(2));

var tempeobi=parseFloat(getNumText($('#txtEobi')))+parseFloat(eobi);
$('#txtEobi').text(parseFloat(tempeobi).toFixed(2));

var tempnetsalary=parseFloat(getNumText($('#txtNetSalary')))+parseFloat(netsalary);
$('#txtNetSalary').text(parseFloat(tempnetsalary).toFixed(2));



}
	var getPartyId = function(partyName) {
		var pid = "";
		$('#name_dropdown option').each(function() { if ($(this).text().trim().toLowerCase() == partyName) pid = $(this).val();  });
		return pid;
	}

	// returns the salarysheet object to save into database
	var getSaveObject = function() {

		var salarysheet = [];
		var pledgers = [];

		var _from = $('#from_date').val();
		var _to = $('#to_date').val();
		var _monthName = general.getCurrentMonthName();
		var _dcno = $('#txtIdHidden').val().trim();
		var _etype = 'salary';
		var _date = general.getCurrentDate();

		$('#salary_table').find('tbody tr').each(function( index, elem ) {

			var ss = {};
			var pledger = {};
			var _pid = $(elem).find('span.name').data('pid');
			var _name = $(elem).find('span.name').text();
			var _penalty = $(elem).find('span.penalty').text();
			var _loan = $(elem).find('span.loan_deduction').text();
			var _advance = $(elem).find('span.advance').text();
			var _incentive = $(elem).find('span.incentive').text();
			var _net_salary = $(elem).find('span.net_salary').text();
			var _salary_account_id= $(elem).find('span.dept_name').data('spid');

			ss.dcno = _dcno;
			ss.etype = _etype;
			ss.dts = _from;
			ss.dte = _to;
			ss.staid = $(elem).find('span.name').data('staid');
			ss.did = $(elem).find('span.dept_name').data('did');
			ss.pid = _pid;
			ss.shid = $(elem).find('span.name').data('shid');
			ss.bsalary = $(elem).find('span.bsalary').text();
			ss.absent = $(elem).find('span.absent').text();
			ss.leave_wp = $(elem).find('span.leave_wp').text();
			ss.leave_wop = $(elem).find('span.leave_wop').text();
			ss.rest_days = $(elem).find('span.rest_days').text();
			ss.work_days = $(elem).find('span.work_days').text();
			ss.paid_days = $(elem).find('span.paid_days').text();
			ss.gross_salary = $(elem).find('span.gross_salary').text();
			ss.otrate = $(elem).find('span.otrate').text();
			ss.othour = $(elem).find('span.othour').text();
			ss.overtime = $(elem).find('span.overtime').text();
			ss.advance = _advance;
			ss.loan_deduction = _loan;
			ss.balance = $(elem).find('span.balance').text();
			ss.incentive = _incentive;
			ss.penalty = _penalty;
			ss.eobi = $(elem).find('span.eobi').text();
			ss.insurance = $(elem).find('span.insurance').text();
			ss.socialsec = $(elem).find('span.socialsec').text();
			ss.net_salary = _net_salary;
			ss.date = _date;
			ss.spid=_salary_account_id == null ? 0: parseInt(_salary_account_id,10);

			///////////////////////////////////////////////////////////////////////////////////////
			// penalty 																			 
			///////////////////////////////////////////////////////////////////////////////////////
			// penalty party -- credit
			if (_penalty != "0.00") {
				pledger = {};
				pledger.pledid = '';
				pledger.pid = _pid;
				pledger.description = 'Penalty deduction from ' + _name;
				pledger.date = _date;
				pledger.debit = 0;
				pledger.credit = _penalty;
				pledger.dcno = _dcno;
				pledger.etype = _etype;
				pledger.deduction = _loan;
				pledger.pid_key = getPartyId('penalty');
				pledgers.push(pledger);
				// penalty itself -- debit
				pledger = {};
				pledger.pledid = '';
				pledger.pid = getPartyId('penalty');
				pledger.description = 'Penalty deduction for the month of ' + _monthName;
				pledger.date = _date;
				pledger.debit = _penalty;
				pledger.credit = 0;
				pledger.dcno = _dcno;
				pledger.etype = _etype;
				pledger.deduction = '0';
				pledger.pid_key = getPartyId('penalty');
				pledgers.push(pledger);
			}

			///////////////////////////////////////////////////////////////////////////////////////
			// loan 																			 
			///////////////////////////////////////////////////////////////////////////////////////
			// loan party -- credit
			if (_loan != "0.00" ) {
				pledger = {};
				pledger.pledid = '';
				pledger.pid = _pid;
				pledger.description = 'Loan deduction from ' + _name;
				pledger.date = _date;
				pledger.debit = 0;
				pledger.credit = _loan;
				pledger.dcno = _dcno;
				pledger.etype = _etype;
				pledger.pid_key = getPartyId('loan');
				pledgers.push(pledger);
				// loan itself -- debit
				pledger = {};
				pledger.pledid = '';
				pledger.pid = getPartyId('loan');
				pledger.description = 'Loan deduction for the month of ' + _monthName;
				pledger.date = _date;
				pledger.debit = _loan;
				pledger.credit = 0;
				pledger.dcno = _dcno;
				pledger.etype = _etype;
				pledger.pid_key = _pid;
				pledgers.push(pledger);
			}

			///////////////////////////////////////////////////////////////////////////////////////
			// advance 																			 
			///////////////////////////////////////////////////////////////////////////////////////
			// advance party -- credit
			if (_advance != "0.00"  ) {
				pledger = {};
				pledger.pledid = '';
				pledger.pid = _pid;
				pledger.description = 'Advance deduction from ' + _name;
				pledger.date = _date;
				pledger.debit = 0;
				pledger.credit = _advance;
				pledger.dcno = _dcno;
				pledger.etype = _etype;
				pledger.pid_key = getPartyId('advance');
				pledgers.push(pledger);
				// advance itself -- debit
				pledger = {};
				pledger.pledid = '';
				pledger.pid = getPartyId('advance');
				pledger.description = 'Advance deduction for the month of ' + _monthName;
				pledger.date = _date;
				pledger.debit = _advance;
				pledger.credit = 0;
				pledger.dcno = _dcno;
				pledger.etype = _etype;
				pledger.pid_key = getPartyId('advance');
				pledgers.push(pledger);
			}

			///////////////////////////////////////////////////////////////////////////////////////
			// incentive 																			 
			///////////////////////////////////////////////////////////////////////////////////////
			// incentive party -- credit
			if (_incentive != "0.00" ) {
				pledger = {};
				pledger.pledid = '';
				pledger.pid = _pid;
				pledger.description = 'Incentive paid to ' + _name;
				pledger.date = _date;
				pledger.debit = _incentive;
				pledger.credit = 0;
				pledger.dcno = _dcno;
				pledger.etype = _etype;
				pledger.pid_key = getPartyId('incentive');
				pledgers.push(pledger);
				// incentive itself -- debit
				pledger = {};
				pledger.pledid = '';
				pledger.pid = getPartyId('incentive');
				pledger.description = 'Incentive paid for the month of ' + _monthName;
				pledger.date = _date;
				pledger.debit = 0;
				pledger.credit = _incentive;
				pledger.dcno = _dcno;
				pledger.etype = _etype;
				pledger.pid_key = getPartyId('incentive');
				pledgers.push(pledger);
			}

			///////////////////////////////////////////////////////////////////////////////////////
			// salary 																			 
			///////////////////////////////////////////////////////////////////////////////////////
			// salary party -- credit
			pledger = {};
			pledger.pledid = '';
			pledger.pid = _pid;
			pledger.description = 'Salary paid to ' + _name;
			pledger.date = _date;
			pledger.debit = '';
			pledger.credit = _net_salary;
			pledger.dcno = _dcno;
			pledger.etype = _etype;
			pledger.pid_key = _salary_account_id == null ? 0: parseInt(_salary_account_id,10);	
			pledgers.push(pledger);
			// salary itself -- debit
			pledger = {};
			pledger.pledid = '';
			pledger.pid = _salary_account_id == null ? 0: parseInt(_salary_account_id,10);//getPartyId('salary');
			pledger.description = 'Salary paid for the month of ' + _monthName;
			pledger.date = _date;
			pledger.debit = _net_salary;
			pledger.credit = 0;
			pledger.dcno = _dcno;
			pledger.etype = _etype;
			pledger.pid_key = getPartyId('salary');
			pledgers.push(pledger);

			salarysheet.push(ss);
		});

		var obj = {};
		obj.p = pledgers;
		obj.s = salarysheet;

		return obj;
	}

	var deleteVoucher = function(dcno, etype) {

		$.ajax({
			url : base_url + 'index.php/staff/deleteSalarySheet',
			type : 'POST',
			data : { 'dcno' : dcno, 'etype' : etype, 'cid': $("#cid").val() },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var printSalarySheet = function() {
		var _width = $(window).width();
		_width = _width - 200;
		var _height = $(window).height();
		_height = _height - 100;
		window.open(base_url + 'application/views/print/salarysheet.php', 'Salarysheet', 'width='+ _width +', height='+_height);
	}

	var printSlips = function () {
		var _width = $(window).width();
		_width = _width - 200;
		var _height = $(window).height();
		_height = _height - 100;
		window.open(base_url + 'application/views/print/salaryslips.php', 'Salaryslips', 'width='+ _width +', height='+_height);
	}

var resettotals = function()
{
			$('#txtBasicSalary').text('');
			$('#txtAbsent').text('');
			$('#txtRestDay').text('');
			$('#txtWorkDay').text('');
			$('#txtPaidDay').text('');
			$('#txtOverTime').text('');
			$('#txtOtAmount').text('');
			$('#txtAdvance').text('');
			$('#txtLoanDeduction').text('');
			$('#txtRemainingBalance').text('');
			$('#txtIncentive').text('');
			$('#txtPenalty').text('');
			$('#txtEobi').text('');
			$('#txtNetSalary').text('');
}

	return {

		init : function() {
			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			// when save button is clicked
			$('.btnSave').on('click', function(e) {
				e.preventDefault();
				if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('updatebtn')==0 ){
					alert('Sorry! you have not update rights..........');
				}else if($('#voucher_type_hidden').val()=='new' && $('.btnSave').data('insertbtn')==0){
					alert('Sorry! you have not insert rights..........');
				}else{
					e.preventDefault();
					self.initSave();
				}
				//self.initSave();
			});

			// when the reset button is clicked
			$('.btnReset').on('click', function(e) {
				e.preventDefault();		// prevent the default behaviour of the link
				//self.resetVoucher();	// resets the voucher
                window.location.reload();
			});

			$('.btnSearch').on('click', function(e) {
				e.preventDefault();		// prevent the default behaviour of the link
				self.initSearch();	// resets the voucher
			});

			$('.btnDelete').on('click', function(e){
				e.preventDefault();

				if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('deletebtn')==0 ){
					alert('Sorry! you have not delete rights..........');
				}else{

					var dcno = $('#txtId').val();
					var etype = 'salary';
					deleteVoucher(dcno, etype);
				}

			});

			$('.btnPrint').on('click', function(e) {
				e.preventDefault();
				//printSalarySheet();
				if ( $('.btnSave').data('printbtn')==0 ){
					
					alert('Sorry! you have not print rights..........');
				}else{

					printSalarySheet();
				}
			});

			$('.btnPrintSlips').on('click', function(e) {
				e.preventDefault();
				printSlips();
			});

			// when text is chenged inside the id textbox
			$('#txtId').on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {

					// get the based on the id entered by the user
					if ( $('#txtId').val().trim() !== "" ) {

						var dcno = $.trim($('#txtId').val());
						fetch(dcno);
					}
				}
			});
			$('#voucher_type_hidden').val('new');

			$('#txtId').on('change', function(e) {
				if ( $('#txtId').val().trim() !== "" ) {
					var dcno = $.trim($('#txtId').val());
					fetch(dcno);
				}
			});



			getMaxId();		// gets the max id of voucher
		},

		initSearch : function() {

			var error = validateSearch();			// checks for the empty fields

			if ( !error ) {
				var from = $('#from_date').val();
				var to = $('#to_date').val();
				var warehouse = $('#warehouse').val();
				//alert(warehouse);
				search( from, to, warehouse );
			} else {
				alert('Correct the errors...');
			}
		},

		// makes the voucher ready to save
		initSave : function() {

			var saveObj = getSaveObject();
			var error = validateSearch();

			if ( !error ) {

				var rows = $('#salary_table tbody tr').length;
				if (rows > 0) {
					var dcno = $('#txtIdHidden').val();
					save( saveObj, dcno );
				} else {
					alert('No data found to save.');
				}
			} else {
				alert('Correct the errors!');
			}
		},
//reset table fields


		// resets the voucher
		resetVoucher : function() {

			$('.inputerror').removeClass('inputerror');
			$('#salary_table').find('tbody tr :not(.dataTables_empty)').remove();
			$('#from_date').val( new Date());
			$('#to_date').val( new Date());
			$('#voucher_type_hidden').val('new');
			getMaxId();		// gets the max id of voucher
			general.setPrivillages();
		}
	};
};

var prepareSalary = new PrepareSalary();
prepareSalary.init();