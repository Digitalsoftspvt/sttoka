 Purchase = function() {

    var fetchVouchers = function (till,dept_id,type, cat_id, subcat_id) {

        if (typeof purchase.dTable != 'undefined') {
            purchase.dTable.fnDestroy();
            $('#datatable_example thead').remove();
            $('#tbl_body').empty();
        }
        $('.grand-total').html('0.00');
        
        $.ajax({
                url: base_url + "index.php/report/fetchmonthlysalecomparison",
                data: {'till' : till, 'dept_id' : dept_id, 'type' : type, 'cat_id': cat_id, 'subcat_id': subcat_id},
                type: 'POST',
                dataType: 'JSON',
                beforeSend: function () {
                    console.log(this.data);
                 },
                 complete: function () { },
                success: function (data) {
                    console.log(data);
                    if (data.length === 0)
                    {
                        alert("No Data Found In This Duration..");

                    }
                    else  {
                        var htmls = '';
                        var grandTotal = 0;
                        var prevVoucher = '';
                        var prevVoucherMatch = '';
                        var Columns = [];
                         var edate  = ($('#to_date').val());
                        var TableHeader = "<thead class='dthead'><tr><th colspan='1' class='text-left'></th><th colspan='3' class='text-center'>Dealer Sales</th><th colspan='3' class='text-center'>Counter Sales</th><th colspan='3' class='text-center'>Sale Return</th><th colspan='3' class='text-center'>Cash Sale Return</th><th colspan='3' class='text-center'>Net Sale</th><th colspan='4' class='text-center'>INC/DEC</th></tr><tr>";
                        const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                        $.each(data[0], function (key, value) {
                            Columns.push({ "data": key })
                            if (key  != 'catid' && key  != 'subcatid' && key != 'category') {
                                var datestr = new Date(edate);
                                var lastyear=(datestr.getFullYear());
                                var lastmonth = monthNames[(datestr.getMonth())];
                                if (monthNames[(datestr.getMonth())]=='Jan')
                                {
                                    lastyear=(datestr.getFullYear()-1);
                                }
                                if (monthNames[(datestr.getMonth())]=='Jan')
                                {
                                    lastmonth='Dec';
                                }
                                else
                               {
                               lastmonth=monthNames[(datestr.getMonth()-1)];
                               }
                                if( key=='prevmonthsale' || key == 'prevmonthcsale' ){
                                    key=lastmonth + "-" + lastyear;                               
                                }else if(key=='thismonthsale' || key == 'thismonthcsale' ){
                                    key=monthNames[(datestr.getMonth())] + "-" + (datestr.getFullYear()) ;
                                }else if(key=='prevyear' || key=='prevyearsale' || key=='prevyearcsale'){
                                    key= monthNames[(datestr.getMonth())] + "-" +(datestr.getFullYear()-1);
                                }else if (key == 'prevmonthsr' || key=='prevmonthcsr'){
                                    key= lastmonth + "-" + lastyear ;
                                }else if (key == 'thismonthsr' || key == 'thismonthcsr' ){
                                    key= monthNames[(datestr.getMonth())] + "-" + (datestr.getFullYear()) ;
                                }else if (key  == 'prevyearsr' || key  == 'prevyearcsr' || key  == 'prevyearsale' || key  == 'prevyearcsale' ){
                                    key=monthNames[(datestr.getMonth())] + "-" + (datestr.getFullYear()-1);
                                }else if (key == 'netsaleprevmonth'){
                                    key= lastmonth + "-" + lastyear ;
                                }else if (key == 'netsalereturnprevmonth'){
                                    key= monthNames[(datestr.getMonth())]+ "-" + (datestr.getFullYear()) ;
                                }else if (key == 'netsalereturnprevyear'){
                                    key=  monthNames[(datestr.getMonth())]+ "-" +(datestr.getFullYear()-1);
                                }else if (key == 'subcatgory'){
                                    key= 'Category';
                                }
                                if (key=='Category')
                                {
                                TableHeader += "<th class='text-left'>" + key + "</th>"    
                                }
                                else
                                {
                                TableHeader += "<th class='text-right'>" + key + "</th>"
                                }
                            }
                        });
                    TableHeader += "<th>Prev Month</th> <th>%</th> <th>Prev Year</th> <th>%</th> ";
                    TableHeader += "</tr></thead>";
                    $("#datatable_example").append(TableHeader);
                    $('#datatable_example').css("background-color", "#BABABA");
                    
                    var Columns = [];
                    var count = 0;
                    var lastmonthsalesum=0; var currentmonthsalesum=0; var lastyearsalesum=0;
                    var lastmonthreturnsum=0;var currentmonthreturnsum=0;var lastyearreturnsum=0;
                    var lastmonthnetsum=0; var currentmonthnetsum=0;var lastyearnetsum=0;
                    for(var i=0;i<data.length;i++){
                        var TableHeader2 = "<tr>";
                        var x = 0;
                        var lmonth=0;
                        var cmonth=0;
                        var curmonth=0;
                     
                        $.each(data[i], function (key, value) {
                             if(key=='category' && prevVoucherMatch != value){
                                prevVoucherMatch=value;
                            }
                            
                            
                           
                            if (prevVoucher != prevVoucherMatch) {
                                // if (count>0)
                                // {
                                //     TableHeader3 = "<tr class='finalsum text-right'>" + "<td class='txtbold subtotal'>Sub Total:</td><td class='txtbold subtotal'>" + parseFloat(lastmonthsalesum).toFixed(0) +  "</td><td class='txtbold subtotal '>" + parseFloat(currentmonthsalesum).toFixed(0) +  "</td><td class='txtbold subtotal'>" + parseFloat(lastyearsalesum).toFixed(0) +  "</td><td class='txtbold subtotal'>" + parseFloat(lastmonthreturnsum).toFixed(0) +  "</td><td class='txtbold subtotal'>" + parseFloat(currentmonthreturnsum).toFixed(0) +  "</td><td class='txtbold subtotal'>" + parseFloat(lastyearreturnsum).toFixed(0) +  "</td><td class='txtbold subtotal'>" + parseFloat(lastmonthnetsum).toFixed(0) +  "</td><td class='txtbold subtotal'>" + parseFloat(currentmonthnetsum).toFixed(0) +  "</td><td class='txtbold subtotal'>" + parseFloat(lastyearnetsum).toFixed(0) +  "</td><td class='txtbold subtotal'>"+  (parseFloat(currentmonthnetsum).toFixed(0)- parseFloat(lastmonthnetsum).toFixed(0) ) +"</td><td class='txtbold subtotal'> "+  parseFloat((((parseFloat(currentmonthnetsum).toFixed(0)- parseFloat(lastmonthnetsum).toFixed(0)) / parseFloat(currentmonthnetsum).toFixed(0) ) *100)).toFixed(2) +"</td><td class='txtbold subtotal'>"+  (parseFloat(currentmonthnetsum).toFixed(0)- parseFloat(lastyearnetsum).toFixed(0)) +"</td><td class='txtbold subtotal'> "+  parseFloat((((parseFloat(currentmonthnetsum).toFixed(0)- parseFloat(lastyearnetsum).toFixed(0)) / parseFloat(currentmonthnetsum).toFixed(0) ) *100)).toFixed(2) +" </td>" + "</tr>";
                                //      $('#tbl_body').append(TableHeader3);
                                //      $('#tbl_body').css("background-color", "white");
                                //      lastmonthsalesum=0;
                                //      currentmonthsalesum=0;
                                //      lastyearsalesum=0;
                                //      lastmonthreturnsum=0;
                                //      currentmonthreturnsum=0;
                                //      lastyearreturnsum=0;
                                //      lastmonthnetsum=0;
                                //      currentmonthnetsum=0;
                                //      lastyearnetsum=0;

                                // }

                                 TableHeader2 = "<tr class='hightlight_tr'>" + "<td colspan='3'>" + prevVoucherMatch +  " </td><td colspan='6'></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td class='hide'></td><td class='hide'></td>" + "</tr>";
                                prevVoucher = prevVoucherMatch;
                                
                            }
                            

                        if (key  != 'catid' && key  != 'subcatid' && key != 'category'){
                          if (key != 'category'){
                              if (key == 'subcatgory')
                              {
                                TableHeader2 += "<td class='text-left'>" + value + "</td>";      
                              }
                              else
                              {
                                  TableHeader2 += "<td class='text-right'>" + parseFloat(value).toFixed(0); + "</td>";
                              }
                            
                            }
                            //Sales Sum
                            if (key == 'prevmonth'){
                            lastmonthsalesum = getVal(lastmonthsalesum) + getVal(value);
                             count+=1;
                            }
                            if (key == 'thismonth'){
                            currentmonthsalesum = getVal(currentmonthsalesum)+getVal(value);
                            }
                            if (key == 'prevyear'){
                            lastyearsalesum = (getVal(lastyearsalesum)) + (getVal(value));
                            }

                            //Sale Return Sum
                            if (key == 'prevmonthsr'){
                            lastmonthreturnsum = getVal(lastmonthreturnsum) + getVal(value);
                            }
                            if (key == 'thismonthsr'){
                            currentmonthreturnsum = getVal(currentmonthreturnsum)+getVal(value);
                            }
                            if (key == 'prevyearsr'){
                            lastyearreturnsum = getVal(lastyearreturnsum)+ getVal(value);
                            }
                            //Net Sales
                            if (key == 'netsaleprevmonth'){
                            lmonth=parseFloat(value).toFixed(0);
                            lastmonthnetsum = getVal(lastmonthnetsum) + getVal(value);
                            }

                            if (key == 'netsalereturnprevmonth'){
                            curmonth=parseFloat(value).toFixed(0);
                            currentmonthnetsum = getVal(currentmonthnetsum)+getVal(value);
                            }

                            if (key == 'netsalereturnprevyear'){
                            cmonth=parseFloat(value).toFixed(0);
                            lastyearnetsum = getVal(lastyearnetsum)+getVal(value);
                            }

                            x+=1;
                        }
                        });
                        var no = curmonth - lmonth;
                        var prev = curmonth - cmonth;
                        if (cmonth==0)
                        {
                            cmonth=curmonth;
                        }
                        if (lmonth==0)
                        {
                            lmonth=curmonth;
                        }
                        TableHeader2 += "<td class='text-right'>"+ no +"</td> <td class='text-right'>"+ getVal((no/lmonth)*100,2).toFixed(2) +"</td> <td class='text-right'>"+ prev +"</td> <td class='text-right'>"+ getVal((prev/cmonth)*100,2).toFixed(2) +"</td> ";
                        TableHeader2 += "</tr>";
                        $('#tbl_body').append(TableHeader2);
                        $('#tbl_body').css("background-color", "white");

                       
                    }
                
                       if (count>0)
                                {
                                    TableHeader3 = "<tr class='finalsum text-right'>" + "<td class='txtbold subtotal'>Sub Total:</td><td class='subtotal'>" + parseFloat(lastmonthsalesum).toFixed(0) +  "</td><td class='txtbold subtotal'>" + parseFloat(currentmonthsalesum).toFixed(0) +  "</td><td class='txtbold subtotal'>" + parseFloat(lastyearsalesum).toFixed(0) +  "</td><td class='txtbold subtotal'>" + parseFloat(lastmonthreturnsum).toFixed(0) +  "</td><td class='txtbold subtotal'>" + parseFloat(currentmonthreturnsum).toFixed(0) +  "</td><td class='txtbold subtotal'>" + parseFloat(lastyearreturnsum).toFixed(0) +  "</td><td class='txtbold subtotal'>" + parseFloat(lastmonthnetsum).toFixed(0) +  "</td><td class='txtbold subtotal'>" + parseFloat(currentmonthnetsum).toFixed(0) +  "</td><td class='txtbold subtotal'>" + parseFloat(lastyearnetsum).toFixed(0) +  "</td class='subtotal'>"+  (parseFloat(currentmonthnetsum).toFixed(0)- parseFloat(lastmonthnetsum).toFixed(0) ) +"<td class='txtbold subtotal'>"+  (parseFloat(currentmonthnetsum).toFixed(0)- parseFloat(lastmonthnetsum).toFixed(0)) +"</td><td class='txtbold subtotal'> "+  parseFloat((((parseFloat(currentmonthnetsum).toFixed(0)- parseFloat(lastmonthnetsum).toFixed(0)) / parseFloat(currentmonthnetsum).toFixed(0) ) *100)).toFixed(2) +"</td><td class='txtbold subtotal'>"+  (parseFloat(currentmonthnetsum).toFixed(0)- parseFloat(lastyearnetsum).toFixed(0)) +"</td><td class='txtbold subtotal'> "+  parseFloat((((parseFloat(currentmonthnetsum).toFixed(0)- parseFloat(lastyearnetsum).toFixed(0)) / parseFloat(currentmonthnetsum).toFixed(0) ) *100)).toFixed(2) +" </td>" + "</tr>";
                                     $('#tbl_body').append(TableHeader3);
                                     $('#tbl_body').css("background-color", "white");
                                }
                    var y=1;
                    
                    var TableHeader2="";
                    var first = 0 ;
                    var second = 0;
                    TableHeader2 = "<tr  class='text-right txtbold'><td>Grand Total</td>";
                    while(y<=x-1)
                    {
                        var total1 = 0;
                        var total2 = 0;
                    
                    $('#datatable_example').find('tbody tr').each(function( index, elem ) {
                        if(($(elem).find("td").eq(y)).hasClass('subtotal')){
                            total1 = getVal(total1) + parseFloat(0);
                        }else{
                        var column=$.trim($(elem).find("td").eq(y).html());
                        values=column.split('<br>');
                        first =values[0];
                        total1 = getVal(total1) + getVal(first);
                    }
                    });
                    y+=1;
                    TableHeader2 += "<td style = 'font-weight:bold !important'>" + parseFloat(total1).toFixed(0) +"</td>";
                    
                    }
                    TableHeader2 += "<td></td><td></td><td></td><td></td></tr>";
                    $('#tbl_body').append(TableHeader2);
                    $('#tbl_body').css("background-color", "white");

                  bindGrid();  
              }
                },
                error: function (result) {
                    $("#loading").hide();
                    alert("Error:" + result);
                }
            });   

    }

    var getVal = function (val) {
        return isNaN(parseFloat(val)) ? 0 : parseFloat(val);
    }
   
   
    var resetd = function () {
            $('#myfirstlinechart').html('');
            $('#myfirstareachart').html('');
            $('#myfirstbarchart').html('');
            $('#myfirstdonutchart').html('');
        
    }
     var getcrit = function (etype){
        var accid=$("#drpAccountID").select2("val");
        var itemid=$('#drpitemID').select2("val");
        var departid=$('#drpdepartId').select2("val");
        var userid=$('#drpuserId').select2("val");
        var customer_name=$('#drpCustomerName').select2("val");
        if(etype=='saleorder' ){
        var status=$('#drpstatusId').select2("val");
        }
        if(etype=='purchaseorder'){
            var status='';
        }
        // Items
        var brandid=$("#drpbrandID").select2("val");
        var catid=$('#drpCatogeoryid').select2("val");
        var subCatid=$('#drpSubCat').select2("val");
        var txtUom=$('#drpUom').select2("val");
        // End Items
        // Account
        // var txtCity=$("#drpCity").select2("val");
        // var txtCityArea=$('#drpCityArea').select2("val");
        var l1id=$('#drpl1Id').select2("val");
        var l2id=$('#drpl2Id').select2("val");
        var l3id=$('#drpl3Id').select2("val");
        var area=$('#drpArea').select2("val");
        var city=$('#drpCity').select2("val");
        var region=$('#drpRegion').select2("val");
        var province=$('#drpProvince').select2("val");
        // End Account
        var company_id = $('#cid').val();
        // var userid=$('#user_namereps').select2("val");
        // alert(userid);
        var crit ='';
        // if (etype === 'saleorder' || etype === 'purchaseorder' || etype === 'sale') {
        if (etype === 'saleorder' || etype === 'purchaseorder') {
            if (accid!=''){
                crit +='AND ordermain.party_id in (' + accid +') ';
            }
            if (status!=''){
                crit +='AND ordermain.status in (' + status +') ';
            }
            if (itemid!='') {
                crit +='AND orderdetail.item_id in (' + itemid +') '
            }
            // if (customer_name!='') {
            //     // customer_name=Array.isArray(customer_name)? customer_name.toString() : customer_name ;
            //     // console.log(customer_name);
            //     crit +='AND ordermain.customer_name in (' + customer_name +') '
            // }
            
            if (departid!='') {
                crit +='AND orderdetail.godown_id in (' + departid +') ';
            }
            if (userid!='') {
                crit +='AND ordermain.uid in (' + userid+ ') ';
            }
            if (area!='') {
                crit +='AND area.area_id in (' + area + ') ';
            }
            if (city!='') {
                crit +='AND city.cid in (' + cid+ ') ';
            }
            if (province!='') {
                crit +='AND province.pvcid in (' + province+ ') ';
            }
            if (region!='') {
                crit +='AND region.rid in (' + region+ ') ';
            }
            // Items
            if (brandid!=''){
                crit +='AND item.bid in (' + brandid +') ';
            }
            if (catid!='') {
                crit +='AND item.catid in (' + catid +') '
            }
            if (subCatid!='') {
                crit +='AND item.subcatid in (' + subCatid +') ';
            }
            if (txtUom!='') {
                // alert('"'+txtUom+'"'); 

                var qry = "";
                $.each(txtUom,function(number){
                     qry +=  "'" + txtUom[number] + "',";
                });
                qry = qry.slice(0,-1);
                // alert(qry);
                crit +='AND item.uom in (' + qry+ ') ';
            }
            // End Items
           

            // Account
            if (area!='') {
                var qry = "";
                $.each(area,function(number){
                     qry +=  "'" + area[number] + "',";
                });
                qry = qry.slice(0,-1);
                crit +='AND area.area_id in (' + qry +') '
            }
            if (city!='') {
                var qry = "";
                $.each(city,function(number){
                     qry +=  "'" + city[number] + "',";
                });
                qry = qry.slice(0,-1);
                crit +='AND city.cid in (' + qry +') '
            }
            if (region!='') {
                var qry = "";
                $.each(region,function(number){
                     qry +=  "'" + region[number] + "',";
                });
                qry = qry.slice(0,-1);
                crit +='AND region.rid in (' + qry +') '
            }
            if (province!='') {
                var qry = "";
                $.each(province,function(number){
                     qry +=  "'" + province[number] + "',";
                });
                qry = qry.slice(0,-1);
                crit +='AND province.pvcid in (' + qry +') '
            }
            if (l1id!='') {
                crit +='AND leveltbl1.l1 in (' + l1id +') ';
            }
            if (l2id!='') {
                crit +='AND leveltbl2.l2 in (' + l2id+ ') ';
            }
            if (l3id!='') {
                crit +='AND party.level3 in (' + l3id+ ') ';
            }
            if($('#Radios1').is(':checked')){
                crit +="AND ordermain.vrnoa not in (select order_vrno from stockmain where etype='outwardvoucher' AND order_vrno <> 0 AND company_id ="+ company_id +")";
            }
            if($('#Radios2').is(':checked')){
                crit +="AND ordermain.vrnoa in (select order_vrno from stockmain where etype='outwardvoucher' AND order_vrno <> 0 AND company_id ="+ company_id +")";
            }

            crit += 'AND ordermain.oid <>0 ';
        }else{
            if (accid!=''){
                crit +='AND ordermain.party_id in (' + accid +') ';
            }
            if (itemid!='') {
                crit +='AND orderdetail.item_id in (' + itemid +') '
            }
            if (departid!='') {
                crit +='AND orderdetail.godown_id in (' + departid +') ';
            }
            if (customer_name !='') {

                var qry=''
                $.each(customer_name,function(number){
                     qry +=  "'" + customer_name[number] + "',";
                });
                qry = qry.slice(0,-1);
                // customer_name=Array.isArray(customer_name)? customer_name.toString().join() : customer_name ;
                // console.log(customer_name);
                crit +='AND ordermain.customer_name in (' + qry + ') '
                // console.log(crit)
            }
            if (userid!='') {
                crit +='AND ordermain.uid in (' + userid+ ') ';
            }
            // Items
            if (brandid!=''){
                crit +='AND item.bid in (' + brandid +') ';
            }
            if (catid!='') {
                crit +='AND item.catid in (' + catid +') '
            }
            if (subCatid!='') {
                crit +='AND item.subcatid in (' + subCatid +') ';
            }
            if (txtUom!='') {
                // alert('"'+txtUom+'"'); 

                var qry = "";
                $.each(txtUom,function(number){
                     qry +=  "'" + txtUom[number] + "',";
                });
                qry = qry.slice(0,-1);
                // alert(qry);
                crit +='AND item.uom in (' + qry+ ') ';
            }
            // End Items

            // Account
                        if (area!='') {
                var qry = "";
                $.each(area,function(number){
                     qry +=  "'" + area[number] + "',";
                });
                qry = qry.slice(0,-1);
                crit +='AND area.area_id in (' + qry +') '
            }
            if (city!='') {
                var qry = "";
                $.each(city,function(number){
                     qry +=  "'" + city[number] + "',";
                });
                qry = qry.slice(0,-1);
                crit +='AND city.cid in (' + qry +') '
            }
            if (region!='') {
                var qry = "";
                $.each(region,function(number){
                     qry +=  "'" + region[number] + "',";
                });
                qry = qry.slice(0,-1);
                crit +='AND region.rid in (' + qry +') '
            }
            if (province!='') {
                var qry = "";
                $.each(province,function(number){
                     qry +=  "'" + province[number] + "',";
                });
                qry = qry.slice(0,-1);
                crit +='AND province.pvcid in (' + qry +') '
            }
            if (l1id!='') {
                crit +='AND leveltbl1.l1 in (' + l1id +') ';
            }
            if (l2id!='') {
                crit +='AND leveltbl2.l2 in (' + l2id+ ') ';
            }
            if (l3id!='') {
                crit +='AND party.level3 in (' + l3id+ ') ';
            }
            //End Account


            crit += 'AND ordermain.oid <>0 ';
            // alert(crit);
        }
        return crit;

   }
    var create_linechart = function (data) {
    
        Morris.Line({

            element:'myfirstlinechart',
            data:data,

            xkey:'voucher',

            ykeys:['qty','amount'],
            parseTime: false,
            labels:['Quantity','Amount']

        });
    }
    var create_areachart = function (data) {
        Morris.Area({

                    element:'myfirstareachart',
                    data:data,


                    xkey: 'voucher',
                    ykeys: ['qty','amount'],
                    parseTime: false,

                    labels: ['Quantity','Amount']

                });
    }
    var create_donutchart = function (data){
            Morris.Donut({

                    element:'myfirstdonutchart',
                    data:data

                });
    }
    var create_barchart  = function (data) {
        Morris.Bar({

            element:'myfirstbarchart',
            data:data,


            xkey: 'voucher',
            ykeys: ['qty','amount'],

            labels: ['Quantity','Amount']

        });
    }

 var toCapitalize = function(str){
        return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    }
var getTabLength = function() {
        return $('#datatable_example thead tr th').length;
    }

    var bindGrid = function() {
        var oldTabLength = getTabLength();
        var etype = $('.page_title').text();
        var what = getCurrentView();
        var msg = "<b>From </b>" +  $('#from_date').val() + " :- <b>To</b> To:- " + $('#to_date').val() + ", " + toCapitalize(what) + " Wise";
        var dontSort = [];
        $('#datatable_example thead th').each(function () {
            if ($(this).hasClass('no_sort')) {
                dontSort.push({ "bSortable": false });
            } else {
                dontSort.push(null);
            }
        });

        purchase.dTable = $('#datatable_example').dataTable({
            // Uncomment, if prolems with datatable.
            // "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone'f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            // "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone'<'row-fluid'<'span8' f>>>'<'pag_top' p> T>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "sDom": '<if<t>lp>',//                 fdiptslp',
            "aaSorting": [[0, "asc"]],
            "bPaginate": true,
            'bFilter': true,
            "sPaginationType": "full_numbers",
            "bJQueryUI": false,
            "aoColumns": dontSort,
            "bSort": false,
            "iDisplayLength" : 150,
            // "bInfo": true,
            // Buttons Plugin for Print in Datatable
            buttons: [
                { 
                    extend: 'print', 
                    text : '<i class="fa fa-print"></i> Print',
                    className: 'btn btn-primary',
                    title : etype,
                    message  : msg,
                    exportOptions: {
                        columns: ':visible',
                        search: 'applied',
                        order: 'applied'
                    },
                    customize: function (win) {
                        var table = $(win.document.body).find('table');
                        // var footer = '<div class="row-fluid"><div class="span12"><table class="signature-fields" styles="border: none; border-spacing: 20px; border-collapse: separate;"><thead><tr><th style="border: 0px; border-top: 1px solid black; border-spacing: 10px;border-top: 1px solid black; border-left: 1px solid white; border-right: 1px solid white; border-bottom: 1px solid white;">Prepared By</th><th style="border: 0px; border-top: 1px solid black; border-spacing: 10px;border:1px solid white;"></th><th style="border-top: 1px solid black; border-left: 1px solid white; border-right: 1px solid white; border-bottom: 1px solid white;">Received By</th></tr></thead></table></div></div>';
                        var oldTab = $('#datatable_example').html();
                        $(table).html(oldTab);
                        // $(table).append(footer);
                        $(win.document.head).append("<style> @page {margin : 10mm 5mm }  @media print{table thead tr th{padding:5px !important; background-color: #999; border: none !important} table tr td{border: none !important;padding:3px !important;} .signature-fields{ border: none; border-spacing: 20px; border-collapse: separate;} .signature-fields th {border: 0px; border-top: 1px solid black; border-spacing: 10px; }</style>");
                        $(win.document.body).find('table').addClass('display').css('font-size', '12px');
                        $(win.document.body).find('table thead tr th').each(function(index, elem){
                            var txt = $.trim($(this).text()).toLowerCase();
                            if(txt == 'qty' || txt == 'rate' || txt == 'gross amount' || txt == 'disc%' || txt == 'disc' || txt =='amount excl tax' || txt =='tax%' || txt =='tax' || txt == 'amount incl tax'){
                                $(this).css({'border': '1px solid #777', 'text-align' : 'right'});
                            }
                            $(this).css({'border': '1px solid #777', 'text-align' : 'left'});
                            
                        });
                                                
                        $(win.document.body).find('table tbody tr td').each(function(index){

                            if(!isNaN(parseFloat($(this).text()))) {
                                $(this).css({'border':'1px solid #777', 'text-align' : 'right'});
                            }
                           $(this).css('border','1px solid #777');
                           // $(this).closest('tr').find('td:eq(0)').css({'border':'1px solid #777', 'text-align' : 'left !important'});
                           var ind = $(this).index();
                           var colName = $.trim($(this).parentsUntil("table thead").find('tr th:eq('+ind+')').text()).toLowerCase();
                           // console.log(colName);
                           if(colName === 'date' || colName == 'vr#' || colName == 'sr#' || colName == 'account' || colName=='item'){
                                console.log(colName);
                                $(this).css({'border':'1px solid #777', 'text-align' : 'left !important'});
                           }
                        });
                        
                        var ind = 1;
                        $(win.document.body).find('table tbody tr').each(function(i){
                            if($.trim($(this).find('td:first').text()).length == 0 ){
                                if(ind % 2 != 0) {
                                    $(this).find('td').css({'font-weight' : 'bold','background': '#e6e6e6'});
                                } else {
                                    $(this).find('td').css({'font-weight' : 'bold','background': 'silver'});
                                }
                                ind = ind+1;
                            }                          
                        });
                        // $(win.document.body).find('table tbody tr:last td').addClass('text-right').css({'font-weight': 'bold','background':'silver'});
                        $(win.document.body).find('table tr.odd').removeClass('odd');
                        $(win.document.body).find('table tr.even').removeClass('even');
                        $(win.document.body).find('h1').css('text-align','center');
                        $(win.document.body).find('div').css({'text-align' : 'center', 'margin-top' : '20px'});
                        // console.log(win.document);
                    }
                },
                { extend: 'copy', text : '<i class="fa fa-copy"></i> Copy', className: 'btn btn-default',exportOptions: {
                        columns: ':visible',
                        search: 'applied',
                        order: 'applied'
                    }, },
                { 
                    extend: 'pdfHtml5', 
                    text : '<i class="fa fa-file"></i> PDF',
                    className: 'btn btn-danger',
                    // download: 'open',
                    orientation : 'portrait',
                    pageSize : 'A4',
                    title : etype,
                    message  : "From :-" +  $('#from_date').val() + " :- To:- " + $('#to_date').val() + ", " + toCapitalize(what) + " Wise",
                    exportOptions: {
                        columns: ':visible',
                        search: 'applied',
                        order: 'applied'
                    },
                    customize: function ( doc ) {
                        console.log(doc);
                        var newLen = getTabLength();
                        // console.log(newLen);
                        if(newLen < 10) {
                            doc.content[2].table.widths = Array(doc.content[2].table.body[0].length + 1).join('*').split('');
                        }
                        doc.pageMargins = [10, 30, 10,10 ];
                        doc.styles.hightlight_tr = {'fillColor' : '#cccccc',  'bold': true,'alignment': 'left'};
                        doc.styles.hightlight_trn = {'fillColor' : '#999999',  'bold': true, 'alignment': 'right'};
                        doc.styles.head_right_aligned = {bold: true, fontSize: 11, color: "white", fillColor: "#666666", alignment: "right"};
                        doc.styles.centered = {alignment: 'right'};
                        // doc.styles.hightlight_trn = {'fillColor' : 'lightgrey',  'bold': true};
                        doc.styles.tableHeader = {bold: true, fontSize: 11, color: "white", fillColor: "#666666", alignment: "left"};

                        // console.log(doc.content[2]['table']['body'][0]);
                        var ind = 1;
                        $.each(doc.content[2]['table']['body'],function(index, value){
                            
                            if(index == 0) {
                                for(var i =0 ; i < newLen; i++){
                                    var txt = $.trim(value[i]['text'].toLowerCase());
                                    if(txt == 'qty' || txt == 'rate' || txt == 'gross amount' || txt == 'disc%' || txt == 'disc' || txt =='amount excl tax' || txt =='tax%' || txt =='tax' || txt == 'amount incl tax'){
                                        value[i]['style'] = 'head_right_aligned';
                                    }
                                }
                                return true;
                            }

                            for(var y = 0; y < this.length; y++){
                                // console.log(this);
                                if(!isNaN(parseFloat(value[y]['text'])) )
                                    value[y]['style'] = 'centered';
                            }                                
                            
                            if(this[0]['text'] == ""){
                                $.each(this, function(index, val){
                                    if(ind % 2 != 0) {
                                        value[index]['style'] = 'hightlight_tr';
                                    }else {
                                        value[index]['style'] = 'hightlight_trn';
                                    }
                                }); 
                                ind++;                               
                            }
                            if(doc.content[2]['table']['body'].length - 2 == index || doc.content[2]['table']['body'].length - 1 == index){
                                $.each(this, function(index, val){
                                    // console.log(val);
                                    this['style'] = 'hightlight_trn';
                                });
                                
                            }

                            
                        });
                        // Splice the image in after the header, but before the table
                        doc.styles.title = { color: 'red', fontSize: '20', alignment: 'center' };
                        doc.styles.message = { fontSize: '8', alignment: 'center' };
                        doc.defaultStyle = {'fontSize' : 7};
                        doc.styles.tableBodyOdd = {'fillColor' : '#ffffff', 'border': [true, true, true, true],};
                    },
                    // download : 'open',

                },
                { 
                    extend: 'excel', text :'<i class="fa fa-file-text-o"></i> Excel', className: 'btn btn-warning',
                    title : etype,
                    customize: function(xlsx) {
                        console.log(xlsx);
                        var sheet = xlsx.xl.worksheets['sheet1.xml'];
         
                        // Loop over the cells in column `C`
                        $('row c[r^="C"]', sheet).each( function () {
                            // Get the value
                            if ( $('is t', this).text() == 'New York' ) {
                                $(this).attr( 's', '20' );
                            }
                        });
                    }
                },
                { extend: 'csv', text :'<i class="fa fa-file-text"></i> CSV', className: 'btn btn-info' },
                { extend:  'colvis', text :'<i class="fa fa-eye"></i> Columns Visible', className: 'btn btn-danger', columns: ':not(:first-child)',},
                //  Creating Custom Button
                //  {
                //     text: 'Reload',
                //     className: 'btn default',
                //     action: function ( e, dt, node, config ) {
                //         //dt.ajax.reload();
                //         alert('Custom Button');
                //     }
                // }
            ],
            "oTableTools": {
                "sSwfPath": "js/copy_cvs_xls_pdf.swf",
                "aButtons": [{ "sExtends": "print", "sButtonText": "Print Report", "sMessage" : "Inventory Report" }]
            },
            // "lengthMenu": [[100, 250, 500, -1], [100, 200, 100, "All"]],
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

        });
        $('.nav-light > a.tool-action').off('click').on('click', function(e, index) {     
            var action = $(this).attr('data-action');
             purchase.dTable.DataTable().button(action).trigger('click');
        });
        $('.buttons-colvis').on('click', function(e){
            $('.dt-button-collection').css({'display': 'block','top': '499px','left': '609.203px'});
            $('.div.dt-button-collection').css('width', '161px');
        });
        $.extend($.fn.dataTableExt.oStdClasses, {
            "s`": "dataTables_wrapper form-inline",
        });
    }
    var Print_Voucher = function( ) {
        var from = ($('#from_date').val());
        var to = ($('#to_date').val());
        // My New 
            var from = ($('#from_date').val());
            var to = ($('#to_date').val());
            var what = getCurrentView();
            var type = ($('#Radio1').is(':checked') ? 'detailed' : 'summary');     // if true means detailed view if false sumamry view
            // alert(type;)
            var etype=($('#etype').val().trim()).toLowerCase();
            etype=etype.replace(' ','');
            if(etype==='cashsale') etype='cs';
            // alert(etype);
            var crit=getcrit(etype);
            // alert(crit);
            var orderBy = '';
            var groupBy = '';
            var field = '';
            var name = '';
            if (what === 'voucher') {
                field =   'ordermain.VRNOA';
                orderBy = 'ordermain.VRNOA';
                groupBy = 'ordermain.VRNOA';
                name    = 'party.NAME';
            }else if (what === 'account') {
                field =   'party.NAME';
                orderBy = 'party.NAME';
                groupBy = 'party.NAME';
                name    = 'party.NAME';
            }else if (what === 'godown') {
                field =   'dept.NAME';
                orderBy = 'dept.NAME';
                groupBy = 'dept.NAME';
                name = ' dept.name AS NAME';
            }else if (what === 'item') {
                field =   'item.item_des';
                orderBy = 'item.item_des';
                groupBy = 'item.item_des';
                if (type === 'detailed') {
                    name = 'party.NAME';
                }else{
                    name = 'item.item_des as NAME';
                }

            }else if (what === 'date') {
                field =   'date(ordermain.VRDATE)';
                orderBy = 'date(ordermain.VRDATE)';
                groupBy = 'date(ordermain.VRDATE)';
                name = 'party.NAME';
            }else if (what === 'year') {
                field =   'year(vrdate)';
                orderBy = 'year(vrdate)';
                groupBy = 'year(vrdate)';
                name    = 'party.NAME';
            }else if (what === 'month') {
                field =   'month(vrdate) ';
                orderBy = 'month(vrdate)';
                groupBy = 'month(vrdate)';
                name    = 'party.NAME';
            }else if (what === 'weekday') {
                field =   'DAYNAME(vrdate)';
                orderBy = 'DAYNAME(vrdate)';
                groupBy = 'DAYNAME(vrdate)';
                name    = 'party.NAME';
            }else if (what === 'user') {
                field =   'user.uname ';
                orderBy = 'user.uname';
                groupBy = 'user.uname';
                name    = 'party.NAME';
            }if (what === 'rate') {
                field =   'orderdetail.rate';
                orderBy = 'orderdetail.rate';
                groupBy = 'orderdetail.rate';
                name    = 'party.NAME';
            }
            if(etype=='purchaseorder' || etype=='saleorder' || etype=='sale'|| etype=='cs') {
                
                 // End of Etype if-else
                         if (what === 'voucher') {
                             field =   'ordermain.VRNOA';
                             orderBy = 'ordermain.VRNOA';
                             groupBy = 'ordermain.VRNOA';
                             name    = 'party.NAME';
                         }else if (what === 'account') {
                             field =   'party.NAME';
                             orderBy = 'ordermain.party_id';
                             groupBy = 'ordermain.party_id';
                             name    = 'party.NAME';
                         }else if (what === 'godown') {
                             field =   'dept.NAME';
                             orderBy = 'orderdetail.godown_id';
                             groupBy = 'orderdetail.godown_id';
                             name = ' dept.name AS NAME';
                         }else if (what === 'item') {
                             field =   'item.item_des';
                             orderBy = 'orderdetail.item_id';
                             groupBy = 'orderdetail.item_id';
                             if (type === 'detailed') {
                                 name = 'party.NAME';
                             }else{
                                 name = 'item.item_des as NAME';
                             }
                         }else if (what === 'date') {
                             field =   'date(ordermain.VRDATE)';
                             orderBy = 'ordermain.vrdate';
                             groupBy = 'ordermain.vrdate';
                             name = 'party.NAME';
                         }else if (what === 'year') {
                            field =   'year(vrdate)';
                            orderBy = 'ordermain.VRNOA';
                            groupBy = 'ordermain.VRNOA';
                            name = 'party.NAME';
                        }else if (what === 'month') {
                            field =   'month(vrdate) ';
                            orderBy = 'ordermain.VRNOA';
                            groupBy = 'ordermain.VRNOA';
                            name = 'party.NAME';
                        }else if (what === 'weekday') {
                            field =   'DAYNAME(vrdate)';
                            orderBy = 'ordermain.VRNOA';
                            groupBy = 'ordermain.VRNOA';
                            name = 'party.NAME';
                        }else if (what === 'user') {
                            field =   'user.uname ';
                            orderBy = 'ordermain.VRNOA';
                            groupBy = 'ordermain.VRNOA';
                            name = 'party.NAME';
                        }if (what === 'rate') {
                             field =   'orderdetail.RATE';
                             orderBy = 'orderdetail.RATE';
                             groupBy = 'orderdetail.RATE';
                             name    = 'party.NAME';
                         }
    }   
        // End New
        /*var what = getCurrentView();
        var type = ($('#Radio1').is(':checked') ? 'detailed' : 'summary');
        var user = $('#uname').val();
        var etype=($('#etype').val().trim()).toLowerCase();
        etype=etype.replace(' ','');*/
        // alert('from  ' +  from  +' to '+ to + 'companyid ' + companyid );
        var companyid = $('#cid').val();
        from= from.replace('/','-');
        from= from.replace('/','-');
        to= to.replace('/','-');
        to= to.replace('/','-');

        if (etype == 'purchaseorder') {
            etype = 'pur_order';
        }
        // alert(crit);
        // fetchVouchers(from, to, what, type,etype,field,crit,orderBy,groupBy,name);    
        var url = base_url + '/doc/vouchers_reports_pdf/' + from + '/' + to + '/' + what  + '/' + type + '/' + etype + '/' + field + '/' + crit + '/' + orderBy + '/' + groupBy + '/' + name;
        // var url = base_url + 'index.php/doc/CashVocuherPrintPdf/' + etype + '/' + 1   + '/' + companyid + '/' + '-1' + '/' + user;

        window.open(url);
    }



    var getCurrentView = function() {
        var what = $('.btnSelCre.btn-primary').text().split('Wise')[0].trim().toLowerCase();
        // alert(what);
        return what;
    }


     var SendReportEmail = function () {

         var to = $("#txtEmail").val();
         if(to == "")
         {
             return false;
         }

         var oSettings = dTable.fnSettings();
         oSettings._iDisplayLength = 50000;
         dTable.fnDraw();

         var table = $("#datatable_example").html();
         var etype = '';
         var type = etype.replace(' ','');
         var accTitle = etype;
         var accCode = "";
         var contactNo = "";
         var address = "";
         var fromDate = ($('#from_date').val());
         var toDate = ($('#to_date').val());

         $.ajax({
             url: base_url + "/sendreportemail",
             data: {
                 'table' : table,
                 'type' : type,
                 'acc_title' : accTitle,
                 'acc_code' : accCode,
                 'contact_no' : contactNo,
                 'address' : address,
                 'email' : to,
                 'from_date': fromDate,
                 'to_date': toDate
             },
             type: 'POST',
             dataType: 'JSON',
             success: function (result) {
                if(result == "success")
                {
                    $("#txtEmail").val('');
                    $('#sendReportEmail').modal('hide');
                }
             }
         });
     }

    return {

        init : function() {
            //$('#from_date').datepicker('update',$('#sdate').val());
            //$('#to_date');
            this.bindUI();

            // alert('ss');
        },

        bindUI : function() {
            var self = this;

            $('#btnSendEmail').click(function (e) {
                SendReportEmail();
                return false;
            });

            $('#btnSearch').on('click', function(e) {
                e.preventDefault();
                var from = ($('#from_date').val());
                var till = ($('#to_date').val());
            //     if ($('#drpdepartId').find("option[value='" + data.id + "']").length) {
            //         $('#drpdepartId').val(data.id).trigger('change');
            //     } else { 
            //     // Create a DOM Option and pre-select by default
            //     var newOption = new Option(data.text, data.id, true, true);
            //     // Append it to the select
            //     $('#drpdepartId').append(newOption).trigger('change');
            // } 
            // console.log(newOption); 
                // var departid=$('#drpdepartId').select2("val");
                var dept_id = $('#drpdepartId').select2("val");
                var cat_id = $('#drpCatId').select2("val");
                var subcat_id = $('#drpSubCatId').select2("val");
                if (dept_id!='') {
                    dept_id =  ''+ dept_id +'';
                } else {
                    dept_id = '';
                }
                // Getting Category and Subcategory Id...
                if (cat_id!='') {
                    cat_id =  ''+ cat_id +'';
                } else {
                    cat_id = '';
                }
                if (subcat_id!='') {
                    subcat_id =  ''+ subcat_id +'';
                } else {
                    subcat_id = '';
                }

                var what = getCurrentView();
                var type = ($('#Radio1').is(':checked') ? 'qty' : 'amount');     // if true means detailed view if false sumamry view
                
                var etype='';
                etype=etype.replace(' ','');
                 if(etype==='cashsale') etype='cs';
                var crit=getcrit(etype);

                fetchVouchers(till, dept_id, type, cat_id, subcat_id);  
                
            });
            $('#btnChart').click(function (e) {
                e.preventDefault();

                var from = ($('#from_date').val());
                var to = ($('#to_date').val());
                var what = getCurrentView();
                var field = '';
                var orderBy = '';
                var groupBy = '';
               if (what === 'voucher') {
                   field =   'ordermain.vrnoa';
                   orderBy = 'ordermain.vrnoa';
                   groupBy = 'ordermain.vrnoa';
                   name    = 'party.name';
               }else if (what === 'account') {
                   field =   'party.name';
                   orderBy = 'party.name';
                   groupBy = 'party.name';
                   name    = 'party.name';
               }else if (what === 'godown') {
                   field =   'dept.name';
                   orderBy = 'dept.name';
                   groupBy = 'dept.name';
                   name = ' dept.name AS name';
               }else if (what === 'item') {
                   field =   'item.item_des';
                   orderBy = 'item.item_des';
                   groupBy = 'item.item_des';
                   if (type === 'detailed') {
                       name = 'party.name';
                   }else{
                       name = 'item.item_des as name';
                   }

               }else if (what === 'date') {
                   field =   'date(ordermain.vrdate)';
                   orderBy = 'date(ordermain.vrdate)';
                   groupBy = 'date(ordermain.vrdate)';
                   name = 'party.name';
               }else if (what === 'year') {
                   field =   'year(vrdate)';
                   orderBy = 'year(vrdate)';
                   groupBy = 'year(vrdate)';
                   name    = 'party.name';
               }else if (what === 'month') {
                   field =   'month(vrdate) ';
                   orderBy = 'month(vrdate)';
                   groupBy = 'month(vrdate)';
                   name    = 'party.name';
               }else if (what === 'weekday') {
                   field =   'DAYNAME(vrdate)';
                   orderBy = 'DAYNAME(vrdate)';
                   groupBy = 'DAYNAME(vrdate)';
                   name    = 'party.name';
               }else if (what === 'user') {
                   field =   'user.uname ';
                   orderBy = 'user.uname';
                   groupBy = 'user.uname';
                   name    = 'party.name';
               }else if (what === 'rate') {
                   field =   'orderdetail.rate';
                   orderBy = 'orderdetail.rate';
                   groupBy = 'orderdetail.rate';
                   name    = 'party.name';
               }else if (what === 'city') {
                    field =   'party.city';
                    orderBy = 'party.city';
                    groupBy = 'party.city';
                    name    = 'party.name';
                }else if (what === 'area') {
                    field =   'party.cityarea';
                    orderBy = 'party.cityarea';
                    groupBy = 'party.cityarea';
                    name    = 'party.name';
                }   
              
                var type = ($('#Radio1').is(':checked') ? 'detailed' : 'summary');     // if true means detailed view if false sumamry view
                var etype='';
                etype=etype.replace(' ','');
                var crit=getcrit(etype);
                 if(etype=='purchaseorder' || etype=='saleorder') {
                     // var etypee= '';
                     // if (etype==='purchaseorder'){
                     //    etypee='purchase';
                     // }else if(etype='saleorder'){
                     //    etypee='sale';
                     // }else{
                     //    etypee='sale';
                     // }
                     // End of Etype if-else
                      if (what === 'voucher') {
                          field =   'ordermain.VRNOA';
                          orderBy = 'ordermain.VRNOA';
                          groupBy = 'ordermain.VRNOA';
                          name    = 'party.NAME';
                      }else if (what === 'account') {
                          field =   'party.NAME';
                          orderBy = 'ordermain.party_id';
                          groupBy = 'ordermain.party_id';
                          name    = 'party.NAME';
                      }else if (what === 'godown') {
                          field =   'dept.NAME';
                          orderBy = 'orderdetail.godown_id';
                          groupBy = 'orderdetail.godown_id';
                          name = ' dept.name AS NAME';
                      }else if (what === 'item') {
                          field =   'item.item_des';
                          orderBy = 'orderdetail.item_id';
                          groupBy = 'orderdetail.item_id';
                          if (type === 'detailed') {
                              name = 'party.NAME';
                          }else{
                              name = 'item.item_des as NAME';
                          }
                      }else if (what === 'date') {
                          field =   'date(ordermain.VRDATE)';
                          orderBy = 'ordermain.vrdate';
                          groupBy = 'ordermain.vrdate';
                          name = 'party.NAME';
                      }else if (what === 'year') {
                         field =   'year(vrdate)';
                         orderBy = 'ordermain.VRNOA';
                         groupBy = 'ordermain.VRNOA';
                         name = 'party.NAME';
                     }else if (what === 'month') {
                         field =   'month(vrdate) ';
                         orderBy = 'ordermain.VRNOA';
                         groupBy = 'ordermain.VRNOA';
                         name = 'party.NAME';
                     }else if (what === 'weekday') {
                         field =   'DAYNAME(vrdate)';
                         orderBy = 'ordermain.VRNOA';
                         groupBy = 'ordermain.VRNOA';
                         name = 'party.NAME';
                     }else if (what === 'user') {
                         field =   'user.uname ';
                         orderBy = 'user.uname';
                         groupBy = 'user.uname';
                         name = 'party.NAME';
                     }if (what === 'rate') {
                          field =   'orderdetail.RATE';
                          orderBy = 'orderdetail.RATE';
                          groupBy = 'orderdetail.RATE';
                          name    = 'party.NAME';
                      }
                       if (etype == 'purchaseorder') {
                        etype = 'pur_order';
                    }   
                     // End of Field if-else
                    fetchchartOrders(from, to, what, type,etype,field,crit,orderBy,groupBy,name);
                }else{
                    if (etype == 'purchaseorder') {
                        etype = 'pur_order';
                    }
                    fetchchartVouchersSale(from, to, what, type,etype,field,crit,orderBy,groupBy,name);
                    // fetchVouchers(from, to, what, type,etype,field);    
                }
                // fetchchartVouchersSale(from, to, what, type,etype,field);
                
            });

            $('#btnReset').on('click', function(e) {
                e.preventDefault();
                self.resetVoucher();
            });
            shortcut.add("F6", function() {
                $('.btnSearch').trigger('click');
            });
            shortcut.add("F3", function() {
                $('.copy5').get()[0].click();
            });
            shortcut.add("F8", function() {
                $('.excel8').get()[0].click();
            });
            shortcut.add("F10", function() {
                $('.csv10').get()[0].click();
            });
            // shortcut.add("F8", function() {
            //     Print_Voucher();
            // });
            shortcut.add("F9", function() {
                $('.btnPrint').trigger('click');
            });

            shortcut.add("F5", function() {
                self.resetVoucher();
            });
            $('.btnPrint').on('click', function(ev) {
                ev.preventDefault();
                // self.showAllRows();
            window.open(base_url + '/app/views/reportprints/monthlysalecom.php', "Purchase Report", 'scrollbars=1,width=1210, height=842');
            });
            // $('#txtEmail').addClass('form-control');
            // $('#btnSendEmail').addClass('pull-right');
            // $('.modal-footer > .btn').addClass('pull-right');

            $('.btnPrint2').on('click', function(ev) {
                // Print_Voucher();
                // $('#datatable_example').tableExport({type:'pdf',pdfFontSize:'7',escape:'false'});
                  
                   /* $("td:hidden,th:hidden","#datatable_example").show();
                        var pdf = new jsPDF('o', 'pt', 'a4');
                         pdf.cellInitialize();
                        pdf.setFontSize(7);
                        $.each( $('#datatable_example tr'), function (i, row){
                            $.each( $(row).find("td, th"), function(j, cell){
                                 var txt = $(cell).text().trim().split(" ").join("\n") || " ";
                                 var width = (j==0) ? 70 : 45; //make with column smaller
                                 //var height = (i==0) ? 40 : 30;
                                 pdf.cell(30, 50, width, 50, txt, i);
                            });
                        });
                            pdf.save('Test.pdf');*/
                    var pdf = new jsPDF('p', 'pt', 'letter');
                    // source can be HTML-formatted string, or a reference
                    // to an actual DOM element from which the text will be scraped.
                    source = $('#htmlexportPDF')[0];

                    // we support special element handlers. Register them with jQuery-style 
                    // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
                    // There is no support for any other type of selectors 
                    // (class, of compound) at this time.
                    specialElementHandlers = {
                        // element with id of "bypass" - jQuery style selector
                        '#bypassme': function (element, renderer) {
                            // true = "handled elsewhere, bypass text extraction"
                            return true
                        }
                    };
                    margins = {
                        top: 0,
                        bottom: 0,
                        left: 80,
                        width: 1122
                        // top: 80,
                        // bottom: 60,
                        // left: 40,
                        // width: 522
                    };
                    // all coords and widths are in jsPDF instance's declared units
                    // 'inches' in this case
                    pdf.fromHTML(
                    source, // HTML string or DOM elem ref.
                    margins.left, // x coord
                    margins.top, { // y coord
                        'width': margins.width, // max width of content on PDF
                        'elementHandlers': specialElementHandlers
                    },

                    function (dispose) {
                        // dispose: object with X, Y of the last line add to the PDF 
                        //          this allow the insertion of new lines after html
                        pdf.save('Report.pdf');
                    }, margins);
            });
            $('.btnAdvaced').on('click', function(ev) {
                ev.preventDefault();
                $('.panel-group').toggleClass("panelDisplay");
            });
          
            

            $('.btnSelCre').on('click', function(e) {
                e.preventDefault();

                $(this).addClass('btn-primary');
                $(this).siblings('.btnSelCre').removeClass('btn-primary');
            });

        },
         showAllRows : function () {

        var oSettings = purchase.dTable.fnSettings();
        oSettings._iDisplayLength = 50000;

        purchase.dTable.fnDraw();
        },

        // instead of reseting values reload the page because its cruel to write to much code to simply do that
        resetVoucher : function() {
            general.reloadWindow();
        }
    }

};

var purchase = new Purchase();
purchase.init();