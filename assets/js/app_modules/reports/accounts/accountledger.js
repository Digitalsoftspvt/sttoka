var AccountLedger = function() {
	var openingbalance=0.00;
	var fetchOpeningBalance = function(_to, _pid) {
		$.ajax({
			url : base_url + 'index.php/Account/fetchPartyOpeningBalance',
			type : 'POST',
			data : {'to': _to, 'pid' : _pid},
			dataType : 'JSON',
			success : function(data) {
				var opbal=data[0]['OPENING_BALANCE'];
				openingbalance=opbal;
				$('.opening-bal').html(parseFloat(opbal).toFixed(2));
							}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var showRunningTotal = function(_to, _pid) {
		$.ajax({
			url : base_url + 'index.php/Account/fetchRunningTotal',
			type : 'POST',
			data : {'to': _to, 'pid' : _pid},
			dataType : 'JSON',
			success : function(data) {
				
				$(data).each(function(index,elem){

					// $('#datatable_example').dataTable().fnAddData( [
					// 	"<span></span>",
					// 	"<span></span>",
					// 	"<span></span>",
					// 	"<span>Total Balance</span>",
					// 	"<span></span>",
					// 	"<span></span>",
					// 	"<span>"+ ((elem.RTotal == null) ? 0 : elem.RTotal) +"</span>" ]
					// );

					appendToTable('', '', '', 'Total Balance', '', '', '', ((elem.RTotal == null) ? 0 : elem.RTotal), '','');
				});
                var rtotal=$('.running-total').html();
	                if (parseInt(rtotal)==0)
                {
                    var openingbal=$('.opening-bal').html();
                    $('.running-total').html(parseFloat(openingbal).toFixed(2));
                }

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});	
	}

	var fetchTurnOver = function(_from , _to, _pid) {
		$.ajax({
			url : base_url + 'index.php/Account/fetchTurnOver',
			type : 'POST',
			data : {'from': _from, 'to': _to, 'pid' : _pid},
			dataType : 'JSON',
			success : function(data) {
				
				var turnOver=data[0]['sale'] - data[0]['salereturn'];

				$('.turnOver').html(parseFloat(Math.abs(turnOver)).toFixed(2));
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});	
	}

	var search = function(_from, _to, _pid) {

		$.ajax({
			url : base_url + 'index.php/Account/getAccLedgerReport',
			type : 'POST',
			data : {'from': _from, 'to' : _to, 'pid' : _pid},
			dataType : 'JSON',
			success : function(data) {

				// removes all rows
				$('#datatable_example').find('tbody tr :not(.dataTables_empty)').remove();
                if (data == 'false') {
					$('.running-total').html('0.00');
	    			$('.net-debit').html('0.00');
	    			$('.net-credit').html('0.00');	
					showRunningTotal(_to, _pid);

				} else {
					populateData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
    function sleep(miliseconds) {
        var currentTime = new Date().getTime();

        while (currentTime + miliseconds >= new Date().getTime()) {
        }
    }
	var populateData = function(data) {

		$('.running-total').html('0.00');
	    $('.net-debit').html('0.00');
	    $('.net-credit').html('0.00');	
	    $('#datatable_example tbody tr').remove();
        var _pid = $('#name_dropdown').val();
        var _from = $('#from_date').val();
	    //fetchOpeningBalance(_from,_pid);

        appendToTable('', '', '', 'Opening Balance', '', '', '', openingbalance, '','');
		var netDebit = 0;
		var netCredit = 0;
		var netRTotal = 0;

		$.each(data, function(index, elem) {

			netDebit += parseFloat(elem.DEBIT);
			netCredit += parseFloat(elem.CREDIT);
			if (index == (data.length - 1)) {
				netRTotal = elem.RTotal;
			};
			var drcr='';
			if (parseFloat(elem.RTotal) > 0) {
				drcr = "Dr";
			} else {
				drcr = "Cr";
			}
			var voucher_type = '';
    		
            if ( elem.ETYPE.toLowerCase() == 'sale' ) {
                voucher_type = base_url +'index.php/saleorder/Sale_Invoice?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'jv' ) {
                voucher_type = base_url +'index.php/jv?vrnoa=' + elem.VRNOA;
            } else if ( ( elem.ETYPE.toLowerCase() == 'cpv' ) || ( elem.ETYPE.toLowerCase() == 'crv' ) ) {
                voucher_type = base_url +'index.php/payment?vrnoa=' + elem.VRNOA + '&etype=' + elem.ETYPE.toLowerCase();
            } else if ( ( elem.ETYPE.toLowerCase() == 'pd_issue' ) ) {
                voucher_type = base_url +'index.php/payment/chequeIssue?vrnoa=' + elem.VRNOA;
            } else if ( ( elem.ETYPE.toLowerCase() == 'pd_receive' ) ) {
                voucher_type = base_url +'index.php/payment/chequeReceive?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'purchase' ) {
                voucher_type = base_url +'index.php/purchase?vrnoa=' + elem.VRNOA ;
            } else if ( elem.ETYPE.toLowerCase() == 'sale' ) {
                voucher_type = base_url +'index.php/saleorder/Sale_Invoice?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'salereturn' ) {
                voucher_type = base_url +'index.php/salereturn?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'purchasereturn' ) {
                voucher_type = base_url +'index.php/purchasereturn?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'pur_import' ) {
                voucher_type = base_url +'index.php/purchase/import?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'assembling' ) {
                voucher_type = base_url +'index.php/item/assdeass?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'navigation' ) {
                voucher_type = base_url +'index.php/stocknavigation/add?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'production' ) {
                voucher_type = base_url +'index.php/production?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'consumption' ) {
                voucher_type = base_url +'index.php/consumption?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'materialreturn' ) {
                voucher_type = base_url +'index.php/materialreturn?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'moulding' ) {
                voucher_type = base_url +'index.php/moulding?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'cashslip' ) {
                voucher_type = base_url +'index.php/cashslip?vrnoa=' + elem.VRNOA;
            } else if ( elem.ETYPE.toLowerCase() == 'foreigncashpayment' || elem.ETYPE.toLowerCase() == 'foreigncashreceipt' ) {
                voucher_type = base_url +'index.php/payment/foreigncash?vrnoa=' + elem.VRNOA + '&etype=' + elem.ETYPE.toLowerCase();;
            }else if ( elem.ETYPE.toLowerCase() == 'order_loading' ) {
                voucher_type = base_url +'index.php/saleorder/partsloading?vrnoa=' + elem.VRNOA;
            }else if ( ( elem.ETYPE.toLowerCase() == 'cspv' ) || ( elem.ETYPE.toLowerCase() == 'csrv' ) ) {
                voucher_type = base_url +'index.php/cashslip?vrnoa=' + elem.VRNOA + '&etype=' + elem.ETYPE.toLowerCase();
            }else {
                voucher_type = elem.VRNOA + '-' + elem.ETYPE;
            }
            // console.log(elem.remarks);
            appendToTable(elem.VRDATE.substr(0, 10), elem.VRNOA + '-' + elem.ETYPE ,voucher_type, elem.DESCRIPTION, elem.REMARKS, elem.DEBIT, elem.CREDIT, elem.RTotal, drcr,elem.FAMOUNT)
			// $('#datatable_example').dataTable().fnAddData( [
			// 	"<span data-title='Amount'>"+ elem.VRDATE.substr(0, 10) +"</span>",
			// 	"<span>"+ voucher_type +"</span>",
			// 	"<span>"+ elem.DESCRIPTION +"</span>",
			// 	"<span>"+ elem.DEBIT +"</span>",
			// 	"<span>"+ elem.CREDIT +"</span>",
			// 	"<span style='text-align: right;'>"+ elem.RTotal +"</span>",
			// 	"<span>"+ drcr +"</span>" ]
			// );
		});
	
		$('.running-total').html(parseFloat(netRTotal).toFixed(2));
		$('.net-debit').html(parseFloat(netDebit).toFixed(2));
		$('.net-credit').html(parseFloat(netCredit).toFixed(2));




	}

	var appendToTable = function(vrdate, voucher_type,hreff, description, remarks, debit, credit, balance, drcr,famount) {

		var remarks = (remarks && remarks != '') ? remarks : '-';
		var srno = $('#datatable_example tbody tr').length + 1;
		var row = 	"<tr>" +
						"<td class='srno numeric' data-title='Sr#' > "+ srno +"</td>" +
						"<td class='vrdate numeric' data-title='Date' > "+ vrdate +"</td>" +
						"<td class='voucher' data-title='Voucher'> <a href='"+ hreff + "'>" + voucher_type + "</a></td>" +
				 		// "<td><a href='"+ hreff +"' class='hreff' data-title='Voucher'><span class='fa fa-edit'></span></a> </td>" +
				 		"<td class='qty numeric' data-title='description'>  "+ description +"</td>" +
				 		"<td class='remarks numeric' data-title='description'>  "+ remarks +"</td>" +
					 	"<td class='weight numeric' data-title='Debit' style='text-align: right;'> "+ debit +"</td>" +
					 	"<td class='rate numeric' data-title='Creidt' style='text-align: right;'> "+ credit +"</td>" +
					 	"<td class='amount numeric' data-title='Balance' style='text-align: right;' > "+ balance +"</td>" +
					 	"<td class='amountre numeric' data-title='Dr/Cr' data-famount='"+famount+"' > "+ drcr +"</td>" +
				 	"</tr>";
		$(row).appendTo('#datatable_example');
	}


	var validateSearch = function() {

		var errorFlag = false;
		var from_date = $('#from_date').val();
		var to_date = $('#to_date').val();
		var pid = $('#name_dropdown').val();

		// remove the error class first
		$('#from_date').removeClass('inputerror');
		$('#to_date').removeClass('inputerror');
		$('#name_dropdown').removeClass('inputerror');

		if ( from_date === '' || from_date === null ) {
			$('#from_date').addClass('inputerror');
			errorFlag = true;
		}
		if ( to_date === '' || to_date === null ) {
			$('#to_date').addClass('inputerror');
			errorFlag = true;
		}
		if ( pid === '' || pid === null ) {
			$('#name_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		if (from_date > to_date ){
            $('#from_date').addClass('inputerror');
            alert('Starting date must Be less than ending date.........')
            errorFlag = true;   
        }

		return errorFlag;
	}

	var printReport = function() {
		var error = validateSearch();
		if (!error) {
			var _from = $('#from_date').val();
			var _to = $('#to_date').val();
			var _pid = $('#name_dropdown').val();
			_from= _from.replace('/','-');
			_from= _from.replace('/','-');
			_to= _to.replace('/','-');
			_to= _to.replace('/','-');

			var companyid =$('#cid').val();
			var user = $('#uname').val();
			
			var url = base_url + 'index.php/doc/pdf_ledger/' + _from + '/' + _to  + '/' + _pid + '/' + companyid + '/' + '-1' + '/' + user;
			window.open(url);

		} else {
				alert('Correct the errors...');
				}
		// window.open(base_url + 'application/views/reportprints/ledger.php', "Sale Report", "width=1000, height=842");
		// window.open(base_url + 'application/views/reportPrints/ledger.php', "Sale Report", "width=1000, height=842");
	}

		var Account_Flow = function() {
		var error = validateSearch();
		if (!error) {
			var _from = $('#from_date').val();
			var _to = $('#to_date').val();
			var _pid = $('#name_dropdown').val();
			var account_name = $('#name_dropdown').find('option:selected').text();
			var account_id = $('#name_dropdown').find('option:selected').data('accountid');
			//alert(account_name+" "+account_id);
			
			//_from= _from.replace('/','-');
			_from = _from.replace(/\//g, '-');
			//_from= _from.replace('/','-');
			_to= _to.replace(/\//g, '-');
			//_to= _to.replace('/','-');
			var companyid =$('#cid').val();
			var user = $('#uname').val();
			
			var url = base_url + 'index.php/doc/Account_Flow/' + _from + '/' + _to  + '/' + _pid + '/' + companyid + '/' + '-1' + '/' + user+ '/'+escape(account_name) + '/' +account_id;
			window.open(url);

		} else {
				alert('Correct the errors...');
				}
		// window.open(base_url + 'application/views/reportprints/ledger.php', "Sale Report", "width=1000, height=842");
		// window.open(base_url + 'application/views/reportPrints/ledger.php', "Sale Report", "width=1000, height=842");
	}


	return {

		init : function () {
			this.bindUI();
			this.bindModalPartyGrid();
		},

		bindUI : function() {

			var self = this;
			$('#from_date').val('2007/11/01');
			$('.modal-lookup .populateAccount').on('click', function(){
				var party_id = $(this).closest('tr').find('input[name=hfModalPartyId]').val();
				$("#name_dropdown").select2("val", party_id); 				
			});

			$('.btnSearch').on('click', function(e){
				e.preventDefault();
				self.initSearch();
			});
			$('.btnprintThermal').on('click', function(e) {
			    e.preventDefault();
			    window.open(base_url + 'application/views/reportprints/counter_sale.php', "Counter Sale", 'width=1000, height=842');
			   });
			$('.btnReset').on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			$('.btnPrint').on('click', function(e) {
				e.preventDefault();
				printReport();
			});

			$('.btnPrint2').on('click', function(e) {
				e.preventDefault();
				Account_Flow();
			});
			$('.btnPrint3').on('click', function(e) {
				e.preventDefault();
				window.open(base_url + 'application/views/reportprints/party_ledger.php', "Party Ledger Report", 'width=1210, height=842');
                 // window.open(base_url + 'application/views/reportprints/vouchers_reports.php', "Party Ledger Report", 'width=1210, height=842');
				
			});

			$('.btnPrint4').on('click', function(e) {
				e.preventDefault();
				window.open(base_url + 'application/views/reportprints/ledger_acc.php', "Ledger Account Report", 'width=1000, height=842');
			});
			shortcut.add("F9", function() {
				printReport();
			});
			shortcut.add("F8", function() {
				Account_Flow();
			});
			shortcut.add("F6", function() {
				e.preventDefault();
    			self.initSearch();
			});
			shortcut.add("F5", function() {
    			self.resetVoucher();
			});

		},

		initSearch : function() {
			var error = validateSearch();

			if (!error) {

				var _from = $('#from_date').val();
				var _to = $('#to_date').val();
				var _pid = $('#name_dropdown').val();
				openingbalance=0.00;
				fetchOpeningBalance(_from, _pid);
				fetchTurnOver(_from, _to, _pid);
				search(_from, _to, _pid);
			} else {
				alert('Correct the errors...');
			}
		},
		bindModalPartyGrid : function() {
	            var dontSort = [];
	            $('#party-lookup table thead th').each(function () {
	                if ($(this).hasClass('no_sort')) {
	                    dontSort.push({ "bSortable": false });
	                } else {
	                    dontSort.push(null);
	                }
	            });
	            accountLedger.pdTable = $('#party-lookup table').dataTable({
	                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
	                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
	                "aaSorting": [[0, "asc"]],
	                "bPaginate": true,
	                "sPaginationType": "full_numbers",
	                "bJQueryUI": false,
	                "aoColumns": dontSort

	            });
	            $.extend($.fn.dataTableExt.oStdClasses, {
	                "s`": "dataTables_wrapper form-inline"
	            });
		},

		resetVoucher : function() {

			/*$('.inputerror').removeClass('inputerror');
			$('#from_date').datepicker('update', new Date());
			$('#to_date').datepicker('update', new Date());
			$('#name_dropdown').val('');

			// removes all rows
			$('#datatable_example').find('tbody tr :not(.dataTables_empty)').remove();*/
			general.reloadWindow();
		}

	};
};


var accountLedger = new AccountLedger();
accountLedger.init();