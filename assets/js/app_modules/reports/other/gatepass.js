Gatepass = function () {

    var fetchVouchers = function (from, to, what, type, etype, field, crit, orderBy, groupBy, name) {
        $('.grand-total').html(0.00);

        if (typeof gatepass.dTable != 'undefined') {
            gatepass.dTable.fnDestroy();
            $('#saleRows').empty();
        }
        $.ajax({
            url: base_url + "index.php/report/fetchGPReportData",
            data: {
                'from': from,
                'to': to,
                'what': what,
                'type': type,
                'company_id': $('#cid').val(),
                'etype': etype,
                'field': field,
                'crit': crit,
                'orderBy': orderBy,
                'groupBy': groupBy,
                'name': name
            },
            type: 'POST',
            dataType: 'JSON',
            beforeSend: function () {
                console.log(this.data);
            },
            complete: function () {
            },
            success: function (result) {
                if (result.length !== 0 || result.length !== '') {
                    $('#chart_tabs').addClass('disp');
                    $('.tableDate').removeClass('disp');
                    var th;
                    var td1;
                    if (type == "summary") {
                        th = $('#summary-godownhead-template').html();
                        td1 = $("#voucher-itemsummary-template").html();
                    } else {
                        th = $('#general-head-template').html();
                        td1 = $("#voucher-item-template").html();
                    }
                    var template = Handlebars.compile(th);
                    var html = template({});

                    $('.dthead').html(html);

                    if (type == "detailed") {

                        $("#datatable_example_wrapper").fadeIn();

                        // $(".cols_options").show();

                        var prevVoucher = "";
                        var prevVoucherMatch = "";
                        var totalSum = 0;
                        var totalQty = 0;
                        var totalWeight = 0;

                        var grandTotal = 0;
                        var grandQty = 0;
                        var grandWeight = 0;

                        var saleRows = $("#saleRows");

                        $.each(result, function (index, elem) {
                            // alert(elem.name);

                            //debugger

                            var obj = {};

                            obj.SERIAL = saleRows.find('tr').length + 1;
                            obj.VRNOA = elem.vrnoa;
                            obj.REMARKS = (elem.remarks) ? elem.remarks : "-";
                            obj.DESCRIPTION = (elem.item_des) ? elem.item_des : "-";
                            obj.NAME = (elem.name) ? elem.name : "Not Available";
                            obj.VRDATE = (elem.vrdate) ? elem.vrdate.substring(0, 10) : "-";
                            obj.QTY = (elem.qty) ? Math.abs(elem.qty) : "-";
                            obj.WEIGHT = (elem.weight) ? Math.abs(elem.weight) : "-";
                            obj.RATE = (elem.rate) ? elem.rate : "-";
                            obj.NETAMOUNT = (elem.netamount) ? elem.netamount : "-";
                            obj.WAREHOUSE = (elem.dname) ? elem.dname : "-";


                            if (what == 'voucher') {
                                prevVoucherMatch = elem.vrnoa;
                            } else if (what == 'account') {
                                prevVoucherMatch = elem.name;
                            } else if (what == 'godown') {
                                prevVoucherMatch = elem.dname;
                            } else if (what == 'item') {
                                prevVoucherMatch = elem.item_des;
                            } else if (what == 'date') {
                                prevVoucherMatch = elem.vrdate.substring(0, 10);
                            } else if (what == 'year') {
                                prevVoucherMatch = elem.yeardate;
                            } else if (what == 'month') {
                                prevVoucherMatch = elem.monthdate;
                            } else if (what == 'weekday') {
                                prevVoucherMatch = elem.weekdate;
                            } else if (what == 'user') {
                                prevVoucherMatch = elem.username;
                            }
                            else if (what == 'rate') {
                                prevVoucherMatch = elem.rate;
                            }
                            // obj.VRNOA1 = prevVoucherMatch;

                            if (prevVoucher != prevVoucherMatch) {
                                if (index !== 0) {

                                    // add the previous one's sum

                                    var source = $("#voucher-sum-template").html();
                                    var template = Handlebars.compile(source);
                                    var html = template({
                                        VOUCHER_SUM: totalSum.toFixed(2),
                                        VOUCHER_QTY_SUM: Math.abs(totalQty).toFixed(2),
                                        VOUCHER_WEIGHT_SUM: Math.abs(totalWeight).toFixed(2),
                                        'TOTAL_HEAD': 'TOTAL'
                                    });

                                    saleRows.append(html);
                                }

                                // Create the heading for this new voucher.
                                var source = $("#voucher-vhead-template").html();
                                var template = Handlebars.compile(source);
                                var html = template({VRNOA1: prevVoucherMatch});

                                saleRows.append(html);


                                // Reset the previous voucher's sum
                                totalSum = 0;
                                totalQty = 0;
                                totalWeight = 0;

                                // Reset the previous voucher to current voucher.
                                prevVoucher = prevVoucherMatch;
                            }

                            // Add the item of the new voucher
                            var source = td1;
                            var template = Handlebars.compile(source);
                            var html = template(obj);

                            saleRows.append(html);


                            grandQty += parseInt(Math.abs(elem.qty));
                            grandWeight += parseFloat(Math.abs(elem.weight));

                            totalSum += parseFloat(elem.netamount);
                            totalQty += parseInt(elem.qty);
                            totalWeight += parseFloat(elem.weight);
                            console.log(result.length - 1);
                            if (index == (result.length - 1)) {
                                var source = $("#voucher-sum-template").html();
                                var template = Handlebars.compile(source);
                                var html = template({
                                    VOUCHER_SUM: totalSum.toFixed(2),
                                    VOUCHER_QTY_SUM: Math.abs(totalQty).toFixed(2),
                                    VOUCHER_WEIGHT_SUM: Math.abs(totalWeight).toFixed(2),
                                    'TOTAL_HEAD': 'TOTAL'
                                });

                                saleRows.append(html);

                                // add the last one's sum
                                var source = $("#voucher-sum-template").html();
                                var template = Handlebars.compile(source);

                                var html = template({
                                    VOUCHER_SUM: grandTotal.toFixed(2),
                                    VOUCHER_QTY_SUM: Math.abs(grandQty).toFixed(2),
                                    VOUCHER_WEIGHT_SUM: Math.abs(grandWeight).toFixed(2),
                                    'TOTAL_HEAD': 'GRAND TOTAL'
                                });
                                debugger

                                saleRows.append(html);
                            }
                            ;

                        });
                        if ((etype == 'purchase' || etype == 'salereturn' || etype == 'purchasereturn') && roleGroup != 'Super Admin') {

                            $('td:nth-child(9),th:nth-child(9)').hide();
                            $('td:nth-child(10),th:nth-child(10)').hide();
                        }
                        $('.grand-total').html(grandTotal);
                    } else {

                        var saleRows = $("#saleRows");
                        var grandTotal = 0;
                        var grandQty = 0;
                        var grandWeight = 0;
                        $(result).each(function (index, elem) {

                            var obj = {};
                            obj.SERIAL = saleRows.find('tr').length + 1;
                            obj.QTY = (elem.qty) ? Math.abs(elem.qty) : "-";
                            obj.WEIGHT = (elem.weight) ? Math.abs(elem.weight) : "-";
                            obj.RATE = (elem.netamount) ? (elem.netamount / elem.qty).toFixed(2) : "-";
                            obj.NETAMOUNT = (elem.netamount) ? elem.netamount : "-";

                            if (what == 'voucher') {
                                prevVoucherMatch = elem.vrnoa;
                            } else if (what == 'account') {
                                prevVoucherMatch = elem.name;
                            } else if (what == 'godown') {
                                prevVoucherMatch = elem.name;
                            } else if (what == 'item') {
                                prevVoucherMatch = elem.name;
                            } else if (what == 'date') {
                                prevVoucherMatch = elem.DATE.substring(0, 10);
                            } else if (what == 'year') {
                                prevVoucherMatch = elem.yeardate;
                            } else if (what == 'month') {
                                prevVoucherMatch = elem.monthdate;
                            } else if (what == 'weekday') {
                                prevVoucherMatch = elem.weekdate;
                            } else if (what == 'user') {
                                prevVoucherMatch = elem.username;
                            } else if (what == 'rate') {
                                prevVoucherMatch = elem.rate;
                            }
                            obj.DESCRIPTION = prevVoucherMatch;

                            grandQty += parseInt(Math.abs(elem.qty));
                            grandWeight += parseFloat(Math.abs(elem.weight));

                            var source = td1;
                            var template = Handlebars.compile(source);
                            var html = template(obj);

                            saleRows.append(html);
                            if (index === (result.length - 1)) {
                                // add the last one's sum
                                var source = $("#voucher-sum_summary-template").html();
                                var template = Handlebars.compile(source);
                                var html = template({
                                    VOUCHER_SUM: grandTotal.toFixed(2),
                                    VOUCHER_QTY_SUM: Math.abs(grandQty).toFixed(2),
                                    VOUCHER_WEIGHT_SUM: Math.abs(grandWeight).toFixed(2),
                                    'TOTAL_HEAD': 'GRAND TOTAL'
                                });

                                saleRows.append(html);
                            }
                            ;

                        });

                        if ((etype == 'purchase' || etype == 'salereturn' || etype == 'purchasereturn') && roleGroup != 'Super Admin') {

                            $('td:nth-child(5),th:nth-child(5)').hide();
                            $('td:nth-child(6),th:nth-child(6)').hide();
                        }
                        $('.grand-total').html(grandTotal);
                    }
                }


                bindGrid();
            },

            error: function (result) {
                alert("Error:" + result);
            }
        });

    }


    var fetchOrderVouchers = function (from, to, what, type, etype, field, crit, orderBy, groupBy, name) {
        // alert('fetchOrderVouchers');
        $('.grand-total').html(0.00);
        // alert(etype);

        if (typeof gatepass.dTable != 'undefined') {
            gatepass.dTable.fnDestroy();
            $('#saleRows').empty();
        }
        // alert("field is"+groupBy);
        // alert("name is"+name);
        // alert("crit is"+crit);
        // alert("etype is"+etype);
        // alert("orderBy is"+orderBy);
        // alert("groupBy is"+groupBy);
        // alert(crit);
        // alert(groupBy);
        $.ajax({
            url: base_url + "index.php/purchaseorder/fetchOrderReportData",
            data: {
                'from': from,
                'to': to,
                'what': what,
                'type': type,
                'company_id': $('#cid').val(),
                'etype': etype,
                'field': field,
                'crit': crit,
                'orderBy': orderBy,
                'groupBy': groupBy,
                'name': name
            },
            type: 'POST',
            dataType: 'JSON',
            beforeSend: function () {
                console.log(this.data);
            },
            complete: function () {
            },
            success: function (result) {

                if (result.length !== 0) {
                    $('#chart_tabs').addClass('disp');
                    $('.tableDate').removeClass('disp');

                    var th;
                    var td1;

                    // if ((what == "godown" || what == "item") && type == "summary"){
                    //     th = $('#summary-godownhead-template').html();
                    // }else if (what == "date" && type == "summary"){
                    //     th = $('#voucher-dhead-template').html();
                    // }else if (type == "detailed") {
                    //     th = $('#general-head-template').html();
                    // }else {
                    //     th = $('#summary-head-template').html();
                    // }

                    if (type == "summary") {
                        th = $('#summary-godownhead-template').html();
                        td1 = $("#voucher-itemsummary-template").html();
                    } else {
                        th = $('#general-head-template').html();
                        td1 = $("#voucher-item-template").html();
                    }
                    var template = Handlebars.compile(th);
                    var html = template({});

                    $('.dthead').html(html);

                    if (type == "detailed") {

                        $("#datatable_example_wrapper").fadeIn();

                        // $(".cols_options").show();

                        var prevVoucher = "";
                        var prevVoucherMatch = "";
                        var totalSum = 0;
                        var totalQty = 0;
                        var totalWeight = 0;

                        var grandTotal = 0;
                        var grandQty = 0;
                        var grandWeight = 0;

                        var saleRows = $("#saleRows");

                        $.each(result, function (index, elem) {

                            //debugger

                            var obj = {};

                            obj.SERIAL = saleRows.find('tr').length + 1;
                            obj.VRNOA = elem.vrnoa;
                            obj.REMARKS = (elem.remarks) ? elem.remarks : "-";
                            obj.DESCRIPTION = (elem.item_des) ? elem.item_des : "-";
                            obj.NAME = (elem.name) ? elem.name : "Not Available";
                            obj.VRDATE = (elem.vrdate) ? elem.vrdate.substring(0, 10) : "-";
                            obj.QTY = (elem.qty) ? Math.abs(elem.qty) : "-";
                            obj.WEIGHT = (elem.weight) ? Math.abs(elem.weight) : "-";
                            obj.RATE = (elem.rate) ? elem.rate : "-";
                            obj.NETAMOUNT = (elem.netamount) ? elem.netamount : "-";

                            if (what == 'voucher') {
                                prevVoucherMatch = elem.vrnoa;
                            } else if (what == 'account') {
                                prevVoucherMatch = elem.name;
                            } else if (what == 'godown') {
                                prevVoucherMatch = elem.name;
                            } else if (what == 'item') {
                                prevVoucherMatch = elem.item_des;
                            } else if (what == 'date') {
                                prevVoucherMatch = elem.vrdate.substring(0, 10);
                            } else if (what == 'year') {
                                prevVoucherMatch = elem.yeardate;
                            } else if (what == 'month') {
                                prevVoucherMatch = elem.monthdate;
                            } else if (what == 'weekday') {
                                prevVoucherMatch = elem.weekdate;
                            } else if (what == 'user') {
                                prevVoucherMatch = elem.username;
                            } else if (what == 'rate') {
                                prevVoucherMatch = elem.rate;
                            }
                            // obj.VRNOA1 = prevVoucherMatch;

                            if (prevVoucher != prevVoucherMatch) {
                                if (index !== 0) {

                                    // add the previous one's sum

                                    var source = $("#voucher-sum-template").html();
                                    var template = Handlebars.compile(source);
                                    var html = template({
                                        VOUCHER_SUM: totalSum.toFixed(2),
                                        VOUCHER_QTY_SUM: Math.abs(totalQty).toFixed(2),
                                        VOUCHER_WEIGHT_SUM: Math.abs(totalWeight).toFixed(2),
                                        'TOTAL_HEAD': 'TOTAL'
                                    });

                                    saleRows.append(html);
                                }

                                // Create the heading for this new voucher.
                                if (etype == 'sale' && what == 'account') {

                                    elem.pbalance = (elem.cbalance > 0) ? elem.cbalance + ' Dr' : elem.cbalance + ' Cr';
                                } else {

                                    elem.pbalance = elem.pbalance;
                                    elem.pbalance = '';
                                }
                                var source = $("#voucher-vhead-template").html();
                                var template = Handlebars.compile(source);
                                var html = template({VRNOA1: prevVoucherMatch, CLOSING: elem.pbalance});

                                saleRows.append(html);


                                // Reset the previous voucher's sum
                                totalSum = 0;
                                totalWeight = 0;
                                totalQty = 0;

                                // Reset the previous voucher to current voucher.
                                prevVoucher = prevVoucherMatch;
                            }

                            // Add the item of the new voucher
                            if (etype === 'sale' && elem.detailentrytype === 'less') {

                                var source1 = $("#voucher2-item-template").html();
                                var template = Handlebars.compile(source1);
                                var html = template(obj);
                                saleRows.append(html);
                                grandTotal += parseFloat(-elem.netamount);
                                totalSum += parseFloat(-elem.netamount);
                            }
                            else {
                                var source = td1;
                                var template = Handlebars.compile(source);
                                var html = template(obj);
                                saleRows.append(html);
                                grandTotal += parseFloat(elem.netamount);
                                grandQty += parseInt(elem.qty);
                                grandWeight += parseFloat(elem.weight);

                                totalSum += parseFloat(elem.netamount);
                                totalQty += parseInt(elem.qty);
                                totalWeight += parseFloat(elem.weight);
                            }


                            if (index === (result.length - 1)) {
                                var source = $("#voucher-sum-template").html();
                                var template = Handlebars.compile(source);
                                var html = template({
                                    VOUCHER_SUM: totalSum.toFixed(2),
                                    VOUCHER_QTY_SUM: Math.abs(totalQty).toFixed(2),
                                    VOUCHER_WEIGHT_SUM: Math.abs(totalWeight).toFixed(2),
                                    'TOTAL_HEAD': 'TOTAL'
                                });

                                saleRows.append(html);

                                // add the last one's sum
                                var source = $("#voucher-sum-template").html();
                                var template = Handlebars.compile(source);
                                var html = template({
                                    VOUCHER_SUM: grandTotal.toFixed(2),
                                    VOUCHER_QTY_SUM: Math.abs(grandQty).toFixed(2),
                                    VOUCHER_WEIGHT_SUM: Math.abs(grandWeight).toFixed(2),
                                    'TOTAL_HEAD': 'GRAND TOTAL'
                                });

                                saleRows.append(html);
                            }
                            ;

                        });
                        $('.grand-total').html(grandTotal);
                        if (etype == 'sale' && roleGroup != 'Super Admin') {

                            $('td:nth-child(9),th:nth-child(9)').hide();
                            $('td:nth-child(10),th:nth-child(10)').hide();
                        }
                    } else {

                        var saleRows = $("#saleRows");
                        var grandTotal = 0;
                        var grandQty = 0;
                        var grandWeight = 0;
                        $(result).each(function (index, elem) {

                            var obj = {};
                            obj.SERIAL = saleRows.find('tr').length + 1;
                            obj.QTY = (elem.qty) ? Math.abs(elem.qty) : "-";
                            obj.WEIGHT = (elem.weight) ? Math.abs(elem.weight) : "-";
                            obj.RATE = (elem.netamount) ? (elem.netamount / elem.qty).toFixed(2) : "-";
                            obj.NETAMOUNT = (elem.netamount) ? elem.netamount : "-";

                            if (what == 'voucher') {
                                prevVoucherMatch = elem.VRNOA;
                            } else if (what == 'account') {
                                prevVoucherMatch = elem.name;
                            } else if (what == 'godown') {
                                prevVoucherMatch = elem.name;
                            } else if (what == 'item') {
                                prevVoucherMatch = elem.name;
                            } else if (what == 'date') {
                                prevVoucherMatch = elem.DATE.substring(0, 10);
                            } else if (what == 'year') {
                                prevVoucherMatch = elem.yeardate;
                            } else if (what == 'month') {
                                prevVoucherMatch = elem.monthdate;
                            } else if (what == 'weekday') {
                                prevVoucherMatch = elem.weekdate;
                            } else if (what == 'user') {
                                prevVoucherMatch = elem.username;
                            } else if (what == 'rate') {
                                prevVoucherMatch = elem.rate;
                            }
                            obj.DESCRIPTION = prevVoucherMatch;

                            grandTotal += parseFloat(elem.netamount);
                            grandQty += parseInt(elem.qty);
                            grandWeight += parseFloat(elem.weight);

                            var source = td1;
                            var template = Handlebars.compile(source);
                            var html = template(obj);

                            saleRows.append(html);
                            if (index === (result.length - 1)) {
                                // add the last one's sum
                                var source = $("#voucher-sum_summary-template").html();
                                var template = Handlebars.compile(source);
                                var html = template({
                                    VOUCHER_SUM: grandTotal.toFixed(2),
                                    VOUCHER_QTY_SUM: Math.abs(grandQty).toFixed(2),
                                    VOUCHER_WEIGHT_SUM: Math.abs(grandWeight).toFixed(2),
                                    'TOTAL_HEAD': 'GRAND TOTAL'
                                });

                                saleRows.append(html);
                            }
                            ;

                        });


                        $('.grand-total').html(grandTotal);

                        if (etype == 'sale' && roleGroup != 'Super Admin') {

                            $('td:nth-child(5),th:nth-child(5)').hide();
                            $('td:nth-child(6),th:nth-child(6)').hide();
                        }
                    }
                }


                bindGrid();
            },

            error: function (result) {
                alert("Error:" + result);
            }
        });

    }
    var getcrit = function (etype) {

        var accid = $("#drpAccountID").select2("val");
        var itemid = $('#drpitemID').select2("val");
        var departid = $('#drpdepartId').select2("val");
        var userid = $('#drpuserId').select2("val");

        var crit = '';
        if (etype === 'gp_in' || etype === 'gp_out' )
        {
            if (accid != '') {
                crit += 'AND stockmain.party_id in (' + accid + ') ';
            }
            if (itemid != '') {
                crit += 'AND stockdetail.item_id in (' + itemid + ') '
            }
            if (departid != '') {
                crit += 'AND stockdetail.godown_id in (' + departid + ') ';
            }

            if (userid != '') {
                crit += 'AND stockmain.uid in (' + userid + ') ';
            }
            //crit += 'AND stockmain.stid <>0 ';
            // alert(crit);
        }
        return crit;

    }
    var bindGrid = function () {
        var dontSort = [];
        $('#datatable_example thead th').each(function () {
            if ($(this).hasClass('no_sort')) {
                dontSort.push({"bSortable": false});
            } else {
                dontSort.push(null);
            }
        });
        gatepass.dTable = $('#datatable_example').dataTable({
            // Uncomment, if prolems with datatable.
            // "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone'<'row-fluid'<'span8' f>>>'<'pag_top' p> T>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "aaSorting": [[0, "asc"]],
            "bPaginate": true,
            "sPaginationType": "full_numbers",
            "bJQueryUI": false,
            "aoColumns": dontSort,
            "bSort": false,
            "iDisplayLength": 100,
            "oTableTools": {
                "sSwfPath": "js/copy_cvs_xls_pdf.swf",
                "aButtons": [{"sExtends": "print", "sButtonText": "Print Report", "sMessage": "Inventory Report"}]
            }
        });
        $.extend($.fn.dataTableExt.oStdClasses, {
            "s`": "dataTables_wrapper form-inline"
        });
    }
    var Print_Voucher = function () {
        var from = $('#from_date').val();
        var to = $('#to_date').val();
        // My New 
        var from = $('#from_date').val();
        var to = $('#to_date').val();
        var what = getCurrentView();
        var type = ($('#Radio1').is(':checked') ? 'detailed' : 'summary');     // if true means detailed view if false sumamry view
        // alert(type;)
        var etype = ($('#etype').val().trim()).toLowerCase();
        etype = etype.replace(' ', '');
        // alert(etype);
        var crit = getcrit(etype);
        // alert(crit);
        var orderBy = '';
        var groupBy = '';
        var field = '';
        var name = '';
        if (what === 'voucher') {
            field = 'stockmain.VRNOA';
            orderBy = 'stockmain.VRNOA';
            groupBy = 'stockmain.VRNOA';
            name = 'party.NAME';
        } else if (what === 'account') {
            field = 'party.NAME';
            orderBy = 'party.NAME';
            groupBy = 'party.NAME';
            name = 'party.NAME';
        } else if (what === 'godown') {
            field = 'dept.NAME';
            orderBy = 'dept.NAME';
            groupBy = 'dept.NAME';
            name = ' dept.name AS NAME';
        } else if (what === 'item') {
            field = 'item.item_des';
            orderBy = 'item.item_des';
            groupBy = 'item.item_des';
            if (type === 'detailed') {
                name = 'party.NAME';
            } else {
                name = 'item.item_des as NAME';
            }

        } else if (what === 'date') {
            field = 'date(stockmain.VRDATE)';
            orderBy = 'date(stockmain.VRDATE)';
            groupBy = 'date(stockmain.VRDATE)';
            name = 'party.NAME';
        } else if (what === 'year') {
            field = 'year(vrdate)';
            orderBy = 'year(vrdate)';
            groupBy = 'year(vrdate)';
            name = 'party.NAME';
        } else if (what === 'month') {
            field = 'month(vrdate) ';
            orderBy = 'month(vrdate)';
            groupBy = 'month(vrdate)';
            name = 'party.NAME';
        } else if (what === 'weekday') {
            field = 'DAYNAME(vrdate)';
            orderBy = 'DAYNAME(vrdate)';
            groupBy = 'DAYNAME(vrdate)';
            name = 'party.NAME';
        } else if (what === 'user') {
            field = 'user.uname ';
            orderBy = 'user.uname';
            groupBy = 'user.uname';
            name = 'party.NAME';
        }
        if (what === 'rate') {
            field = 'stockdetail.rate';
            orderBy = 'stockdetail.rate';
            groupBy = 'stockdetail.rate';
            name = 'party.NAME';
        }
        if (etype == 'purchaseorder' || etype == 'saleorder' || etype == 'sale') {
            // alert(etype +'  sss');
            var etypee = '';
            if (etype === 'purchaseorder') {
                // alert('wron');
                etypee = 'pur_order';
            } else if (etype = 'saleorder') {
                etypee = 'sale_order';
            } else {
                etypee = 'sale';
            }
            // End of Etype if-else
            if (what === 'voucher') {
                field = 'ordermain.VRNOA';
                orderBy = 'ordermain.VRNOA';
                groupBy = 'ordermain.VRNOA';
                name = 'party.NAME';
            } else if (what === 'account') {
                field = 'party.NAME';
                orderBy = 'ordermain.party_id';
                groupBy = 'ordermain.party_id';
                name = 'party.NAME';
            } else if (what === 'godown') {
                field = 'dept.NAME';
                orderBy = 'orderdetail.godown_id';
                groupBy = 'orderdetail.godown_id';
                name = ' dept.name AS NAME';
            } else if (what === 'item') {
                field = 'item.item_des';
                orderBy = 'orderdetail.item_id';
                groupBy = 'orderdetail.item_id';
                if (type === 'detailed') {
                    name = 'party.NAME';
                } else {
                    name = 'item.item_des as NAME';
                }
            } else if (what === 'date') {
                field = 'date(ordermain.VRDATE)';
                orderBy = 'ordermain.vrdate';
                groupBy = 'ordermain.vrdate';
                name = 'party.NAME';
            } else if (what === 'year') {
                field = 'year(vrdate)';
                orderBy = 'ordermain.VRNOA';
                groupBy = 'ordermain.VRNOA';
                name = 'party.NAME';
            } else if (what === 'month') {
                field = 'month(vrdate) ';
                orderBy = 'ordermain.VRNOA';
                groupBy = 'ordermain.VRNOA';
                name = 'party.NAME';
            } else if (what === 'weekday') {
                field = 'DAYNAME(vrdate)';
                orderBy = 'ordermain.VRNOA';
                groupBy = 'ordermain.VRNOA';
                name = 'party.NAME';
            } else if (what === 'user') {
                field = 'user.uname ';
                orderBy = 'ordermain.VRNOA';
                groupBy = 'ordermain.VRNOA';
                name = 'party.NAME';
            }
            if (what === 'rate') {
                field = 'orderdetail.RATE';
                orderBy = 'orderdetail.RATE';
                groupBy = 'orderdetail.RATE';
                name = 'party.NAME';
            }
        }
        // End New
        /*var what = getCurrentView();
        var type = ($('#Radio1').is(':checked') ? 'detailed' : 'summary');
        var user = $('#uname').val();
        var etype=($('#etype').val().trim()).toLowerCase();
        etype=etype.replace(' ','');*/
        // alert('from  ' +  from  +' to '+ to + 'companyid ' + companyid );
        var companyid = $('#cid').val();
        from = from.replace('/', '-');
        from = from.replace('/', '-');
        to = to.replace('/', '-');
        to = to.replace('/', '-');
        // alert(crit);
        var url = base_url + 'index.php/doc/vouchers_reports_pdf/' + from + '/' + to + '/' + what + '/' + type + '/' + etype + '/' + field + '/' + crit + '/' + orderBy + '/' + groupBy + '/' + name;
        // var url = base_url + 'index.php/doc/CashVocuherPrintPdf/' + etype + '/' + 1   + '/' + companyid + '/' + '-1' + '/' + user;

        window.open(url);
    }


    var getCurrentView = function () {
        var what = $('.btnSelCre.btn-primary').text().split('Wise')[0].trim().toLowerCase();
        // alert(what);
        return what;
    }

    return {

        init: function () {
            $('#from_date').val('2007/11/01');
            this.bindUI();

            // alert('ss');
        },

        bindUI: function () {
            var self = this;

            $('#btnSearch').on('click', function (e) {
                e.preventDefault();

                var from = $('#from_date').val();
                var to = $('#to_date').val();
                var what = getCurrentView();
                var type = ($('#Radio1').is(':checked') ? 'detailed' : 'summary');     // if true means detailed view if false sumamry view
                // alert(type;)
                var etype = ($('#etype').val().trim());
                if (etype==="IGP")
                {
                    etype="gp_in";
                }
                else if (etype==="OGP")
                {
                    etype="gp_out";
                }
                etype = etype.replace(' ', '');
                // alert(etype);
                var crit = getcrit(etype);
                // alert(crit);
                var orderBy = '';
                var groupBy = '';
                var field = '';
                var name = '';
                if (what === 'voucher') {
                    field = 'stockmain.vrnoa';
                    orderBy = 'stockmain.vrnoa';
                    groupBy = 'stockmain.vrnoa';
                    name = 'party.name';
                } else if (what === 'account') {
                    field = 'party.name';
                    orderBy = 'party.name';
                    groupBy = 'party.name';
                    name = 'party.name';
                } else if (what === 'godown') {
                    field = 'dept.name';
                    orderBy = 'dept.name';
                    groupBy = 'dept.name';
                    name = ' dept.name AS name';
                } else if (what === 'item') {
                    field = 'item.item_des';
                    orderBy = 'item.item_des';
                    groupBy = 'item.item_des';
                    if (type === 'detailed') {
                        name = 'party.name';
                    } else {
                        name = 'item.item_des as name';
                    }

                } else if (what === 'date') {
                    field = 'date(stockmain.vrdate)';
                    orderBy = 'date(stockmain.vrdate)';
                    groupBy = 'date(stockmain.vrdate)';
                    name = 'party.name';
                } else if (what === 'year') {
                    field = 'year(vrdate)';
                    orderBy = 'year(vrdate)';
                    groupBy = 'year(vrdate)';
                    name = 'party.name';
                } else if (what === 'month') {
                    field = 'month(vrdate) ';
                    orderBy = 'month(vrdate)';
                    groupBy = 'month(vrdate)';
                    name = 'party.name';
                } else if (what === 'weekday') {
                    field = 'DAYNAME(vrdate)';
                    orderBy = 'DAYNAME(vrdate)';
                    groupBy = 'DAYNAME(vrdate)';
                    name = 'party.name';
                } else if (what === 'user') {
                    field = 'user.uname ';
                    orderBy = 'user.uname';
                    groupBy = 'user.uname';
                    name = 'party.name';
                }
                if (what === 'rate') {
                    field = 'stockdetail.rate';
                    orderBy = 'stockdetail.rate';
                    groupBy = 'stockdetail.rate';
                    name = 'party.name';
                }

                if (etype == 'purchaseorder' || etype == 'saleorder' || etype == 'sale') {
                    // alert(etype +'  sss');

                    var etypee = '';
                    if (etype === 'purchaseorder') {
                        // alert('wron');
                        etypee = 'pur_order';
                    } else if (etype == 'saleorder') {
                        etypee = 'sale_order';
                    } else {
                        etypee = 'sale';
                    }
                    // End of Etype if-else
                    if (what === 'voucher') {
                        field = 'ordermain.vrnoa';
                        orderBy = 'ordermain.vrnoa';
                        groupBy = 'ordermain.vrnoa';
                        name = 'party.name';
                    } else if (what === 'account') {
                        field = 'party.name';
                        orderBy = 'party.name';
                        groupBy = 'party.name';
                        name = 'party.name';
                    } else if (what === 'godown') {
                        field = 'dept.name';
                        orderBy = 'dept.name';
                        groupBy = 'dept.name';
                        name = ' dept.name AS name';
                    } else if (what === 'item') {
                        field = 'item.item_des';
                        orderBy = 'item.item_des';
                        groupBy = 'item.item_des';
                        if (type === 'detailed') {
                            name = 'party.name';
                        } else {
                            name = 'item.item_des as name';
                        }
                    } else if (what === 'date') {
                        field = 'date(ordermain.vrdate)';
                        orderBy = 'date(ordermain.vrdate)';
                        groupBy = 'date(ordermain.vrdate)';
                        name = 'party.name';
                    } else if (what === 'year') {
                        field = 'year(vrdate)';
                        orderBy = 'year(vrdate)';
                        groupBy = 'year(vrdate)';
                        name = 'party.name';
                    } else if (what === 'month') {
                        field = 'month(vrdate) ';
                        orderBy = 'month(vrdate)';
                        groupBy = 'month(vrdate)';
                        name = 'party.name';
                    } else if (what === 'weekday') {
                        field = 'dayname(vrdate)';
                        orderBy = 'dayname(vrdate)';
                        groupBy = 'dayname(vrdate)';
                        name = 'party.name';
                    } else if (what === 'user') {
                        field = 'user.uname ';
                        orderBy = 'user.uname';
                        groupBy = 'user.uname';
                        name = 'party.name';
                    }
                    if (what === 'rate') {
                        field = 'orderdetail.rate';
                        orderBy = 'orderdetail.rate';
                        groupBy = 'orderdetail.rate';
                        name = 'party.name';
                    }
                    // End of Field if-else
                    // alert(etypee);

                    fetchOrderVouchers(from, to, what, type, etypee, field, crit, orderBy, groupBy, name);
                } else {
                    // alert(groupBy);
                    fetchVouchers(from, to, what, type, etype, field, crit, orderBy, groupBy, name);
                }

            });
            $('#btnReset').on('click', function (e) {
                e.preventDefault();
                self.resetVoucher();
            });
            shortcut.add("F6", function () {
                $('.btnSearch').trigger('click');
            });
            // shortcut.add("F1", function() {
            //     $('a[href="#party-lookup"]').trigger('click');
            // });
            shortcut.add("F8", function () {
                Print_Voucher();
            });
            shortcut.add("F9", function () {
                $('.btnPrint').trigger('click');
            });

            shortcut.add("F5", function () {
                self.resetVoucher();
            });
            $('.btnPrint').on('click', function (ev) {

                gatepass.showAllRows();
                window.open(base_url + 'application/views/reportprints/vouchers_reports.php', "Purchase Report", 'width=1210, height=842');
            });
            $('.btnPrint3').on('click', function (ev) {
                var what = getCurrentView();
                var etype = ($('#etype').val().trim()).toLowerCase();
                etype = etype.replace(' ', '');
                if (what == 'account' && etype == 'saleorder') {
                    window.open(base_url + 'application/views/reportprints/vouchers_reportss.php', "Purchase Report", 'width=1210, height=842');
                }

            });
            $('.btnAdvaced').on('click', function (ev) {
                ev.preventDefault();
                $('.panel-group').toggleClass("panelDisplay");
            });

            $('.btnSelCre').on('click', function (e) {
                e.preventDefault();

                $(this).addClass('btn-primary');
                $(this).siblings('.btnSelCre').removeClass('btn-primary');
            });

        },
        showAllRows: function () {

            var oSettings = gatepass.dTable.fnSettings();
            oSettings._iDisplayLength = 50000;

            gatepass.dTable.fnDraw();
        },

        // instead of reseting values reload the page because its cruel to write to much code to simply do that
        resetVoucher: function () {
            general.reloadWindow();
        }
    }

};

var gatepass = new Gatepass();
gatepass.init();