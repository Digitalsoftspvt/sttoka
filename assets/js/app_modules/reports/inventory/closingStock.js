 Purchase = function() {

 	var fetchVouchers = function (from, to, what, company_id, rpt , crit) {

        $('.grand-total').html(0.00);

        if (typeof purchase.dTable != 'undefined') {
            purchase.dTable.fnDestroy();
            $('#saleRows').empty();
        }

        var departid=$('#drpdepartId').select2("val");
        godownIds = '';
        if (departid!='') {

            godownIds = 'AND d.godown_id in (' + departid +') ';
        }

		$.ajax({
                url: base_url + "index.php/report/fetchClosingStockReport",
                data: { 'from' : from, 'to' : to, 'what' : what, 'company_id': company_id , 'crit' : crit, godown_ids : godownIds },
                type: 'POST',
                dataType: 'JSON',
                beforeSend: function () {
                    console.log(this.data);
                 },
                complete: function () { },
                success: function (result) {

                    if (result.length !== 0) {
                        
                        var th = $('#general-head-template').html();
                       
                        
                        var template = Handlebars.compile( th );
                        var html = template({});
                        $('.dthead').html( html );

                        

                            // $("#datatable_example_wrapper").fadeIn();

                            // $(".cols_options").show();

                            var prevVoucher = "";
                            var prevVoucher22 = "";
                            var i =0 ;

                            if (result.length != 0) {

                                var saleRows = $("#saleRows");
                                $.each(result, function (index, elem) {

                                    //debugger

                                    var obj1 = {};
                                    
                                    obj1.SERIAL =index+1; //saleRows.find('tr').length+1;
                                    obj1.DESCRPTION = (elem.partyname) ? (elem.godown+' '+elem.partyname +'    '+elem.etype) : "-";
                                    obj1.VRDATE = (elem.vrdate ) ? elem.vrdate.substr(0,10) : '-';
                                    //obj1.OQTY = (elem.opqty) ? elem.opqty : "-";
                                    //obj1.OWEIGHT = (elem.opweight) ? (elem.opweight) : "-";
                                    obj1.RQTY = (elem.qty > 0) ? (elem.qty) : "-";
                                    obj1.RWEIGHT = (elem.weight > 0) ? (elem.weight) : "-";
                                    obj1.IQTY = (elem.qty < 0) ? (elem.qty) : "-";
                                    obj1.IWEIGHT = (elem.weight < 0) ? (elem.weight) : "-";
                                    obj1.VRNOA = (elem.vrnoa) ? elem.vrnoa : "-";
                                    obj1.ETYPE = (elem.vrnoa) ? elem.etype : "-";
                                    
                                   

                                    prevVoucher22  = elem.item_des;

                                    if (prevVoucher != prevVoucher22 ) {
                                        i = 0;
                                        var obj = {};
                                        //obj.SERIAL =index+1; //saleRows.find('tr').length+1;
                                        obj.ITEMCODE = (elem.item_code) ? elem.item_code : "-";
                                        obj.ITEMNAME = (elem.item_des) ? elem.item_des : "-";
                                        obj.GODOWNNAME = (elem.godown) ? elem.godown : "-";
                                        obj.OQTY = (elem.opqty) ? elem.opqty : "-";
                                        obj.OWEIGHT = (elem.opweight) ? (elem.opweight) : "-";
                                        obj.RQTY = (elem.inqty) ? (elem.inqty) : "-";
                                        obj.RWEIGHT = (elem.inweight) ? (elem.inweight) : "-";
                                        obj.IQTY = (elem.outqty) ? (elem.outqty) : "-";
                                        obj.IWEIGHT = (elem.outweight) ? (elem.outweight) : "-";
                                        obj.BQTY = (elem.balanceqty) ? (elem.balanceqty) : "-";
                                        obj.BWEIGHT = (elem.balanceweight) ? (elem.balanceweight) : "-";
                                        obj.UNIT = (elem.uom) ? elem.uom : "-";
                                        
                                        var source   = $("#general-vhead-template").html();
                                        var template = Handlebars.compile(source);
                                        var html = template(obj);
                                        saleRows.append(html);

                                        prevVoucher = prevVoucher22;
                                    }
                                    // if( i== 0){
                                    //     obj1.BQTY = (elem.opqty) ? (elem.opqty) : "-";
                                    //     obj1.BWEIGHT = (elem.opweight) ? (elem.opweight) : "-";
                                    //     $('#hiddenBalanceQty').val(elem.opqty);
                                    //     $('#hiddenIssueReceiveQty').val(elem.qty);
                                    //     $('#hiddenBalanceWeight').val(elem.opweight);
                                    //     $('#hiddenIssueReceiveWeight').val(elem.weight);
                                    //     i++;
                                    // }else{
                                    //     obj1.BQTY = (parseFloat($('#hiddenBalanceQty').val()) + parseFloat($('#hiddenIssueReceiveQty').val())).toFixed(2);
                                    //     obj1.BWEIGHT = (parseFloat($('#hiddenBalanceWeight').val()) + parseFloat($('#hiddenIssueReceiveWeight').val())).toFixed(2);
                                    //     $('#hiddenBalanceQty').val(obj1.BQTY);
                                    //     $('#hiddenIssueReceiveQty').val(elem.qty);
                                    //     $('#hiddenBalanceWeight').val(obj1.BWEIGHT);
                                    //     $('#hiddenIssueReceiveWeight').val(elem.weight);
                                    //     i++;
                                    // }
                                    if( i== 0){
                                        var topqty = (elem.opqty) ? (elem.opqty) : 0;
                                        var topweight = (elem.opweight) ? (elem.opweight) : 0;
                                        obj1.BQTY = parseFloat(topqty) + parseFloat(elem.qty) ;
                                        obj1.BWEIGHT = parseFloat(topweight) + parseFloat(elem.weight) ;
                                        $('#hiddenBalanceQty').val(obj1.BQTY);
                                        //$('#hiddenIssueReceiveQty').val(elem.qty);
                                        $('#hiddenBalanceWeight').val(obj1.BWEIGHT);
                                        //$('#hiddenIssueReceiveWeight').val(elem.weight);
                                        i++;
                                    }else{
                                        obj1.BQTY = (parseFloat($('#hiddenBalanceQty').val()) + parseFloat(elem.qty)).toFixed(2);
                                        obj1.BWEIGHT = (parseFloat($('#hiddenBalanceWeight').val()) + parseFloat(elem.weight)).toFixed(2);
                                        $('#hiddenBalanceQty').val(obj1.BQTY);
                                        //$('#hiddenIssueReceiveQty').val(elem.qty);
                                        $('#hiddenBalanceWeight').val(obj1.BWEIGHT);
                                        //$('#hiddenIssueReceiveWeight').val(elem.weight);
                                        i++;
                                    }
                                    var source   = $("#general-item-template").html();
                                    var template = Handlebars.compile(source);
                                    var html = template(obj1);
                                    saleRows.append(html);
                                    
                                });
                            }
                       
                        }

					

                    bindGrid();
                },

                error: function (result) {
                    alert("Error:" + result);
                }
            });

	}

    var validateSearch = function() {

        var errorFlag = false;
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();
        

        // remove the error class first
        $('#from_date').removeClass('inputerror');
        $('#to_date').removeClass('inputerror');     

        if ( from_date === '' || from_date === null ) {
            $('#from_date').addClass('inputerror');
            errorFlag = true;
        }
        if ( to_date === '' || to_date === null ) {
            $('#to_date').addClass('inputerror');
            errorFlag = true;
        }
        if (from_date > to_date ){
            $('#from_date').addClass('inputerror');
            alert('Starting date must Be less than ending date.........')
            errorFlag = true;   
        }
        return errorFlag;
    }


	var bindGrid = function() {
        var dontSort = [];
        $('#datatable_example thead th').each(function () {
            if ($(this).hasClass('no_sort')) {
                dontSort.push({ "bSortable": false });
            } else {
                dontSort.push(null);
            }
        });
        purchase.dTable = $('#datatable_example').dataTable({
            // Uncomment, if prolems with datatable.
            // "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone'<'row-fluid'<'span8' f>>>'<'pag_top' p> T>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "aaSorting": [[0, "asc"]],
            "bPaginate": true,
            "sPaginationType": "full_numbers",
            "bJQueryUI": false,
            "aoColumns": dontSort,
			"bSort": false,
			"iDisplayLength" : 100,
            "oTableTools": {
                "sSwfPath": "js/copy_cvs_xls_pdf.swf",
                "aButtons": [{ "sExtends": "print", "sButtonText": "Print Report", "sMessage" : "Inventory Report" }]
            }
        });
        $.extend($.fn.dataTableExt.oStdClasses, {
            "s`": "dataTables_wrapper form-inline"
        });
    }

    var Print_Voucher = function(hd) {
        var error = validateSearch();
        if (!error) {
        var from = $('#from_date').val();
        var to = $('#to_date').val();
        from= from.replace('/','-');
        from= from.replace('/','-');
        to= to.replace('/','-');
        to= to.replace('/','-');

        var what = getCurrentView();
        var company_id = $('#cid').val();
        var user = $('#uname').val();

        // alert('etype  ' +  etype  +' dcno '+ dcno );
        var url = base_url + 'index.php/doc/Stock_Pdf/' + from + '/' + to  + '/' + what  + '/' + company_id + '/' + '-1' + '/' + user + '/' + hd;
        window.open(url);
    }
    }


    var getCurrentView = function() {
        var what = $('.btnSelCre.btn-primary').text().split('Wise')[0].trim().toLowerCase();
        return what;
    }

         var getcrit = function (){

        //var accid=$("#drpAccountID").select2("val");
        var itemid=$('#drpitemID').select2("val");
        var departid=$('#drpdepartId').select2("val");
        var userid=$('#drpuserId').select2("val");
        // Items
        var brandid=$("#drpbrandID").select2("val");
        var catid=$('#drpCatogeoryid').select2("val");
        var subCatid=$('#drpSubCat').select2("val");
        var txtUom=$('#drpUom').select2("val");
        // End Items
        // Account
       
        // End Account
        // var userid=$('#user_namereps').select2("val");
        // alert(userid);
        var crit ='';
        
            // if (accid!=''){
            //     crit +='AND m.party_id in (' + accid +') ';
            // }
            if (itemid!='') {
                crit +='AND d.item_id in (' + itemid +') '
            }
            if (departid!='') {
                crit +='AND d.godown_id in (' + departid +') ';
            }
            
            if (userid!='') {
                crit +='AND m.uid in (' + userid+ ') ';
            }
            // Items
            if (brandid!=''){
                crit +='AND items.bid in (' + brandid +') ';
            }
            if (catid!='') {
                crit +='AND items.catid in (' + catid +') '
            }
            if (subCatid!='') {
                crit +='AND items.subcatid in (' + subCatid +') ';
            }
            if (txtUom!='') {
                // alert('"'+txtUom+'"'); 

                var qry = "";
                $.each(txtUom,function(number){
                     qry +=  "'" + txtUom[number] + "',";
                });
                qry = qry.slice(0,-1);
                // alert(qry);
                crit +='AND items.uom in (' + qry+ ') ';
            }
            // End Items

            // Account
            
            //End Account


            crit += 'AND m.stid <>0 ';
            // alert(crit);

        return crit;

   }

	return {

		init : function() {
			this.bindUI();
		},

		bindUI : function() {
			var self = this;

            $('.btnAdvaced').on('click', function(ev) {
                ev.preventDefault();
                $('.panel-group').toggleClass("panelDisplay");
            });
            $('#from_date').val('2007/11/01');
			$('.btnSearch').on('click', function(e) {				
                e.preventDefault();
				var error = validateSearch();
                if (!error) {
                var from = $('#from_date').val();
				var to = $('#to_date').val();
                var company_id = $('#cid').val();
                var what = getCurrentView();	
                var crit=getcrit();			
				fetchVouchers(from, to, what,company_id, 0 , crit);
            }
			});
            $('.btnSearchValue').on('click', function(e) {               
                e.preventDefault();
                var error = validateSearch();
                if (!error) {
                var from = $('#from_date').val();
                var to = $('#to_date').val();
                var company_id = $('#cid').val();
                var what = getCurrentView();
                fetchVouchers(from, to, what,company_id,1);
            }
            });

            $('.btnReset').on('click', function(e) {
                e.preventDefault();
                self.resetVoucher();
            });
            $('.btnPrint').on('click', function(e) {
                e.preventDefault();
                purchase.showAllRows();
                window.open(base_url + 'application/views/reportprints/vouchers_reports.php', "Stock Report", 'width=1000, height=842');
                
            });
            $('.btnPrintPdf').on('click', function(e) {
                e.preventDefault();
                Print_Voucher(1);
            });
            $('.btnPrintPdfWithoutHeader').on('click', function(e) {
                e.preventDefault();
                Print_Voucher(0);
            });

            $('.btnSelCre').on('click', function(e) {
                e.preventDefault();

                $(this).addClass('btn-primary');
                $(this).siblings('.btnSelCre').removeClass('btn-primary');
            });

            shortcut.add("F9", function() {
                $('.btnPrint').trigger('click');
            });
             shortcut.add("F8", function() {
                Print_Voucher(1);
            });
            shortcut.add("F6", function() {
              
                var error = validateSearch();
                if (!error) {
                var from = $('#from_date').val();
                var to = $('#to_date').val();
                var company_id = $('#cid').val();
                var what = getCurrentView();                
                fetchVouchers(from, to, what,company_id,0);
            }
            });
            shortcut.add("F5", function() {
                self.resetVoucher();
            });

		},
        showAllRows : function () {

            var oSettings = purchase.dTable.fnSettings();
            oSettings._iDisplayLength = 50000;

            purchase.dTable.fnDraw();
        },
		// instead of reseting values reload the page because its cruel to write to much code to simply do that
		resetVoucher : function() {
			general.reloadWindow();
		}
	}

};

var purchase = new Purchase();
purchase.init();