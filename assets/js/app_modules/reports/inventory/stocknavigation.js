 Purchase = function() {

 	var fetchVouchers = function (from, to, what, type, crit) {

        $('.grand-total').html(0.00);

        if (typeof purchase.dTable != 'undefined') {
            purchase.dTable.fnDestroy();
            $('#saleRows').empty();
        }

		$.ajax({
                url: base_url + "index.php/report/fetchStockNavigationData",
                data: { 'from' : from, 'to' : to, 'what' : what, 'type' : type, 'crit' : crit },
                type: 'POST',
                dataType: 'JSON',
                beforeSend: function () {
                    console.log(this.data);
                 },
                complete: function () { },
                success: function (result) {

                    if (result.length !== 0) {

                        var th = $('#general-head-template').html();
                        var template = Handlebars.compile( th );
                        var html = template({});

                        $('.dthead').html( html );

                        if (what == "voucher" && type == "detailed") {

                            // $("#datatable_example_wrapper").fadeIn();

                            // $(".cols_options").show();

                            var prevVoucher = "";
                            var totalSum = 0;
                            var totalQty = 0;
                            var grandTotal = 0;

                            if (result.length != 0) {

                                var saleRows = $("#saleRows");

                                $.each(result, function (index, elem) {

                                    //debugger

                                    var obj = { };

                                    obj.SERIAL = saleRows.find('tr').length+1;
                                    obj.VRNOA = elem.VRNOA;
                                    obj.VRDATE = (elem.VRDATE) ? elem.VRDATE.substring(0,10) : "Not Available";                                    
                                    obj.DESCRIPTION = (elem.DESCRIPTION) ? elem.DESCRIPTION : "Not Available";
                                    obj.UOM = (elem.UOM) ? elem.UOM : "Not Available";
                                    obj.QTY = (elem.QTY) ? Math.abs(elem.QTY) : "Not Available";
                                    obj.WEIGHT = Math.abs(elem.weight);
                                    obj.FROM = (elem.to_dept) ? elem.to_dept: "Not Available";
                                    obj.TO = (elem.from_dept) ? elem.from_dept  : "Not Available";
                                    obj.REMARKS = elem.remarks;

                                    if (prevVoucher != obj.VRNOA) {
                                        if (index !== 0) {

                                            // add the previous one's sum

                                            var source   = $("#voucher-sum-template").html();
                                            var template = Handlebars.compile(source);
                                            var html = template({VOUCHER_QTY_SUM : Math.abs(totalQty).toFixed(2), VOUCHER_WEIGHT_SUM : Math.abs(totalWeight).toFixed(2) });

                                            saleRows.append(html);
                                        }

                                        // Create the heading for this new voucher.
                                        var source   = $("#voucher-vhead-template").html();
                                        var template = Handlebars.compile(source);
                                        var html = template(obj);

                                        saleRows.append(html);

                                        // Reset the previous voucher's sum
                                        totalSum = 0;
                                        totalQty = 0;
                                        totalWeight = 0;

                                        // Reset the previous voucher to current voucher.
                                        prevVoucher = obj.VRNOA;
                                    }

                                    // Add the item of the new voucher
                                    var source   = $("#voucher-item-template").html();
                                    var template = Handlebars.compile(source);
                                    var html = template(obj);

                                    saleRows.append(html);

                                    totalSum += parseFloat(elem.NETAMOUNT);
                                    totalQty += parseInt(elem.QTY);
                                    totalWeight += parseInt(elem.weight);
                                    grandTotal += parseFloat(elem.QTY);

                                    if (index === (result.length -1)) {
                                        // add the last one's sum

                                        var source   = $("#voucher-sum-template").html();
                                        var template = Handlebars.compile(source);
                                        var html = template({VOUCHER_SUM : totalSum.toFixed(2), VOUCHER_QTY_SUM : Math.abs(totalQty).toFixed(2), VOUCHER_WEIGHT_SUM : Math.abs(totalWeight).toFixed(2) });

                                        saleRows.append(html);
                                    };

                                });

                                $('.grand-total').html(grandTotal);
                            }
                        }

                        else if (what == "location" && type == "detailed") {

                            $("#datatable_example_wrapper").fadeIn();

                            // $(".cols_options").show();

                            var prevParty = "";
                            var totalSum = 0;
                            var totalQty = 0;
                            var totalWeight = 0;
                            var grandTotal = 0;

                            if (result.length != 0) {

                                var saleRows = $("#saleRows");

                                $.each(result, function (index, elem) {

                                    // debugger

                                    var obj = { };

                                    obj.SERIAL = saleRows.find('tr').length+1;
                                    obj.VRNOA = elem.VRNOA;
                                    obj.VRDATE = (elem.VRDATE) ? elem.VRDATE.substring(0,10) : "Not Available";                                    
                                    obj.DESCRIPTION = (elem.DESCRIPTION) ? elem.DESCRIPTION : "Not Available";
                                    obj.UOM = (elem.UOM) ? elem.UOM : "Not Available";
                                    obj.QTY = (elem.QTY) ? Math.abs(elem.QTY) : "Not Available";
                                    obj.WEIGHT = Math.abs(elem.weight);
                                    obj.FROM = (elem.to_dept) ? elem.to_dept: "Not Available";
                                    obj.TO = (elem.from_dept ) ? elem.from_dept  : "Not Available";
                                    obj.REMARKS = elem.remarks;

                                    if (prevParty != obj.FROM) {
                                        if (index !== 0) {

                                            // add the previous one's sum

                                            var source   = $("#voucher-sum-template").html();
                                            var template = Handlebars.compile(source);
                                            var html = template({VOUCHER_QTY_SUM : Math.abs(totalQty).toFixed(2) , VOUCHER_WEIGHT_SUM : Math.abs(totalWeight).toFixed(2) });

                                            saleRows.append(html);
                                        }

                                        // Create the heading for this new voucher.
                                        var source   = $("#voucher-phead-template").html();
                                        var template = Handlebars.compile(source);
                                        var html = template(obj);

                                        saleRows.append(html);

                                        // Reset the previous voucher's sum
                                        totalSum = 0;
                                        totalQty = 0;
                                        totalWeight = 0;

                                        // Reset the previous voucher to current voucher.
                                        prevParty = obj.FROM;
                                    }

                                    // Add the item of the new voucher
                                    var source   = $("#voucher-item-template").html();
                                    var template = Handlebars.compile(source);
                                    var html = template(obj);

                                    saleRows.append(html);

                                    totalSum += parseFloat(elem.NETAMOUNT);
                                    totalQty += parseInt(elem.QTY);
                                    totalWeight += parseInt(elem.weight);
                                    grandTotal += parseFloat(elem.QTY);

                                    if (index === (result.length -1)) {
                                        // add the last one's sum

                                        var source   = $("#voucher-sum-template").html();
                                        var template = Handlebars.compile(source);
                                        var html = template({ VOUCHER_QTY_SUM : Math.abs(totalQty).toFixed(2) , VOUCHER_WEIGHT_SUM : Math.abs(totalWeight).toFixed(2) });

                                        saleRows.append(html);
                                    };

                                });

                                $('.grand-total').html(grandTotal);
                            }
                        }

                        else if (what == "item" && type == "detailed") {

                            $("#datatable_example_wrapper").fadeIn();

                            // $(".cols_options").show();

                            var prevParty = "";
                            var totalSum = 0;
                            var totalQty = 0;
                            var totalWeight = 0;
                            var grandTotal = 0;

                            if (result.length != 0) {

                                var saleRows = $("#saleRows");

                                $.each(result, function (index, elem) {

                                    // debugger

                                    var obj = { };

                                    obj.SERIAL = saleRows.find('tr').length+1;
                                    obj.VRNOA = elem.VRNOA;
                                    obj.VRDATE = (elem.VRDATE) ? elem.VRDATE.substring(0,10) : "Not Available";                                    
                                    obj.DESCRIPTION = (elem.DESCRIPTION) ? elem.DESCRIPTION : "Not Available";
                                    obj.UOM = (elem.UOM) ? elem.UOM : "Not Available";
                                    obj.QTY = (elem.QTY) ? Math.abs(elem.QTY) : "Not Available";
                                    obj.WEIGHT = Math.abs(elem.weight);
                                    obj.FROM = (elem.to_dept) ? elem.to_dept: "Not Available";
                                    obj.TO = (elem.from_dept ) ? elem.from_dept  : "Not Available";
                                    obj.REMARKS = elem.remarks;

                                    if (prevParty != obj.DESCRIPTION) {
                                        if (index !== 0) {

                                            // add the previous one's sum

                                            var source   = $("#voucher-sum-template").html();
                                            var template = Handlebars.compile(source);
                                            var html = template({ VOUCHER_QTY_SUM : Math.abs(totalQty).toFixed(2) , VOUCHER_WEIGHT_SUM : Math.abs(totalWeight).toFixed(2) });

                                            saleRows.append(html);
                                        }

                                        // Create the heading for this new voucher.
                                        var source   = $("#voucher-ihead-template").html();
                                        var template = Handlebars.compile(source);
                                        var html = template(obj);

                                        saleRows.append(html);

                                        // Reset the previous voucher's sum
                                        totalSum = 0;
                                        totalQty = 0;
                                        totalWeight = 0;

                                        // Reset the previous voucher to current voucher.
                                        prevParty = obj.DESCRIPTION;
                                    }

                                    // Add the item of the new voucher
                                    var source   = $("#voucher-item-template").html();
                                    var template = Handlebars.compile(source);
                                    var html = template(obj);

                                    saleRows.append(html);

                                    totalSum += parseFloat(elem.NETAMOUNT);
                                    totalQty += parseInt(elem.QTY);
                                    totalWeight += parseInt(elem.weight);
                                    grandTotal += parseFloat(elem.QTY);

                                    if (index === (result.length -1)) {
                                        // add the last one's sum

                                        var source   = $("#voucher-sum-template").html();
                                        var template = Handlebars.compile(source);
                                        var html = template({VOUCHER_QTY_SUM : Math.abs(totalQty).toFixed(2) , VOUCHER_WEIGHT_SUM : Math.abs(totalWeight).toFixed(2) });

                                        saleRows.append(html);
                                    };

                                });

                                $('.grand-total').html(grandTotal);
                            }
                        }

                        else if (what == "date" && type == "detailed") {

                            $("#datatable_example_wrapper").fadeIn();

                            // $(".cols_options").show();

                            var prevParty = "";
                            var totalSum = 0;
                            var totalQty = 0;
                            var totalWeight = 0;
                            var grandTotal = 0;

                            if (result.length != 0) {

                                var saleRows = $("#saleRows");

                                $.each(result, function (index, elem) {

                                    // debugger

                                    var obj = { };

                                    obj.SERIAL = saleRows.find('tr').length+1;
                                    obj.VRNOA = elem.VRNOA;
                                    obj.VRDATE = (elem.VRDATE) ? elem.VRDATE.substring(0,10) : "Not Available";                                    
                                    obj.DESCRIPTION = (elem.DESCRIPTION) ? elem.DESCRIPTION : "Not Available";
                                    obj.UOM = (elem.UOM) ? elem.UOM : "Not Available";
                                    obj.QTY = (elem.QTY) ? Math.abs(elem.QTY) : "Not Available";
                                    obj.WEIGHT = Math.abs(elem.weight);
                                    obj.FROM = (elem.to_dept) ? elem.to_dept  : "Not Available";
                                    obj.TO = (elem.from_dept) ? elem.from_dept: "Not Available";
                                    obj.REMARKS = elem.remarks;

                                    if (prevParty != obj.VRDATE) {
                                        if (index !== 0) {

                                            // add the previous one's sum

                                            var source   = $("#voucher-sum-template").html();
                                            var template = Handlebars.compile(source);
                                            var html = template({VOUCHER_SUM : totalSum.toFixed(2), VOUCHER_QTY_SUM : Math.abs(totalQty).toFixed(2) , VOUCHER_WEIGHT_SUM : Math.abs(totalWeight).toFixed(2) });

                                            saleRows.append(html);
                                        }

                                        // Create the heading for this new voucher.
                                        var source   = $("#summary-dhead-template").html();
                                        var template = Handlebars.compile(source);
                                        var html = template(obj);

                                        saleRows.append(html);

                                        // Reset the previous voucher's sum
                                        totalSum = 0;
                                        totalQty = 0;
                                        totalWeight = 0;

                                        // Reset the previous voucher to current voucher.
                                        prevParty = obj.VRDATE;
                                    }

                                    // Add the item of the new voucher
                                    var source   = $("#voucher-item-template").html();
                                    var template = Handlebars.compile(source);
                                    var html = template(obj);

                                    saleRows.append(html);

                                    totalSum += parseFloat(elem.NETAMOUNT);
                                    totalQty += parseInt(elem.QTY);
                                    totalWeight += parseInt(elem.weight);
                                    grandTotal += parseFloat(elem.QTY);

                                    if (index === (result.length -1)) {
                                        // add the last one's sum

                                        var source   = $("#voucher-sum-template").html();
                                        var template = Handlebars.compile(source);
                                        var html = template({VOUCHER_SUM : totalSum.toFixed(2), VOUCHER_QTY_SUM : Math.abs(totalQty).toFixed(2) , VOUCHER_WEIGHT_SUM : Math.abs(totalWeight).toFixed(2) });

                                        saleRows.append(html);
                                    };

                                });

                                $('.grand-total').html(grandTotal);
                            }
                        }

					}

                    bindGrid();
                },

                error: function (result) {
                    alert("Error:" + result);
                }
            });

	}

	var bindGrid = function() {
        var dontSort = [];
        $('#datatable_example thead th').each(function () {
            if ($(this).hasClass('no_sort')) {
                dontSort.push({ "bSortable": false });
            } else {
                dontSort.push(null);
            }
        });
        purchase.dTable = $('#datatable_example').dataTable({
            // Uncomment, if prolems with datatable.
            // "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone'<'row-fluid'<'span8' f>>>'<'pag_top' p> T>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "aaSorting": [[0, "asc"]],
            "bPaginate": true,
            "sPaginationType": "full_numbers",
            "bJQueryUI": false,
            "aoColumns": dontSort,
			"bSort": false,
			"iDisplayLength" : 100,
            "oTableTools": {
                "sSwfPath": "js/copy_cvs_xls_pdf.swf",
                "aButtons": [{ "sExtends": "print", "sButtonText": "Print Report", "sMessage" : "Inventory Report" }]
            }
        });
        $.extend($.fn.dataTableExt.oStdClasses, {
            "s`": "dataTables_wrapper form-inline"
        });
    }

    var getCurrentView = function() {
        var what = $('.btnSelCre.btn-primary').text().split('Wise')[0].trim().toLowerCase();
        return what;
    }

    var getcrit = function (){

        var accid=$("#drpAccountID").select2("val");
        var itemid=$('#drpitemID').select2("val");
        var departid=$('#drpdepartId').select2("val");
        var userid=$('#drpuserId').select2("val");
        // Items
        var brandid=$("#drpbrandID").select2("val");
        var catid=$('#drpCatogeoryid').select2("val");
        var subCatid=$('#drpSubCat').select2("val");
        var txtUom=$('#drpUom').select2("val");
        // End Items
        // Account
        var txtCity=$("#drpCity").select2("val");
        var txtCityArea=$('#drpCityArea').select2("val");
        var l1id=$('#drpl1Id').select2("val");
        var l2id=$('#drpl2Id').select2("val");
        var l3id=$('#drpl3Id').select2("val");
        // End Account
        // var userid=$('#user_namereps').select2("val");
        // alert(userid);
        var crit ='';
        if (accid!=''){
            crit +='AND stockmain.party_id in (' + accid +') ';
        }
        if (itemid!='') {
            crit +='AND stockdetail.item_id in (' + itemid +') '
        }
        if (departid!='') {
            crit +='AND stockdetail.godown_id2 in (' + departid +') ';
        }
        
        if (userid!='') {
            crit +='AND stockmain.uid in (' + userid+ ') ';
        }
        // Items
        if (brandid!=''){
            crit +='AND item.bid in (' + brandid +') ';
        }
        if (catid!='') {
            crit +='AND item.catid in (' + catid +') '
        }
        if (subCatid!='') {
            crit +='AND item.subcatid in (' + subCatid +') ';
        }
        if (txtUom!='') {
            // alert('"'+txtUom+'"'); 

            var qry = "";
            $.each(txtUom,function(number){
                 qry +=  "'" + txtUom[number] + "',";
            });
            qry = qry.slice(0,-1);
            // alert(qry);
            crit +='AND item.uom in (' + qry+ ') ';
        }
        // End Items

        // Account
        if (txtCity!=''){
            var qry = "";
            $.each(txtCity,function(number){
                 qry +=  "'" + txtCity[number] + "',";
            });
            qry = qry.slice(0,-1);
            crit +='AND party.city in (' + qry +') ';
        }
        if (txtCityArea!='') {
            var qry = "";
            $.each(txtCityArea,function(number){
                 qry +=  "'" + txtCityArea[number] + "',";
            });
            qry = qry.slice(0,-1);
            crit +='AND party.cityarea in (' + qry +') '
        }
        if (l1id!='') {
            crit +='AND leveltbl1.l1 in (' + l1id +') ';
        }
        if (l2id!='') {
            crit +='AND leveltbl2.l2 in (' + l2id+ ') ';
        }
        if (l3id!='') {
            crit +='AND party.level3 in (' + l3id+ ') ';
        }
        //End Account


        crit += 'AND stockmain.stid <>0 ';
        // alert(crit);
        
        return crit;
    }

	return {

		init : function() {
            
			$('#voucher_type_hidden').val('new');
            this.bindUI();
		},

		bindUI : function() {
			var self = this;

            $('.btnAdvaced').on('click', function(ev) {
                ev.preventDefault();
                $('.panel-group').toggleClass("panelDisplay");
            });

			$('.btnSearch').on('click', function(e) {
				e.preventDefault();

                var crit = getcrit();
				var from = $('#from_date').val();
				var to = $('#to_date').val();
                var what = getCurrentView();
				var type = ($('#Radio1').is(':checked') ? 'detailed' : 'summary');     // if true means detailed view if false sumamry view

				fetchVouchers(from, to, what, type, crit);
			});

            $('.btnReset').on('click', function(e) {
                e.preventDefault();
                self.resetVoucher();
            });

            // $('.btnPrint').on('click',function(e){
            //     e.preventDefault();
            // });

            $('.btnPrint').on('click', function(ev) {

                purchase.showAllRows();
                window.open(base_url + 'application/views/reportprints/vouchers_reports.php', "Purchase Report", 'width=1210, height=842');
            });

            $('.btnSelCre').on('click', function(e) {
                e.preventDefault();

                $(this).addClass('btn-primary');
                $(this).siblings('.btnSelCre').removeClass('btn-primary');
            });
            $('.btnPrint').on('click',function(e){
                e.preventDefault();
            });
		},
        showAllRows : function () {

            var oSettings = purchase.dTable.fnSettings();
            oSettings._iDisplayLength = 50000;

            purchase.dTable.fnDraw();
        },

		// instead of reseting values reload the page because its cruel to write to much code to simply do that
		resetVoucher : function() {
			general.reloadWindow();
		}
	}

};

var purchase = new Purchase();
purchase.init();