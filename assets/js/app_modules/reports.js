Reports = function () {
    var fetchReports = function (from, to, what, type, etype, crit, company_id, fn_id, what2) {
        var etypebeforeundefine = etype;
        if (typeof Reports.dTable != 'undefined') {
            Reports.dTable.fnDestroy();

            $('#datatable_example1 thead').remove();
            $('#tbl_body1').empty();
        }
        $('.grand-total').html('0.00');
        $.ajax({
            url: base_url + requestname,
            data: {
                'from': from,
                'to': to,
                'what': what,
                'type': type,
                'etype': etype,
                'crit': crit,
                'company_id': company_id,
                'fn_id': fn_id,
                'what2': what2
            },
            type: 'POST',
            dataType: 'JSON',
            beforeSend: function () {
                console.log(this.data);
            },
            complete: function () {},
            success: function (mdata) {
                // here we are checking for data availability
                $('.grand-opening').text('0');
                $('.grand-closing').text('0');
                if (etypebeforeundefine == 'focashflow') {
                    $('.grand-opening').text(mdata['OPENING_BALANCE'][0]['OPENING_BALANCE']);
                    $('.grand-closing').text(mdata['CLOSING_BALANCE'][0]['CLOSING_BALANCE']);
                    mdata = mdata['maindata'];
                }
                var TableHeader =""
                if (mdata != false) {
                    var detailrecordno = 0;
                    $.each(mdata, function (ind, data) {
                    detailrecordno +=1;
                    if (detailrecordno > 1 ){
                        $('#detailrecordno').val(detailrecordno);
                        if (typeof Reports.dTable != 'undefined') {
                            Reports.dTable.fnDestroy();
                            $('#datatable_example'+ detailrecordno +' thead').remove();
                            $('#tbl_body'+ detailrecordno).empty();
                        }

                        TableHeader="";
                        TableHeader += " </tbody> </table>";
                        TableHeader +='<table id="datatable_example'+ detailrecordno +'" class="table table-striped full table-bordered "> <tbody id="tbl_body'+ detailrecordno +'" class="report-rows saleRows"> ';
                        $('#tablesdiv').append(TableHeader);
                        TableHeader = "";
                    }
                    if (data.length !== 0) {
                        var htmls = '';
                        var grandTotal = 0;
                        var Columns = [];
                        var groupby = what;
                        var subgroupby = what2;
                        var columncount = 0;
                        var grouptotal = [];
                        var subgrouptotal = [];
                        var grandtotal = [];
                        var groupflag = false;
                        var subgroupflag = false;
                        var sr = 0;
                        var vrprimary = 0;
                        var vrprimaryold = 0;
                        var tdflag = false;
                        var rowvisibility = '';
                        if (type == 'Summary') {
                            rowvisibility = "hide";
                        }

                        TableHeader = "<thead class='dthead'><tr><th>Sr#</th>";
                        // Loop For Table Header
                        $.each(data[0], function (key, value) {
                            // addClass
                            var p = key.indexOf("_hide");
                            if (key.replace("_hide", "").toLowerCase() != groupby && key.replace("_hide", "").toLowerCase() != subgroupby) {

                                var clas = '';
                                if (key.indexOf("_sum") >= 0) {
                                    clas = 'text-right';
                                } else {
                                    clas = 'text-left';
                                }
                                if (!key.indexOf("_sum") >= 0 || key.indexOf("_hide") >= 0 || key.indexOf("_num") >= 0) {
                                    tdflag = false;

                                } else {
                                    tdflag = true;
                                }
                                var width = '';
                                var x = key;
                                if (key.indexOf("_px") >= 0) {
                                    arr = x.split('_');
                                    if (key.indexOf("_sum") >= 0) {
                                        width = arr[2];
                                    } else {
                                        width = arr[1];
                                    }
                                    x = arr[0];
                                    if (components.titleCase(groupby) == arr[0]) {
                                        groupby = key;
                                    }


                                    if (components.titleCase(subgroupby) == arr[0]) {
                                        subgroupby = key;
                                    }


                                    width = 'style="width:' + width + 'px !important;"';
                                }
                                if (key != groupby && key != subgroupby) {

                                    if (key.indexOf("_hide") < 0) {

                                        var header = [];
                                        if (key.indexOf("_num") < 0) {
                                            header = x.replace("_sum", "");
                                        } else if (key.indexOf("_avg") > 0) {
                                            var vhead = x.split("_")
                                            header = vhead[0];
                                            clas = 'text-right';
                                        } else {
                                            header = x.replace("_num", "");
                                            clas = 'text-right';
                                        }
                                        TableHeader += "<th class=" + clas + " " + width + " >" + header + "</th>";
                                        columncount += 1;
                                        Columns.push(header)
                                    }
                                }

                            }
                        });
                        TableHeader += "</thead></tr>";

                        $("#datatable_example"+ detailrecordno).append(TableHeader);
                        $('#datatable_example'+ detailrecordno).css("background-color", "#BABABA");

                        // Loop For Table Header End

                        //var Columns = [];
                        groupby = components.titleCase(groupby);
                        subgroupby = components.titleCase(subgroupby);
                        var groupbyval = data[0][groupby];
                        var subgroupbyval = data[0][subgroupby];
                        var tgroupby = groupby;
                        if (groupbyval === undefined) {
                            groupbyval = data[0][groupby + '_hide'];
                            tgroupby = groupby + '_hide';
                            data = components.sortByKeyAsc(data, groupby + '_hide');
                        } else {
                            data = components.sortByKeyAsc(data, groupby);
                        }


                        if (subgroupbyval === undefined) {
                            subgroupbyval = data[0][subgroupby + '_hide'];
                            if (subgroupbyval != undefined) {
                                data.sort(firstBy(tgroupby, {
                                    ignoreCase: true
                                }).thenBy(subgroupby + '_hide'));
                            }
                        } else {
                            data.sort(firstBy(tgroupby, {
                                ignoreCase: true
                            }).thenBy(subgroupby));
                        }

                        var prevgroupbyval = data[0][groupby];
                        if (prevgroupbyval === undefined) {
                            prevgroupbyval = data[0][groupby + '_hide'];
                        }


                        var subprevgroupbyval = data[0][subgroupby];
                        if (prevgroupbyval === undefined) {
                            subprevgroupbyval = data[0][subgroupby + '_hide'];
                        }

                        // Loop[] For Tacking Sub GroupTotal
                        for (var i = 0; i < data.length; i++) {

                            groupbyval = data[i][groupby];
                            subgroupbyval = data[i][subgroupby];
                            if (groupbyval === undefined) {
                                groupbyval = data[i][groupby + '_hide'];
                            }

                            if (subgroupbyval === undefined) {
                                subgroupbyval = data[i][subgroupby + '_hide'];
                            }
                            var subgroupcheck = false;
                            if (i != 0 && groupbyval != prevgroupbyval && groupflag === true) {
                                // Sub Group Total In Case Main Group Has Been Changed
                                subgroupcheck = true;
                                var SubGroupSubTot = "<tr class='finalsumsub'><td></td>";
                                for (var y = 0; y < columncount; y++) {
                                    tdflag = true;
                                    if (subgrouptotal[y].indexOf('_avg') >= 0) {
                                        var cols = subgrouptotal[y].split("_");
                                        var cols2 = cols[2].split("/");
                                        var col1 = Columns.indexOf(cols2[0]);
                                        var col2 = Columns.indexOf(cols2[1]);
                                        SubGroupSubTot += "<td class='text-right text-bold'>" + parseFloat(subgrouptotal[col1] / subgrouptotal[col2]).toFixed(2) + "</td>";
                                    } else {
                                        SubGroupSubTot += "<td class='text-right text-bold'>" + subgrouptotal[y] + "</td>";
                                    }
                                }
                                subgrouptotal = [];
                                SubGroupSubTot += "</tr>";
                                if (what2 != '') {
                                    $('#tbl_body'+ detailrecordno ).append(SubGroupSubTot);
                                }


                                var GroupSubTot = "<tr class='finalsum'><td></td>";
                                for (var y = 0; y < columncount; y++) {
                                    tdflag = true;
                                    if (grouptotal[y].indexOf('_avg') >= 0) {
                                        var cols = grouptotal[y].split("_");
                                        var cols2 = cols[2].split("/");
                                        var col1 = Columns.indexOf(cols2[0]);
                                        var col2 = Columns.indexOf(cols2[1]);
                                        GroupSubTot += "<td class='text-right text-bold'>" + parseFloat(grouptotal[col1] / grouptotal[col2]).toFixed(2) + "</td>";
                                    } else {
                                        GroupSubTot += "<td class='text-right text-bold'>" + grouptotal[y] + "</td>";
                                    }
                                }
                                grouptotal = [];
                                GroupSubTot += "</tr>";
                                if (what != '') {
                                    $('#tbl_body'+ detailrecordno).append(GroupSubTot);
                                }
                            }


                            if (i != 0 && subgroupbyval != subprevgroupbyval && groupflag === true && subgroupcheck === false) {

                                // Sub Group Total In Case Main Group Has Been Changed
                                var SubGroupSubTot = "<tr class='finalsumsub'><td></td>";
                                for (var y = 0; y < columncount; y++) {
                                    tdflag = true;
                                    if (subgrouptotal[y].indexOf('_avg') >= 0) {
                                        var cols = subgrouptotal[y].split("_");
                                        var cols2 = cols[2].split("/");
                                        var col1 = Columns.indexOf(cols2[0]);
                                        var col2 = Columns.indexOf(cols2[1]);
                                        SubGroupSubTot += "<td class='text-right text-bold'>" + parseFloat(subgrouptotal[col1] / subgrouptotal[col2]).toFixed(2) + "</td>";
                                    } else {
                                        SubGroupSubTot += "<td class='text-right text-bold'>" + subgrouptotal[y] + "</td>";
                                    }
                                }
                                subgrouptotal = [];
                                SubGroupSubTot += "</tr>";
                                if (what2 != '') {
                                    $('#tbl_body'+ detailrecordno).append(SubGroupSubTot);
                                }

                            }


                            if (i == 0 || groupbyval != prevgroupbyval) {

                                prevgroupbyval = groupbyval;

                                groupbyval = (groupbyval) ? groupbyval : '-';
                                var GroupHeader = "<tr class='hightlight_tr'><td></td>";

                                GroupHeader += "<td colspan='" + columncount +"'>" + groupbyval + "</td>";
                                for (var y = 0; y < columncount - 1; y++) {
                                    GroupHeader += "<td class='hide'> </td>";
                                }
                                // these hide columns for Group row td equal to tbody td to avoid error in datatable
                                GroupHeader += "</tr>";
                                if (what != '') {
                                    $('#tbl_body'+ detailrecordno).append(GroupHeader);
                                    $('#tbl_body'+ detailrecordno).css("background-color", "green");
                                }

                                // Sub Group Header In Case First Group is Changed 

                                subprevgroupbyval = subgroupbyval;

                                subgroupbyval = (subgroupbyval) ? subgroupbyval : '-';
                                var GroupHeader = "<tr class='hightlight_tr " + rowvisibility + "'><td></td>";

                                GroupHeader += "<td colspan='" + columncount +"'>" + subgroupbyval + "</td>";
                                for (var y = 0; y < columncount - 1; y++) {
                                    GroupHeader += "<td class='hide'> </td>";
                                }
                                // these hide columns for Group row td equal to tbody td to avoid error in datatable
                                GroupHeader += "</tr>";
                                if (what2 != '') {
                                    $('#tbl_body'+ detailrecordno).append(GroupHeader);
                                    $('#tbl_body'+ detailrecordno).css("background-color", "green");
                                }
                            }




                            if (subgroupbyval != subprevgroupbyval && subgroupcheck === false) {

                                // Sub Group Header In Case First Group is Changed 

                                subprevgroupbyval = subgroupbyval;

                                subgroupbyval = (subgroupbyval) ? subgroupbyval : '-';
                                var GroupHeader = "<tr class='hightlight_tr " + rowvisibility + "'><td></td>";

                                GroupHeader += "<td colspan='" + columncount +"'>" + subgroupbyval + "</td>";
                                for (var y = 0; y < columncount - 1; y++) {
                                    GroupHeader += "<td class='hide'> </td>";
                                }
                                // these hide columns for Group row td equal to tbody td to avoid error in datatable
                                GroupHeader += "</tr>";
                                if (what2 != '') {
                                    $('#tbl_body'+ detailrecordno).append(GroupHeader);
                                    $('#tbl_body'+ detailrecordno).css("background-color", "green");
                                }
                            }



                            var TableHeader2 = "<tr class = " + rowvisibility + ">";
                            var x = 0;
                            var etype = '';
                            var vrnoa = '';
                            var href = '';
                            $.each(data[i], function (key, value) {
                                if (key === 'etype_hide') {
                                    etype = value;
                                }
                                if (key === 'vrnoa_hide') {
                                    vrnoa = value;
                                } else if (key === 'pid_hide') {

                                    vrnoa = value;
                                }
                                if (key === 'refurl_hide') {
                                    href = value;
                                }
                            });
                            if (href != '') {
                                href = href.replace('vrnoa_hide', vrnoa);
                                href = href.replace('etype_hide', "'" + etype + "'");

                                TableHeader2 += "<td style='width=30px !important;'> <a style='width:30px !important;'" + href + " >" + parseInt(sr + 1) + "</a></td>";
                            } else {
                                TableHeader2 += "<td>" + parseInt(sr + 1) + "</td>";
                            }
                            sr = sr + 1;

                            $.each(data[i], function (key, value) {
                                if (key === 'vrprimary_hide') {
                                    vrprimary = value;
                                }
                                if (key != groupby && key != subgroupby) {
                                    if (key.indexOf("_sum") >= 0) {
                                        tdflag = true;
                                    } else {
                                        tdflag = false;
                                    }
                                    if (key.indexOf("_sum") >= 0) {
                                        value = (value) ? parseFloat(value).toFixed(2) + value.replace(/[^a-zA-Z]+/g, '') : parseFloat(0).toFixed(2);
                                        clas = 'text-right';
                                    } else if (key.indexOf("_num") >= 0) {
                                        value = (value) ? parseFloat(value).toFixed(2) : 0;
                                        clas = 'text-right';
                                    } else {
                                        value = (value) ? value : "-";
                                        clas = 'text-left';
                                    }

                                    if (key.indexOf("_hide") < 0) {
                                        TableHeader2 += "<td class=" + clas + ">" + value + "</td>";
                                        if (x == 0) {
                                            subgrouptotal[x] = subgroupbyval + ' TOTAL';
                                            grouptotal[x] = groupbyval + ' TOTAL';
                                            grandtotal[x] = 'GRAND TOTAL';
                                        } else if (key.indexOf("_sum") >= 0) {
                                            if (key.indexOf("_sumc") >= 0) {
                                                if (vrprimary != vrprimaryold) {
                                                    vrprimaryold = vrprimary;
                                                    grouptotal[x] = (grouptotal[x] ? parseFloat(grouptotal[x]) : 0) + parseFloat(value);
                                                    subgrouptotal[x] = (subgrouptotal[x] ? parseFloat(subgrouptotal[x]) : 0) + parseFloat(value);
                                                    grandtotal[x] = (grandtotal[x] ? parseFloat(grandtotal[x]) : 0) + parseFloat(value);
                                                    grouptotal[x] = parseFloat(grouptotal[x]).toFixed(2);
                                                    subgrouptotal[x] = parseFloat(subgrouptotal[x]).toFixed(2);
                                                    grandtotal[x] = parseFloat(grandtotal[x]).toFixed(2);
                                                    groupflag = true;
                                                    tdflag = true;
                                                }

                                            } else {
                                                grouptotal[x] = (grouptotal[x] ? parseFloat(grouptotal[x]) : 0) + parseFloat(value);
                                                subgrouptotal[x] = (subgrouptotal[x] ? parseFloat(subgrouptotal[x]) : 0) + parseFloat(value);
                                                grandtotal[x] = (grandtotal[x] ? parseFloat(grandtotal[x]) : 0) + parseFloat(value);
                                                grouptotal[x] = parseFloat(grouptotal[x]).toFixed(2);
                                                subgrouptotal[x] = parseFloat(subgrouptotal[x]).toFixed(2);
                                                grandtotal[x] = parseFloat(grandtotal[x]).toFixed(2);
                                                groupflag = true;
                                                tdflag = true;
                                            }

                                        } else if (key.indexOf("_avg") >= 0) {
                                            var cols = key.split("_");
                                            subgrouptotal[x] = "_" + cols[2] + "_" + cols[3];
                                            grouptotal[x] = "_" + cols[2] + "_" + cols[3];
                                            grandtotal[x] = "_" + cols[2] + "_" + cols[3];
                                        } else {
                                            grouptotal[x] = '';
                                            subgrouptotal[x] = '';
                                            grandtotal[x] = '';
                                        }
                                        x += 1;
                                    }
                                }

                            });

                            TableHeader2 += "</tr>";
                            $('#tbl_body'+ detailrecordno).append(TableHeader2);
                            $('#tbl_body'+ detailrecordno).css("background-color", "white");
                        }
                    }
                
                    if (groupflag === true) {
                        var SubGroupSubTot = "<tr class='finalsumsub'><td></td>";
                        for (var y = 0; y < columncount; y++) {
                            tdflag = true;
                            if (subgrouptotal[y].indexOf('_avg') >= 0) {
                                var cols = subgrouptotal[y].split("_");
                                var cols2 = cols[2].split("/");
                                var col1 = Columns.indexOf(cols2[0]);
                                var col2 = Columns.indexOf(cols2[1]);
                                SubGroupSubTot += "<td class='text-right text-bold'>" + parseFloat(subgrouptotal[col1] / subgrouptotal[col2]).toFixed(2) + "</td>";
                            } else {
                                SubGroupSubTot += "<td class='text-right text-bold'>" + subgrouptotal[y] + "</td>";
                            }

                        }
                        SubGroupSubTot += "</tr>";

                        var GroupSubTot = "<tr class='finalsum'><td></td>";
                        for (var y = 0; y < columncount; y++) {
                            tdflag = true;

                            if (grouptotal[y].indexOf('_avg') >= 0) {
                                var cols = grouptotal[y].split("_");
                                var cols2 = cols[2].split("/");
                                var col1 = Columns.indexOf(cols2[0]);
                                var col2 = Columns.indexOf(cols2[1]);
                                GroupSubTot += "<td class='text-right text-bold'>" + parseFloat(grouptotal[col1] / grouptotal[col2]).toFixed(2) + "</td>";
                            } else {
                                GroupSubTot += "<td class='text-right text-bold'>" + grouptotal[y] + "</td>";
                            }


                        }
                        GroupSubTot += "</tr>";


                        var GrandTot = "<tr class='finalsum'><td></td>";
                        for (var y = 0; y < columncount; y++) {
                            tdflag = true;
                            if (grandtotal[y].indexOf('_avg') >= 0) {
                                var cols = grouptotal[y].split("_");
                                var cols2 = cols[2].split("/");
                                var col1 = Columns.indexOf(cols2[0]);
                                var col2 = Columns.indexOf(cols2[1]);
                                GrandTot += "<td class='text-right text-bold'>" + parseFloat(grandtotal[col1] / grandtotal[col2]).toFixed(2) + "</td>";
                            } else {
                                GrandTot += "<td class='text-right text-bold'>" + grandtotal[y] + "</td>";
                            }

                        }

                        GrandTot += "</tr>";
                        if (what !== '') {
                            $('#tbl_body'+ detailrecordno).append(GroupSubTot);
                        }
                        if (what2 !== '') {
                            $('#tbl_body'+ detailrecordno).append(SubGroupSubTot)
                        }
                        $('#tbl_body'+ detailrecordno).append(GrandTot);

                    }
                });
                    var y = 2;
                    var TableHeader2 = "";
                    var first = 0;
                    var second = 0;
                    $('#tbl_body'+ detailrecordno).append(TableHeader2);
                    $('#tbl_body'+ detailrecordno).css("background-color", "white");
                    bindGrid();
                } else {
                    alert('No Data Found!!');
                }
            },
            error: function (result) {
                $("#loading").hide();
                alert("Error:" + result);
            }
        });

    }

    var toCapitalize = function (str) {
        return str.replace(/\w\S*/g, function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    }
    var getTabLength = function (detailrecordno) {
        return $('#datatable_example'+ detailrecordno +' thead tr th').length;
    }
    var bindGrid = function (detailrecordno) {
        var oldTabLength = getTabLength(detailrecordno);
        var etype = $('.page_title').text();
        var what = components.getCurrentView();
        var msg = "<b>From </b>" + $('#from_date').val() + " :- <b>To</b> To:- " + $('#to_date').val() + ", " + toCapitalize(what) + " Wise";
        var dontSort = [];
        $('#datatable_example'+ detailrecordno +' thead th').each(function () {
            if ($(this).hasClass('no_sort')) {
                dontSort.push({
                    "bSortable": false
                });
            } else {
                dontSort.push(null);
            }
        });
        Reports.dTable = $('#datatable_example'+detailrecordno).dataTable({
            "sDom": '<if<t>lp>', //                 fdiptslp',
            "aaSorting": [
                [0, "asc"]
            ],
            "bPaginate": true,
            'bFilter': true,
            "sPaginationType": "full_numbers",
            "bJQueryUI": false,
            "aoColumns": dontSort,
            "bSort": false,
            "iDisplayLength": 150,
            buttons: [{
                    extend: 'print',
                    text: '<i class="fa fa-print"></i> Print',
                    className: 'btn btn-primary',
                    title: etype,
                    message: msg,
                    exportOptions: {
                        columns: ':visible',
                        search: 'applied',
                        order: 'applied'
                    },
                },
                {
                    extend: 'copy',
                    text: '<i class="fa fa-copy"></i> Copy',
                    className: 'btn btn-default',
                    exportOptions: {
                        columns: ':visible',
                        search: 'applied',
                        order: 'applied'
                    },
                },
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file"></i> PDF',
                    className: 'btn btn-danger',
                    // download: 'open',
                    orientation: 'portrait',
                    pageSize: 'A4',
                    title: etype,
                    message: "From :-" + $('#from_date').val() + " :- To:- " + $('#to_date').val() + ", " + toCapitalize(what) + " Wise",
                    exportOptions: {
                        columns: ':visible',
                        search: 'applied',
                        order: 'applied'
                    },

                },
                {
                    extend: 'excel',
                    text: '<i class="fa fa-file-text-o"></i> Excel',
                    className: 'btn btn-warning',
                    title: etype,
                    customize: function (xlsx) {
                        console.log(xlsx);
                        var sheet = xlsx.xl.worksheets['sheet1.xml'];

                        // Loop over the cells in column `C`
                        $('row c[r^="C"]', sheet).each(function () {
                            // Get the value
                            if ($('is t', this).text() == 'New York') {
                                $(this).attr('s', '20');
                            }
                        });
                    }
                },
                {
                    extend: 'csv',
                    text: '<i class="fa fa-file-text"></i> CSV',
                    className: 'btn btn-info'
                },
                {
                    extend: 'colvis',
                    text: '<i class="fa fa-eye"></i> Columns Visible',
                    className: 'btn btn-danger',
                    columns: ':not(:first-child)',
                },
            ],
            "oTableTools": {
                "sSwfPath": "js/copy_cvs_xls_pdf.swf",
                "aButtons": [{
                    "sExtends": "print",
                    "sButtonText": "Print Report",
                    "sMessage": "Inventory Report"
                }]
            },
            // "lengthMenu": [[100, 250, 500, -1], [100, 200, 100, "All"]],
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

        });
        $('.nav-light > a.tool-action').off('click').on('click', function (e, index) {
            var action = $(this).attr('data-action');
            Reports.dTable.DataTable().button(action).trigger('click');
        });
        $('.buttons-colvis').on('click', function (e) {
            $('.dt-button-collection').css({
                'display': 'block',
                'top': '499px',
                'left': '609.203px'
            });
            $('.div.dt-button-collection').css('width', '161px');
        });
        $.extend($.fn.dataTableExt.oStdClasses, {
            "s`": "dataTables_wrapper form-inline",
        });
    }




    var Print_Voucher = function () {

        var rowsCount = $('#datatable_example').find('tbody tr').length;
        var title = $('#report_title').val();
        if (title == 'Event of the Day') {
            rowsCount = 1;
        }
        if (rowsCount > 0) {
            var title = $('#report_title').val();
            var variable = $('#requestprint').val();
            var from = $('#from22').val();
            var to = $('#to22').val();
            var what = components.getCurrentView();
            var what2 = components.getCurrentView2();
            var etype = $('#etype').val();
            var type = $('input[name="types"]:checked').val();
            var fn_id = $('#fn_id').val();
            var company_id = $('#cid').val();
            var crit = components.getcrit(etype);
            var crittxt = components.getcrittxt(etype);
            if (from != undefined) {
                from = from.replace('/', '-');
                from = from.replace('/', '-');
            }
            if (to != undefined) {
                to = to.replace('/', '-');
                to = to.replace('/', '-');
            }
            var subgroup = "";
            var group = "";
            var includegroup = ($('#includegroup').bootstrapSwitch('state') === true) ? '1' : '0';
            var includegroup1 = ($('#includegroup1').bootstrapSwitch('state') === true) ? '1' : '0';

            if (includegroup === '1') {
                group = what;
            } else {
                group = ""
            }

            if (includegroup1 === '1') {
                subgroup = what2;
            } else {
                subgroup = ""
            }
            var url = base_url + 'index.php/doc/Print_Reports/?from=' + from + '&to=' + to + '&what=' + group + '&what2=' + subgroup + '&type=' + type + '&etype=' + etype + '&fn_id=' + fn_id + '&company_id=' + company_id + '&crit=' + encodeURIComponent(JSON.stringify(crit)) + '&crittxt=' + crittxt;

            window.open(url);
        } else {
            alert('No Data Found');
        }

    }

    return {

        init: function () {
            $('#from22').datepicker('update', new Date() );
            $('#to22').datepicker('update', new Date());
            this.bindUI();

            var etype = ($('#etype').val().trim()).toLowerCase();
            // components.Components(etype);
            requestname = $('#requestname').val();
        },
        bindUI: function () {
            var self = this;
            $('.btnPrint').click(function (e) {
                e.preventDefault();
                var vouchertitle = $.trim($('.report_title').text());
                if (vouchertitle ==='Daily Transaction Report'){
                    // Reports.showAllRows();
                    window.open(base_url + 'application/views/reportprints/DailyTransaction.php', "Purchase Report", 'width=1210, height=842');
                }else{
                    Print_Voucher();
                }
            });
            $('#btnSearch').on('click', function (e) {

                e.preventDefault();

                var from = $('#from22').val();
                var to = $('#to22').val();
                var what = components.getCurrentView();
                var what2 = components.getCurrentView2();
                var type = $('input[name="types"]:checked').val();
                var etype = ($('#etype').val().trim()).toLowerCase();
                etype = etype.replace(' ', '');
                var fn_id = $('#fn_id').val();
                var company_id = $('#cid').val();
                var crit = components.getcrit(etype);
                if (from != undefined) {
                    from = from.replace('/', '-');
                    from = from.replace('/', '-');
                }
                if (to != undefined) {
                    to = to.replace('/', '-');
                    to = to.replace('/', '-');
                }
                var error = false;
                var subgroup = "";
                var group = "";
                var includegroup = ($('#includegroup').bootstrapSwitch('state') === true) ? '1' : '0';
                var includegroup1 = ($('#includegroup1').bootstrapSwitch('state') === true) ? '1' : '0';

                if (includegroup === '1') {
                    group = what;
                } else {
                    group = ""
                }

                if (includegroup1 === '1') {
                    subgroup = what2;
                } else {
                    subgroup = ""
                }

                if (etype == 'focashflow') {
                    if (!$('#posid').val()) {
                        $('#posid').addClass('inputerror');
                        error = true;
                    } else {
                        error = false;
                        $('#posid').removeClass('inputerror');

                    }
                }
                if (!error) {
                    fetchReports(from, to, group, type, etype, crit, company_id, fn_id, subgroup);
                } else {
                    alert('Correct Your Errors!!')
                }
            });

            $('#btnReset').on('click', function (e) {
                e.preventDefault();
                self.resetVoucher();
            });
            shortcut.add("F6", function () {
                $('.btnSearch').trigger('click');
            });
            shortcut.add("F9", function () {
                $('btnPrint').get()[0].click();
            });

            shortcut.add("F3", function () {
                $('.copy5 ').get()[0].click();
            });

            shortcut.add("F8", function () {
                $('.excel8  ').get()[0].click();
            });



            shortcut.add("F5", function () {
                self.resetVoucher();
            });
            $('.btnAdvaced').on('click', function (ev) {
                ev.preventDefault();
                $('.panel-group').toggleClass("panelDisplay");
            });
            $(document).ajaxStart(function () {
                $(".loader").show();
            });

            $(document).ajaxComplete(function (event, xhr, settings) {
                $(".loader").hide();
            });
        },
        showAllRows: function () {
            var oSettings = Reports.dTable.fnSettings();
            oSettings._iDisplayLength = 50000;
            Reports.dTable.fnDraw();
        }, // instead of reseting values reload the page because its cruel to write to much code to simply do that
        resetVoucher: function () {
            general.reloadWindow();
        }
    }
};
var Reports = new Reports();
Reports.init();