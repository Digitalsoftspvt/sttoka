var payment = function() {

	
	var save = function( saveObj, dcno, etype ) {
		general.disableSave();

		$.ajax({
			url : base_url + 'index.php/payment/save',
			type : 'POST',
			data : { 'saveObj' : JSON.stringify(saveObj), 'dcno' : dcno, 'etype' : etype,'voucher_type_hidden': $('#voucher_type_hidden').val() , 'vrdate' : $('#vrdate').val(), 'chk_date' : $('#cur_date').val()},
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'date close') {
					alert('Sorry! you can not insert update in close date................');
				}else if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					var printConfirmation = confirm('Voucher saved!\nWould you like to print the invoice as well?');
					if (printConfirmation === true) {
						Print_Voucher(1);
						$('#cash_table').find('tbody tr').remove();
						$('#txtNetAmount').val('');
						var etype = ($('#cpv').is(':checked') === true) ? 'cpv' : 'crv';
						getMaxId(etype);

					} else {
						general.reloadWindow();
					}
				}
				general.enableSave();
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var reload = function() {
		general.reloadWindow();
	}
	var Print_Voucher = function(pr,prnt) {
		if ( $('.btnSave').data('printbtn')==0 ){
			alert('Sorry! you have not save rights..........');
		}else{
			var etype= ($('#cpv').is(':checked') === true) ? 'cpv' : 'crv';
			var dcno = $('#txtId').val();
			var companyid = $('#cid').val();
			var user = $('#uname').val();
			var url = base_url + 'index.php/doc/CashVocuherPrintPdf/' + etype + '/' + dcno   + '/' + companyid + '/' + '-1' + '/' + user + '/' + pr + '/' + prnt ;
			window.open(url);
		}
	}


	// gets the max id of the voucher
	var getMaxId = function(etype) {

		$.ajax({

			url : base_url + 'index.php/payment/getMaxId',
			type : 'POST',
			data : {'etype' : etype, 'company_id': $('#cid').val()},
			dataType : 'JSON',
			success : function(data) {

				$('#txtId').val(data);
				$('#txtMaxIdHidden').val(data);
				$('#vrnoa_all_hidden').val(data);
				$('#txtIdHidden').val(data);
				$('#txtIdImg').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var validateEntry = function() {


		var errorFlag = false;
		var name = $('#name_dropdown').val();
		var amount = $('#txtAmount').val();

		// remove the error class first
		$('#name_dropdown').removeClass('inputerror');
		$('#txtAmount').removeClass('inputerror');

		if ( name === '' || name === null ) {
			$('#name_dropdown').addClass('inputerror');
			errorFlag = true;
			$('#name_dropdown').focus();
		}
		if ( amount === '' || amount === null || amount <= "0") {
			$('#txtAmount').addClass('inputerror');
			errorFlag = true;
			$('#txtAmount').focus();
		}

		return errorFlag;
	}

	var setNetAmount = function(amount) {
		var net = ($('#txtNetAmount').val() === '') ? 0 : $('#txtNetAmount').val();
		var net = parseFloat(net) + parseFloat(amount);
		$('#txtNetAmount').val(net);
	}

	var appendToTable = function(pid, name, remarks, inv, amount) {

		var row = "";
		row = 	"<tr> <td class='pid'> "+ pid +"</td> <td class='name'> "+ name +"</td> <td class='remarks'> "+ remarks +"</td> <td class='inv'> "+ inv +"</td> <td class='amnt'> "+ amount +"</td> <td class='text-center'><a href='' class='btn btn-primary btnRowEdit'><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td> </tr>";
		$(row).appendTo('#cash_table');
	}

	var getCashPartyId = function() {
		var pid = "";
		$('#name_dropdown option').each(function() { if ($(this).text().trim().toLowerCase() == 'cash') pid = $(this).val();  });
		return pid;
	}

	var getSaveObjectAccount = function() {

		var obj = {
			pid : '20000',
			active : '1',
			name : $.trim($('#txtAccountName').val()),
			level3 : $.trim($('#txtLevel3').val()),
			dcno : $('#txtId').val(),
			etype : 'cpv/crv',
			uid : $.trim($('#uid').val()),
			company_id : $.trim($('#cid').val()),
		};

		return obj;
	}

	var getSaveObject = function() {

		var _etype = ($('#cpv').is(':checked') === true) ? 'cpv' : 'crv';
		var _date = $('#cur_date').val();
		var _vrno = $('#txtIdHidden').val();
		var _cpid =  $('#cash_dropdown').val();//getCashPartyId();
		var _uid =  $('#uid').val();//getCashPartyId();
		var _company_id =  $('#cid').val();//getCashPartyId();

		var ledgers = [];

		// alert(_cpid);

		// var ledgers = [];

		// var pledger = {};
		// pledger.pledid = '';
		// pledger.pid = _cpid;
		// pledger.description = 'CASH HEAD';
		// pledger.date = _date;
		// pledger.debit = (_etype === 'crv') ? $('#txtNetAmount').val() : 0;
		// pledger.credit = (_etype === 'cpv') ? $('#txtNetAmount').val() : 0;
		// pledger.dcno = _vrno;
		// pledger.etype = _etype;
		// pledger.pid_key = _cpid;
		// pledger.uid=_uid;
		// pledger.company_id=_company_id;
		// ledgers.push(pledger);

		$('#cash_table').find('tbody tr').each(function() {

			var _pid = $.trim($(this).closest('tr').find('td.pid').text());
			var _name = $.trim($(this).closest('tr').find('td.name').text());
			var _remarks = $.trim($(this).closest('tr').find('td.remarks').text());
			var _inv = $.trim($(this).closest('tr').find('td.inv').text());
			var _amnt = $.trim($(this).closest('tr').find('td.amnt').text());
			var pledger = {};

			pledger.pledid = '';
			pledger.pid = _pid;
			pledger.description = _name;
			pledger.remarks =  _remarks;
			pledger.date = _date;
			pledger.invoice = _inv;
			pledger.debit = (_etype === 'cpv') ? _amnt : 0;
			pledger.credit = (_etype === 'crv') ? _amnt : 0;
			pledger.dcno = _vrno;
			pledger.etype = _etype;
			pledger.pid_key = _cpid;
			pledger.uid=_uid;
			pledger.company_id=_company_id;
			ledgers.push(pledger);

			

			var pledger = {};
			pledger.pledid = '';
			pledger.pid = _cpid;
			pledger.description = _name;  //+ ' / ' + (_etype === 'crv') ? 'CASH RECEIVED' : 'CASH PAID';
			pledger.remarks =  _remarks;
			pledger.date = _date;
			pledger.debit = (_etype === 'crv') ? _amnt : 0;
			pledger.credit = (_etype === 'cpv') ? _amnt : 0;
			pledger.dcno = _vrno;
			pledger.etype = _etype;
			pledger.pid_key = _pid;
			pledger.uid=_uid;
			pledger.company_id=_company_id;
			ledgers.push(pledger);

		});

return ledgers;
}


	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		var cur_date = $('#cur_date').val();
		var cash_dropdown = $('#cash_dropdown').val();
		var amount = $('#txtNetAmount').val();
		

		// remove the error class first
		$('#cur_date').removeClass('inputerror');

		if ( cur_date === '' || cur_date === null ) {
			$('#cur_date').addClass('cur_date');
			$('#cur_date').focus();
			errorFlag = true;
		}
		if ( cash_dropdown === '' || cash_dropdown === null ) {
			$('#cash_dropdown').addClass('inputerror');
			$('#cash_dropdown').focus();
			errorFlag = true;
		}
		if ( amount === '' || amount === null || amount === "0"  ) {
			$('#txtNetAmount').addClass('inputerror');
			$('#txtNetAmount').focus();
			errorFlag = true;
		}

		return errorFlag;
	}

	var search = function(from, to, etype) {

		$.ajax({
			url : base_url + 'index.php/payment/fetchVoucherRange',
			type : 'POST',
			data : { 'from' : from, 'to' : to, 'etype' : etype },
			dataType : 'JSON',
			success : function(data) {

				$('#search_cash_table tbody tr').remove();
				populateSearchData(data);

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateSearchData = function(data) {

		var rows = "";
		$.each(data, function(index, elem) {

			rows += 	"<tr> <td class='dcno' data-etype='"+ elem.etype +"'> "+ elem.dcno +"</td> <td> "+ elem.date +"</td> <td> "+ elem.party_name +"</td> <td> "+ elem.amount +"</td> <td> "+ elem.description +"</td> <td class='text-center'><a href='' class='btn btn-primary btnRowEdit'><span class='fa fa-edit'></span></a></td> </tr>";
		});

		$(rows).appendTo('#search_cash_table tbody');
	}

	var saveAccount = function( accountObj ) {
		$.ajax({
			url : base_url + 'index.php/account/save',
			type : 'POST',
			data : { 'accountDetail' : accountObj },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'false') {
					alert('An internal error occured while saving account. Please try again.');
				} else {
					alert('Account saved successfully.');
					$('#AccountAddModel').modal('hide');
					fetchAccount();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	
	var validateSaveAccount = function() {

		var errorFlag = false;
		var partyEl = $('#txtAccountName');
		var deptEl = $('#txtLevel3');

		// remove the error class first
		$('.inputerror').removeClass('inputerror');

		if ( !partyEl.val() ) {
			$('#txtAccountName').addClass('inputerror');
			errorFlag = true;
		}
		if ( !deptEl.val() ) {
			deptEl.addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var fetchAccount = function() {

		$.ajax({
			url : base_url + 'index.php/account/fetchAll',
			type : 'POST',
			data : { 'active' : 1 },
			dataType : 'JSON',
			success : function(data) {
				if (data === 'false') {
					alert('No data found');
				} else {
					populateDataAccount(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var populateDataAccount = function(data) {
		$("#name_dropdown").empty();
		$("#pid_dropdown").empty();

		$.each(data, function(index, elem){
			var opt="<option value='"+elem.pid+"' >" +  elem.name + "</option>";
			$(opt).appendTo('#name_dropdown');
			
			var opt1="<option value='"+elem.pid+"' >" +  elem.pid + "</option>";
			$(opt1).appendTo('#pid_dropdown');
		});
	}


	var fetch = function(dcno, etype) {

		$.ajax({
			url : base_url + 'index.php/payment/fetch',
			type : 'POST',
			data : { 'dcno' : dcno, 'etype' : etype,'company_id':$('#cid').val() },
			dataType : 'JSON',
			success : function(data) {

				$('#cash_table').find('tbody tr').remove();
				$('#txtNetAmount').val('');

				if (data== 'false') {
					$('#txtIdHidden').val($('#txtMaxIdHidden').val());
					$('#vrnoa_all_hidden').val($('#txtMaxIdHidden').val());
					$('#voucher_type_hidden').val('new');
					alert('No data found');
					general.reloadWindow();
				} else {
					populateData(data);
					// general.setUpdatePrivillage();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateData = function(data) {

		$('#txtId').val(data[0]['dcno']);
		$('#txtIdHidden').val(data[0]['dcno']);
		$('#txtIdImg').val(data[0]['dcno']);

		$('#cur_date').datepicker('update', data[0]['date'].substring(0, 10));
		$('#vrdate').datepicker('update', data[0]['date'].substring(0, 10));
		var net = (data[0]['etype'] == 'cpv') ? data[0]['credit'] : data[0]['debit'];
		$('#txtNetAmount').val(parseFloat(net).toFixed(2));
		$('#user_dropdown').val(data[0]['uid']);
		// alert( data[0]['uid']);
		var cash_id =0;
		$.each(data, function(index, elem) {
			if (elem.etype == 'cpv') {
				var amnt = parseFloat(elem.credit).toFixed(1);
				if (amnt == 0.0) {
					appendToTable(elem.pid, elem.party_name, elem.remarks, elem.invoice, parseFloat(elem.debit).toFixed(2));
					setNetAmount(parseFloat(elem.debit).toFixed(2));
				}
				else{
					cash_id=elem.pid;
				}

			} else if (elem.etype == 'crv') {
				var amnt = parseFloat(elem.debit).toFixed(1);
				if (amnt == 0.0) {
					appendToTable(elem.pid, elem.party_name, elem.remarks, elem.invoice, parseFloat(elem.credit).toFixed(2));
					setNetAmount(parseFloat(elem.debit).toFixed(2));
				}
				else{
					cash_id=elem.pid;
				}

			}
			// alert(cash_id);
			$('#cash_dropdown').select2('val',cash_id);
			$('#vrnoa_all_hidden').val(data[0]['dcno']);
			$('#voucher_type_hidden').val('edit');
		});
	}

	var deleteVoucher = function(dcno, etype) {

		$.ajax({
			url : base_url + 'index.php/payment/deleteVoucher',
			type : 'POST',
			data : { 'chk_date' : $('#cur_date').val(), 'vrdate' : $('#vrdate').val(), 'dcno' : dcno, 'etype' : etype,'company_id': $('#cid').val() },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'date close') {
					alert('Sorry! you can not delete in close date................');
				}else if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var fetchRunningTotal = function(_pid , _to) {
		$.ajax({
			url : base_url + 'index.php/account/fetchRunningTotal',
			type : 'POST',
			data : {'to': _to, 'pid' : _pid},
			dataType : 'JSON',
			success : function(data) {
				var opbal=data[0]['RTotal'];

				$('#balance_lbl').text('Account,     Balance: '+parseFloat(opbal).toFixed(2));
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	let fetchImages = function(vrnoa,etype)
	{
		$.ajax({
			url : base_url + 'index.php/fileuploader/fetchImages',
			type : 'POST',
			data : { 'vrnoa' : vrnoa, 'etype' : etype },
			dataType : 'JSON',
			success : function(data)
			{
				// console.log(data);
				if (data === 'false')
				{
					alert('No Attachments found');
				} 
				else
				{
					populateImages(data);
				}

			}, 
			error : function(xhr, status, error)
			{
				console.log(xhr.responseText);
			}
		});
	}

	let updateRemarks = function(data,etype)
	{

		$.ajax({
			url : base_url + 'index.php/fileuploader/updateFilesRemarks',
			type : 'POST',
			data : { 'vrnoa' : $('#txtIdImg').val(), 'etype' : etype, 'data' : data },
			dataType : 'JSON',
			beforeSend : function()
			{
				$(".loader").show();
			},
			success : function(data) 
			{
				if($.inArray( false, data ) == -1)
				{
					alert('Files Uploaded successfully With Remarks!');
					general.reloadWindow();
				}
				$('.loader').show();		
			}, 
			error : function(xhr, status, error)
			{
				console.log(xhr.responseText);
				$('.loader').hide();
			}
		});
	}

	let populateImages = function(data)
	{
		$(".dz-preview").empty();

		$.each(data, function(i, elem)
		{
			let re 	= /(?:\.([^.]+))?$/;
			let ext = re.exec(data[i]['photo'])[1];

			ext = ext.toLowerCase();
			// Create the mock file:

			let mockFile = { name: data[i]['photo'], size: 12345 };
			if (data[i]['photo'] != "")
			{
				var src = base_url + 'assets/uploads/' + data[0]['etype'] + '/';
				if(ext == 'jpg' || ext == 'jpeg' || ext == 'bmp' || ext == 'png' || ext == 'gif')
				{
					// Call the default addedfile event handler
					imageUpload.emit("addedfile", mockFile);

					// And optionally show the thumbnail of the file:
					imageUpload.emit("thumbnail", mockFile, src + data[i]['photo']);
				} 
				else
				{

					// Call the default addedfile event handler
					imageUpload.emit("addedfile", mockFile);

					// And optionally show the thumbnail of the file:
					imageUpload.emit("thumbnail", mockFile, base_url + '/assets/uploads/icons/doc_icon.png');
				}

				$('.dz-remove').each(function(index, elem)
				{
					if(index == i)
					{
						$(this).after("<a class='btn btn-primary downloadBtn' href='" + src + data[i]['photo'] + "' target='_blank'>Download <i class='fa fa-download'></i></a>");
						var imageName = document.createElement('span');
						$(imageName).addClass('text-success');
						if(data[i]['photo'].length > 23)
						{
							var newName = data[i]['photo'].slice(0,23);
							imageName.innerHTML = newName + '...';

						} 
						else
						{
							imageName.innerHTML = data[i]['photo'];
						}

						$(this).before(imageName);
						let remarks = data[i]['remarks'];

						$(this).siblings('.attachRemarks').val(remarks);
					}
				});
				
				// // Or if the file on your server is not yet in the right
				// // size, you can let Dropzone download and resize it
				// // callback and crossOrigin are optional.
				// // imageUpload.createThumbnailFromUrl(file, imageUrl, callback, crossOrigin);
				// imageUpload.files.push( mockFile ); 
				// Make sure that there is no progress bar, etc...
				imageUpload.emit("complete", mockFile);
				// If you use the maxFiles option, make sure you adjust it to the
				// correct amount:
				var existingFileCount = 1; // The number of files already uploaded
				imageUpload.options.maxFiles = imageUpload.options.maxFiles - existingFileCount;
			}
		});
	}
	return {

		init : function() {
			this.bindUI();
			// $('#pid_dropdown').select2('open');
			$('#voucher_type_hidden').val('new'); 
			payment.Populate_PartyGrid();
			payment.bindModalPartyGrid();
			payment.fetchRequestedVr();
		},

		bindUI : function() {

			var self = this;
			let etype 	=  ($('#cpv').is(':checked') === true) ? 'cpv' : 'crv'; 
			console.log(etype);
			Dropzone.options.imageUpload = {
				url: base_url + "index.php/fileuploader/uploadFiles",
				maxFilesize: 1,
				acceptedFiles: "image/*,application/pdf,.docx,.doc,.xls,.xlsx,.txt,.rtf",
				parallelUploads: 100,
		        maxFiles: 100,
		        thumbnailWidth:"180",
           		thumbnailHeight:"180",
           		autoProcessQueue: false,
		        dictRemoveFileConfirmation: "Are you Sure to Delete this File?",
		        dictDefaultMessage: "Drop Files here to upload",
		        addRemoveLinks: true,
		        dictResponseError: 'Server not Configured',
		        dictRemoveFile: "X (remove)",
		        accept: function(file, done)
		        {
		        	let src 		= base_url + '/assets/uploads/icons/';
		        	let thumbnail 	= $('.dropzone .dz-preview.dz-file-preview .dz-image');

		        	switch (file.type)
		        	{
		        		case 'application/pdf':
		        		thumbnail.css({'background': 'url('+ src +'doc_icon.png', 'background-size': '100% 100%'});
		        		break;
		        		case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
		        		thumbnail.css({'background': 'url('+ src +'doc_icon.png', 'background-size': '100% 100%'});
		        		break;
		        		case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
				       	thumbnail.css({'background': 'url('+ src +'doc_icon.png', 'background-size': '100% 100%'});
				       	break;
				       	case 'application/vnd.ms-excel':
				       	thumbnail.css({'background': 'url('+ src +'doc_icon.png', 'background-size': '100% 100%'});
				        break;
				    }
				    done();
				},
		        
		        init : function()
		        {
		        	imageUpload = this;
			        //Restore initial message when queue has been completed]
			        this.on("addedfile", function(file, dataUrl)
			        {
			        	// console.log(file.previewTemplate);
			        	let elem = "<input type='text' placeholder='Enter Remarks' class='form-control attachRemarks' id='attachRemark' />";
			        	$(file.previewTemplate).append(elem);
			    	});
			    	this.on('sending', function(file, xhr, formData)
			    	{
			    		let remarks = $(file.previewTemplate).find('.attachRemarks').val();
			    		formData.append('vrnoa', 	$("#txtIdImg").val());
			    		formData.append('imgName', 	file.name);
			    		formData.append('etype', 	etype);
			    		formData.append('remarks', 	remarks);
			    		formData.append('table', 	$('#tablename').val());					    
					});

					this.on('success', function(file, response)
					{
						if (response===true) 
						{
							alert('File Upload successfully!!!');
							general.reloadWindow();
						}
						else
						{	
							alert('This Voucher Not Saved!!!');
							general.reloadWindow();
						}
					});

					this.on('error', function(file, errorMessage, xhr)
					{
						$(file.previewElement).find('.dz-error-message').text(errorMessage['message']);
					});

			        this.on('removedfile', function(file)
			        {
						if ($('#voucher_type_hidden').val()=='edit' && $('#uploadFiles').data('delete')==0 ){
							alert('Sorry! you have not delete rights..........');
						}
						else if($('#voucher_type_hidden').val()=='new' && $('#uploadFiles').data('delete')==0)
						{
							alert('Sorry! you have not delete rights..........');
						} 
						else 
						{
							$.ajax({
								url : base_url + "index.php/fileuploader/deleteImage",
								type : 'POST',
								data : {'imgName' : file.name, 'item_id': $("#txtIdImg").val(), 'etype' : etype},
								success : function(response)
								{
									alert(response + "  Removed");
								   },
								   error : function(xhr, status, error)
								   {
									   alert('File Not Found on Server!');
									console.log(xhr.responseText);
								}
							   });	
						}
			        	
					});

			     },
		    };
		    $('#uploadFiles').on('click', function(e)
		    {
				
				if ($('#voucher_type_hidden').val()=='edit' && $('#uploadFiles').data('update')==0 ){
					alert('Sorry! you have not update  rights..........');
				}else if($('#voucher_type_hidden').val()=='new' && $('#uploadFiles').data('insert')==0){
					alert('Sorry! you have not insert rights..........');
				}
				else
				{
					e.preventDefault();
					let files = [];
					if($('.dz-preview').length > 0)
					{
						$('.dz-preview').each(function(e)
						{
							let obj 	= {};
							obj.photo 	= $(this).find('.dz-filename span').text();
							obj.remarks = $(this).find('#attachRemark').val();
							files.push(obj);
						});
						updateRemarks(files,etype);
						imageUpload.processQueue();
					}
					else
					{
						alert('No Files Found to Upload');
					}
				}
			});
			
		    $("#viewImages").on('click', function(e){
				e.preventDefault();
				var vrnoa = $('#txtIdImg').val().trim();
				$('.dz-preview').remove();
				$('.dz-default.dz-message').css('display', 'block');
				// var etype = ($('#cpv').is(':checked') === true) ? 'cpv' : 'crv';
				fetchImages(vrnoa,etype);
			});
			
			$('#txtLevel3').on('change', function() {
				
				var level3 = $('#txtLevel3').val();
				$('#txtselectedLevel1').text('');
				$('#txtselectedLevel2').text('');
				if (level3 !== "" && level3 !== null) {
					// alert('enter' + $(this).find('option:selected').data('level2') );	
					$('#txtselectedLevel2').text(' ' + $(this).find('option:selected').data('level2'));
					$('#txtselectedLevel1').text(' ' + $(this).find('option:selected').data('level1'));
				}
			});
			// $('#txtLevel3').select2();
			$('.btnSaveM').on('click',function(e){	
				if ( $('.btnSave').data('saveaccountbtn')==0 ){
					alert('Sorry! you have not save accounts rights..........');
				}else{
					e.preventDefault();
					self.initSaveAccount();
				}
			});
			$('.btnResetM').on('click',function(){
				
				$('#txtAccountName').val('');
				$('#txtselectedLevel2').text('');
				$('#txtselectedLevel1').text('');
				$('#txtLevel3').select2('val','');
			});
			$('#AccountAddModel').on('shown.bs.modal',function(e){
				$('#txtAccountName').focus();
			});
			shortcut.add("F3", function() {
				$('#AccountAddModel').modal('show');
			});

			$('#name_dropdown').on('change',function(){

				var partyId = $(this).val();
				var vrDate = $('#cur_date').val();
				
				fetchRunningTotal(partyId, vrDate);
			});


			$('.modal-lookup .populateAccount').on('click', function(){
				// alert('dfsfsdf');
				var party_id = $(this).closest('tr').find('input[name=hfModalPartyId]').val();
				$("#name_dropdown").select2("val", party_id); //set the value
				$("#pid_dropdown").select2("val", party_id); //set the value

				// var partyEl = $('#drpParty');
				//party.fetchParty(party_id);
				// partyEl.val(party_id);
				// partyEl.trigger('liszt:updated');
				// alert('search party ' + party_id);
				// $('#pid_dropdown').val(party_id);
				// $('#name_dropdown').val(party_id);
			});

			$('.btnSave').on('click',  function(e) {
				e.preventDefault();
				if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('updatebtn')==0 ){
					alert('Sorry! you have not update rights..........');
				}else if($('#voucher_type_hidden').val()=='new' && $('.btnSave').data('insertbtn')==0){
					alert('Sorry! you have not insert rights..........');
				}else{
					self.initSave();
				}

			});

			$('.btnReset').on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			$('.btnSearch').on('click', function(e) {
				e.preventDefault();
				self.initSearch();
			});
			$('#name_dropdown').select2();
			$('#pid_dropdown').select2();

			$('#pid_dropdown').on('change', function() {
				var pid = $(this).val();
				// $('#name_dropdown').val(pid);
				$("#name_dropdown").select2("val", pid); //set the value
			});
			$('#name_dropdown').on('change', function() {

				var pid = $(this).val();
				// $('#pid_dropdown').val(pid);
				$("#pid_dropdown").select2("val", pid);
			});
			
			shortcut.add("F10", function() {
				$('.btnSave').trigger('click');
			});
			shortcut.add("F1", function() {
				$('a[href="#party-lookup"]').trigger('click');
			});
			shortcut.add("F9", function() {
				Print_Voucher(1);
			});
			shortcut.add("F6", function() {
				$('#txtId').focus();
    			// alert('focus');
    		});
			shortcut.add("F5", function() {
				self.resetVoucher();
			});

			shortcut.add("F12", function() {
				$('.btnDelete').trigger('click');
			});
			// shortcut.add("enter", function(e) {
   //  			e.preventDefaulte();
			// });


$('#cpv').on('click', function() {
	self.resetVoucher();
	var check = $(this).is(':checked');
	if (check) {
		getMaxId('cpv');
		// $('#txtetype').val('cpv');
	}
});
// $('#txtetype').val('cpv');
$('#txtAmount').on('blur',function(){
	$('#btnAddCash').focus();
});
$('#crv').on('click', function() {
	self.resetVoucher();
	var check = $(this).is(':checked');
	if (check) {
		getMaxId('crv');
		// $('#txtetype').val('crv');
	}
});
$('input[name="vrEtype"]:checked').on('change',function(event)
{
	var etype = $('input[name="vrEtype"]:checked').val();
	$('#txtetype').val(etype);
});

$('#btnAddCash').on('click', function(e) {
	e.preventDefault();

	var pid = $('#pid_dropdown').val();
	var name = $('#name_dropdown').find('option:selected').text();
	var remarks = $('#txtRemarks').val();
	var inv = $('#txtInvNo').val();
	var amount = $('#txtAmount').val();
	

	var error = validateEntry();
	if (!error) {

		setNetAmount(amount);
		appendToTable(pid, name, remarks, inv, amount);
		$('#pid_dropdown').select2('val','');
		$('#name_dropdown').select2('val','');
		$('#txtRemarks').val('');
		$('#txtInvNo').val('');
		$('#txtAmount').val('');
		$('#pid_dropdown').select2('open');
	}
});

			// when btnRowRemove is clicked
			$('#cash_table').on('click', '.btnRowRemove', function(e) {
				e.preventDefault();

				var amnt = $.trim($(this).closest('tr').find('td.amnt').text());

				setNetAmount("-"+amnt);
				$(this).closest('tr').remove();
			});

			$('#cash_table').on('click', '.btnRowEdit', function(e) {
				e.preventDefault();

				var pid = $.trim($(this).closest('tr').find('td.pid').text());
				var name = $.trim($(this).closest('tr').find('td.name').text());
				var remarks = $.trim($(this).closest('tr').find('td.remarks').text());
				var inv = $.trim($(this).closest('tr').find('td.inv').text());
				var amnt = $.trim($(this).closest('tr').find('td.amnt').text());

				setNetAmount("-"+amnt);
				$('#pid_dropdown').val(pid);
				$('#name_dropdown').val(pid);
				$('#txtRemarks').val(remarks);
				$('#txtInvNo').val(inv);
				$('#txtAmount').val(amnt);
				$(this).closest('tr').remove();
			});

			$('#txtId').on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {
					e.preventDefault();
					// get the based on the id entered by the user
					if ( $('#txtId').val().trim() !== "" ) {

						var dcno = $.trim($('#txtId').val());
						var etype = ($('#cpv').is(':checked') === true) ? 'cpv' : 'crv';
						fetch(dcno, etype);
					}
				}
			});
			$('#txtId').on('change', function(e) {
				e.preventDefault();
					// get the based on the id entered by the user
					if ( $('#txtId').val().trim() !== "" ) {

						var dcno = $.trim($('#txtId').val());
						var etype = ($('#cpv').is(':checked') === true) ? 'cpv' : 'crv';
						fetch(dcno, etype);
					}
				});

			$('.btnDelete').on('click', function(e){
				if ( $('.btnSave').data('deletebtn')==0 ){
					alert('Sorry! you have not delete rights..........');
				}else{
					e.preventDefault();
					var dcno = $('#txtId').val();
					var etype = ($('#cpv').is(':checked') === true) ? 'cpv' : 'crv';
					if (confirm('Are you sure to delete this voucher?'))
						deleteVoucher(dcno, etype);
				}
			});
			
			$('.btnPrint').on('click', function(e){
				e.preventDefault();
				Print_Voucher(1,'lg');
			});
			$('.envelopePrint').on('click', function(e){
				// e.preventDefault();
				// window.open(base_url + 'application/views/reportprints/gatepassinward.php', "Purchase Invoice", 'width=1210, height=842');
			
					window.open(base_url + 'application/views/reportprints/envelope.php', "Inward", 'width=1210, height=842');
			});
			
			$('.btnprintwithOutHeader').on('click', function(e) {
				e.preventDefault();
				Print_Voucher(0,'lg');
			});
			$('.btnprint_sm').on('click', function(e){
				e.preventDefault();
				Print_Voucher(1,'sm');
			});
			$('.btnprint_sm_withOutHeader').on('click', function(e) {
				e.preventDefault();
				Print_Voucher(0,'sm');
			});


			$('#search_cash_table').on('click', '.btnRowEdit', function(e) {
				e.preventDefault();		// prevent the default behaviour of the link
				var dcno = $.trim($(this).closest('tr').find('td.dcno').text());
				var etype = $.trim($(this).closest('tr').find('td.dcno').data('etype'));
				fetch(dcno, etype);		// get the fee category detail by id

				$('a[href="#addupdateCash"]').trigger('click');
			});
			// $('select option:first-child').attr("selected", "selected");
			// $('#cash_dropdown option:nth-child(2)');
			// $("#cash_dropdown option:first-child");
			$("#cash_dropdown").prop("selectedIndex", 1);

			var update = $('.txtidupdate').data('txtidupdate');
			if (update == 0 ) {
				$('#searchcash').hide();
				$('.nav-pills').find('a[href="#searchcash"]').hide();
			}
			$('.form-control').keypress(function (e) {

				if (e.which == 13) {
					e.preventDefault();
				}
			});

			
		},

		initSearch : function() {

			var from = $('#from_date').val();
			var to = $('#to_date').val();
			var etype = ($('#scpv').is(':checked') === true) ? 'cpv' : 'crv';

			search(from, to, etype);
		},
		Populate_PartyGrid : function() {

			if (typeof payment.dTable != 'undefined') {
				payment.dTable.fnDestroy();
				$('#partyRows').empty();
			}

			$.ajax({
				url: base_url + 'index.php/account/fetchAll',
				type: 'POST',
				dataType : 'JSON',

				complete : function (){
					$(".to_hide_phone").append("<div class='row-fluid'>"+
						"<div class='advanced-options'>" +
						"<div class='container-fluid'>" +
						"<div class='row-fluid mb10'>" +
						"<div class='span4'><select class='input-block-level fils-select' id='name-filter'><option value='-1' selected disabled>Chose Party</option></select></div>" +
						"<div class='span4'><select class='input-block-level fils-select' id='address-filter'><option value='-1' selected disabled>Chose Address</option></select></div>" +
						"<div class='span4'><select class='input-block-level fils-select' id='level1-filter'><option value='-1' selected disabled>Chose Level 1</option></select></div>" +
						"</div>" +
						"<div class='row-fluid mb10'>" +
						"<div class='span4'><select class='input-block-level fils-select' id='level2-filter'><option value='-1' selected disabled>Chose Level 2</option></select></div>" +
						"<div class='span4'><select class='input-block-level fils-select' id='level3-filter'><option value='-1' selected disabled>Chose Level 3</option></select></div>" +
						"</div>" +
						"<div class='row-fluid mb10'>" +
						"<div class='span2'><input type=\"button\" style='margin:2px;' class='btn btn-success srch-adva-filter' value='Search' /><input type=\"button\" style='margin:2px;' class='btn btn-info rst-adva-filter' value='Reset'/></div>" +
						"<div class='span10'></div>" +
						"</div>" +
						"</div>" +
						"</div>"+
						"</div>");	

$('.dataTables_filter input').attr("placeholder", "Type to search");
$('.dataTables_filter input').after("<a class='btn btn-info search-btn'><i class='icon-search'></i></a><a href='#' class='btn btn-advanced'>Advanced Filter</a>");

$(".search-btn").on("click", function () {
	var filterString = $('.dataTables_filter :input').val();
	payment.dTable.fnFilter(filterString);
});

$(".fils-select").on("change", function () {
	$(this).parent("div").siblings("div").find(".fils-select").val("-1");
	$(this).parent("div").parent("div").siblings("div").find(".fils-select").val("-1");
});

var nodes = payment.dTable.fnGetNodes();
var partyNames = new Array();
var partyNamesOptions = "";

var partyAddresses = new Array();
var partyAddressesOptions = "";

var partyLevel1 = new Array();
var partyLevel1Options = "";

var partyLevel2 = new Array();
var partyLevel2Options = "";

var partyLevel3 = new Array();
var partyLevel3Options = "";

$(nodes).each(function (index, elem) {
	var name = $(elem).find(".party-name-filter").html().trim();
	var address = $(elem).find(".party-address-filter").html().trim();
	var level1 = $(elem).find(".level1-name-filter").html().trim();
	var level2 = $(elem).find(".level2-name-filter").html().trim();
	var level3 = $(elem).find(".level3-name-filter").html().trim();

	if (($.inArray(name, partyNames) == -1) && (name != '')) {
		partyNames.push(name);
		partyNamesOptions += "<option value='" + name + "'>" + name + "</option>";
	}

	if (($.inArray(address, partyAddresses) == -1) && (address != '')) {
		partyAddresses.push(address);
		partyAddressesOptions += "<option value='" + address + "'>" + address + "</option>";
	}

	if (($.inArray(level1, partyLevel1) == -1) && (level1 != '')) {
		partyLevel1.push(level1);
		partyLevel1Options += "<option value='" + level1 + "'>" + level1 + "</option>";
	}

	if (($.inArray(level2, partyLevel2) == -1) && (level2 != '')) {
		partyLevel2.push(level2);
		partyLevel2Options += "<option value='" + level2 + "'>" + level2 + "</option>";
	}

	if (($.inArray(level3, partyLevel3) == -1) && (level3 != '')) {
		partyLevel3.push(level3);
		partyLevel3Options += "<option value='" + level3 + "'>" + level3 + "</option>";
	}
});

$("#name-filter").html($("#name-filter").html() + partyNamesOptions);
$("#address-filter").html($("#address-filter").html() + partyAddressesOptions);
$("#level1-filter").html($("#level1-filter").html() + partyLevel1Options);
$("#level2-filter").html($("#level2-filter").html() + partyLevel2Options);
$("#level3-filter").html($("#level3-filter").html() + partyLevel3Options);

						                    ////////////////////////////////////////////////////////////////

						                    $(".srch-adva-filter").on("click", function () {
						                    	if (($("#name-filter").val() !== -1) && $("#name-filter").val() !== null) {
						                    		payment.dTable.fnFilter($("#name-filter").val());
						                    	}
						                    	if (($("#address-filter").val() != -1) && ($("#address-filter").val() != null)) {
						                    		payment.dTable.fnFilter($("#address-filter").val());
						                    	}
						                    	if (($("#level1-filter").val() != -1) && ($("#level1-filter").val() != null)) {
						                    		payment.dTable.fnFilter($("#level1-filter").val());
						                    	}
						                    	if (($("#level2-filter").val() != -1) && ($("#level2-filter").val() != null)) {
						                    		payment.dTable.fnFilter($("#level2-filter").val());
						                    	}
						                    	if (($("#level3-filter").val() != -1) && ($("#level3-filter").val() != null)) {
						                    		payment.dTable.fnFilter($("#level3-filter").val());
						                    	}
						                    });

$(".rst-adva-filter").on("click", function () {
	$("#name-filter").val("-1");
	$("#address-filter").val("-1");
	$("#level1-filter").val("-1");
	$("#level2-filter").val("-1");
	$("#level3-filter").val("-1");

	payment.dTable.fnFilter("");
	$("#datatable_example_filter input[type='text']").val("");
});

payment.dTable.fnDraw();

$(".btn.btn-advanced").on("click", function () {
	if ($(".advanced-options").is(":hidden"))
		$(".advanced-options").show("slow");
	else
		$(".advanced-options").hide("slow");
});

$('.filter-search-btn').on("click", function () {
	payment.dTable.fnFilter($('#table-search-filter').val());
	payment.dTable.fnFilter()
	payment.dTable.fnFilter("%" + $('#table-search-filter').val() + "%", 2, true, false);
});				                    

},

success : function (data) {

	if (data.length !== 0) {

		$(data).each(function(index,elem){

													elem.PDATE = '2014/10/01';//elem.PDATE.substring(0,10);

													var source   = $("#prow-template").html();
													var template = Handlebars.compile(source);
													var html = template(elem);

													$('#partyRows').append(html);
												});
	}
	else{
		//alert("No record found.");
	}

	payment.bindTableGrid();

},
error : function (error){
	alert("Error : " + error);
}
});		

},

fetchRequestedVr : function () {
	var vrnoa = general.getQueryStringVal('vrnoa');
	var etype = general.getQueryStringVal('etype');

	vrnoa = parseInt( vrnoa );

	if ( !isNaN(vrnoa) && etype !== '' ) {

		if (etype=='crv'){
			$('#crv').prop('checked', true);	
		}else{
			$('#cpv').prop('checked', true);	
		}
		fetch(vrnoa, etype);
	}else{
		getMaxId(($('#cpv').is(':checked') === true) ? 'cpv' : 'crv');
	}
},

bindTableGrid : function() {
						        			            // $("input[type=checkbox], input:radio, input:file").uniform();
						        			            var dontSort = [];
						        			            $('#datatable_example thead th').each(function () {
						        			            	if ($(this).hasClass('no_sort')) {
						        			            		dontSort.push({ "bSortable": false });
						        			            	} else {
						        			            		dontSort.push(null);
						        			            	}
						        			            });
						        			            payment.dTable = $('#datatable_example').dataTable({
						        			                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
						        			                //"sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
						        			                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone'<'row-fluid'<'span8' f>>>'<'pag_top' p>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
						        			                "aaSorting": [[0, "asc"]],
						        			                "bPaginate": true,
						        			                "sPaginationType": "full_numbers",
						        			                "bJQueryUI": false,
						        			                "aoColumns": dontSort,
						        			                "oLanguage": { "sSearch": "" },
						        			                "fnDrawCallback": function (oSettings) {
						        			                }
						        			            });
$.extend($.fn.dataTableExt.oStdClasses, {
	"s`": "dataTables_wrapper form-inline"
});

},		
bindModalPartyGrid : function() {


	var dontSort = [];
	$('#party-lookup table thead th').each(function () {
		if ($(this).hasClass('no_sort')) {
			dontSort.push({ "bSortable": false });
		} else {
			dontSort.push(null);
		}
	});
	payment.pdTable = $('#party-lookup table').dataTable({
				                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
				                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
				                "aaSorting": [[0, "asc"]],
				                "bPaginate": true,
				                "sPaginationType": "full_numbers",
				                "bJQueryUI": false,
				                "aoColumns": dontSort

				            });
	$.extend($.fn.dataTableExt.oStdClasses, {
		"s`": "dataTables_wrapper form-inline"
	});
},
				        // },
				        initSaveAccount : function() {

				        	var saveObjAccount = getSaveObjectAccount();
				        	var error = validateSaveAccount();

				        	if (!error) {
				        		saveAccount(saveObjAccount);
				        	} else {
				        		alert('Correct the errors...');
				        	}
				        },
		// prepares the data to save it into the database
		initSave : function() {

			var error = validateSave();

			if (!error) {

				var rowsCount = $('#cash_table').find('tbody tr').length;

				if (rowsCount > 0 ) {
					var dcno =0;
					var etype = ($('#cpv').is(':checked') === true) ? 'cpv' : 'crv';
					var saveObj = getSaveObject();
					if ($('#voucher_type_hidden').val() =='new'){
						getMaxId(etype);
					}
					dcno = $('#vrnoa_all_hidden').val();
					
					save( saveObj, dcno, etype );
				} else {
					alert('No data found.');
				}
			} else {
				alert('Correct the errors...');
			}
		},
		


		// resets the voucher to its default state
		resetVoucher : function() {

			$('.inputerror').removeClass('inputerror');
			$('#cur_date').datepicker('update', new Date());
			$('#cash_table').find('tbody tr').remove();
			$('#txtNetAmount').val('');
			$('#search_cash_table').find('tbody tr').remove();
			$('#pid_dropdown').select2('open');
			$('#vrnoa_all_hidden').val('0');
			$('#voucher_type_hidden').val('new');
			$('#cash_dropdown').val('');
			// $('#pid_dropdown').css('backgroundcolor','red');
			getMaxId(($('#cpv').is(':checked') === true) ? 'cpv' : 'crv');
			general.setPrivillages();

		}
	}

};

var payment = new payment();
payment.init();