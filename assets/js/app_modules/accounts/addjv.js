var addjv = function() {
	var saveAccount = function( accountObj ) {
		$.ajax({
			url : base_url + 'index.php/account/save',
			type : 'POST',
			data : { 'accountDetail' : accountObj },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'false') {
					alert('An internal error occured while saving account. Please try again.');
				} else {
					alert('Account saved successfully.');
					$('#AccountAddModel').modal('hide');
					fetchAccount();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	
var validateSaveAccount = function() {

		var errorFlag = false;
		var partyEl = $('#txtAccountName');
		var deptEl = $('#txtLevel3');

		// remove the error class first
		$('.inputerror').removeClass('inputerror');

		if ( !partyEl.val() ) {
			$('#txtAccountName').addClass('inputerror');
			errorFlag = true;
		}
		if ( !deptEl.val() ) {
			deptEl.addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var fetchAccount = function() {

		$.ajax({
			url : base_url + 'index.php/account/fetchAll',
			type : 'POST',
			data : { 'active' : 1 },
			dataType : 'JSON',
			success : function(data) {
				if (data === 'false') {
					alert('No data found');
				} else {
					populateDataAccount(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var populateDataAccount = function(data) {
		$("#name_dropdown").empty();
		$("#pid_dropdown").empty();

		$.each(data, function(index, elem){
			var opt="<option value='"+elem.pid+"' >" +  elem.name + "</option>";
			$(opt).appendTo('#name_dropdown');
			
			var opt1="<option value='"+elem.pid+"' >" +  elem.pid + "</option>";
			$(opt1).appendTo('#pid_dropdown');
		});
	}

	var save = function( saveObj, dcno ) {
		general.disableSave();
		$.ajax({
			url : base_url + 'index.php/jv/save',
			type : 'POST',
			data : { 'saveObj' : saveObj, 'dcno' : dcno, 'etype' : 'jv','voucher_type_hidden': $('#voucher_type_hidden').val() , 'vrdate' : $('#vrdate').val(), 'chk_date' : $('#cur_date').val()},
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'date close') {
					alert('Sorry! you can not insert update in close date................');
				}else if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					// general.ShowAlert('save');
					var printConfirmation = confirm('Voucher saved!\nWould you like to print the invoice as well?');
					if (printConfirmation === true) {
						Print_Voucher();
						$('#cash_table').find('tbody tr').remove();
						$('#txtNetDebit').val('');
						$('#txtNetCredit').val('');
						getMaxId();
					} else {
						general.reloadWindow();
					}
				}
				general.enableSave();
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var Print_Voucher = function( ) {
		var etype= 'jv';
		var dcno = $('#txtIdHidden').val();
		var companyid = $('#cid').val();
		var user = $('#uname').val();
		// alert('etype  ' +  etype  +' dcno '+ dcno + 'company_id' + companyid );
		var url = base_url + 'index.php/doc/JvVocuherPrintPdf/' + etype + '/' + dcno   + '/' + companyid + '/' + '-1' + '/' + user;
		window.open(url);
	}


	// gets the max id of the voucher
	var getMaxId = function() {

		$.ajax({

			url : base_url + 'index.php/jv/getMaxId',
			type : 'POST',
			data : { 'company_id': $('#cid').val()},
			dataType : 'JSON',
			success : function(data) {

				$('#txtId').val(data);
				$('#txtMaxIdHidden').val(data);
				$('#txtIdHidden').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var validateEntry = function() {


		var errorFlag = false;
		var name = $('#name_dropdown').val();
		var debit = $('#txtDebit').val();
		var credit = $('#txtCredit').val();

		// remove the error class first
		$('#name_dropdown').removeClass('inputerror');
		$('#txtDebit').removeClass('inputerror');
		$('#txtCredit').removeClass('inputerror');

		if ( name === '' || name === null ) {
			$('#name_dropdown').addClass('inputerror');
			errorFlag = true;
		}

		if ( (debit === '' || debit === null) && (credit === '' || credit === null)) {
			$('#txtDebit').addClass('inputerror');
			errorFlag = true;
		}

		if ( (credit === '' || credit === null) && (debit === '' || debit === null)) {
			$('#txtCredit').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var setNetAmount = function(debit, credit) {
		
		var _debit = ($('#txtNetDebit').val() === '') ? 0 : $('#txtNetDebit').val();
		var _credit = ($('#txtNetCredit').val() === '') ? 0 : $('#txtNetCredit').val();

		debit = (debit == '') ? 0 : debit;
		credit = (credit == '') ? 0 : credit;

		var netDebit = parseFloat(debit) + parseFloat(_debit);
		var netCredit = parseFloat(credit) + parseFloat(_credit);
		$('#txtNetDebit').val(netDebit);
		$('#txtNetCredit').val(netCredit);
	}

	var appendToTable = function(pid, name, remarks, inv, debit, credit) {

		var row = "";
		row = 	"<tr> <td class='pid'> "+ pid +"</td> <td class='name'> "+ name +"</td> <td class='remarks'> "+ remarks +"</td> <td class='inv'> "+ inv +"</td> <td class='debit'> "+ debit +"</td> <td class='credit'> "+ credit +"</td> <td class='text-center'><a href='' class='btn btn-primary btnRowEdit'><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td> </tr>";
		$(row).appendTo('#cash_table');
	}

	var getCashPartyId = function() {
		var pid = "";
		$('#name_dropdown option').each(function() { if ($(this).text().trim().toLowerCase() == 'cash') pid = $(this).val();  });
		return pid;
	}

	var getSaveObject = function() {

		var _date = $('#cur_date').val();
		var _vrno = $('#txtIdHidden').val();
		var _uid = $('#uid').val();
		var _company_id = $('#cid').val();

		var ledgers = [];

		$('#cash_table').find('tbody tr').each(function() {

			var _pid = $.trim($(this).closest('tr').find('td.pid').text());
			var _name = $.trim($(this).closest('tr').find('td.name').text());
			var _remarks = $.trim($(this).closest('tr').find('td.remarks').text());
			var _inv = $.trim($(this).closest('tr').find('td.inv').text());
			var _debit = $.trim($(this).closest('tr').find('td.debit').text());
			var _credit = $.trim($(this).closest('tr').find('td.credit').text());


			var pledger = {};

			pledger.pledid = '';
			pledger.pid = _pid;
			pledger.description = _remarks;
			pledger.remarks =  _remarks;
			pledger.date = _date;
			pledger.invoice = _inv;
			pledger.debit = _debit;
			pledger.credit = _credit;
			pledger.dcno = _vrno;
			pledger.etype = 'jv';
			pledger.pid_key = _pid;
			pledger.uid=_uid;
			pledger.company_id=_company_id;
			ledgers.push(pledger);
		});

		return ledgers;
	}

	var saveAccount = function( accountObj ) {
		$.ajax({
			url : base_url + 'index.php/account/save',
			type : 'POST',
			data : { 'accountDetail' : accountObj },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'false') {
					alert('An internal error occured while saving account. Please try again.');
				} else {
					alert('Account saved successfully.');
					$('#AccountAddModel').modal('hide');
					fetchAccount();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	
var validateSaveAccount = function() {

		var errorFlag = false;
		var partyEl = $('#txtAccountName');
		var deptEl = $('#txtLevel3');

		// remove the error class first
		$('.inputerror').removeClass('inputerror');

		if ( !partyEl.val() ) {
			$('#txtAccountName').addClass('inputerror');
			errorFlag = true;
		}
		if ( !deptEl.val() ) {
			deptEl.addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var fetchAccount = function() {

		$.ajax({
			url : base_url + 'index.php/account/fetchAll',
			type : 'POST',
			data : { 'active' : 1 },
			dataType : 'JSON',
			success : function(data) {
				if (data === 'false') {
					alert('No data found');
				} else {
					populateDataAccount(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var populateDataAccount = function(data) {
		$("#name_dropdown").empty();
		$("#pid_dropdown").empty();

		$.each(data, function(index, elem){
			var opt="<option value='"+elem.pid+"' >" +  elem.name + "</option>";
			$(opt).appendTo('#name_dropdown');
			
			var opt1="<option value='"+elem.pid+"' >" +  elem.pid + "</option>";
			$(opt1).appendTo('#pid_dropdown');
		});
	}

	var getSaveObjectAccount = function() {

		var obj = {
			pid : '20000',
			active : '1',
			name : $.trim($('#txtAccountName').val()),
			level3 : $.trim($('#txtLevel3').val()),
			dcno : $('#txtId').val(),
			etype : 'cpv/crv',
			uid : $.trim($('#uid').val()),
			company_id : $.trim($('#cid').val()),
		};

		return obj;
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		var debit = $('#txtNetDebit').val();
		var credit = $('#txtNetCredit').val();

		if ( debit != credit) {
			errorFlag = true;
		}

		return errorFlag;
	}

	var search = function(from, to) {

		$.ajax({
			url : base_url + 'index.php/jv/fetchVoucherRange',
			type : 'POST',
			data : { 'from' : from, 'to' : to },
			dataType : 'JSON',
			success : function(data) {

				$('#search_cash_table tbody tr').remove();
				populateSearchData(data);

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateSearchData = function(data) {

		var rows = "";
		$.each(data, function(index, elem) {

			rows += 	"<tr> <td class='dcno' data-etype='"+ elem.etype +"'> "+ elem.dcno +"</td> <td> "+ elem.date.substr(0, 10) +"</td> <td> "+ elem.party_name +"</td> <td> "+ ((elem.debit == '0.00') ? '-' : elem.debit) +"</td> <td> "+ ((elem.credit == '0.00') ? '-' : elem.credit) +"</td> <td> "+ elem.description +"</td> <td class='text-center'><a href='' class='btn btn-primary btnRowEdit'><span class='fa fa-edit'></span></a></td> </tr>";
		});

		$(rows).appendTo('#search_cash_table tbody');
	}

	var fetch = function(dcno) {

		$.ajax({
			url : base_url + 'index.php/jv/fetch',
			type : 'POST',
			data : { 'dcno' : dcno, 'company_id':$('#cid').val() },
			dataType : 'JSON',
			success : function(data) {

				$('#cash_table').find('tbody tr').remove();
				$('#txtNetDebit').val('');
				$('#txtNetCredit').val('');

				if (data.length == 0) {
					alert('No data found');
				} else {
					populateData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateData = function(data) {

		$('#txtId').val(data[0]['dcno']);
		$('#txtIdHidden').val(data[0]['dcno']);

		$('#cur_date').datepicker('update', data[0]['date'].substring(0, 10));
		$('#vrdate').datepicker('update', data[0]['date'].substring(0, 10));
		$('#user_dropdown').val(data[0]['uid']);

		$.each(data, function(index, elem) {
			appendToTable(elem.pid, elem.party_name, elem.description, elem.invoice, parseFloat(elem.debit).toFixed(2), parseFloat(elem.credit).toFixed(2));
			setNetAmount(parseFloat(elem.debit).toFixed(2), parseFloat(elem.credit).toFixed(2));
		});
		$('#voucher_type_hidden').val('edit');
	}

	var deleteVoucher = function(dcno) {

		$.ajax({
			url : base_url + 'index.php/jv/deleteVoucher',
			type : 'POST',
			data : {'chk_date' : $('#cur_date').val(), 'vrdate' : $('#vrdate').val(), 'dcno' : dcno,'company_id':$('#cid').val() },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'date close') {
					alert('Sorry! you can not delete in close date................');
				}else if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var fetchRunningTotal = function(_pid , _to) {
		$.ajax({
			url : base_url + 'index.php/account/fetchRunningTotal',
			type : 'POST',
			data : {'to': _to, 'pid' : _pid},
			dataType : 'JSON',
			success : function(data) {
				var opbal=data[0]['RTotal'];

				$('#balance_lbl').text('Account,     Balance: '+parseFloat(opbal).toFixed(2));
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	
	return {

		init : function() {
			this.bindUI();
			// $('#pid_dropdown').select2('open');
			$('#voucher_type_hidden').val('new'); 
			addjv.bindModalPartyGrid();
		    addjv.fetchRequestedVr();
		},

		bindUI : function() {

			var self = this;

			$('#txtLevel3').on('change', function() {
				
				var level3 = $('#txtLevel3').val();
				$('#txtselectedLevel1').text('');
				$('#txtselectedLevel2').text('');
				if (level3 !== "" && level3 !== null) {
					// alert('enter' + $(this).find('option:selected').data('level2') );	
					$('#txtselectedLevel2').text(' ' + $(this).find('option:selected').data('level2'));
					$('#txtselectedLevel1').text(' ' + $(this).find('option:selected').data('level1'));
				}
			});
			// $('#txtLevel3').select2();
			$('.btnSaveM').on('click',function(e){
				
				if ( $('.btnSave').data('saveaccountbtn')==0 ){
					alert('Sorry! you have not save accounts rights..........');
				}else{
					e.preventDefault();
					self.initSaveAccount();
				}
			});
			$('.btnResetM').on('click',function(){
				
				$('#txtAccountName').val('');
				$('#txtselectedLevel2').text('');
				$('#txtselectedLevel1').text('');
				$('#txtLevel3').select2('val','');
			});
			$('#AccountAddModel').on('shown.bs.modal',function(e){
				$('#txtAccountName').focus();
			});
			shortcut.add("F3", function() {
    			$('#AccountAddModel').modal('show');
			});

			$('#name_dropdown').on('change',function(){

				var partyId = $(this).val();
				var vrDate = $('#cur_date').val();
				
				fetchRunningTotal(partyId, vrDate);
			});


			$('.modal-lookup .populateAccount').on('click', function(){
				// alert('dfsfsdf');
				var party_id = $(this).closest('tr').find('input[name=hfModalPartyId]').val();
				$("#name_dropdown").select2("val", party_id); //set the value
				$("#pid_dropdown").select2("val", party_id); //set the value

				// var partyEl = $('#drpParty');
				//party.fetchParty(party_id);
				// partyEl.val(party_id);
				// partyEl.trigger('liszt:updated');
				// alert('search party ' + party_id);
				// $('#pid_dropdown').val(party_id);
				// $('#name_dropdown').val(party_id);
			});


			$('.btnSave').on('click',  function(e) {
				
				if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('updatebtn')==0 ){
					alert('Sorry! you have not update rights..........');
				}else if($('#voucher_type_hidden').val()=='new' && $('.btnSave').data('insertbtn')==0){
					alert('Sorry! you have not insert rights..........');
				}else{
					e.preventDefault();
					self.initSave();
				}
			});

			$('.btnReset').on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});
			$('.btnPrint').on('click', function(e){
				e.preventDefault();
				Print_Voucher();
			});

			$('#pid_dropdown').on('change', function() {

				var pid = $(this).val();
				$("#name_dropdown").select2("val", pid);
			});
			$('#name_dropdown').on('change', function() {
				var pid = $(this).val();
				$("#pid_dropdown").select2("val", pid);
			});

			shortcut.add("F10", function() {
    			self.initSave();
			});
			shortcut.add("F1", function() {
				$('a[href="#party-lookup"]').trigger('click');
			});
			shortcut.add("F9", function() {
				Print_Voucher();
			});
			shortcut.add("F6", function() {
    			$('#txtId').focus();
    			// alert('focus');
			});
			shortcut.add("F5", function() {
    			self.resetVoucher();
			});

			shortcut.add("F12", function() {
    			$('.btnDelete').trigger('click');
			});
			$('#name_dropdown').select2();
			$('#pid_dropdown').select2();


			$('.btnSearch').on('click', function(e) {
				e.preventDefault();
				self.initSearch();
			});

			$('#btnAddCash').on('click', function(e) {
				e.preventDefault();

				var pid = $('#pid_dropdown').val();
				var name = $('#name_dropdown').find('option:selected').text();
				var remarks = $('#txtRemarks').val();
				var inv = $('#txtInvNo').val();
				var debit = $('#txtDebit').val();
				var credit = $('#txtCredit').val();

				var error = validateEntry();
				if (!error) {

					setNetAmount(debit, credit);
					appendToTable(pid, name, remarks, inv, debit, credit);
					$('#pid_dropdown').select2('val','');
					$('#name_dropdown').select2('val','');
					$('#txtRemarks').val('');
					$('#txtInvNo').val('');
					$('#txtDebit').val('');
					$('#txtCredit').val('');
					$('#pid_dropdown').select2('open');
				}
			});

			// when btnRowRemove is clicked
			$('#cash_table').on('click', '.btnRowRemove', function(e) {
				e.preventDefault();

				var debit = $.trim($(this).closest('tr').find('td.debit').text());
				var credit = $.trim($(this).closest('tr').find('td.credit').text());

				debit = (debit == '') ? 0 : debit;
				credit = (credit == '') ? 0 : credit;

				setNetAmount("-"+debit, "-"+credit);
				$(this).closest('tr').remove();
			});

			$('#cash_table').on('click', '.btnRowEdit', function(e) {
				e.preventDefault();

				var pid = $.trim($(this).closest('tr').find('td.pid').text());
				var name = $.trim($(this).closest('tr').find('td.name').text());
				var remarks = $.trim($(this).closest('tr').find('td.remarks').text());
				var inv = $.trim($(this).closest('tr').find('td.inv').text());
				var debit = $.trim($(this).closest('tr').find('td.debit').text());
				var credit = $.trim($(this).closest('tr').find('td.credit').text());

				debit = (debit == '') ? 0 : debit;
				credit = (credit == '') ? 0 : credit;

				setNetAmount("-"+debit, "-"+credit);
				$('#pid_dropdown').val(pid);
				$('#name_dropdown').val(pid);
				$('#txtRemarks').val(remarks);
				$('#txtInvNo').val(inv);
				$('#txtDebit').val(debit);
				$('#txtCredit').val(credit);
				$(this).closest('tr').remove();
			});

			$('#txtId').on('change', function(e) {
				// get the based on the id entered by the user
				if ( $('#txtId').val().trim() !== "" ) {
					e.preventDefault();
					var dcno = $.trim($('#txtId').val());
					fetch(dcno);
				}
			});

			$('#txtId').on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {
					e.preventDefault();
					// get the based on the id entered by the user
					if ( $('#txtId').val().trim() !== "" ) {

						var dcno = $.trim($('#txtId').val());
						fetch(dcno);
					}
				}
			});


			$('.btnDelete').on('click', function(e){
				if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('deletebtn')==0 ){
					alert('Sorry! you have not delete rights..........');
				}else{
					e.preventDefault();
					var dcno = $('#txtId').val();
					if (dcno !== '') {
					if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('deletebtn')==0 ){
						alert('Sorry! you have not delete rights..........');
					}else{
						e.preventDefault();
						var vrnoa = $('#txtVrnoaHidden').val();
						if (vrnoa !== '') {
						if (confirm('Are you sure to delete this voucher?'))
						deleteVoucher(dcno);
					}
				}
			}}
			});

			$('#search_cash_table').on('click', '.btnRowEdit', function(e) {
				e.preventDefault();		// prevent the default behaviour of the link
				var dcno = $.trim($(this).closest('tr').find('td.dcno').text());
				var etype = $.trim($(this).closest('tr').find('td.dcno').data('etype'));
				fetch(dcno, etype);		// get the fee category detail by id

				$('a[href="#addupdateJV"]').trigger('click');
			});

			
		},

			initSaveAccount : function() {

			var saveObjAccount = getSaveObjectAccount();
			var error = validateSaveAccount();

			if (!error) {
					saveAccount(saveObjAccount);
			} else {
				alert('Correct the errors...');
			}
		},
		fetchRequestedVr : function () {
			var vrnoa = general.getQueryStringVal('vrnoa');
			vrnoa = parseInt( vrnoa );

			if ( !isNaN(vrnoa) ) {
				fetch(vrnoa);
			}else{
				getMaxId();
			}
		},


		// prepares the data to save it into the database
		initSave : function() {

			var error = validateSave();

			if (!error) {

				var rowsCount = $('#cash_table').find('tbody tr').length;

				if (rowsCount > 0 ) {
					var dcno =0;
					var saveObj = getSaveObject();
					if ($('#voucher_type_hidden').val() =='new'){
						  getMaxId();
					}
					dcno = $('#txtIdHidden').val();
					save( saveObj, dcno );
				} else {
					alert('No data found.');
				}
			} else {
				alert('Error! Debit side must be equal to credit side.');
			}
		},
		bindModalPartyGrid : function() {

			
				            var dontSort = [];
				            $('#party-lookup table thead th').each(function () {
				                if ($(this).hasClass('no_sort')) {
				                    dontSort.push({ "bSortable": false });
				                } else {
				                    dontSort.push(null);
				                }
				            });
				            addjv.pdTable = $('#party-lookup table').dataTable({
				                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
				                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
				                "aaSorting": [[0, "asc"]],
				                "bPaginate": true,
				                "sPaginationType": "full_numbers",
				                "bJQueryUI": false,
				                "aoColumns": dontSort

				            });
				            $.extend($.fn.dataTableExt.oStdClasses, {
				                "s`": "dataTables_wrapper form-inline"
				            });
},

		initSearch : function() {

			var from = $('#from_date').val();
			var to = $('#to_date').val();
			search(from, to, 'jv');
		},

		// resets the voucher to its default state
		resetVoucher : function() {

			// $('.inputerror').removeClass('inputerror');
			// $('#cur_date').datepicker('update', new Date());
			// $('#cash_table').find('tbody tr').remove();
			// $('#txtNetAmount').val('');
			// $('#search_cash_table').find('tbody tr').remove();

			// getMaxId('cpv');
			// general.setPrivillages();
			$('#txtIdHidden').val('0');
			$('#voucher_type_hidden').val('new');
			general.reloadWindow();
		}
	}

};

var addjv = new addjv();
addjv.init();