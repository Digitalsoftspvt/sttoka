var salereturn = function() {

    var settings = {
        // basic information section
        switchPreBal : $('#switchPreBal')

    };
    var fetchAccount = function() {

        $.ajax({
            url : base_url + 'index.php/account/fetchAll',
            type : 'POST',
            data : { 'active' : 1,'typee':'sale return'},
            dataType : 'JSON',
            success : function(data) {
                if (data === 'false') {
                    alert('No data found');
                } else {
                    populateDataAccount(data);
                }
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }
    var fetchItems = function() {
        $.ajax({
            url : base_url + 'index.php/item/fetchAll',
            type : 'POST',
            data : { 'active' : 1 },
            dataType : 'JSON',
            success : function(data) {
                if (data === 'false') {
                    alert('No data found');
                } else {
                    populateDataItem(data);
                }
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    var populateDataAccount = function(data) {
        $("#party_dropdown").empty();
        
        $.each(data, function(index, elem){
            var opt="<option value='"+elem.party_id+"' >" +  elem.name + "</option>";
            $(opt).appendTo('#party_dropdown');
        });
    }
    var populateDataItem = function(data) {
        $("#itemid_dropdown").empty();
        $("#item_dropdown").empty();

        $.each(data, function(index, elem){
            var opt="<option value='"+elem.item_id+"' data-prate= '"+ elem.cost_price +"' data-uom_item= '"+ elem.uom +"' data-grweight= '"+ elem.grweight +"' >" +  elem.item_des + "</option>";
             // var = "<option value='" + $item['item_id'] + "' data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>"><?php echo $item['item_des']; ?></option>";
            $(opt).appendTo('#item_dropdown');
            var opt1="<option value='"+elem.item_id+"' data-prate= '"+ elem.cost_price +"' data-uom_item= '"+ elem.uom +"' data-grweight= '"+ elem.grweight +"' >" +  elem.item_id + "</option>";
             // var = "<option value='" + $item['item_id'] + "' data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>"><?php echo $item['item_des']; ?></option>";
            $(opt1).appendTo('#itemid_dropdown');

        });
    }
    
    var saveItem = function( item ) {
        $.ajax({
            url : base_url + 'index.php/item/save',
            type : 'POST',
            data : item,
            // processData : false,
            // contentType : false,
            dataType : 'JSON',
            success : function(data) {

                if (data.error === 'true') {
                    alert('An internal error occured while saving voucher. Please try again.');
                } else {
                    alert('Item saved successfully.');
                    $('#ItemAddModel').modal('hide');
                    fetchItems();
                }
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    var saveAccount = function( accountObj ) {
        $.ajax({
            url : base_url + 'index.php/account/save',
            type : 'POST',
            data : { 'accountDetail' : accountObj },
            dataType : 'JSON',
            success : function(data) {

                if (data.error === 'false') {
                    alert('An internal error occured while saving account. Please try again.');
                } else {
                    alert('Account saved successfully.');
                    $('#AccountAddModel').modal('hide');
                    fetchAccount();
                }
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }
    var save = function(salereturn) {
        general.disableSave();
        $.ajax({
            url : base_url + 'index.php/salereturn/save',
            type : 'POST',
            data : { 'stockmain' : salereturn.stockmain, 'stockdetail' : salereturn.stockdetail, 'vrnoa' : salereturn.vrnoa, 'ledger' : salereturn.ledger ,'voucher_type_hidden':$('#voucher_type_hidden').val(), 'vrdate' : $('#vrdate').val() },
            dataType : 'JSON',
            success : function(data) {

                if (data.error === 'date close') {
                    alert('Sorry! you can not insert update in close date................');
                }else if (data.error === 'true') {
                    alert('An internal error occured while saving voucher. Please try again.');
                } else {
                    var printConfirmation = confirm('Voucher saved!\nWould you like to print the invoice as well?');
                    if (printConfirmation === true) {
                        Print_Voucher(1);
                        general.reloadWindow();
                    } else {
                        general.reloadWindow();
                    }
                }
                general.enableSave();
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }
    var Print_Voucher = function( hd ,prnt , wrate) {
        if ( $('.btnSave').data('printbtn')==0 ){
                alert('Sorry! you have not print rights..........');
        }else{
            var etype=  'salereturn';
            var vrnoa = $('#txtVrnoa').val();
            var company_id = $('#cid').val();
            var user = $('#uname').val();
            // var hd = $('#hd').val();
            var pre_bal_print = ($(settings.switchPreBal).bootstrapSwitch('state') === true) ? '1' : '0';
            
           // var url = base_url + 'index.php/doc/Print_Voucher/' + etype + '/' + vrnoa + '/' + company_id + '/' + '-1' + '/' + user + '/' + pre_bal_print+ '/' + hd + '/' + prnt + '/' + wrate;
            // var url = base_url + 'index.php/doc/CashVocuherPrintPdf/' + etype + '/' + dcno   + '/' + companyid + '/' + '-1' + '/' + user;
            window.open(base_url + 'application/views/reportprints/salereturn.php', "Sale Return", 'width=1210, height=842');
            //window.open(url);
        }
    }

    var fetch = function(vrnoa) {

        $.ajax({

            url : base_url + 'index.php/salereturn/fetch',
            type : 'POST',
            data : { 'vrnoa' : vrnoa , 'company_id': $('#cid').val()},
            dataType : 'JSON',
            success : function(data) {

                resetFields();
                $('#txtOrderNo').val('');
                if (data === 'false') {
                    alert('No data found.');
                    general.reloadWindow();
                } else {

                    $(".btnPrint").removeAttr("disabled").next().removeAttr("disabled");

                    populateData(data);
                }

            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    var populateData = function(data) {

        $('#txtVrno').val(data[0]['vrno']);
        $('#txtVrnoHidden').val(data[0]['vrno']);
        $('#txtVrnoaHidden').val(data[0]['vrnoa']);
        $('#txtIdImg').val(data[0]['vrnoa']);
        $('#current_date').datepicker('update', data[0]['vrdate'].substr(0, 10));
        $('#vrdate').datepicker('update', data[0]['vrdate'].substr(0, 10));
        $('#party_dropdown').select2('val', data[0]['party_id']);
        $('#txtInvNo').val(data[0]['inv_no']);
        $('#due_date').datepicker('update', data[0]['due_date'].substr(0, 10));
        $('#receivers_list').val(data[0]['received_by']);
        $('#transporter_dropdown').select2('val', data[0]['transporter_id']);
        $('#txtRemarks').val(data[0]['remarks']);
        $('#txtNetAmount').val(data[0]['namount']);
        $('#txtOrderNo').val(data[0]['order_no']);

        $('#txtDiscount').val(data[0]['discp']);
        $('#txtExpense').val(data[0]['exppercent']);
        $('#txtExpAmount').val(data[0]['expense']);
        $('#txtTax').val(data[0]['taxpercent']);
        $('#txtTaxAmount').val(data[0]['tax']);
        $('#txtDiscAmount').val(data[0]['discount']);
        $('#user_dropdown').val(data[0]['uid']);
        $('#txtPaid').val(data[0]['paid']);
        $('#txtOtherCharges').val(data[0]['othercharges']);

        $('#dept_dropdown').select2('val', data[0]['godown_id']);
        $('#voucher_type_hidden').val('edit');      
        $('#user_dropdown').val(data[0]['uid']);
        $('#expense_dropdown').select2('val', data[0]['expense_id']);

        fetchpreviousbalance($('#current_date').val(),$('#party_dropdown').val(),$('#txtVrnoa').val());
        $.each(data, function(index, elem) {
            elem.s_amount=parseFloat(elem.s_amount).toFixed(2);
            appendToTable('', elem.item_name, elem.item_id, elem.s_qty, elem.s_rate, elem.s_amount, elem.weight, elem.uitem_des, elem.item_code);
            calculateLowerTotal(elem.s_qty, elem.s_amount, elem.weight);
        });
    }

    var fetchpreviousbalance = function(date,party_id,dcno) {

        $.ajax({

            url : base_url + 'index.php/saleorder/fetchpreviousbalance',
            type : 'POST',
            data : { 'date' : date , 'party_id': party_id,'dcno':dcno, 'etype':'salereturn'},
            dataType : 'JSON',
            success : function(data) {
                    console.log(data);
                if (data === 'false') {
                    $('#txtpre_balance').val(0);

                } else {
                    $('#txtpre_balance').val(data);
                    }
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    // gets the max id of the voucher
    var getMaxVrno = function() {

        $.ajax({

            url : base_url + 'index.php/salereturn/getMaxVrno',
            type : 'POST',
            data : {'company_id': $('#cid').val()},
            dataType : 'JSON',
            success : function(data) {

                $('#txtVrno').val(data);
                $('#txtMaxVrnoHidden').val(data);
                $('#txtVrnoHidden').val(data);
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    var getMaxVrnoa = function() {

        $.ajax({

            url : base_url + 'index.php/salereturn/getMaxVrnoa',
            type : 'POST',
            data : {'company_id': $('#cid').val()},
            dataType : 'JSON',
            success : function(data) {

                $('#txtVrnoa').val(data);
                $('#txtMaxVrnoaHidden').val(data);
                $('#txtVrnoaHidden').val(data);
                $('#txtIdImg').val(data);
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    var validateSingleProductAdd = function() {


        var errorFlag = false;
        var itemEl = $('#txtItemId');
        var qtyEl = $('#txtQty');
        var rateEl = $('#txtPRate');

        // remove the error class first
        $('.inputerror').removeClass('inputerror');

        if ( !itemEl.val() ) {
            itemEl.addClass('inputerror');
            errorFlag = true;
        }
        if ( !qtyEl.val() ) {
            qtyEl.addClass('inputerror');
            errorFlag = true;
        }
        if ( !rateEl.val() ) {
            rateEl.addClass('inputerror');
            errorFlag = true;
        }

        return errorFlag;
    }

    var appendToTable = function(srno, item_desc, item_id, qty, rate, amount, weight, itemUname, itemCode) {
        var pid = '';
        $('#item_dropdown option').each(function()
        { if ($(this).val().trim().toLowerCase() == item_id)  pid =$(this).data('uitem_des');  });
        amount=parseFloat(amount).toFixed(2);
        var srno = $('#salereturn_table tbody tr').length + 1;
        var row =   "<tr>" +
                        "<td class='srno numeric' data-title='Sr#' > "+ srno +"</td>" +
                        "<td class='item_desc' data-title='Description' data-item_code='"+ itemCode +"' data-uitem_des='"+ itemUname +"' data-item_id='"+ item_id +"' data-uitem_des='"+ pid +"'> "+ item_desc +"</td>" +
                        "<td class='qty numeric' data-title='Qty'>  "+ qty +"</td>" +
                        "<td class='weight numeric' data-title='Weigh' > "+ weight +"</td>" +
                        "<td class='rate numeric' data-title='Rate'> "+ rate +"</td>" +
                        "<td class='amount numeric' data-title='Amount' > "+ amount +"</td>" +
                        "<td><a href='' class='btn btn-primary btnRowEdit'><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td>" +
                    "</tr>";
        $(row).appendTo('#salereturn_table');
    }

    var getPartyId = function(partyName) {
        var pid = "";
        $('#party_dropdown option').each(function() { if ($(this).text().trim().toLowerCase() == partyName) pid = $(this).val();  });
        return pid;
    }

    var getSaveObject = function() {

        var ledgers = [];
        var stockmain = {};
        var stockdetail = [];

        stockmain.vrno = $('#txtVrnoHidden').val();
        stockmain.vrnoa = $('#txtVrnoaHidden').val();
        stockmain.vrdate = $('#current_date').val();
        stockmain.party_id = $('#party_dropdown').val();
        stockmain.bilty_no = $('#txtInvNo').val();
        stockmain.bilty_date = $('#due_date').val();
        stockmain.received_by = $('#receivers_list').val();
        stockmain.transporter_id = $('#transporter_dropdown').val();
        stockmain.remarks = $('#txtRemarks').val();
        stockmain.etype = 'salereturn';
        stockmain.namount = $('#txtNetAmount').val();
        stockmain.order_vrno = $('#txtOrderNo').val();
        stockmain.discp = $('#txtDiscount').val();
        stockmain.discount = $('#txtDiscAmount').val();
        stockmain.expense =$('#txtExpAmount').val();
        stockmain.exppercent = $('#txtExpense').val();
        stockmain.tax = $('#txtTaxAmount').val();
        stockmain.taxpercent = $('#txtTax').val();
        stockmain.paid = $('#txtPaid').val();
        stockmain.othercharges = $('#txtOtherCharges').val();
        stockmain.expense_id=$('#expense_dropdown').val();
        stockmain.uid = $('#uid').val();
        stockmain.company_id = $('#cid').val();


        $('#salereturn_table').find('tbody tr').each(function( index, elem ) {
            var sd = {};
            sd.stid = '';
            sd.item_id = $.trim($(elem).find('td.item_desc').data('item_id'));
            sd.godown_id = $('#dept_dropdown').val();
            sd.qty = $.trim($(elem).find('td.qty').text());
            sd.weight = $.trim($(elem).find('td.weight').text());
            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = $.trim($(elem).find('td.amount').text());
            sd.netamount = $.trim($(elem).find('td.amount').text());
            stockdetail.push(sd);
        });

        ///////////////////////////////////////////////////////////////
        //// for over all voucher
        ///////////////////////////////////////////////////////////////
        
        var pledger = {};
        pledger.pledid = '';
        pledger.pid = $('#party_dropdown').val();
        pledger.description =  $('#txtRemarks').val();
        pledger.remarks =  $('#txtRemarks').val();
        pledger.date = $('#current_date').val();
        pledger.debit = 0;
        pledger.credit = $('#txtNetAmount').val();
        pledger.dcno = $('#txtVrnoaHidden').val();
        pledger.invoice = $('#txtVrnoaHidden').val();
        pledger.etype = 'salereturn';
        pledger.pid_key = $('#salereturnid').val();
        pledger.uid = $('#uid').val();
        pledger.company_id = $('#cid').val();
        pledger.isFinal = 0;    
        ledgers.push(pledger);

        var pledger = {};
        pledger.pledid = '';
        pledger.pid = $('#salereturnid').val();
        pledger.description = $('#party_dropdown').find('option:selected').text();
        pledger.remarks =  $('#txtRemarks').val();
        pledger.date = $('#current_date').val();
        pledger.debit = parseInt($('#txtTotalAmount').val());
        pledger.credit = 0;
        pledger.dcno = $('#txtVrnoaHidden').val();
        pledger.invoice = $('#txtInvNo').val();
        pledger.etype = 'salereturn';
        pledger.pid_key = $('#party_dropdown').val();
        pledger.uid = $('#uid').val();
        pledger.company_id = $('#cid').val();   
        pledger.isFinal = 0;
        ledgers.push(pledger);

        ///////////////////////////////////////////////////////////////
        //// for Discount
        ///////////////////////////////////////////////////////////////
        if ($('#txtDiscAmount').val() != 0 ) {
            pledger = undefined;
            var pledger = {};
            pledger.etype = 'salereturn';
            pledger.description = $('#party_dropdown option:selected').text();
            pledger.remarks =  $('#txtRemarks').val();
            // pledger.description = 'salereturn Head';
            pledger.dcno = $('#txtVrnoaHidden').val();
            pledger.invoice = $('#txtVrnoaHidden').val();
            pledger.pid = $('#discountid').val();
            pledger.date = $('#current_date').val();
            pledger.debit = 0;
            pledger.credit = $('#txtDiscAmount').val();
            pledger.isFinal = 0;
            pledger.uid = $('#uid').val();
            pledger.company_id = $('#cid').val();   
            pledger.pid_key = $('#party_dropdown').val();                               

            ledgers.push(pledger);
        }       
        if ($('#txtTaxAmount').val() != 0 ) {
            pledger = undefined;
            var pledger = {};
            pledger.etype = 'salereturn';
            pledger.description = $('#party_dropdown option:selected').text();
            pledger.remarks =  $('#txtRemarks').val();
            // pledger.description = 'salereturn Head';
            pledger.dcno = $('#txtVrnoaHidden').val();
            pledger.invoice = $('#txtVrnoaHidden').val();
            pledger.pid = $('#taxid').val();
            pledger.date = $('#current_date').val();
            pledger.debit = $('#txtTaxAmount').val();
            pledger.credit = 0;
            pledger.isFinal = 0;
            pledger.uid = $('#uid').val();
            pledger.company_id = $('#cid').val();   
            pledger.pid_key = $('#party_dropdown').val();
            ledgers.push(pledger);
        }
        if ($('#txtExpAmount').val() != 0 ) {
            pledger = undefined;
            var pledger = {};
            pledger.etype = 'salereturn';
            pledger.description = $('#party_dropdown option:selected').text();
            pledger.remarks =  $('#txtRemarks').val();
            // pledger.description = 'salereturn Head';
            pledger.dcno = $('#txtVrnoaHidden').val();
            pledger.invoice = $('#txtVrnoaHidden').val();
            pledger.pid = $('#expense_dropdown').val();
            pledger.date = $('#current_date').val();
            pledger.debit = 0;
            pledger.credit =$('#txtExpAmount').val();
            pledger.isFinal = 0;
            pledger.uid = $('#uid').val();
            pledger.company_id = $('#cid').val();   
            pledger.pid_key = $('#salereturnid').val();
            ledgers.push(pledger);
        }
        if ($('#txtPaid').val() != 0 ) {
            pledger = undefined;
            var pledger = {};
            pledger.etype = 'salereturn';
            pledger.description = $('#party_dropdown option:selected').text() + '.';
            pledger.remarks =  $('#txtRemarks').val();
            // pledger.description = 'salereturn Head';
            pledger.dcno = $('#txtVrnoaHidden').val();
            pledger.invoice = $('#txtVrnoaHidden').val();
            pledger.pid = $('#cashid').val();
            pledger.date = $('#current_date').val();
            pledger.debit = 0;
            pledger.credit = $('#txtPaid').val();
            pledger.isFinal = 0;
            pledger.uid = $('#uid').val();
            pledger.company_id = $('#cid').val();   
            pledger.pid_key = $('#party_dropdown').val();
            ledgers.push(pledger);

            pledger = undefined;
            var pledger = {};
            pledger.etype = 'salereturn';
            pledger.description =  'Cash Paid';
            pledger.remarks =  $('#txtRemarks').val();
            // pledger.description = 'salereturn Head';
            pledger.dcno = $('#txtVrnoaHidden').val();
            pledger.invoice = $('#txtVrnoaHidden').val();
            pledger.pid = $('#party_dropdown').val();
            pledger.date = $('#current_date').val();
            pledger.debit = $('#txtPaid').val();
            pledger.credit = 0;
            pledger.isFinal = 0;
            pledger.uid = $('#uid').val();
            pledger.company_id = $('#cid').val();   
            pledger.pid_key = $('#cashid').val();
            ledgers.push(pledger);

        }
        var data = {};
        data.stockmain = stockmain;
        data.stockdetail = stockdetail;
        data.ledger = ledgers;
        data.vrnoa = $('#txtVrnoaHidden').val();

        return data;
    }
var getSaveObjectAccount = function() {

        var obj = {
            pid : '20000',
            active : '1',
            name : $.trim($('#txtAccountName').val()),
            level3 : $.trim($('#txtLevel3').val()),
            dcno : $('#txtVrnoa').val(),
            etype : 'purchase',
            uid : $.trim($('#uid').val()),
            company_id : $.trim($('#cid').val()),
        };

        return obj;
    }
    var getSaveObjectItem = function() {
        
        var itemObj = {
            item_id : 20000,
            active : '1',
            open_date : $.trim($('#current_date').val()),
            catid : $('#category_dropdown').val(),
            subcatid : $.trim($('#subcategory_dropdown').val()),
            bid : $.trim($('#brand_dropdown').val()),
            barcode : $.trim($('#txtBarcode').val()),
            description : $.trim($('#txtItemName').val()),
            item_des : $.trim($('#txtItemName').val()),
            cost_price : $.trim($('#txtPurPrice').val()),
            srate : $.trim($('#txtSalePrice').val()),
            uid : $.trim($('#uid').val()),
            company_id : $.trim($('#cid').val()),
            uom : $.trim($('#uom_dropdown').val()),
        };
        return itemObj;
    }
    var validateSaveItem = function() {

        var errorFlag = false;
        // var _barcode = $('#txtBarcode').val();
        var _desc = $.trim($('#txtItemName').val());
        var cat = $.trim($('#category_dropdown').val());
        var subcat = $('#subcategory_dropdown').val();
        var brand = $.trim($('#brand_dropdown').val());
        var uom_ = $.trim($('#uom_dropdown').val());
        // remove the error class first
        
        $('.inputerror').removeClass('inputerror');
        if ( !uom_ ) {
            $('#uom_dropdown').addClass('inputerror');
            errorFlag = true;
        }
        if ( _desc === '' || _desc === null ) {
            $('#txtItemName').addClass('inputerror');
            errorFlag = true;
        }
        if ( !cat ) {
            $('#category_dropdown').addClass('inputerror');
            errorFlag = true;
        }
        if ( !subcat ) {
            $('#subcategory_dropdown').addClass('inputerror');
            errorFlag = true;
        }
        if ( !brand ) {
            $('#brand_dropdown').addClass('inputerror');
            errorFlag = true;
        }

        return errorFlag;
    }
    var populateDataGodowns = function(data) {
        $("#dept_dropdown").empty();
        $.each(data, function(index, elem){
            var opt1="<option value=" + elem.did + ">" +  elem.name + "</option>";
            $(opt1).appendTo('#dept_dropdown');
        });
    }
    var fetchGodowns = function() {
        $.ajax({
            url : base_url + 'index.php/department/fetchAllDepartments',
            type : 'POST',
            dataType : 'JSON',
            success : function(data) {
                if (data === 'false') {
                    alert('No data found');
                } else {
                    populateDataGodowns(data);
                }
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }
    var getSaveObjectGodown = function() {
        var obj = {};
        obj.did = 20000;
        obj.name = $.trim($('#txtNameGodownAdd').val());
        obj.description = $.trim($('.page_title').val());
        return obj;
    }
    var saveGodown = function( department ) {
        $.ajax({
            url : base_url + 'index.php/department/saveDepartment',
            type : 'POST',
            data : { 'department' : department },
            dataType : 'JSON',
            success : function(data) {

                if (data.error === 'false') {
                    alert('An internal error occured while saving department. Please try again.');
                } else {
                    alert('Department saved successfully.');
                    $('#GodownAddModel').modal('hide');
                    fetchGodowns();
                }
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }
    var validateSaveGodown = function() {
        var errorFlag = false;
        var _desc = $.trim($('#txtNameGodownAdd').val());
        $('.inputerror').removeClass('inputerror');
        if ( !_desc ) {
            $('#txtNameGodownAdd').addClass('inputerror');
            errorFlag = true;
        }
        return errorFlag;
    }

    var validateSaveAccount = function() {

        var errorFlag = false;
        var partyEl = $('#txtAccountName');
        var deptEl = $('#txtLevel3');

        // remove the error class first
        $('.inputerror').removeClass('inputerror');

        if ( !partyEl.val() ) {
            $('#txtAccountName').addClass('inputerror');
            errorFlag = true;
        }
        if ( !deptEl.val() ) {
            deptEl.addClass('inputerror');
            errorFlag = true;
        }

        return errorFlag;
    }
    // checks for the empty fields
    var validateSave = function() {

        var errorFlag = false;
        var partyEl = $('#party_dropdown');
        var deptEl = $('#dept_dropdown');
        var transEl = $('#transporter_dropdown');

        // remove the error class first
        $('.inputerror').removeClass('inputerror');

        if ( !partyEl.val() ) {
            $('#party_dropdown').addClass('inputerror');
            errorFlag = true;
        }
        if ( !deptEl.val() ) {
            // deptEl.addClass('inputerror');
            $('#dept_dropdown').addClass('inputerror');
            errorFlag = true;
        }
    /*    if ( !transEl.val() ) {
            // deptEl.addClass('inputerror');
            $('#transporter_dropdown').addClass('inputerror');
            errorFlag = true;
        }*/

        return errorFlag;
    }

    var deleteVoucher = function(vrnoa) {

        $.ajax({
            url : base_url + 'index.php/salereturn/delete',
            type : 'POST',
            data : { 'chk_date' : $('#current_date').val(), 'vrdate' : $('#vrdate').val(), 'vrnoa' : vrnoa, 'etype':'salereturn','company_id':$('#cid').val() },
            dataType : 'JSON',
            success : function(data) {

                if (data.error === 'date close') {
                    alert('Sorry! you can not delete in close date................');
                }else if (data === 'false') {
                    alert('No data found');
                } else {
                    alert('Voucher deleted successfully');
                    general.reloadWindow();
                }
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    ///////////////////////////////////////////////////////////////
    /// calculations related to the overall voucher
    ////////////////////////////////////////////////////////////////
    var calculateLowerTotal = function(qty, amount, weight) {

        var _qty = getNumVal($('#txtTotalQty'));
        var _weight = getNumVal($('#txtTotalWeight'));
        var _amnt = getNumVal($('#txtTotAmount'));
        var _totamnt = getNumVal($('#txtTotalAmount'));

        var _discp = getNumVal($('#txtDiscount'));
        var _disc = getNumVal($('#txtDiscAmount'));
        var _tax = getNumVal($('#txtTax'));
        var _taxamount = getNumVal($('#txtTaxAmount'));
        var _expense = getNumVal($('#txtExpAmount'));
        var _exppercent = getNumVal($('#txtExpense'));
        var _otherCharges = getNumVal($('#txtOtherCharges'));


        var tempQty = parseFloat(_qty) + parseFloat(qty);
        $('#txtTotalQty').val(tempQty);
        var tempAmnt = parseFloat(_amnt) + parseFloat(amount);
        $('#txtTotAmount').val(tempAmnt);
        if(_otherCharges != 0){
            tempAmnt = tempAmnt - _otherCharges;
            $('#txtTotalAmount').val(tempAmnt);
        }
        else{
            $('#txtTotalAmount').val(tempAmnt);
        }
        var totalWeight = parseFloat(parseFloat(_weight) + parseFloat(weight)).toFixed(2);
        $('#txtTotalWeight').val(totalWeight);

        var net = parseFloat(tempAmnt) - parseFloat(_disc) + parseFloat(_taxamount) - parseFloat(_expense) ;
        $('#txtNetAmount').val(net);
    }

    var getNumVal = function(el){
        return isNaN(parseFloat(el.val())) ? 0 : parseFloat(el.val());
    }

    ///////////////////////////////////////////////////////////////
    /// calculations related to the single product calculation
    ///////////////////////////////////////////////////////////////
    var calculateUpperSum = function() {

        var _qty = getNumVal($('#txtQty'));
        var _amnt = getNumVal($('#txtAmount'));
        var _net = getNumVal($('#txtNet'));
        var _prate = getNumVal($('#txtPRate'));
        var _gw = getNumVal($('#txtGWeight'));
        if ($('#txtGWeight').val()!=0)
        {
            $('#txtWeight').val(parseFloat(_gw) * parseFloat(_qty));
        }
        var _weight=getNumVal($('#txtWeight'));
        var _uom=$('#txtUom').val().toUpperCase();
        // alert('uom_item ' + _uom);
        kg=-1;
        gram=-1;

        var kg = _uom.search("KG");
        var gram = _uom.search("GRAM");
        if (kg ==-1 && gram ==-1 ){
            var _tempAmnt = parseFloat(_qty) * parseFloat(_prate);          
        }else{
            var _tempAmnt = parseFloat(_weight) * parseFloat(_prate);           
        }
        

        $('#txtAmount').val(_tempAmnt);
    }

    var fetchThroughPO = function(po) {

        $.ajax({

            url : base_url + 'index.php/salereturnorder/fetch',
            type : 'POST',
            data : { 'vrnoa' : po },
            dataType : 'JSON',
            success : function(data) {

                resetFields();
                if (data === 'false') {
                    alert('No data found.');
                } else {
                    populatePOData(data);
                }

            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    var populatePOData = function(data) {

        $('#party_dropdown').select2('val', data[0]['party_id']);
        $('#txtRemarks').val(data[0]['remarks']);
        $('#txtDiscount').val(data[0]['discp']);
        $('#txtExpense').val(data[0]['exppercent']);
        $('#txtExpAmount').val(data[0]['expense']);
        $('#txtTax').val(data[0]['taxpercent']);
        $('#txtTaxAmount').val(data[0]['tax']);
        $('#txtDiscAmount').val(data[0]['discount']);

        $('#dept_dropdown').select2('val', data[0]['godown_id']);
        $('#txtNetAmount').val(data[0]['namount']);
        $('#voucher_type_hidden').val('edit');

        $.each(data, function(index, elem) {
            appendToTable('', elem.item_name, elem.item_id, elem.item_qty, elem.item_rate, elem.item_amount, elem.weight);
            calculateLowerTotal(elem.item_qty, elem.item_amount, elem.weight);
        });
    }

    var resetFields = function() {

        $('#current_date').datepicker('update', new Date());
        $('#party_dropdown').select2('val', '');
        $('#txtInvNo').val('');
        $('#due_date').datepicker('update', new Date());
        $('#receivers_list').val('');
        $('#transporter_dropdown').select2('val', '');
        $('#txtRemarks').val('');
        $('#txtNetAmount').val('');     
        $('#txtDiscount').val('');
        $('#txtExpense').val('');
        $('#txtExpAmount').val('');
        $('#txtTax').val('');
        $('#txtTaxAmount').val('');
        $('#txtDiscAmount').val('');
        $('#txtTotalAmount').val('');
        $('#txtOtherCharges').val('');
        $('#txtPaid').val('');
        $('#txtTotalQty').val('');
        $('#txtTotalWeight').val('');
        $('#dept_dropdown').select2('val', '');
        $('#voucher_type_hidden').val('new');
        $('#txtTotAmount').val('');
        $('table tbody tr').remove();
    }

    var paging = function (total)
    {
        $("#paging").smartpaginator({
            totalrecords: parseInt(total),
            recordsperpage: 10,
            datacontainer: 'tbody',
            dataelement: 'tr',
            theme: 'custom',
            onchange: onChange
        });
    }

    var onChange = function (newPageValue)
    {
        $.ajax({
            type: "POST",
            url: base_url + 'index.php/item/paging',
            data: {
                page_number: newPageValue,
                search: $.trim($("#txtItemSearch").val())
            }
        }).done(function (result) {

            var total = "";
            var jsonR = JSON.stringify($.parseJSON(result)).replace(/\\n/g, "\\n")
                                        .replace(/\\'/g, "\\'")
                                        .replace(/\\"/g, '\\"')
                                        .replace(/\\&/g, "\\&")
                                        .replace(/\\r/g, "\\r")
                                        .replace(/\\t/g, "\\t")
                                        .replace(/\\b/g, "\\b")
                                        .replace(/\\f/g, "\\f");
            var json = $.parseJSON(jsonR);
            total = json.total;

            if (json.records != false) {
                var html = "";
                $.each(json.records, function (index, value) {
                    html += "<tr id='" + value.item_id + "'>";
                    html += "<td>"+value.item_id+"<input type='hidden' name='hfModalitemId' value='" + value.item_id + "'></td>";
                    html += "<td class='tdItemCode'><b>" + value.item_code + "<b></td>";
                    html += "<td class='tdDescription'><b>" + value.item_des + "</b></td>";
                    html += "<td class='tdItemUom'><b>" + value.uom + "</b></td>";
                    html += "<td class='tdPRate hide'>" + value.cost_price + "</td>";
                    html += "<td class='tdGrWeight hide'>" + value.grweight + "</td>";
                    html += "<td class='tdStQty hide'>" + value.stqty + "</td>";
                    html += "<td class='tdStWeight hide'>" + value.stweight + "</td>";
                    html += "<td class='tdSrate hide'>" + value.srate + "</td>";
                    html += "<td class='tdUItem hide'>" + value.uitem_des + "</td>";
                    html += "<td><a href='#' data-dismiss='modal' class='btn btn-primary populateItem'><i class='fa fa-search'></i></a></td>"
                    html += "</tr>";
                });

                $("#divNoRecord").removeClass('divNoRecord');
                $("#divNoRecord").html('');
                $("#tbItems > tbody").html('');
                $("#tbItems > tbody").append(html);

                $('.modal-lookup .populateItem').on('click', function()
                {
                    var itemId = $(this).closest('tr').find('input[name=hfModalitemId]').val();
                    var itemDescription = $(this).closest('tr').find('.tdDescription').text();
                    var uom = $(this).closest('tr').find('.tdItemUom').text();
                    var pRate = $(this).closest('tr').find('.tdPRate').text();
                    var grWeight = $(this).closest('tr').find('.tdGrWeight').text();
                    var stQty = $(this).closest('tr').find('.tdStQty').text();
                    var stWeight = $(this).closest('tr').find('.tdStWeight').text();
                    var srate = $(this).closest('tr').find('.tdSrate').text();
                    var uItemName = $(this).closest('tr').find('.tdUItem').text();
                    var itemCode = $(this).closest('tr').find('.tdItemCode').text();

                    var itemCodeOption = "<option value='" + itemId + "' data-uom_item='" + uom + "' data-prate='" + pRate + "' data-grweight='" + grWeight + "' data-stqty='" + stQty + "' data-stweight='" + stWeight + "' data-uitem_des='" + uItemName + "' data-srate='" + srate + "'>" + itemCode + "</option>";
                    var itemOption = "<option value='" + itemId + "'  data-uom_item='" + uom + "' data-prate='" + pRate + "' data-grweight='" + grWeight + "' data-stqty='" + stQty + "' data-stweight='" + stWeight + "'  data-uitem_des='" + uItemName + "' data-srate='" + srate + "'>" + itemDescription + "</option>";
                    $('#itemid_dropdown').append(itemCodeOption);
                    $('#item_dropdown').append(itemOption);

                    $('#itemid_dropdown').val(itemId).trigger('change');
                    $('#item_dropdown').val(itemId).trigger('change');

                    $('#txtPRate').val(parseFloat(srate).toFixed(2));

                    $('#txtQty').focus();
                });

                $("#paging").show();
                $("#tbItems").show();
                if (newPageValue == "1") {
                    paging(total);
                }
            }
            else {
                $("#tbItems > tbody").html('');
                $("#divNoRecord").show();
                $("#divNoRecord").addClass('divNoRecord');
                $("#paging").hide();
                $("#divNoRecord").html('No Record Found');
            }
        });
    }

    var clearItemData = function (){
        $("#hfItemId").val("");
        $("#hfItemSize").val("");
        $("#hfItemBid").val("");
        $("#hfItemUom").val("");
        $("#hfItemPrate").val("");
        $("#hfItemGrWeight").val("");
        $("#hfItemStQty").val("");
        $("#hfItemStWeight").val("");
        $("#hfItemLength").val("");
        $("#hfItemCatId").val("");
        $("#hfItemSubCatId").val("");
        $("#hfItemDesc").val("");
        $("#hfItemShortCode").val("");

         //$("#txtItemId").val("");
    }

    var getItemInfo = function(itemId)
    {
        $.ajax({
            type: "POST",
            url: base_url + 'index.php/item/getiteminfo',
            data: {
                item_id: itemId
            }
        }).done(function (result) {

            var jsonR = JSON.stringify($.parseJSON(result)).replace(/\\n/g, "\\n")
                                    .replace(/\\'/g, "\\'")
                                    .replace(/\\"/g, '\\"')
                                    .replace(/\\&/g, "\\&")
                                    .replace(/\\r/g, "\\r")
                                    .replace(/\\t/g, "\\t")
                                    .replace(/\\b/g, "\\b")
                                    .replace(/\\f/g, "\\f");
            var json = $.parseJSON(jsonR);

            if (json.records != false)
            {
                $.each(json.records, function (index, value) {
                    var itemCodeOption = "<option value='" + value.item_id + "' data-uom_item='" + value.uom + "' data-data-prate='" + value.cost_price + "' data-grweight='" + value.grweight + "' data-stqty='" + value.stqty + "' data-stweight='" + value.stweight + "' data-srate='" + value.srate + "'>" + value.item_code + "</option>";
                    var itemOption = "<option value='" + value.item_id + "'  data-uom_item='" + value.uom + "' data-data-prate='" + value.cost_price + "' data-grweight='" + value.grweight + "' data-stqty='" + value.stqty + "' data-stweight='" + value.stweight + "' data-srate='" + value.srate + "'>" + value.item_des + "</option>";
                    $('#itemid_dropdown').append(itemCodeOption);
                    $('#item_dropdown').append(itemOption);

                    $('#itemid_dropdown').select2('val', itemId);
                    $('#item_dropdown').select2('val', itemId);

                    var grweight = $('#item_dropdown').find('option:selected').data('grweight');
                    $('#txtGWeight').val(parseFloat(grweight).toFixed());
                });
            }
        });
    }


    var last_stockLocatons = function(item_id) {
        $.ajax({

            url : base_url + 'index.php/saleorder/last_stocklocatons',
            type : 'POST',
            data : { 'item_id' : item_id , 'company_id': $('#cid').val(),'etype':'sale'},
            dataType : 'JSON',
            
            success : function(data) {
                console.log(data);

                $('#laststockLocation_table tbody').html('');
                var total_qty = 0;
                $.each(data, function(index, elem) {
                total_qty += parseInt(elem.qty);
                appendTo_last_stockLocatons(elem.location, elem.qty);
            });
                
                last_stockLocatons_total(total_qty);

            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }    

    var appendTo_last_stockLocatons = function(location, qty) {

        var row =   "<tr>" +
                        "<td class='location numeric' data-title='location' > "+ location +"</td>" +
                        "<td class='qty numeric' data-title='qty'> "+ qty +"</td>" +
                    "</tr>";
        
            $(row).appendTo('#laststockLocation_table tbody');
        
    }

    var last_stockLocatons_total = function(total_qty){
        var row = "<tr>" +
                    "<td class='location numeric' data-title='location' >Total</td>" +
                    "<td class='qty numeric' data-title='qty'> "+ total_qty +"</td>" +
                "</tr>";
        $(row).appendTo('#laststockLocation_table tbody');
    }




    var fetchRunningTotal = function(_pid , _to) {
        $.ajax({
            url : base_url + 'index.php/account/fetchRunningTotal',
            type : 'POST',
            data : {'to': _to, 'pid' : _pid},
            dataType : 'JSON',
            success : function(data) {
                var opbal=data[0]['RTotal'];

                $('#balance_lbl').text('Party Name,     Balance: '+parseFloat(opbal).toFixed(2));
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }


    return {

        init : function() {
            this.bindUI();
            this.bindModalPartyGrid();
            //this.bindModalItemGrid();
        },

        bindUI : function() {
            var self = this;
            $(".btnPrint").attr("disabled","disabled").next().attr("disabled","disabled")

            
            $('#party_dropdown').focus().css('border', '1px solid #368EE0');

            $('.btn').focus(function(){
                $(this).css('border', '2px solid black');
            }
            );
            $('.btn').blur(function(){
                $(this).css('border', '');
            }
            );
            
            
            $('.btnprintHeader').on('click', function(e) {
                e.preventDefault();
                Print_Voucher(1);

            });
            $('.btnprintwithOutHeader').on('click', function(e) {
                e.preventDefault();
                Print_Voucher(0);
            });

            $('#txtLevel3').on('change', function() {
                
                var level3 = $('#txtLevel3').val();
                $('#txtselectedLevel1').text('');
                $('#txtselectedLevel2').text('');
                if (level3 !== "" && level3 !== null) {
                    // alert('enter' + $(this).find('option:selected').data('level2') );    
                    $('#txtselectedLevel2').text(' ' + $(this).find('option:selected').data('level2'));
                    $('#txtselectedLevel1').text(' ' + $(this).find('option:selected').data('level1'));
                }
            });
            // $('#txtLevel3').select2();
            $('.btnSaveM').on('click',function(e){
                if ( $('.btnSave').data('saveaccountbtn')==0 ){
                    alert('Sorry! you have not save accounts rights..........');
                }else{
                    e.preventDefault();
                    self.initSaveAccount();
                }
            });
            $('.btnResetM').on('click',function(){
                
                $('#txtAccountName').val('');
                $('#txtselectedLevel2').text('');
                $('#txtselectedLevel1').text('');
                $('#txtLevel3').select2('val','');
            });
            $('#AccountAddModel').on('shown.bs.modal',function(e){
                $('#txtAccountName').focus();
            });
            shortcut.add("F3", function() {
                $('#AccountAddModel').modal('show');
            });

            $('.btnSaveMItem').on('click',function(e){
                if ( $('.btnSave').data('saveitembtn')==0 ){
                    alert('Sorry! you have not save item rights..........');
                }else{
                    e.preventDefault();
                    self.initSaveItem();
                }
            });
            $('.btnResetMItem').on('click',function(){
                
                $('#txtItemName').val('');
                $('#category_dropdown').select2('val','');
                $('#subcategory_dropdown').select2('val','');
                $('#brand_dropdown').select2('val','');
                $('#txtBarcode').val('');
            });
            
            $('#GodownAddModel').on('shown.bs.modal',function(e){
                $('#txtNameGodownAdd').focus();
            });
            $('.btnSaveMGodown').on('click',function(e){
                if ( $('.btnSave').data('savegodownbtn')==0 ){
                    alert('Sorry! you have not save departments rights..........');
                }else{
                    e.preventDefault();
                    self.initSaveGodown();
                }
            });
            $('.btnResetMGodown').on('click',function(){
                
                $('#txtNameGodownAdd').val('');
                
            });

            $('#party_dropdown').on('change',function(){

                var partyId = $(this).val();
                var vrDate = $('#current_date').val();
                
                fetchRunningTotal(partyId, vrDate);
            });
            
            $('#ItemAddModel').on('shown.bs.modal',function(e){
                $('#txtItemName').focus();
            });

            shortcut.add("F7", function() {
                $('#ItemAddModel').modal('show');
            });
            
            $("#switchPreBal").bootstrapSwitch('offText', 'No');
            $("#switchPreBal").bootstrapSwitch('onText', 'Yes');

            $('#voucher_type_hidden').val('new');
            $('.modal-lookup .populateAccount').on('click', function(){
                // alert('dfsfsdf');
                var party_id = $(this).closest('tr').find('input[name=hfModalPartyId]').val();
                $("#party_dropdown").select2("val", party_id);              
            });
            $('.modal-lookup .populateItem').on('click', function(){
                // alert('dfsfsdf');
                var item_id = $(this).closest('tr').find('input[name=hfModalitemId]').val();
                $("#item_dropdown").select2("val", item_id); //set the value
                $("#itemid_dropdown").select2("val", item_id);
                $('#txtQty').focus();               
            });

            $('#voucher_type_hidden').val('new');
            $('#txtVrnoa').on('change', function() {
                fetch($(this).val());
            });

            $('.btnSave').on('click',  function(e) {
               if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('updatebtn')==0 ){
                    alert('Sorry! you have not update rights..........');
                }else if($('#voucher_type_hidden').val()=='new' && $('.btnSave').data('insertbtn')==0){
                    alert('Sorry! you have not insert rights..........');
                }else{
                    e.preventDefault();
                    self.initSave();
                }
            });

            $('.btnPrint').on('click',  function(e) {
                e.preventDefault();

                Print_Voucher(1);
            });
            $('.btnprint_sm').on('click', function(e){
                e.preventDefault();
                Print_Voucher(1,'sm');
            });
            $('.btnprint_sm_withOutHeader').on('click', function(e) {
                e.preventDefault();
                Print_Voucher(0,'sm');
            });
            $('.btnprint_sm_rate').on('click', function(e){
                e.preventDefault();
                Print_Voucher(1,'sm','wrate');
            });
            $('.btnprint_sm_withOutHeader_rate').on('click', function(e) {
                e.preventDefault();
                Print_Voucher(0,'sm','wrate');
            });
            $('.btnPrints').on('click', function(ev) {
                 window.open(base_url + 'application/views/reportprints/salereturn.php', "Sale Return", 'width=1210, height=842');
            });
            $('.btnPrints_urdu').on('click', function(ev) {
                 window.open(base_url + 'application/views/reportprints/inwardwapsi.php', "Inward Return", 'width=1210, height=842');
            });

            $('.btnReset').on('click', function(e) {
                e.preventDefault();
                self.resetVoucher();
            });

            $('.btnDelete').on('click', function(e){
                if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('deletebtn')==0 ){
                    alert('Sorry! you have not delete rights..........');
                }else{
                    e.preventDefault();
                    var vrnoa = $('#txtVrnoaHidden').val();
                    if (vrnoa !== '') {
                        if (confirm('Are you sure to delete this voucher?'))
                            deleteVoucher(vrnoa);
                    }
                }
            });

            $('#txtOrderNo').on('keypress', function(e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    if ($(this).val() != '') {
                        fetchThroughPO($(this).val());
                    }
                }
            });

            /////////////////////////////////////////////////////////////////
            /// setting calculations for the single product
            /////////////////////////////////////////////////////////////////

            $('#txtWeight').on('input', function() {
                // var _gw = getNumVal($('#txtGWeight'));
                // if (_gw!=0) {
                // var w = parseInt(parseFloat($(this).val())/parseFloat(_gw));
                // $('#txtQty').val(w); 
                // }
                //calculateUpperSum();
                var _uom=$('#txtUom').val().toUpperCase();
                var kg = _uom.search("KG");
                var gram = _uom.search("GRAM");
                var _tempAmnt=0;
                if (kg ==-1 && gram ==-1 ){
                _tempAmnt = parseFloat(_qty) * parseFloat(_prate);
                }else{

                _tempAmnt = parseFloat($('#txtWeight').val()) * parseFloat($('#txtPRate').val());
                }
                if (_tempAmnt!=0)
                {
                    $('#txtAmount').val(_tempAmnt);
                }
            });

            $('#itemid_dropdown').on('change', function() {
                var item_id = $(this).val();
                var prate = $(this).find('option:selected').data('srate');
                var grweight = $(this).find('option:selected').data('grweight');
                var uom_item = $(this).find('option:selected').data('uom_item');
                var stqty = $(this).find('option:selected').data('stqty');
                var stweight = $(this).find('option:selected').data('stweight');
                $('#stqty_lbl').text('Item,     Qty:' + stqty + ', Weight ' + stweight);

                $('#txtPRate').val(parseFloat(prate).toFixed(2));
                $('#item_dropdown').select2('val', item_id);
                $('#txtGWeight').val(parseFloat(grweight).toFixed(2));
                $('#txtUom').val(uom_item);

                // calculateUpperSum();
                // $('#txtQty').focus();
            });
            $('#item_dropdown').on('change', function() {
                var item_id = $(this).val();
                var prate = $(this).find('option:selected').data('srate');
                var grweight = $(this).find('option:selected').data('grweight');
                var uom_item = $(this).find('option:selected').data('uom_item');
                var stqty = $(this).find('option:selected').data('stqty');
                var stweight = $(this).find('option:selected').data('stweight');
                $('#stqty_lbl').text('Item,     Qty:' + stqty + ', Weight ' + stweight);
                
                $('#txtPRate').val(parseFloat(prate).toFixed(2));
                $('#itemid_dropdown').select2('val', item_id);
                $('#txtGWeight').val(parseFloat(grweight).toFixed());
                $('#txtUom').val(uom_item);
                // calculateUpperSum();
                // $('#txtQty').focus();
            });

            var countItem = 0;
            $('input[id="txtItemId"]').autoComplete({

                minChars: 1,
                cache: false,
                menuClass: '',
                source: function(search, response)
                {
                    try { xhr.abort(); } catch(e){}
                    $('#txtItemId').removeClass('inputerror');
                    $("#imgItemLoader").hide();
                    if(search != "")
                    {
                        xhr = $.ajax({
                            url: base_url + 'index.php/item/searchitem',
                            type: 'POST',
                            data: {

                                search: search
                            },
                            dataType: 'JSON',
                            beforeSend: function (data) {

                                $(".loader").hide();
                                $("#imgItemLoader").show();
                                countItem = 0;
                            },
                            success: function (data) {

                                if(data == '')
                                {
                                    $('#txtItemId').addClass('inputerror');
                                    clearItemData();
                                    $('#itemDesc').val('');
                                    $('#txtPRate').val('');
                                    $('#txtGWeight').val('');
                                    $('#txtUom').val('');
                                }
                                else
                                {
                                    $('#txtItemId').removeClass('inputerror');
                                    response(data);
                                    $("#imgItemLoader").hide();
                                }
                            }
                        });
                    }
                    else
                    {
                        clearItemData();
                    }
                },
                renderItem: function (item, search)
                {
                    var sea = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                    var re = new RegExp("(" + sea.split(' ').join('|') + ")", "gi");

                    var selected = "";
                    if((search.toLowerCase() == (item.item_des).toLowerCase() && countItem == 0) || (search.toLowerCase() != (item.item_des).toLowerCase() && countItem == 0))
                    {
                        selected = "selected";
                    }
                    countItem++;
                    clearItemData();

                    return '<div class="autocomplete-suggestion ' + selected + '" data-val="' + search + '" data-item_id="' + item.item_id + '" data-size="' + item.pack + '" data-bid="' + item.bid +
                    '" data-uom_item="'+ item.uom + '" data-srate="' + item.srate + '" data-grweight="' + item.grweight + '" data-stqty="' + item.stqty +
                    '" data-stweight="' + item.stweight + '" data-length="' + item.length  + '" data-catid="' + item.catid +
                    '" data-subcatid="' + item.subcatid + '" data-desc="' + item.item_des + '" data-code="' + item.item_code + '" data-self="' + item.self_freight_rate + '" data-bilty="' + item.bilty_freight_rate + 
                    '" data-gari="' + item.gari_freight_rate + '" data-uitem_des="' + item.uitem_des + '"  data-srate1="' + item.srate1 + '">' + item.item_des.replace(re, "<b>$1</b>") + '</div>';
                },
                onSelect: function(e, term, item)
                {

                    $("#imgItemLoader").hide();
                    $("#hfItemId").val(item.data('item_id'));
                    $("#hfItemSize").val(item.data('size'));
                    $("#hfItemBid").val(item.data('bid'));
                    $("#hfItemUom").val(item.data('uom_item'));
                    $("#hfItemPrate").val(item.data('srate'));
                    $("#hfItemGrWeight").val(item.data('grweight'));
                    $("#hfItemStQty").val(item.data('stqty'));
                    $("#hfItemStWeight").val(item.data('stweight'));
                    $("#hfItemLength").val(item.data('length'));
                    $("#hfItemCatId").val(item.data('catid'));
                    $("#hfItemSubCatId").val(item.data('subcatid'));
                    $("#hfItemDesc").val(item.data('desc'));
                    $("#hfItemShortCode").val(item.data('code'));
                    $("#hfItemUname").val(item.data('uitem_des'));


                    $("#txtItemId").val(item.data('code'));

                    var itemId = item.data('item_id');
                    var itemDesc = item.data('desc');
                    var srate = item.data('srate');
                    var grWeight = item.data('grweight');
                    var uomItem = item.data('uom_item');
                    var stQty = item.data('stqty');
                    var stWeight = item.data('stweight');
                    var size = item.data('size');
                    var brandId = item.data('bid');
                    var prate = item.data('srate1');

            
                    $('#itemDesc').val(itemDesc);
                    $('#stqty_lbl').text('Item,     Qty:' + stQty + ', Weight ' + stWeight);
                    $('#txtPRate').val(parseFloat(srate).toFixed(2));
                    $('#txtGWeight').val(parseFloat(grWeight).toFixed());
                    $('#txtUom').val(uomItem);
                }
            });
            $('#txtQty').on('input', function() {
                calculateUpperSum();

            });
            $('#txtPRate').on('input', function() {
                calculateUpperSum();
            });


            $('#btnAdd').on('click', function(e) {
                e.preventDefault();

                var error = validateSingleProductAdd();
                if (!error) {

                    var item_desc = $('#itemDesc').val();
                    var item_id = $('#hfItemId').val();
                    var qty = $('#txtQty').val();
                    var rate = $('#txtPRate').val();
                    var weight = getNumVal($('#txtWeight'));
                    var amount = $('#txtAmount').val();
                    var itemUname = $('#hfItemUname').val();
                    var itemCode = $('#txtItemId').val();
                    // reset the values of the annoying fields
                    $('#itemDesc').val('');
                    $('#txtItemId').val('');
                    $('#txtQty').val('');
                    $('#txtPRate').val('');
                    $('#txtWeight').val('');
                    $('#txtAmount').val('');
                    $('#txtGWeight').val('');

                    appendToTable('', item_desc, item_id, qty, rate, amount, weight, itemUname, itemCode);
                    calculateLowerTotal(qty, amount, weight);
                    $('#stqty_lbl').text('Item');
                    $('#txtItemId').focus();
                } else {
                    alert('Correct the errors!');
                }

            });

            // when btnRowRemove is clicked
            $('#salereturn_table').on('click', '.btnRowRemove', function(e) {
                e.preventDefault();
                var qty = $.trim($(this).closest('tr').find('td.qty').text());
                var amount = $.trim($(this).closest('tr').find('td.amount').text());
                var weight = $.trim($(this).closest('tr').find('td.weight').text());
                calculateLowerTotal("-"+qty, "-"+amount, '-'+weight);
                $(this).closest('tr').remove();
            });
            $('#salereturn_table').on('click', '.btnRowEdit', function(e) {
                e.preventDefault();

                // getting values of the cruel row
                var item_id = $.trim($(this).closest('tr').find('td.item_desc').data('item_id'));
                var qty = $.trim($(this).closest('tr').find('td.qty').text());
                var weight = $.trim($(this).closest('tr').find('td.weight').text());
                var rate = $.trim($(this).closest('tr').find('td.rate').text());
                var amount = $.trim($(this).closest('tr').find('td.amount').text());
                //getItemInfo(item_id);
                $.ajax({
                    type: "POST",
                    url: base_url + 'index.php/item/getiteminfobyid',
                    data: {
                        item_id: item_id
                    }
                }).done(function (result) {

                    $("#imgItemLoader").hide();
                    var item = $.parseJSON(result);

                    if (item != false)
                    {
                        $("#hfItemId").val(item[0].item_id);
                        $("#hfItemSize").val(item[0].size);
                        $("#hfItemBid").val(item[0].bid);
                        $("#hfItemUom").val(item[0].uom);
                        $("#hfItemPrate").val(item[0].cost_price);
                        $("#hfItemGrWeight").val(item[0].grweight);
                        $("#hfItemStQty").val(item[0].stqty);
                        $("#hfItemStWeight").val(item[0].stweight);
                        $("#hfItemLength").val(item[0].length);
                        $("#hfItemCatId").val(item[0].catid);
                        $("#hfItemSubCatId").val(item[0].subcatid);
                        $("#hfItemDesc").val(item[0].item_des);
                        $("#hfItemShortCode").val(item[0].short_code);
                        $("#hfItemUname").val(item[0].uitem_des);


                        $("#txtItemId").val(item[0].item_code);
                        $('#itemDesc').val(item[0].item_des);
                        $('#txtUom').val(item[0].uom);
                        $('#txtGWeight').val(item[0].grweight);

                        $('#stqty_lbl').text('Item,     Uom:' + item[0].uom);


                        $('#txtQty').val(qty);
                        $('#txtPRate').val(rate);
                        $('#txtWeight').val(weight);
                        $('#txtAmount').val(amount);
                        calculateLowerTotal("-"+qty, "-"+amount, '-'+weight);

                    }
                });


                // $('#itemid_dropdown').select2('val', item_id);
                // $('#item_dropdown').select2('val', item_id);

                // var grweight = $('#item_dropdown').find('option:selected').data('grweight');

                // $('#txtGWeight').val(parseFloat(grweight).toFixed());
               
                // now we have get all the value of the row that is being deleted. so remove that cruel row
                $(this).closest('tr').remove(); // yahoo removed
            });

            $('#txtDiscount').on('input', function() {
                var _disc= $('#txtDiscount').val();
                var _totalAmount= $('#txtTotalAmount').val();
                var _discamount=0;
                if (_disc!=0 && _totalAmount!=0){
                    _discamount=_totalAmount*_disc/100;
                }
                $('#txtDiscAmount').val(_discamount);
                calculateLowerTotal(0, 0, 0);
            });

            $('#txtDiscAmount').on('input', function() {
                var _discamount= $('#txtDiscAmount').val();
                var _totalAmount= $('#txtTotalAmount').val();
                var _discp=0;
                if (_discamount!=0 && _totalAmount!=0){
                    _discp=_discamount*100/_totalAmount;
                }
                $('#txtDiscount').val(parseFloat(_discp).toFixed(2));
                calculateLowerTotal(0, 0, 0);
            });

            $('#txtExpense').on('input', function() {
                var _exppercent= $('#txtExpense').val();
                var _totalAmount= $('#txtTotalAmount').val();
                var _expamount=0;
                if (_exppercent!=0 && _totalAmount!=0){
                    _expamount=_totalAmount*_exppercent/100;
                }
                $('#txtExpAmount').val(_expamount);
                calculateLowerTotal(0, 0, 0);
            });

            $('#txtExpAmount').on('input', function() {
                var _expamount= $('#txtExpAmount').val();
                var _totalAmount= $('#txtTotalAmount').val();
                var _exppercent=0;
                if (_expamount!=0 && _totalAmount!=0){
                    _exppercent=_expamount*100/_totalAmount;
                }
                $('#txtExpense').val(parseFloat(_exppercent).toFixed(2));
                calculateLowerTotal(0, 0, 0);
            });
            $('#txtOtherCharges').on('input', function() {
                
                var _otherCharges= getNumVal($('#txtOtherCharges'));
                var _totalAmount= getNumVal($('#txtTotAmount'));
                
                var tempTotal = parseFloat(_totalAmount) - parseFloat(_otherCharges);
                //alert(tempTotal);
                $('#txtTotalAmount').val(parseFloat(tempTotal));
                calculateLowerTotal(0, 0, 0);
            });

            $('#txtTax').on('input', function() {
                var _taxpercent= $('#txtTax').val();
                var _totalAmount= $('#txtTotalAmount').val();
                var _taxamount=0;
                if (_taxpercent!=0 && _totalAmount!=0){
                    _taxamount=_totalAmount*_taxpercent/100;
                }
                $('#txtTaxAmount').val(_taxamount);
                calculateLowerTotal(0, 0, 0);
            });

            $('#txtTaxAmount').on('input', function() {
                var _taxamount= $('#txtTaxAmount').val();
                var _totalAmount= $('#txtTotalAmount').val();
                var _taxpercent=0;
                if (_taxamount!=0 && _totalAmount!=0){
                    _taxpercent=_taxamount*100/_totalAmount;
                }
                $('#txtTax').val(parseFloat(_taxpercent).toFixed(2));
                calculateLowerTotal(0, 0, 0);
            });

            $('#item_dropdown').on('change', function(){
                var item_id = $(this).find('option:selected').val();
                last_stockLocatons(item_id);
            });

            $("form").submit(function() { return false; });
            
            shortcut.add("F10", function() {
                $('.btnSave1').trigger('click');
            });
            shortcut.add("F1", function() {
                $('a[href="#party-lookup"]').trigger('click');
            });
            shortcut.add("F2", function() {
                $('#item_dropdown').select2('close');
                $('a[href="#item-lookup"]').trigger('click');
            });
            shortcut.add("F9", function() {
                Print_Voucher(1);
            });
            shortcut.add("F6", function() {
                $('#txtVrnoa').focus();
                // alert('focus');
            });
            shortcut.add("F5", function() {
                self.resetVoucher();
            });

            shortcut.add("F12", function() {
                $('.btnDelete').trigger('click');
            });


            $('#txtVrnoa').on('keypress', function(e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    var vrnoa = $('#txtVrnoa').val();
                    if (vrnoa !== '') {
                        fetch(vrnoa);
                    }
                }
            });

            onChange(1);
            $("input[id$='txtItemSearch']").keydown(function(event){
                var keyCode = event.keyCode;
                if(keyCode == 13)
                {
                    onChange(1);
                    event.preventDefault();
                    return false;
                }
                else if(keyCode == 8 || keyCode == 46)
                {
                    var txt = $("input[id$='txtItemSearch']").val();
                    if(txt.length <= 1)
                    {
                        $("input[id$='txtItemSearch']").val('');
                        onChange(1);
                    }
                }
            });
            salereturn.fetchRequestedVr();
        },

        // prepares the data to save it into the database
        initSave : function() {

            var saveObj = getSaveObject();
            var error = validateSave();

            if (!error) {
                var rowsCount = $('#salereturn_table').find('tbody tr').length;
                if (rowsCount > 0 ) {
                    save(saveObj);
                } else {
                    alert('No date found to save!');
                }
            } else {
                alert('Correct the errors...');
            }
        },
        fetchRequestedVr : function () {

        var vrnoa = general.getQueryStringVal('vrnoa');
        vrnoa = parseInt( vrnoa );
        $('#txtVrnoa').val(vrnoa);
        $('#txtVrnoaHidden').val(vrnoa);
        if ( !isNaN(vrnoa) ) {
            fetch(vrnoa);
        }else{
            getMaxVrno();
            getMaxVrnoa();
        }
    },
        initSaveAccount : function() {

            var saveObjAccount = getSaveObjectAccount();
            var error = validateSaveAccount();

            if (!error) {
                    saveAccount(saveObjAccount);
            } else {
                alert('Correct the errors...');
            }
        },
        initSaveItem : function() {

            var saveObjItem = getSaveObjectItem();
            var error = validateSaveItem();

            if (!error) {
                    saveItem(saveObjItem);
            } else {
                alert('Correct the errors...');
            }
        },
        initSaveGodown : function() {

            var saveObjGodown = getSaveObjectGodown();
            var error = validateSaveGodown();

            if (!error) {
                    saveGodown(saveObjGodown);
            } else {
                alert('Correct the errors...');
            }
        },
                bindModalPartyGrid : function() {

            
                            var dontSort = [];
                            $('#party-lookup table thead th').each(function () {
                                if ($(this).hasClass('no_sort')) {
                                    dontSort.push({ "bSortable": false });
                                } else {
                                    dontSort.push(null);
                                }
                            });
                            salereturn.pdTable = $('#party-lookup table').dataTable({
                                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
                                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
                                "aaSorting": [[0, "asc"]],
                                "bPaginate": true,
                                "sPaginationType": "full_numbers",
                                "bJQueryUI": false,
                                "aoColumns": dontSort

                            });
                            $.extend($.fn.dataTableExt.oStdClasses, {
                                "s`": "dataTables_wrapper form-inline"
                            });
},

bindModalItemGrid : function() {

            
                            var dontSort = [];
                            $('#item-lookup table thead th').each(function () {
                                if ($(this).hasClass('no_sort')) {
                                    dontSort.push({ "bSortable": false });
                                } else {
                                    dontSort.push(null);
                                }
                            });
                            salereturn.pdTable = $('#item-lookup table').dataTable({
                                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
                                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
                                "aaSorting": [[0, "asc"]],
                                "bPaginate": true,
                                "sPaginationType": "full_numbers",
                                "bJQueryUI": false,
                                "aoColumns": dontSort

                            });
                            $.extend($.fn.dataTableExt.oStdClasses, {
                                "s`": "dataTables_wrapper form-inline"
                            });
},

        // instead of reseting values reload the page because its cruel to write to much code to simply do that
        resetVoucher : function() {
            general.reloadWindow();
        }
    }

};

var salereturn = new salereturn();
salereturn.init();