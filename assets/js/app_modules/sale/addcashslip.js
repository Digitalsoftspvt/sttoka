var Transporter = function() {

	// saves the data into the database
	var save = function( obj , etype ) {
		general.disableSave();
		$.ajax({
			url : base_url + 'index.php/cashslip/save',
			type : 'POST',
			data : { 'cashmain' : obj.cashmain, 'vrnoa':obj.vrnoa,'etype': etype, 'ledger' : obj.ledger ,'voucher_type_hidden':$('#voucher_type_hidden').val() , 'vrdate' : $('#vrdate').val()},
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'date close') {
					alert('Sorry! you can not insert update in close date................');
				}else if (data.error === 'false') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					var printConfirmation = confirm('Cash slip saved successfully!\nWould you like to print the slip as well?');
					if (printConfirmation == true) {
						
						window.open(base_url + 'application/views/reportprints/cash_slip.php', "Cash Slip", 'width=1000, height=842');
						setTimeout(function(){ general.reloadWindow(); }, 3000);
					} else {
						
						general.reloadWindow();
					}
				}
				general.enableSave();
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var fetch = function(vrnoa , etype) {

		$.ajax({

			url : base_url + 'index.php/cashslip/fetch',
			type : 'POST',
			data : { 'vrnoa' : vrnoa , 'company_id': $('#cid').val(),'etype':etype},
			dataType : 'JSON',
			success : function(data) {

				resetFields();
				//$('#txtOrderNo').val('');
				if (data === 'false') {
					alert('No data found.');

                    $(".btnPrint").attr("disabled","disabled").next().attr("disabled","disabled")
				} else {

                    $(".btnPrint").removeAttr("disabled").next().removeAttr("disabled");

                    populateData(data);
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var deleteVoucher = function(vrnoa , etype) {

		$.ajax({
			url : base_url + 'index.php/cashslip/delete',
			type : 'POST',
			data : { 'chk_date' : $('#current_date').val(), 'vrdate' : $('#vrdate').val(), 'vrnoa' : vrnoa , 'etype':etype ,'company_id':$('#cid').val() },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'date close') {
					alert('Sorry! you can not delete in close date................');
				}else if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// generates the view
	var populateData = function(data) {

		
		$('#txtVrnoaHidden').val(data[0]['vrnoa']);
		$('#txtIdImg').val(data[0]['vrnoa']);
		$('#current_date').val(data[0]['vrdate'].substr(0,10));
		$('#vrdate').val(data[0]['vrdate'].substr(0,10));
		$('#cus_dropdown').select2('val',data[0]['cus_id']);
		$('#field_dropdown').select2('val',data[0]['field_id']);
		$('#bank_dropdown').val(data[0]['bank_id']);
		$('#cash_dropdown').select2('val',data[0]['cash_id']);
		$('#txtRemarks').val(data[0]['remarks']);
		$('#txtRemarks2').val(data[0]['ttremarks']);
		$('#txtTotal').val(data[0]['totalamount']);
		$('#msgStatus').val(data[0]['msg_status']);
		$('#voucher_type_hidden').val('edit');		

		$('#cid').val(data[0]['company_id']);
		$('#uid').val(data[0]['uid']);

		var i = 0;
		$('#conversionTable').find('tbody tr').each(function( index, elem ) {
			
			$(elem).find('td.firsrtTd').find('input').val(data[i]['enter_amount']);
			$(elem).find('td.secondTd').find('input').val(data[i]['fix_amount']);
			$(elem).find('td.thirdTd').find('input').val(data[i]['conversion_amount']);
			$(elem).find('td.exchangeRate').find('input').val(data[i]['extra_rate']);
			i++;
		});
	}

	var resetFields = function(){
		
		$('#current_date').datepicker('update', new Date());
		$('#cus_dropdown').select2('val','');
		$('#field_dropdown').val('');
		$('#bank_dropdown').select2('val','');
		$('#cash_dropdown').select2('val','');
		$('#txtRemarks').val('');
		$('#txtRemarks2').val('');
		$('#txtTotal').val('');
		$('#msgStatus').val(1);
		$('#voucher_type_hidden').val('new');


		$('#conversionTable').find('tbody tr').each(function( index, elem ) {
			
			$(elem).find('td.firsrtTd').find('input').val('');
			//$(elem).find('td.secondTd').find('input').val('');
			$(elem).find('td.thirdTd').find('input').val('');
			$(elem).find('td.exchangeRate').find('input').val('');
			
		});
	}
	var getMaxVrnoa = function( etype ) {

		$.ajax({

			url : base_url + 'index.php/cashslip/getMaxVrnoa',
			type : 'POST',
			data : {'company_id': $('#cid').val() , 'etype' : etype},
			dataType : 'JSON',
			success : function(data) {

				$('#txtVrnoa').val(data);
				$('#txtMaxVrnoaHidden').val(data);
				$('#txtVrnoaHidden').val(data);
				$('#txtIdImg').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;

		var name = $.trim($('#cus_dropdown').val());
		var bankName = $('#bank_dropdown').val();
		var cash = $('#cash_dropdown').val();
		var amount = $('#txtTotal').val();
		

		// remove the error class first
		$('#cus_dropdown').removeClass('inputerror');
		$('#bank_dropdown').removeClass('inputerror');
		$('#cash_dropdown').removeClass('inputerror');
		$('#txtTotal').removeClass('inputerror');

		if ( name === '' ) {
			$('#cus_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		/*if ( bankName === '' || bankName === null ) {
			$('#bank_dropdown').addClass('inputerror');
			errorFlag = true;
		}*/
		if ( cash === '' || cash === null ) {
			$('#cash_dropdown').addClass('inputerror');
			errorFlag = true;
		}

		if ( amount === '' || amount === null || amount <= "0") {
			$('#txtTotal').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	// returns the fee category object to save into database
	var getSaveObject = function() {

		var ledgers = [];
		var cashmain = [];

		var _etype = ($('#cpv').is(':checked') === true) ? 'cspv' : 'csrv';
		
		$('#conversionTable').find('tbody tr').each(function( index, elem ) {
			var sd = {};

			sd.vrnoa 			 = $('#txtVrnoaHidden').val();
			sd.vrdate 			 = $('#current_date').val();
			sd.etype 			 = _etype;
			sd.cus_id 			 = $('#cus_dropdown').val();
			sd.enter_amount 	 = $.trim($(elem).find('td.firsrtTd').find('input').val());
			sd.fix_amount 		 = $.trim($(elem).find('td.secondTd').find('input').val());
			sd.conversion_amount = $.trim($(elem).find('td.thirdTd').find('input').val());
			sd.extra_rate 		 = $.trim($(elem).find('td.exchangeRate').find('input').val());
			sd.field_id 		 = $('#field_dropdown').val();
			sd.bank_id			 = $('#bank_dropdown').val();
			sd.cash_id 			 = $('#cash_dropdown').val();
			sd.remarks 			 = $('#txtRemarks').val();
			sd.ttremarks 			 = $('#txtRemarks2').val();
			sd.msg_status 		 = $('#msgStatus').val();
			sd.totalamount 		 = $('#txtTotal').val();
			sd.company_id		 = $('#cid').val();
			sd.uid				 = $('#uid').val();
			
			cashmain.push(sd);
		});

		  /////////////////////////////////////
		 ////	for over all voucher      ////
		/////////////////////////////////////

		if($('#txtTotal').val() && $('#txtTotal').val() > 0){
			
			var pledger = {};
			pledger.pledid = '';
			pledger.pid = $('#cus_dropdown').val();
			pledger.description =   $('#cus_dropdown').find('option:selected').text()+' '+$('#bank_dropdown').val();
			pledger.remarks =  $('#txtRemarks').val();
			pledger.date = $('#current_date').val();
			pledger.debit = (_etype === 'cspv') ? $('#txtTotal').val() : 0 ; //0;
			pledger.credit = (_etype === 'csrv') ? $('#txtTotal').val() : 0 ; //$('#txtTotal').val();
			pledger.dcno = $('#txtVrnoaHidden').val();
			pledger.invoice = $('#txtVrnoaHidden').val();
			pledger.etype = _etype;
			pledger.pid_key = $('#cash_dropdown').val();
			pledger.uid = $('#uid').val();
			pledger.company_id = $('#cid').val();
			pledger.isFinal = 0;	
			ledgers.push(pledger);

			var pledger = {};
			pledger.pledid = '';
			pledger.pid = $('#cash_dropdown').val();
			pledger.description = $('#cus_dropdown').find('option:selected').text()+' '+$('#bank_dropdown').val();
			pledger.remarks =  $('#txtRemarks').val();
			pledger.date = $('#current_date').val();
			pledger.debit = (_etype === 'csrv') ? $('#txtTotal').val() : 0 ; //$('#txtTotal').val();
			pledger.credit = (_etype === 'cspv') ? $('#txtTotal').val() : 0 ; //0;
			pledger.dcno = $('#txtVrnoaHidden').val();
			pledger.invoice = $('#txtVrnoaHidden').val();
			pledger.etype = _etype;
			pledger.pid_key = $('#cus_dropdown').val();
			pledger.uid = $('#uid').val();
			pledger.company_id = $('#cid').val();	
			pledger.isFinal = 0;
			ledgers.push(pledger);
		}

		/*if($('#field_dropdown').val() && $('#field_dropdown').val() != ''){

			var pledger = {};
			pledger.pledid = '';
			pledger.pid = $('#cash_dropdown').val();
			pledger.description =   $('#bank_dropdown').val() + ' ' + $('#txtRemarks').val();
			pledger.date = $('#current_date').val();
			pledger.debit = (_etype === 'cspv') ? $('#txtTotal').val() : 0 ; //0;
			pledger.credit = (_etype === 'csrv') ? $('#txtTotal').val() : 0 ; //$('#txtTotal').val();
			pledger.dcno = $('#txtVrnoaHidden').val();
			pledger.invoice = $('#txtVrnoaHidden').val();
			pledger.etype = _etype;
			pledger.pid_key = $('#field_dropdown').val();
			pledger.uid = $('#uid').val();
			pledger.company_id = $('#cid').val();
			pledger.isFinal = 0;	
			ledgers.push(pledger);

			var pledger = {};
			pledger.pledid = '';
			pledger.pid = $('#field_dropdown').val();
			pledger.description = $('#bank_dropdown').val() + ' ' + $('#txtRemarks').val();
			pledger.date = $('#current_date').val();
			pledger.debit = (_etype === 'csrv') ? $('#txtTotal').val() : 0 ; //$('#txtTotal').val();
			pledger.credit = (_etype === 'cspv') ? $('#txtTotal').val() : 0 ; //0;
			pledger.dcno = $('#txtVrnoaHidden').val();
			pledger.invoice = $('#txtVrnoaHidden').val();
			pledger.etype = _etype;
			pledger.pid_key = $('#cash_dropdown').val();
			pledger.uid = $('#uid').val();
			pledger.company_id = $('#cid').val();	
			pledger.isFinal = 0;
			ledgers.push(pledger);	
		}*/
		

		// var pledger = {};
		// pledger.pledid = '';
		// pledger.pid = $('#bank_dropdown').val();
		// pledger.description = $('#cus_dropdown').find('option:selected').text() + ' ' + $('#txtRemarks').val();
		// pledger.date = $('#current_date').val();
		// pledger.debit = (_etype === 'csrv') ? $('#txtTotal').val() : 0 ; //$('#txtTotal').val();
		// pledger.credit = (_etype === 'cspv') ? $('#txtTotal').val() : 0 ; //0;
		// pledger.dcno = $('#txtVrnoaHidden').val();
		// pledger.invoice = $('#txtVrnoaHidden').val();
		// pledger.etype = _etype;
		// pledger.pid_key = $('#cus_dropdown').val();
		// pledger.uid = $('#uid').val();
		// pledger.company_id = $('#cid').val();	
		// pledger.isFinal = 0;
		// ledgers.push(pledger);

		if($('#field_dropdown').val() && $('#txtTTAmountVal').val() && $('#txtTTAmountVal').val() != '' && $('#txtTTAmountVal').val() != 0 && $('#field_dropdown').val()){

			var pledger = {};
			pledger.pledid = '';
			pledger.pid = $('#field_dropdown').val();
			pledger.description =  $('#cus_dropdown').find('option:selected').text()+' '+$('#bank_dropdown').val();
			pledger.remarks =  $('#txtRemarks2').val();
			pledger.date = $('#current_date').val();
			pledger.debit = (_etype === 'csrv') ? $('#txtTTAmountVal').val() : 0 ; //0;
			pledger.credit = (_etype === 'cspv') ? $('#txtTTAmountVal').val() : 0 ; //$('#txtTotal').val();
			pledger.dcno = $('#txtVrnoaHidden').val();
			pledger.invoice = $('#txtVrnoaHidden').val();
			pledger.etype = _etype;
			pledger.pid_key = $('#cash_dropdown').val();
			pledger.uid = $('#uid').val();
			pledger.company_id = $('#cid').val();
			pledger.isFinal = 0;	
			ledgers.push(pledger);

			var pledger = {};
			pledger.pledid = '';
			pledger.pid = $('#cash_dropdown').val();
			pledger.description = $('#field_dropdown').find('option:selected').text()+' '+$('#bank_dropdown').val();
			pledger.remarks =  $('#txtRemarks2').val();
			pledger.date = $('#current_date').val();
			pledger.debit = (_etype === 'cspv') ? $('#txtTTAmountVal').val() : 0 ; //$('#txtTTAmountVal').val();
			pledger.credit = (_etype === 'csrv') ? $('#txtTTAmountVal').val() : 0 ; //0;
			pledger.dcno = $('#txtVrnoaHidden').val();
			pledger.invoice = $('#txtVrnoaHidden').val();
			pledger.etype = _etype;
			pledger.pid_key = $('#field_dropdown').val();
			pledger.uid = $('#uid').val();
			pledger.company_id = $('#cid').val();	
			pledger.isFinal = 0;
			ledgers.push(pledger);
		}


		
		var data = {};
		data.cashmain 	= cashmain;
		data.ledger 	= ledgers;
		// console.log(ledgers);
		data.vrnoa 		= $('#txtVrnoaHidden').val();

		return data;
	}




	var getNumVal = function(el){
	  return isNaN(parseFloat(el.val())) ? 0 : parseFloat(el.val());
	}
	var calculateLowerTotal = function( amount ) {
		var totals = 0;
		$('#conversionTable').find('tbody tr').each(function( index, elem ) {
			
			totals += getNumVal($(elem).find('td.thirdTd').find('input'));

		});

		$('#txtTotal').val(totals);

	}

	var fetchLastLedger = function(partyId) {
		$.ajax({

			url : base_url + 'index.php/cashslip/fetchLastLedger',
			type : 'POST',
			data : { 'party_id' : partyId, 'to_date' : $('#current_date').val()},
			dataType : 'JSON',
			
			success : function(data) {
				console.log(data);

				$('#lastLedgerTable tbody').html('');
				var total_qty = 0;
				if(data && data != 'false'){

					$.each(data, function(index, elem) {
						
						appendTo_lastLedgerTable(elem.date, elem.debit, elem.credit);
					});
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}    

	var appendTo_lastLedgerTable = function(date, debit, credit) {

		var row = 	"<tr>" +
						"<td class='numeric' data-title='date' > "+ date.substr(0,10) +"</td>" +
					 	"<td class='numeric' data-title='debit'> "+ debit +"</td>" +
					 	"<td class='numeric' data-title='credit'> "+ credit +"</td>" +
				 	"</tr>";
		console.log(row);
			$(row).appendTo('#lastLedgerTable tbody');
		
	}
	let fetchImages = function(vrnoa,etype)
	{
		$.ajax({
			url : base_url + 'index.php/fileuploader/fetchImages',
			type : 'POST',
			data : { 'vrnoa' : vrnoa, 'etype' : etype },
			dataType : 'JSON',
			success : function(data)
			{
				// console.log(data);
				if (data === 'false')
				{
					alert('No Attachments found');
				} 
				else
				{
					populateImages(data);
				}

			}, 
			error : function(xhr, status, error)
			{
				console.log(xhr.responseText);
			}
		});
	}

	let updateRemarks = function(data,etype)
	{

		$.ajax({
			url : base_url + 'index.php/fileuploader/updateFilesRemarks',
			type : 'POST',
			data : { 'vrnoa' : $('#txtIdImg').val(), 'etype' : etype, 'data' : data },
			dataType : 'JSON',
			beforeSend : function()
			{
				$(".loader").show();
			},
			success : function(data) 
			{
				if($.inArray( false, data ) == -1)
				{
					alert('Files Uploaded successfully With Remarks!');
					general.reloadWindow();
				}
				$('.loader').show();		
			}, 
			error : function(xhr, status, error)
			{
				console.log(xhr.responseText);
				$('.loader').hide();
			}
		});
	}

	let populateImages = function(data)
	{
		$(".dz-preview").empty();

		$.each(data, function(i, elem)
		{
			let re 	= /(?:\.([^.]+))?$/;
			let ext = re.exec(data[i]['photo'])[1];

			ext = ext.toLowerCase();
			// Create the mock file:

			let mockFile = { name: data[i]['photo'], size: 12345 };
			if (data[i]['photo'] != "")
			{
				var src = base_url + 'assets/uploads/' + data[0]['etype'] + '/';
				if(ext == 'jpg' || ext == 'jpeg' || ext == 'bmp' || ext == 'png' || ext == 'gif')
				{
					// Call the default addedfile event handler
					imageUpload.emit("addedfile", mockFile);

					// And optionally show the thumbnail of the file:
					imageUpload.emit("thumbnail", mockFile, src + data[i]['photo']);
				} 
				else
				{

					// Call the default addedfile event handler
					imageUpload.emit("addedfile", mockFile);

					// And optionally show the thumbnail of the file:
					imageUpload.emit("thumbnail", mockFile, base_url + '/assets/uploads/icons/doc_icon.png');
				}

				$('.dz-remove').each(function(index, elem)
				{
					if(index == i)
					{
						$(this).after("<a class='btn btn-primary downloadBtn' href='" + src + data[i]['photo'] + "' target='_blank'>Download <i class='fa fa-download'></i></a>");
						var imageName = document.createElement('span');
						$(imageName).addClass('text-success');
						if(data[i]['photo'].length > 23)
						{
							var newName = data[i]['photo'].slice(0,23);
							imageName.innerHTML = newName + '...';

						} 
						else
						{
							imageName.innerHTML = data[i]['photo'];
						}

						$(this).before(imageName);
						let remarks = data[i]['remarks'];

						$(this).siblings('.attachRemarks').val(remarks);
					}
				});
				
				// // Or if the file on your server is not yet in the right
				// // size, you can let Dropzone download and resize it
				// // callback and crossOrigin are optional.
				// // imageUpload.createThumbnailFromUrl(file, imageUrl, callback, crossOrigin);
				// imageUpload.files.push( mockFile ); 
				// Make sure that there is no progress bar, etc...
				imageUpload.emit("complete", mockFile);
				// If you use the maxFiles option, make sure you adjust it to the
				// correct amount:
				var existingFileCount = 1; // The number of files already uploaded
				imageUpload.options.maxFiles = imageUpload.options.maxFiles - existingFileCount;
			}
		});
	}


	return {

		init : function() {

			this.bindUI();
		},

		bindUI : function() {

			var self 	= this;
			let etype 	= ($('#cpv').is(':checked') === true) ? 'cspv' : 'csrv'; 
			Dropzone.options.imageUpload = {
				url: base_url + "index.php/fileuploader/uploadFiles",
				maxFilesize: 1,
				acceptedFiles: "image/*,application/pdf,.docx,.doc,.xls,.xlsx,.txt,.rtf",
				parallelUploads: 100,
		        maxFiles: 100,
		        thumbnailWidth:"180",
           		thumbnailHeight:"180",
           		autoProcessQueue: false,
		        dictRemoveFileConfirmation: "Are you Sure to Delete this File?",
		        dictDefaultMessage: "Drop Files here to upload",
		        addRemoveLinks: true,
		        dictResponseError: 'Server not Configured',
		        dictRemoveFile: "X (remove)",
		        accept: function(file, done)
		        {
					// alert('file');
		        	let src 		= base_url + '/assets/uploads/icons/';
		        	let thumbnail 	= $('.dropzone .dz-preview.dz-file-preview .dz-image');

		        	switch (file.type)
		        	{
		        		case 'application/pdf':
		        		thumbnail.css({'background': 'url('+ src +'doc_icon.png', 'background-size': '100% 100%'});
		        		break;
		        		case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
		        		thumbnail.css({'background': 'url('+ src +'doc_icon.png', 'background-size': '100% 100%'});
		        		break;
		        		case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
				       	thumbnail.css({'background': 'url('+ src +'doc_icon.png', 'background-size': '100% 100%'});
				       	break;
				       	case 'application/vnd.ms-excel':
				       	thumbnail.css({'background': 'url('+ src +'doc_icon.png', 'background-size': '100% 100%'});
				        break;
				    }
				    done();
				},
		        
		        init : function()
		        {
		        	imageUpload = this;
			        //Restore initial message when queue has been completed]
			        this.on("addedfile", function(file, dataUrl)
			        {
			        	// console.log(file.previewTemplate);
			        	let elem = "<input type='text' placeholder='Enter Remarks' class='form-control attachRemarks' id='attachRemark' />";
			        	$(file.previewTemplate).append(elem);
			    	});
			    	this.on('sending', function(file, xhr, formData)
			    	{
			    		let remarks = $(file.previewTemplate).find('.attachRemarks').val();
			    		formData.append('vrnoa', 	$("#txtIdImg").val());
			    		formData.append('imgName', 	file.name);
			    		formData.append('etype', 	etype);
			    		formData.append('remarks', 	remarks);
			    		formData.append('table', 	$('#tablename').val());					    
					});

					this.on('success', function(file, response)
					{
						if (response===true) 
						{
							alert('File Upload successfully!!!');
							general.reloadWindow();
						}
						else
						{	
							alert('This Voucher Not Saved!!!');
							general.reloadWindow();
						}
					});

					this.on('error', function(file, errorMessage, xhr)
					{
						$(file.previewElement).find('.dz-error-message').text(errorMessage['message']);
					});

					this.on('removedfile', function(file)
			        {
						if ($('#voucher_type_hidden').val()=='edit' && $('#uploadFiles').data('delete')==0 ){
							alert('Sorry! you have not delete rights..........');
						}
						else if($('#voucher_type_hidden').val()=='new' && $('#uploadFiles').data('delete')==0)
						{
							alert('Sorry! you have not delete rights..........');
						} 
						else 
						{
							$.ajax({
								url : base_url + "index.php/fileuploader/deleteImage",
								type : 'POST',
								data : {'imgName' : file.name, 'item_id': $("#txtIdImg").val(), 'etype' : etype},
								success : function(response)
								{
									alert(response + "  Removed");
								   },
								   error : function(xhr, status, error)
								   {
									   alert('File Not Found on Server!');
									console.log(xhr.responseText);
								}
							   });	
						}
			        	
					});

			     },
		    };
		    $('#uploadFiles').on('click', function(e)
		    {
				if ($('#voucher_type_hidden').val()=='edit' && $('#uploadFiles').data('update')==0 ){
					alert('Sorry! you have not update  rights..........');
				}else if($('#voucher_type_hidden').val()=='new' && $('#uploadFiles').data('insert')==0){
					alert('Sorry! you have not insert rights..........');
				}
				else
				{
		    	e.preventDefault();
		    	let files = [];
		    	if($('.dz-preview').length > 0)
		    	{
		    		$('.dz-preview').each(function(e)
		    		{
		    			let obj 	= {};
		    			
		    			obj.photo 	= $(this).find('.dz-filename span').text();
		    			obj.remarks = $(this).find('#attachRemark').val();
		    			
		    			files.push(obj);
		    		});

		    		updateRemarks(files,etype);
		    		imageUpload.processQueue();
		    	} 
		    	else
		    	{
		    		alert('No Files Found to Upload');
				}
			}
			});
			
		    $("#viewImages").on('click', function(e){
				e.preventDefault();
				var vrnoa = $('#txtIdImg').val().trim();
				$('.dz-preview').remove();
				$('.dz-default.dz-message').css('display', 'block');
				// var etype = ($('#cpv').is(':checked') === true) ? 'cpv' : 'crv';
				fetchImages(vrnoa,etype);
			});
            $(".btnPrint").attr("disabled","disabled").next().attr("disabled","disabled")

			$('#voucher_type_hidden').val('new');
			$('#txtVrnoa').on('change', function() {
				var etype = ($('#cpv').is(':checked') === true) ? 'cspv' : 'csrv';
				fetch($(this).val() , etype);
			});

			// $()
			// when save button is clicked
			$('.btnSave').on('click', function(e) {

				if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('updatebtn')==0 ){
					alert('Sorry! you have not update rights..........');
				}else if($('#voucher_type_hidden').val()=='new' && $('.btnSave').data('insertbtn')==0){
					alert('Sorry! you have not insert rights..........');
				}else{
					e.preventDefault();
					self.initSave();
				}
			});

			$('.btnSave1').on('click', function(e) {
				
				$('.btnSave').click();
			})
			$('.btnDelete').on('click', function(e){
					var etype = ($('#cpv').is(':checked') === true) ? 'cspv' : 'csrv';
					if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('deletebtn')==0 ){
						alert('Sorry! you have not delete rights..........');
					}else{
						e.preventDefault();
						var vrnoa = $('#txtVrnoaHidden').val();
						if (vrnoa !== '') {
						if (confirm('Are you sure to delete this voucher?'))
							deleteVoucher(vrnoa , etype);
					}
			}});

			$('.btnPrintThermal, .btnPrint').on('click', function(e) {
			    e.preventDefault();
			    window.open(base_url + 'application/views/reportprints/cash_slip.php', "Cash Slip", 'width=1000, height=842');
			});

			$("#cash_dropdown").prop("selectedIndex", 1);

			// when the reset button is clicked
			$('.btnReset').on('click', function(e) {
				e.preventDefault();		// prevent the default behaviour of the link

                window.location.reload();
			});

			$('#cus_dropdown').on('change',function(){

				var partyId = $(this).val();
				var vrDate = $('#current_date').val();
				var pbalance = $(this).find('option:selected').data('pbalance');
				$('#pbalance').text(parseFloat(pbalance).toFixed(0));
				fetchLastLedger(partyId);
			});

			$('#conversionTable').on('input', '.firstVal', function(e) {
				e.preventDefault();
				var enterAmount= getNumVal($(this).closest('tr').find('td.firsrtTd').find('input'));
				var fixAmount = getNumVal($(this).closest('tr').find('td.secondTd').find('input'));
				var conversionAmount = 0;
				if(fixAmount != 0){
					 conversionAmount = parseFloat(enterAmount) * parseFloat(fixAmount);
					//alert(fixAmount);
				}	
				else{
					var exchangeRate = getNumVal($(this).closest('tr').find('td.exchangeRate').find('input'));
					if(exchangeRate != 0){
						conversionAmount = parseFloat(enterAmount) * parseFloat(exchangeRate);
					}
					else{
						conversionAmount = parseFloat(enterAmount);
					}
				}
				$(this).closest('tr').find('td.thirdTd').find('input').val(conversionAmount);
				calculateLowerTotal();
			});	

			$('#cpv').on('click', function() {

				window.location = 'cashslip?etype=cspv';
				// resetFields();
				// var check = $(this).is(':checked');
				// if (check) {
				// 	getMaxVrnoa('cspv');
				// }
			});
			$('#crv').on('click', function() {
				
				window.location = 'cashslip?etype=csrv';
				// resetFields();
				// var check = $(this).is(':checked');
				// if (check) {
				// 	getMaxVrnoa('csrv');
				// }
			});

			$('#conversionTable').on('input', '.exchangeRate', function(e) {
				e.preventDefault();
				var exchangeRateAmount= getNumVal($(this).closest('tr').find('td.exchangeRate').find('input'));
				var enterAmount = getNumVal($(this).closest('tr').find('td.firsrtTd').find('input'));
				var conversionAmount = 0;
				if(enterAmount != 0){
					if(exchangeRateAmount !=0){
					    conversionAmount = parseFloat(enterAmount) * parseFloat(exchangeRateAmount);
					}
					else{
						conversionAmount = parseFloat(enterAmount);
					}
					$(this).closest('tr').find('td.thirdTd').find('input').val(parseFloat(conversionAmount));
				}else{
					$(this).closest('tr').find('td.thirdTd').find('input').val('');
				}
				calculateLowerTotal();	
				
				
			});	
			// when text is chenged inside the id textbox
			$('#txtId').on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {

					// get the based on the id entered by the user
					if ( $('#txtId').val().trim() !== "" ) {

						var transporter_id = $.trim($('#txtId').val());
						fetch(transporter_id);
					}
				}
			});

			// when edit button is clicked inside the table view
			$('.btn-edit-dept').on('click', function(e) {
				e.preventDefault();

				fetch($(this).data('transporter_id'));		// get the class detail by id
				$('a[href="#add_transporter"]').trigger('click');
			});


			shortcut.add("F10", function() {
    			$('.btnSave').trigger('click');
			});
			
			shortcut.add("F9", function() {
				$('.btnPrintThermal').trigger('click');
			});
			shortcut.add("F6", function() {
    			$('#txtVrnoa').focus();
    			// alert('focus');
			});
			shortcut.add("F5", function() {
    			general.reloadWindow();
			});

			shortcut.add("F12", function() {
    			$('.btnDelete').trigger('click');
			});


			transporter.fetchRequestedVr();
		},
		fetchRequestedVr : function () {

			var vrnoa = general.getQueryStringVal('vrnoa');
			var etype = general.getQueryStringVal('etype');
			vrnoa = parseInt( vrnoa );
			$('#txtVrnoa').val(vrnoa);
			$('#txtVrnoaHidden').val(vrnoa);
			if ( !isNaN(vrnoa && etype !== '') ) {
			
				if (etype=='csrv'){
			
					$('#crv').prop('checked', true);	
				}else{
			
					$('#cpv').prop('checked', true);	
				}
				fetch(vrnoa, etype);
			}else if(etype && etype !== ''){

				if (etype=='cspv'){
			
					$('#cpv').prop('checked', true);	
				}else{
				
					etype = 'csrv';
					$('#crv').prop('checked', true);	
				}
				
				getMaxVrnoa(etype);

			}else{
				//getMaxVrno();
				getMaxVrnoa(($('#cpv').is(':checked') === true) ? 'cspv' : 'csrv');
				//getMaxVrnoa();
			}
		},

		// makes the voucher ready to save
		initSave : function() {

			var saveObj = getSaveObject();	// returns the class detail object to save into database
			var error = validateSave();			// checks for the empty fields
			var etype = ($('#cpv').is(':checked') === true) ? 'cspv' : 'csrv';
			if ( !error ) {
				save( saveObj , etype );
			} else {
				alert('Correct the errors...');
			}
		},

		// resets the voucher
		resetVoucher : function() {

			$('.inputerror').removeClass('inputerror');
			$('#txtId').val('');
			$('#txtName').val('');
			$('#txtContact').val('');
			$('#txtPhone').val('');
			$('#txtAreaCover').val('');
			$('#txtnewentry').val('');

			getMaxVrnoa(($('#cpv').is(':checked') === true) ? 'cspv' : 'csrv');		// gets the max id of voucher
		}
	};
};

var transporter = new Transporter();
transporter.init();