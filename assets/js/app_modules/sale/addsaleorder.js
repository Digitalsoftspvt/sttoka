var saleorder = function() {
	var settings = {
		// basic information section
		switchPreBal : $('#switchPreBal')

	};

	var saveItem = function( item ) {
		$.ajax({
			url : base_url + 'index.php/item/save',
			type : 'POST',
			data : item,
			// processData : false,
			// contentType : false,
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					alert('Item saved successfully.');
					$('#ItemAddModel').modal('hide');
					fetchItems();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var saveAccount = function( accountObj ) {
		$.ajax({
			url : base_url + 'index.php/account/save',
			type : 'POST',
			data : { 'accountDetail' : accountObj },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'false') {
					alert('An internal error occured while saving account. Please try again.');
				} else {
					alert('Account saved successfully.');
					$('#AccountAddModel').modal('hide');
					fetchAccount();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var fetchAccount = function() {

		$.ajax({
			url : base_url + 'index.php/account/fetchAll',
			type : 'POST',
			data : { 'active' : 1,'typee':'purchase return'},
			dataType : 'JSON',
			success : function(data) {
				if (data === 'false') {
					alert('No data found');
				} else {
					populateDataAccount(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var fetchItems = function() {
		$.ajax({
			url : base_url + 'index.php/item/fetchAll',
			type : 'POST',
			data : { 'active' : 1 },
			dataType : 'JSON',
			success : function(data) {
				if (data === 'false') {
					alert('No data found');
				} else {
					populateDataItem(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateDataAccount = function(data) {
		$("#party_dropdown").empty();
		
		$.each(data, function(index, elem){
			var opt="<option value='"+elem.party_id+"' >" +  elem.name + "</option>";
			$(opt).appendTo('#party_dropdown');
		});
	}
	var populateDataItem = function(data) {
		$("#itemid_dropdown").empty();
		$("#item_dropdown").empty();

		$.each(data, function(index, elem){
			var opt="<option value='"+elem.item_id+"' data-prate= '"+ elem.cost_price +"' data-uom_item= '"+ elem.uom +"' data-grweight= '"+ elem.grweight +"' >" +  elem.item_des + "</option>";
			 // var = "<option value='" + $item['item_id'] + "' data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>"><?php echo $item['item_des']; ?></option>";
			$(opt).appendTo('#item_dropdown');
			var opt1="<option value='"+elem.item_id+"' data-prate= '"+ elem.cost_price +"' data-uom_item= '"+ elem.uom +"' data-grweight= '"+ elem.grweight +"' >" +  elem.item_id + "</option>";
			 // var = "<option value='" + $item['item_id'] + "' data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>"><?php echo $item['item_des']; ?></option>";
			$(opt1).appendTo('#itemid_dropdown');

		});
	}
	var getSaveObjectAccount = function() {

		var obj = {
			pid : '20000',
			active : '1',
			name : $.trim($('#txtAccountName').val()),
			level3 : $.trim($('#txtLevel3').val()),
			dcno : $('#txtVrnoa').val(),
			etype : 'sale_order',
			uid : $.trim($('#uid').val()),
			company_id : $.trim($('#cid').val()),
		};

		return obj;
	}
	var getSaveObjectItem = function() {
		
		var itemObj = {
			item_id : 20000,
			active : '1',
			open_date : $.trim($('#current_date').val()),
			catid : $('#category_dropdown').val(),
			subcatid : $.trim($('#subcategory_dropdown').val()),
			bid : $.trim($('#brand_dropdown').val()),
			barcode : $.trim($('#txtBarcode').val()),
			description : $.trim($('#txtItemName').val()),
			item_des : $.trim($('#txtItemName').val()),
			cost_price : $.trim($('#txtPurPrice').val()),
			srate : $.trim($('#txtSalePrice').val()),
			uid : $.trim($('#uid').val()),
			company_id : $.trim($('#cid').val()),
			uom : $.trim($('#uom_dropdown').val()),
		};
		
		return itemObj;
	}
	var populateDataGodowns = function(data) {
        $("#dept_dropdown").empty();
        $.each(data, function(index, elem){
            var opt1="<option value=" + elem.did + ">" +  elem.name + "</option>";
            $(opt1).appendTo('#dept_dropdown');
        });
    }
    var fetchGodowns = function() {
        $.ajax({
            url : base_url + 'index.php/department/fetchAllDepartments',
            type : 'POST',
            dataType : 'JSON',
            success : function(data) {
                if (data === 'false') {
                    alert('No data found');
                } else {
                    populateDataGodowns(data);
                }
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }
    var getSaveObjectGodown = function() {
        var obj = {};
        obj.did = 20000;
        obj.name = $.trim($('#txtNameGodownAdd').val());
        obj.description = $.trim($('.page_title').val());
        return obj;
    }
    var saveGodown = function( department ) {
        $.ajax({
            url : base_url + 'index.php/department/saveDepartment',
            type : 'POST',
            data : { 'department' : department },
            dataType : 'JSON',
            success : function(data) {

                if (data.error === 'false') {
                    alert('An internal error occured while saving department. Please try again.');
                } else {
                    alert('Department saved successfully.');
                    $('#GodownAddModel').modal('hide');
                    fetchGodowns();
                }
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }
    var validateSaveGodown = function() {
        var errorFlag = false;
        var _desc = $.trim($('#txtNameGodownAdd').val());
        $('.inputerror').removeClass('inputerror');
        if ( !_desc ) {
            $('#txtNameGodownAdd').addClass('inputerror');
            errorFlag = true;
        }
        return errorFlag;
    }
	var validateSaveItem = function() {

		var errorFlag = false;
		// var _barcode = $('#txtBarcode').val();
		var _desc = $.trim($('#txtItemName').val());
		var cat = $.trim($('#category_dropdown').val());
		var subcat = $('#subcategory_dropdown').val();
		var brand = $.trim($('#brand_dropdown').val());
		var uom_ = $.trim($('#uom_dropdown').val());
		// remove the error class first
		
		$('.inputerror').removeClass('inputerror');
		if ( !uom_ ) {
			$('#uom_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		if ( _desc === '' || _desc === null ) {
			$('#txtItemName').addClass('inputerror');
			errorFlag = true;
		}
		if ( !cat ) {
			$('#category_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		if ( !subcat ) {
			$('#subcategory_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		if ( !brand ) {
			$('#brand_dropdown').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}
	var validateSaveAccount = function() {

		var errorFlag = false;
		var partyEl = $('#txtAccountName');
		var deptEl = $('#txtLevel3');

		// remove the error class first
		$('.inputerror').removeClass('inputerror');

		if ( !partyEl.val() ) {
			$('#txtAccountName').addClass('inputerror');
			errorFlag = true;
		}
		if ( !deptEl.val() ) {
			deptEl.addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}
	


	var save = function(saleorder) {
		general.disableSave();
		$.ajax({
			url : base_url + 'index.php/saleorder/save',
			type : 'POST',
			data : { 'ordermain' : saleorder.ordermain, 'orderdetail' : saleorder.orderdetail, 'vrnoa' : saleorder.vrnoa, 'ledger' : saleorder.ledger ,'voucher_type_hidden':$('#voucher_type_hidden').val() ,'etype':'sale_order','vrdate' : $('#vrdate').val()},
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'date close') {
					alert('Sorry! you can not insert update in close date................');
				}else if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					var printConfirmation = confirm('Voucher saved!\nWould you like to print the invoice as well?');
					if (printConfirmation === true) {
						Print_Voucher(1,'lg','');
						general.reloadWindow();
					} else {
						general.reloadWindow();
					}
				}
				general.enableSave();
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var Print_Voucher = function(hd,prnt,wrate) {
		if ( $('.btnSave').data('printbtn')==0 ){
				alert('Sorry! you have not print rights..........');
		}else{
			var etype=  'sale_order';
			var vrnoa = $('#txtVrnoa').val();
			var company_id = $('#cid').val();
			var user = $('#uname').val();
			// var hd = $('#hd').val();
			var pre_bal_print = ($(settings.switchPreBal).bootstrapSwitch('state') === true) ? '1' : '0';
			
			var url = base_url + 'index.php/doc/Print_Order_Voucher/' + etype + '/' + vrnoa + '/' + company_id + '/' + '-1' + '/' + user + '/' + pre_bal_print+ '/' + hd + '/' + prnt + '/' + wrate;
			// var url = base_url + 'index.php/doc/CashVocuherPrintPdf/' + etype + '/' + dcno   + '/' + companyid + '/' + '-1' + '/' + user;
			window.open(url);
		}

	}

	var Print_Vouchers = function(hd,prnt,wrate) {
		if ( $('.btnSave').data('printbtn')==0 ){
				alert('Sorry! you have not print rights..........');
		}else{
			var etype=  'sale_order';
			var vrnoa = $('#txtVrnoa').val();
			var company_id = $('#cid').val();
			var user = $('#uname').val();
			// var hd = $('#hd').val();
			var pre_bal_print = ($(settings.switchPreBal).bootstrapSwitch('state') === true) ? '1' : '0';
			
			var url = base_url + 'index.php/doc/Print_Order_Vouchers/' + etype + '/' + vrnoa + '/' + company_id + '/' + '-1' + '/' + user + '/' + pre_bal_print+ '/' + hd + '/' + prnt + '/' + wrate;
			// var url = base_url + 'index.php/doc/CashVocuherPrintPdf/' + etype + '/' + dcno   + '/' + companyid + '/' + '-1' + '/' + user;
			window.open(url);
		}

	}

	var fetch = function(vrnoa) {

		$.ajax({

			url : base_url + 'index.php/saleorder/fetch',
			type : 'POST',
			data : { 'vrnoa' : vrnoa , 'company_id': $('#cid').val(),'etype':'sale_order'},
			dataType : 'JSON',
			success : function(data) {

				resetFields();
				$('#txtOrderNo').val('');
				if (data === 'false') {
					alert('No data found.');
					general.reloadWindow();
				} else {
					populateData(data);
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	


	var populateData = function(data) {

		$('#txtVrno').val(data[0]['vrno']);
		$('#txtVrnoHidden').val(data[0]['vrno']);
		$('#txtVrnoaHidden').val(data[0]['vrnoa']);
		$('#current_date').datepicker('update', data[0]['vrdate'].substr(0, 10));
		$('#vrdate').datepicker('update', data[0]['vrdate'].substr(0, 10));
		$('#party_dropdown').select2('val', data[0]['party_id']);
		$('#txtInvNo').val(data[0]['inv_no']);
		// $('#due_date').datepicker('update', data[0]['due_date'].substr(0, 10));
		$('#receivers_list').val(data[0]['received_by']);

		$('#transporter_dropdown').select2('val', data[0]['transporter_id']);
		$('#txtRemarks').val(data[0]['remarks']);
		$('#txtNetAmount').val(data[0]['namount']);
		$('#txtOrderNo').val(data[0]['ordno']);
		
		$('#txtDiscount').val(data[0]['discp']);
		$('#txtExpense').val(data[0]['exppercent']);
		$('#txtExpAmount').val(data[0]['expense']);
		$('#txtTax').val(data[0]['taxpercent']);
		$('#txtTaxAmount').val(data[0]['tax']);
		$('#txtDiscAmount').val(data[0]['discount']);
		$('#user_dropdown').val(data[0]['uid']);
		// $('#txtPaid').val(data[0]['paid']);

		$('#dept_dropdown').select2('val', data[0]['godown_id']);
		$('#voucher_type_hidden').val('edit');		
		$('#user_dropdown').val(data[0]['uid']);
		$.each(data, function(index, elem) {
            elem.fedamount=parseFloat(elem.fedamount).toFixed(2);
			appendToTable('', elem.item_name, elem.item_id, elem.qty, elem.rate, elem.amount, elem.weight, elem.type,elem.fedrate,elem.fedamount, elem.item_code);
			
		});
	}

	// gets the max id of the voucher
	var getMaxVrno = function() {
		
		$.ajax({

			url : base_url + 'index.php/saleorder/getMaxVrno',
			type : 'POST',
			data : {'company_id': $('#cid').val(),'etype':'sale_order'},
			dataType : 'JSON',
			success : function(data) {

				$('#txtVrno').val(data);
				$('#txtMaxVrnoHidden').val(data);
				$('#txtVrnoHidden').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var getMaxVrnoa = function() {

		$.ajax({

			url : base_url + 'index.php/saleorder/getMaxVrnoa',
			type : 'POST',
			data : {'company_id': $('#cid').val() ,'etype':'sale_order' },
			dataType : 'JSON',
			success : function(data) {

				$('#txtVrnoa').val(data);
				$('#txtMaxVrnoaHidden').val(data);
				$('#txtVrnoaHidden').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var validateSingleProductAdd = function() {


		var errorFlag = false;
		var itemEl = $('#hfItemId');
		//var itemEl = $('#item_dropdown');
		var qtyEl = $('#txtQty');
		var rateEl = $('#txtPRate');

		// remove the error class first
		$('.inputerror').removeClass('inputerror');

		if ( !itemEl.val() ) {
			$('#txtItemId').addClass('inputerror');
			errorFlag = true;
		}
		if ( !qtyEl.val() ) {
			qtyEl.addClass('inputerror');
			errorFlag = true;
		}
		if ( !rateEl.val() ) {
			rateEl.addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var validateSingleProductAdd_less = function() {


		var errorFlag = false;
		var itemEl = $('#less_item_dropdown');
		var qtyEl = $('#less_txtQty');
		var rateEl = $('#less_txtPRate');

		// remove the error class first
		$('.inputerror').removeClass('inputerror');

		if ( !itemEl.val() ) {
			itemEl.addClass('inputerror');
			errorFlag = true;
		}
		if ( !qtyEl.val() ) {
			qtyEl.addClass('inputerror');
			errorFlag = true;
		}
		if ( !rateEl.val() ) {
			rateEl.addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}




	var appendToTable = function(srno, item_desc, item_id, qty, rate, amount, weight, tbl, fRate, fAmount, itemCode) {
        fAmount=parseFloat(fAmount).toFixed(2);
        amount=parseFloat(amount).toFixed(2);
		if (tbl=="add" ){
			var srno = $('#purchase_table tbody tr').length + 1;
			var row = 	"<tr>" +
						"<td class='srno numeric' data-title='Sr#' > "+ srno +"</td>" +
						"<td class='item_code' data-title='Item Code'> "+ itemCode +"</td>" +
				 		"<td class='item_desc' data-title='Description' data-item_id='"+ item_id +"'> "+ item_desc +"</td>" +
				 		"<td class='qty numeric text-right' data-title='Qty'>  "+ qty +"</td>" +
					 	"<td class='weight numeric text-right' data-title='Weigh' > "+ weight +"</td>" +
					 	"<td class='rate numeric text-right' data-title='Rate'> "+ rate +"</td>" +
					 	"<td class='frate numeric text-right' data-title='F.Rate'> "+ fRate +"</td>" +
					 	"<td class='famount numeric text-right' data-title='F.Amount'> "+ fAmount +"</td>" +
					 	"<td class='amount numeric text-right' data-title='Amount' > "+ amount +"</td>" +
					 	"<td><a href='' class='btn btn-primary btnRowEdit'><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td>" +
				 	"</tr>";

			calculateLowerTotal(qty, amount, weight,0,fAmount);
		}else{
			var srno = $('#purchase_table_less tbody tr').length + 1;
			var row = 	"<tr>" +
						"<td class='srno numeric' data-title='Sr#' > "+ srno +"</td>" +
				 		"<td class='item_desc' data-title='Description' data-item_id='"+ item_id +"'> "+ item_desc +"</td>" +
				 		"<td class='qty numeric text-right' data-title='Qty'>  "+ qty +"</td>" +
					 	"<td class='weight numeric text-right' data-title='Weigh' > "+ weight +"</td>" +
					 	"<td class='rate numeric text-right' data-title='Rate'> "+ rate +"</td>" +
					 	"<td class='amount numeric text-right' data-title='Amount' > "+ amount +"</td>" +
					 	"<td><a href='' class='btn btn-primary btnRowEdit'><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td>" +
				 	"</tr>";
			calculateLowerTotal(0, 0,0,amount,0);
		}

		
		

		if (tbl=="add" ){
			$(row).appendTo('#purchase_table');
		}else{
			$(row).appendTo('#purchase_table_less');
		}


	}

	var getPartyId = function(partyName) {
		var pid = "";
		$('#party_dropdown option').each(function() { if ($(this).text().trim().toLowerCase() == partyName) pid = $(this).val();  });
		return pid;
	}



	var getSaveObject = function() {

		
		var ordermain = {};
		var orderdetail = [];

		ordermain.vrno = $('#txtVrnoHidden').val();
		ordermain.vrnoa = $('#txtVrnoaHidden').val();
		ordermain.vrdate = $('#current_date').val();
		ordermain.party_id = $('#party_dropdown').val();
		ordermain.bilty_no = $('#txtInvNo').val();
		ordermain.bilty_date = $('#due_date').val();
		ordermain.received_by = $('#receivers_list').val();
		ordermain.transporter_id = $('#transporter_dropdown').val();
		ordermain.remarks = $('#txtRemarks').val();
		ordermain.etype = 'sale_order';
		ordermain.namount = $('#txtNetAmount').val();
		ordermain.ordno = $('#txtVrnoaHidden').val();
		ordermain.discp = $('#txtDiscount').val();
		ordermain.discount = $('#txtDiscAmount').val();
		ordermain.expense =$('#txtExpAmount').val();
		ordermain.exppercent = $('#txtExpense').val();
		ordermain.tax = $('#txtTaxAmount').val();
		ordermain.taxpercent = $('#txtTax').val();
		ordermain.paid = $('#txtPaid').val();

		ordermain.uid = $('#uid').val();
		ordermain.company_id = $('#cid').val();


		$('#purchase_table').find('tbody tr').each(function( index, elem ) {
			var sd = {};
			sd.oid = '';
			sd.item_id = $.trim($(elem).find('td.item_desc').data('item_id'));
			sd.godown_id = $('#dept_dropdown').val();
			sd.qty = $.trim($(elem).find('td.qty').text());
			sd.weight = $.trim($(elem).find('td.weight').text());
			sd.rate = $.trim($(elem).find('td.rate').text());
			sd.amount = $.trim($(elem).find('td.amount').text());
			sd.fedrate = $.trim($(elem).find('td.frate').text());
			sd.fedamount = $.trim($(elem).find('td.famount').text());

			sd.netamount = $.trim($(elem).find('td.amount').text());
			sd.type="add";
			orderdetail.push(sd);
		});

		$('#purchase_table_less').find('tbody tr').each(function( index, elem ) {
			var sd = {};
			sd.oid = '';
			sd.item_id = $.trim($(elem).find('td.item_desc').data('item_id'));
			sd.godown_id = $('#dept_dropdown').val();
			sd.qty = $.trim($(elem).find('td.qty').text());
			sd.weight = $.trim($(elem).find('td.weight').text());
			sd.rate = $.trim($(elem).find('td.rate').text());
			sd.amount = $.trim($(elem).find('td.amount').text());
			sd.netamount = $.trim($(elem).find('td.amount').text());
			sd.type="less";
			orderdetail.push(sd);
		});


		
		var data = {};
		data.ordermain = ordermain;
		data.orderdetail = orderdetail;
		data.vrnoa = $('#txtVrnoaHidden').val();

		return data;
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		var partyEl = $('#party_dropdown');
		var deptEl = $('#dept_dropdown');

		// remove the error class first
		$('.inputerror').removeClass('inputerror');

		if ( !partyEl.val() ) {
			partyEl.addClass('inputerror');
			errorFlag = true;
		}
		if ( !deptEl.val() ) {
			deptEl.addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}
	

	var deleteVoucher = function(vrnoa) {

		$.ajax({
			url : base_url + 'index.php/saleorder/delete',
			type : 'POST',
			data : { 'chk_date' : $('#current_date').val(), 'vrdate' : $('#vrdate').val(), 'vrnoa' : vrnoa , 'etype':'sale_order','company_id':$('#cid').val() },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'date close') {
					alert('Sorry! you can not delete in close date................');
				}else if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	///////////////////////////////////////////////////////////////
	/// calculations related to the overall voucher
	////////////////////////////////////////////////////////////////
	var calculateLowerTotal = function(qty, amount, weight,ammount_less,fAmount) {

		var _qty = getNumVal($('#txtTotalQty'));
		var _weight = getNumVal($('#txtTotalWeight'));
		var _famount = getNumVal($('#txtTotalFAmount'));
		var _amnt = getNumVal($('#txtTotalAmount'));

		var _discp = getNumVal($('#txtDiscount'));
		var _disc = getNumVal($('#txtDiscAmount'));
		var _tax = getNumVal($('#txtTax'));
		var _taxamount = getNumVal($('#txtTaxAmount'));
		var _expense = getNumVal($('#txtExpAmount'));
		var _exppercent = getNumVal($('#txtExpense'));
		var _amountless = getNumVal($('#txtPaid'));


		var tempQty = parseFloat(_qty) + parseFloat(qty);
		$('#txtTotalQty').val(tempQty);


		var tempAmountLess = parseFloat(_amountless) + parseFloat(ammount_less);
		$('#txtPaid').val(tempAmountLess);
		
		

		var tempAmnt = parseFloat(_amnt) + parseFloat(amount);
		$('#txtTotalAmount').val(tempAmnt);

		var totalWeight = parseFloat(parseFloat(_weight) + parseFloat(weight)).toFixed(2);
		$('#txtTotalWeight').val(totalWeight);

		var totalFAmount = parseFloat(parseFloat(_famount) + parseFloat(fAmount)).toFixed(2);
		$('#txtTotalFAmount').val(totalFAmount);


		var net = parseFloat(tempAmnt) - parseFloat(_disc) + parseFloat(_taxamount) + parseFloat(_expense)-parseFloat(tempAmountLess)+parseFloat(totalFAmount) ;
		$('#txtNetAmount').val(net);
		// alert('call');

	}

	var getNumVal = function(el){
		return isNaN(parseFloat(el.val())) ? 0 : parseFloat(el.val());
	}

	///////////////////////////////////////////////////////////////
	/// calculations related to the single product calculation
	///////////////////////////////////////////////////////////////
	var calculateUpperSum = function() {

		var _qty = getNumVal($('#txtQty'));
		var _amnt = getNumVal($('#txtAmount'));
		var _net = getNumVal($('#txtNet'));
		var _prate = getNumVal($('#txtPRate'));
		var _gw = getNumVal($('#txtGWeight'));
        $('#txtWeight').val(getNumVal($('#txtGWeight'))*getNumVal($('#txtQty')));
		var _weight= getNumVal($('#txtWeight'));
		var _uom=$('#txtUom').val().toUpperCase();
		var fRate = getNumVal($('#txtFRate'));

		// alert('uom_item ' + _uom);
		kg=-1;
		gram=-1;
		var kg = _uom.search("KG");
		var gram = _uom.search("GRAM");

		if (kg ==-1 && gram ==-1 ){

			var _tempAmnt = parseFloat(_qty) * parseFloat(_prate);		
			var _tempFAmnt = parseFloat(_qty) * parseFloat(fRate);	
		}else{

			var _tempAmnt = parseFloat(_weight) * parseFloat(_prate);			
			var _tempFAmnt = parseFloat(_weight) * parseFloat(fRate);	
		}
		
		//$('#txtWeight').val(parseFloat(_gw) * parseFloat(_qty));
		$('#txtAmount').val(_tempAmnt);
		$('#txtFAmount').val(_tempFAmnt);
	}

	var calculateUpperSum_Less = function() {

		var _qty = getNumVal($('#less_txtQty'));
		var _amnt = getNumVal($('#less_txtAmount'));
		var _net = getNumVal($('#less_txtNet'));
		var _prate = getNumVal($('#less_txtPRate'));
		var _gw = getNumVal($('#less_txtGWeight'));
		var _weight=getNumVal($('#less_txtWeight'));
		var _uom=$('#less_txtUom').val().toUpperCase();
		// alert('uom_item ' + _uom);
		kg=-1;
		gram=-1;
		var kg = _uom.search("KG");
		var gram = _uom.search("GRAM");
		if (kg ==-1 && gram ==-1 ){
			var _tempAmnt = parseFloat(_qty) * parseFloat(_prate);			
		}else{
			var _tempAmnt = parseFloat(_weight) * parseFloat(_prate);			
		}
		
		//$('#txtWeight').val(parseFloat(_gw) * parseFloat(_qty));
		$('#less_txtAmount').val(_tempAmnt);
	}


	var fetchThroughPO = function(po) {

		$.ajax({

			url : base_url + 'index.php/saleorderorder/fetch',
			type : 'POST',
			data : { 'vrnoa' : po },
			dataType : 'JSON',
			success : function(data) {

				resetFields();
				if (data === 'false') {
					alert('No data found.');
				} else {
					populatePOData(data);
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populatePOData = function(data) {

		$('#party_dropdown').select2('val', data[0]['party_id']);
		$('#txtRemarks').val(data[0]['remarks']);
		$('#txtDiscount').val(data[0]['discp']);
		$('#txtExpense').val(data[0]['exppercent']);
		$('#txtExpAmount').val(data[0]['expense']);
		$('#txtTax').val(data[0]['taxpercent']);
		$('#txtTaxAmount').val(data[0]['tax']);
		$('#txtDiscAmount').val(data[0]['discount']);

		$('#dept_dropdown').select2('val', data[0]['godown_id']);
		$('#txtNetAmount').val(data[0]['namount']);
		$('#voucher_type_hidden').val('edit');

		$.each(data, function(index, elem) {
			appendToTable('', elem.item_name, elem.item_id, elem.item_qty, elem.item_rate, elem.item_amount, elem.weight,"add");
			calculateLowerTotal(elem.item_qty, elem.item_amount, elem.weight,0,0);
		});
	}

	var resetFields = function() {

		$('#current_date').datepicker('update', new Date());
		$('#party_dropdown').select2('val', '');
		$('#txtInvNo').val('');
		$('#due_date').datepicker('update', new Date());
		$('#receivers_list').val('');
		$('#transporter_dropdown').select2('val', '');
		$('#txtRemarks').val('');
		$('#txtNetAmount').val('');		
		$('#txtDiscount').text('');
		$('#txtExpense').text('');
		$('#txtExpAmount').text('');
		$('#txtTax').text('');
		$('#txtTaxAmount').text('');
		$('#txtDiscAmount').text('');
		$('#txtPaid').val('');
		$('#txtTotalFAmount').val('');
		
		$('#txtTotalAmount').val('');
		$('#txtTotalQty').val('');
		$('#txtTotalWeight').val('');
		$('#dept_dropdown').select2('val', '');
		$('#voucher_type_hidden').val('new');
		$('table tbody tr').remove();
	}

	var clearItemData = function (){

		$('#stqty_lbl').text('Item');
        $("#hfItemId").val("");
        $("#hfItemSize").val("");
        $("#hfItemBid").val("");
        $("#hfItemUom").val("");
        $("#hfItemPrate").val("");
        $("#hfItemGrWeight").val("");
        $("#hfItemStQty").val("");
        $("#hfItemStWeight").val("");
        $("#hfItemLength").val("");
        $("#hfItemCatId").val("");
        $("#hfItemSubCatId").val("");
        $("#hfItemDesc").val("");
        $("#hfItemShortCode").val("");
        //$("#txtItemId").val("");
    }


	 var paging = function (total)
	{
    	$("#paging").smartpaginator({
        	totalrecords: parseInt(total),
        	recordsperpage: 10,
        	datacontainer: 'tbody',
        	dataelement: 'tr',
        	theme: 'custom',
        	onchange: onChange
    	});
	}
	 var onChange = function (newPageValue)
    {
        $.ajax({
            type: "POST",
            url: base_url + 'index.php/item/paging',
            data: {
                page_number: newPageValue,
                search: $.trim($("#txtItemSearch").val())
            }
        }).done(function (result) {

            var total = "";
            var jsonR = JSON.stringify($.parseJSON(result)).replace(/\\n/g, "\\n")
                                        .replace(/\\'/g, "\\'")
                                        .replace(/\\"/g, '\\"')
                                        .replace(/\\&/g, "\\&")
                                        .replace(/\\r/g, "\\r")
                                        .replace(/\\t/g, "\\t")
                                        .replace(/\\b/g, "\\b")
                                        .replace(/\\f/g, "\\f");
            var json = $.parseJSON(jsonR);
            total = json.total;

            if (json.records != false) {
                var html = "";
                $.each(json.records, function (index, value) {
                    html += "<tr id='" + value.item_id + "'>";
                    html += "<td>"+value.item_id+"<input type='hidden' name='hfModalitemId' value='" + value.item_id + "'></td>";
                    html += "<td class='tdItemCode'><b>" + value.item_code + "<b></td>";
                    html += "<td class='tdDescription'><b>" + value.item_des + "</b></td>";
                    html += "<td class='tdItemUom'><b>" + value.uom + "</b></td>";
                    html += "<td class='tdPRate hide'>" + value.cost_price + "</td>";
                    html += "<td class='tdGrWeight hide'>" + value.grweight + "</td>";
                    html += "<td class='tdStQty hide'>" + value.stqty + "</td>";
                    html += "<td class='tdStWeight hide'>" + value.stweight + "</td>";
                    html += "<td class='tdSrate1 hide'>" + value.srate1 + "</td>";
                    html += "<td class='tdSrate hide'>" + value.srate + "</td>";
                    html += "<td class='tdUItem hide'>" + value.uitem_des + "</td>";
                    html += "<td><a href='#' data-dismiss='modal' class='btn btn-primary populateItem'><i class='fa fa-search'></i></a></td>"
                    html += "</tr>";
                });

                $("#divNoRecord").removeClass('divNoRecord');
                $("#divNoRecord").html('');
                $("#tbItems > tbody").html('');
                $("#tbItems > tbody").append(html);

                $('.modal-lookup .populateItem').on('click', function()
                {	
                	var hiddenFieldValue = $('#hiddenFieldValue').val();
                    var itemId = $(this).closest('tr').find('input[name=hfModalitemId]').val();
                    var itemDescription = $(this).closest('tr').find('.tdDescription').text();
                    var uom = $(this).closest('tr').find('.tdItemUom').text();
                    var pRate = $(this).closest('tr').find('.tdPRate').text();
                    var grWeight = $(this).closest('tr').find('.tdGrWeight').text();
                    var stQty = $(this).closest('tr').find('.tdStQty').text();
                    var stWeight = $(this).closest('tr').find('.tdStWeight').text();
                    var uItemName = $(this).closest('tr').find('.tdUItem').text();
                    var deduction = $(this).closest('tr').find('.tdSrate1').text();
                    var srate = $(this).closest('tr').find('.tdSrate').text();
                    var itemCode = $(this).closest('tr').find('.tdItemCode').text();

                    if(hiddenFieldValue == 'it_add'){
	                    var itemCodeOption = "<option value='" + itemId + "' data-uom_item='" + uom + "' data-prate='" + pRate + "' data-grweight='" + grWeight + "' data-stqty='" + stQty + "' data-stweight='" + stWeight + "' data-uitem_des='" + uItemName + "' data-srate='" + srate + "' data-deduction='" + deduction + "' >" + itemCode + "</option>";
	                    var itemOption = "<option value='" + itemId + "'  data-uom_item='" + uom + "' data-prate='" + pRate + "' data-grweight='" + grWeight + "' data-stqty='" + stQty + "' data-stweight='" + stWeight + "'  data-uitem_des='" + uItemName + "' data-srate='" + srate + "' data-deduction='" + deduction + "' >" + itemDescription + "</option>";
	                    $('#itemid_dropdown').append(itemCodeOption);
	                    $('#item_dropdown').append(itemOption);

	                    $('#itemid_dropdown').val(itemId).trigger('change');
	                    $('#item_dropdown').val(itemId).trigger('change');

	                    //$('#txtPRate').val(parseFloat(srate).toFixed(2));

	                    $('#txtQty').focus();
                	}else{
                		var itemCodeOption = "<option value='" + itemId + "' data-uom_item='" + uom + "' data-prate='" + pRate + "' data-grweight='" + grWeight + "' data-stqty='" + stQty + "' data-stweight='" + stWeight + "' data-uitem_des='" + uItemName + "' data-srate='" + srate + "' data-deduction='" + deduction + "' >" + itemCode + "</option>";
	                    var itemOption = "<option value='" + itemId + "'  data-uom_item='" + uom + "' data-prate='" + pRate + "' data-grweight='" + grWeight + "' data-stqty='" + stQty + "' data-stweight='" + stWeight + "'  data-uitem_des='" + uItemName + "' data-srate='" + srate + "' data-deduction='" + deduction + "' >" + itemDescription + "</option>";
	                    $('#less_itemid_dropdown').append(itemCodeOption);
	                    $('#less_item_dropdown').append(itemOption);

	                    $('#less_itemid_dropdown').val(itemId).trigger('change');
	                    $('#less_item_dropdown').val(itemId).trigger('change');

	                    //$('#less_txtPRate').val(parseFloat(srate).toFixed(2));

	                    $('#less_txtQty').focus();
                	}
                });

                $("#paging").show();
                $("#tbItems").show();
                if (newPageValue == "1") {
                    paging(total);
                }
            }
            else {
                $("#tbItems > tbody").html('');
                $("#divNoRecord").show();
                $("#divNoRecord").addClass('divNoRecord');
                $("#paging").hide();
                $("#divNoRecord").html('No Record Found');
            }
        });
    }
    var getItemInfo = function(itemId,table)
    {
        $.ajax({
            type: "POST",
            url: base_url + 'index.php/item/getiteminfo',
            data: {
                item_id: itemId
            }
        }).done(function (result) {

            var jsonR = JSON.stringify($.parseJSON(result)).replace(/\\n/g, "\\n")
                                    .replace(/\\'/g, "\\'")
                                    .replace(/\\"/g, '\\"')
                                    .replace(/\\&/g, "\\&")
                                    .replace(/\\r/g, "\\r")
                                    .replace(/\\t/g, "\\t")
                                    .replace(/\\b/g, "\\b")
                                    .replace(/\\f/g, "\\f");
            var json = $.parseJSON(jsonR);

            if (json.records != false)
            {	
            	if (table == 'purchase_table') {
	                $.each(json.records, function (index, value) {
	                    var itemCodeOption = "<option value='" + value.item_id + "' data-uom_item='" + value.uom + "' data-prate='" + value.cost_price + "' data-grweight='" + value.grweight + "' data-stqty='" + value.stqty + "' data-stweight='" + value.stweight + "' data-srate='" + value.srate + "' data-deduction='" + value.deduction + "' >" + value.item_code + "</option>";
	                    var itemOption = "<option value='" + value.item_id + "'  data-uom_item='" + value.uom + "' data-prate='" + value.cost_price + "' data-grweight='" + value.grweight + "' data-stqty='" + value.stqty + "' data-stweight='" + value.stweight + "' data-srate='" + value.srate + "' data-deduction='" + value.deduction + "' >" + value.item_des + "</option>";
	                    $('#itemid_dropdown').append(itemCodeOption);
	                    $('#item_dropdown').append(itemOption);

	                    $('#itemid_dropdown').select2('val', itemId);
	                    $('#item_dropdown').select2('val', itemId);

	                    var grweight = $('#item_dropdown').find('option:selected').data('grweight');
	                    $('#txtGWeight').val(parseFloat(grweight).toFixed());
	                });
				}
				else{
					$.each(json.records, function (index, value) {
	                    var itemCodeOption = "<option value='" + value.item_id + "' data-uom_item='" + value.uom + "' data-prate='" + value.cost_price + "' data-grweight='" + value.grweight + "' data-stqty='" + value.stqty + "' data-stweight='" + value.stweight + "' data-srate='" + value.srate + "' data-deduction='" + value.deduction + "' >" + value.item_code + "</option>";
	                    var itemOption = "<option value='" + value.item_id + "'  data-uom_item='" + value.uom + "' data-prate='" + value.cost_price + "' data-grweight='" + value.grweight + "' data-stqty='" + value.stqty + "' data-stweight='" + value.stweight + "' data-srate='" + value.srate + "' data-deduction='" + value.deduction + "' >" + value.item_des + "</option>";
	                    $('#less_itemid_dropdown').append(itemCodeOption);
	                    $('#less_item_dropdown').append(itemOption);

	                    $('#less_itemid_dropdown').select2('val', itemId);
	                    $('#less_item_dropdown').select2('val', itemId);

	                    var grweight = $('#less_item_dropdown').find('option:selected').data('grweight');
	                    $('#less_txtGWeight').val(parseFloat(grweight).toFixed());
	                });
				}
            }
        });
    }

	var last_stockLocatons = function(item_id) {
		$.ajax({

			url : base_url + 'index.php/saleorder/last_stocklocatons',
			type : 'POST',
			data : { 'item_id' : item_id , 'company_id': $('#cid').val(),'etype':'sale'},
			dataType : 'JSON',
			
			success : function(data) {
				console.log(data);

				$('#laststockLocation_table tbody').html('');
				var total_qty = 0;
				if(data != 'false'){

					$.each(data, function(index, elem) {
						total_qty += parseInt(elem.qty);
						appendTo_last_stockLocatons(elem.location, elem.qty);
					});
				}
				
				last_stockLocatons_total(total_qty);

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}    

	var appendTo_last_stockLocatons = function(location, qty) {

		var row = 	"<tr>" +
						"<td class='location numeric' data-title='location' > "+ location +"</td>" +
					 	"<td class='qty numeric' data-title='qty'> "+ qty +"</td>" +
				 	"</tr>";
		
			$(row).appendTo('#laststockLocation_table tbody');
		
	}

	var last_stockLocatons_total = function(total_qty){
		var row = "<tr>" +
					"<td class='location numeric' data-title='location' >Total</td>" +
				 	"<td class='qty numeric' data-title='qty'> "+ total_qty +"</td>" +
				"</tr>";
		$(row).appendTo('#laststockLocation_table tbody');
	}

    var fetchRunningTotal = function(_pid , _to) {
		$.ajax({
			url : base_url + 'index.php/account/fetchRunningTotal',
			type : 'POST',
			data : {'to': _to, 'pid' : _pid},
			dataType : 'JSON',
			success : function(data) {
				var opbal=data[0]['RTotal'];

				$('#balance_lbl').text('Party Name,     Balance: '+parseFloat(opbal).toFixed(2));
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}


	return {

		init : function() {
			this.bindUI();
			this.bindModalPartyGrid();
			//this.bindModalItemGrid();
		},

		bindUI : function() {
			
			var self = this;

			$('#party_dropdown').focus().css('border', '1px solid #368EE0');

			$('.btn').focus(function(){
				$(this).css('border', '2px solid black');
			}
			);
			$('.btn').blur(function(){
				$(this).css('border', '');
			}
			);
			
			$('#GodownAddModel').on('shown.bs.modal',function(e){
                $('#txtNameGodownAdd').focus();
            });
            $('.btnSaveMGodown').on('click',function(e){
                if ( $('.btnSave').data('savegodownbtn')==0 ){
                    alert('Sorry! you have not save departments rights..........');
                }else{
                    e.preventDefault();
                    self.initSaveGodown();
                }
            });
            $('.btnResetMGodown').on('click',function(){
                
                $('#txtNameGodownAdd').val('');
                
            });

			$('#txtLevel3').on('change', function() {
				
				var level3 = $('#txtLevel3').val();
				$('#txtselectedLevel1').text('');
				$('#txtselectedLevel2').text('');
				if (level3 !== "" && level3 !== null) {
					// alert('enter' + $(this).find('option:selected').data('level2') );	
					$('#txtselectedLevel2').text(' ' + $(this).find('option:selected').data('level2'));
					$('#txtselectedLevel1').text(' ' + $(this).find('option:selected').data('level1'));
				}
			});
			// $('#txtLevel3').select2();
			$('.btnSaveM').on('click',function(e){
				if ( $('.btnSave').data('saveaccountbtn')==0 ){
					alert('Sorry! you have not save accounts rights..........');
				}else{
					e.preventDefault();
					self.initSaveAccount();
				}
			});
			$('.btnResetM').on('click',function(){
				
				$('#txtAccountName').val('');
				$('#txtselectedLevel2').text('');
				$('#txtselectedLevel1').text('');
				$('#txtLevel3').select2('val','');
			});
			$('#AccountAddModel').on('shown.bs.modal',function(e){
				$('#txtAccountName').focus();
			});
			shortcut.add("F3", function() {
    			$('#AccountAddModel').modal('show');
			});

			$('.btnSaveMItem').on('click',function(e){
				if ( $('.btnSave').data('saveitembtn')==0 ){
					alert('Sorry! you have not save item rights..........');
				}else{
					e.preventDefault();
					self.initSaveItem();
				}
			});
			$('.btnResetMItem').on('click',function(){
				
				$('#txtItemName').val('');
				$('#category_dropdown').select2('val','');
				$('#subcategory_dropdown').select2('val','');
				$('#brand_dropdown').select2('val','');
				$('#txtBarcode').val('');
			});

			$('#party_dropdown').on('change',function(){

				var partyId = $(this).val();
				var vrDate = $('#current_date').val();
				
				fetchRunningTotal(partyId, vrDate);
			});
			
			$('#ItemAddModel').on('shown.bs.modal',function(e){
				$('#txtItemName').focus();
			});
			shortcut.add("F7", function() {
    			$('#ItemAddModel').modal('show');
			});
			$("#switchPreBal").bootstrapSwitch('offText', 'No');
			$("#switchPreBal").bootstrapSwitch('onText', 'Yes');
			$('#voucher_type_hidden').val('new');
			$('.modal-lookup .populateAccount').on('click', function(){
				// alert('dfsfsdf');
				var party_id = $(this).closest('tr').find('input[name=hfModalPartyId]').val();
				$("#party_dropdown").select2("val", party_id); 				
			});
			$('.modal-lookup .populateItem').on('click', function(){
				// alert('dfsfsdf');
				var item_id = $(this).closest('tr').find('input[name=hfModalitemId]').val();
				$("#item_dropdown").select2("val", item_id); //set the value
				$("#itemid_dropdown").select2("val", item_id);
				$('#txtQty').focus();				
			});

			$('#voucher_type_hidden').val('new');

			$('#txtVrnoa').on('change', function() {
				fetch($(this).val());
			});

			var countItem = 0;
            $('input[id="itemDesc"]').autoComplete({

                minChars: 1,
                cache: false,
                menuClass: '',
                source: function(search, response)
                {
                    try { xhr.abort(); } catch(e){}
                    $('#txtItemId').removeClass('inputerror');
	                $("#imgItemLoader").hide();
                    if(search != "")
                    {
                        xhr = $.ajax({
                            url: base_url + 'index.php/item/searchitem',
                            type: 'POST',
                            data: {

                                search: search
                            },
                            dataType: 'JSON',
                            beforeSend: function (data) {

                                $(".loader").hide();
                                $("#imgItemLoader").show();
                                countItem = 0;
                            },
                            success: function (data) {
                                
	                            if(data == '')
	                            {
	                            	$('#itemDesc').addClass('inputerror');
	                            	clearItemData();
	                            	$('#txtItemId').val('');
									$('#txtQty').val('');
									$('#txtPRate').val('');
									$('#txtBundle').val('');
				                    $('#txtGBundle').val('');
									$('#txtWeight').val('');
									$('#txtAmount').val('');
									$('#txtGWeight').val('');
									$('#txtDiscp').val('');
									$('#txtDiscount1_tbl').val('');
	                            }
	                            else
	                            {
	                            	$('#itemDesc').removeClass('inputerror');
	                            	response(data);
	                                $("#imgItemLoader").hide();
	                            }
                            }
                        });
                    }
                    else
                    {
                        clearItemData();
                    }
                },
                renderItem: function (item, search)
                {
                    var sea = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                    var re = new RegExp("(" + sea.split(' ').join('|') + ")", "gi");

                    var selected = "";
                    if((search.toLowerCase() == (item.item_des).toLowerCase() && countItem == 0) || (search.toLowerCase() != (item.item_des).toLowerCase() && countItem == 0))
                    {
                        selected = "selected";
                    }
                    countItem++;
                    clearItemData();

                    return '<div class="autocomplete-suggestion ' + selected + '" data-val="' + search + '" data-item_id="' + item.item_id + '" data-size="' + item.pack + '" data-bid="' + item.bid +
                            '" data-uom_item="'+ item.uom + '" data-srate="' + item.srate + '" data-grweight="' + item.grweight + '" data-stqty="' + item.stqty +
                            '" data-stweight="' + item.stweight + '" data-length="' + item.length  + '" data-catid="' + item.catid +
                            '" data-subcatid="' + item.subcatid + '" data-desc="' + item.item_des + '" data-code="' + item.item_code + '" data-self="' + item.self_freight_rate + '" data-bilty="' + item.bilty_freight_rate + 
                            '" data-gari="' + item.gari_freight_rate + '">' + item.item_des.replace(re, "<b>$1</b>") + '</div>';
                },
                onSelect: function(e, term, item)
                {
                	
            		$("#imgItemLoader").hide();
                    $("#hfItemId").val(item.data('item_id'));
                    $("#hfItemSize").val(item.data('size'));
                    $("#hfItemBid").val(item.data('bid'));
                    $("#hfItemUom").val(item.data('uom_item'));
                    $("#hfItemPrate").val(item.data('srate'));
                    $("#hfItemGrWeight").val(item.data('grweight'));
                    $("#hfItemStQty").val(item.data('stqty'));
                    $("#hfItemStWeight").val(item.data('stweight'));
                    $("#hfItemLength").val(item.data('length'));
                    $("#hfItemCatId").val(item.data('catid'));
                    $("#hfItemSubCatId").val(item.data('subcatid'));
                    $("#hfItemDesc").val(item.data('desc'));
                    $("#hfItemShortCode").val(item.data('code'));

                    var freightStatus = $('input[name=inlineRadioOptions]:checked').val();
                   
                    if(freightStatus == 'self'){
                    	$('#txtFRate').val(item.data('self'));	
                    }else if(freightStatus == 'bilty'){
                    	$('#txtFRate').val(item.data('bilty'));
                    }else if(freightStatus == 'gari'){
                    	$('#txtFRate').val(item.data('gari'));
                   	}
                   
                    $("#txtItemId").val(item.data('code'));

                    var itemId = item.data('item_id');
                    var itemDesc = item.data('desc');
                    var srate = item.data('srate');
                    var grWeight = item.data('grweight');
                    var uomItem = item.data('uom_item');
                    var stQty = item.data('stqty');
                    var stWeight = item.data('stweight');
                    var size = item.data('size');
                    var brandId = item.data('bid');

                    $('#stqty_lbl').text('Item,     Uom:' + uomItem);
                    $('#itemDesc').val(itemDesc);
                    $('#txtPRate').val(parseFloat(srate).toFixed(2));
                    $('#txtGWeight').val(parseFloat(grWeight).toFixed(2));
                    $('#txtUom').val(uomItem);

                    $('#txtGWeight').trigger('input');	
            	}
            });
	
			$('#txtItemId').on('change', function(){
				var item_id = $('#hfItemId').val();
				last_stockLocatons(item_id);
			});

			$('.btnSave').on('click',  function(e) {
				
				if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('updatebtn')==0 ){
					alert('Sorry! you have not update rights..........');
				}else if($('#voucher_type_hidden').val()=='new' && $('.btnSave').data('insertbtn')==0){
					alert('Sorry! you have not insert rights..........');
				}else{
					e.preventDefault();
					self.initSave();
				}
			});

			$('input[name=inlineRadioOptions]').on('click',function(){
				$('#inlineRadio1').attr('disabled', true);
				$('#inlineRadio2').attr('disabled', true);
				$('#inlineRadio3').attr('disabled', true);
			});

			$('.btnPrint').on('click',  function(e) {
				e.preventDefault();
				Print_Voucher(1,'lg','');
			});
			$('.btnprints').on('click', function(e){
				e.preventDefault();
				Print_Vouchers('','','');
			});
			$('.btnprint_sm').on('click', function(e){
				e.preventDefault();
				Print_Voucher(1,'sm','');
			});
			$('.btnprint_sm_withOutHeader').on('click', function(e) {
				e.preventDefault();
				Print_Voucher(0,'sm','');
			});
			$('.btnprint_sm_rate').on('click', function(e){
				e.preventDefault();
				Print_Voucher(1,'sm','wrate');
			});
			$('.btnprint_sm_withOutHeader_rate').on('click', function(e) {
				e.preventDefault();
				Print_Voucher(0,'sm','wrate');
			});

			$('.btnReset').on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			$('.btnDelete').on('click', function(e){
				if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('deletebtn')==0 ){
					alert('Sorry! you have not delete rights..........');
				}else{

					// alert($('#voucher_type_hidden').val() +' - '+ $('.btnSave').data('deletebtn') );
					e.preventDefault();
					var vrnoa = $('#txtVrnoaHidden').val();
					if (vrnoa !== '') {
						if (confirm('Are you sure to delete this voucher?'))
							deleteVoucher(vrnoa);
					}
				}

			});

			// $('#txtOrderNo').on('keypress', function(e) {
			
			// 	if (e.keyCode === 13) {
			// 		if ($(this).val() != '') {
			// 			fetchThroughPO($(this).val());
			// 		}
			// 	}
			// });

			/////////////////////////////////////////////////////////////////
			/// setting calculations for the single product
			/////////////////////////////////////////////////////////////////

			$('#txtWeight').on('input', function() {
				/*// var _gw = getNumVal($('#txtGWeight'));
				// if (_gw!=0) {
				// var w = parseInt(parseFloat($(this).val())/parseFloat(_gw));
				// $('#txtQty').val(w);	
				// }
				calculateUpperSum();*/

				var _qty = getNumVal($('#txtQty'));
				var _amnt = getNumVal($('#txtAmount'));
				var _net = getNumVal($('#txtNet'));
				var _prate = getNumVal($('#txtPRate'));
				var _gw = getNumVal($('#txtGWeight'));
				var _weight= getNumVal($('#txtWeight'));
				var _uom=$('#txtUom').val().toUpperCase();
				var fRate = getNumVal($('#txtFRate'));

				// alert('uom_item ' + _uom);
				kg=-1;
				gram=-1;
				var kg = _uom.search("KG");
				var gram = _uom.search("GRAM");

				if (kg ==-1 && gram ==-1 ){

					var _tempAmnt = parseFloat(_qty) * parseFloat(_prate);		
					var _tempFAmnt = parseFloat(_qty) * parseFloat(fRate);	
				}else{

					var _tempAmnt = parseFloat(_weight) * parseFloat(_prate);			
					var _tempFAmnt = parseFloat(_weight) * parseFloat(fRate);	
				}
				
				//$('#txtWeight').val(parseFloat(_gw) * parseFloat(_qty));
				$('#txtAmount').val(_tempAmnt);
				$('#txtFAmount').val(_tempFAmnt);
				
			});
			$('#txtFRate').on('input', function() {
				// var _gw = getNumVal($('#txtGWeight'));
				// if (_gw!=0) {
				// var w = parseInt(parseFloat($(this).val())/parseFloat(_gw));
				// $('#txtQty').val(w);	
				// }
				calculateUpperSum();
				
			});

			$('#itemid_dropdown').on('change', function() {
				var item_id = $(this).val();
				var prate = $(this).find('option:selected').data('srate');
				var grweight = $(this).find('option:selected').data('grweight');
				var uom_item = $(this).find('option:selected').data('uom_item');
				var stqty = $(this).find('option:selected').data('stqty');
				var stweight = $(this).find('option:selected').data('stweight');
				$('#stqty_lbl').text('Item,     Qty:' + stqty + ', Weight ' + stweight);
				$('#txtPRate').val(parseFloat(prate).toFixed(2));
				$('#item_dropdown').select2('val', item_id);
				$('#txtGWeight').val(parseFloat(grweight).toFixed());
				$('#txtUom').val(uom_item);

				// calculateUpperSum();
				// $('#txtQty').focus();
			});
			$('#item_dropdown').on('change', function() {
				var item_id = $(this).val();
				var prate = $(this).find('option:selected').data('srate');
				var grweight = $(this).find('option:selected').data('grweight');
				var uom_item = $(this).find('option:selected').data('uom_item');
				// $('#txtQty').val('1');
				var stqty = $(this).find('option:selected').data('stqty');
				var stweight = $(this).find('option:selected').data('stweight');
				$('#stqty_lbl').text('Item,    Qty:' + stqty + ', Weight ' + stweight);
				$('#txtPRate').val(parseFloat(prate).toFixed(2));
				$('#itemid_dropdown').select2('val', item_id);
				$('#txtGWeight').val(parseFloat(grweight).toFixed(2));
				$('#txtUom').val(uom_item);
				// calculateUpperSum();
				// $('#txtQty').focus();
			});
			$('#txtQty').on('input', function() {
				calculateUpperSum();
			});
			$('#txtPRate').on('input', function() {
				calculateUpperSum();
			});

			// '''''''less single product

			$('#less_txtWeight').on('input', function() {
				// var _gw = getNumVal($('#txtGWeight'));
				// if (_gw!=0) {
				// var w = parseInt(parseFloat($(this).val())/parseFloat(_gw));
				// $('#txtQty').val(w);	
				// }
				calculateUpperSum_Less();
				
			});

			$('#less_itemid_dropdown').on('change', function() {
				var item_id = $(this).val();
				var prate = $(this).find('option:selected').data('deduction');
				var grweight = $(this).find('option:selected').data('grweight');
				var uom_item = $(this).find('option:selected').data('uom_item');
				// $('#less_txtQty').val('1');
				$('#less_txtPRate').val(parseFloat(prate).toFixed(2));
				$('#less_item_dropdown').select2('val', item_id);
				$('#less_txtGWeight').val(parseFloat(grweight).toFixed());
				$('#less_txtUom').val(uom_item);

				// calculateUpperSum_Less();
				// $('#less_txtQty').focus();
			});
			$('#less_item_dropdown').on('change', function() {
				var item_id = $(this).val();
				var prate = $(this).find('option:selected').data('deduction');
				var grweight = $(this).find('option:selected').data('grweight');
				var uom_item = $(this).find('option:selected').data('uom_item');
				// $('#less_txtQty').val('1');
				$('#less_txtPRate').val(parseFloat(prate).toFixed(2));
				$('#less_itemid_dropdown').select2('val', item_id);
				$('#less_txtGWeight').val(parseFloat(grweight).toFixed(2));
				$('#less_txtUom').val(uom_item);
				// calculateUpperSum_Less();
				// $('#less_txtQty').focus();
			});
			$('#less_txtQty').on('input', function() {
				calculateUpperSum_Less();
			});
			$('#less_txtPRate').on('input', function() {
				calculateUpperSum_Less();
			});
			// ''''''''''''''end


			$('#btnAdd').on('click', function(e) {
				e.preventDefault();

				var error = validateSingleProductAdd();
				if (!error) {

					var item_desc = $.trim($("#hfItemDesc").val());
					var item_id = $.trim($("#hfItemId").val());
					var itemCode = $.trim($("#txtItemId").val());
					//var item_desc = $('#item_dropdown').find('option:selected').text();
					//var item_id = $('#item_dropdown').val();
					var qty = $('#txtQty').val();
					var rate = $('#txtPRate').val();
					var weight = $('#txtWeight').val();
					var amount = $('#txtAmount').val();
					var fRate = $('#txtFRate').val();
					var fAmount = $('#txtFAmount').val();

					// reset the values of the annoying fields
					//$('#itemid_dropdown').select2('val', '');
					//$('#item_dropdown').select2('val', '');
					$('#txtItemId').val('');
	                $('#itemDesc').val('');
	                $('#txtUom').val('');
					$('#txtQty').val('');
					$('#txtPRate').val('');
					$('#txtWeight').val('');
					$('#txtAmount').val('');
					$('#txtGWeight').val('');
					$('#txtFRate').val('');
					$('#txtFAmount').val('');

					appendToTable('', item_desc, item_id, qty, rate, amount, weight,"add", fRate, fAmount, itemCode);
					// calculateLowerTotal(qty, amount, weight,0);
					$('#stqty_lbl').text('Item');

					//$('#item_dropdown').focus();
					clearItemData();
					$('#itemDesc').focus();
	                
				} else {
					alert('Correct the errors!');
				}

			});
			$('#less_btnAdd').on('click', function(e) {
				e.preventDefault();

				var error = validateSingleProductAdd_less();
				if (!error) {

					var item_desc = $('#less_item_dropdown').find('option:selected').text();
					var item_id = $('#less_item_dropdown').val();
					var qty = $('#less_txtQty').val();
					var rate = $('#less_txtPRate').val();
					var weight = $('#less_txtWeight').val();
					var amount = $('#less_txtAmount').val();

					// reset the values of the annoying fields
					$('#less_itemid_dropdown').select2('val', '');
					$('#less_item_dropdown').select2('val', '');
					$('#less_txtQty').val('');
					$('#less_txtPRate').val('');
					$('#less_txtWeight').val('');
					$('#less_txtAmount').val('');
					$('#less_txtGWeight').val('');

					appendToTable('', item_desc, item_id, qty, rate, amount, weight,"less");
					// calculateLowerTotal(0, 0, 0,amount);
					$('#less_item_dropdown').focus();
					
				} else {
					alert('Correct the errors!');
				}


			});

			// when btnRowRemove is clicked
			$('#purchase_table').on('click', '.btnRowRemove', function(e) {
				e.preventDefault();
				var qty = $.trim($(this).closest('tr').find('td.qty').text());
				var amount = $.trim($(this).closest('tr').find('td.amount').text());
				var weight = $.trim($(this).closest('tr').find('td.weight').text());

				var famount = $.trim($(this).closest('tr').find('td.famount').text());
				

				calculateLowerTotal("-"+qty, "-"+amount, '-'+weight,0,'-'+famount);
				$(this).closest('tr').remove();
			});
			$('#purchase_table').on('click', '.btnRowEdit', function(e) {
				e.preventDefault();

				// getting values of the cruel row
				var item_id = $.trim($(this).closest('tr').find('td.item_desc').data('item_id'));
				var qty = $.trim($(this).closest('tr').find('td.qty').text());
				var weight = $.trim($(this).closest('tr').find('td.weight').text());
				var rate = $.trim($(this).closest('tr').find('td.rate').text());
				var fRate = $.trim($(this).closest('tr').find('td.frate').text());
				var fAmount = $.trim($(this).closest('tr').find('td.famount').text());
				var amount = $.trim($(this).closest('tr').find('td.amount').text());
				var famount = $.trim($(this).closest('tr').find('td.famount').text());
				//getItemInfo(item_id,'purchase_table');

				// $('#itemid_dropdown').select2('val', item_id);
				// $('#item_dropdown').select2('val', item_id);

				// var grweight = $('#item_dropdown').find('option:selected').data('grweight');

				// $('#txtGWeight').val(parseFloat(grweight).toFixed());
				
				$.ajax({
                    type: "POST",
                    url: base_url + 'index.php/item/getiteminfobyid',
                    data: {
                        item_id: item_id
                    }
                }).done(function (result) {

                    $("#imgItemLoader").hide();
                    var item = $.parseJSON(result);

                    if (item != false)
                    {
                        $("#hfItemId").val(item[0].item_id);
                        $("#hfItemSize").val(item[0].size);
                        $("#hfItemBid").val(item[0].bid);
                        $("#hfItemUom").val(item[0].uom);
                        $("#hfItemPrate").val(item[0].cost_price);
                        $("#hfItemGrWeight").val(item[0].grweight);
                        $("#hfItemStQty").val(item[0].stqty);
                        $("#hfItemStWeight").val(item[0].stweight);
                        $("#hfItemLength").val(item[0].length);
                        $("#hfItemCatId").val(item[0].catid);
                        $("#hfItemSubCatId").val(item[0].subcatid);
                        $("#hfItemDesc").val(item[0].item_des);
                        $("#hfItemShortCode").val(item[0].short_code);

                        $("#txtItemId").val(item[0].item_code);
                        $('#itemDesc').val(item[0].item_des);
                        $('#txtUom').val(item[0].uom);
                        $('#txtGWeight').val(item[0].grweight);

                        $('#txtQty').val(qty);
						$('#txtPRate').val(rate);
						$('#txtWeight').val(weight);
						$('#txtFRate').val(fRate);
						$('#txtFAmount').val(fAmount);
						$('#txtAmount').val(amount);

						$('#stqty_lbl').text('Item,     Uom:' + item[0].uom);

                        
                        calculateLowerTotal("-"+qty, "-"+amount, '-'+weight,0,'-'+famount);
                        // now we have get all the value of the row that is being deleted. so remove that cruel row

                    }
                });
				
				// now we have get all the value of the row that is being deleted. so remove that cruel row
				$(this).closest('tr').remove();	// yahoo removed
			});

			$('#txtItemId').mask("99-99-999");
			$('#txtItemId').on('keypress', function(e) {
				if (e.keyCode === 13) {
					e.preventDefault();
					$.ajax({
                    	url: base_url + 'index.php/item/searchitem',
                    	type: 'POST',
                    	data: {

                        	search: $(this).val()
                    	}
	                }).done(function (result) {

	                    $("#imgItemLoader").hide();
	                    var item = $.parseJSON(result);

	                    if (item != false)
	                    {
	                    	$('#txtItemId').removeClass('inputerror');
	                        $("#hfItemId").val(item[0].item_id);
	                        $("#hfItemSize").val(item[0].size);
	                        $("#hfItemBid").val(item[0].bid);
	                        $("#hfItemUom").val(item[0].uom);
	                        $("#hfItemPrate").val(item[0].cost_price);
	                        $("#hfItemGrWeight").val(item[0].grweight);
	                        $("#hfItemStQty").val(item[0].stqty);
	                        $("#hfItemStWeight").val(item[0].stweight);
	                        $("#hfItemLength").val(item[0].length);
	                        $("#hfItemCatId").val(item[0].catid);
	                        $("#hfItemSubCatId").val(item[0].subcatid);
	                        $("#hfItemDesc").val(item[0].item_des);
	                        $("#hfItemShortCode").val(item[0].short_code);

	                        $("#txtItemId").val(item[0].item_code);
	                        $('#itemDesc').val(item[0].item_des);
	                        $('#txtUom').val(item[0].uom);
	                        $('#txtGWeight').val(item[0].grweight);
							$('#stqty_lbl').text('Item,     Uom:' + item[0].uom);
							$('#txtItemId').trigger('change');
	                    }else{
	                    	$('#txtItemId').addClass('inputerror');
	                        $("#hfItemSize").val('');
	                        $("#hfItemBid").val('');
	                        $("#hfItemUom").val('');
	                        $("#hfItemPrate").val('');
	                        $("#hfItemGrWeight").val('');
	                        $("#hfItemStQty").val('');
	                        $("#hfItemStWeight").val('');
	                        $("#hfItemLength").val('');
	                        $("#hfItemCatId").val('');
	                        $("#hfItemSubCatId").val('');
	                        $("#hfItemDesc").val('');
	                        $("#hfItemShortCode").val('');

	                        $('#itemDesc').val('');
	                        $('#txtUom').val('');
	                        $('#txtGWeight').val('');
							$('#stqty_lbl').text('Item');
	                    }
	                });
				}
			});
			
			$('#purchase_table_less').on('click', '.btnRowRemove', function(e) {
				e.preventDefault();
				var qty = $.trim($(this).closest('tr').find('td.qty').text());
				var amount = $.trim($(this).closest('tr').find('td.amount').text());
				var weight = $.trim($(this).closest('tr').find('td.weight').text());
				calculateLowerTotal("-"+qty, "-"+amount, '-'+weight,0,0);
				$(this).closest('tr').remove();
			});

			$('#purchase_table_less').on('click', '.btnRowEdit', function(e) {
				// alert('end table');
				e.preventDefault();

				// getting values of the cruel row
				var item_id = $.trim($(this).closest('tr').find('td.item_desc').data('item_id'));
				var qty = $.trim($(this).closest('tr').find('td.qty').text());
				var weight = $.trim($(this).closest('tr').find('td.weight').text());
				var rate = $.trim($(this).closest('tr').find('td.rate').text());
				var amount = $.trim($(this).closest('tr').find('td.amount').text());
				getItemInfo(item_id,'purchase_table_less');

				// $('#less_itemid_dropdown').select2('val', item_id);
				// $('#less_item_dropdown').select2('val', item_id);

				// var grweight = $('#less_item_dropdown').find('option:selected').data('grweight');

				// $('#less_txtGWeight').val(parseFloat(grweight).toFixed());
				$('#less_txtQty').val(qty);
				$('#less_txtPRate').val(rate);
				$('#less_txtWeight').val(weight);
				$('#less_txtAmount').val(amount);

                var _amountless = getNumVal($('#txtPaid'));
                var tempAmountLess = parseFloat(_amountless) - parseFloat(amount);
                $('#txtPaid').val(tempAmountLess);

				//calculateLowerTotal(0, 0, 0,amount);
				// now we have get all the value of the row that is being deleted. so remove that cruel row
				$(this).closest('tr').remove();	// yahoo removed

			});


			$('#txtDiscount').on('input', function() {
				var _disc= $('#txtDiscount').val();
				var _totalAmount= $('#txtTotalAmount').val();
				var _discamount=0;
				if (_disc!=0 && _totalAmount!=0){
					_discamount=_totalAmount*_disc/100;
				}
				$('#txtDiscAmount').val(_discamount);
				calculateLowerTotal(0, 0, 0,0,0);
			});

			$('#txtDiscAmount').on('input', function() {
				var _discamount= $('#txtDiscAmount').val();
				var _totalAmount= $('#txtTotalAmount').val();
				var _discp=0;
				if (_discamount!=0 && _totalAmount!=0){
					_discp=_discamount*100/_totalAmount;
				}
				$('#txtDiscount').val(parseFloat(_discp).toFixed(2));
				calculateLowerTotal(0, 0, 0,0,0);
			});

			$('#txtExpense').on('input', function() {
				var _exppercent= $('#txtExpense').val();
				var _totalAmount= $('#txtTotalAmount').val();
				var _expamount=0;
				if (_exppercent!=0 && _totalAmount!=0){
					_expamount=_totalAmount*_exppercent/100;
				}
				$('#txtExpAmount').val(_expamount);
				calculateLowerTotal(0, 0, 0,0,0);
			});

			$('#txtExpAmount').on('input', function() {
				var _expamount= $('#txtExpAmount').val();
				var _totalAmount= $('#txtTotalAmount').val();
				var _exppercent=0;
				if (_expamount!=0 && _totalAmount!=0){
					_exppercent=_expamount*100/_totalAmount;
				}
				$('#txtExpense').val(parseFloat(_exppercent).toFixed(2));
				calculateLowerTotal(0, 0, 0,0,0);
			});

			$('#txtTax').on('input', function() {
				var _taxpercent= $('#txtTax').val();
				var _totalAmount= $('#txtTotalAmount').val();
				var _taxamount=0;
				if (_taxpercent!=0 && _totalAmount!=0){
					_taxamount=_totalAmount*_taxpercent/100;
				}
				$('#txtTaxAmount').val(_taxamount);
				calculateLowerTotal(0, 0, 0,0,0);
			});

			$('#txtTaxAmount').on('input', function() {
				var _taxamount= $('#txtTaxAmount').val();
				var _totalAmount= $('#txtTotalAmount').val();
				var _taxpercent=0;
				if (_taxamount!=0 && _totalAmount!=0){
					_taxpercent=_taxamount*100/_totalAmount;
				}
				$('#txtTax').val(parseFloat(_taxpercent).toFixed(2));
				calculateLowerTotal(0, 0, 0,0,0);
			});
			
			$('#txtQty').change(function(){
				var Qty = $(this).val();
				var GrWeight = $('#txtGWeight').val();
				var totalWeight = Qty * GrWeight;
				$('#txtWeight').val(totalWeight);
			});




			// alert('load');

			shortcut.add("F10", function() {
    			$('.btnSave').trigger('click');
			});
            $(".btnSave1").on('click', function() {
                $('.btnSave').trigger('click');
            });
			shortcut.add("F1", function() {
				$('a[href="#party-lookup"]').trigger('click');
			});
			shortcut.add("F2", function() {
				$('a[href="#item-lookup"]').trigger('click');
			});
			shortcut.add("F9", function() {
				Print_Voucher(1,'lg','');
			});
			shortcut.add("F6", function() {
    			$('#txtVrnoa').focus();
    			// alert('focus');
			});
			shortcut.add("F5", function() {
    			self.resetVoucher();
			});

			shortcut.add("F12", function() {
    			$('.btnDelete').trigger('click');
			});


			$('#txtVrnoa').on('keypress', function(e) {
				if (e.keyCode === 13) {
					e.preventDefault();
					var vrnoa = $('#txtVrnoa').val();
					if (vrnoa !== '') {
						fetch(vrnoa);
					}
				}
			});

			$('#txtQty,#txtWeight,#txtFRate,#txtPRate').on('keypress', function(e) {
				if (e.keyCode === 13) {
					e.preventDefault();
					$('#btnAdd').trigger('click');
				}
			});
			$('#less_txtQty,#less_txtWeight,#less_txtPRate').on('keypress', function(e) {
				if (e.keyCode === 13) {
					e.preventDefault();
					$('#less_btnAdd').trigger('click');
				}
			});


			$('.btnprintHeader').on('click', function(e) {
				e.preventDefault();
				Print_Voucher(1,'lg','');

			});
			$('.btnprintwithOutHeader').on('click', function(e) {
				e.preventDefault();
				Print_Voucher(0,'lg','');
			});
			$('.form-control').keypress(function (e) {
			  
			  if (e.which == 13) {
			    e.preventDefault();
			  }
			});

			$('.btnsearchitem').on('click', function (e) {
			  var values = $(this).prop('id');
			  $('#hiddenFieldValue').val(values); 
			});
			onChange(1);
            $("input[id$='txtItemSearch']").keydown(function(event){
                var keyCode = event.keyCode;
                if(keyCode == 13)
                {
                    onChange(1);
                    event.preventDefault();
                    return false;
                }
                else if(keyCode == 8 || keyCode == 46)
                {
                    var txt = $("input[id$='txtItemSearch']").val();
                    if(txt.length <= 1)
                    {
                        $("input[id$='txtItemSearch']").val('');
                        onChange(1);
                    }
                }
            });
			getMaxVrno();
			getMaxVrnoa();
		},

		// prepares the data to save it into the database
		initSave : function() {

			var saveObj = getSaveObject();
			var error = validateSave();

			if (!error) {
				var rowsCount = $('#purchase_table').find('tbody tr').length;
				if (rowsCount > 0 ) {
					save(saveObj);
				} else {
					alert('No date found to save!');
				}
			} else {
				alert('Correct the errors...');
			}
		},
		initSaveAccount : function() {

			var saveObjAccount = getSaveObjectAccount();
			var error = validateSaveAccount();

			if (!error) {
					saveAccount(saveObjAccount);
			} else {
				alert('Correct the errors...');
			}
		},
		initSaveItem : function() {

			var saveObjItem = getSaveObjectItem();
			var error = validateSaveItem();

			if (!error) {
					saveItem(saveObjItem);
			} else {
				alert('Correct the errors...');
			}
		},
		initSaveGodown : function() {

            var saveObjGodown = getSaveObjectGodown();
            var error = validateSaveGodown();

            if (!error) {
                    saveGodown(saveObjGodown);
            } else {
                alert('Correct the errors...');
            }
        },

				bindModalPartyGrid : function() {

			
				            var dontSort = [];
				            $('#party-lookup table thead th').each(function () {
				                if ($(this).hasClass('no_sort')) {
				                    dontSort.push({ "bSortable": false });
				                } else {
				                    dontSort.push(null);
				                }
				            });
				            saleorder.pdTable = $('#party-lookup table').dataTable({
				                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
				                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
				                "aaSorting": [[0, "asc"]],
				                "bPaginate": true,
				                "sPaginationType": "full_numbers",
				                "bJQueryUI": false,
				                "aoColumns": dontSort

				            });
				            $.extend($.fn.dataTableExt.oStdClasses, {
				                "s`": "dataTables_wrapper form-inline"
				            });
},

bindModalItemGrid : function() {

			
				            var dontSort = [];
				            $('#item-lookup table thead th').each(function () {
				                if ($(this).hasClass('no_sort')) {
				                    dontSort.push({ "bSortable": false });
				                } else {
				                    dontSort.push(null);
				                }
				            });
				            saleorder.pdTable = $('#item-lookup table').dataTable({
				                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
				                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
				                "aaSorting": [[0, "asc"]],
				                "bPaginate": true,
				                "sPaginationType": "full_numbers",
				                "bJQueryUI": false,
				                "aoColumns": dontSort

				            });
				            $.extend($.fn.dataTableExt.oStdClasses, {
				                "s`": "dataTables_wrapper form-inline"
				            });
},

		// instead of reseting values reload the page because its cruel to write to much code to simply do that
		resetVoucher : function() {
			 general.reloadWindow();
		}
	}

};

var saleorder = new saleorder();
saleorder.init();