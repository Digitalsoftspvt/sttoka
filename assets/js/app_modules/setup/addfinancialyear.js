var Transporter = function() {

	// saves the data into the database
	var save = function( transporter ) {
		general.disableSave();

		$.ajax({
			url : base_url + 'index.php/finanicalyear/save',
			type : 'POST',
			data : { 'transporter' : transporter,'start_date':$('#txtContact').val(),'end_date':$('#txtPhone').val() },
			dataType : 'JSON',
			success : function(data) {
				console.log(data);
				if (data.error === 'false') {
					alert('An internal error occured while saving voucher. Please try again.');
				}else {
					alert('Financial Year saved successfully.');
					general.reloadWindow();
				}
				general.enableSave();
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var fetch = function(transporter_id) {

		$.ajax({
			url : base_url + 'index.php/finanicalyear/fetch',
			type : 'POST',
			data : { 'transporter_id' : transporter_id },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					populateData(data);
				}
			}, erro
			 : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var fetchAll = function(sdate,edate) {
		var errorFlag=false;
		$.ajax({
			url : base_url + 'index.php/finanicalyear/fetchAll',
			type : 'POST',
			data : { 'sdate' : sdate },
			dataType : 'JSON',
			async: false,
        	cache: false,
        	timeout :30000,
			success : function(data) {

				if (data === 'false') {
					// alert('No data found');
				} else {

					$.each(data, function(index, elem) {
						var A=elem.start_date.substr(0,10);
						var B=elem.end_date.substr(0,10);
						var C= sdate.replace("/","-");
						C=C.replace("/","-");
						var D= edate.replace("/","-");
						D=D.replace("/","-");
						 var isOverlapping =  ((A == null || D == null || A <= D) 
									            && (C == null || B == null || C <= B)
            									&& (A == null || B == null || A <= B)
            									&& (C == null || D == null || C <= D));
						 if(isOverlapping==true)
						 {
						 	errorFlag=true;						 
						 }

					});
				}
			}, error: function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
		return errorFlag;
	}
	var deleteVoucher = function(transporter_id) {
		general.disableSave();

		$.ajax({
			url : base_url + 'index.php/finanicalyear/delete',
			type : 'POST',
			data : {'transporter_id': transporter_id},
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					alert('Transporter deleted successfully.');
					general.reloadWindow();
				}
				general.enableSave();
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// generates the view
	var populateData = function(elem) {

		$('#vouchertypehidden').val('edit');
		$('#txtId').val(elem.fn_id);
		$('#txtIdHidden').val(elem.fn_id);
		$('#txtName').val(elem.fname);
		$('#txtContact').datepicker('update',elem.start_date.substr(0,10));
		$('#txtPhone').datepicker('update',elem.end_date.substr(0,10));;
		// $('#party_dropdown11').select2('val',elem.phone);
		$('#txtAreaCover').val(elem.remarks);
	}

	// gets the maxid of the voucher
	var getMaxId = function() {

		$.ajax({
			url : base_url + 'index.php/finanicalyear/getMaxId',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$('#txtId').val(data);
				$('#txtIdHidden').val(data);
				$('#txtMaxIdHidden').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;

		var name = $.trim($('#txtName').val());
		// var party=$('#party_dropdown11')
		// remove the error class first
		$('#txtName').removeClass('inputerror');
		// if(!party.val()){
		// 	party.addClass('inputerror');
		// 	errorFlag = true;

		// }
		if ( name === '' ) {
			$('#txtName').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	// returns the fee category object to save into database
	var getSaveObject = function() {

		var obj = {};
		obj.fn_id = $.trim($('#txtIdHidden').val());
		obj.fname = $.trim($('#txtName').val());
		obj.start_date = $.trim($('#txtContact').val());
		obj.end_date = $.trim($('#txtPhone').val());
		obj.remarks = $.trim($('#txtAreaCover').val());
		// obj.tpid = $.trim($('#party_dropdown11').val());

		return obj;
	}

	return {

		init : function() {
			$('#vouchertypehidden').val('new');
			this.bindUI();
		},

		bindUI : function() {
			// general.setupfetchhelp($('.page_title').text());
			// $('[data-toggle="tooltip"]').tooltip();
			// $("div").children("input[type='text']").each(function(){
			// var obj = {};

			// 			obj.inputid =$(this).attr("id");
			// 			obj.vouchername =$('.page_title').text();
			// 			obj.fieldname=$(this).siblings('.input-group-addon').text();
			// 		general.savehelp(obj);
			// 					});
			// $("div label").children("input[type='radio']").each(function(){
			// var obj = {};

			// 			obj.inputid =$(this).attr("id");
			// 			obj.vouchername =$('.page_title').text();
			// 			obj.fieldname=$(this).siblings('.input-group-addon').text();


			// 	general.savehelp(obj);

			// });
			// $("div").children("select").each(function(){
			// var obj = {};

			// 			obj.inputid =$(this).attr("id");
			// 			obj.vouchername =$('.page_title').text();
			// 			obj.fieldname=$(this).siblings('.input-group-addon').text();


			// 	general.savehelp(obj);

			// });
			// $("div").children("input[type='checkbox']").each(function(){
			// var obj = {};

			// 			obj.inputid =$(this).attr("id");
			// 			obj.vouchername =$('.page_title').text();
			// 			obj.fieldname=$(this).siblings('.input-group-addon').text();

			
			// 	general.savehelp(obj);
			
			// });
			// $("div").children("input[type='number']").each(function(){
			// var obj = {};

			// 			obj.inputid =$(this).attr("id");
			// 			obj.vouchername =$('.page_title').text();
			// 			obj.fieldname=$(this).siblings('.input-group-addon').text();
						
			// 	general.savehelp(obj);
					
			// });
			var self = this;

			shortcut.add("F10", function() {
    			$('.btnSave').trigger('click');
			});
			shortcut.add("F6", function() {
    			$('#txtId').focus();
			});
			shortcut.add("F5", function() {
    			self.resetVoucher();
			});

			$('#txtId').on('change', function() {
				fetch($(this).val());
			});

			// when save button is clicked
			$('.btnSave').on('click', function(e) {
				if ($('#vouchertypehidden').val()=='edit' && $('.btnSave').data('updatebtn')==0 ){
					alert('Sorry! you have not update rights..........');
				}else if($('#vouchertypehidden').val()=='new' && $('.btnSave').data('insertbtn')==0){
					alert('Sorry! you have not insert rights..........');
				}else{
					e.preventDefault();
					self.initSave();
				}
			});

			// when the reset button is clicked
			$('.btnReset').on('click', function(e) {
				e.preventDefault();		// prevent the default behaviour of the link
				self.resetVoucher();	// resets the voucher
			});

			// when text is chenged inside the id textbox
			$('#txtId').on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {

					// get the based on the id entered by the user
					if ( $('#txtId').val().trim() !== "" ) {

						var transporter_id = $.trim($('#txtId').val());
						fetch(transporter_id);
					}
				}
			});

			// when edit button is clicked inside the table view
			$('.btn-edit-dept').on('click', function(e) {
				e.preventDefault();

				fetch($(this).data('transporter_id'));		// get the class detail by id
				$('a[href="#add_transporter"]').trigger('click');
			});

			getMaxId();		// gets the max id of voucher
		},

		// makes the voucher ready to save
		initSave : function() {

			var saveObj = getSaveObject();	// returns the class detail object to save into database
			var error = validateSave();
			var sdate=$('#txtContact').val()
			var edate=$('#txtPhone').val()
			var error1=fetchAll(sdate,edate);
			if(!error1)
			{
			if ( !error ) {
				save( saveObj );
			} else {
				alert('Correct the errors...');
			}
		}
		else{
				alert('Financial Date Already Used!!');
			}
		},

		// resets the voucher
		resetVoucher : function() {

			$('.inputerror').removeClass('inputerror');
			$('#txtId').val('');
			$('#txtName').val('');
			$('#txtContact').datepicker('update',new Date());
			$('#txtPhone').datepicker('update',new Date());
			$('#txtAreaCover').val('');
			// $('#party_dropdown11').select2('val','');


			getMaxId();		// gets the max id of voucher
		}
	};
};

var transporter = new Transporter();
transporter.init();