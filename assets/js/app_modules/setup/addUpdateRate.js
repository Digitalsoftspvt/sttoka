var UpdateRate = function() {


	var getSavePartyRateObj = function () {

		var partyRate = [];
		
		$('#updateRateTbl').find('tbody tr').each(function(index, elem) {
			var sd = {};

			sd.party_id = $.trim($('#partyDropdown').val());
			sd.item_id =  $.trim($(elem).find('td.item_desc').data('item_id'));
			sd.rate =  $.trim($(elem).find('td.rate').text());
			partyRate.push(sd);
		});

		return partyRate;
	}

	// saves the data into the database
	var save = function( rateObj ) {
		
		$.ajax({
			url : base_url + '/index.php/updateRate/save',
			type : 'POST',
			data : { 'party_rate' : rateObj},
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'false') {

					alert('An internal error occured while saving branch. Please try again.');
				} else {

					alert('Rate saved successfully.');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		var name = $.trim($('#partyDropdown').val());
		
		$('#partyDropdown').removeClass('inputerror');

		if ( name === '' ) {
			$('#partyDropdown').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var fetchPartyWiseRate = function(pid) {

		$.ajax({
			url : base_url + 'index.php/updateRate/fetchPartyWiseRate',
			type : 'POST',
			data : { 'party_id' : pid },
			dataType : 'JSON',
			success : function(data) {
				if (data  != 'false') {

					populateRateData(data);	
				} 
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateRateData = function(data) {

		$.each(data, function(index, elem){
		
			appendToTable(elem.item_id, elem.item_des, elem.rate)
		});

		//updaterate.bindRateGrid();
	}
	
	var appendToTable = function(itemId, itemDesc, rate) {
		 
		var srno = $('#updateRateTbl tbody tr').length + 1;
		var row = 	"<tr>" +
						"<td class='srno numeric' data-title='Sr#' > "+ srno +"</td>" +
				 		"<td class='item_desc' data-title='Description' data-item_id='"+ itemId +"'> "+ itemDesc +"</td>" +
					 	"<td class='rate numeric' data-title='Rate'> "+ rate +"</td>" +
					 	"<td><a href='' class='btn btn-primary btnRowEdit'><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td>" +
				 	"</tr>";
		$(row).appendTo('#updateRateTbl');
	}

	var validateSingleProductAdd = function() {

		var errorFlag = false;
		var item = $('#itemDropdown');
		var rate = $('#txtRate');

		// remove the error class first
		$('.inputerror').removeClass('inputerror');
		
		if ( !item.val() ) {
			$('#itemDropdown').addClass('inputerror');
			errorFlag = true;
		}
		if ( !rate.val() ) {
			rate.addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	return {

		init : function() {
			this.bindUI();
			//this.bindRateGrid();
		},

		bindUI : function() {

			var self = this;

			$.ajaxSetup({ cache: false });

			// bind application wide loader
			$(document).ajaxStart(function() {

				$(".loader").show();
			});

			$(document).ajaxComplete(function(event, xhr, settings) {

				$(".loader").hide();
			});
			
			$('#btnAdd').on('click', function(e) {
				e.preventDefault();

				var error = validateSingleProductAdd();
				if (!error) {

					var itemId = $('#itemDropdown').val();
					var itemDesc = $('#itemDropdown').find('option:selected').text();
					var rate = $('#txtRate').val();

					$('#itemDropdown').select2('val', '');
					$('#txtRate').val('');
			
					appendToTable(itemId, itemDesc , rate);
				} else {

					alert('Correct the errors!');
				}
			});

			$('#updateRateTbl').on('click', '.btnRowRemove', function(e) {
				e.preventDefault();
				$(this).closest('tr').remove();
			});

			$('#itemDropdown').on('change', function(){

				var purRate = $('#itemDropdown').find('option:selected').data('purrate');
				$('#txtRate').val(purRate);
			});

			$('#updateRateTbl').on('click', '.btnRowEdit', function(e) {
				e.preventDefault();

				// getting values of the cruel row
				var itemId = $.trim($(this).closest('tr').find('td.item_desc').data('item_id'));
				var rate = $.trim($(this).closest('tr').find('td.rate').text());
				
				$('#itemDropdown').select2('val', itemId);
				$('#txtRate').val(rate);
				// now we have get all the value of the row that is being deleted. so remove that cruel row
				$(this).closest('tr').remove();	// yahoo removed
			});

			$('#partyDropdown').on('change', function() {

				$('#updateRateTbl tbody tr').remove();
				fetchPartyWiseRate($(this).val());
			});

		
			// $('#txtType').select2();
			shortcut.add("F10", function() {
    	
    			$('.btnSave').trigger('click');
			});
			// when save button is clicked
			$('.btnSave').on('click', function(e) {
				if ($('#VoucherTypeHidden').val()=='edit' && $('.btnSave').data('updatebtn')==0 ){
					alert('Sorry! you have not update rights..........');
				}else if($('#VoucherTypeHidden').val()=='new' && $('.btnSave').data('insertbtn')==0){
					alert('Sorry! you have not insert rights..........');
				}else{
					e.preventDefault();
					self.initSave();
				}
			});

			$('.btnSave1').on('click', function(e) {
				if ($('#VoucherTypeHidden').val()=='edit' && $('.btnSave').data('updatebtn')==0 ){
					alert('Sorry! you have not update rights..........');
				}else if($('#VoucherTypeHidden').val()=='new' && $('.btnSave').data('insertbtn')==0){
					alert('Sorry! you have not insert rights..........');
				}else{
					e.preventDefault();
					self.initSave();
				}
			});
			
			shortcut.add("F5", function(e) {
    			e.preventDefault();

    			self.resetVoucher();
			});


			// alert('enter'+ e.keyCode )
			// when reset button is clicked
			$('.btnReset').on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			$('.btnReset1').on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});
		},

		// makes the voucher ready to save
		initSave : function() {
			var accountObj = getSavePartyRateObj();	// returns the account detail object to save into database
			var isValid = validateSave();			// checks for the empty fields

			if (!isValid) {
				save( accountObj );		// saves the detail into the database	
			} else {
				alert('Correct the errors!');
			}
		},
		bindRateGrid : function() {
	            var dontSort = [];
	            $('#discountTable thead th').each(function () {
	                if ($(this).hasClass('no_sort')) {
	                    dontSort.push({ "bSortable": false });
	                } else {
	                    dontSort.push(null);
	                }
	            });
	            updaterate.rateDTable = $('#discountTable').dataTable({
	                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
	                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
	                "aaSorting": [[0, "asc"]],
	                "bPaginate": true,
	                "sPaginationType": "full_numbers",
	                "bJQueryUI": false,
	                "iDisplayLength" : 50,
	                "aoColumns": dontSort
	            });

	            $.extend($.fn.dataTableExt.oStdClasses, {
	                "s`": "dataTables_wrapper form-inline"
	            });
		},
		// resets the voucher
		resetVoucher : function() {
			$('#VoucherTypeHidden').val('new');
			general.reloadWindow();

		}
	};
};

var updaterate = new UpdateRate();
updaterate.init();