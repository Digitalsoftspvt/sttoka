var Message = function() {

	// saves the data into the database
	var save = function( message ) {

		$.ajax({
			url : base_url + 'index.php/message/save',
			type : 'POST',
			data : { 'message' : message },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'false') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					alert('Message saved successfully.');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var fetch = function(message_id) {

		$.ajax({
			url : base_url + 'index.php/message/fetch',
			type : 'POST',
			data : { 'message_id' : message_id },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					populateData(data);
				}
			},
			error: function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var fetchPartyInfo = function(id){
		$.ajax({

			type:'POST',
			url:base_url + 'index.php/message/fetchPartyInfo',
			data:{'id':id},
			dataType:'json',
			success:function(data){

				$('#txtcity').val(data[0]['city']);
				$('#txtcountry').val(data[0]['country']);
				$('#txtmobile').val(data[0]['mobile']);

			},error:function(xhr,status,error){
				console.log(xhr.responseText);
			}
		});
	}

	// generates the view
	var populateData = function(elem) {

		$('#vouchertypehidden').val('edit');
		$('#txtId').val(elem.message_id);
		$('#txtIdHidden').val(elem.message_id);
		$('#txtcity').val(elem.city);
		$('#txtcountry').val(elem.country);
		$('#txtmobile').val(elem.mobile);
		$('#message_text').val(elem.msg);
		$('#party_dropdown11').select2('val',elem.party_id);
	}

	// gets the maxid of the voucher
	var getMaxId = function() {

		$.ajax({
			url : base_url + 'index.php/message/getMaxId',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$('#txtId').val(data);
				$('#txtIdHidden').val(data);
				$('#txtMaxIdHidden').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;

		var name = $.trim($('#party_dropdown11').val());

		// remove the error class first
		$('#party_dropdown11').removeClass('inputerror');

		if ( name === '' ) {
			$('#party_dropdown11').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	// returns the fee category object to save into database
	var getSaveObject = function() {

		var obj = {};
		obj.message_id = $.trim($('#txtIdHidden').val());
		obj.date = $.trim($('#current_date').val());
		obj.city = $.trim($('#txtcity').val());
		obj.country = $.trim($('#txtcountry').val());
		obj.mobile = $.trim($('#txtmobile').val());
		obj.party_id = $.trim($('#party_dropdown11').val());
		obj.msg = $.trim($('#message_text').val());

		return obj;
	}

	return {

		init : function() {
			$('#vouchertypehidden').val('new');
			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			shortcut.add("F10", function() {
    			$('.btnSave').trigger('click');
			});
			shortcut.add("F6", function() {
    			$('#txtId').focus();
			});
			shortcut.add("F5", function() {
    			self.resetVoucher();
			});

			$('#txtId').on('change', function() {
				fetch($(this).val());
			});

			// when save button is clicked
			$('.btnSave').on('click', function(e) {
				
				if ($('#vouchertypehidden').val()=='edit' && $('.btnSave').data('updatebtn')==0 ){
					alert('Sorry! you have not update rights..........');
				}else if($('#vouchertypehidden').val()=='new' && $('.btnSave').data('insertbtn')==0){
					alert('Sorry! you have not insert rights..........');
				}else{
					// alert($('#vouchertypehidden').val() + '- ' + $('.btnSave').data('updatebtn') );
					e.preventDefault();
					self.initSave();
				}
			});

			// when the reset button is clicked
			$('.btnReset').on('click', function(e) {
				e.preventDefault();		// prevent the default behaviour of the link
				self.resetVoucher();	// resets the voucher
			});

			// when text is chenged inside the id textbox
			$('#txtId').on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {

					// get the based on the id entered by the user
					if ( $('#txtId').val().trim() !== "" ) {

						var message_id = $.trim($('#txtId').val());
						fetch(message_id);
					}
				}
			});
			$('#mesage_table').on('click', '.btn-edit-account',function(e) {
				e.preventDefault();
				
				var partyname = $.trim($(this).closest('tr').find('td.partyid').text());
				var partymobile = $.trim($(this).closest('tr').find('td.partymobile').text());
				var partycity = $.trim($(this).closest('tr').find('td.partycity').text());
				var partycountry = $.trim($(this).closest('tr').find('td.partycountry').text());

				$('#party_dropdown11').select2('val',partyname);
				$('#txtcity').val(partycity);
				$('#txtcountry').val(partycountry);
				$('#txtmobile').val(partymobile);

				$('a[href="#add_message"]').trigger('click');
			});

			$('#party_dropdown11').on('change',function(){
				var id=$(this).val();
				fetchPartyInfo(id);
			});

			// when edit button is clicked inside the table view
			$('.btn-edit-dept').on('click', function(e) {
				e.preventDefault();

				fetch($(this).data('message_id'));		// get the class detail by id
				$('a[href="#add_message"]').trigger('click');
			});

			getMaxId();		// gets the max id of voucher
		},

		// makes the voucher ready to save
		initSave : function() {

			var saveObj = getSaveObject();	// returns the class detail object to save into database
			var error = validateSave();			// checks for the empty fields

			if ( !error ) {
				save( saveObj );
			} else {
				alert('Correct the errors...');
			}
		},

		// resets the voucher
		resetVoucher : function() {

			$('.inputerror').removeClass('inputerror');
			$('#txtId').val('');
			$('#current_date').val('');
			$('#txtcity').val('');
			$('#txtcountry').val('');
			$('#txtmobile').val('');
			$('#party_dropdown11').select2('val','');
			$('#message_text').val('');

			getMaxId();		// gets the max id of voucher
		}
	};
};

var message = new Message();
message.init();