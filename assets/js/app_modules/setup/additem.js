var AddItem = function() {

	// saves the data into the database
	var save = function( item ) {

		$.ajax({
			url : base_url + 'index.php/item/save',
			type : 'POST',
			data : item,
			processData : false,
			contentType : false,
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					alert('Item saved successfully.');
					// $(".alert-message").alert();
					// window.setTimeout(function() { $(".alert-message").alert('close'); }, 2000);
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	// gets the image uploaded and show it to the user
	var getImage = function() {

		var file = $('#itemImage').get(0).files[0];
		if (file) {
			if (!!file.type.match(/image.*/)) {
                if ( window.FileReader ) {
                    reader = new FileReader();
                    reader.onloadend = function (e) {
                        //showUploadedItem(e.target.result, file.fileName);
                        $('#itemImageDisplay').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(file);
                }
            }
		};

		return file;
	}

	var fetch = function(item_id) {

		$.ajax({
			url : base_url + 'index.php/item/fetch',
			type : 'POST',
			data : { 'item_id' : item_id },
			dataType : 'JSON',
			success : function(data) {
				
				$('#itemImageDisplay').attr('src', base_url + 'assets/img/blank_image.png');
				if (data === 'false') {
					alert('No data found');
				} else {
					populateData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateData = function(data) {

		$('#VoucherTypeHidden').val('edit');
		$('#txtIdHidden').val(data.item_id);

		$('#txtId').val(data.item_id);
		(data.active === "1") ? $('#active').bootstrapSwitch('state', true) : $('#active').bootstrapSwitch('state', false);
		$('#current_date').datepicker("update", data.open_date.substring(0, 10));
		$('#category_dropdown').val(data.catid);
		$('#category_dropdown').select2('val',data.catid);

		$('#subcategory_dropdown').val(data.subcatid);
		$('#subcategory_dropdown').select2('val',data.subcatid);

		$('#brand_dropdown').val(data.bid);
		$('#brand_dropdown').select2('val',data.bid);
		$('#ic_dropdown').select2('val',data.item_id);

		$('#txtBarcode').val(data.barcode);
		$('#txtDescription').val(data.item_des);
        $('#txtUrduDescription').val(data.uitem_des);
		$('#txtMinLevel').val(data.min_level);
		$('#txtMaxLevel').val(data.max_level);
		$('#uom_dropdown').val(data.uom);
		$('#txtPurPrice').val(data.cost_price);
		$('#txtPacking').val(data.size);
		$('#txtSalePrice').val(data.srate);
		$('#txtDiscount').val(data.srate1);
		$('#txtComm').val(data.srate2);
		$('#txtRemarks').val(data.description);
		$('#txtNetWeight').val(parseFloat(data.netweight).toFixed(2));
		$('#txtGrWeight').val(parseFloat(data.grweight).toFixed(2));
		$('#txtMould').val(data.srate3);
		$('#txtRate').val(data.srate4);
		$('#txtSelfFrightRate').val(data.self_freight_rate);
		$('#txtBiltyFrightRate').val(data.bilty_freight_rate);
		$('#txtGariFrightRate').val(data.gari_freight_rate);
		

		 // set image
		if (data.photo !== "") {
			$('#itemImageDisplay').attr('src', base_url + '/assets/uploads/items/' + data.photo);
		}
	}

	// gets the max id of the voucher
	var getMaxId = function() {

		$.ajax({

			url : base_url + 'index.php/item/getMaxId',
			type : 'POST',
			dataType : 'JSON',
			success : function(data) {

				$('#txtId').val(data);
				$('#txtMaxIdHidden').val(data);
				$('#txtIdHidden').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var getSaveObject = function() {

		var itemObj = {
			item_id : $.trim($('#txtIdHidden').val()),

			active : ($('#active').bootstrapSwitch('state') === true) ? 1 : 0,
			open_date : $.trim($('#current_date').val()),
			catid : $('#category_dropdown').val(),
			subcatid : $.trim($('#subcategory_dropdown').val()),
			bid : $.trim($('#brand_dropdown').val()),
			barcode : $.trim($('#txtBarcode').val()),
			description : $.trim($('#txtRemarks').val()),
			item_des : $.trim($('#txtDescription').val()),
            uitem_des : $.trim($('#txtUrduDescription').val()),
			min_level : $.trim($('#txtMinLevel').val()),
			max_level : $.trim($('#txtMaxLevel').val()),
			uom : $.trim($('#uom_dropdown').val()),
			cost_price : $.trim($('#txtPurPrice').val()),
			srate : $.trim($('#txtSalePrice').val()),	// sale price 1
			srate1 : $.trim($('#txtDiscount').val()),	// sale price 2
			srate2 : $.trim($('#txtComm').val()),		// sale price 3
			srate3 : $.trim($('#txtMould').val()),		// sale price 4
			srate4 : $.trim($('#txtRate').val()),		// sale price 5
			size : $('#txtPacking').val(),				// sale price 6
			netweight : $.trim($('#txtNetWeight').val()),
			grweight : $.trim($('#txtGrWeight').val()),
			self_freight_rate :  $.trim($('#txtSelfFrightRate').val()),
			bilty_freight_rate :  $.trim($('#txtBiltyFrightRate').val()),
			gari_freight_rate :  $.trim($('#txtGariFrightRate').val()),
			uid : $.trim($('#uid').val()),
			company_id : $.trim($('#cid').val()),
			// paryt_id : $.trim($('#debit_dropdown').val()),
			// party_id_cr : $.trim($('#credit_dropdown').val())
		};

		var form_data = new FormData();

		for ( var key in itemObj ) {
		    form_data.append(key, itemObj[key]);
		}

		form_data.append("photo", getImage());

		return form_data;
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		// var _barcode = $('#txtBarcode').val();
		var _desc = $.trim($('#txtDescription').val());
		var cat = $.trim($('#category_dropdown').val());
		var subcat = $('#subcategory_dropdown').val();
		var brand = $.trim($('#brand_dropdown').val());
		var _uom = $.trim($('#uom_dropdown').val());

		// remove the error class first
		$('.inputerror').removeClass('inputerror');
		if ( _desc === '' || _desc === null ) {
			$('#txtDescription').addClass('inputerror');
			errorFlag = true;
		}
		if ( !cat ) {
			$('#category_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		if ( !subcat ) {
			$('#subcategory_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		if ( !brand ) {
			$('#brand_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		if ( !_uom ) {
			$('#uom_dropdown').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	return {

		init : function() {
			this.bindUI();
			$('#VoucherTypeHidden').val('new');
			this.bindModalItemGrid();
			this.bindModalItemGridViewAll();
		},

		bindUI : function() {

			var self = this;
			$('.modal-lookup .populateItem').on('click', function(){
				// alert('dfsfsdf');
				var item_id = $(this).closest('tr').find('input[name=hfModalitemId]').val();
				$("#item_dropdown").select2("val", item_id); //set the value
				$("#itemid_dropdown").select2("val", item_id);
				// $('#txtQty').focus();				
				fetch(item_id);
			});
			shortcut.add("F2", function() {
				$('a[href="#item-lookup"]').trigger('click');
			});
			shortcut.add("F10", function() {
    			$('.btnSave').trigger('click');
			});
			shortcut.add("F6", function() {
    			$('#txtId').focus();
			});
			shortcut.add("F5", function() {
    			self.resetVoucher();
			});

			$("#active").bootstrapSwitch('offText', 'No');
			$("#active").bootstrapSwitch('onText', 'Yes');

			$('#ic_dropdown').on('change', function() {
				fetch($(this).val());
			});

			$('#txtId').on('change', function() {
				fetch($(this).val());
			});

			// when btnSave is clicked
			$('.btnSave').on('click', function(e) {
				if ($('#VoucherTypeHidden').val()=='edit' && $('.btnSave').data('updatebtn')==0 ){
					alert('Sorry! you have not update rights..........');
				}else if($('#VoucherTypeHidden').val()=='new' && $('.btnSave').data('insertbtn')==0){
					alert('Sorry! you have not insert rights..........');
				}else{
					// alert($('#VoucherTypeHidden').val() + '- ' + $('.btnSave').data('updatebtn') );
					e.preventDefault();
					self.initSave();
				}
			});
			// when btnReset is clicked
			$('.btnReset').on('click', function(e) {
				e.preventDefault();		// removes the default behaviour of the click event
				self.resetVoucher();
			});

			// when text is changed in txtStudentId
			$('#txtId').on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {

					// get the based on the id entered by the user
					if ( $('#txtId').val().trim() !== "" ) {

						var item_id = $.trim($('#txtId').val());
						fetch(item_id);
					}
				}
			});

			// when image is changed
			$('#itemImage').on('change', function() {
				getImage();
			});

			////////////////////////////////////////////////////////////////////
			/// Models
			////////////////////////////////////////////////////////////////////
			$("a[href='#UOM']").on('click', function() {
				$('#txtNewUOM').val('');
			});
			$("a[href='#Brand']").on('click', function() {
				$('#txtNewBrand').val('');
			});
			$("a[href='#CategoryModel']").on('click', function() {
				$('#txtNewCategory').val('');
			});
			$("a[href='#SubCategory']").on('click', function() {
				$('#txtNewSubCategory').val('');
			});
			$('.btnNewUOM').on('click', function() {

				if ($('#txtNewUOM').val() !== "") {

					var newUOM = "<option value='"+ $('#txtNewUOM').val() +"' selected>"+ $('#txtNewUOM').val() + "</option>";

					$(newUOM).appendTo('#uom_dropdown');
					$(this).siblings().trigger('click');
				}
			});

			$('.btnPrint').on('click', function(ev) {
				ev.preventDefault();
				self.showAllRows();
                window.open(base_url + 'application/views/reportprints/item_print.php', "Purchase Report", 'width=1210, height=842');
            });
			// when edit button is clicked inside the table view
			$('table').on('click', '.btn-edit-item', function(e) {
				e.preventDefault();
				
				fetch($(this).data('itemid'));		// get the class detail by id
				$('a[href="#additem"]').trigger('click');
			});

			$(".importBtn").on('click',function(e) {
					e.preventDefault();

					var saveimport_object = getimport_object();
					var error = validateimport_object();

					if (!error) {
						var rowsCount = $('#import_tbl').find('tbody tr').length;
						if (rowsCount > 0) {
							save_import(saveimport_object);
						} else {
							alert('No date found in the table to save!');
						} 	
					} else {
						alert('correct the errors..!!');
					}
					
				});
				$('form #upload_file').submit(function(e) {
					e.preventDefault();
					var fileName = $('#userfile').val().split('\\').pop();
					$.ajaxFileUpload({
						url 			: base_url + 'index.php/upload/upload_file/', 
						secureuri		:false,
						fileElementId	:'userfile',
						dataType		: 'JSON',
						data			: {
							'title'				: 'Import Item'
						},
						success	: function (data)
						{
							var obj = $.parseJSON( data );
							if (obj.msg !='File successfully uploaded') {
								alert(obj.msg);
							} else {
								alert(obj.msg);
								$('#excel_path').val(obj.file_name);
							}
						},
						error : function(xhr, status, error) {
							alert('fail');
							console.log(xhr.responseText);
						}
					});
					return false;
				});

                $('input[type="text"]').keydown(function(event){
                    if(event.keyCode == 13) {
                        event.preventDefault();
                        return false;
                    }
                });

	

			getMaxId();
		},
		
		// prepares the data to save it into the database
		initSave : function() {

			var obj = getSaveObject();
			var error = validateSave();

			if (!error) {
				save( obj );
			} else {
				alert('Correct the errors...');
			}
		},
		bindModalItemGrid : function() {
            var dontSort = [];
            $('#item-lookup table thead th').each(function () {
                if ($(this).hasClass('no_sort')) {
                    dontSort.push({ "bSortable": false });
                } else {
                    dontSort.push(null);
                }
            });
            addItem.pdTable = $('#item-lookup table').dataTable({
                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
                "aaSorting": [[0, "asc"]],
                "bPaginate": true,
                "sPaginationType": "full_numbers",
                "bJQueryUI": false,
                "aoColumns": dontSort

            });
            $.extend($.fn.dataTableExt.oStdClasses, {
                "s`": "dataTables_wrapper form-inline"
            });
		},
		bindModalItemGridViewAll : function() {
            var dontSort = [];
            $('#datatable_example thead th').each(function () {
                if ($(this).hasClass('no_sort')) {
                    dontSort.push({ "bSortable": false });
                } else {
                    dontSort.push(null);
                }
            });
            addItem.dTable = $('#datatable_example').dataTable({
                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
                "aaSorting": [[0, "asc"]],
                "bPaginate": true,
                "sPaginationType": "full_numbers",
                "bJQueryUI": false,
                "aoColumns": dontSort,
                "iDisplayLength" : 100

            });
            $.extend($.fn.dataTableExt.oStdClasses, {
                "s`": "dataTables_wrapper form-inline"
            });
		},
		 showAllRows : function () {
		 	
	        var oSettings = addItem.dTable.fnSettings();
	        oSettings._iDisplayLength = 50000;

	        addItem.dTable.fnDraw();
        },

		// resets the voucher to its default state
		resetVoucher : function() {
			$('#VoucherTypeHidden').val('new');
			general.reloadWindow();

		}
	}

};

var addItem = new AddItem();
addItem.init();