var settingConfiguration = function(){
	var validateSave = function() {

		var flag 			= false;
		var sale	    	= $('#sale');
		var purchase 		= $('#purchase');
		var salereturn 		= $('#saleReturn');
		var purchaseReturn 	= $('#purchaseReturn');
		var discount 		= $('#discount');
		var expenses 		= $('#expenses');
		var tax 			= $('#tax');
		var cash 			= $('#cash');
		var freight 		= $('#freight');
		var itemdiscount 	= $('#itemdiscount');

		// remove the error class first
		$('.inputerror').removeClass('inputerror');

		if ( !sale.val() ) {
			sale.addClass('inputerror');
			flag = true;
		}

		if ( !purchase.val() ) {
			purchase.addClass('inputerror');
			flag = true;
		}

		if ( !salereturn.val() ) {
			salereturn.addClass('inputerror');
			flag = true;
		}
		if ( !purchaseReturn.val() ) {
			purchaseReturn.addClass('inputerror');
			flag = true;
		}
		if ( !itemdiscount.val() ) {
			itemdiscount.addClass('inputerror');
			flag = true;
		}

		
		return flag;
	}


	var getSaveObject = function() {

		var obj 	= {};
		
		obj.sale      		= $('#sale').val();
		obj.purchase		= $('#purchase').val();
		obj.salereturn		= $('#saleReturn').val();
		obj.purchasereturn 	= $('#purchaseReturn').val();
		obj.discount		= $('#discount').val();
		obj.expenses 		= $('#expenses').val();
		obj.tax 			= $('#tax').val();
		obj.cash 			= $('#cash').val();
		// obj.freight 		= $('#freight').val();
		obj.itemdiscount 		= $('#itemdiscount').val();
						
		return obj;
	}

	  /////////////////////////////////////////////////////
	 ///////////   Saving Data     ///////////////////////
	/////////////////////////////////////////////////////

	var save = function(obj) {
		
		$.ajax({
			url 	: base_url + "index.php/setting_configuration/save",
			type 	: 'POST',
			data 	: { 'obj' : obj },
			dataType: 'JSON',
			success : function(data) {
			
					if (data.error === 'true') {
						alert('An internal error occured while saving voucher. Please try again.');
					} else {
						alert('Voucher saved successfully.');
						general.reloadWindow();
					}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}


	  /////////////////////////////////////////////////
	 /////////    Fetching Records   /////////////////
	/////////////////////////////////////////////////

	var fetch = function() {

		$.ajax({

			url  : base_url + 'index.php/setting_configuration/fetch',
			type : 'POST',
			data : {  },
			dataType : 'JSON',
			success : function(data) {

			    //resetfields();
				if (data === 'false') {
					alert('No data found.');
				} else {
					populateData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateData = function(data){

		$('#VoucherTypeHidden').val('edit');
		$('#txtId').select2('val',data[0]['id']);
		$('#sale').select2('val',data[0]['sale']);
		$('#purchase').select2('val',data[0]['purchase']);
		$('#saleReturn').select2('val',data[0]['salereturn']);
		$('#purchaseReturn').select2('val',data[0]['purchasereturn']);
		$('#discount').select2('val',data[0]['discount']);
		$('#expenses').select2('val',data[0]['expenses']);
		$('#tax').select2('val',data[0]['tax']);
		$('#cash').select2('val',data[0]['cash']);
		$('#freight').select2('val',data[0]['freight']);
		$('#itemdiscount').select2('val',data[0]['itemdiscount']);

	}




	return{
		init : function(){
			
			$('#VoucherTypeHidden').val('new');
			this.bindUI();
		},
		bindUI : function(){
			var self = this;
			$('#sale,#purchase,#saleReturn,#purchaseReturn,#discount,#expenses,#tax,#cash,#freight,#itemdiscount').select2();
			
			$('.btnSave').on('click', function(e) {

				if ($('#VoucherTypeHidden').val()=='edit' && $('.btnSave').data('updatebtn')==0 ){

					alert('Sorry! you have not update rights..........');
				}else if($('#VoucherTypeHidden').val()=='new' && $('.btnSave').data('insertbtn')==0){

					alert('Sorry! you have not insert rights..........');
				}else{

					e.preventDefault();
					self.initSave();
				}
			});

			$('.btnReset').on('click', function(e) {
				e.preventDefault();		// prevent the default behaviour of the link
				self.resetVoucher();	// resets the voucher
			});

			shortcut.add('F10',function(){
				self.initSave();
			});
			shortcut.add('F5',function(){
				self.resetVoucher();
			});

			fetch();
		},
		// makes the voucher ready to save
		initSave : function() {

			var saveObj = getSaveObject();	// returns the object to save into database
			var error   = validateSave();	// checks for the empty fields

			if ( !error ) {
				save(saveObj);
			} 
		},
		// resets the voucher
		resetVoucher : function() {
			general.reloadWindow();
		},

	}

};
var setting = new settingConfiguration();
setting.init();