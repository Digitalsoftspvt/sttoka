var Shift = function() {

	// saves the data into the database
	var save = function( itemDetail ) {

		$.ajax({
			url : base_url + 'index.php/item/updateitemsRate',
			type : 'POST',
			data : { 'item_detail' : JSON.stringify(itemDetail.item_detail), 'status' : itemDetail.status, 'update_item_history' : itemDetail.update_item_history },
			dataType : 'JSON',
			success : function(data) {
				
				if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					alert('Items Updated successfully.');
					general.reloadWindow();
				}
				
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
var fetchItemByBrand = function() {
		crit= getcrit();
		$.ajax({
			url : base_url + 'index.php/item/fetchitembybrand',
			type : 'POST',
			data : { 'crit' : crit },
			dataType : 'JSON',
			success : function(data) {
				
				if (data === 'false') {
   
					alert('No data found');
				} else {

					populateData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	
	var getcrit = function (){

        var category=$("#category_dropdown").select2("val");
        var subcategory=$('#subcategory_dropdown').select2("val");
        var brand=$('#brand_dropdown').select2("val");
        
        
        var crit ='';
        
        if (category!=''){
            crit +='AND i.catid in (' + category +') ';
        }
        if (subcategory!='') {
            crit +='AND i.subcatid in (' + subcategory +') '
        }
        if (brand!='') {
            crit +='AND i.bid in (' + brand +') ';
        }
            
        
        return crit;

   }
	var resetFields = function(){
		$('#txtName').val('');
		$('#txtStrength').val('');
		$('#txtDescription').val('');
		$('#txturduName').val('');
	}

	// generates the view
	var populateData = function(data) {
		
		if (typeof shift.dTable != 'undefined') {
            shift.dTable.fnDestroy();
            $('#itemRows').empty();
        }

		$.each(data, function(index, elem){

			appendToTable(elem.item_id, elem.item_code, elem.item_des, parseFloat(elem.grweight).toFixed(2), parseFloat(elem.cost_price).toFixed(2), parseFloat(elem.srate).toFixed(2), parseFloat(elem.srate1).toFixed(2), parseFloat(elem.srate2).toFixed(2), parseFloat(elem.size).toFixed(2), parseFloat(elem.srate3).toFixed(2), parseFloat(elem.srate4).toFixed(2), parseFloat(elem.bilty_freight_rate).toFixed(2), parseFloat(elem.gari_freight_rate).toFixed(2), parseFloat(elem.self_freight_rate).toFixed(2));
		});

		bindGrid();
		$('#itemUpdateTable_filter').addClass('hide');
	}

	var appendToTable = function(itemId, shortCode, itemDes,grWeight, costPrice, sRateDelear, sRate1Deduction,sRate2Retail,sizeSalePrice4,sRate3Mould,sRate4Dhary,bilty_freight_rate,gari_freight_rate,self_freight_rate) {

		var srno = $('#itemUpdateTable tbody tr').length + 1;
		var row = 	"<tr>" +
						"<td class='srno numeric text-left' style='width:5px !important;' data-title='Sr#' > "+ srno +"</td>" +
						"<td class='shortCode text-left' style='width:70px !important;' data-title='Short Code' > " + shortCode + "</td>" +
				 		"<td class='item_desc' data-title='Description' style='width:200px !important;' data-item_id='"+ itemId +"'> "+ itemDes +"</td>" +
				 		"<td class='' data-title='GWeight' style='width:60px !important;'><input type='text' class='form-control input_back grweight' value="+ grWeight +" /></td>" +
				 		"<td class='' data-title='Purchas Rate' style='width:60px !important;'><input type='text' class='form-control input_back costPrice' value="+ costPrice +" /></td>" +
				 		"<td class='' data-title='Dealer Rate' style='width:60px !important;'> <input type='text' class='form-control input_back sRateDelear' value="+ sRateDelear +" /></td>" +
				 		"<td class='' data-title='Retail Rate' style='width:60px !important;'><input type='text' class='form-control input_back sRate2Retail' value="+ sRate2Retail +" /></td>" +
				 		"<td class='' data-title='Deduction Rate' style='width:60px !important;'><input type='text' class='form-control input_back sRate1Deduction' value="+ sRate1Deduction +" /></td>" +
				 		"<td class='' data-title='Mould Rate' style='width:60px !important;'><input type='text' class='form-control input_back sRate3Mould' value="+ sRate3Mould +" /></td>" +
				 		"<td class='' data-title='Dhary Rate' style='width:60px !important;'><input type='text' class='form-control input_back sRate4Dhary' value="+ sRate4Dhary +" /></td>" +
				 		"<td class='' data-title='Bilty Rate' style='width:60px !important;'><input type='text' class='form-control input_back bilty_freight_rate' value="+ bilty_freight_rate +" /></td>" +
				 		"<td class='' data-title='Gari Rate' style='width:60px !important;'><input type='text' class='form-control input_back gari_freight_rate' value="+ gari_freight_rate +" /></td>" +
				 		"<td class='' data-title='Self Rate' style='width:60px !important;'><input type='text' class='form-control input_back self_freight_rate' value="+ self_freight_rate +" /></td>" +
				 		
				 	"</tr>";
		$(row).appendTo('#itemUpdateTable');
	}

	// returns the fee category object to save into database
	var getSaveObject = function() {

		var itemDetail = [];
		var status = 0;
		if($('#updateItemCheck').is(":checked")){
    		status = 1;
    	};

		$('#itemUpdateTable').find('tbody tr').each(function( index, elem ) {
			var id = {};

			id.item_id = $.trim($(elem).find('td.item_desc').data('item_id'));
			id.grweight = $.trim($(elem).find('input.grweight').val());
			id.cost_price = $.trim($(elem).find('input.costPrice').val());
			id.srate = $.trim($(elem).find('input.sRateDelear').val());
			id.srate1 = $.trim($(elem).find('input.sRate1Deduction').val());
			id.srate2 = $.trim($(elem).find('input.sRate2Retail').val());
			id.srate3 = $.trim($(elem).find('input.sRate3Mould').val());
			id.srate4 = $.trim($(elem).find('input.sRate4Dhary').val());
			id.bilty_freight_rate = $.trim($(elem).find('input.bilty_freight_rate').val());
			id.gari_freight_rate = $.trim($(elem).find('input.gari_freight_rate').val());
			id.self_freight_rate = $.trim($(elem).find('input.self_freight_rate').val());

			
			itemDetail.push(id);
		});

		var updateItemHistory = {};

		updateItemHistory.curr_date = $('#currDate').val();
		updateItemHistory.effected_date = $('#effectedDate').val();

		var data = {};
		data.item_detail = itemDetail;
		data.status = status;
		data.update_item_history = updateItemHistory;
		
		return data;
	}

	var bindGrid = function() {
        var dontSort = [];
        $('#itemUpdateTable thead th').each(function () {
            if ($(this).hasClass('no_sort')) {
                dontSort.push({ "bSortable": false });
            } else {
                dontSort.push(null);
            }
        });
        shift.dTable = $('#itemUpdateTable').dataTable({
            // Uncomment, if prolems with datatable.
            // "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone'<'row-fluid'<'span8' f>>>'<'pag_top' p> T>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
            "aaSorting": [[0, "asc"]],
            "bPaginate": false,
            "sPaginationType": "full_numbers",
            "bJQueryUI": false,
            "aoColumns": dontSort,
            "bSort": false,
            "iDisplayLength" : 10000,
            "oTableTools": {
                "sSwfPath": "js/copy_cvs_xls_pdf.swf",
                "aButtons": [{ "sExtends": "print", "sButtonText": "Print Report", "sMessage" : "Inventory Report" }]
            }
        });
        $.extend($.fn.dataTableExt.oStdClasses, {
            "s`": "dataTables_wrapper form-inline"
        });
    }

	return {

		init : function() {
			$('#vouchertypehidden').val('new');
			this.bindUI();
		},

		bindUI : function() {

			var self = this;

			shortcut.add("F10", function() {
    			$('.btnSave').trigger('click');
			});
			
			shortcut.add("F5", function() {
    			self.resetVoucher();
			});
			
			$('#txtId').on('change', function() {
				fetch($(this).val());
			});

			// when save button is clicked
			$('.btnSave').on('click', function(e) {
					e.preventDefault();
					self.initSave();
			});

			// when the reset button is clicked
			$('.btnReset').on('click', function(e) {
				e.preventDefault();		// prevent the default behaviour of the link
				general.reloadWindow();
			});

			$('#radioCheckedAll').on('click', function(e) {
                $('.checkboxes').prop('checked', true);
            });

            $('#radioUncheckedAll').on('click', function(e) {
                $('.checkboxes').prop('checked', false);
            });

            $('.btnsearch').on('click',function(e){
            	e.preventDefault();
            	fetchItemByBrand();
            });

            $('#txtSearch').on('input',function(){

            	$('#itemUpdateTable_filter').find('input').val($(this).val()).trigger('input');
            });

		},

		// makes the voucher ready to save
		initSave : function() {

			var saveObj = getSaveObject();	// returns the class detail object to save into database
			
			var rowsCount = $('#itemUpdateTable').find('tbody tr').length;

			if (rowsCount > 0 ) {
				save( saveObj );
			} else {
				alert('No date found to save!');
			}
		}
	};
};

var shift = new Shift();
shift.init();