var SearchFile = function() {

	var  createThumbnail = function(src, filename) {
		var re = /(?:\.([^.]+))?$/;

		var ext = re.exec(filename);
		var ext = ext[1].toLowerCase();
		
		if(ext == 'jpg' || ext == 'jpeg' || ext == 'bmp' || ext == 'png' || ext == 'gif'){
			var img = "<a id='img_thumbnail' href='"+src+"' target='_blank'><img alt='"+filename+"' src='" 
						+ src +
					   "' class='img-responsive img-thumbnail' id='file' /><div class='caption text-center'><p> " + filename + "</p></div></a>";
			$('#thumbnail').html(img);
		} else {
			img = "<a id='img_thumbnail' href='"+src+"' target='_blank'><img alt='"+filename+"' src='" + base_url + "/assets/uploads/icons/doc_icon.png' class='img-responsive img-thumbnail' id='file' /></a>";
			$('#thumbnail').html(img);
		}
	}
	
	var fetchData = function(searchedTxt) {

		$.ajax({

			url : base_url + 'index.php/searchfile/fetch',
			type : 'POST',
			data : { 'searchedTxt' : searchedTxt},
			dataType : 'JSON',
			success : function(data) {
				// console.log(data);
				$('#thumbnail').empty();
				$('#sale_table tbody').empty();
				if(data == 'false'){
					$('.msg').text("0 Result Found for \"" + searchedTxt + "\"");
				}else {
					$('.msg').text(data.length + " Results Found  for \"" + searchedTxt + "\"");
					populateData(data);
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var isNullOrUndefined = function (variable) { 
		var x = $.trim(variable);
		return (x === null || x === undefined || x === '') ? "-" : x; 
	}

	var populateData = function(data) {
		$('#sale_table tbody').empty();

		$.each(data, function(index, elem) {
			var path = base_url + '/uploads/' + elem.etype ;
			var remarks = isNullOrUndefined(elem.remarks);
 			appendToTable(elem.img_id, elem.vrnoa, elem.etype, elem.photo, path, remarks,elem.datetime);
		});
	}

	var appendToTable = function(img_id, vrnoa, etype, filename, path, remarks,datetime) {
		var srno = $('#sale_table tbody tr').length + 1;
		var row = 	"<tr>" +
						"<td class='filename ' data-title='Filename' > <a href='#' style='overflow: hidden;' id='showFile'>"+ filename +"</a><input type='hidden' value'"+img_id+"'></td>" +
						"<td class='path ' data-title='Path' >"+ path +"</td>" +
				 		"<td class='vrnoa' data-title='vnroa' > "+ vrnoa +"</td>" +
				 		"<td class='vrnoa' data-title='vnroa' > "+ datetime.substr(0, 10) +"</td>" +
				 		"<td class='etype' data-title='Etype' > "+ etype +"</td>" +
				 		"<td class='remarks' data-title='Remarks' > "+ remarks +"</td>" +
				 	"</tr>";
		$(row).appendTo('#sale_table');
	}

	return {

		init : function() {
			this.bindUI();
			// this.bindModalPartyGrid();
			// this.bindModalItemGrid();
		},

		bindUI : function() {
			var self = this;

			shortcut.add("F6", function() {
                $('#searchForm').trigger('submit');
            });


			$('#searchForm').on('submit', function(e){
				e.preventDefault();
				var searchTxt = $.trim($('#txtSearch').val());
				// if(searchTxt.length !== 0) {
					fetchData(searchTxt);
				// }else {
				// 	alert('Type Something to Search!');
				// }
				
			});

			$(document).on('click', '#showFile', function(e){
				e.preventDefault();
				var name = $(this).text();
				var  etype = $.trim($(this).closest('tr').find('td.etype').text());
				var path = base_url + 'assets/uploads/' + etype + '/' + name;
				console.log(path);
				createThumbnail(path, name);

			});

		},

		// prepares the data to save it into the database
		bindModalPartyGrid : function() {
            var dontSort = [];
            $('#sale_table table thead th').each(function () {
                if ($(this).hasClass('no_sort')) {
                    dontSort.push({ "bSortable": false });
                } else {
                    dontSort.push(null);
                }
            });
            yarnContract.pdTable = $('#sale_table table').dataTable({
                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
                "aaSorting": [[0, "asc"]],
                "bPaginate": true,
                "sPaginationType": "full_numbers",
                "bJQueryUI": false,
                "aoColumns": dontSort

            });
            $.extend($.fn.dataTableExt.oStdClasses, {
                "s`": "dataTables_wrapper form-inline"
            });
		},

bindModalItemGrid : function() {

			
				            var dontSort = [];
				            $('#item-lookup table thead th').each(function () {
				                if ($(this).hasClass('no_sort')) {
				                    dontSort.push({ "bSortable": false });
				                } else {
				                    dontSort.push(null);
				                }
				            });
				            yarnContract.pdTable = $('#item-lookup table').dataTable({
				                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
				                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
				                "aaSorting": [[0, "asc"]],
				                "bPaginate": true,
				                "sPaginationType": "full_numbers",
				                "bJQueryUI": false,
				                "aoColumns": dontSort

				            });
				            $.extend($.fn.dataTableExt.oStdClasses, {
				                "s`": "dataTables_wrapper form-inline"
				            });
},

		
	}

};

var search = new SearchFile();
search.init();