var Purchase = function() {
	var settings = {
		// basic information section
		switchPreBal : $('#switchPreBal')

	};

	var saveItem = function( item ) {
		$.ajax({
			url : base_url + 'index.php/item/save',
			type : 'POST',
			data : item,
			// processData : false,
			// contentType : false,
			dataType : 'JSON',
			success : function(data) {

				if (data == "duplicateshortcode") {
					$('#txtShortCode').addClass('inputerror');
					alert('ShortCode already Saved!');
				}
				else if(data == "duplicateitem"){
					$('#txtItemName').addClass('inputerror');
					alert('Item already Saved!');
				}
				else{
					if (data.error === 'true') {
						alert('An internal error occured while saving voucher. Please try again.');
					} else {
						alert('Item saved successfully.');
						$('#ItemAddModel').modal('hide');
						resetItemsModalFields();
						fetchItems();
					}
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var resetItemsModalFields = function(){
		$('#category_dropdown').select2('val','');
		$('#subcategory_dropdown').select2('val','');
		$('#brand_dropdown').select2('val','');
		$('#txtBarcode').val('');
		$('#txtItemName').val('');
		$('#txtItemName').val('');
		$('#txtPurPrice').val('');
		$('#txtSalePrice').val('');
		$('#txtShortCode').val('');
		$('#uom_dropdown').val('');
	}

	var saveAccount = function( accountObj ) {
		$.ajax({
			url : base_url + 'index.php/account/save',
			type : 'POST',
			data : { 'accountDetail' : accountObj },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'false') {
					alert('An internal error occured while saving account. Please try again.');
				} else {
					alert('Account saved successfully.');
					$('#AccountAddModel').modal('hide');
					fetchAccount();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

var fetchAccount = function() {

		$.ajax({
			url : base_url + 'index.php/account/fetchall',
			type : 'POST',
			data : { 'active' : 1,'typee':'purchase'},
			dataType : 'JSON',
			success : function(data) {
				if (data === 'false') {
					alert('No data found');
				} else {
					populateDataAccount(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var fetchItems = function() {
		$.ajax({
			url : base_url + 'index.php/item/fetchall',
			type : 'POST',
			data : { 'active' : 1 },
			dataType : 'JSON',
			success : function(data) {
				if (data === 'false') {
					alert('No data found');
				} else {
					populateDataItem(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateDataAccount = function(data) {
		$("#party_dropdown11").empty();
		
		$.each(data, function(index, elem){
			var opt="<option value='"+elem.party_id+"' >" +  elem.name + "</option>";
			$(opt).appendTo('#party_dropdown11');
		});
	}
	var last_5_srate = function(crit) {
		$("table .rate_tbody tr").remove();
		$.ajax({

			url : base_url + 'index.php/saleorder/last_5_srate',
			type : 'POST',
			data : { 'company_id': $('#cid').val(),'etype':'sale','date':$('#current_date').val()},
			dataType : 'JSON',
			
			success : function(data) {
				console.log(data);
				//reset table
				if (data === 'false') {
					$('.l5ratesdisp').addClass('disp');
				} else {
					$('.l5ratesdisp').removeClass('disp');
					// $(".rate_tbody").html("");
					
					$.each(data, function(index, elem) {
					appendToTable_last_rate(elem.vrnoa, elem.vrdate, elem.rate,elem.qty);

		});
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var last_stockLocatons = function(item_id) {
		$("table .rate_tbody tr").remove();
		$.ajax({

			url : base_url + 'index.php/saleorder/last_stocklocatons',
			type : 'POST',
			data : { 'item_id' : item_id , 'company_id': $('#cid').val(),'etype':'sale'},
			dataType : 'JSON',
			
			success : function(data) {
				console.log(data);
				//reset table
				if (data === 'false') {
					   $('.laststockLocation_tabledisp').addClass('disp');
				} else {
					   $('.laststockLocation_tabledisp').removeClass('disp');
					// $(".rate_tbody").html("");
					
					$.each(data, function(index, elem) {
					appendToTable_last_stockLocation(elem.location, elem.qty);

		});
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var appendToTable_last_stockLocation = function(location, qty) {
		

		var row = 	"<tr>" +
						"<td class='location numeric' data-title='location' > "+ location +"</td>" +
					 	"<td class='qty numeric' data-title='qty'> "+ qty +"</td>" +
				 	"</tr>";
		
			$(row).appendTo('#laststockLocation_table');
		
	}
		var getcrit = function(){
			var crit ='';
			var accid = $('#party_dropdown11').select2('val');
			var item_id = $('#itemid_dropdown').select2('val');
			// alert(accid);
			// alert(item_id);

			if (item_id !== null){
	             crit +='AND i.item_id in (' + item_id +') ';
	         }
	         if (accid !== null) {
	             crit +='AND m.party_id in (' + accid +') '
	         }
	        crit += 'AND m.stid <>0 ';

	        return crit;
		}
	var appendToTable_last_rate = function(vrnoa, vrdate, rate,stock) {
		

		var row = 	"<tr>" +
						"<td class='vr# numeric' data-title='Vr#' > "+ vrnoa +"</td>" +
				 		"<td class='vrdatee' data-title='Date'> "+ vrdate +"</td>" +
					 	"<td class='rate numeric' data-title='Rate'> "+ rate +"</td>" +
					 	"<td class='stock numeric' data-title='stock'> "+ stock +"</td>" +
				 	"</tr>";
		
			$(row).appendTo('#lastrate_table');
		
	}
	var populateDataItem = function(data) {
		$("#itemid_dropdown").empty();
		$("#item_dropdown").empty();

		$.each(data, function(index, elem){
			var opt="<option value='"+elem.item_id+"' data-prate= '"+ elem.cost_price +"' data-uom_item= '"+ elem.uom +"' data-grweight= '"+ elem.grweight +"' >" +  elem.item_des + "</option>";
			 // var = "<option value='" + $item['item_id'] + "' data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>"><?php echo $item['item_des']; ?></option>";
			$(opt).appendTo('#item_dropdown');
			var opt1="<option value='"+elem.item_id+"' data-prate= '"+ elem.cost_price +"' data-uom_item= '"+ elem.uom +"' data-grweight= '"+ elem.grweight +"' >" +  elem.item_id + "</option>";
			 // var = "<option value='" + $item['item_id'] + "' data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>"><?php echo $item['item_des']; ?></option>";
			$(opt1).appendTo('#itemid_dropdown');

		});
	}
	var getSaveObjectAccount = function() {

		var obj = {
			pid : '20000',
			active : '1',
			name : $.trim($('#txtAccountName').val()),
			level3 : $.trim($('#txtLevel3').val()),
			dcno : $('#txtVrnoa').val(),
			etype : 'purchase',
			uid : $.trim($('#uid').val()),
			company_id : $.trim($('#cid').val()),
		};

		return obj;
	}
var getSaveObjectItem = function() {
		
		var itemObj = {
			item_id : 20000,
			active : '1',
			open_date : $.trim($('#current_date').val()),
			catid : $('#category_dropdown').val(),
			subcatid : $.trim($('#subcategory_dropdown').val()),
			bid : $.trim($('#brand_dropdown').val()),
			barcode : $.trim($('#txtBarcode').val()),
			description : $.trim($('#txtItemName').val()),
			item_des : $.trim($('#txtItemName').val()),
			cost_price : $.trim($('#txtPurPrice').val()),
			srate : $.trim($('#txtSalePrice').val()),
			uid : $.trim($('#uid').val()),
			company_id : $.trim($('#cid').val()),
			short_code : $.trim($('#txtShortCode').val()),
			uom : $.trim($('#uom_dropdown').val()),
		};
		return itemObj;
	}
var populateDataGodowns = function(data) {
        $("#dept_dropdown").empty();
        $.each(data, function(index, elem){
            var opt1="<option value=" + elem.did + ">" +  elem.name + "</option>";
            $(opt1).appendTo('#dept_dropdown');
        });
    }
    var fetchGodowns = function() {
        $.ajax({
            url : base_url + 'index.php/department/fetchalldepartments',
            type : 'POST',
            dataType : 'JSON',
            success : function(data) {
                if (data === 'false') {
                    alert('No data found');
                } else {
                    populateDataGodowns(data);
                }
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }
    var getSaveObjectGodown = function() {
        var obj = {};
        obj.did = 20000;
        obj.name = $.trim($('#txtNameGodownAdd').val());
        obj.description = $.trim($('.page_title').val());
        return obj;
    }
    var saveGodown = function( department ) {
        $.ajax({
            url : base_url + 'index.php/department/savedepartment',
            type : 'POST',
            data : { 'department' : department },
            dataType : 'JSON',
            success : function(data) {

                if (data == "duplicate") {
					$('#txtNameGodownAdd').addClass('inputerror');
					alert('Department already Saved!');
				}
				else{
					if (data.error === 'true') {
						alert('An internal error occured while saving voucher. Please try again.');
					} else {
						alert('Department saved successfully.');
						general.reloadWindow();
					}
				}
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }
    var validateSaveGodown = function() {
        var errorFlag = false;
        var _desc = $.trim($('#txtNameGodownAdd').val());
        $('.inputerror').removeClass('inputerror');
        if ( !_desc ) {
            $('#txtNameGodownAdd').addClass('inputerror');
            errorFlag = true;
        }
        return errorFlag;
    }
	var validateSaveItem = function() {

		var errorFlag = false;
		// var _barcode = $('#txtBarcode').val();
		var _desc = $.trim($('#txtItemName').val());
		var cat = $.trim($('#category_dropdown').val());
		var subcat = $('#subcategory_dropdown').val();
		var brand = $.trim($('#brand_dropdown').val());
		var uom_ = $.trim($('#uom_dropdown').val());
		var shortcode_ = $.trim($('#txtShortCode').val());

		// remove the error class first
		
		$('.inputerror').removeClass('inputerror');
		if ( !shortcode_ ) {
			$('#txtShortCode').addClass('inputerror');
			errorFlag = true;
		}
		if ( !uom_ ) {
			$('#uom_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		if ( _desc === '' || _desc === null ) {
			$('#txtItemName').addClass('inputerror');
			errorFlag = true;
		}
		if ( !cat ) {
			$('#category_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		if ( !subcat ) {
			$('#subcategory_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		if ( !brand ) {
			$('#brand_dropdown').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}
	var validateSaveAccount = function() {

		var errorFlag = false;
		var partyEl = $('#txtAccountName');
		var deptEl = $('#txtLevel3');

		// remove the error class first
		$('.inputerror').removeClass('inputerror');

		if ( !partyEl.val() ) {
			$('#txtAccountName').addClass('inputerror');
			errorFlag = true;
		}
		if ( !deptEl.val() ) {
			deptEl.addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}
	


	var save = function(purchase) {
		general.disableSave();
		
		$.ajax({
			url : base_url + 'index.php/purchase/saveItemConversion',
			type : 'POST',
			data : { 'stockmain' : purchase.stockmain, 'stockdetail' : purchase.stockdetail, 'vrnoa' : purchase.vrnoa, 'ledger' : purchase.ledger ,'voucher_type_hidden':$('#voucher_type_hidden').val(),'etype':'item_conversion' , 'vrdate' : $('#vrdate').val()},
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'date close') {
					alert('Sorry! you can not insert update in close date................');
				}else if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					// alert('Voucher saved successfully.');
					// general.reloadWindow();
					// general.ShowAlert('Save');
					var printConfirmation = confirm('Voucher saved!\nWould you like to print the invoice as well?');
					if (printConfirmation === true) {
						 window.open(base_url + 'application/views/reportprints/conversionreport.php', "Recipe Costing", 'width=1210, height=842');
						//Print_Voucher(0,'lg','');
					} else {
						general.reloadWindow();
					}
				}
				general.enableSave();
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var Print_Voucher = function(hd,prnt,wrate) {
		if ( $('.btnSave').data('printbtn')==0 ){
				alert('Sorry! you have not print rights..........');
		}else{
			var etype=  'item_conversion';
			var vrnoa = $('#txtVrnoa').val();
			var company_id = $('#cid').val();
			var user = $('#uname').val();
			// var hd = $('#hd').val();
			var pre_bal_print = ($(settings.switchPreBal).bootstrapSwitch('state') === true) ? '1' : '0';
			
			var url = base_url + 'index.php/doc/print_voucheritemconversition/' + etype + '/' + vrnoa + '/' + company_id + '/' + '-1' + '/' + user + '/' + pre_bal_print + '/' + hd + '/' + prnt + '/' + wrate;
			// var url = base_url + 'index.php/doc/CashVocuherPrintPdf/' + etype + '/' + dcno   + '/' + companyid + '/' + '-1' + '/' + user;
			window.open(url);
		}
	}

	var fetch = function(vrnoa) {

		$.ajax({
			url : base_url + 'index.php/purchase/fetchitemc',
			type : 'POST',
			data : { 'vrnoa' : vrnoa , 'company_id': $('#cid').val()},
			dataType : 'JSON',
			success : function(data) {
				console.log(data);

				resetFields();
				$('#txtOrderNo').val('');
				if (data === 'false') {
                    $('#txtVrnoaHidden').val($('#txtMaxVrnoaHidden').val());
                    $('#voucher_type_hidden').val('new');
					alert('No data found.');
				} else {
					populateData(data);
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var fetchReports = function (from, to,companyid,etype,uid) {


	    $('.grand-total').html(0.00);

	    
	        $('#saleRows').empty();
	    
	    $.ajax({
	            url: base_url + "index.php/purchase/fetchreportdataitemcmain",
	            data: { 'from' : from, 'to' : to, 'company_id':companyid, 'etype':etype, 'uid':uid},
	            type: 'POST',
	            dataType: 'JSON',
	            beforeSend: function () {
	                console.log(this.data);
	             },
	            complete: function () { },
	            success: function (result) {

	         

	                if (result.length !== 0 || result.length !== '' || result !== '' || typeof result[index] !== 'undefined') {
	                    // $('#chart_tabs').addClass('disp');
	                    // $('.tableDate').removeClass('disp');

	                    var th;
	                    var td1;

	                    
	                  
	                        
	                    
	                    // var template = Handlebars.compile( th );
	                    // var html = template({});

	                    // $('.dthead').html( html );


	                        // $("#datatable_example_wrapper").fadeIn();

	                        // $(".cols_options").show();

	                     

	                            var saleRows = $("#saleRows");

	                            $.each(result, function (index, elem) {
	                                // alert(elem.name);
	                                // alert(elem.VRNOA);
	                                //debugger

	                                var obj = { };

	                                obj.SERIAL = saleRows.find('tr').length+1;
	                                obj.VRNOA = elem.vrnoa;
	                                obj.VRDATE = (elem.vrdate) ? elem.vrdate.substring(0,10) : "-";
	                                obj.REMARKS = (elem.remarks) ? elem.remarks : "-";
									obj.DESCRIPTION = (elem.item_des) ? elem.item_des : "-";	                               
	                                obj.UOM = (elem.uom) ? elem.uom : "-";
	                                obj.GODOWN = (elem.dept_name) ? elem.dept_name : "-";
	                                obj.QTY = (elem.qty) ? Math.abs(elem.qty) : "-";
	                                obj.RATE = (elem.rate) ? elem.rate : "-";
	                                obj.TYPE = (elem.detype) ? elem.detype.replace("_"," ") : "-";
	                                // obj.PCS = (elem.namount) ? Math.abs(elem.namount) : "-";
	                                // obj.RATE = (elem.rate) ? elem.rate : "-";
	                                obj.NETAMOUNT = (elem.amount) ? elem.amount : "-";
	                                
	                        
	                                // obj.VRNOA1 = prevVoucherMatch;

	                              

	                                // Add the item of the new voucher
	                                td1 = $("#voucher-item-template").html();
	                                var source   = td1;
	                                var template = Handlebars.compile(source);
	                                var html = template(obj);
	                                
	                                saleRows.append(html);

	                                
	                            
	                            });
	                            // $('.grand-total').html(grandTotal);

	                }else{
	                	alert('No result Found');
	                }


	                // bindGrid();
	            },

	            error: function (result) {
	                alert("Error:" + result);
	            }
	        });

	}
	


	var populateData = function(data) {

		$('#txtVrno').val(data[0]['vrno']);
		$('#txtVrnoHidden').val(data[0]['vrno']);
		$('#txtVrnoaHidden').val(data[0]['vrnoa']);
		$('#current_date').datepicker('update', data[0]['vrdate'].substr(0, 10));
		$('#vrdate').datepicker('update', data[0]['vrdate'].substr(0, 10));
		$('#party_dropdown11').select2('val', data[0]['party_id']);
		$('#txtInvNo').val(data[0]['inv_no']);
		$('#due_date').datepicker('update', data[0]['due_date'].substr(0, 10));
		$('#receivers_list').val(data[0]['received_by']);
		$('#transporter_dropdown').select2('val', data[0]['transporter_id']);
		$('#txtRemarks').val(data[0]['remarks']);
		$('#txtNetAmount').val(data[0]['namount']);
		$('#txtOrderNo').val(data[0]['order_no']);
		
		$('#txtDiscount').val(data[0]['discp']);
		$('#txtExpense').val(data[0]['exppercent']);
		$('#txtExpAmount').val(data[0]['expense']);
		$('#txtTax').val(data[0]['taxpercent']);
		$('#txtTaxAmount').val(data[0]['tax']);
		$('#txtDiscAmount').val(data[0]['discount']);
		$('#user_dropdown').val(data[0]['uid']);
		$('#txtPaid').val(data[0]['paid']);

		// $('#dept_dropdown').select2('val', data[0]['godown_id']);
		$('#voucher_type_hidden').val('edit');		
		$('#user_dropdown').val(data[0]['uid']);
		$('#txtcustomerName').val(data[0]['customer_name']);
		$('#txtcustomerMob').val(data[0]['mobile']);
		$('#posting_date').val(data[0]['date_time'].substr(0, 10));
		
		$.each(data, function(index, elem) {
			// alert(elem.detype);
			if (elem.detype == 'stock_in') {

				appendToTable('', elem.item_name, elem.item_id,elem.dept_name,elem.godown_id,elem.uom,Math.abs(elem.s_qty),0,0, elem.s_rate, elem.s_net,elem.weight);
				calculateLowerTotal(Math.abs(elem.s_qty), elem.s_net,elem.weight);
			}else{
					getItemInfo(elem.item_id,'');
				// $('#itemid_dropdownItemC').select2('val',elem.item_id);
				// $('#item_dropdownItemC').select2('val',elem.item_id);
				 $('#dept_dropdownitemc').select2('val',elem.godown_id);
				 $('#txtWeightItemC').val(elem.weight);
				 $('#txtQtyItemC').val(Math.abs(elem.s_qty));
				 $('#txtPRateItemC').val(elem.s_rate);
				 $('#txtAmountItemC').val(elem.s_net);
			}
			// alert(elem.s_rate);
		});
	}

	// gets the max id of the voucher
	var getMaxVrno = function() {

		$.ajax({

			url : base_url + 'index.php/purchase/getmaxvrnoitemc',
			type : 'POST',
			data : {'company_id': $('#cid').val()},
			dataType : 'JSON',
			success : function(data) {

				$('#txtVrno').val(data);
				$('#txtMaxVrnoHidden').val(data);
				$('#txtVrnoHidden').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var getMaxVrnoa = function() {

		$.ajax({

			url : base_url + 'index.php/purchase/getmaxvrnoaitemc',
			type : 'POST',
			data : {'company_id': $('#cid').val()},
			dataType : 'JSON',
			success : function(data) {

				$('#txtVrnoa').val(data);
				$('#txtMaxVrnoaHidden').val(data);
				$('#txtVrnoaHidden').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var validateSingleProductAdd = function() {


		var errorFlag = false;
		var itemEl = $('#item_dropdown');
		var depEl = $('#dept_dropdown');
		var qtyEl = $('#txtQty');
		var rateEl = $('#txtPRate');

		// remove the error class first
		$('.inputerror').removeClass('inputerror');

		if ( !itemEl.val() ) {
			itemEl.addClass('inputerror');
			errorFlag = true;
		}
		if ( !qtyEl.val() ) {
			qtyEl.addClass('inputerror');
			errorFlag = true;
		}
		// if ( !rateEl.val() ) {
		// 	rateEl.addClass('inputerror');
		// 	errorFlag = true;
		// }
		if ( !depEl.val() ) {
			depEl.addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var appendToTable = function(srno, item_desc, item_id,dep_desc,dep_id,uom,qty,discp,discount, rate, amount , weight) {
		var pid = '';
		$('#item_dropdown option').each(function() { if ($(this).val().trim().toLowerCase() == item_id) pid = $(this).data('grweight');  });
		var srno = $('#purchase_table tbody tr').length + 1;
		var row = 	"<tr>" +
						"<td class='srno numeric text-left' data-title='Sr#' > "+ srno +"</td>" +
				 		"<td class='item_desc' data-title='Description' data-item_id='"+ item_id +"' data-item_weight='"+ pid +"'> "+ item_desc +"</td>" +
				 		"<td class='godown_desc' data-title='Description' data-godown_id='"+ dep_id +"'> "+ dep_desc +"</td>" +
				 		"<td class='uom' data-title='Description' > "+ uom +"</td>" +
				 		"<td class='qty numeric text-right' data-title='Qty'>  "+ qty +"</td>" +
				 		"<td class='weight numeric text-right' data-title='Dis%'>  "+ weight +"</td>" +
				 		"<td class='rate numeric text-right hide' data-title='Rate'> "+ rate +"</td>" +
				 		// "<td class='discp numeric text-right' data-title='Dis%'>  "+ discp +"</td>" +
				 		// "<td class='discount numeric text-right' data-title='Discount'>  "+ discount +"</td>" +
					 	"<td class='amount numeric text-right hide' data-title='Amount' > "+ amount +"</td>" +
					 	"<td><a href='' class='btn btn-primary btnRowEdit'><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td>" +
				 	"</tr>";
		$(row).appendTo('#purchase_table');
	}

	var getPartyId = function(partyName) {
		var pid = "";
		$('#party_dropdown11 option').each(function() { if ($(this).text().trim().toLowerCase() == partyName) pid = $(this).val();  });
		return pid;
	}



	var getSaveObject = function() {

		var ledgers = [];
		var stockmain = {};
		var stockdetail = [];

		stockmain.vrno = $('#txtVrnoHidden').val();
		stockmain.vrnoa = $('#txtVrnoaHidden').val();
		stockmain.vrdate = $('#current_date').val();
		stockmain.party_id = $('#party_dropdown11').val();
		stockmain.bilty_no = $('#txtInvNo').val();
		stockmain.bilty_date = $('#due_date').val();
		stockmain.received_by = $('#receivers_list').val();
		stockmain.transporter_id = $('#transporter_dropdown').val();
		stockmain.remarks = $('#txtRemarks').val();
		stockmain.etype = 'item_conversion';
		stockmain.namount = $('#txtNetAmount').val();
		stockmain.order_vrno = $('#txtOrderNo').val();
		stockmain.discp = $('#txtDiscount').val();
		stockmain.discount = $('#txtDiscAmount').val();
		stockmain.expense =$('#txtExpAmount').val();
		stockmain.exppercent = $('#txtExpense').val();
		stockmain.tax = $('#txtTaxAmount').val();
		stockmain.taxpercent = $('#txtTax').val();
		stockmain.paid = $('#txtPaid').val();
		stockmain.customer_name = $('#txtcustomerName').val();
		stockmain.mobile = $('#txtcustomerMob').val();

		stockmain.uid = $('#uid').val();
		stockmain.company_id = $('#cid').val();
		stockmain.item_discount = $('.txtTotalDiscount').val();



		var prd = '';
		var sd = {};
		sd.stid = '';
		sd.item_id = $('#itemid_dropdownItemC').val();
		sd.qty = -$('#txtQtyItemC').val();
		sd.weight = -$('#txtWeightItemC').val();
		sd.rate = $('#txtPRateItemC').val();
		sd.amount = Math.abs(sd.qty)*sd.rate ;
		sd.netamount = Math.abs(sd.qty)*sd.rate; 
		sd.etype = 'stock_out';
		sd.godown_id = $('#dept_dropdownitemc').val();
		// prd += $.trim($(elem).find('td.item_desc').text()) + ', ' + Math.abs(sd.qty) + '@' + sd.netamount/Math.abs(sd.qty) + ', ' ;
		stockdetail.push(sd);

		$('#purchase_table').find('tbody tr').each(function( index, elem ) {
			var sd = {};
			sd.stid = '';
			sd.item_id = $.trim($(elem).find('td.item_desc').data('item_id'));
			sd.uom = $.trim($(elem).find('td.uom').text());
			sd.godown_id = $.trim($(elem).find('td.godown_desc').data('godown_id'));
			sd.qty = Number($.trim($(elem).find('td.qty').text()));
			sd.weight = Number($.trim($(elem).find('td.weight').text()));
			// sd.discount = $.trim($(elem).find('td.discp').text());
			// sd.damount /= $.trim($(elem).find('td.discount').text());
			sd.rate = $.trim($(elem).find('td.rate').text());
			sd.amount = Math.abs(sd.qty)*sd.rate ;
			sd.netamount = $.trim($(elem).find('td.amount').text());
			sd.etype = 'stock_in';
			prd += $.trim($(elem).find('td.item_desc').text()) + ', ' + Math.abs(sd.qty) + '@' + sd.netamount/Math.abs(sd.qty) + ', ' ;
			stockdetail.push(sd);
		});

		///////////////////////////////////////////////////////////////
		//// for over all voucher
		///////////////////////////////////////////////////////////////
		
		var pledger = {};
		pledger.pledid = '';
		pledger.pid = $('#party_dropdown11').val();
		pledger.description = prd;
		pledger.remarks =  $('#txtRemarks').val();
		pledger.date = $('#current_date').val();
		pledger.debit = 0;
		pledger.credit = $('#txtNetAmount').val();
		pledger.dcno = $('#txtVrnoaHidden').val();
		pledger.invoice = $('#txtVrnoaHidden').val();
		pledger.etype = 'item_conversion';
		pledger.pid_key = $('#purchaseid').val();
		pledger.uid = $('#uid').val();
		pledger.company_id = $('#cid').val();
		pledger.isFinal = 0;	
		ledgers.push(pledger);
 		
 		var pur_amount= parseFloat($('.txtTotalAmount').text()) + parseFloat($('.txtTotalDiscount').text());
		var pledger = {};
		pledger.pledid = '';
		pledger.pid = $('#purchaseid').val();
		pledger.description = $('#party_dropdown11').find('option:selected').text();
		pledger.remarks =  $('#txtRemarks').val();
		pledger.date = $('#current_date').val();
		pledger.debit = pur_amount;
		pledger.credit = 0;
		pledger.dcno = $('#txtVrnoaHidden').val();
		pledger.invoice = $('#txtInvNo').val();
		pledger.etype = 'item_conversion';
		pledger.pid_key = $('#party_dropdown11').val();
		pledger.uid = $('#uid').val();
		pledger.company_id = $('#cid').val();	
		pledger.isFinal = 0;
		ledgers.push(pledger);

		///////////////////////////////////////////////////////////////
		//// for Discount
		///////////////////////////////////////////////////////////////
		if ($('.txtTotalDiscount').text() != 0 ) {
			pledger = undefined;
			var pledger = {};
			pledger.etype = 'item_conversion';
			pledger.description = $('#party_dropdown11 option:selected').text() + '.';
			pledger.remarks =  $('#txtRemarks').val();
			// pledger.description = 'Purchase Head';
			pledger.dcno = $('#txtVrnoaHidden').val();
			pledger.invoice = $('#txtVrnoaHidden').val();
			pledger.pid = $('#itemdiscountid').val();
			pledger.date = $('#current_date').val();
			pledger.debit = 0;
			pledger.credit = $('.txtTotalDiscount').text();
			pledger.isFinal = 0;
			pledger.uid = $('#uid').val();
			pledger.company_id = $('#cid').val();	
			pledger.pid_key = $('#party_dropdown11').val();								

			ledgers.push(pledger);
		}

		if ($('#txtDiscAmount').val() != 0 ) {
			pledger = undefined;
			var pledger = {};
			pledger.etype = 'item_conversion';
			pledger.description = $('#party_dropdown11 option:selected').text() + '.';
			pledger.remarks =  $('#txtRemarks').val();
			// pledger.description = 'Purchase Head';
			pledger.dcno = $('#txtVrnoaHidden').val();
			pledger.invoice = $('#txtVrnoaHidden').val();
			pledger.pid = $('#discountid').val();
			pledger.date = $('#current_date').val();
			pledger.debit = 0;
			pledger.credit = $('#txtDiscAmount').val();
			pledger.isFinal = 0;
			pledger.uid = $('#uid').val();
			pledger.company_id = $('#cid').val();	
			pledger.pid_key = $('#party_dropdown11').val();								

			ledgers.push(pledger);
		}		
		if ($('#txtTaxAmount').val() != 0 ) {
			pledger = undefined;
			var pledger = {};
			pledger.etype = 'item_conversion';
			pledger.description = $('#party_dropdown11 option:selected').text() + '.';
			pledger.remarks =  $('#txtRemarks').val();
			// pledger.description = 'Purchase Head';
			pledger.dcno = $('#txtVrnoaHidden').val();
			pledger.invoice = $('#txtVrnoaHidden').val();
			pledger.pid = $('#taxid').val();
			pledger.date = $('#current_date').val();
			pledger.debit = $('#txtTaxAmount').val();
			pledger.credit = 0;
			pledger.isFinal = 0;
			pledger.uid = $('#uid').val();
			pledger.company_id = $('#cid').val();	
			pledger.pid_key = $('#party_dropdown11').val();
			ledgers.push(pledger);
		}
		if ($('#txtExpAmount').val() != 0 ) {
			pledger = undefined;
			var pledger = {};
			pledger.etype = 'item_conversion';
			pledger.description = $('#party_dropdown11 option:selected').text() + '.';
			pledger.remarks =  $('#txtRemarks').val();
			// pledger.description = 'Purchase Head';
			pledger.dcno = $('#txtVrnoaHidden').val();
			pledger.invoice = $('#txtVrnoaHidden').val();
			pledger.pid = $('#expenseid').val();
			pledger.date = $('#current_date').val();
			pledger.debit = $('#txtExpAmount').val();
			pledger.credit = 0;
			pledger.isFinal = 0;
			pledger.uid = $('#uid').val();
			pledger.company_id = $('#cid').val();	
			pledger.pid_key = $('#party_dropdown11').val();
			ledgers.push(pledger);
		}
		if ($('#txtPaid').val() != 0 ) {
			pledger = undefined;
			var pledger = {};
			pledger.etype = 'purchase';
			pledger.description = $('#party_dropdown11 option:selected').text() + '.';
			pledger.remarks =  $('#txtRemarks').val();
			// pledger.description = 'Purchase Head';
			pledger.dcno = $('#txtVrnoaHidden').val();
			pledger.invoice = $('#txtVrnoaHidden').val();
			pledger.pid = $('#cashid').val();
			pledger.date = $('#current_date').val();
			pledger.debit = 0;
			pledger.credit = $('#txtPaid').val();
			pledger.isFinal = 0;
			pledger.uid = $('#uid').val();
			pledger.company_id = $('#cid').val();	
			pledger.pid_key = $('#party_dropdown11').val();
			ledgers.push(pledger);

			pledger = undefined;
			var pledger = {};
			pledger.etype = 'purchase';
			pledger.description =  'Cash Paid';
			pledger.remarks =  $('#txtRemarks').val();
			// pledger.description = 'Purchase Head';
			pledger.dcno = $('#txtVrnoaHidden').val();
			pledger.invoice = $('#txtVrnoaHidden').val();
			pledger.pid = $('#party_dropdown11').val();
			pledger.date = $('#current_date').val();
			pledger.debit = $('#txtPaid').val();
			pledger.credit = 0;
			pledger.isFinal = 0;
			pledger.uid = $('#uid').val();
			pledger.company_id = $('#cid').val();	
			pledger.pid_key = $('#cashid').val();
			ledgers.push(pledger);

		}
		var data = {};
		data.stockmain = stockmain;
		data.stockdetail = stockdetail;
		data.ledger = ledgers;
		data.vrnoa = $('#txtVrnoaHidden').val();

		return data;
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		var itemEl = $('#item_dropdownItemC');
		var deptEl = $('#dept_dropdownitemc');
		var qtyEl = $('#txtQtyItemC');
		var rateEl = $('#txtPRateItemC');

		// remove the error class first
		$('.inputerror').removeClass('inputerror');

		if ( !deptEl.val() ) {
			deptEl.addClass('inputerror');
			errorFlag = true;
		}
		if ( !itemEl.val() ) {
			$('#item_dropdownItemC').addClass('inputerror');
			errorFlag = true;
		}
		if ( !qtyEl.val() ) {
			$('#txtQtyItemC').addClass('inputerror');
			errorFlag = true;
		}
		// if ( !rateEl.val() ) {
		// 	$('#txtPRateItemC').addClass('inputerror');
		// 	errorFlag = true;
		// }
		

		return errorFlag;
	}
		// checks for the empty fields
	var validateSearch = function() {

		var errorFlag = false;
		var fromEl = $('#from_date');
		var toEl = $('#to_date');

		// remove the error class first
		$('.inputerror').removeClass('inputerror');

		if ( !toEl.val() ) {
			toEl.addClass('inputerror');
			errorFlag = true;
		}
		if ( !fromEl.val() ) {
			$('#from_date').addClass('inputerror');
			errorFlag = true;
		}
		

		return errorFlag;
	}
	

	var deleteVoucher = function(vrnoa) {

		$.ajax({
			url : base_url + 'index.php/purchase/delete',
			type : 'POST',
			data : { 'chk_date' : $('#current_date').val(), 'vrdate' : $('#vrdate').val(), 'vrnoa' : vrnoa , 'etype':'item_conversion','company_id':$('#cid').val() },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'date close') {
					alert('Sorry! you can not delete in close date................');
				}else if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	
	var calculateLowerTotal = function(qty, amount, weight) {

		var _qty = getNumText($('.txtTotalQty'));
		var _weight = getNumText($('.txtTotalWeight'));
		var _discount = getNumText($('.txtTotalDiscount'));
		var _amnt = getNumText($('.txtTotalAmount'));

		var _discp = getNumVal($('#txtDiscount'));
		var _disc = getNumVal($('#txtDiscAmount'));
		var _tax = getNumVal($('#txtTax'));
		var _taxamount = getNumVal($('#txtTaxAmount'));
		var _expense = getNumVal($('#txtExpAmount'));
		var _exppercent = getNumVal($('#txtExpense'));
		var _amountless = getNumVal($('#txtPaid'));

		var tempQty = parseFloat(_qty) + parseFloat(qty);
		$('.txtTotalQty').text(tempQty);

		var tempWeight = parseFloat(_weight) + parseFloat(weight);
		$('.txtTotalWeight').text(tempWeight);

		// var tempdiscount = parseFloat(parseFloat(_discount) + parseFloat(discount)).toFixed(2);
		// $('.txtTotalDiscount').text(tempdiscount);

		var tempAmnt = (parseFloat(_amnt) + parseFloat(amount)).toFixed(2);
		// $('#txtTotalAmount').text(tempAmnt);
		$('.txtTotalAmount').text(tempAmnt);

		var net = (parseFloat(tempAmnt) - parseFloat(_disc) + parseFloat(_taxamount) + parseFloat(_expense)).toFixed(2) ;
		$('#txtNetAmount').val(net);

	}
	var getNumText = function(el){
		return isNaN(parseFloat(el.text())) ? 0 : parseFloat(el.text());
	}
	var getNumVal = function(el){
		return isNaN(parseFloat(el.val())) ? 0 : parseFloat(el.val());
	}

	var calculateUpperSum = function() {

		var _qty = getNumVal($('#txtQty'));
		var _discount = getNumVal($('#txtDiscount1_tbl'));
		var _prate = getNumVal($('#txtPRate'));
		
		var _tempAmnt =  ((parseFloat(_prate)*parseFloat(_qty))-parseFloat(_discount)).toFixed(2);			
		$('#txtAmount').val(_tempAmnt);
	}
	var calculateUpperSumItemC = function() {

		var _qty = getNumVal($('#txtQtyItemC'));
		var _discount = getNumVal($('#txtDiscount1_tbl'));
		var _prate = getNumVal($('#txtPRateItemC'));
		
		var _tempAmnt =  ((parseFloat(_prate)*parseFloat(_qty))-parseFloat(_discount)).toFixed(2);			
		$('#txtAmountItemC').val(_tempAmnt);
	}
	

	var fetchThroughPO = function(po) {

		$.ajax({

			url : base_url + 'index.php/purchaseorder/fetch',
			type : 'POST',
			data : { 'vrnoa' : po , 'company_id': $('#cid').val()},
			dataType : 'JSON',
			success : function(data) {

				resetFields();
				if (data === 'false') {
					alert('No data found.');
				} else {
					populatePOData(data);
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populatePOData = function(data) {

		$('#current_date').datepicker('update', data[0]['vrdate'].substr(0, 10));
		$('#party_dropdown11').select2('val', data[0]['party_id']);
		$('#txtInvNo').val(data[0]['inv_no']);
		// $('#due_date').datepicker('update', data[0]['due_date'].substr(0, 10));
		$('#receivers_list').val(data[0]['received_by']);
		$('#transporter_dropdown').select2('val', data[0]['transporter_id']);
		$('#txtRemarks').val(data[0]['remarks']);
		$('#txtNetAmount').val(data[0]['namount']);
		// $('#txtOrderNo').val(data[0]['ordno']);
		
		$('#txtDiscount').val(data[0]['discp']);
		$('#txtExpense').val(data[0]['exppercent']);
		$('#txtExpAmount').val(data[0]['expense']);
		$('#txtTax').val(data[0]['taxpercent']);
		$('#txtTaxAmount').val(data[0]['tax']);
		$('#txtDiscAmount').val(data[0]['discount']);
		$('#user_dropdown').val(data[0]['uid']);
		$('#txtPaid').val(data[0]['paid']);

		$('#dept_dropdown').select2('val', data[0]['godown_id']);
		$('#voucher_type_hidden').val('edit');		
		$('#user_dropdown').val(data[0]['uid']);
		$.each(data, function(index, elem) {
			appendToTable('', elem.item_name, elem.item_id, elem.qty,elem.discp1,elem.discount1, elem.rate, elem.amount);
			calculateLowerTotal(elem.qty, elem.amount, elem.discount1);
		});
	}

	var resetFields = function() {

		$('#current_date').datepicker('update', new Date());
		$('#party_dropdown11').select2('val', '');
		$('#txtInvNo').val('');
		$('#due_date').datepicker('update', new Date());
		$('#receivers_list').val('');
		$('#transporter_dropdown').select2('val', '');
		$('#txtRemarks').val('');
		$('#txtNetAmount').val('');		
		$('#txtDiscount').text('');
		$('#txtExpense').text('');
		$('#txtExpAmount').text('');
		$('#txtTax').text('');
		$('#txtTaxAmount').text('');
		$('#txtDiscAmount').text('');

		$('#txtTotalAmount').text('');
		$('#txtTotalQty').val('');
		$('#txtTotalWeight').val('');
		$('#dept_dropdown').select2('val', '');
		$('#voucher_type_hidden').val('new');
		$('table tbody tr').remove();
		$('.txtTotalQty').text('');
		$('.txtTotalDiscount').text('');
		$('.txtTotalDiscp').text('');
		$('.txtTotalRate').text('');
		$('.txtTotalAmount').text('');
		$('.txtTotalWeight').text('');
		$('#posting_date').val('');

        $('#itemid_dropdownItemC').select2('val', '');
        $('#item_dropdownItemC').select2('val', '');
        $('#s2id_dept_dropdownitemc').select2('val', '');
        $('#dept_dropdownitemc').select2('val', '');
        $('#txtQtyItemC').val('');
        $('#txtPRateItemC').val('');
        $('#txtAmountItemC').val('');
	}

	 var paging = function (total)
	{
    	$("#paging").smartpaginator({
        	totalrecords: parseInt(total),
        	recordsperpage: 10,
        	datacontainer: 'tbody',
        	dataelement: 'tr',
        	theme: 'custom',
        	onchange: onChange
    	});
	}
	 var onChange = function (newPageValue)
    {
        $.ajax({
            type: "POST",
            url: base_url + 'index.php/item/paging',
            data: {
                page_number: newPageValue,
                search: $.trim($("#txtItemSearch").val())
            }
        }).done(function (result) {

            var total = "";
            var jsonR = JSON.stringify($.parseJSON(result)).replace(/\\n/g, "\\n")
                                        .replace(/\\'/g, "\\'")
                                        .replace(/\\"/g, '\\"')
                                        .replace(/\\&/g, "\\&")
                                        .replace(/\\r/g, "\\r")
                                        .replace(/\\t/g, "\\t")
                                        .replace(/\\b/g, "\\b")
                                        .replace(/\\f/g, "\\f");
            var json = $.parseJSON(jsonR);
            total = json.total;

            if (json.records != false) {
                var html = "";
                $.each(json.records, function (index, value) {
                    html += "<tr id='" + value.item_id + "'>";
                    html += "<td>"+value.item_id+"<input type='hidden' name='hfModalitemId' value='" + value.item_id + "'></td>";
                    html += "<td class='tdItemCode'><b>" + value.item_code + "<b></td>";
                    html += "<td class='tdDescription'><b>" + value.item_des + "</b></td>";
                    html += "<td class='tdItemUom'><b>" + value.uom + "</b></td>";
                    html += "<td class='tdPRate hide'>" + value.cost_price + "</td>";
                    html += "<td class='tdGrWeight hide'>" + value.grweight + "</td>";
                    html += "<td class='tdStQty hide'>" + value.stqty + "</td>";
                    html += "<td class='tdStWeight hide'>" + value.stweight + "</td>";
                    html += "<td class='tdSrate1 hide'>" + value.srate1 + "</td>";
                    html += "<td class='tdSrate hide'>" + value.srate + "</td>";
                    html += "<td class='tdUItem hide'>" + value.uitem_des + "</td>";
                    html += "<td><a href='#' data-dismiss='modal' class='btn btn-primary populateItem'><i class='fa fa-search'></i></a></td>"
                    html += "</tr>";
                });

                $("#divNoRecord").removeClass('divNoRecord');
                $("#divNoRecord").html('');
                $("#tbItems > tbody").html('');
                $("#tbItems > tbody").append(html);

                $('.modal-lookup .populateItem').on('click', function()
                {	
                	var hiddenFieldValue = $('#hiddenFieldValue').val();
                    var itemId = $(this).closest('tr').find('input[name=hfModalitemId]').val();
                    var itemDescription = $(this).closest('tr').find('.tdDescription').text();
                    var uom = $(this).closest('tr').find('.tdItemUom').text();
                    var pRate = $(this).closest('tr').find('.tdPRate').text();
                    var grWeight = $(this).closest('tr').find('.tdGrWeight').text();
                    var stQty = $(this).closest('tr').find('.tdStQty').text();
                    var stWeight = $(this).closest('tr').find('.tdStWeight').text();
                    var uItemName = $(this).closest('tr').find('.tdUItem').text();
                    var deduction = $(this).closest('tr').find('.tdSrate1').text();
                    var srate = $(this).closest('tr').find('.tdSrate').text();
                    var itemCode = $(this).closest('tr').find('.tdItemCode').text();

                    if(hiddenFieldValue == 'it_add'){
	                    var itemCodeOption = "<option value='" + itemId + "' data-uom_item='" + uom + "' data-prate='" + pRate + "' data-grweight='" + grWeight + "' data-stqty='" + stQty + "' data-stweight='" + stWeight + "' data-uitem_des='" + uItemName + "' data-srate='" + srate + "' data-deduction='" + deduction + "' >" + itemCode + "</option>";
	                    var itemOption = "<option value='" + itemId + "'  data-uom_item='" + uom + "' data-prate='" + pRate + "' data-grweight='" + grWeight + "' data-stqty='" + stQty + "' data-stweight='" + stWeight + "'  data-uitem_des='" + uItemName + "' data-srate='" + srate + "' data-deduction='" + deduction + "' >" + itemDescription + "</option>";
	                    $('#itemid_dropdownItemC').append(itemCodeOption);
	                    $('#item_dropdownItemC').append(itemOption);

	                    $('#itemid_dropdownItemC').val(itemId).trigger('change');
	                    $('#item_dropdownItemC').val(itemId).trigger('change');

	                    //$('#txtPRate').val(parseFloat(srate).toFixed(2));

	                    $('#txtQty').focus();
                	}else{
                		var itemCodeOption = "<option value='" + itemId + "' data-uom_item='" + uom + "' data-prate='" + pRate + "' data-grweight='" + grWeight + "' data-stqty='" + stQty + "' data-stweight='" + stWeight + "' data-uitem_des='" + uItemName + "' data-srate='" + srate + "' data-deduction='" + deduction + "' >" + itemCode + "</option>";
	                    var itemOption = "<option value='" + itemId + "'  data-uom_item='" + uom + "' data-prate='" + pRate + "' data-grweight='" + grWeight + "' data-stqty='" + stQty + "' data-stweight='" + stWeight + "'  data-uitem_des='" + uItemName + "' data-srate='" + srate + "' data-deduction='" + deduction + "' >" + itemDescription + "</option>";
	                    $('#itemid_dropdown').append(itemCodeOption);
	                    $('#item_dropdown').append(itemOption);

	                    $('#itemid_dropdown').val(itemId).trigger('change');
	                    $('#item_dropdown').val(itemId).trigger('change');

	                    //$('#less_txtPRate').val(parseFloat(srate).toFixed(2));

	                    $('#less_txtQty').focus();
                	}
                });

                $("#paging").show();
                $("#tbItems").show();
                if (newPageValue == "1") {
                    paging(total);
                }
            }
            else {
                $("#tbItems > tbody").html('');
                $("#divNoRecord").show();
                $("#divNoRecord").addClass('divNoRecord');
                $("#paging").hide();
                $("#divNoRecord").html('No Record Found');
            }
        });
    }
    var getItemInfo = function(itemId,table)
    {
        $.ajax({
            type: "POST",
            url: base_url + 'index.php/item/getiteminfo',
            data: {
                item_id: itemId
            }
        }).done(function (result) {

            var jsonR = JSON.stringify($.parseJSON(result)).replace(/\\n/g, "\\n")
                                    .replace(/\\'/g, "\\'")
                                    .replace(/\\"/g, '\\"')
                                    .replace(/\\&/g, "\\&")
                                    .replace(/\\r/g, "\\r")
                                    .replace(/\\t/g, "\\t")
                                    .replace(/\\b/g, "\\b")
                                    .replace(/\\f/g, "\\f");
            var json = $.parseJSON(jsonR);

            if (json.records != false)
            {	
            	if (table == 'purchase_table') {
	                $.each(json.records, function (index, value) {
	                    var itemCodeOption = "<option value='" + value.item_id + "' data-uom_item='" + value.uom + "' data-prate='" + value.cost_price + "' data-grweight='" + value.grweight + "' data-stqty='" + value.stqty + "' data-stweight='" + value.stweight + "' data-srate='" + value.srate + "' data-deduction='" + value.deduction + "' >" + value.item_code + "</option>";
	                    var itemOption = "<option value='" + value.item_id + "'  data-uom_item='" + value.uom + "' data-prate='" + value.cost_price + "' data-grweight='" + value.grweight + "' data-stqty='" + value.stqty + "' data-stweight='" + value.stweight + "' data-srate='" + value.srate + "' data-deduction='" + value.deduction + "' >" + value.item_des + "</option>";
	                    $('#itemid_dropdown').append(itemCodeOption);
	                    $('#item_dropdown').append(itemOption);

	                    $('#itemid_dropdown').select2('val', itemId);
	                    $('#item_dropdown').select2('val', itemId);

	                    var grweight = $('#item_dropdown').find('option:selected').data('grweight');
	                    $('#txtGWeight').val(parseFloat(grweight).toFixed());
	                });
				}
				else{
					$.each(json.records, function (index, value) {
	                    var itemCodeOption = "<option value='" + value.item_id + "' data-uom_item='" + value.uom + "' data-prate='" + value.cost_price + "' data-grweight='" + value.grweight + "' data-stqty='" + value.stqty + "' data-stweight='" + value.stweight + "' data-srate='" + value.srate + "' data-deduction='" + value.deduction + "' >" + value.item_code + "</option>";
	                    var itemOption = "<option value='" + value.item_id + "'  data-uom_item='" + value.uom + "' data-prate='" + value.cost_price + "' data-grweight='" + value.grweight + "' data-stqty='" + value.stqty + "' data-stweight='" + value.stweight + "' data-srate='" + value.srate + "' data-deduction='" + value.deduction + "' >" + value.item_des + "</option>";
	                    $('#itemid_dropdownItemC').append(itemCodeOption);
	                    $('#item_dropdownItemC').append(itemOption);

	                    $('#itemid_dropdownItemC').select2('val', itemId);
	                    $('#item_dropdownItemC').select2('val', itemId);

	                    var grweight = $('#item_dropdownItemC').find('option:selected').data('grweight');
	                    $('#less_txtGWeight').val(parseFloat(grweight).toFixed());
	                });
				}
            }
        });
    }
	return {

		init : function() {
			this.bindUI();
			this.bindModalPartyGrid();
			//this.bindModalItemGrid();
			// $('#purchase_tableReport').dataTable();
			// this.bindSearchGrid();
		},

		bindUI : function() {
			
			var self = this;
			$('#GodownAddModel').on('shown.bs.modal',function(e){
                $('#txtNameGodownAdd').focus();
            });
            $('.btnSaveMGodown').on('click',function(e){
                if ( $('.btnSave').data('savegodownbtn')==0 ){
                    alert('Sorry! you have not save departments rights..........');
                }else{
                    e.preventDefault();
                    self.initSaveGodown();
                }
            });
			$('#party_dropdown11').on('change', function() {
				$('.partydisp').removeClass('disp');
				var pid = $(this).val();
				var current_date = $('#current_date').val();
				var city = $(this).find('option:selected').data('city');
				var address = $(this).find('option:selected').data('address');
				var area = $(this).find('option:selected').data('cityarea');
				var contact = $(this).find('option:selected').data('mobile');
	            var credit = $(this).find('option:selected').data('credit');
	            var vendor = $(this).find('option:selected').text();
	            var br = ' <br/> ';
	            $('#party_p').html(   ' Balance is ' + credit +'  <br/>' + city  + '<br/>' + address + ' ' + area + '<br/> ' + contact  );
	        
			});
            $('.btnResetMGodown').on('click',function(){
                
                $('#txtNameGodownAdd').val('');
                
            });

			$('#txtLevel3').on('change', function() {
				
				var level3 = $('#txtLevel3').val();
				$('#txtselectedLevel1').text('');
				$('#txtselectedLevel2').text('');
				if (level3 !== "" && level3 !== null) {
					// alert('enter' + $(this).find('option:selected').data('level2') );	
					$('#txtselectedLevel2').text(' ' + $(this).find('option:selected').data('level2'));
					$('#txtselectedLevel1').text(' ' + $(this).find('option:selected').data('level1'));
				}
			});
			// $('#txtLevel3').select2();
			$('.btnSaveM').on('click',function(e){
				if ( $('.btnSave').data('saveaccountbtn')==0 ){
					alert('Sorry! you have not save accounts rights..........');
				}else{
					e.preventDefault();
					self.initSaveAccount();
				}
			});
			$('.btnResetM').on('click',function(){
				
				$('#txtAccountName').val('');
				$('#txtselectedLevel2').text('');
				$('#txtselectedLevel1').text('');
				$('#txtLevel3').select2('val','');
			});
			$('#AccountAddModel').on('shown.bs.modal',function(e){
				$('#txtAccountName').focus();
			});
			shortcut.add("F3", function() {
    			$('#AccountAddModel').modal('show');
			});

			$('.btnSaveMItem').on('click',function(e){
				if ( $('.btnSave').data('saveitembtn')==0 ){
					alert('Sorry! you have not save item rights..........');
				}else{
					e.preventDefault();
					self.initSaveItem();
				}
			});
			$('.btnResetMItem').on('click',function(){
				resetItemsModalFields();
			});
			
			$('#ItemAddModel').on('shown.bs.modal',function(e){
				$('#txtItemName').focus();
			});
			shortcut.add("F7", function() {
    			$('#ItemAddModel').modal('show');
			});
			$("#switchPreBal").bootstrapSwitch('offText', 'No');
			$("#switchPreBal").bootstrapSwitch('onText', 'Yes');
			$('#voucher_type_hidden').val('new');
			$('.modal-lookup .populateAccount').on('click', function(){
				// alert('dfsfsdf');
				var party_id = $(this).closest('tr').find('input[name=hfModalPartyId]').val();
				$("#party_dropdown11").select2("val", party_id); 				
			});
			$('.modal-lookup .populateItem').on('click', function(){
				// alert('dfsfsdf');
				var item_id = $(this).closest('tr').find('input[name=hfModalitemId]').val();
				$("#item_dropdown").select2("val", item_id); //set the value
				$("#itemid_dropdown").select2("val", item_id);
				$('#txtQty').focus();				
			});

			$('#voucher_type_hidden').val('new');

			$('#txtVrnoa').on('change', function() {
				fetch($(this).val());
			});

			$('.btnSave').on('click',  function(e) {
				
				if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('updatebtn')==0 ){
					alert('Sorry! you have not update rights..........');
				}else if($('#voucher_type_hidden').val()=='new' && $('.btnSave').data('insertbtn')==0){
					alert('Sorry! you have not insert rights..........');
				}else{
					e.preventDefault();
					self.initSave();
				}
			});
			$('#btnSearch').on('click',function(e){
				e.preventDefault();
			 		var error = validateSearch();
			 		var from = $('#from_date').val();
			 		var to = $('#to_date').val();
			 		var companyid =  $('#cid').val();
			 		var etype = 'item_conversion';
			 		var uid = $('#uid').val();

			 		if (!error) {
			 			fetchReports(from,to,companyid,etype,uid);
			 		} else {
			 			alert('Correct the errors...');
			 		}
			});

		
			// $('.btnPrint').on('click',  function(e) {
			// 	e.preventDefault();
			// 	Print_Voucher(1,'lg','');
			// });
			$('.btnprint_sm').on('click', function(e){
				e.preventDefault();
				Print_Voucher(1,'sm','');
			});
			$('.btnprint_sm_withOutHeader').on('click', function(e) {
				e.preventDefault();
				Print_Voucher(0,'sm');
			});
			$('.btnprint_sm_rate').on('click', function(e){
				e.preventDefault();
				Print_Voucher(1,'sm','wrate');
			});
			$('.btnprint_sm_withOutHeader_rate').on('click', function(e) {
				e.preventDefault();
				Print_Voucher(0,'sm','wrate');
			});
			$('.btnPrints').on('click', function(ev) {
                 window.open(base_url + 'application/views/reportprints/conversionreport.php', "Recipe Costing", 'width=1210, height=842');
            });
			$('.btnReset').on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			$('.btnDelete').on('click', function(e){
				if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('deletebtn')==0 ){
					alert('Sorry! you have not delete rights..........');
				}else{

					// alert($('#voucher_type_hidden').val() +' - '+ $('.btnSave').data('deletebtn') );
					e.preventDefault();
					var vrnoa = $('#txtVrnoaHidden').val();
					if (vrnoa !== '') {
					if ($('.btnSave').data('deletebtn')==0)
					{
						alert('Sorry! you have no Delete rights..........');
					}
					else
					{
						if (confirm('Are you sure to delete this voucher?'))
							deleteVoucher(vrnoa);
							}
					}
				}

			});

			$('#txtOrderNo').on('keypress', function(e) {
				if (e.keyCode === 13) {
					if ($(this).val() != '') {
						e.preventDefault();
						fetchThroughPO($(this).val());
					}
				}
			});

			/////////////////////////////////////////////////////////////////
			/// setting calculations for the single product
			/////////////////////////////////////////////////////////////////

			$('#txtWeight').on('input', function() {
				// var _gw = getNumVal($('#txtGWeight'));
				// if (_gw!=0) {
				// var w = parseInt(parseFloat($(this).val())/parseFloat(_gw));
				// $('#txtQty').val(w);	
				// }
				calculateUpperSum();
				
			});

						$('#itemid_dropdown').on('change', function() {
							var item_id = $(this).val();
							var prate = $(this).find('option:selected').data('prate');
							var grweight = $(this).find('option:selected').data('grweight');
							var uom_item = $(this).find('option:selected').data('uom_item');
							var stqty = $(this).find('option:selected').data('stqty');
			                var stweight = $(this).find('option:selected').data('stweight');
			                var size = $(this).find('option:selected').data('size');
			                
			                $('#stqty_lbl').text('Item,     Uom:' + uom_item + ', Size is ' + size);
			                $('#txtSizehidden').val(size);

			                // $('#stqty_lbl').text('Item,     Qty:' + stqty + ', Weight ' + stweight + 'Size is' + size);

							$('#txtPRate').val(parseFloat(prate).toFixed(2));
							$('#item_dropdown').select2('val', item_id);
							$('#txtGWeight').val(parseFloat(grweight).toFixed());
							$('#txtUom').val(uom_item);
							var crit = getcrit();
							
							last_5_srate(crit);
							last_stockLocatons(item_id);

							// last_5_srate($('#party_dropdown11').val(), item_id);
						
						});
						$('#item_dropdown').on('change', function() {
							var item_id = $(this).val();
							var prate = $(this).find('option:selected').data('prate');
							var grweight = $(this).find('option:selected').data('grweight');
							var uom_item = $(this).find('option:selected').data('uom_item');
							var stqty = $(this).find('option:selected').data('stqty');
			                var stweight = $(this).find('option:selected').data('stweight');
			                var size = $(this).find('option:selected').data('size');
			                $('#stqty_lbl').text('Item,     Uom:' + uom_item + ', Size is ' + size);
			                // $('#stqty_lbl').text('Item,     Qty:' + stqty + ', Weight ' + stweight);
			                $('#txtSizehidden').val(size);

							$('#txtPRate').val(parseFloat(prate).toFixed(2));
							$('#itemid_dropdown').select2('val', item_id);
							$('#txtGWeight').val(parseFloat(grweight).toFixed(2));
							$('#txtUom').val(uom_item);
							// alert($('#party_dropdown11').val());
							var crit = getcrit();
							
							last_5_srate(crit);
							last_stockLocatons(item_id);

							// last_5_srate($('#party_dropdown11').val(), item_id);
							// calculateUpperSum();
							// $('#txtQty').focus();
						});
						$('#itemid_dropdownItemC').on('change', function() {
							var item_id = $(this).val();
							var prate = $(this).find('option:selected').data('prate');
							var grweight = $(this).find('option:selected').data('grweight');
							var uom_item = $(this).find('option:selected').data('uom_item');
							var stqty = $(this).find('option:selected').data('stqty');
			                var stweight = $(this).find('option:selected').data('stweight');
			                var size = $(this).find('option:selected').data('size');
			                $('#stqty_lblItemC').text('Item,     Uom:' + uom_item + ', Size is ' + size);
			                // $('#stqty_lbl').text('Item,     Qty:' + stqty + ', Weight ' + stweight);
			                $('#txtSizehidden').val(size);

							$('#txtPRateItemC').val(parseFloat(prate).toFixed(2));
							$('#item_dropdownItemC').select2('val', item_id);
							// $('#txtGWeight').val(parseFloat(grweight).toFixed(2));
							// $('#txtUom').val(uom_item);
							// alert($('#party_dropdown11').val());
							var crit = getcrit();
							
							last_5_srate(crit);
							last_stockLocatons(item_id);

							// last_5_srate($('#party_dropdown11').val(), item_id);
							// calculateUpperSum();
							// $('#txtQty').focus();
						});
						$('#item_dropdownItemC').on('change', function() {
							var item_id = $(this).val();
							var prate = $(this).find('option:selected').data('prate');
							var grweight = $(this).find('option:selected').data('grweight');
							var uom_item = $(this).find('option:selected').data('uom_item');
							var stqty = $(this).find('option:selected').data('stqty');
			                var stweight = $(this).find('option:selected').data('stweight');
			                var size = $(this).find('option:selected').data('size');
			                $('#stqty_lblItemC').text('Item,     Uom:' + uom_item + ', Size is ' + size);
			                // $('#stqty_lbl').text('Item,     Qty:' + stqty + ', Weight ' + stweight);
			                $('#txtSizehidden').val(size);

							$('#txtPRateItemC').val(parseFloat(prate).toFixed(2));
							$('#itemid_dropdownItemC').select2('val', item_id);
							// $('#txtGWeight').val(parseFloat(grweight).toFixed(2));
							// $('#txtUom').val(uom_item);
							// alert($('#party_dropdown11').val());
							var crit = getcrit();
							
							last_5_srate(crit);
							last_stockLocatons(item_id);

							// last_5_srate($('#party_dropdown11').val(), item_id);
							// calculateUpperSum();
							// $('#txtQty').focus();
						});
			/*$('#itemid_dropdown').on('change', function() {
				var item_id = $(this).val();
				var prate = $(this).find('option:selected').data('prate');
				var grweight = $(this).find('option:selected').data('grweight');
				var uom_item = $(this).find('option:selected').data('uom_item');
				// $('#txtQty').val('1');
				var stqty = $(this).find('option:selected').data('stqty');
				var stweight = $(this).find('option:selected').data('stweight');
				$('#stqty_lbl').text('Item,     Qty:' + stqty + ', Weight ' + stweight);

				$('#txtPRate').val(parseFloat(prate).toFixed(2));
				$('#item_dropdown').select2('val', item_id);
				$('#txtGWeight').val(parseFloat(grweight).toFixed());
				$('#txtUom').val(uom_item);

				// calculateUpperSum();
				// $('#txtQty').focus();
			});
			$('#item_dropdown').on('change', function() {
				var item_id = $(this).val();
				var prate = $(this).find('option:selected').data('prate');
				var grweight = $(this).find('option:selected').data('grweight');
				var uom_item = $(this).find('option:selected').data('uom_item');
				// $('#txtQty').val('1');
				var stqty = $(this).find('option:selected').data('stqty');
				var stweight = $(this).find('option:selected').data('stweight');
				$('#stqty_lbl').text('Item,     Qty:' + stqty + ', Weight ' + stweight);

				$('#txtPRate').val(parseFloat(prate).toFixed(2));
				$('#itemid_dropdown').select2('val', item_id);
				$('#txtGWeight').val(parseFloat(grweight).toFixed(2));
				$('#txtUom').val(uom_item);
				// calculateUpperSum();
				// $('#txtQty').focus();
			});*/
			$('#txtQty').on('input', function() {
				calculateUpperSum();
			});
			$('#txtQtyItemC').on('input', function() {
				calculateUpperSumItemC();
			});
			$('#txtDiscp').on('input', function() {
				var discp = $(this).val();
				var qty = $('#txtQty').val();
				var rate = $('#txtPRate').val();
				var amount= qty * rate ;
				if(amount!==0 && discp!==0){
					var discount= amount*discp/100;
					$('#txtDiscount1_tbl').val( parseFloat(discount).toFixed(2));
				}else{
					$('#txtDiscount1_tbl').val('0');
				}
				calculateUpperSum();
			});
			$('#txtDiscount1_tbl').on('input', function() {
				var discount = $(this).val();
				var qty = $('#txtQty').val();
				var rate = $('#txtPRate').val();
				var amount= qty * rate ;
				if(amount!==0 &&  discount!==0){
					var discp= discount*100/amount;
					$('#txtDiscp').val( parseFloat(discp).toFixed(2));
				}else{
					$('#txtDiscp').val('0');
				}
				calculateUpperSum();
			});
			
			$('#txtPRate').on('input', function() {
				calculateUpperSum();
			});
			$('#txtPRateItemC').on('input', function() {
				calculateUpperSumItemC();
			});


			$('#btnAdd').on('click', function(e) {
				e.preventDefault();

				var error = validateSingleProductAdd();
				if (!error) {

					var item_desc = $('#item_dropdown').find('option:selected').text();
					var item_id = $('#item_dropdown').val();
					var dep_desc = $('#dept_dropdown').find('option:selected').text();
					var dep_id = $('#dept_dropdown').val();
					var qty = $('#txtQty').val();
					var rate = $('#txtPRate').val();
					var weight = $('#txtWeight').val();
					var amount = $('#txtAmount').val();
					var discp = $('#txtDiscp').val();
					var uom = $('#txtUom').val();
					var discount = $('#txtDiscount1_tbl').val();
					if (discount == '' || discount == null) {
						discount = 0;
					}

					// reset the values of the annoying fields
					$('#itemid_dropdown').select2('val', '');
					$('#item_dropdown').select2('val', '');
					$('#txtQty').val('');
					$('#txtPRate').val('');
					$('#txtWeight').val('');
					$('#txtAmount').val('');
					$('#txtGWeight').val('');
					$('#txtDiscp').val('');
					$('#txtDiscount1_tbl').val('');
					$('#txtUom').val('');
					$('#dept_dropdown').select2('val','');

					appendToTable('', item_desc, item_id,dep_desc,dep_id,uom,qty,0,0 , rate, amount, weight );
					calculateLowerTotal(qty, amount, weight);
					$('#stqty_lbl').text('Item');
					$('#item_dropdown').focus();
				} else {
					alert('Correct the errors!');
				}

			});

			// when btnRowRemove is clicked
			$('#purchase_table').on('click', '.btnRowRemove', function(e) {
				e.preventDefault();
				var qty = $.trim($(this).closest('tr').find('td.qty').text());
				var weight = $.trim($(this).closest('tr').find('td.weight').text());
				var amount = $.trim($(this).closest('tr').find('td.amount').text());
				var rate = $.trim($(this).closest('tr').find('td.rate').text());
				var discount = $.trim($(this).closest('tr').find('td.discount').text());
				var discp = $.trim($(this).closest('tr').find('td.discp').text());

				calculateLowerTotal("-"+qty, "-"+amount, "-"+weight);
				$(this).closest('tr').remove();
			});
			$('#purchase_table').on('click', '.btnRowEdit', function(e) {
				e.preventDefault();

				// getting values of the cruel row
				var item_id = $.trim($(this).closest('tr').find('td.item_desc').data('item_id'));
				var dept_id = $.trim($(this).closest('tr').find('td.godown_desc').data('godown_id'));
				var qty = $.trim($(this).closest('tr').find('td.qty').text());
				var weight = $.trim($(this).closest('tr').find('td.weight').text());
				var rate = $.trim($(this).closest('tr').find('td.rate').text());
				var uom = $.trim($(this).closest('tr').find('td.uom').text());
				var amount = $.trim($(this).closest('tr').find('td.amount').text());
				var discount = $.trim($(this).closest('tr').find('td.discount').text());
				var discp = $.trim($(this).closest('tr').find('td.discp').text());
				getItemInfo(item_id,'purchase_table');


				$('#txtDiscp').val(discp);
				$('#txtDiscount1_tbl').val(discount);
				// $('#itemid_dropdown').select2('val', item_id);
				// $('#item_dropdown').select2('val', item_id);
				$('#dept_dropdown').select2('val', dept_id);
				$('#txtUom').val(uom);

				$('#txtQty').val(qty);
				$('#txtWeight').val(weight);
				$('#txtPRate').val(rate);
				$('#txtAmount').val(amount);
				calculateLowerTotal("-"+qty, "-"+amount,"-"+weight);
				// now we have get all the value of the row that is being deleted. so remove that cruel row
				$(this).closest('tr').remove();	// yahoo removed
			});

			$('#txtDiscount').on('input', function() {
				var _disc= $('#txtDiscount').val();
				var _totalAmount= $('.txtTotalAmount').text();
				var _discamount=0;
				if (_disc!=0 && _totalAmount!=0){
					_discamount=_totalAmount*_disc/100;
				}
				$('#txtDiscAmount').val(_discamount);
				calculateLowerTotal(0, 0, 0);
			});

			$('#txtDiscAmount').on('input', function() {
				var _discamount= $('#txtDiscAmount').val();
				var _totalAmount= $('.txtTotalAmount').text();
				var _discp=0;
				if (_discamount!=0 && _totalAmount!=0){
					_discp=_discamount*100/_totalAmount;
				}
				$('#txtDiscount').val(parseFloat(_discp).toFixed(2));
				calculateLowerTotal(0, 0, 0);
			});

			$('#txtExpense').on('input', function() {
				var _exppercent= $('#txtExpense').val();
				var _totalAmount= $('.txtTotalAmount').text();
				var _expamount=0;
				if (_exppercent!=0 && _totalAmount!=0){
					_expamount=_totalAmount*_exppercent/100;
				}
				$('#txtExpAmount').val(parseFloat(_expamount).toFixed(2));
				calculateLowerTotal(0, 0, 0);
			});

			$('#txtExpAmount').on('input', function() {
				var _expamount= $('#txtExpAmount').val();
				var _totalAmount= $('.txtTotalAmount').text();
				var _exppercent=0;
				if (_expamount!=0 && _totalAmount!=0){
					_exppercent=_expamount*100/_totalAmount;
				}
				$('#txtExpense').val(parseFloat(_exppercent).toFixed(2));
				calculateLowerTotal(0, 0, 0);
			});

			$('#txtTax').on('input', function() {
				var _taxpercent= $('#txtTax').val();
				var _totalAmount= $('.txtTotalAmount').text();
				var _taxamount=0;
				if (_taxpercent!=0 && _totalAmount!=0){
					_taxamount=_totalAmount*_taxpercent/100;
				}
				$('#txtTaxAmount').val(parseFloat(_taxamount).toFixed(2));
				calculateLowerTotal(0, 0, 0);
			});

			$('#txtTaxAmount').on('input', function() {
				var _taxamount= $('#txtTaxAmount').val();
				var _totalAmount= $('.txtTotalAmount').text();
				var _taxpercent=0;
				if (_taxamount!=0 && _totalAmount!=0){
					_taxpercent=_taxamount*100/_totalAmount;
				}
				$('#txtTax').val(parseFloat(_taxpercent).toFixed(2));
				calculateLowerTotal(0, 0, 0);
			});
			// alert('load');

			shortcut.add("F10", function() {
    			$('.btnSave').first().trigger('click');
			});
			shortcut.add("F1", function() {
				$('a[href="#party-lookup"]').trigger('click');
			});
			shortcut.add("F2", function() {
				$('a[href="#item-lookup"]').trigger('click');
			});
			shortcut.add("F9", function() {
				Print_Voucher(1,'lg','');
			});
			shortcut.add("F6", function() {
    			$('#txtVrnoa').focus();
    			// alert('focus');
			});
			shortcut.add("F5", function() {
    			self.resetVoucher();
			});

			shortcut.add("F12", function() {
    			$('.btnDelete').first().trigger('click');
			});


			$('#txtVrnoa').on('keypress', function(e) {
				if (e.keyCode === 13) {
					e.preventDefault();
					var vrnoa = $('#txtVrnoa').val();
					if (vrnoa !== '') {
						fetch(vrnoa);
					}
				}
			});
			$('.btnprintHeader').on('click', function(e) {
				e.preventDefault();
				Print_Voucher(1,'lg','');

			});
			$('.btnprintwithOutHeader').on('click', function(e) {
				e.preventDefault();
				Print_Voucher(0,'lg','amount');
			});
			
			$('.btnsearchitem').on('click', function (e) {
			  var values = $(this).prop('id');
			  $('#hiddenFieldValue').val(values); 
			});
			onChange(1);
            $("input[id$='txtItemSearch']").keydown(function(event){
                var keyCode = event.keyCode;
                if(keyCode == 13)
                {
                    onChange(1);
                    event.preventDefault();
                    return false;
                }
                else if(keyCode == 8 || keyCode == 46)
                {
                    var txt = $("input[id$='txtItemSearch']").val();
                    if(txt.length <= 1)
                    {
                        $("input[id$='txtItemSearch']").val('');
                        onChange(1);
                    }
                }
            });
			purchase.fetchRequestedVr();
		},

		// prepares the data to save it into the database
		initSave : function() {

			var saveObj = getSaveObject();
			var error = validateSave();

			if (!error) {
				var rowsCount = $('#purchase_table').find('tbody tr').length;
				if (rowsCount > 0 ) {
					save(saveObj);
				} else {
					alert('No date found to save!');
				}
			} else {
				alert('Correct the errors...');
			}
		},
		initSaveAccount : function() {

			var saveObjAccount = getSaveObjectAccount();
			var error = validateSaveAccount();

			if (!error) {
					saveAccount(saveObjAccount);
			} else {
				alert('Correct the errors...');
			}
		},
		initSaveItem : function() {

			var saveObjItem = getSaveObjectItem();
			var error = validateSaveItem();

			if (!error) {
					saveItem(saveObjItem);
			} else {
				alert('Correct the errors...');
			}
		},
		initSaveGodown : function() {

            var saveObjGodown = getSaveObjectGodown();
            var error = validateSaveGodown();

            if (!error) {
                    saveGodown(saveObjGodown);
            } else {
                alert('Correct the errors...');
            }
        },
		fetchRequestedVr : function () {

		var vrnoa = general.getQueryStringVal('vrnoa');
		vrnoa = parseInt( vrnoa );
		$('#txtVrnoa').val(vrnoa);
		$('#txtVrnoaHidden').val(vrnoa);
		if ( !isNaN(vrnoa) ) {
			fetch(vrnoa);
		}else{
			getMaxVrno();
			getMaxVrnoa();
		}
	},

	bindModalPartyGrid : function() {


	            var dontSort = [];
	            $('#party-lookup table thead th').each(function () {
	                if ($(this).hasClass('no_sort')) {
	                    dontSort.push({ "bSortable": false });
	                } else {
	                    dontSort.push(null);
	                }
	            });
	            purchase.pdTable = $('#party-lookup table').dataTable({
	                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
	                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
	                "aaSorting": [[0, "asc"]],
	                "bPaginate": true,
	                "sPaginationType": "full_numbers",
	                "bJQueryUI": false,
	                "aoColumns": dontSort

	            });
	            $.extend($.fn.dataTableExt.oStdClasses, {
	                "s`": "dataTables_wrapper form-inline"
	            });
	},
	bindSearchGrid : function() {


	            var dontSort = [];
	            $('#purchase_tableReport thead th').each(function () {
	                if ($(this).hasClass('no_sort')) {
	                    dontSort.push({ "bSortable": false });
	                } else {
	                    dontSort.push(null);
	                }
	            });
	            purchase.pdTable = $('#purchase_tableReport').dataTable({
	                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
	                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
	                "aaSorting": [[0, "asc"]],
	                "bPaginate": true,
	                "sPaginationType": "full_numbers",
	                "bJQueryUI": false,
	                "aoColumns": dontSort

	            });
	            $.extend($.fn.dataTableExt.oStdClasses, {
	                "s`": "dataTables_wrapper form-inline"
	            });
	},

bindModalItemGrid : function() {

			
				            var dontSort = [];
				            $('#item-lookup table thead th').each(function () {
				                if ($(this).hasClass('no_sort')) {
				                    dontSort.push({ "bSortable": false });
				                } else {
				                    dontSort.push(null);
				                }
				            });
				            purchase.pdTable = $('#item-lookup table').dataTable({
				                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
				                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
				                "aaSorting": [[0, "asc"]],
				                "bPaginate": true,
				                "sPaginationType": "full_numbers",
				                "bJQueryUI": false,
				                "aoColumns": dontSort

				            });
				            $.extend($.fn.dataTableExt.oStdClasses, {
				                "s`": "dataTables_wrapper form-inline"
				            });
},

		// instead of reseting values reload the page because its cruel to write to much code to simply do that
		resetVoucher : function() {
			 general.reloadWindow();
		}
	}

};

var purchase = new Purchase();
purchase.init();