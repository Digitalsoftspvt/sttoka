var addjv = function() {
	var saveAccount = function( accountObj ) {
		$.ajax({
			url : base_url + 'index.php/account/saveaccount',
			type : 'POST',
			data : { 'accountDetail' : accountObj },
			dataType : 'JSON',
			success : function(data) {
				if (data == "duplicate") {
					$('#txtAccountName').addClass('inputerror');
					alert('Account already Saved!');
				}
				else{
					if (data.error === 'true') {
						alert('An internal error occured while saving voucher. Please try again.');
					} else {
						alert('Account saved successfully.');
						$('#AccountAddModel').modal('hide');
						fetchAccount();
					}
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	
var validateSaveAccount = function() {

		var errorFlag = false;
		var partyEl = $('#txtAccountName');
		var deptEl = $('#txtLevel3');

		// remove the error class first
		$('.inputerror').removeClass('inputerror');

		if ( !partyEl.val() ) {
			$('#txtAccountName').addClass('inputerror');
			errorFlag = true;
		}
		if ( !deptEl.val() ) {
			deptEl.addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var fetchAccount = function() {

		$.ajax({
			url : base_url + 'index.php/account/fetchall',
			type : 'POST',
			data : { 'active' : 1 },
			dataType : 'JSON',
			success : function(data) {
				if (data === 'false') {
					alert('No data found');
				} else {
					populateDataAccount(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var populateDataAccount = function(data) {
		$("#name_dropdown").empty();
		$("#pid_dropdown").empty();

		$.each(data, function(index, elem){
			var opt="<option value='"+elem.pid+"' >" +  elem.name + "</option>";
			$(opt).appendTo('#name_dropdown');
			
			var opt1="<option value='"+elem.pid+"' >" +  elem.pid + "</option>";
			$(opt1).appendTo('#pid_dropdown');
		});
	}

	var save = function( purchase, dcno ) {
		general.disableSave();
		$.ajax({
			url : base_url + 'index.php/opening_stock/save',
			type : 'POST',
			data : { 'stockmain' : JSON.stringify(purchase.stockmain), 'stockdetail' : JSON.stringify(purchase.stockdetail), 'vrnoa' : purchase.vrnoa, 'voucher_type_hidden':$('#voucher_type_hidden').val(), 'vrdate' : $('#vrdate').val()},
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'date close') {
					alert('Sorry! you can not insert update in close date................');
				}else if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					
					alert('Voucher saved successfully.');
					general.reloadWindow();
				}
				general.enableSave();
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var Print_Voucher = function( ) {
		var etype= 'openingstock';
		var dcno = $('#txtIdHidden').val();
		var companyid = $('#cid').val();
		var user = $('#uname').val();
		// alert('etype  ' +  etype  +' dcno '+ dcno + 'company_id' + companyid );
		var url = base_url + 'index.php/doc/openingStockVocuherPrintPdf/' + etype + '/' + dcno   + '/' + companyid + '/' + '-1' + '/' + user;
		var lastChar = url.substr(url.length - 1);
		if(lastChar == "/")
		{
			url = url.slice(0,-1);
		}
		window.open(url);
	}


	// gets the max id of the voucher
	var getMaxId = function() {

		$.ajax({

			url : base_url + 'index.php/opening_stock/getmaxid',
			type : 'POST',
			data : { 'company_id': $('#cid').val()},
			dataType : 'JSON',
			success : function(data) {

				$('#txtId').val(data);
				$('#txtMaxIdHidden').val(data);
				$('#txtIdHidden').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var validateEntry = function() {


		var errorFlag = false;
		var name = $('#hfItemId').val();
		var qty = $('#txtQty').val();

		// remove the error class first
		$('#txtItemId').removeClass('inputerror');
		$('#txtQty').removeClass('inputerror');

		if ( name === '' || name === null ) {
			$('#txtItemId').addClass('inputerror');
			errorFlag = true;
		}

		if ( (qty === '' || qty === null)) {
			$('#txtQty').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var setNetAmount = function(debit, credit) {
		
		var _debit = ($('#txtNetDebit').val() === '') ? 0 : $('#txtNetDebit').val();
		var _credit = ($('#txtNetCredit').val() === '') ? 0 : $('#txtNetCredit').val();

		debit = (debit == '') ? 0 : debit;
		credit = (credit == '') ? 0 : credit;

		var netDebit = parseFloat(debit) + parseFloat(_debit);
		var netCredit = parseFloat(credit) + parseFloat(_credit);
		$('#txtNetDebit').val(netDebit);
		$('#txtNetCredit').val(netCredit);
	}

	var appendToTable = function(item_id, item_desc, qty, weight) {
     
     
		var row = "";
		var srno = $('#cash_table tbody tr').length + 1;
		row = 	"<tr> <td class='srno'> "+ srno +"</td>"+
				"<td class='item_desc' data-item_id='"+ item_id +"'> "+ item_desc +"</td>" +
				"<td class='qty'> "+ qty +"</td>"+
				"<td class='weight'> "+ weight +"</td>"+
				"<td class='text-center'><a href='' class='btn btn-primary btnRowEdit'><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td> </tr>";
		$(row).appendTo('#cash_table');
	}

	var getCashPartyId = function() {
		var pid = "";
		$('#name_dropdown option').each(function() { if ($(this).text().trim().toLowerCase() == 'cash') pid = $(this).val();  });
		return pid;
	}

	var getSaveObject = function() {

		var stockmain = {};
		var stockdetail = [];

		stockmain.vrdate = $('#cur_date').val();
		stockmain.vrnoa = $('#txtIdHidden').val();
		stockmain.etype = 'openingstock';
		stockmain.uid = $('#uid').val();
		stockmain.company_id = $('#cid').val();
	

		$('#cash_table').find('tbody tr').each(function( index, elem ) {

			var sd = {};
			sd.stid = '';
			sd.item_id = $.trim($(elem).find('td.item_desc').data('item_id'));
			sd.qty = $.trim($(elem).find('td.qty').text());
			sd.weight = $.trim($(elem).find('td.weight').text());
			sd.godown_id = $('#dept_dropdown').val();
			stockdetail.push(sd);

		});

		var data = {};
		data.stockmain = stockmain;
		data.stockdetail = stockdetail;
		data.vrnoa = $('#txtIdHidden').val();

		return data;
	}

	// var saveAccount = function( accountObj ) {
	// 	$.ajax({
	// 		url : base_url + 'index.php/account/save',
	// 		type : 'POST',
	// 		data : { 'accountDetail' : accountObj },
	// 		dataType : 'JSON',
	// 		success : function(data) {

	// 			if (data.error === 'false') {
	// 				alert('An internal error occured while saving account. Please try again.');
	// 			} else {
	// 				alert('Account saved successfully.');
	// 				$('#AccountAddModel').modal('hide');
	// 				fetchAccount();
	// 			}
	// 		}, error : function(xhr, status, error) {
	// 			console.log(xhr.responseText);
	// 		}
	// 	});
	// }
	
var validateSaveAccount = function() {

		var errorFlag = false;
		var partyEl = $('#txtAccountName');
		var deptEl = $('#txtLevel3');

		// remove the error class first
		$('.inputerror').removeClass('inputerror');

		if ( !partyEl.val() ) {
			$('#txtAccountName').addClass('inputerror');
			errorFlag = true;
		}
		if ( !deptEl.val() ) {
			deptEl.addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var fetchAccount = function() {

		$.ajax({
			url : base_url + 'index.php/account/fetchall',
			type : 'POST',
			data : { 'active' : 1 },
			dataType : 'JSON',
			success : function(data) {
				if (data === 'false') {
					alert('No data found');
				} else {
					populateDataAccount(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var populateDataAccount = function(data) {
		$("#name_dropdown").empty();
		$("#pid_dropdown").empty();

		$.each(data, function(index, elem){
			var opt="<option value='"+elem.pid+"' >" +  elem.name + "</option>";
			$(opt).appendTo('#name_dropdown');
			
			var opt1="<option value='"+elem.pid+"' >" +  elem.pid + "</option>";
			$(opt1).appendTo('#pid_dropdown');
		});
	}

	var getSaveObjectAccount = function() {

		var obj = {
			pid : '20000',
			active : '1',
			name : $.trim($('#txtAccountName').val()),
			level3 : $.trim($('#txtLevel3').val()),
			dcno : $('#txtId').val(),
			etype : 'cpv/crv',
			uid : $.trim($('#uid').val()),
			company_id : $.trim($('#cid').val()),
		};

		return obj;
	}

	// checks for the empty fields
	var validateSave = function() {

		var errorFlag = false;
		var dept_dropdown = $('#dept_dropdown').val();
		// var credit = $('#txtNetCredit').val();

		$('.inputerror').removeClass('inputerror');

		if ( !dept_dropdown ) {
			$('#dept_dropdown').addClass('inputerror');
			errorFlag = true;
		}


		return errorFlag;
	}

	var search = function(from, to) {

		$.ajax({
			url : base_url + 'index.php/opening_stock/fetchvoucherrange',
			type : 'POST',
			data : { 'from' : from, 'to' : to },
			dataType : 'JSON',
			success : function(data) {

				$('#search_cash_table tbody tr').remove();
				populateSearchData(data);

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateSearchData = function(data) {

		var rows = "";
		$.each(data, function(index, elem) {

			rows += 	"<tr> <td class='dcno' data-etype='"+ elem.etype +"'> "+ elem.vrnoa +"</td> <td> "+ elem.vrdate.substr(0, 10) +"</td> <td> "+ elem.item_name +"</td> <td> "+ elem.s_qty +"</td> <td> "+ elem.weight +"</td><td class='text-center'><a href='' class='btn btn-primary btnRowEdit'><span class='fa fa-edit'></span></a></td> </tr>";
		});

		$(rows).appendTo('#search_cash_table tbody');
	}

	var fetch = function(vrnoa) {

		$.ajax({
			url : base_url + 'index.php/opening_stock/fetch',
			type : 'POST',
			data : { 'vrnoa' : vrnoa, 'company_id':$('#cid').val() },
			dataType : 'JSON',
			success : function(data) {
				resetFields();
				if (data == 'false') {
                    $('#txtIdHidden').val($('#txtMaxIdHidden').val());
                    $('#voucher_type_hidden').val('new');
					alert('No data found');
				} else {
					populateData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var resetFields = function(){
		$('#cash_table').find('tbody tr').remove();
		$('#txtNetDebit').val('');
		$('#txtNetCredit').val('');
		$('#cur_date').datepicker('update', new Date());
		$('#user_dropdown').val('');
		$('#posting_date').val('');
	}

	var populateData = function(data) {

		$('#txtId').val(data[0]['vrnoa']);
		$('#txtIdHidden').val(data[0]['vrnoa']);

		$('#cur_date').datepicker('update', data[0]['vrdate'].substring(0, 10));
		$('#vrdate').datepicker('update', data[0]['vrdate'].substring(0, 10));
		$('#user_dropdown').val(data[0]['uid']);
		$('#posting_date').val(data[0]['date_time'].substr(0, 10));
		$('#dept_dropdown').select2('val', data[0]['godown_id']);

		$.each(data, function(index, elem) {
			appendToTable(elem.item_id, elem.item_name, elem.s_qty,elem.weight);
			setNetAmount(parseFloat(elem.s_qty).toFixed(2), parseFloat(elem.weight).toFixed(2));
		});
		$('#voucher_type_hidden').val('edit');
	}

	var deleteVoucher = function(vrnoa) {

		$.ajax({
			url : base_url + 'index.php/opening_stock/deletevoucher',
			type : 'POST',
			data : {'chk_date' : $('#cur_date').val(), 'vrdate' : $('#vrdate').val(), 'vrnoa' : vrnoa,'company_id':$('#cid').val() },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'date close') {
					alert('Sorry! you can not delete in close date................');
				}else if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var paging = function (total)
	{
    	$("#paging").smartpaginator({
        	totalrecords: parseInt(total),
        	recordsperpage: 10,
        	datacontainer: 'tbody',
        	dataelement: 'tr',
        	theme: 'custom',
        	onchange: onChange
    	});
	}

	var onChange = function (newPageValue)
    {
        $.ajax({
            type: "POST",
            url: base_url + 'index.php/item/paging',
            data: {
                page_number: newPageValue,
                search: $.trim($("#txtItemSearch").val())
            }
        }).done(function (result) {

            var total = "";
            var jsonR = JSON.stringify($.parseJSON(result)).replace(/\\n/g, "\\n")
                                        .replace(/\\'/g, "\\'")
                                        .replace(/\\"/g, '\\"')
                                        .replace(/\\&/g, "\\&")
                                        .replace(/\\r/g, "\\r")
                                        .replace(/\\t/g, "\\t")
                                        .replace(/\\b/g, "\\b")
                                        .replace(/\\f/g, "\\f");
            var json = $.parseJSON(jsonR);
            total = json.total;

            if (json.records != false) {
                var html = "";
                $.each(json.records, function (index, value) {
                    html += "<tr id='" + value.item_id + "'>";
                    html += "<td>"+value.item_id+"<input type='hidden' name='hfModalitemId' value='" + value.item_id + "'></td>";
                    html += "<td class='tdItemCode'><b>" + value.item_code + "<b></td>";
                    html += "<td class='tdDescription'><b>" + value.item_des + "</b></td>";
                    html += "<td class='tdItemUom'><b>" + value.uom + "</b></td>";
                    html += "<td class='tdPRate hide'>" + value.cost_price + "</td>";
                    html += "<td class='tdGrWeight hide'>" + value.grweight + "</td>";
                    html += "<td class='tdStQty hide'><b>" + value.stqty + "</b></td>";
                    html += "<td class='tdStWeight hide'>" + value.stweight + "</td>";
                    html += "<td class='tdUItem hide'>" + value.uitem_des + "</td>";
                    html += "<td><a href='#' data-dismiss='modal' class='btn btn-primary populateItem'><i class='fa fa-search'></i></a></td>"
                    html += "</tr>";
                });

                $("#divNoRecord").removeClass('divNoRecord');
                $("#divNoRecord").html('');
                $("#tbItems > tbody").html('');
                $("#tbItems > tbody").append(html);

                $('.modal-lookup .populateItem').on('click', function()
                {
                    var itemId = $(this).closest('tr').find('input[name=hfModalitemId]').val();
                    var itemDescription = $(this).closest('tr').find('.tdDescription').text();
                    var uom = $(this).closest('tr').find('.tdItemUom').text();
                    var pRate = $(this).closest('tr').find('.tdPRate').text();
                    var grWeight = $(this).closest('tr').find('.tdGrWeight').text();
                    var stQty = $(this).closest('tr').find('.tdStQty').text();
                    var stWeight = $(this).closest('tr').find('.tdStWeight').text();
                    var uItemName = $(this).closest('tr').find('.tdUItem').text();
                    var itemCode = $(this).closest('tr').find('.tdItemCode').text();

                    var itemCodeOption = "<option value='" + itemId + "' data-uom_item='" + uom + "' data-prate='" + pRate + "' data-grweight='" + grWeight + "' data-stqty='" + stQty + "' data-stweight='" + stWeight + "' data-uitem_des='" + uItemName + "' >" + itemCode + "</option>";
                    var itemOption = "<option value='" + itemId + "'  data-uom_item='" + uom + "' data-prate='" + pRate + "' data-grweight='" + grWeight + "' data-stqty='" + stQty + "' data-stweight='" + stWeight + "'  data-uitem_des='" + uItemName + "' >" + itemDescription + "</option>";
                    $('#itemid_dropdown').append(itemCodeOption);
                    $('#item_dropdown').append(itemOption);

                    $('#itemid_dropdown').val(itemId).trigger('change');
                    $('#item_dropdown').val(itemId).trigger('change');

                    $('#txtPRate').val(parseFloat(pRate).toFixed(2));

                    $('#txtQty').focus();
                });

                $("#paging").show();
                $("#tbItems").show();
                if (newPageValue == "1") {
                    paging(total);
                }
            }
            else {
                $("#tbItems > tbody").html('');
                $("#divNoRecord").show();
                $("#divNoRecord").addClass('divNoRecord');
                $("#paging").hide();
                $("#divNoRecord").html('No Record Found');
            }
        });
    }

     var getItemInfo = function(itemId)
    {
        $.ajax({
            type: "POST",
            url: base_url + 'index.php/item/getiteminfo',
            data: {
                item_id: itemId
            }
        }).done(function (result) {

            var jsonR = JSON.stringify($.parseJSON(result)).replace(/\\n/g, "\\n")
                                    .replace(/\\'/g, "\\'")
                                    .replace(/\\"/g, '\\"')
                                    .replace(/\\&/g, "\\&")
                                    .replace(/\\r/g, "\\r")
                                    .replace(/\\t/g, "\\t")
                                    .replace(/\\b/g, "\\b")
                                    .replace(/\\f/g, "\\f");
            var json = $.parseJSON(jsonR);

            if (json.records != false)
            {
                $.each(json.records, function (index, value) {
                    var itemCodeOption = "<option value='" + value.item_id + "' data-uom_item='" + value.uom + "' data-data-prate='" + value.cost_price + "' data-grweight='" + value.grweight + "' data-stqty='" + value.stqty + "' data-stweight='" + value.stweight + "' >" + value.item_code + "</option>";
                    var itemOption = "<option value='" + value.item_id + "'  data-uom_item='" + value.uom + "' data-data-prate='" + value.cost_price + "' data-grweight='" + value.grweight + "' data-stqty='" + value.stqty + "' data-stweight='" + value.stweight + "' >" + value.item_des + "</option>";
                    $('#itemid_dropdown').append(itemCodeOption);
                    $('#item_dropdown').append(itemOption);

                    $('#itemid_dropdown').select2('val', itemId).trigger('change');
                    $('#item_dropdown').select2('val', itemId).trigger('change');

                    var grweight = $('#item_dropdown').find('option:selected').data('grweight');
                    $('#txtGWeight').val(parseFloat(grweight).toFixed());
                });
            }
        });
    }

    var clearItemData = function (){
		
		$("#hfItemId").val("");
		$("#hfItemSize").val("");
		$("#hfItemBid").val("");
		$("#hfItemUom").val("");
		$("#hfItemPrate").val("");
		$("#hfItemGrWeight").val("");
		$("#hfItemStQty").val("");
		$("#hfItemStWeight").val("");
		$("#hfItemLength").val("");
		$("#hfItemCatId").val("");
		$("#hfItemSubCatId").val("");
		$("#hfItemDesc").val("");
		$("#hfItemShortCode").val("");

        //$("#txtItemId").val("");
    }

	return {

		init : function() {
			this.bindUI();
			// $('#pid_dropdown').select2('open');
			$('#voucher_type_hidden').val('new'); 
			addjv.bindModalPartyGrid();
		    addjv.fetchRequestedVr();
		},

		bindUI : function() {

			var self = this;

			$('#txtLevel3').on('change', function() {
				
				var level3 = $('#txtLevel3').val();
				$('#txtselectedLevel1').text('');
				$('#txtselectedLevel2').text('');
				if (level3 !== "" && level3 !== null) {
					// alert('enter' + $(this).find('option:selected').data('level2') );	
					$('#txtselectedLevel2').text(' ' + $(this).find('option:selected').data('level2'));
					$('#txtselectedLevel1').text(' ' + $(this).find('option:selected').data('level1'));
				}
			});
			// $('#txtLevel3').select2();
			$('.btnSaveM').on('click',function(e){
				
				if ( $('.btnSave').data('saveaccountbtn')==0 ){
					alert('Sorry! you have not save accounts rights..........');
				}else{
					e.preventDefault();
					self.initSaveAccount();
				}
			});
			$('.btnResetM').on('click',function(){
				
				$('#txtAccountName').val('');
				$('#txtselectedLevel2').text('');
				$('#txtselectedLevel1').text('');
				$('#txtLevel3').select2('val','');
			});
			$('#AccountAddModel').on('shown.bs.modal',function(e){
				$('#txtAccountName').focus();
			});
			shortcut.add("F3", function() {
    			$('#AccountAddModel').modal('show');
			});

			$('.modal-lookup .populateAccount').on('click', function(){
				// alert('dfsfsdf');
				var party_id = $(this).closest('tr').find('input[name=hfModalPartyId]').val();
				$("#name_dropdown").select2("val", party_id); //set the value
				$("#pid_dropdown").select2("val", party_id); //set the value

				// var partyEl = $('#drpParty');
				//party.fetchParty(party_id);
				// partyEl.val(party_id);
				// partyEl.trigger('liszt:updated');
				// alert('search party ' + party_id);
				// $('#pid_dropdown').val(party_id);
				// $('#name_dropdown').val(party_id);
			});


			$('.btnSave').on('click',  function(e) {
				
				if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('updatebtn')==0 ){
					alert('Sorry! you have not update rights..........');
				}else if($('#voucher_type_hidden').val()=='new' && $('.btnSave').data('insertbtn')==0){
					alert('Sorry! you have not insert rights..........');
				}else{
					e.preventDefault();
					self.initSave();
				}
			});

			$('.btnReset').on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});
			$('.btnPrint').on('click', function(e){
				e.preventDefault();
				Print_Voucher();
			});

			$('#pid_dropdown').on('change', function() {

				var pid = $(this).val();
				$("#name_dropdown").select2("val", pid);
			});
			$('#name_dropdown').on('change', function() {
				var pid = $(this).val();
				$("#pid_dropdown").select2("val", pid);
			});
			$('.btnSave1').on('click', function(){
				$('.btnSave').click();
			});
			shortcut.add("F10", function() {
    			self.initSave();
			});
			shortcut.add("F1", function() {
				$('a[href="#party-lookup"]').trigger('click');
			});
			shortcut.add("F9", function() {
				Print_Voucher();
			});
			shortcut.add("F6", function() {
    			$('#txtId').focus();
    			// alert('focus');
			});
			shortcut.add("F5", function() {
    			self.resetVoucher();
			});

			shortcut.add("F12", function() {
    			$('.btnDelete').first().trigger('click');
			});
			$('#name_dropdown').select2();
			$('#pid_dropdown').select2();


			$('.btnSearch').on('click', function(e) {
				e.preventDefault();
				self.initSearch();
			});

			$('#btnAddCash').on('click', function(e) {
				e.preventDefault();

				var item_desc = $('#itemDesc').val();
				var uitem_des = $('#hfItemUname').val();
				var uom_item = $('#hfItemUom').val();
				var item_code = $('#hfItemShortCode').val();
				var item_id = $('#hfItemId').val();
				
				var qty = $('#txtQty').val();
				var weight = $('#txtWeight').val();

				var error = validateEntry();
				if (!error) {

					setNetAmount(qty, weight);
					appendToTable(item_id, item_desc, qty, weight);
					$('#itemDesc').val('');
					$('#txtItemId').val('');
					$('#hfItemId').val('');
					$('#txtQty').val('');
					$('#txtWeight').val('');
					$('#txtAmount').val('');
					$('#txtUom').val('');
				}
			});

			// when btnRowRemove is clicked
			$('#cash_table').on('click', '.btnRowRemove', function(e) {
				e.preventDefault();

				var debit = $.trim($(this).closest('tr').find('td.debit').text());
				var credit = $.trim($(this).closest('tr').find('td.credit').text());

				debit = (debit == '') ? 0 : debit;
				credit = (credit == '') ? 0 : credit;

				setNetAmount("-"+debit, "-"+credit);
				$(this).closest('tr').remove();
			});

			$('#cash_table').on('click', '.btnRowEdit', function(e) {
				e.preventDefault();

				var item_id = $.trim($(this).closest('tr').find('td.item_desc').data('item_id'));
				var qty = $.trim($(this).closest('tr').find('td.qty').text());
				var weight = $.trim($(this).closest('tr').find('td.weight').text());
				var rate = $.trim($(this).closest('tr').find('td.rate').text());
				var amount = $.trim($(this).closest('tr').find('td.amount').text());

                //getItemInfo(item_id);
				$.ajax({
					type: "POST",
					url: base_url + 'index.php/item/getiteminfobyid',
					data: {
						item_id: item_id
					}
				}).done(function (result) {

					$("#imgItemLoader").hide();
					var item = $.parseJSON(result);

					if (item != false)
					{
						$("#hfItemId").val(item[0].item_id);
						$("#hfItemSize").val(item[0].size);
						$("#hfItemBid").val(item[0].bid);
						$("#hfItemUom").val(item[0].uom);
						$("#hfItemPrate").val(item[0].cost_price);
						$("#hfItemGrWeight").val(item[0].grweight);
						$("#hfItemStQty").val(item[0].stqty);
						$("#hfItemStWeight").val(item[0].stweight);
						$("#hfItemLength").val(item[0].length);
						$("#hfItemCatId").val(item[0].catid);
						$("#hfItemSubCatId").val(item[0].subcatid);
						$("#hfItemDesc").val(item[0].item_des);
						$("#hfItemShortCode").val(item[0].short_code);
						$("#hfItemUname").val(item[0].uname);


						$("#txtItemId").val(item[0].item_code);
						$('#itemDesc').val(item[0].item_des);
						$('#txtUom').val(item[0].uom);
						
						$('#txtQty').val(qty);
						$('#txtWeight').val(weight);
						
						setNetAmount("-"+qty, "-"+weight);
						
						$('#stqty_lbl').text('Item,     Uom:' + item[0].uom);
                        // now we have get all the value of the row that is being deleted. so remove that cruel row
                    }
                });
				
				$(this).closest('tr').remove();
			});

			$('#txtId').on('change', function(e) {
				// get the based on the id entered by the user
				if ( $('#txtId').val().trim() !== "" ) {
					e.preventDefault();
					var dcno = $.trim($('#txtId').val());
					fetch(dcno);
				}
			});

			$('#txtId').on('keypress', function(e) {

				// check if enter key is pressed
				if (e.keyCode === 13) {
					e.preventDefault();
					// get the based on the id entered by the user
					if ( $('#txtId').val().trim() !== "" ) {

						var dcno = $.trim($('#txtId').val());
						fetch(dcno);
					}
				}
			});


			$('.btnDelete').on('click', function(e){
				if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('deletebtn')==0 ){
					alert('Sorry! you have not delete rights..........');
				}else{
					e.preventDefault();
					var dcno = $('#txtId').val();
					if (dcno !== '') {
						if ($('.btnSave').data('deletebtn')==0)
					{
						alert('Sorry! you have no Delete rights..........');
					}
					else
					{
						if (confirm('Are you sure to delete this voucher?'))
						deleteVoucher(dcno);
					}
				}
				}
			});

			$('#search_cash_table').on('click', '.btnRowEdit', function(e) {
				e.preventDefault();		// prevent the default behaviour of the link
				var dcno = $.trim($(this).closest('tr').find('td.dcno').text());
				var etype = $.trim($(this).closest('tr').find('td.dcno').data('etype'));
				fetch(dcno);		// get the fee category detail by id

				$('a[href="#addupdateJV"]').trigger('click');
			});

			$('#itemid_dropdown').on('change', function() {
				var item_id = $(this).val();
				var uom_item = $(this).find('option:selected').data('uom_item');
			
				$('#item_dropdown').select2('val',item_id);
				$('#txtUom').val(uom_item);

				// calculateUpperSum();
				// $('#txtQty').focus();
			});
			$('#item_dropdown').on('change', function() {

				var item_id = $(this).val();
				var uom_item = $(this).find('option:selected').data('uom_item');
			
				$('#itemid_dropdown').select2('val',item_id);
				$('#txtUom').val(uom_item);

				// calculateUpperSum();
				// $('#txtQty').focus();
			});

			var countItem = 0;
			$('input[id="txtItemId"]').autoComplete({

				minChars: 1,
				cache: false,
				menuClass: '',
				source: function(search, response)
				{
					try { xhr.abort(); } catch(e){}
					$('#txtItemId').removeClass('inputerror');
					$("#imgItemLoader").hide();
					if(search != "")
					{
						xhr = $.ajax({
							url: base_url + 'index.php/item/searchitem',
							type: 'POST',
							data: {

								search: search
							},
							dataType: 'JSON',
							beforeSend: function (data) {

								$(".loader").hide();
								$("#imgItemLoader").show();
								countItem = 0;
							},
							success: function (data) {

								if(data == '')
								{
									$('#txtItemId').addClass('inputerror');
									clearItemData();
									$('#itemDesc').val('');
									$('#txtQty').val('');
									$('#txtPRate').val('');
									$('#txtBundle').val('');
									$('#txtGBundle').val('');
									$('#txtWeight').val('');
									$('#txtAmount').val('');
									$('#txtGWeight').val('');
									$('#txtDiscp').val('');
									$('#txtDiscount1_tbl').val('');
								}
								else
								{
									$('#txtItemId').removeClass('inputerror');
									response(data);
									$("#imgItemLoader").hide();
								}
							}
						});
					}
					else
					{
						clearItemData();
					}
				},
				renderItem: function (item, search)
				{
					var sea = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
					var re = new RegExp("(" + sea.split(' ').join('|') + ")", "gi");

					var selected = "";
					if((search.toLowerCase() == (item.item_des).toLowerCase() && countItem == 0) || (search.toLowerCase() != (item.item_des).toLowerCase() && countItem == 0))
					{
						selected = "selected";
					}
					countItem++;
					clearItemData();

					return '<div class="autocomplete-suggestion ' + selected + '" data-val="' + search + '" data-item_id="' + item.item_id + '" data-size="' + item.pack + '" data-bid="' + item.bid +
					'" data-uom_item="'+ item.uom + '" data-srate="' + item.srate + '" data-grweight="' + item.grweight + '" data-stqty="' + item.stqty +
					'" data-stweight="' + item.stweight + '" data-length="' + item.length  + '" data-catid="' + item.catid +
					'" data-subcatid="' + item.subcatid + '" data-desc="' + item.item_des + '" data-cost_price="' + item.cost_price + '" data-code="' + item.item_code + '" data-self="' + item.self_freight_rate + '" data-bilty="' + item.bilty_freight_rate + 
					'" data-gari="' + item.gari_freight_rate + '">' + item.item_des.replace(re, "<b>$1</b>") + '</div>';
				},
				onSelect: function(e, term, item)
				{

					$("#imgItemLoader").hide();
					$("#hfItemId").val(item.data('item_id'));
					$("#hfItemSize").val(item.data('size'));
					$("#hfItemBid").val(item.data('bid'));
					$("#hfItemUom").val(item.data('uom_item'));
					$("#hfItemPrate").val(item.data('cost_price'));
					$("#hfItemGrWeight").val(item.data('grweight'));
					$("#hfItemStQty").val(item.data('stqty'));
					$("#hfItemStWeight").val(item.data('stweight'));
					$("#hfItemLength").val(item.data('length'));
					$("#hfItemCatId").val(item.data('catid'));
					$("#hfItemSubCatId").val(item.data('subcatid'));
					$("#hfItemDesc").val(item.data('desc'));
					$("#hfItemShortCode").val(item.data('code'));
					$("#hfItemUname").val(item.data('uname'));


					var freightStatus = $('input[name=inlineRadioOptions]:checked').val();

					if(freightStatus == 'self'){
						$('#txtFRate').val(item.data('self'));	
					}else if(freightStatus == 'bilty'){
						$('#txtFRate').val(item.data('bilty'));
					}else if(freightStatus == 'gari'){
						$('#txtFRate').val(item.data('gari'));
					}

					$("#txtItemId").val(item.data('code'));

					var itemId = item.data('item_id');
					var itemDesc = item.data('desc');
					var pRate = item.data('cost_price');
					var grWeight = item.data('grweight');
					var uomItem = item.data('uom_item');
					var stQty = item.data('stqty');
					var stWeight = item.data('stweight');
					var size = item.data('size');
					var brandId = item.data('bid');

					$('#stqty_lbl').text('Item,     Uom:' + uomItem);
					$('#itemDesc').val(itemDesc);
					$('#txtPRate').val(parseFloat(pRate).toFixed(2));
					$('#txtGrWeight').val(parseFloat(grWeight).toFixed(2));
					$('#txtUom').val(uomItem);
					$('#txtGWeight').trigger('input');	
				}
			});


			onChange(1);
            $("input[id$='txtItemSearch']").keydown(function(event){
                var keyCode = event.keyCode;
                if(keyCode == 13)
                {
                    onChange(1);
                    event.preventDefault();
                    return false;
                }
                else if(keyCode == 8 || keyCode == 46)
                {
                    var txt = $("input[id$='txtItemSearch']").val();
                    if(txt.length <= 1)
                    {
                        $("input[id$='txtItemSearch']").val('');
                        onChange(1);
                    }
                }
            });

			
		},

			initSaveAccount : function() {

			var saveObjAccount = getSaveObjectAccount();
			var error = validateSaveAccount();

			if (!error) {
					saveAccount(saveObjAccount);
			} else {
				alert('Correct the errors...');
			}
		},
		fetchRequestedVr : function () {
			var vrnoa = general.getQueryStringVal('vrnoa');
			vrnoa = parseInt( vrnoa );

			if ( !isNaN(vrnoa) ) {
				fetch(vrnoa);
			}else{
				getMaxId();
			}
		},


		// prepares the data to save it into the database
		initSave : function() {

			var error = validateSave();

			if (!error) {

				var rowsCount = $('#cash_table').find('tbody tr').length;

				if (rowsCount > 0 ) {
					var dcno =0;
					var saveObj = getSaveObject();
					if ($('#voucher_type_hidden').val() =='new'){
						  getMaxId();
					}
					dcno = $('#txtIdHidden').val();
					save( saveObj, dcno );
				} else {
					alert('No data found.');
				}
			} else {
				alert('Error! Enter required field............');
			}
		},
		bindModalPartyGrid : function() {

			
				            var dontSort = [];
				            $('#party-lookup table thead th').each(function () {
				                if ($(this).hasClass('no_sort')) {
				                    dontSort.push({ "bSortable": false });
				                } else {
				                    dontSort.push(null);
				                }
				            });
				            addjv.pdTable = $('#party-lookup table').dataTable({
				                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
				                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
				                "aaSorting": [[0, "asc"]],
				                "bPaginate": true,
				                "sPaginationType": "full_numbers",
				                "bJQueryUI": false,
				                "aoColumns": dontSort

				            });
				            $.extend($.fn.dataTableExt.oStdClasses, {
				                "s`": "dataTables_wrapper form-inline"
				            });
},

		initSearch : function() {

			var from = $('#from_date').val();
			var to = $('#to_date').val();
			search(from, to );
		},

		// resets the voucher to its default state
		resetVoucher : function() {

			// $('.inputerror').removeClass('inputerror');
			// $('#cur_date').datepicker('update', new Date());
			// $('#cash_table').find('tbody tr').remove();
			// $('#txtNetAmount').val('');
			// $('#search_cash_table').find('tbody tr').remove();

			// getMaxId('cpv');
			// general.setPrivillages();
			$('#txtIdHidden').val('0');
			$('#voucher_type_hidden').val('new');
			general.reloadWindow();
		}
	}

};

var addjv = new addjv();
addjv.init();