var Purchase = function() {

	var saveItem = function( item ) {
		$.ajax({
			url : base_url + 'index.php/item/save',
			type : 'POST',
			data : item,
			// processData : false,
			// contentType : false,
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					alert('Item saved successfully.');
					$('#ItemAddModel').modal('hide');
					fetchItems();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var saveAccount = function( accountObj ) {
		$.ajax({
			url : base_url + 'index.php/account/save',
			type : 'POST',
			data : { 'accountDetail' : accountObj },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'false') {
					alert('An internal error occured while saving account. Please try again.');
				} else {
					alert('Account saved successfully.');
					$('#AccountAddModel').modal('hide');
					fetchAccount();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

var fetchAccount = function() {

		$.ajax({
			url : base_url + 'index.php/account/fetchAll',
			type : 'POST',
			data : { 'active' : 1 },
			dataType : 'JSON',
			success : function(data) {
				if (data === 'false') {
					alert('No data found');
				} else {
					populateDataAccount(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
	var fetchItems = function() {
		$.ajax({
			url : base_url + 'index.php/item/fetchAll',
			type : 'POST',
			data : { 'active' : 1 },
			dataType : 'JSON',
			success : function(data) {
				if (data === 'false') {
					alert('No data found');
				} else {
					populateDataItem(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateDataAccount = function(data) {
		$("#party_dropdown").empty();
		
		$.each(data, function(index, elem){
			var opt="<option value='"+elem.party_id+"' >" +  elem.name + "</option>";
			$(opt).appendTo('#party_dropdown');
		});
	}
	var populateDataItem = function(data) {
		$("#itemid_dropdown").empty();
		$("#item_dropdown").empty();

		$.each(data, function(index, elem){
			var opt="<option value='"+elem.item_id+"' data-prate= '"+ elem.cost_price +"' data-uom_item= '"+ elem.uom +"' data-grweight= '"+ elem.grweight +"' >" +  elem.item_des + "</option>";
			 // var = "<option value='" + $item['item_id'] + "' data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>"><?php echo $item['item_des']; ?></option>";
			$(opt).appendTo('#item_dropdown');
			var opt1="<option value='"+elem.item_id+"' data-prate= '"+ elem.cost_price +"' data-uom_item= '"+ elem.uom +"' data-grweight= '"+ elem.grweight +"' >" +  elem.item_id + "</option>";
			 // var = "<option value='" + $item['item_id'] + "' data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>"><?php echo $item['item_des']; ?></option>";
			$(opt1).appendTo('#itemid_dropdown');

		});
	}
	var getSaveObjectAccount = function() {

		var obj = {
			pid : '20000',
			active : '1',
			name : $.trim($('#txtAccountName').val()),
			level3 : $.trim($('#txtLevel3').val()),
			dcno : $('#txtVrnoa').val(),
			etype : 'purchase',
			uid : $.trim($('#uid').val()),
			company_id : $.trim($('#cid').val()),
		};

		return obj;
	}
var getSaveObjectItem = function() {
		
		var itemObj = {
			item_id : 20000,
			active : '1',
			open_date : $.trim($('#current_date').val()),
			catid : $('#category_dropdown').val(),
			subcatid : $.trim($('#subcategory_dropdown').val()),
			bid : $.trim($('#brand_dropdown').val()),
			barcode : $.trim($('#txtBarcode').val()),
			description : $.trim($('#txtItemName').val()),
			item_des : $.trim($('#txtItemName').val()),
			cost_price : $.trim($('#txtPurPrice').val()),
			srate : $.trim($('#txtSalePrice').val()),
			uid : $.trim($('#uid').val()),
			company_id : $.trim($('#cid').val()),
		};
		return itemObj;
	}

	var validateSaveItem = function() {

		var errorFlag = false;
		// var _barcode = $('#txtBarcode').val();
		var _desc = $.trim($('#txtItemName').val());
		var cat = $.trim($('#category_dropdown').val());
		var subcat = $('#subcategory_dropdown').val();
		var brand = $.trim($('#brand_dropdown').val());

		// remove the error class first
		$('.inputerror').removeClass('inputerror');
		if ( _desc === '' || _desc === null ) {
			$('#txtItemName').addClass('inputerror');
			errorFlag = true;
		}
		if ( !cat ) {
			$('#category_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		if ( !subcat ) {
			$('#subcategory_dropdown').addClass('inputerror');
			errorFlag = true;
		}
		if ( !brand ) {
			$('#brand_dropdown').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}
	var validateSaveAccount = function() {

		var errorFlag = false;
		var partyEl = $('#txtAccountName');
		var deptEl = $('#txtLevel3');

		// remove the error class first
		$('.inputerror').removeClass('inputerror');

		if ( !partyEl.val() ) {
			$('#txtAccountName').addClass('inputerror');
			errorFlag = true;
		}
		if ( !deptEl.val() ) {
			deptEl.addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}
	var save = function(production) {
		general.disableSave();
		// alert(production.stockmain['company_id']);
		$.ajax({
			url : base_url + 'index.php/production/save',
			type : 'POST',
			data : { 'stockmain' : production.stockmain, 'stockdetail' : production.stockdetail, 'vrnoa' : production.vrnoa,'voucher_type_hidden':$('#voucher_type_hidden').val() },
			dataType : 'JSON',
			success : function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					var printConfirmation = confirm('Voucher saved!\nWould you like to print the invoice as well?');
					if (printConfirmation === true) {
						Print_Voucher(1);
					} else {
						general.reloadWindow();
					}
				}
				general.enableSave();
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}
var Print_Voucher = function(hd) {
		if ( $('.btnSave').data('printbtn')==0 ){
				alert('Sorry! you have not print rights..........');
		}else{
			var etype=  'production';
			var vrnoa = $('#txtVrnoa').val();
			var company_id = $('#cid').val();
			var user = $('#uname').val();
			// var hd = $('#hd').val();
			var pre_bal_print =  '0';
			
			var url = base_url + 'index.php/doc/print_pro_voucher/' + etype + '/' + vrnoa + '/' + company_id + '/' + '-1' + '/' + user + '/' + pre_bal_print+ '/' + hd;
			// var url = base_url + 'index.php/doc/CashVocuherPrintPdf/' + etype + '/' + dcno   + '/' + companyid + '/' + '-1' + '/' + user;
			window.open(url);
		}

	}
	var Print_Vouchers = function(hd) {
		if ( $('.btnSave').data('printbtn')==0 ){
				alert('Sorry! you have not print rights..........');
		}else{
			var etype=  'production';
			var vrnoa = $('#txtVrnoa').val();
			var company_id = $('#cid').val();
			var user = $('#uname').val();
			// var hd = $('#hd').val();
			var pre_bal_print =  '0';
			
			var url = base_url + 'index.php/doc/print_pro_vouchers/' + etype + '/' + vrnoa + '/' + company_id + '/' + '-1' + '/' + user + '/' + pre_bal_print+ '/' + hd;
			// var url = base_url + 'index.php/doc/CashVocuherPrintPdf/' + etype + '/' + dcno   + '/' + companyid + '/' + '-1' + '/' + user;
			window.open(url);
		}

	}
	var fetchThroughPO = function(poNo) {

		$.ajax({

			url : base_url + 'index.php/purchaseorder/fetch',
			type : 'POST',
			data : { 'vrnoa' : poNo },
			dataType : 'JSON',
			success : function(data) {

				$('#purchase_table').find('tbody tr').remove();
				if (data === 'false') {
					alert('No data found.');
				} else {
					populatePOData(data);
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populatePOData = function(data) {

		$.each(data, function(index, elem) {
			appendToTable('1', elem.item_name, elem.item_id, elem.godown_id,elem.dept_name,elem.uom, elem.item_qty, elem.weight, '');
			calculateNetQty(elem.item_qty,elem.weight);
		});
	}

	var fetch = function(vrnoa) {

		$.ajax({

			url : base_url + 'index.php/production/fetch',
			type : 'POST',
			data : { 'vrnoa' : vrnoa ,'company_id': $('#cid').val() },
			dataType : 'JSON',
			success : function(data) {

				$('#purchase_table').find('tbody tr').remove();
				$('#txtGQty').val('');
				$('#txtGWeight').val('');
				if (data === 'false') {
					alert('No data found.');
					general.reloadWindow();
				} else {
					populateData(data);
				}

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateData = function(data) {
		$('#voucher_type_hidden').val('edit');

		$('#txtVrnoHidden').val(data[0]['vrnoa']);
		$('#txtVrnoa').val(data[0]['vrnoa']);
		$('#txtVrnoaHidden').val(data[0]['vrnoa']);
		// alert(data[0]['vrdate']);
		// $('#current_date').datepicker('update', data[0]['vrdate']);
		$('#current_date').datepicker('update', data[0]['vrdate'].substr(0, 10));

		$('#receivers_list').val(data[0]['received_by']);
		$('#txtRemarks').val(data[0]['remarks']);

		$.each(data, function(index, elem) {
			
			appendToTable('1', elem.item_name, elem.item_id, elem.dept_name, elem.godown_id, elem.uom, elem.s_qty, elem.weight, elem.type);
			if(elem.type !='less')
			calculateNetQty(elem.s_qty,elem.weight);
		});
	}

	// gets the max id of the voucher
	var getMaxVrno = function() {

		$.ajax({

			url : base_url + 'index.php/production/getMaxVrno',
			type : 'POST',
			data : {'company_id':$('#cid').val()},
			dataType : 'JSON',
			success : function(data) {

				$('#txtVrno').val(data);
				$('#txtMaxVrnoHidden').val(data);
				$('#txtVrnoHidden').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var getMaxVrnoa = function() {

		$.ajax({

			url : base_url + 'index.php/production/getMaxVrnoa',
			type : 'POST',
			data : {'company_id':$('#cid').val()},
			dataType : 'JSON',
			success : function(data) {

				$('#txtVrnoa').val(data);
				$('#txtMaxVrnoaHidden').val(data);
				$('#txtVrnoaHidden').val(data);
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var validateSingleProductAdd = function() {


		var errorFlag = false;
		var item_id = $('#hfItemId').val();
		var qty = $('#txtSQty').val();
		var dept = $('#dept_dropdown').val();
		var job = $('#job_dropdown').val();

		// remove the error class first
		$('#txtItemId').removeClass('inputerror');
		$('#txtSQty').removeClass('inputerror');
		$('#dept_dropdown').removeClass('inputerror');
		$('#job_dropdown').removeClass('inputerror');

		if ( item_id === '' || item_id === null ) {
			$('#txtItemId').addClass('inputerror');
			errorFlag = true;
		}

		if ( qty === '' || qty === null ) {
			$('#txtSQty').addClass('inputerror');
			errorFlag = true;
		}

		if ( dept === '' || dept === null ) {
			$('#dept_dropdown').addClass('inputerror');
			errorFlag = true;
		}

		if ( job === '' || job === null ) {
			$('#job_dropdown').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var appendToTable = function(srno, item_desc, item_id, dept, dept_id, uom, qty , weight, type) {

		var srno = $('#purchase_table tbody tr').length + 1;

        if(type != 'less')
        {
            var row = "<tr>" +
                "<td class='srno'> " + srno + "</td>" +
                "<td class='item' data-item_id='" + item_id + "'> " + item_desc + "</td>" +
                "<td class='dept' data-dept_id='" + dept_id + "'> " + dept + "</td>" +
                "<td class='uom'> " + uom + "</td>" +
                "<td class='qty'> " + qty + "</td>" +
                "<td class='weight'> " + weight + "</td>" +
                "<td class='txtGrWeight hide'> " + type + "</td>" + //if received from btnadd then type = txtGrWeight.
                "<td><a href='' class='btn btn-primary btnRowEdit'><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td>" +
                "</tr>";
            $(row).appendTo('#purchase_table');
        }
	}

	var getSaveObject = function() {

		var stockmain = {};
		var stockdetail = [];

		stockmain.vrnoa = $('#txtVrnoaHidden').val();
		
		stockmain.remarks = $('#txtRemarks').val();
		stockmain.etype = 'production';
		stockmain.uid = $('#uid').val();
		stockmain.company_id = $('#cid').val();
        stockmain.vrdate = $('#current_date').val();
        stockmain.received_by = $('#receivers_list').val();


		$('#purchase_table').find('tbody tr').each(function( index, elem ) {
			var od = {};

			od.stdid = '';
            od.type = 'add';
			od.item_id = $.trim($(elem).find('td.item').data('item_id'));
			od.godown_id = $.trim($(elem).find('td.dept').data('dept_id'));
			od.qty = $.trim($(elem).find('td.qty').text());
			od.uom = $.trim($(elem).find('td.uom').text());
			od.weight = $.trim($(elem).find('td.weight').text());
			stockdetail.push(od);
		});

		var data = {};
		data.stockmain = stockmain;
		data.stockdetail = stockdetail;
		data.vrnoa = $('#txtVrnoaHidden').val();

		return data;
	}

	var deleteVoucher = function(vrnoa) {
		
		$.ajax({
			url : base_url + 'index.php/production/delete',
			type : 'POST',
			data : { 'vrnoa' : vrnoa,'company_id': $('#cid').val() },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					alert('Voucher deleted successfully');
					general.reloadWindow();
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}


	var calculateNetQty = function(qty, weight) {

		var _qty = ($('#txtGQty').val() == "") ? 0 : $('#txtGQty').val();
		var _weight = ($('#txtGWeight').val() == "") ? 0 : $('#txtGWeight').val();

		var tempQty = parseFloat(_qty) + parseFloat(qty);
		var tempWeight = parseFloat(_weight) + parseFloat(weight);
		$('#txtGQty').val(parseFloat(tempQty).toFixed(2));
		$('#txtGWeight').val(parseFloat(tempWeight).toFixed(2));
	}

	var calculateUpperTotal = function() {

		var job_cost = ($('#job_dropdown').val() == '') ? 0 : $('#job_dropdown').find('option:selected').data('job_cost');
		var qty = ($('#txtSQty').val() == '') ? 0 : $('#txtSQty').val();
		$('#txtCost').val(job_cost);
		var net = parseFloat(job_cost) * parseFloat(qty);
		$('#txtAmpunt').val(net);
	}

	var paging = function (total)
	{
    	$("#paging").smartpaginator({
        	totalrecords: parseInt(total),
        	recordsperpage: 10,
        	datacontainer: 'tbody',
        	dataelement: 'tr',
        	theme: 'custom',
        	onchange: onChange
    	});
	}

	
	var last_stockLocatons = function(item_id) {
		$.ajax({

			url : base_url + 'index.php/saleorder/last_stocklocatons',
			type : 'POST',
			data : { 'item_id' : item_id , 'company_id': $('#cid').val(),'etype':'sale'},
			dataType : 'JSON',
			
			success : function(data) {
				console.log(data);

				$('#laststockLocation_table tbody').html('');
				var total_qty = 0;
				if(data != false && data != 'false'){

					$.each(data, function(index, elem) {
						total_qty += parseInt(elem.qty);
						appendTo_last_stockLocatons(elem.location, elem.qty);
					});
				}
				
				last_stockLocatons_total(total_qty);

			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}    

	var appendTo_last_stockLocatons = function(location, qty) {

		var row = 	"<tr>" +
						"<td class='location numeric' data-title='location' > "+ location +"</td>" +
					 	"<td class='qty numeric' data-title='qty'> "+ qty +"</td>" +
				 	"</tr>";
		
			$(row).appendTo('#laststockLocation_table tbody');
		
	}

	var last_stockLocatons_total = function(total_qty){
		var row = "<tr>" +
					"<td class='location numeric' data-title='location' >Total</td>" +
				 	"<td class='qty numeric' data-title='qty'> "+ total_qty +"</td>" +
				"</tr>";
		$(row).appendTo('#laststockLocation_table tbody');
	}




	var onChange = function (newPageValue)
    {
        $.ajax({
            type: "POST",
            url: base_url + 'index.php/item/paging',
            data: {
                page_number: newPageValue,
                search: $.trim($("#txtItemSearch").val())
            }
        }).done(function (result) {

            var total = "";
            var jsonR = JSON.stringify($.parseJSON(result)).replace(/\\n/g, "\\n")
                                        .replace(/\\'/g, "\\'")
                                        .replace(/\\"/g, '\\"')
                                        .replace(/\\&/g, "\\&")
                                        .replace(/\\r/g, "\\r")
                                        .replace(/\\t/g, "\\t")
                                        .replace(/\\b/g, "\\b")
                                        .replace(/\\f/g, "\\f");
            var json = $.parseJSON(jsonR);
            total = json.total;

            if (json.records != false) {
                var html = "";
                $.each(json.records, function (index, value) {
                    html += "<tr id='" + value.item_id + "'>";
                    html += "<td>"+value.item_id+"<input type='hidden' name='hfModalitemId' value='" + value.item_id + "'></td>";
                    html += "<td class='tdItemCode'><b>" + value.item_code + "<b></td>";
                    html += "<td class='tdDescription'><b>" + value.item_des + "</b></td>";
                    html += "<td class='tdItemUom'><b>" + value.uom + "</b></td>";
                    html += "<td class='tdPRate hide'>" + value.cost_price + "</td>";
                    html += "<td class='tdGrWeight hide'>" + value.grweight + "</td>";
                    html += "<td class='tdStQty hide'>" + value.stqty + "</td>";
                    html += "<td class='tdStWeight hide'>" + value.stweight + "</td>";
                    html += "<td class='tdUItem hide'>" + value.uitem_des + "</td>";
                    html += "<td><a href='#' data-dismiss='modal' class='btn btn-primary populateItem'><i class='fa fa-search'></i></a></td>"
                    html += "</tr>";
                });

                $("#divNoRecord").removeClass('divNoRecord');
                $("#divNoRecord").html('');
                $("#tbItems > tbody").html('');
                $("#tbItems > tbody").append(html);

                $('.modal-lookup .populateItem').on('click', function()
                {
                    var itemId = $(this).closest('tr').find('input[name=hfModalitemId]').val();
                    var itemDescription = $(this).closest('tr').find('.tdDescription').text();
                    var uom = $(this).closest('tr').find('.tdItemUom').text();
                    var pRate = $(this).closest('tr').find('.tdPRate').text();
                    var grWeight = $(this).closest('tr').find('.tdGrWeight').text();
                    var stQty = $(this).closest('tr').find('.tdStQty').text();
                    var stWeight = $(this).closest('tr').find('.tdStWeight').text();
                    var uItemName = $(this).closest('tr').find('.tdUItem').text();
                    var itemCode = $(this).closest('tr').find('.tdItemCode').text();

                    var itemCodeOption = "<option value='" + itemId + "' data-uom_item='" + uom + "' data-prate='" + pRate + "' data-grweight='" + grWeight + "' data-stqty='" + stQty + "' data-stweight='" + stWeight + "' data-uitem_des='" + uItemName + "' >" + itemCode + "</option>";
                    var itemOption = "<option value='" + itemId + "'  data-uom_item='" + uom + "' data-prate='" + pRate + "' data-grweight='" + grWeight + "' data-stqty='" + stQty + "' data-stweight='" + stWeight + "'  data-uitem_des='" + uItemName + "' >" + itemDescription + "</option>";
                    $('#itemid_dropdown').append(itemCodeOption);
                    $('#item_dropdown').append(itemOption);

                    $('#itemid_dropdown').val(itemId).trigger('change');
                    $('#item_dropdown').val(itemId).trigger('change');

                    $('#txtPRate').val(parseFloat(pRate).toFixed(2));
                    $('#txtGrWeight').val(parseFloat(grWeight).toFixed(2));

                    $('#txtQty').focus();
                });

                $("#paging").show();
                $("#tbItems").show();
                if (newPageValue == "1") {
                    paging(total);
                }
            }
            else {
                $("#tbItems > tbody").html('');
                $("#divNoRecord").show();
                $("#divNoRecord").addClass('divNoRecord');
                $("#paging").hide();
                $("#divNoRecord").html('No Record Found');
            }
        });
    }

    var getItemInfo = function(itemId)
    {
        $.ajax({
            type: "POST",
            url: base_url + 'index.php/item/getiteminfo',
            data: {
                item_id: itemId
            }
        }).done(function (result) {

            var jsonR = JSON.stringify($.parseJSON(result)).replace(/\\n/g, "\\n")
                                    .replace(/\\'/g, "\\'")
                                    .replace(/\\"/g, '\\"')
                                    .replace(/\\&/g, "\\&")
                                    .replace(/\\r/g, "\\r")
                                    .replace(/\\t/g, "\\t")
                                    .replace(/\\b/g, "\\b")
                                    .replace(/\\f/g, "\\f");
            var json = $.parseJSON(jsonR);

            if (json.records != false)
            {
                $.each(json.records, function (index, value) {
                    var itemCodeOption = "<option value='" + value.item_id + "' data-uom_item='" + value.uom + "' data-data-prate='" + value.cost_price + "' data-grweight='" + value.grweight + "' data-stqty='" + value.stqty + "' data-stweight='" + value.stweight + "' >" + value.item_code + "</option>";
                    var itemOption = "<option value='" + value.item_id + "'  data-uom_item='" + value.uom + "' data-data-prate='" + value.cost_price + "' data-grweight='" + value.grweight + "' data-stqty='" + value.stqty + "' data-stweight='" + value.stweight + "' >" + value.item_des + "</option>";
                    $('#itemid_dropdown').append(itemCodeOption);
                    $('#item_dropdown').append(itemOption);

                    $('#itemid_dropdown').select2('val', itemId);
                    $('#item_dropdown').select2('val', itemId);

                    var grweight = $('#item_dropdown').find('option:selected').data('grweight');
                    //$('#txtGWeight').val(parseFloat(grweight).toFixed());
                });
            }
        });
    }

    var clearItemData = function (){
		
		$("#hfItemId").val("");
		$("#hfItemSize").val("");
		$("#hfItemBid").val("");
		$("#hfItemUom").val("");
		$("#hfItemPrate").val("");
		$("#hfItemGrWeight").val("");
		$("#hfItemStQty").val("");
		$("#hfItemStWeight").val("");
		$("#hfItemLength").val("");
		$("#hfItemCatId").val("");
		$("#hfItemSubCatId").val("");
		$("#hfItemDesc").val("");
		$("#hfItemShortCode").val("");

        //$("#txtItemId").val("");
    }


	return {

		init : function() {
			$('#voucher_type_hidden').val('new');
			this.bindUI();
		},

		bindUI : function() {
			var self = this;


			$('#receivers_list').focus().css('border', '1px solid #368EE0');
			

			$('#txtLevel3').on('change', function() {
				
				var level3 = $('#txtLevel3').val();
				$('#txtselectedLevel1').text('');
				$('#txtselectedLevel2').text('');
				if (level3 !== "" && level3 !== null) {
					// alert('enter' + $(this).find('option:selected').data('level2') );	
					$('#txtselectedLevel2').text(' ' + $(this).find('option:selected').data('level2'));
					$('#txtselectedLevel1').text(' ' + $(this).find('option:selected').data('level1'));
				}
			});
			// $('#txtLevel3').select2();
			$('.btnSaveM').on('click',function(e){
				if ( $('.btnSave').data('saveaccountbtn')==0 ){
					alert('Sorry! you have not save accounts rights..........');
				}else{
					e.preventDefault();
					self.initSaveAccount();
				}
			});
			$('.btnPrint').on('click',  function(e) {
				e.preventDefault();
				Print_Voucher(1);
			});
			$('.btnPrints').on('click',  function(e) {
				e.preventDefault();
				Print_Vouchers(0);
			});
			$('.btnprintwithOutHeader').on('click', function(e) {
				e.preventDefault();
				Print_Voucher(0);
			});
			$('.btnResetM').on('click',function(){
				
				$('#txtAccountName').val('');
				$('#txtselectedLevel2').text('');
				$('#txtselectedLevel1').text('');
				$('#txtLevel3').select2('val','');
			});
			$('#AccountAddModel').on('shown.bs.modal',function(e){
				$('#txtAccountName').focus();
			});
			shortcut.add("F3", function() {
    			$('#AccountAddModel').modal('show');
			});

			$('.btnSaveMItem').on('click',function(e){
				if ( $('.btnSave').data('saveitembtn')==0 ){
					alert('Sorry! you have not save item rights..........');
				}else{
					e.preventDefault();
					self.initSaveItem();
				}
			});

			$('.btnSave').on('click',  function(e) {
				if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('updatebtn')==0 ){
					alert('Sorry! you have not update rights..........');
				}else if($('#voucher_type_hidden').val()=='new' && $('.btnSave').data('insertbtn')==0){
					alert('Sorry! you have not insert rights..........');
				}else{
					e.preventDefault();
					self.initSave();
				}
			});

			$('.btnReset').on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});

			$('.btnDelete').on('click', function(e){
				e.preventDefault();
				var vrnoa = $('#txtVrnoa').val();
				if (vrnoa !== '') {
					if ($('.btnSave').data('deletebtn')==0)
					{
						alert('Sorry! you have no Delete rights..........');
					}
					else
					{
						if (confirm('Are you sure to delete this voucher?'))
						deleteVoucher(vrnoa);
					}
					
				}
			});


			$('#item_dropdown').on('change', function(){
				var item_id = $(this).find('option:selected').val();
				last_stockLocatons(item_id);
			});


			$('#job_dropdown').on('change', function(){
				calculateUpperTotal();
			});
			$('#txtSQty').on('input', function() {
				calculateUpperTotal();
			});
			shortcut.add("F10", function() {
    			self.initSave();
			});
			shortcut.add("F2", function() {
				$('a[href="#item-lookup"]').trigger('click');
			});
			shortcut.add("F9", function() {
				Print_Voucher();
			});
			shortcut.add("F6", function() {
    			$('#txtVrnoa').focus();
    			// alert('focus');
			});
			shortcut.add("F5", function() {
    			self.resetVoucher();
			});

			shortcut.add("F12", function() {
    			var vrnoa = $('#txtVrnoa').val();
				if (vrnoa !== '') {
					deleteVoucher(vrnoa);
				}
			});


			/////////////////////////////////////////////////////////////////
			/// setting calculations for the single product
			/////////////////////////////////////////////////////////////////

			$('#itemid_dropdown').on('change', function() {
				var item_id = $(this).val();
				var uom = $(this).find('option:selected').data('uom_item');
				$('#item_dropdown').select2('val', item_id);
				$('#txtUOM').val(uom);
				var stqty = $(this).find('option:selected').data('stqty');
                var stweight = $(this).find('option:selected').data('stweight');
                $('#stqty_lbl').text('Item,     Qty:' + stqty + ', Weight ' + stweight);
                
			});
			$('#item_dropdown').on('change', function() {
				var item_id = $(this).val();
				var uom = $(this).find('option:selected').data('uom_item');
				$('#itemid_dropdown').select2('val', item_id);
				$('#txtUOM').val(uom);
				var stqty = $(this).find('option:selected').data('stqty');
                var stweight = $(this).find('option:selected').data('stweight');
                $('#stqty_lbl').text('Item,     Qty:' + stqty + ', Weight ' + stweight);
			});


			$('#btnAdd').on('click', function(e) {
				e.preventDefault();

				var error = validateSingleProductAdd();
				if (!error) {

					var item_desc = $('#itemDesc').val();
					var uitem_des = $('#hfItemUname').val();
					var uom_item = $('#hfItemUom').val();
					var item_code = $('#hfItemShortCode').val();
					var item_id = $('#hfItemId').val();
					var dept = $('#dept_dropdown').find('option:selected').text();
					var dept_id = $('#dept_dropdown').val();
					var qty = $('#txtSQty').val();
					var uom = $('#txtUOM').val();
					var weight = $('#txtWeight').val();
					var txtGrWeight = $('#txtGrWeight').val();
					

					// reset the values of the annoying fields
					$('#itemDesc').val('');
					$('#txtItemId').val('');
					$('#hfItemId').val('');
					$('#txtSQty').val('');
					$('#txtUOM').val('');
					$('#dept_dropdown').select2('val', '');
					$('#txtWeight').val('');
					$('#stqty_lbl').text('Item');
					$('#txtGrWeight').val('');

					appendToTable('1', item_desc, item_id, dept, dept_id,uom, qty, weight, txtGrWeight);
					calculateNetQty(qty , weight);
					$('#txtItemId').focus();
				} else {
					alert('Correct the errors!');
				}

			});

			// when btnRowRemove is clicked
			$('#purchase_table').on('click', '.btnRowRemove', function(e) {
				e.preventDefault();
				var qty = $.trim($(this).closest('tr').find('td.qty').text());
				calculateNetQty("-"+qty);
				$(this).closest('tr').remove();
			});
			$('#purchase_table').on('click', '.btnRowEdit', function(e) {
				e.preventDefault();

				// getting values of the cruel row
				var item_id = $.trim($(this).closest('tr').find('td.item').data('item_id'));
				var qty = $.trim($(this).closest('tr').find('td.qty').text());
				var weight = $.trim($(this).closest('tr').find('td.weight').text());
				var dept_id = $.trim($(this).closest('tr').find('td.dept').data('dept_id'));
				var uom = $.trim($(this).closest('tr').find('td.uom').text());
				var txtGrWeight = $.trim($(this).closest('tr').find('td.txtGrWeight').text());
				$.ajax({
					type: "POST",
					url: base_url + 'index.php/item/getiteminfobyid',
					data: {
						item_id: item_id
					}
				}).done(function (result) {

					$("#imgItemLoader").hide();
					var item = $.parseJSON(result);

					if (item != false)
					{
						$("#hfItemId").val(item[0].item_id);
						$("#hfItemSize").val(item[0].size);
						$("#hfItemBid").val(item[0].bid);
						$("#hfItemUom").val(item[0].uom);
						$("#hfItemPrate").val(item[0].cost_price);
						$("#hfItemGrWeight").val(item[0].grweight);
						$("#hfItemStQty").val(item[0].stqty);
						$("#hfItemStWeight").val(item[0].stweight);
						$("#hfItemLength").val(item[0].length);
						$("#hfItemCatId").val(item[0].catid);
						$("#hfItemSubCatId").val(item[0].subcatid);
						$("#hfItemDesc").val(item[0].item_des);
						$("#hfItemShortCode").val(item[0].short_code);
						$("#hfItemUname").val(item[0].uname);


						$("#txtItemId").val(item[0].item_code);
						$('#itemDesc').val(item[0].item_des);
						$('#txtUom').val(item[0].uom);
						
						$('#txtSQty').val(qty);
						$('#dept_dropdown').select2('val', dept_id);
						$('#txtUOM').val(uom);
						$('#txtWeight').val(weight);
						$('#txtGrWeight').val(txtGrWeight);
						
						
						$('#stqty_lbl').text('Item,     Uom:' + item[0].uom);


						calculateNetQty("-"+qty ,"-"+weight);
                        // now we have get all the value of the row that is being deleted. so remove that cruel row

                    }
                });

				// $('#itemid_dropdown').select2('val', item_id);
				// $('#item_dropdown').select2('val', item_id);

				// now we have get all the value of the row that is being deleted. so remove that cruel row
				$(this).closest('tr').remove();	// yahoo removed
			});
		
			$('#txtVrnoa').on('change', function() {
				fetch($(this).val());
			});
			$('.btn').focus(function(){
				$(this).css('border', '2px solid black');
			}
			);
			$('.btn').blur(function(){
				$(this).css('border', '');
			}
			);

			$('#txtItemId').on('change', function(){
				var item_id = $('#hfItemId').val();
				last_stockLocatons(item_id);
			});

			var countItem = 0;
			$('input[id="txtItemId"]').autoComplete({

				minChars: 1,
				cache: false,
				menuClass: '',
				source: function(search, response)
				{
					try { xhr.abort(); } catch(e){}
					$('#txtItemId').removeClass('inputerror');
					$("#imgItemLoader").hide();
					if(search != "")
					{
						xhr = $.ajax({
							url: base_url + 'index.php/item/searchitem',
							type: 'POST',
							data: {

								search: search
							},
							dataType: 'JSON',
							beforeSend: function (data) {

								$(".loader").hide();
								$("#imgItemLoader").show();
								countItem = 0;
							},
							success: function (data) {

								if(data == '')
								{
									$('#txtItemId').addClass('inputerror');
									clearItemData();
									$('#itemDesc').val('');
									$('#txtQty').val('');
									$('#txtPRate').val('');
									$('#txtBundle').val('');
									$('#txtGBundle').val('');
									$('#txtWeight').val('');
									$('#txtAmount').val('');
									$('#txtGWeight').val('');
									$('#txtDiscp').val('');
									$('#txtDiscount1_tbl').val('');
								}
								else
								{
									$('#txtItemId').removeClass('inputerror');
									response(data);
									$("#imgItemLoader").hide();
								}
							}
						});
					}
					else
					{
						clearItemData();
					}
				},
				renderItem: function (item, search)
				{
					var sea = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
					var re = new RegExp("(" + sea.split(' ').join('|') + ")", "gi");

					var selected = "";
					if((search.toLowerCase() == (item.item_des).toLowerCase() && countItem == 0) || (search.toLowerCase() != (item.item_des).toLowerCase() && countItem == 0))
					{
						selected = "selected";
					}
					countItem++;
					clearItemData();

					return '<div class="autocomplete-suggestion ' + selected + '" data-val="' + search + '" data-item_id="' + item.item_id + '" data-size="' + item.pack + '" data-bid="' + item.bid +
					'" data-uom_item="'+ item.uom + '" data-srate="' + item.srate + '" data-grweight="' + item.grweight + '" data-stqty="' + item.stqty +
					'" data-stweight="' + item.stweight + '" data-length="' + item.length  + '" data-catid="' + item.catid +
					'" data-subcatid="' + item.subcatid + '" data-desc="' + item.item_des + '" data-cost_price="' + item.cost_price + '" data-code="' + item.item_code + '" data-self="' + item.self_freight_rate + '" data-bilty="' + item.bilty_freight_rate + 
					'" data-gari="' + item.gari_freight_rate + '">' + item.item_des.replace(re, "<b>$1</b>") + '</div>';
				},
				onSelect: function(e, term, item)
				{

					$("#imgItemLoader").hide();
					$("#hfItemId").val(item.data('item_id'));
					$("#hfItemSize").val(item.data('size'));
					$("#hfItemBid").val(item.data('bid'));
					$("#hfItemUom").val(item.data('uom_item'));
					$("#hfItemPrate").val(item.data('cost_price'));
					$("#hfItemGrWeight").val(item.data('grweight'));
					$("#hfItemStQty").val(item.data('stqty'));
					$("#hfItemStWeight").val(item.data('stweight'));
					$("#hfItemLength").val(item.data('length'));
					$("#hfItemCatId").val(item.data('catid'));
					$("#hfItemSubCatId").val(item.data('subcatid'));
					$("#hfItemDesc").val(item.data('desc'));
					$("#hfItemShortCode").val(item.data('code'));
					$("#hfItemUname").val(item.data('uname'));


					var freightStatus = $('input[name=inlineRadioOptions]:checked').val();

					if(freightStatus == 'self'){
						$('#txtFRate').val(item.data('self'));	
					}else if(freightStatus == 'bilty'){
						$('#txtFRate').val(item.data('bilty'));
					}else if(freightStatus == 'gari'){
						$('#txtFRate').val(item.data('gari'));
					}

					$("#txtItemId").val(item.data('code'));

					var itemId = item.data('item_id');
					var itemDesc = item.data('desc');
					var pRate = item.data('cost_price');
					var grWeight = item.data('grweight');
					var uomItem = item.data('uom_item');
					var stQty = item.data('stqty');
					var stWeight = item.data('stweight');
					var size = item.data('size');
					var brandId = item.data('bid');

					$('#stqty_lbl').text('Item,     Uom:' + uomItem);
					$('#itemDesc').val(itemDesc);
					$('#txtPRate').val(parseFloat(pRate).toFixed(2));
					$('#txtGrWeight').val(parseFloat(grWeight).toFixed(2));
					$('#txtUOM').val(uomItem);
					$('#txtGWeight').trigger('input');	
				}
			});

			$('#txtSQty').change(function(){
				var Qty = $(this).val();
				var GrWeight = $('#txtGrWeight').val();
				var totalWeight = Qty * GrWeight;
				$('#txtWeight').val(totalWeight);
			});
			$('#txtVrnoa').on('keypress', function(e) {

				if (e.keyCode === 13) {
					e.preventDefault();
					var vrnoa = $('#txtVrnoa').val();
					if (vrnoa !== '') {
						fetch(vrnoa);
					}
				}
			});

			$('#txtPoNo').on('keypress', function(e) {
				if (e.keyCode === 13) {
					e.preventDefault();
					var poNo = $('#txtPoNo').val();
					if (poNo !== '') {
						fetchThroughPO(poNo);
					}
				}
			});

			onChange(1);
            $("input[id$='txtItemSearch']").keydown(function(event){
                var keyCode = event.keyCode;
                if(keyCode == 13)
                {
                    onChange(1);
                    event.preventDefault();
                    return false;
                }
                else if(keyCode == 8 || keyCode == 46)
                {
                    var txt = $("input[id$='txtItemSearch']").val();
                    if(txt.length <= 1)
                    {
                        $("input[id$='txtItemSearch']").val('');
                        onChange(1);
                    }
                }
            });

			purchase.fetchRequestedVr();
		},

		// prepares the data to save it into the database
		initSave : function() {

			var saveObj = getSaveObject();

			var rowsCount = $('#purchase_table').find('tbody tr').length;

			if (rowsCount > 0 ) {
				save(saveObj);
			} else {
				alert('No data found to save!');
			}
		},
		fetchRequestedVr : function () {

		var vrnoa = general.getQueryStringVal('vrnoa');
		vrnoa = parseInt( vrnoa );
		$('#txtVrnoa').val(vrnoa);
		$('#txtVrnoaHidden').val(vrnoa);
		if ( !isNaN(vrnoa) ) {
			fetch(vrnoa);
		}else{
			getMaxVrno();
			getMaxVrnoa();
		}
	},
			initSaveAccount : function() {

			var saveObjAccount = getSaveObjectAccount();
			var error = validateSaveAccount();

			if (!error) {
					saveAccount(saveObjAccount);
			} else {
				alert('Correct the errors...');
			}
		},
		initSaveItem : function() {

			var saveObjItem = getSaveObjectItem();
			var error = validateSaveItem();

			if (!error) {
					saveItem(saveObjItem);
			} else {
				alert('Correct the errors...');
			}
		},

				bindModalPartyGrid : function() {

			
				            var dontSort = [];
				            $('#party-lookup table thead th').each(function () {
				                if ($(this).hasClass('no_sort')) {
				                    dontSort.push({ "bSortable": false });
				                } else {
				                    dontSort.push(null);
				                }
				            });
				            purchase.pdTable = $('#party-lookup table').dataTable({
				                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
				                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
				                "aaSorting": [[0, "asc"]],
				                "bPaginate": true,
				                "sPaginationType": "full_numbers",
				                "bJQueryUI": false,
				                "aoColumns": dontSort

				            });
				            $.extend($.fn.dataTableExt.oStdClasses, {
				                "s`": "dataTables_wrapper form-inline"
				            });
},

bindModalItemGrid : function() {

			
				            var dontSort = [];
				            $('#item-lookup table thead th').each(function () {
				                if ($(this).hasClass('no_sort')) {
				                    dontSort.push({ "bSortable": false });
				                } else {
				                    dontSort.push(null);
				                }
				            });
				            purchase.pdTable = $('#item-lookup table').dataTable({
				                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
				                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
				                "aaSorting": [[0, "asc"]],
				                "bPaginate": true,
				                "sPaginationType": "full_numbers",
				                "bJQueryUI": false,
				                "aoColumns": dontSort

				            });
				            $.extend($.fn.dataTableExt.oStdClasses, {
				                "s`": "dataTables_wrapper form-inline"
				            });
},

		// instead of reseting values reload the page because its cruel to write to much code to simply do that
		resetVoucher : function() {
			general.reloadWindow();
		}
	}

};

var purchase = new Purchase();
purchase.init();