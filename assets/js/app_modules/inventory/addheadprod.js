var headprod = function () {


    var getmaxvrnoa = function () {
        $.ajax({
            url: base_url + 'index.php/headprod/getmaxvrnoa',
            type: 'POST',
            data: {'company_id': $('#cid').val()},
            dataType: 'JSON',
            success: function (data) {
                $('#txtVrnoa').val(data);
                $('#txtMaxVrnoaHidden').val(data);
                $('#txtVrnoaHidden').val(data);
                $('#txtIdImg').val(data);
            }, error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    var validateSingleProductAdd = function () {


        var errorFlag = false;

        var item_id = $('#hfItemId').val();
        var item_qty = $('#txtSQty').val();
        var godown = $('#dept_dropdown').find('option:selected').val();

        // remove the error class first
        $('#txtItemId').removeClass('inputerror');
        $('#item_dropdown').removeClass('inputerror');
        $('#txtSQty').removeClass('inputerror');
        $('#dept_dropdown').removeClass('inputerror');

        if (item_id === '' || item_id === null) {
            $('#txtItemId').addClass('inputerror').focus();
            errorFlag = true;
        }

        if (item_qty === '' || item_qty === null) {
            $('#txtSQty').addClass('inputerror').focus();
            errorFlag = true;
        }

        if (godown === '' || godown === null) {
            $('#dept_dropdown').addClass('inputerror').focus();
            errorFlag = true;
        }

        return errorFlag;
    }
    var getNumVal = function (el) {
        return isNaN(parseFloat(el.val())) ? 0 : parseFloat(el.val());
    }

    var calculateLowerTotal = function (qty,weight) {
        var _qty = getNumVal($('#txtTotalQty'));
        var tempQty = parseFloat(_qty) + parseFloat(qty);
        $('#txtTotalQty').val(tempQty);
        var _Weight = getNumVal($('#txtTotalWeight'));
        var tempWeight = parseFloat(_Weight) + parseFloat(weight);
        $('#txtTotalWeight').val(tempWeight);
    }

    var appendToTable = function (srno, item_id, item_code, item_desc, item_qty, item_weight) {

        var srno = $('#item_table tbody tr').length + 1;
        if (item_weight === null) {
            item_weight = "-";
        }

        var row = "<tr>" +
            "<td class='srno'> " + srno + "</td>" +
            "<td class='item' data-item_id='" + item_id + "'> " + item_code + "</td>" +
            "<td class='item_desc'> " + item_desc + "</td>" +
            "<td class='qty' id='item_id_" + item_id + "'> " + item_qty + "</td>" +
            "<td class='weight' id='item_id_" + item_weight + "'> " + item_weight + "</td>" +
            "<td><a href='' class='btn btn-primary btnRowEdit'><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td>" +
            "</tr>";
        $(row).appendTo('#item_table tbody');

    }

    var getOptions = function(empData) {
        
        var options = "";
        $.each(empData, function(index, elem){
            options += "<option value='"+elem['emp_id']+"'>"+elem['emp_name']+"</option>";
        });
        
        return options;
    }
    

    var appendOption = function(empData) {
        // $('.emps').empty();
        var options = getOptions(empData);
        
        $('.emps').each(function(){
        
            $(this).append(options);
        });
        
    }

    var appendToStaff = function (srno, itemIds, item_des, pid, name, rate, qty, weight, phaseId, perday, empData,sts='') {
        // console.log(empData);
        if (weight === null) {
            weight = "-";
        }
        var srno = '';
        if (phaseId == 1) {
            srno = $('#staff_tableF tbody tr').length + 1;
        }
        else if (phaseId == 2) {
            srno = $('#staff_tableC tbody tr').length + 1;
        }
        else if (phaseId == 3) {
            srno = $('#staff_tableG tbody tr').length + 1;
        }
        else if (phaseId == 4) {
            srno = $('#staff_tableW tbody tr').length + 1;
        }
        else if (phaseId == 5) {
            srno = $('#staff_tableM1 tbody tr').length + 1;
        }
        else if (phaseId == 6) {
            srno = $('#staff_tableM2 tbody tr').length + 1;
        }
        else if (phaseId == 7) {
            srno = $('#staff_tableM3 tbody tr').length + 1;
        }
        else if (phaseId == 8) {
            srno = $('#staff_tableM4 tbody tr').length + 1;
        }
        else if (phaseId == 9) {
            srno = $('#staff_tableM5 tbody tr').length + 1;
        }
        else if (phaseId == 10) {
            srno = $('#staff_tableM6 tbody tr').length + 1;
        }
        else if (phaseId == 11) {
            srno = $('#staff_tableM7 tbody tr').length + 1;
        } else if (phaseId == 12) {
            srno = $('#staff_tableM8 tbody tr').length + 1;
        }

        
        var amount = rate * qty;
        if(sts !=='Present' && sts !='') 
        {
         var row = "<tr>" +
            "<td class='srno bg-red'> " + srno + "</td>" +
            "<td class='item_des bg-red' data-item_id='" + itemIds + "'> " + item_des + "</td>" +
            "<td class='name bg-red' id='"+pid+"' data-pid=' " + pid + " '><select class='form-control emps' id='emp_"+pid+"'   ></select></td>" +
            "<td class='qty bg-red'> <input type='text' class='form-control qty' value='" + qty + "' /></td>" +
            "<td class='weight bg-red'> " + weight + "</td>" +
            "<td class='rate bg-red'> " + rate + "</td>" +
            "<td class='amount bg-red'> " + amount.toFixed(2) + "</td>" +
            "<td class='perday hide'> " + perday + "</td>" +
            "<td > <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td>" +
            "</tr>";  
        }
        else
        {
            var row = "<tr>" +
            "<td class='srno'> " + srno + "</td>" +
            "<td class='item_des' data-item_id='" + itemIds + "'> " + item_des + "</td>" +
            "<td class='name' id='"+pid+"' data-pid=' " + pid + " '><select class='form-control emps' id='emp_"+pid+"'   ></select></td>" +
            "<td class='qty'> <input type='text' class='form-control qty' value='" + qty + "' /></td>" +
            "<td class='weight'> " + weight + "</td>" +
            "<td class='rate'> " + rate + "</td>" +
            "<td class='amount'> " + amount.toFixed(2) + "</td>" +
            "<td class='perday hide'> " + perday + "</td>" +
            "<td> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td>" +
            "</tr>";
       }

        
        if (phaseId == 1) {
            $(row).appendTo('#staff_tableF');
        }
        else if (phaseId == 2) {
            $(row).appendTo('#staff_tableC');
        } else if (phaseId == 3) {
            $(row).appendTo('#staff_tableG');
        }
         else if (phaseId == 4) {
            $(row).appendTo('#staff_tableW');
        } else if (phaseId == 5) {
            $(row).appendTo('#staff_tableM1');
        } else if (phaseId == 6) {
            $(row).appendTo('#staff_tableM2');
        } else if (phaseId == 7) {
            $(row).appendTo('#staff_tableM3');
        } else if (phaseId == 8) {
            $(row).appendTo('#staff_tableM4');
        } else if (phaseId == 9) {
            $(row).appendTo('#staff_tableM5');
        }
        else if (phaseId == 10) {
            $(row).appendTo('#staff_tableM6');
        } else if (phaseId == 11) {
            $(row).appendTo('#staff_tableM7');
        } else if (phaseId == 12) {
            $(row).appendTo('#staff_tableM8');
        }
        else {
            alert('no data found');
        }

    }

    var validateentrycalculations =function()
    {
        var amount = 0;
        var fitting = 0;
        var helping1 = 0;
        var helping2 = 0;
        var cleaning=0;
        var grinding=0;
        var warma=0;
        var machine1 =0;
        var machine2 =0;
        var machine3 =0;
        var machine4 =0;
        var machine5 =0;
        var machine6 =0;



        $('#staff_tableF').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            fitting +=parseFloat($(elem).val());
        });
        $('#staff_tableC').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            cleaning += parseFloat($(elem).val());
        });
        $('#staff_tableG').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            grinding += parseFloat($(elem).val());
        });
        $('#staff_tableW').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            warma += parseFloat($(elem).val());
        });
        $('#staff_tableM1').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            machine1 += parseFloat($(elem).val());
        });
        $('#staff_tableM2').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            machine2 += parseFloat($(elem).val());
        });
        $('#staff_tableM3').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            machine3 += parseFloat($(elem).val());
        });
        $('#staff_tableM4').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            machine4 += parseFloat($(elem).val());
        });
        $('#staff_tableM5').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            machine5 += parseFloat($(elem).val());
        });
        $('#staff_tableM6').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            machine6 += parseFloat($(elem).val());
        });
        $('#staff_tableM7').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            helping1 += parseFloat($(elem).val());
        });
        $('#staff_tableM8').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            helping2 += parseFloat($(elem).val());
        });
        var fittingval=$('#txtFittingqty').val();
        var helping1val=$('#txtHelping1qty').val();
        var helping2val=$('#txtHelping2qty').val();
        var cleaningval=$('#txtClearningqty').val();
        var grindingval=$('#txtGrindingqty').val();
        var warmaval=$('#txtWarmaqty').val();
        var machine1val=$('#txtMachine1qty').val();
        var machine2val=$('#txtMachine2qty').val();
        var machine3val=$('#txtMachine3qty').val();
        var machine4val=$('#txtMachine4qty').val();
        var machine5val=$('#txtMachine5qty').val();
        var machine6val=$('#txtMachine6qty').val();
        var errorflag=false;
        if (Math.round(fittingval) != Math.round(fitting))
        {
            alert("Fitting Value Does Not Match");
            errorflag=true;
        }
        else if (Math.round(helping1)!=Math.round(helping1val))
        {
         alert("Helping 1 Value Does Not Match")   ;
            errorflag=true;
        }
        else if (Math.round(helping2)!=Math.round(helping2val))
        {
         alert("Helping 2 Value Does Not Match");
            errorflag=true;
        }
        else if (Math.round(cleaningval)!=Math.round(cleaning))
        {
         alert("Cleaning Value Does Not Match");
            errorflag=true;
        }
        else if (Math.round(grindingval)!=Math.round(grinding))
        {
         alert("Grinding Value Does Not Match");
            errorflag=true;
        }
        else if (Math.round(warmaval)!=Math.round(warma))
        {
         alert("Warma Value Does Not Match");
            errorflag=true;
        }
        else if (Math.round(machine1val)!=Math.round(machine1))
        {
         alert("Machine 1 Value Does Not Match");
            errorflag=true;
        }
        else if (Math.round(machine2val)!=Math.round(machine2))
        {
         alert("Machine 2 Value Does Not Match");
            errorflag=true;
        }
        else if (Math.round(machine3val)!=Math.round(machine3))
        {
         alert("Machine 3 Value Does Not Match");
            errorflag=true;
        }
        else if (Math.round(machine4val)!=Math.round(machine4))
        {
         alert("Machine 4 Value Does Not Match");
            errorflag=true;
        }
        else if (Math.round(machine5val)!=Math.round(machine5))
        {
         alert("Machine 5 Value Does Not Match");
            errorflag=true;
        }
        else if (Math.round(machine6val)!=Math.round(machine6))
        {
         alert("Machine 6 Value Does Not Match");
            errorflag=true;
        }
        return errorflag;

    }

    var calculateTotalAmount = function () {

        var amount = 0;
        var fitting = 0;
        var helping1 = 0;
        var helping2 = 0;
        var cleaning=0;
        var grinding=0;
        var warma=0;
        var machine1 =0;
        var machine2 =0;
        var machine3 =0;
        var machine4 =0;
        var machine5 =0;
        var machine6 =0;



        $('#staff_tableF').find('tbody td.amount').each(function (index, elem) {
            amount += parseFloat($(elem).text());
            fitting += parseFloat($(elem).text());
        });
        $('#staff_tableC').find('tbody td.amount').each(function (index, elem) {
            amount += parseFloat($(elem).text());
            cleaning += parseFloat($(elem).text());
        });
        $('#staff_tableG').find('tbody td.amount').each(function (index, elem) {
            amount += parseFloat($(elem).text());
            grinding += parseFloat($(elem).text());
        });
        $('#staff_tableW').find('tbody td.amount').each(function (index, elem) {
            amount += parseFloat($(elem).text());
            warma += parseFloat($(elem).text());
        });
        $('#staff_tableM1').find('tbody td.amount').each(function (index, elem) {
            amount += parseFloat($(elem).text());
            machine1 += parseFloat($(elem).text());
        });
        $('#staff_tableM2').find('tbody td.amount').each(function (index, elem) {
            amount += parseFloat($(elem).text());
            machine2 += parseFloat($(elem).text());
        });
        $('#staff_tableM3').find('tbody td.amount').each(function (index, elem) {
            amount += parseFloat($(elem).text());
            machine3 += parseFloat($(elem).text());
        });
        $('#staff_tableM4').find('tbody td.amount').each(function (index, elem) {
            amount += parseFloat($(elem).text());
            machine4 += parseFloat($(elem).text());
        });
        $('#staff_tableM5').find('tbody td.amount').each(function (index, elem) {
            amount += parseFloat($(elem).text());
            machine5 += parseFloat($(elem).text());
        });
        $('#staff_tableM6').find('tbody td.amount').each(function (index, elem) {
            amount += parseFloat($(elem).text());
            machine6 += parseFloat($(elem).text());
        });
        $('#staff_tableM7').find('tbody td.amount').each(function (index, elem) {
            amount += parseFloat($(elem).text());
            helping1 += parseFloat($(elem).text());
        });
        $('#staff_tableM8').find('tbody td.amount').each(function (index, elem) {
            amount += parseFloat($(elem).text());
            helping2 += parseFloat($(elem).text());
        });
        $('#txtFittingAmnt').val(fitting.toFixed(2));
        $('#txtHelping1Amnt').val(helping1.toFixed(2));
        $('#txtHelping2Amnt').val(helping2.toFixed(2));
        $('#txtTotAmnt').val(amount.toFixed(2));
        $('#txtClearningAmnt').val(cleaning.toFixed(2));
        $('#txtGrindingAmnt').val(grinding.toFixed(2));
        $('#txtWarmaAmnt').val(warma.toFixed(2));
        $('#txtMachine1Amnt').val(machine1.toFixed(2));
        $('#txtMachine2Amnt').val(machine2.toFixed(2));
        $('#txtMachine3Amnt').val(machine3.toFixed(2));
        $('#txtMachine4Amnt').val(machine4.toFixed(2));
        $('#txtMachine5Amnt').val(machine5.toFixed(2));
        $('#txtMachine6Amnt').val(machine6.toFixed(2));
        

          amount = 0;
         fitting = 0;
         helping1 = 0;
         helping2 = 0;
         cleaning=0;
         grinding=0;
         warma=0;
         machine1 =0;
         machine2 =0;
         machine3 =0;
         machine4 =0;
         machine5 =0;
         machine6 =0;



        $('#staff_tableF').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            fitting += parseFloat($(elem).val());

        });
        $('#staff_tableC').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            cleaning += parseFloat($(elem).val());
        });
        $('#staff_tableG').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            grinding += parseFloat($(elem).val());
        });
        $('#staff_tableW').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            warma += parseFloat($(elem).val());
        });
        $('#staff_tableM1').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            machine1 += parseFloat($(elem).val());
        });
        $('#staff_tableM2').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            machine2 += parseFloat($(elem).val());
        });
        $('#staff_tableM3').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            machine3 += parseFloat($(elem).val());
        });
        $('#staff_tableM4').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            machine4 += parseFloat($(elem).val());
        });
        $('#staff_tableM5').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            machine5 += parseFloat($(elem).val());
        });
        $('#staff_tableM6').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            machine6 += parseFloat($(elem).val());
        });
        $('#staff_tableM7').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            helping1 += parseFloat($(elem).val());
        });
        $('#staff_tableM8').find('tbody td input.qty').each(function (index, elem) {
            amount += parseFloat($(elem).val());
            helping2 += parseFloat($(elem).val());
        });
        $('#txtFittingqty').val(fitting.toFixed(2));
        $('#txtHelping1qty').val(helping1.toFixed(2));
        $('#txtHelping2qty').val(helping2.toFixed(2));
        $('#txtTotqty').val(amount.toFixed(2));
        $('#txtClearningqty').val(cleaning.toFixed(2));
        $('#txtGrindingqty').val(grinding.toFixed(2));
        $('#txtWarmaqty').val(warma.toFixed(2));
        $('#txtMachine1qty').val(machine1.toFixed(2));
        $('#txtMachine2qty').val(machine2.toFixed(2));
        $('#txtMachine3qty').val(machine3.toFixed(2));
        $('#txtMachine4qty').val(machine4.toFixed(2));
        $('#txtMachine5qty').val(machine5.toFixed(2));
        $('#txtMachine6qty').val(machine6.toFixed(2));

    }

    var lessTotalAmount = function (amount) {

        var tempAmount = 0;
        var totAmount = $('#txtTotAmnt').val();
        tempAmount = parseFloat(totAmount) - parseFloat(amount);
        $('#txtTotAmnt').val(tempAmount);
    }

    var getSaveObject = function () {

        var stockmain = {};
        var stockdetail = [];
        var ledger = [];


        stockmain.vrnoa = $('#txtVrnoaHidden').val();
        stockmain.remarks = $('#txtRemarks').val();
        stockmain.etype = 'head_production';
        stockmain.uid = $('#uid').val();
        stockmain.company_id = $('#cid').val();
        stockmain.vrdate = $('#current_date').val();
        stockmain.party_id = $.trim($('#party_dropdown').find('option:selected').val());
        stockmain.type = $('#voucher_type_hidden').val();
        $('#item_table').find('tbody tr').each(function (index, elem) {
            var sd = {};

            sd.stdid = '';
            sd.type = 'add';
            sd.item_id = $.trim($(elem).find('td.item').data('item_id'));
            sd.godown_id = $.trim($('#dept_dropdown').find('option:selected').val());
            sd.qty = $.trim($(elem).find('td.qty').text());
            sd.weight = $.trim($(elem).find('td.weight').text());
            sd.perday = $.trim($(elem).find('td.perday').text());

            stockdetail.push(sd);
        });

        $('#staff_tableF').find('tbody tr').each(function (index, elem) {


            var sd = {};

            sd.stdid = '';
            sd.type = 'add_fitting';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#fiting #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = $.trim($(elem).find('td input.qty').val());
            sd.weight = $.trim($(elem).find('td.weight').text());
            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());
            sd.produced = qty / perday;
            stockdetail.push(sd);

            var sd = {};
            sd.stdid = '';
            sd.type = 'less_fitting';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#fiting #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = '-' + $.trim($(elem).find('td input.qty').val());
            sd.weight = '-' + $.trim($(elem).find('td.weight').text());
            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = '-' + $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());
            sd.produced = qty / perday;
            stockdetail.push(sd);


            var pledger = {};

            pledger.pid = $.trim($(elem).find('td.name').data('pid'));
            pledger.description = $.trim($(elem).find('td.item_des').text() + $(elem).find('td input.qty').val() + '@' + $(elem).find('td.rate').text());
            pledger.remarks = $('#txtRemarks').val();
            pledger.credit = $.trim($(elem).find('td.amount').text());
            pledger.debit = 0;
            pledger.date = $.trim($('#current_date').val());
            pledger.etype = 'head_production';
            pledger.dcno = $.trim($('#txtVrnoa').val());
            pledger.pid_key = $.trim($(elem).find('td.name').data('pid'));

            ledger.push(pledger);
        });


        $('#staff_tableM1').find('tbody tr').each(function (index, elem) {


            var sd = {};

            sd.stdid = '';
            sd.type = 'add_machine1';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#machine1 #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = $.trim($(elem).find('td input.qty').val());
            sd.weight = $.trim($(elem).find('td.weight input').val());

            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());
            sd.produced = qty / perday;
            stockdetail.push(sd);

            var sd = {};
            sd.stdid = '';
            sd.type = 'less_machine1';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#machine1 #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = '-' + $.trim($(elem).find('td input.qty').val());
            sd.weight = '-' + $.trim($(elem).find('td.weight').text());
            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = '-' + $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());
            sd.produced = qty / perday;
            stockdetail.push(sd);


            var pledger = {};

            pledger.pid = $.trim($(elem).find('td.name').data('pid'));
            pledger.description = $.trim($(elem).find('td.item_des').text() + $(elem).find('td input.qty').val() + '@' + $(elem).find('td.rate').text());
            pledger.remarks = $('#txtRemarks').val();
            pledger.credit = $.trim($(elem).find('td.amount').text());
            pledger.debit = 0;
            pledger.date = $.trim($('#current_date').val());
            pledger.etype = 'head_production';
            pledger.dcno = $.trim($('#txtVrnoa').val());
            pledger.pid_key = $.trim($(elem).find('td.name').data('pid'));
            ledger.push(pledger);
        });
        $('#staff_tableM2').find('tbody tr').each(function (index, elem) {


            var sd = {};

            sd.stdid = '';
            sd.type = 'add_machine2';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#machine2 #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = $.trim($(elem).find('td input.qty').val());
            sd.weight = $.trim($(elem).find('td.weight').text());

            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());
            sd.produced = qty / perday;
            stockdetail.push(sd);

            var sd = {};
            sd.stdid = '';
            sd.type = 'less_machine2';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#machine2 #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = '-' + $.trim($(elem).find('td input.qty').val());
            sd.weight = '-' + $.trim($(elem).find('td.weight').text());
            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = '-' + $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());
            sd.produced = qty / perday;
            stockdetail.push(sd);


            var pledger = {};

            pledger.pid = $.trim($(elem).find('td.name').data('pid'));
            pledger.description = $.trim($(elem).find('td.item_des').text() + $(elem).find('td input.qty').val() + '@' + $(elem).find('td.rate').text());
            pledger.remarks = $('#txtRemarks').val();
            pledger.credit = $.trim($(elem).find('td.amount').text());
            pledger.debit = 0;
            pledger.date = $.trim($('#current_date').val());
            pledger.etype = 'head_production';
            pledger.dcno = $.trim($('#txtVrnoa').val());
            pledger.pid_key = $.trim($(elem).find('td.name').data('pid'));
            ledger.push(pledger);
        });
        $('#staff_tableM3').find('tbody tr').each(function (index, elem) {


            var sd = {};

            sd.stdid = '';
            sd.type = 'add_machine3';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#machine3 #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = $.trim($(elem).find('td input.qty').val());
            sd.weight = $.trim($(elem).find('td.weight').text());

            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());
            sd.produced = qty / perday;
            stockdetail.push(sd);

            var sd = {};
            sd.stdid = '';
            sd.type = 'less_machin3';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#machine3 #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = '-' + $.trim($(elem).find('td input.qty').val());
            sd.weight = '-' + $.trim($(elem).find('td.weight').text());
            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = '-' + $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());
            sd.produced = qty / perday;
            stockdetail.push(sd);


            var pledger = {};

            pledger.pid = $.trim($(elem).find('td.name').data('pid'));
            pledger.description = $.trim($(elem).find('td.item_des').text() + $(elem).find('td input.qty').val() + '@' + $(elem).find('td.rate').text());
            pledger.remarks = $('#txtRemarks').val();
            pledger.credit = $.trim($(elem).find('td.amount').text());
            pledger.debit = 0;
            pledger.date = $.trim($('#current_date').val());
            pledger.etype = 'head_production';
            pledger.dcno = $.trim($('#txtVrnoa').val());
            pledger.pid_key = $.trim($(elem).find('td.name').data('pid'));
            ledger.push(pledger);
        });
        $('#staff_tableM4').find('tbody tr').each(function (index, elem) {


            var sd = {};

            sd.stdid = '';
            sd.type = 'add_machine4';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#machine4 #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = $.trim($(elem).find('td input.qty').val());
            sd.weight = $.trim($(elem).find('td.weight').text());

            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());
            sd.produced = qty / perday;
            stockdetail.push(sd);

            var sd = {};
            sd.stdid = '';
            sd.type = 'less_machine4';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#machine4 #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = '-' + $.trim($(elem).find('td input.qty').val());
            sd.weight = '-' + $.trim($(elem).find('td.weight').text());
            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = '-' + $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());
            sd.produced = qty / perday;
            stockdetail.push(sd);


            var pledger = {};

            pledger.pid = $.trim($(elem).find('td.name').data('pid'));
            pledger.description = $.trim($(elem).find('td.item_des').text() + $(elem).find('td input.qty').val() + '@' + $(elem).find('td.rate').text());
            pledger.remarks = $('#txtRemarks').val();
            pledger.credit = $.trim($(elem).find('td.amount').text());
            pledger.debit = 0;
            pledger.date = $.trim($('#current_date').val());
            pledger.etype = 'head_production';
            pledger.dcno = $.trim($('#txtVrnoa').val());
            pledger.pid_key = $.trim($(elem).find('td.name').data('pid'));
            ledger.push(pledger);
        });
        $('#staff_tableM5').find('tbody tr').each(function (index, elem) {


            var sd = {};

            sd.stdid = '';
            sd.type = 'add_machine5';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#machine5 #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = $.trim($(elem).find('td input.qty').val());
            sd.weight = $.trim($(elem).find('td.weight').text());

            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());
            sd.produced = qty / perday;
            stockdetail.push(sd);

            var sd = {};
            sd.stdid = '';
            sd.type = 'less_machine5';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#machin5 #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = '-' + $.trim($(elem).find('td input.qty').val());
            sd.weight = '-' + $.trim($(elem).find('td.weight').text());
            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = '-' + $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());
            sd.produced = qty / perday;
            stockdetail.push(sd);


            var pledger = {};

            pledger.pid = $.trim($(elem).find('td.name').data('pid'));
            pledger.description = $.trim($(elem).find('td.item_des').text() + $(elem).find('td input.qty').val() + '@' + $(elem).find('td.rate').text());
            pledger.remarks = $('#txtRemarks').val();
            pledger.credit = $.trim($(elem).find('td.amount').text());
            pledger.debit = 0;
            pledger.date = $.trim($('#current_date').val());
            pledger.etype = 'head_production';
            pledger.dcno = $.trim($('#txtVrnoa').val());
            pledger.pid_key = $.trim($(elem).find('td.name').data('pid'));
            ledger.push(pledger);
        });
        $('#staff_tableM6').find('tbody tr').each(function (index, elem) {


            var sd = {};

            sd.stdid = '';
            sd.type = 'add_machine6';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#machine6 #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = $.trim($(elem).find('td input.qty').val());
            sd.weight = $.trim($(elem).find('td.weight').text());

            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());
            sd.produced = qty / perday;
            stockdetail.push(sd);

            var sd = {};
            sd.stdid = '';
            sd.type = 'less_machine6';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#machine6 #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = '-' + $.trim($(elem).find('td input.qty').val());
            sd.weight = '-' + $.trim($(elem).find('td.weight').text());
            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = '-' + $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());
            sd.produced = qty / perday;
            stockdetail.push(sd);


            var pledger = {};

            pledger.pid = $.trim($(elem).find('td.name').data('pid'));
            pledger.description = $.trim($(elem).find('td.item_des').text() + $(elem).find('td input.qty').val() + '@' + $(elem).find('td.rate').text());
            pledger.remarks = $('#txtRemarks').val();
            pledger.credit = $.trim($(elem).find('td.amount').text());
            pledger.debit = 0;
            pledger.date = $.trim($('#current_date').val());
            pledger.etype = 'head_production';
            pledger.dcno = $.trim($('#txtVrnoa').val());
            pledger.pid_key = $.trim($(elem).find('td.name').data('pid'));
            ledger.push(pledger);
        });
        $('#staff_tableC').find('tbody tr').each(function (index, elem) {


            var sd = {};

            sd.stdid = '';
            sd.type = 'add_cleaning';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#cleaning #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = $.trim($(elem).find('td input.qty').val());
            sd.weight = $.trim($(elem).find('td.weight').text());

            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());
            sd.produced = qty / perday;
            stockdetail.push(sd);

            var sd = {};
            sd.stdid = '';
            sd.type = 'less_cleaning';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#cleaning #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = '-' + $.trim($(elem).find('td input.qty').val());
            sd.weight = '-' + $.trim($(elem).find('td.weight').text());
            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = '-' + $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());
            sd.produced = qty / perday;
            stockdetail.push(sd);


            var pledger = {};

            pledger.pid = $.trim($(elem).find('td.name').data('pid'));
            pledger.description = $.trim($(elem).find('td.item_des').text() + $(elem).find('td input.qty').val() + '@' + $(elem).find('td.rate').text());
            pledger.remarks = $('#txtRemarks').val();
            pledger.credit = $.trim($(elem).find('td.amount').text());
            pledger.debit = 0;
            pledger.date = $.trim($('#current_date').val());
            pledger.etype = 'head_production';
            pledger.dcno = $.trim($('#txtVrnoa').val());
            pledger.pid_key = $.trim($(elem).find('td.name').data('pid'));
            ledger.push(pledger);
        });
        $('#staff_tableG').find('tbody tr').each(function (index, elem) {


            var sd = {};

            sd.stdid = '';
            sd.type = 'add_grinding';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#grinding #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = $.trim($(elem).find('td input.qty').val());
            sd.weight = $.trim($(elem).find('td.weight').text());

            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());
            sd.produced = qty / perday;
            stockdetail.push(sd);

            var sd = {};
            sd.stdid = '';
            sd.type = 'less_granding';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#grinding #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = '-' + $.trim($(elem).find('td input.qty').val());
            sd.weight = '-' + $.trim($(elem).find('td.weight').text());
            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = '-' + $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());
            sd.produced = qty / perday;
            stockdetail.push(sd);


            var pledger = {};

            pledger.pid = $.trim($(elem).find('td.name').data('pid'));
            pledger.description = $.trim($(elem).find('td.item_des').text() + $(elem).find('td input.qty').val() + '@' + $(elem).find('td.rate').text());
            pledger.remarks = $('#txtRemarks').val();
            pledger.credit = $.trim($(elem).find('td.amount').text());
            pledger.debit = 0;
            pledger.date = $.trim($('#current_date').val());
            pledger.etype = 'head_production';
            pledger.dcno = $.trim($('#txtVrnoa').val());
            pledger.pid_key = $.trim($(elem).find('td.name').data('pid'));
            ledger.push(pledger);
        });
        $('#staff_tableW').find('tbody tr').each(function (index, elem) {


            var sd = {};

            sd.stdid = '';
            sd.type = 'add_warma';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#warma #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = $.trim($(elem).find('td input.qty').val());
            sd.weight = $.trim($(elem).find('td.weight').text());

            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());
            sd.produced = qty / perday;
            stockdetail.push(sd);

            var sd = {};
            sd.stdid = '';
            sd.type = 'less_warma';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#warma #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = '-' + $.trim($(elem).find('td input.qty').val());
            sd.weight = '-' + $.trim($(elem).find('td.weight').text());
            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = '-' + $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());
            sd.produced = qty / perday;
            stockdetail.push(sd);


            var pledger = {};

            pledger.pid = $.trim($(elem).find('td.name').data('pid'));
            pledger.description = $.trim($(elem).find('td.item_des').text() + $(elem).find('td input.qty').val() + '@' + $(elem).find('td.rate').text());
            pledger.remarks = $('#txtRemarks').val();
            pledger.credit = $.trim($(elem).find('td.amount').text());
            pledger.debit = 0;
            pledger.date = $.trim($('#current_date').val());
            pledger.etype = 'head_production';
            pledger.dcno = $.trim($('#txtVrnoa').val());
            pledger.pid_key = $.trim($(elem).find('td.name').data('pid'));
            ledger.push(pledger);
        });
        $('#staff_tableM7').find('tbody tr').each(function (index, elem) {


            var sd = {};

            sd.stdid = '';
            sd.type = 'add_helping1';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#helping1 #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = $.trim($(elem).find('td input.qty').val());
            sd.weight = $.trim($(elem).find('td.weight').text());

            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());
            sd.produced = qty / perday;
            stockdetail.push(sd);

            var sd = {};
            sd.stdid = '';
            sd.type = 'less_helping1';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#helping1 #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = '-' + $.trim($(elem).find('td input.qty').val());
            sd.weight = '-' + $.trim($(elem).find('td.weight').text());
            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = '-' + $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());
            sd.produced = qty / perday;
            stockdetail.push(sd);


            var pledger = {};

            pledger.pid = $.trim($(elem).find('td.name').data('pid'));
            pledger.description = $.trim($(elem).find('td.item_des').text() + $(elem).find('td input.qty').val() + '@' + $(elem).find('td.rate').text());
            pledger.remarks = $('#txtRemarks').val();
            pledger.credit = $.trim($(elem).find('td.amount').text());
            pledger.debit = 0;
            pledger.date = $.trim($('#current_date').val());
            pledger.etype = 'head_production';
            pledger.dcno = $.trim($('#txtVrnoa').val());
            pledger.pid_key = $.trim($(elem).find('td.name').data('pid'));
            ledger.push(pledger);
        });

        $('#staff_tableM8').find('tbody tr').each(function (index, elem) {


            var sd = {};

            sd.stdid = '';
            sd.type = 'add_helping2';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#helping2 #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = $.trim($(elem).find('td input.qty').val());
            sd.weight = $.trim($(elem).find('td.weight').text());
            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());

            sd.produced = qty / perday;
            stockdetail.push(sd);

            var sd = {};
            sd.stdid = '';
            sd.type = 'less_helping2';
            sd.item_id = $.trim($(elem).find('td.item_des').data('item_id'));
            sd.godown_id = $.trim($('#helping2 #dept_dropdown').find('option:selected').val());
            sd.emp_id = $.trim($(elem).find('td.name').data('pid'));
            sd.qty = '-' + $.trim($(elem).find('td input.qty').val());
            sd.weight = '-' + $.trim($(elem).find('td.weight').text());
            sd.rate = $.trim($(elem).find('td.rate').text());
            sd.amount = '-' + $.trim($(elem).find('td.amount').text());
            sd.perday = $.trim($(elem).find('td.perday').text());
            var perday = $.trim($(elem).find('td.perday').text());
            var qty = $.trim($(elem).find('td input.qty').val());
            sd.produced = qty / perday;
            stockdetail.push(sd);


            var pledger = {};

            pledger.pid = $.trim($(elem).find('td.name').data('pid'));
            pledger.description = $.trim($(elem).find('td.item_des').text() + $(elem).find('td input.qty').val() + '@' + $(elem).find('td.rate').text());
            pledger.remarks = $('#txtRemarks').val();
            pledger.credit = $.trim($(elem).find('td.amount').text());
            pledger.debit = 0;
            pledger.date = $.trim($('#current_date').val());
            pledger.etype = 'head_production';
            pledger.dcno = $.trim($('#txtVrnoa').val());
            pledger.pid_key = $.trim($(elem).find('td.name').data('pid'));
            ledger.push(pledger);
        });

        var acledger = {};
        acledger.pid = $.trim($('#party_dropdown').find('option:selected').val());
        acledger.description = 'Expence';
        acledger.remarks = $('#txtRemarks').val();
        acledger.credit = 0;
        acledger.debit = $.trim($('#txtTotAmnt').val());
        acledger.date = $.trim($('#current_date').val());
        acledger.etype = 'head_production';
        acledger.dcno = $.trim($('#txtVrnoa').val());
        acledger.pid_key = $.trim($('#party_dropdown').find('option:selected').val());
        ledger.push(acledger);
        var data = {};
        data.stockmain = stockmain;
        data.stockdetail = stockdetail;
        data.ledger = ledger;
        data.vrnoa = $('#txtVrnoaHidden').val();
        console.table(data.stockdetail)
        return data;
    }

    shortcut.add("F10", function () {
        $('.btnSave').trigger('click');
    });

    shortcut.add("F12", function () {
        $('.btnDelete').trigger('click');
    });
    shortcut.add("F5", function () {
        self.resetVoucher();
    });
    shortcut.add("F6", function () {
        $('#txtVrnoa').focus();
        // alert('focus');
    });
    shortcut.add("F9", function () {
        Print_Voucher(1, 'lg', '');
    });
    var save = function (production) {
        general.disableSave();
        // alert(production.stockmain['company_id']);
        $.ajax({
            url: base_url + 'index.php/headprod/save',
            type: 'POST',
            data: {
                'stockmain': production.stockmain,
                'stockdetail': JSON.stringify(production.stockdetail),
                'ledger': JSON.stringify(production.ledger),
                'vrnoa': production.stockmain['vrnoa'],
                'etype': production.stockmain['etype'],
                'vrdate' : $('#vrdate').val()
            },
            dataType: 'JSON',
            success: function (data) {

                if (data.error === 'date close') {
                    alert('Sorry! you can not insert update in close date................');
                }else if (data.error === 'true') {
                    alert('An internal error occured while saving voucher. Please try again.');
                } else {
                    var printConfirmation = confirm('Voucher saved!\nWould you like to print the invoice as well?');
                    if (printConfirmation === true) {
                        Print_Voucher(1);
                    } else {
                        general.reloadWindow();
                    }
                    // alert('Voucher Saved successfully...');
                }
                general.enableSave();
            }, error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    var fetch = function (vrnoa) {

        $.ajax({

            url: base_url + 'index.php/headprod/fetch',
            type: 'POST',
            data: {'vrnoa': vrnoa, 'company_id': $('#cid').val()},
            dataType: 'JSON',
            success: function (data) {

                $('.st').find('tbody tr').remove();
                $('#dept_dropdown').select2('val', '');
                $('#txtRemarks').val('');

                if (data === 'false') {
                    alert('No data found.');
                    getmaxvrnoa();
                } else {
                    populateData(data);
                }

            }, error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }
    var Print_Voucher = function (hd) {
        if ($('.btnSave').data('printbtn') == 0) {
            alert('Sorry! you have not print rights..........');
        } else {
            var etype = 'head_production';
            var vrnoa = $('#txtVrnoa').val();
            var company_id = $('#cid').val();
            var user = $('#uname').val();
            // var hd = $('#hd').val();
            var pre_bal_print = '0';

            var url = base_url + 'index.php/doc/print_pro_voucher/' + etype + '/' + vrnoa + '/' + company_id + '/' + '-1' + '/' + user + '/' + pre_bal_print + '/' + hd;
            // var url = base_url + 'index.php/doc/CashVocuherPrintPdf/' + etype + '/' + dcno   + '/' + companyid + '/' + '-1' + '/' + user;
            window.open(url);
        }

    }

    var getEmplyees = function() {

        // $.ajax({
        //     url: base_url + 'index.php/headprod/fetch',
        //     type: 'POST',
        //     data: {'vrnoa': vrnoa, 'company_id': $('#cid').val()},
        //     dataType: 'JSON',
        //     success: function (data) {

        //         $('.st').find('tbody tr').remove();
        //         $('#dept_dropdown').select2('val', '');
        //         $('#txtRemarks').val('');

        //         if (data === 'false') {
        //             alert('No data found.');
        //         } else {
        //             populateData(data);
        //         }

        //     }, error: function (xhr, status, error) {
        //         console.log(xhr.responseText);
        //     }
        // })

    }

    var findIndexInData = function(data, property, value) {
        var result = -1;
        data.some(function (item, i) {
            if (item[property] === value) {
                result = i;
                return true;
            }
        });
        return result;
    }

    var populateData = function (data) {
        var data2 = [];
        var empData = {};
        $('#voucher_type_hidden').val('edit');
        $('#txtVrnoHidden').val(data[0]['vrnoa']);
        $('#txtVrnoa').val(data[0]['vrnoa']);
        $('#txtVrnoaHidden').val(data[0]['vrnoa']);
        $('#txtIdImg').val(data[0]['vrnoa']);
        $('#current_date').datepicker('update', data[0]['vrdate'].substr(0, 10));
        $('#vrdate').datepicker('update', data[0]['vrdate'].substr(0, 10));
        $('#receivers_list').val(data[0]['received_by']);
        $('#txtRemarks').val(data[0]['remarks']);
        $('#dept_dropdown').select2('val', data[0]['godown_id']);
        $('#party_dropdown').select2('val', data[0]['party_id']);
        console.log(data);

        $.each(data, function(index, elem){
            if (elem.emp_id != '0') {
                if(findIndexInData(data2, 'emp_id', elem.emp_id) == -1 ){
                    empData = {
                        'emp_id' : elem.emp_id,
                        'emp_name' : elem.name, 
                    }
                    data2.push(empData);
                }
            }
        })
        
        $.each(data, function(index, elem) {
        

            if (elem.type == 'add') {
                appendToTable('1', elem.item_id, elem.item_code, elem.item_name, elem.s_qty, elem.weight);
                calculateLowerTotal(elem.s_qty,elem.weight);
            } else if (elem.type == 'add_fitting') {
                $("#fiting #dept_dropdown").select2("val",elem.godown_id);
                appendToStaff('1', elem.item_id, elem.item_name, elem.emp_id, elem.name, elem.s_rate, elem.s_qty, elem.weight, 1, elem.perday, data2);
            } else if (elem.type == 'add_helping1') {
                $("#helping1 #dept_dropdown").select2("val",elem.godown_id);
                appendToStaff('1', elem.item_id, elem.item_name, elem.emp_id, elem.name, elem.s_rate, elem.s_qty, elem.weight, 11, elem.perday, data2);
            } else if (elem.type == 'add_helping2') {
                // console.log('hello');
                $("#helping2 #dept_dropdown").select2("val",elem.godown_id);
                appendToStaff('1', elem.item_id, elem.item_name, elem.emp_id, elem.name, elem.s_rate, elem.s_qty, elem.weight, 12, elem.perday, data2);
            }
            else if (elem.type == 'add_machine1') {
                $("#machine1 #dept_dropdown").select2("val",elem.godown_id);
                appendToStaff('1', elem.item_id, elem.item_name, elem.emp_id, elem.name, elem.s_rate, elem.s_qty, elem.weight, 5, elem.perday, data2);
            }
            else if (elem.type == 'add_machine2') {
                $("#machine2 #dept_dropdown").select2("val",elem.godown_id);
                appendToStaff('1', elem.item_id, elem.item_name, elem.emp_id, elem.name, elem.s_rate, elem.s_qty, elem.weight, 6, elem.perday, data2);
            }
            else if (elem.type == 'add_machine3') {
                $("#machine3 #dept_dropdown").select2("val",elem.godown_id);
                appendToStaff('1', elem.item_id, elem.item_name, elem.emp_id, elem.name, elem.s_rate, elem.s_qty, elem.weight, 7, elem.perday, data2);
            }
            else if (elem.type == 'add_machine4') {
                $("#machine4 #dept_dropdown").select2("val",elem.godown_id);
                appendToStaff('1', elem.item_id, elem.item_name, elem.emp_id, elem.name, elem.s_rate, elem.s_qty, elem.weight, 8, elem.perday, data2);
            }
            else if (elem.type == 'add_machine5') {
                $("#machine5 #dept_dropdown").select2("val",elem.godown_id);
                appendToStaff('1', elem.item_id, elem.item_name, elem.emp_id, elem.name, elem.s_rate, elem.s_qty, elem.weight, 9, elem.perday, data2);
            }
            else if (elem.type == 'add_machine6') {
                $("#machine6 #dept_dropdown").select2("val",elem.godown_id);
                appendToStaff('1', elem.item_id, elem.item_name, elem.emp_id, elem.name, elem.s_rate, elem.s_qty, elem.weight, 10, elem.perday, data2);
            }
            else if (elem.type == 'add_cleaning') {
                $("#cleaning #dept_dropdown").select2("val",elem.godown_id);
                appendToStaff('1', elem.item_id, elem.item_name, elem.emp_id, elem.name, elem.s_rate, elem.s_qty, elem.weight, 2, elem.perday, data2);
            }
            else if (elem.type == 'add_grinding') {
                $("#grinding #dept_dropdown").select2("val",elem.godown_id);
                appendToStaff('1', elem.item_id, elem.item_name, elem.emp_id, elem.name, elem.s_rate, elem.s_qty, elem.weight, 3, elem.perday, data2);
            }
            else if (elem.type == 'add_warma') {
                $("#warma #dept_dropdown").select2("val",elem.godown_id);
                appendToStaff('1', elem.item_id, elem.item_name, elem.emp_id, elem.name, elem.s_rate, elem.s_qty, elem.weight, 4, elem.perday, data2);
            }
            
            calculateTotalAmount();
        });
        if(data2 != undefined) {
            appendOption(data2);
        }
        $('select.emps').select2();
        popoulateEmps();
    }

    var popoulateEmps = function() {
        $('.emps').each(function(index, elem){
            var pid = $.trim($(this).closest('tr').find('td.name').data('pid'));
            $(this).select2('val', pid);
        });
    }

    var fetch_present_staff = function (itemIds, itemData, deptId, phaseId) {
        var data2 = [];
        $.ajax({
            url: base_url + 'index.php/headprod/fetch_present_staff',
            type: 'POST',
            data: {
                'item_ids': itemIds,
                item_data: itemData,
                'dept_id': deptId,
                'phase_id': phaseId,
                'date': $('#current_date').val()
                // 'date':'2008-02-28'
            },
            dataType: 'JSON',
            success: function (data) {
                
                if (data === 'false') {
                    alert('No data found');
                } else {
                    $.each(data, function(index, elem){
                    if (elem.pid != '0') {
                    if(findIndexInData(data2, 'emp_id', elem.pid) == -1 ){
                    empData = {
                        'emp_id' : elem.pid,
                        'emp_name' : elem.name, 
                    }
                    data2.push(empData);
                }
            }
                });

                    
                    $.each(data, function (index, elem) {
                        appendToStaff('1', elem.item_id, elem.item_des, elem.pid, elem.name, elem.rate, elem.qty, elem.weight, phaseId, elem.perday,data2,elem.status);
                    });
                     if(data2 != undefined) {
                    appendOption(data2);
                        }
                    $('select.emps').select2();
                    popoulateEmps();
                    calculateTotalAmount();
                }
            }, error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    // checks for the empty fields
    var validateSave = function () {

        var errorFlag = false;
        var partyEl = $('#party_dropdown');

        $('.inputerror').removeClass('inputerror');

        if (!partyEl.val()) {

            $('#party_dropdown').addClass('inputerror');
            errorFlag = true;
        }

        return errorFlag;
    }

    var deleteVoucher = function (vrnoa) {

        $.ajax({
            url: base_url + 'index.php/headprod/delete',
            type: 'POST',
            data: {'chk_date' : $('#current_date').val(), 'vrdate' : $('#vrdate').val(), 'vrnoa': vrnoa, 'company_id': $('#cid').val()},
            dataType: 'JSON',
            success: function (data) {

                if (data.error === 'date close') {
                    alert('Sorry! you can not delete in close date................');
                }else if (data === 'false') {

                    alert('No data found');
                } else {

                    alert('Voucher deleted successfully');
                    general.reloadWindow();
                }
            }, error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    var clearItemData = function () {

        $("#hfItemId").val("");
        $("#hfItemSize").val("");
        $("#hfItemBid").val("");
        $("#hfItemUom").val("");
        $("#hfItemPrate").val("");
        $("#hfItemGrWeight").val("");
        $("#hfItemStQty").val("");
        $("#hfItemStWeight").val("");
        $("#hfItemLength").val("");
        $("#hfItemCatId").val("");
        $("#hfItemSubCatId").val("");
        $("#hfItemDesc").val("");
        $("#hfItemShortCode").val("");

        //$("#txtItemId").val("");
    }


    return {

        init: function () {
            $('#voucher_type_hidden').val('new');
            $('#cid').val(1)
            this.bindUI();
        },

        bindUI: function () {
            var self = this;

            $('#dept_dropdown').focus().css('border', '1px solid #368EE0');

            $('.btn').focus(function () {
                    $(this).css('border', '2px solid black');
                }
            );
            $('.btn').blur(function () {
                    $(this).css('border', '');
                }
            );

            getmaxvrnoa();

            $('#btnAddItem').on('click', function (e) {
                e.preventDefault();

                var error = validateSingleProductAdd();
                if (!error) {

                    var item_desc = $('#itemDesc').val();
                    var uitem_des = $('#hfItemUname').val();
                    var uom_item = $('#hfItemUom').val();
                    var item_code = $('#hfItemShortCode').val();
                    var item_id = $('#hfItemId').val();
                    var item_qty = $('#txtSQty').val();
                    var item_weight = $('#txtWeight').val();

                    $('#txtItemId').val('');
                    $('#itemDesc').val('');
                    $('#txtSQty').val('');
                    $('#txtWeight').val('');


                    appendToTable('1', item_id, item_code, item_desc, item_qty, item_weight);
                    calculateLowerTotal(item_qty,item_weight);
                } else {
                    alert('Correct the errors!');
                }

            });

            $('.st').on('click', '.btnRowRemove', function (e) {
                e.preventDefault();

                var amountLess = $.trim($(this).closest('tr').find('td.amount').text());
                var qty = $.trim($(this).closest('tr').find('td.qty').text());
                var weight = $.trim($(this).closest('tr').find('td.weight').text());
                lessTotalAmount(amountLess);
                $(this).closest('tr').remove();
                calculateLowerTotal(-qty,-weight);


            });


             $('.st').on('input', 'tr td input.qty', function (e) {
                e.preventDefault();
                var qty = getNumVal($(this).closest('tr').find('td input.qty'));
                var rate = $.trim($(this).closest('tr').find('td.rate').text());
                console.log(qty +'   '+ rate);
                var amount=parseFloat(qty*rate).toFixed(2);
                $.trim($(this).closest('tr').find('td.amount').text(amount));
            });

            $('#item_table').on('click', '.btnRowEdit', function (e) {
                e.preventDefault();

                // getting values of the cruel row
                var item_id = $.trim($(this).closest('tr').find('td.item').data('item_id'));
                var qty = $.trim($(this).closest('tr').find('td.qty').text());
                var weight = $.trim($(this).closest('tr').find('td.weight').text());
                var item_code = $.trim($(this).closest('tr').find('td.item_id').data('item_code'));

                $.ajax({
                    type: "POST",
                    url: base_url + 'index.php/item/getiteminfobyid',
                    data: {
                        item_id: item_id
                    }
                }).done(function (result) {

                    $("#imgItemLoader").hide();
                    var item = $.parseJSON(result);

                    if (item != false) {
                        $("#hfItemId").val(item[0].item_id);
                        $("#hfItemSize").val(item[0].size);
                        $("#hfItemBid").val(item[0].bid);
                        $("#hfItemUom").val(item[0].uom);
                        $("#hfItemPrate").val(item[0].cost_price);
                        $("#hfItemGrWeight").val(item[0].grweight);
                        $("#hfItemStQty").val(item[0].stqty);
                        $("#hfItemStWeight").val(item[0].stweight);
                        $("#hfItemLength").val(item[0].length);
                        $("#hfItemCatId").val(item[0].catid);
                        $("#hfItemSubCatId").val(item[0].subcatid);
                        $("#hfItemDesc").val(item[0].item_des);
                        $("#hfItemShortCode").val(item[0].item_code);
                        $("#hfItemUname").val(item[0].uname);

                        $("#txtItemId").val(item[0].item_code);
                        $('#itemDesc').val(item[0].item_des);
                        $('#txtUom').val(item[0].uom);


                        //$("#txtItemId").val(item_code);
                        $("#txtItemId").val(item[0].item_code);
                        $('#txtSQty').val(qty);
                        $('#stqty_lbl').text('Item,     Uom:' + item[0].uom);
                        // now we have get all the value of the row that is being deleted. so remove that cruel row
                    }
                });
                // now we have get all the value of the row that is being deleted. so remove that cruel row
                $(this).closest('tr').remove();
                calculateLowerTotal(-qty,-weight);
            });


            $('.btnSave').on('click', function (e) {
                if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('updatebtn')==0 ){
					alert('Sorry! you have not update rights..........');
				}else if($('#voucher_type_hidden').val()=='new' && $('.btnSave').data('insertbtn')==0){
					alert('Sorry! you have not insert rights..........');
				}else{
                e.preventDefault();
                self.initSave();
                }
            });

            /*$('.btnDelete').on('click', function(e){
                e.preventDefault();
                var vrnoa = $('#txtVrnoa').val();
                if (vrnoa !== '') {
                    if (confirm('Are you sure to delete this voucher?'))
                    deleteVoucher(vrnoa);
                }
            });*/

            $('.btnDelete').on('click', function (e) {
                e.preventDefault();
                var vrnoa = $('#txtVrnoa').val();
                if (vrnoa !== '') {
                    if ($('.btnSave').data('deletebtn')==0)
					{
						alert('Sorry! you have no Delete rights..........');
					}
					else
					{
                    if (confirm('Are you sure to delete this voucher?'))
                        deleteVoucher(vrnoa);
                    }
                }
            });
            $('.btnPrint').on('click', function (e) {
                e.preventDefault();
                Print_Voucher(1);
            });
            $('.btnPrints').on('click', function (e) {
                e.preventDefault();
                Print_Vouchers(0);
            });
            $('.btnprintwithOutHeader').on('click', function (e) {
                e.preventDefault();
                Print_Voucher(0);
            });

            $('#btnAddF, #btnAddC, #btnAddG, #btnAddW, #btnAddM1, #btnAddM2, #btnAddM3, #btnAddM4, #btnAddM5, #btnAddM6, #btnAddM7, #btnAddM8').on('click', function (e) {
                e.preventDefault();

                var itemArray = new Array();
                var itemData = [];

                $('#item_table').find('tbody tr').each(function (index, elem) {
                    var itemId = $.trim($(elem).find('td.item').data('item_id'));
                    var qty = $.trim($(elem).find('td.qty').text());
                    var weight = $.trim($(elem).find('td.weight').text());

                    itemArray.push(itemId);

                    var item = {
                        item_id: itemId,
                        qty: qty,
                        weight:weight
                    }
                    itemData.push(item);
                });

                // console.log(itemArray);
                var deptId = $.trim($(this).parent().parent().find('#dept_dropdown').val());
                var phaseId = $.trim($(this).parent().parent().find('#dept_dropdown').find('option:selected').data('phaseid'));
                console.log(phaseId);
                fetch_present_staff(itemArray, itemData, deptId, phaseId);
            });

            var countItem = 0;
            $('input[id="txtItemId"]').autoComplete({

                minChars: 1,
                cache: false,
                menuClass: '',
                source: function (search, response) {
                    try {
                        xhr.abort();
                    } catch (e) {
                    }
                    $('#txtItemId').removeClass('inputerror');
                    $("#imgItemLoader").hide();
                    if (search != "") {
                        xhr = $.ajax({
                            url: base_url + 'index.php/item/searchitem',
                            type: 'POST',
                            data: {

                                search: search
                            },
                            dataType: 'JSON',
                            beforeSend: function (data) {

                                $(".loader").hide();
                                $("#imgItemLoader").show();
                                countItem = 0;
                            },
                            success: function (data) {

                                if (data == '') {
                                    $('#txtItemId').addClass('inputerror');
                                    clearItemData();
                                    $('#itemDesc').val('');
                                    $('#txtQty').val('');
                                    $('#txtPRate').val('');
                                    $('#txtBundle').val('');
                                    $('#txtGBundle').val('');
                                    $('#txtWeight').val('');
                                    $('#txtAmount').val('');
                                    $('#txtGWeight').val('');
                                    $('#txtDiscp').val('');
                                    $('#txtDiscount1_tbl').val('');
                                }
                                else {
                                    $('#txtItemId').removeClass('inputerror');
                                    response(data);
                                    $("#imgItemLoader").hide();
                                }
                            }
                        });
                    }
                    else {
                        clearItemData();
                    }
                },
                renderItem: function (item, search) {
                    var sea = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                    var re = new RegExp("(" + sea.split(' ').join('|') + ")", "gi");

                    var selected = "";
                    if ((search.toLowerCase() == (item.item_des).toLowerCase() && countItem == 0) || (search.toLowerCase() != (item.item_des).toLowerCase() && countItem == 0)) {
                        selected = "selected";
                    }
                    countItem++;
                    clearItemData();

                    return '<div class="autocomplete-suggestion ' + selected + '" data-val="' + search + '" data-item_id="' + item.item_id + '" data-size="' + item.pack + '" data-bid="' + item.bid +
                        '" data-uom_item="' + item.uom + '" data-srate="' + item.srate + '" data-grweight="' + item.grweight + '" data-stqty="' + item.stqty +
                        '" data-stweight="' + item.stweight + '" data-length="' + item.length + '" data-catid="' + item.catid +
                        '" data-subcatid="' + item.subcatid + '" data-desc="' + item.item_des + '" data-cost_price="' + item.cost_price + '" data-code="' + item.item_code + '" data-self="' + item.self_freight_rate + '" data-bilty="' + item.bilty_freight_rate +
                        '" data-gari="' + item.gari_freight_rate + '">' + item.item_des.replace(re, "<b>$1</b>") + '</div>';
                },
                onSelect: function (e, term, item) {

                    $("#imgItemLoader").hide();
                    $("#hfItemId").val(item.data('item_id'));
                    $("#hfItemSize").val(item.data('size'));
                    $("#hfItemBid").val(item.data('bid'));
                    $("#hfItemUom").val(item.data('uom_item'));
                    $("#hfItemPrate").val(item.data('cost_price'));
                    $("#hfItemGrWeight").val(item.data('grweight'));
                    $("#hfItemStQty").val(item.data('stqty'));
                    $("#hfItemStWeight").val(item.data('stweight'));
                    $("#hfItemLength").val(item.data('length'));
                    $("#hfItemCatId").val(item.data('catid'));
                    $("#hfItemSubCatId").val(item.data('subcatid'));
                    $("#hfItemDesc").val(item.data('desc'));
                    $("#hfItemShortCode").val(item.data('code'));
                    $("#hfItemUname").val(item.data('uname'));


                    var freightStatus = $('input[name=inlineRadioOptions]:checked').val();

                    if (freightStatus == 'self') {
                        $('#txtFRate').val(item.data('self'));
                    } else if (freightStatus == 'bilty') {
                        $('#txtFRate').val(item.data('bilty'));
                    } else if (freightStatus == 'gari') {
                        $('#txtFRate').val(item.data('gari'));
                    }

                    $("#txtItemId").val(item.data('code'));

                    var itemId = item.data('item_id');
                    var itemDesc = item.data('desc');
                    var pRate = item.data('cost_price');
                    var grWeight = item.data('grweight');
                    var uomItem = item.data('uom_item');
                    var stQty = item.data('stqty');
                    var stWeight = item.data('stweight');
                    var size = item.data('size');
                    var brandId = item.data('bid');

                    $('#stqty_lbl').text('Item,     Uom:' + uomItem);
                    $('#itemDesc').val(itemDesc);
                    $('#txtPRate').val(parseFloat(pRate).toFixed(2));
                    $('#txtGrWeight').val(parseFloat(grWeight).toFixed(2));
                    $('#txtUOM').val(uomItem);
                    $('#txtGWeight').trigger('input');
                }
            });

            $('.btnReset').on('click', function (e) {
                e.preventDefault();
                self.resetVoucher();
            });

            $('#txtVrnoa').on('change', function () {
                fetch($(this).val());
            });
            $('#txtSQty').on('input', function () {
              var grWeight = $("#hfItemGrWeight").val();
              var netweight = $(this).val() * parseFloat(grWeight);
              $("#txtWeight").val(netweight);
            });
            

            $('#itemid_dropdown').on('change', function () {
                var item_id = $(this).val();
                $('#item_dropdown').select2('val', item_id);
            });

            $('#item_dropdown').on('change', function () {
                var item_id = $(this).val();
                $('#itemid_dropdown').select2('val', item_id);
            });
        },

        initSave: function () {

            var error = validateSave();
            var errorcalc = validateentrycalculations();
            if (!error && !errorcalc) {
                var saveObj = getSaveObject();
                
                var rowsCount = $('#item_table').find('tbody tr').length;

                if (rowsCount > 0 ) {
                    save(saveObj);
                } else {
                    alert('No data found to save!');
                }
            } else {
                alert('Correct the errors...');
            }
        },
        resetVoucher: function () {
            general.reloadWindow();
        }
    }

};
var headprod = new headprod();
headprod.init();