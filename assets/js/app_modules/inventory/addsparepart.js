var Purchase = function() {

    var saveItem = function( item ) {
        $.ajax({
            url : base_url + 'index.php/item/save',
            type : 'POST',
            data : item,
            // processData : false,
            // contentType : false,
            dataType : 'JSON',
            success : function(data) {

                if (data.error === 'true') {
                    alert('An internal error occured while saving voucher. Please try again.');
                } else {
                    alert('Item saved successfully.');
                    $('#ItemAddModel').modal('hide');
                    fetchItems();
                }
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    var saveAccount = function( accountObj ) {
        $.ajax({
            url : base_url + 'index.php/account/save',
            type : 'POST',
            data : { 'accountDetail' : accountObj },
            dataType : 'JSON',
            success : function(data) {

                if (data.error === 'false') {
                    alert('An internal error occured while saving account. Please try again.');
                } else {
                    alert('Account saved successfully.');
                    $('#AccountAddModel').modal('hide');
                    fetchAccount();
                }
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    var fetchAccount = function() {

        $.ajax({
            url : base_url + 'index.php/account/fetchAll',
            type : 'POST',
            data : { 'active' : 1 },
            dataType : 'JSON',
            success : function(data) {
                if (data === 'false') {
                    alert('No data found');
                } else {
                    populateDataAccount(data);
                }
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }
    var fetchItems = function() {
        $.ajax({
            url : base_url + 'index.php/item/fetchAll',
            type : 'POST',
            data : { 'active' : 1 },
            dataType : 'JSON',
            success : function(data) {
                if (data === 'false') {
                    alert('No data found');
                } else {
                    populateDataItem(data);
                }
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    var populateDataAccount = function(data) {
        $("#party_dropdown").empty();

        $.each(data, function(index, elem){
            var opt="<option value='"+elem.party_id+"' >" +  elem.name + "</option>";
            $(opt).appendTo('#party_dropdown');
        });
    }
    var populateDataItem = function(data) {
        $("#itemid_dropdown").empty();
        $("#item_dropdown").empty();

        $.each(data, function(index, elem){
            var opt="<option value='"+elem.item_id+"' data-prate= '"+ elem.cost_price +"' data-uom_item= '"+ elem.uom +"' data-grweight= '"+ elem.grweight +"' >" +  elem.item_des + "</option>";
            // var = "<option value='" + $item['item_id'] + "' data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>"><?php echo $item['item_des']; ?></option>";
            $(opt).appendTo('#item_dropdown');
            var opt1="<option value='"+elem.item_id+"' data-prate= '"+ elem.cost_price +"' data-uom_item= '"+ elem.uom +"' data-grweight= '"+ elem.grweight +"' >" +  elem.item_id + "</option>";
            // var = "<option value='" + $item['item_id'] + "' data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>"><?php echo $item['item_des']; ?></option>";
            $(opt1).appendTo('#itemid_dropdown');

        });
    }
    var getSaveObjectAccount = function() {

        var obj = {
            pid : '20000',
            active : '1',
            name : $.trim($('#txtAccountName').val()),
            level3 : $.trim($('#txtLevel3').val()),
            dcno : $('#txtVrnoa').val(),
            etype : 'purchase',
            uid : $.trim($('#uid').val()),
            company_id : $.trim($('#cid').val()),
        };

        return obj;
    }
    var getSaveObjectItem = function() {

        var itemObj = {
            item_id : 20000,
            active : '1',
            open_date : $.trim($('#current_date').val()),
            catid : $('#category_dropdown').val(),
            subcatid : $.trim($('#subcategory_dropdown').val()),
            bid : $.trim($('#brand_dropdown').val()),
            barcode : $.trim($('#txtBarcode').val()),
            description : $.trim($('#txtItemName').val()),
            item_des : $.trim($('#txtItemName').val()),
            cost_price : $.trim($('#txtPurPrice').val()),
            srate : $.trim($('#txtSalePrice').val()),
            uid : $.trim($('#uid').val()),
            company_id : $.trim($('#cid').val()),
        };
        return itemObj;
    }

    // checks for the empty fields
    var validateSave = function() {

        var errorFlag = false;
        var partyEl = $('#employee_dropdown11');
        var expenseEl = $('#expenseParty_dropdown');
        var dept = $('#dept_dropdown');

        // remove the error class first
        $('.inputerror').removeClass('inputerror');

        // if ( !deptEl.val() ) {
        // 	deptEl.addClass('inputerror');
        // 	errorFlag = true;
        // }
        if ( !partyEl.val() ) {

            $('#employee_dropdown11').addClass('inputerror');
            errorFlag = true;
        }
        if ( !expenseEl.val() ) {

            $('#expenseParty_dropdown').addClass('inputerror');
            errorFlag = true;
        }
        if ( !dept.val() ) {

            $('#dept_dropdown').addClass('inputerror');
            errorFlag = true;
        }


        return errorFlag;
    }


    var validateSaveItem = function() {

        var errorFlag = false;
        // var _barcode = $('#txtBarcode').val();
        var _desc = $.trim($('#txtItemName').val());
        var cat = $.trim($('#category_dropdown').val());
        var subcat = $('#subcategory_dropdown').val();
        var brand = $.trim($('#brand_dropdown').val());

        // remove the error class first
        $('.inputerror').removeClass('inputerror');
        if ( _desc === '' || _desc === null ) {
            $('#txtItemName').addClass('inputerror');
            errorFlag = true;
        }
        if ( !cat ) {
            $('#category_dropdown').addClass('inputerror');
            errorFlag = true;
        }
        if ( !subcat ) {
            $('#subcategory_dropdown').addClass('inputerror');
            errorFlag = true;
        }
        if ( !brand ) {
            $('#brand_dropdown').addClass('inputerror');
            errorFlag = true;
        }

        return errorFlag;
    }
    var validateSaveAccount = function() {

        var errorFlag = false;
        var partyEl = $('#txtAccountName');
        var deptEl = $('#txtLevel3');

        // remove the error class first
        $('.inputerror').removeClass('inputerror');

        if ( !partyEl.val() ) {
            $('#txtAccountName').addClass('inputerror');
            errorFlag = true;
        }
        if ( !deptEl.val() ) {
            deptEl.addClass('inputerror');
            errorFlag = true;
        }

        return errorFlag;
    }
    var save = function(production) {
        general.disableSave();
        // alert(production.stockmain['company_id']);
        $.ajax({
            url : base_url + 'index.php/sparepart/save',
            type : 'POST',
            data : {'vrdate' : $('#vrdate').val(), 'stockmain' : production.stockmain, 'stockdetail' : production.stockdetail, 'vrnoa' : production.vrnoa, 'ledger' : production.ledger ,'voucher_type_hidden':$('#voucher_type_hidden').val() },
            dataType : 'JSON',
            success : function(data) {

                if (data.error === 'date close') {
                    alert('Sorry! you can not insert update in close date................');
                }else if (data.error === 'true') {
                    alert('An internal error occured while saving voucher. Please try again.');
                } else {
                    var printConfirmation = confirm('Voucher saved!\nWould you like to print the invoice as well?');
                    if (printConfirmation === true) {
                        Print_Voucher(1);
                    } else {
                        general.reloadWindow();
                    }
                }
                general.enableSave();
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }
    var Print_Voucher = function(hd) {
        if ( $('.btnSave').data('printbtn')==0 ){
            alert('Sorry! you have not print rights..........');
        }else{
            var etype=  'spare_parts';
            var vrnoa = $('#txtVrnoa').val();
            var company_id = $('#cid').val();
            var user = $('#uname').val();
            // var hd = $('#hd').val();
            var pre_bal_print =  '0';

            var url = base_url + 'index.php/doc/print_pro_voucher/' + etype + '/' + vrnoa + '/' + company_id + '/' + '-1' + '/' + user + '/' + pre_bal_print+ '/' + hd;
            // var url = base_url + 'index.php/doc/CashVocuherPrintPdf/' + etype + '/' + dcno   + '/' + companyid + '/' + '-1' + '/' + user;
            window.open(url);
        }

    }
    var Print_Vouchers = function(hd) {
        if ( $('.btnSave').data('printbtn')==0 ){
            alert('Sorry! you have not print rights..........');
        }else{
            var etype=  'spare_parts';
            var vrnoa = $('#txtVrnoa').val();
            var company_id = $('#cid').val();
            var user = $('#uname').val();
            // var hd = $('#hd').val();
            var pre_bal_print =  '0';

            var url = base_url + 'index.php/doc/print_pro_vouchers/' + etype + '/' + vrnoa + '/' + company_id + '/' + '-1' + '/' + user + '/' + pre_bal_print+ '/' + hd;
            // var url = base_url + 'index.php/doc/CashVocuherPrintPdf/' + etype + '/' + dcno   + '/' + companyid + '/' + '-1' + '/' + user;
            window.open(url);
        }

    }
    var fetchThroughPO = function(poNo) {

        $.ajax({

            url : base_url + 'index.php/purchaseorder/fetch',
            type : 'POST',
            data : { 'vrnoa' : poNo },
            dataType : 'JSON',
            success : function(data) {

                $('#purchase_table').find('tbody tr').remove();
                if (data === 'false') {
                    alert('No data found.');
                } else {
                    populatePOData(data);
                }

            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    var populatePOData = function(data) {

        $.each(data, function(index, elem) {
            appendToTable('1', elem.item_name, elem.item_id, elem.godown_id,elem.dept_name,elem.uom, elem.item_qty, elem.weight, elem.rate, elem.amount,elem.perday,elem.produced);
            calculateNetQty(elem.item_qty,elem.weight);
        });
    }

    var fetchStaffPhaseData = function(employeeId) {

        $.ajax({

            url : base_url + 'index.php/employeeproduction/fetchStaffPhaseData',
            type : 'POST',
            data : { 'employee_id' : employeeId ,'company_id': $('#cid').val() },
            dataType : 'JSON',
            success : function(data) {

                if (data === 'false') {

                    //$('#purchase_table tbody tr').remove();
                    alert("No item Found Against This Employee");
                } else {
                    $('#item_dropdown').empty();
                    $('#itemid_dropdown').empty();
                    $('#item_dropdown').append("<option value=''> Choose Item </option>");
                    $('#itemid_dropdown').append("<option value=''> Choose Code</option>");
                    $.each(data, function(index, elem) {

                        //console.log(data);
                        //elem.phase_name = (elem.phase_name) ? elem.phase_name : '-';
                        //elem.phase_id = (elem.phase_id) ? elem.phase_id : 0;
                        //appendToTable('1', elem.item_des, elem.item_id, elem.phase_name, elem.phase_id, elem.grweight, 1, 0, elem.rate, elem.rate);
                        //calculateNetQty(1 , 0, elem.rate);
                        //$('#item_dropdown').select2('val', data[0]['item_des']);

                        var option="<option value='"+  elem['item_id'] +"' data-uom='"+  elem['uom'] +"' data-stqty='"+  elem['stqty'] +"'data-grweight='"+  elem['grweight']+"'data-stweight='"+  elem['stweight'] +"'>  " + elem['item_des'] + "</option>";
                        var item_iddropdown="<option value='"+  elem['item_id'] +"' data-uom='"+  elem['uom'] + "' data-stqty='"+  elem['stqty'] +"'data-grweight='"+  elem['grweight']+"'data-stweight='"+  elem['stweight'] +"' >  " + elem['item_id'] + "</option>";
                        //option.appendTo($('#item_dropdown'));
                        //console.log(option);
                        $('#item_dropdown').append(option);
                        $('#itemid_dropdown').append(item_iddropdown);
                    });
                }

            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    var fetch = function(vrnoa) {

        $.ajax({

            url : base_url + 'index.php/sparepart/fetch',
            type : 'POST',
            data : { 'vrnoa' : vrnoa ,'company_id': $('#cid').val() },
            dataType : 'JSON',
            success : function(data) {

                $('#purchase_table').find('tbody tr').remove();
                $('#txtGWeight').val('');
                $('#txtGQty').val('');
                $('#txtGAmount').val('');
                if (data === 'false') {
                    alert('No data found.');
                    getMaxVrnoa(); 
                } else {
                    populateData(data);
                }

            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    var populateData = function(data) {
        $('#voucher_type_hidden').val('edit');

        $('#txtVrnoHidden').val(data[0]['vrnoa']);
        $('#txtVrnoa').val(data[0]['vrnoa']);
        $('#txtVrnoaHidden').val(data[0]['vrnoa']);
        $('#txtIdImg').val(data[0]['vrnoa']);
        $('#current_date').datepicker('update', data[0]['vrdate'].substr(0, 10));
        $('#vrdate').datepicker('update', data[0]['vrdate'].substr(0, 10));
        $('#receivers_list').val(data[0]['received_by']);
        $('#txtRemarks').val(data[0]['remarks']);
        $('#dept_dropdown').select2('val', data[0]['main_godown_id']);
        $('#expenseParty_dropdown').select2('val', data[0]['expense_id']);
        $('#employee_dropdown11').select2('val', data[0]['employee_id']);


        $.each(data, function(index, elem) {
            elem.amount=parseFloat(elem.amount).toFixed(2);
            elem.phase_name = (elem.phase_name) ? elem.phase_name : '-';
            elem.phase_id = (elem.phase_id) ? elem.phase_id : 0;
            appendToTable('1', elem.item_name, elem.item_id, elem.phase_name, elem.phase_id, elem.grweight, elem.qty, elem.weight, elem.rate, elem.amount,elem.perday,elem.produced);
            calculateNetQty(elem.qty,elem.weight,elem.amount);
        });
    }

    // gets the max id of the voucher
    var getMaxVrno = function() {

        $.ajax({

            url : base_url + 'index.php/sparepart/getMaxVrno',
            type : 'POST',
            data : {'company_id':$('#cid').val()},
            dataType : 'JSON',
            success : function(data) {

                $('#txtVrno').val(data);
                $('#txtMaxVrnoHidden').val(data);
                $('#txtVrnoHidden').val(data);
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    var getMaxVrnoa = function() {

        $.ajax({

            url : base_url + 'index.php/sparepart/getMaxVrnoa',
            type : 'POST',
            data : {'company_id':$('#cid').val()},
            dataType : 'JSON',
            success : function(data) {

                $('#txtVrnoa').val(data);
                $('#txtMaxVrnoaHidden').val(data);
                $('#txtVrnoaHidden').val(data);
                $('#txtIdImg').val(data);
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }

    var validateSingleProductAdd = function() {


        var errorFlag = false;
        var item_id = $('#item_dropdown').val();
        var qty = $('#txtSQty').val();
        var phase = $('#phase_dropdown').val();

        // remove the error class first
        $('#item_dropdown').removeClass('inputerror');
        $('#txtSQty').removeClass('inputerror');
        $('#phase_dropdown').removeClass('inputerror');

        if ( item_id === '' || item_id === null ) {
            $('#item_dropdown').addClass('inputerror');
            errorFlag = true;
        }

        if ( qty === '' || qty === null ) {
            $('#txtSQty').addClass('inputerror');
            errorFlag = true;
        }

        if ( phase === '' || phase === null ) {
            $('#phase_dropdown').addClass('inputerror');
            errorFlag = true;
        }

        return errorFlag;
    }

    var appendToTable = function(srno, item_desc, item_id, phase, phase_id, gWeight, qty , weight, rate, amount,perday,produced) {

        var srno = $('#purchase_table tbody tr').length + 1;
        amount=parseFloat(amount).toFixed(2);

        var row = 	"<tr>" +
            "<td class='srno'> "+ srno +"</td>" +
            "<td class='item' data-item_id='"+ item_id +"'> "+ item_desc +"</td>" +
            "<td class='phase' data-phase_id='"+ phase_id +"'> "+ phase +"</td>" +
            "<td class='gWeight'> "+ gWeight +"</td>" +
            "<td class='qty'> "+ qty +"</td>" +
            "<td class='weight'> "+ weight +"</td>" +
            "<td class='rate'> "+ rate +"</td>" +
            "<td class='amount'> "+ amount +"</td>" +
            "<td class='perday hide'> "+ perday +"</td>" +
            "<td class='produced hide'> "+ produced +"</td>" +
            "<td><a href='' class='btn btn-primary btnRowEdit'><span class='fa fa-edit'></span></a> <a href='' class='btn btn-primary btnRowRemove'><span class='fa fa-trash-o'></span></a> </td>" +
            "</tr>";
        $(row).appendTo('#purchase_table');
    }

    var getSaveObject = function() {

        var stockmain = {};
        var stockdetail = [];
        var stoweightetail = [];
        var ledgers = [];

        stockmain.vrnoa = $('#txtVrnoaHidden').val();
        stockmain.vrno = $('#txtVrnoHidden').val();
        stockmain.remarks = $('#txtRemarks').val();
        stockmain.godown_id = $('#dept_dropdown').val();
        stockmain.employee_id = $('#employee_dropdown11').val();
        stockmain.expense_id = $('#expenseParty_dropdown').val();
        stockmain.received_by = $('#receivers_list').val();
        stockmain.etype = 'spare_parts';
        stockmain.uid = $('#uid').val();
        stockmain.company_id = $('#cid').val();
        stockmain.vrdate = $('#current_date').val();

        $('#purchase_table').find('tbody tr').each(function( index, elem ) {
            var od = {};

            od.oid = '';
            od.item_id = $.trim($(elem).find('td.item').data('item_id'));
            od.phase_id = $.trim($(elem).find('td.phase').data('phase_id'));
            od.qty = $.trim($(elem).find('td.qty').text());
            od.godown_id = $('#dept_dropdown').val();
            //od.uom = $.trim($(elem).find('td.uom').text());
            od.weight = $.trim($(elem).find('td.weight').text());
            od.rate = $.trim($(elem).find('td.rate').text());
            od.amount = $.trim($(elem).find('td.amount').text());
            od.perday=$.trim($(elem).find('td.perday').text());
            od.produced=$.trim($(elem).find('td.produced').text());
            stockdetail.push(od);
        });

        var pledger = {};
        pledger.pledid = '';
        pledger.pid = $('#employee_dropdown11').val();
        pledger.description =  $('#txtRemarks').val();
        pledger.remarks =  $('#txtRemarks').val();
        pledger.date = $('#current_date').val();
        pledger.debit = 0;
        pledger.credit = parseInt($('#txtGAmount').val());
        pledger.dcno = $('#txtVrnoaHidden').val();
        pledger.invoice = $('#txtVrnoaHidden').val();
        pledger.etype = 'spare_parts';
        pledger.pid_key = $('#expenseParty_dropdown').val();
        pledger.uid = $('#uid').val();
        pledger.company_id = $('#cid').val();
        pledger.isFinal = 0;
        ledgers.push(pledger);

        var pledger = {};
        pledger.pledid = '';
        pledger.pid = $('#expenseParty_dropdown').val();
        pledger.description = $('#expenseParty_dropdown').find('option:selected').text();
        pledger.remarks =  $('#txtRemarks').val();
        pledger.date = $('#current_date').val();
        pledger.debit =  parseInt($('#txtGAmount').val());
        pledger.credit = 0;
        pledger.dcno = $('#txtVrnoaHidden').val();
        pledger.invoice = $('#txtInvNo').val();
        pledger.etype = 'spare_parts';
        pledger.pid_key = $('#employee_dropdown11').val();
        pledger.uid = $('#uid').val();
        pledger.company_id = $('#cid').val();
        pledger.isFinal = 0;
        ledgers.push(pledger);

        var data = {};
        data.stockmain = stockmain;
        data.stockdetail = stockdetail;
        data.ledger = ledgers;
        data.vrnoa = $('#txtVrnoaHidden').val();

        return data;
    }

    var deleteVoucher = function(vrnoa) {

        $.ajax({
            url : base_url + 'index.php/sparepart/delete',
            type : 'POST',
            data : { 'chk_date' : $('#current_date').val(), 'vrdate' : $('#vrdate').val(), 'vrnoa' : vrnoa,'company_id': $('#cid').val() },
            dataType : 'JSON',
            success : function(data) {

                if (data.error === 'date close') {
                    alert('Sorry! you can not delete in close date................');
                }else if (data === 'false') {
                    alert('No data found');
                } else {
                    alert('Voucher deleted successfully');
                    general.reloadWindow();
                }
            }, error : function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }


    var calculateNetQty = function(qty, weight , amount) {

        var _qty = ($('#txtGQty').val() == "") ? 0 : $('#txtGQty').val();
        var _weight = ($('#txtGWeight').val() == "") ? 0 : $('#txtGWeight').val();
        var _amount = ($('#txtGAmount').val() == "") ? 0 : $('#txtGAmount').val();

        var tempQty = parseFloat(_qty) + parseFloat(qty);
        var tempWeight = parseFloat(_weight) + parseFloat(weight);
        var tempAmount = parseFloat(_amount) + parseFloat(amount);
        $('#txtGQty').val(parseFloat(tempQty).toFixed(2));
        $('#txtGWeight').val(parseFloat(tempWeight).toFixed(2));
        $('#txtGAmount').val(parseFloat(tempAmount).toFixed(2));

    }

    var calculateUpperTotal = function() {

        //var job_cost = ($('#job_dropdown').val() == '') ? 0 : $('#job_dropdown').find('option:selected').data('job_cost');
        var qty = ($('#txtSQty').val() == '') ? 0 : $('#txtSQty').val();
        var rate =($('#txtRate').val() == '') ? 0 : $('#txtRate').val();
        var perday=($('#txtperday').val() == '') ? 0 : $('#txtperday').val();

        var net = parseFloat(rate) * parseFloat(qty);
        var produced =   parseFloat(qty) /parseFloat(perday);
        $('#txtproduced').val(produced);
        $('#txtAmount').val(net);

    }

    var paging = function (total)
    {
        $("#paging").smartpaginator({
            totalrecords: parseInt(total),
            recordsperpage: 10,
            datacontainer: 'tbody',
            dataelement: 'tr',
            theme: 'custom',
            onchange: onChange
        });
    }

    var onChange = function (newPageValue)
    {
        $.ajax({
            type: "POST",
            url: base_url + 'index.php/item/paging',
            data: {
                page_number: newPageValue,
                search: $.trim($("#txtItemSearch").val())
            }
        }).done(function (result) {

            var total = "";
            var jsonR = JSON.stringify($.parseJSON(result)).replace(/\\n/g, "\\n")
                .replace(/\\'/g, "\\'")
                .replace(/\\"/g, '\\"')
                .replace(/\\&/g, "\\&")
                .replace(/\\r/g, "\\r")
                .replace(/\\t/g, "\\t")
                .replace(/\\b/g, "\\b")
                .replace(/\\f/g, "\\f");
            var json = $.parseJSON(jsonR);
            total = json.total;

            if (json.records != false) {
                var html = "";
                $.each(json.records, function (index, value) {
                    html += "<tr id='" + value.item_id + "'>";
                    html += "<td>"+value.item_id+"<input type='hidden' name='hfModalitemId' value='" + value.item_id + "'></td>";
                    html += "<td class='tdItemCode'><b>" + value.item_code + "<b></td>";
                    html += "<td class='tdDescription'><b>" + value.item_des + "</b></td>";
                    html += "<td class='tdItemUom'><b>" + value.uom + "</b></td>";
                    html += "<td class='tdPRate hide'>" + value.cost_price + "</td>";
                    html += "<td class='tdGrWeight hide'>" + value.grweight + "</td>";
                    html += "<td class='tdStQty hide'>" + value.stqty + "</td>";
                    html += "<td class='tdStWeight hide'>" + value.stweight + "</td>";
                    html += "<td class='tdUItem hide'>" + value.uitem_des + "</td>";
                    html += "<td><a href='#' data-dismiss='modal' class='btn btn-primary populateItem'><i class='fa fa-search'></i></a></td>"
                    html += "</tr>";
                });

                $("#divNoRecord").removeClass('divNoRecord');
                $("#divNoRecord").html('');
                $("#tbItems > tbody").html('');
                $("#tbItems > tbody").append(html);

                $('.modal-lookup .populateItem').on('click', function()
                {
                    var itemId = $(this).closest('tr').find('input[name=hfModalitemId]').val();
                    var itemDescription = $(this).closest('tr').find('.tdDescription').text();
                    var uom = $(this).closest('tr').find('.tdItemUom').text();
                    var pRate = $(this).closest('tr').find('.tdPRate').text();
                    var grWeight = $(this).closest('tr').find('.tdGrWeight').text();
                    var stQty = $(this).closest('tr').find('.tdStQty').text();
                    var stWeight = $(this).closest('tr').find('.tdStWeight').text();
                    var uItemName = $(this).closest('tr').find('.tdUItem').text();
                    var itemCode = $(this).closest('tr').find('.tdItemCode').text();

                    var itemCodeOption = "<option value='" + itemId + "' data-uom_item='" + uom + "' data-prate='" + pRate + "' data-grweight='" + grWeight + "' data-stqty='" + stQty + "' data-stweight='" + stWeight + "' data-uitem_des='" + uItemName + "' >" + itemCode + "</option>";
                    var itemOption = "<option value='" + itemId + "'  data-uom_item='" + uom + "' data-prate='" + pRate + "' data-grweight='" + grWeight + "' data-stqty='" + stQty + "' data-stweight='" + stWeight + "'  data-uitem_des='" + uItemName + "' >" + itemDescription + "</option>";
                    $('#itemid_dropdown').append(itemCodeOption);
                    $('#item_dropdown').append(itemOption);

                    $('#itemid_dropdown').val(itemId).trigger('change');
                    $('#item_dropdown').val(itemId).trigger('change');

                    //$('#txtPRate').val(parseFloat(pRate).toFixed(2));

                    $('#txtQty').focus();
                });

                $("#paging").show();
                $("#tbItems").show();
                if (newPageValue == "1") {
                    paging(total);
                }
            }
            else {
                $("#tbItems > tbody").html('');
                $("#divNoRecord").show();
                $("#divNoRecord").addClass('divNoRecord');
                $("#paging").hide();
                $("#divNoRecord").html('No Record Found');
            }
        });
    }

    var getItemInfo = function(itemId)
    {
        $.ajax({
            type: "POST",
            url: base_url + 'index.php/item/getiteminfo',
            data: {
                item_id: itemId
            }
        }).done(function (result) {

            var jsonR = JSON.stringify($.parseJSON(result)).replace(/\\n/g, "\\n")
                .replace(/\\'/g, "\\'")
                .replace(/\\"/g, '\\"')
                .replace(/\\&/g, "\\&")
                .replace(/\\r/g, "\\r")
                .replace(/\\t/g, "\\t")
                .replace(/\\b/g, "\\b")
                .replace(/\\f/g, "\\f");
            var json = $.parseJSON(jsonR);

            if (json.records != false)
            {
                $.each(json.records, function (index, value) {
                    var itemCodeOption = "<option value='" + value.item_id + "' data-uom_item='" + value.uom + "' data-data-prate='" + value.cost_price + "' data-grweight='" + value.grweight + "' data-stqty='" + value.stqty + "' data-stweight='" + value.stweight + "' >" + value.item_code + "</option>";
                    var itemOption = "<option value='" + value.item_id + "'  data-uom_item='" + value.uom + "' data-data-prate='" + value.cost_price + "' data-grweight='" + value.grweight + "' data-stqty='" + value.stqty + "' data-stweight='" + value.stweight + "' >" + value.item_des + "</option>";
                    $('#itemid_dropdown').append(itemCodeOption);
                    $('#item_dropdown').append(itemOption);

                    $('#itemid_dropdown').select2('val', itemId);
                    $('#item_dropdown').select2('val', itemId);

                    var grweight = $('#item_dropdown').find('option:selected').data('grweight');
                    $('#txtGWeight').val(parseFloat(grweight).toFixed());
                });
            }
        });
    }

    return {

        init : function() {
            $('#voucher_type_hidden').val('new');
            this.bindUI();
        },

        bindUI : function() {
            var self = this;

            $('#txtLevel3').on('change', function() {

                var level3 = $('#txtLevel3').val();
                $('#txtselectedLevel1').text('');
                $('#txtselectedLevel2').text('');
                if (level3 !== "" && level3 !== null) {
                    // alert('enter' + $(this).find('option:selected').data('level2') );
                    $('#txtselectedLevel2').text(' ' + $(this).find('option:selected').data('level2'));
                    $('#txtselectedLevel1').text(' ' + $(this).find('option:selected').data('level1'));
                }
            });
            // $('#txtLevel3').select2();
            $('.btnSaveM').on('click',function(e){
                if ( $('.btnSave').data('saveaccountbtn')==0 ){
                    alert('Sorry! you have not save accounts rights..........');
                }else{
                    e.preventDefault();
                    self.initSaveAccount();
                }
            });
            $('.btnPrint').on('click',  function(e) {
                e.preventDefault();
                Print_Voucher(1);
            });
            $('.btnPrints').on('click',  function(e) {
                e.preventDefault();
                Print_Vouchers(0);
            });
            $('.btnprintwithOutHeader').on('click', function(e) {
                e.preventDefault();
                Print_Voucher(0);
            });
            $('.btnResetM').on('click',function(){

                $('#txtAccountName').val('');
                $('#txtselectedLevel2').text('');
                $('#txtselectedLevel1').text('');
                $('#txtLevel3').select2('val','');
            });
            $('#AccountAddModel').on('shown.bs.modal',function(e){
                $('#txtAccountName').focus();
            });
            shortcut.add("F3", function() {
                $('#AccountAddModel').modal('show');
            });

            $('.btnSaveMItem').on('click',function(e){
                if ( $('.btnSave').data('saveitembtn')==0 ){
                    alert('Sorry! you have not save item rights..........');
                }else{
                    e.preventDefault();
                    self.initSaveItem();
                }
            });

            $('.btnSave').on('click',  function(e) {
                if ($('#voucher_type_hidden').val()=='edit' && $('.btnSave').data('updatebtn')==0 ){
                    alert('Sorry! you have not update rights..........');
                }else if($('#voucher_type_hidden').val()=='new' && $('.btnSave').data('insertbtn')==0){
                    alert('Sorry! you have not insert rights..........');
                }else{
                    e.preventDefault();
                    self.initSave();
                }
            });

            $('.btnReset').on('click', function(e) {
                e.preventDefault();
                self.resetVoucher();
            });

            $('.btnDelete').on('click', function(e){
                e.preventDefault();
                var vrnoa = $('#txtVrnoa').val();
                if (vrnoa !== '') {
                    if ($('.btnSave').data('deletebtn')==0)
					{
						alert('Sorry! you have no Delete rights..........');
					}
					else
					{
                    if (confirm('Are you sure to delete this voucher?'))
                    deleteVoucher(vrnoa);
                    }
                }
            });

            $('#job_dropdown').on('change', function(){
                calculateUpperTotal();
            });
            $('#txtSQty').on('input', function() {
                calculateUpperTotal();
            });
            shortcut.add("F10", function() {
                self.initSave();
            });
            shortcut.add("F2", function() {
                $('a[href="#item-lookup"]').trigger('click');
            });
            shortcut.add("F9", function() {
                Print_Voucher();
            });
            shortcut.add("F6", function() {
                $('#txtVrnoa').focus();
                // alert('focus');
            });
            shortcut.add("F5", function() {
                self.resetVoucher();
            });

            shortcut.add("F12", function() {
                var vrnoa = $('#txtVrnoa').val();
                if (vrnoa !== '') {
                    deleteVoucher(vrnoa);
                }
            });


            /////////////////////////////////////////////////////////////////
            /// setting calculations for the single product
            /////////////////////////////////////////////////////////////////


            $('#itemid_dropdown').on('change', function() {
                var item_id = $(this).val();
                var uom = $(this).find('option:selected').data('uom');
                var grweight = $(this).find('option:selected').data('grweight');
                $('#item_dropdown').select2('val', item_id);
                //alert(grweight);
                $('#txtGW').val(grweight);
                var stqty = $(this).find('option:selected').data('stqty');
                var stweight = $(this).find('option:selected').data('stweight');
                $('#stqty_lbl').text('Item,     Qty:' + stqty + ', Weight ' + stweight);

            });
            $('#item_dropdown').on('change', function() {
                $.ajax({
                    url : base_url + 'index.php/employeeproduction/fetchStaffPhaseInfo',
                    type : 'POST',
                    data : { 'employee_id' : $('#employee_dropdown11').val() ,'company_id': $('#cid').val(),'item_id': $('#item_dropdown').val() },
                    dataType : 'JSON',
                    success : function(data) {

                        if (data === 'false') {

                            //$('#purchase_table tbody tr').remove();
                        } else {
                            $('#phase_dropdown').empty();
                            $('#phase_dropdown').append("<option value=''> Choose Phase</option>");
                            $.each(data, function(index, elem) {
                                var option="<option value='"+  elem['id'] +"' data-rate='"+  elem['rate'] +"' data-perday='"+  elem['perday'] +"'> " + elem['name'] + " </option>"
                                $('#phase_dropdown').append(option)
                            });
                        }

                    }, error : function(xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });


                var item_id = $(this).val();
                var uom = $(this).find('option:selected').data('uom_item');
                var grweight = $(this).find('option:selected').data('grweight');
                $('#itemid_dropdown').select2('val', item_id);
                $('#txtGW').val(grweight);
                var stqty = $(this).find('option:selected').data('stqty');
                var stweight = $(this).find('option:selected').data('stweight');
                $('#stqty_lbl').text('Item,     Qty:' + stqty + ', Weight ' + stweight);

            });
            $('#phase_dropdown').on('change', function(e) {
                e.preventDefault();
                var rate = $(this).find('option:selected').data('rate');
                var perday=$(this).find('option:selected').data('perday');
                $('#txtRate').val(rate);
                $('#txtperday').val(perday);
                calculateUpperTotal();
            });

            $('#btnAdd').on('click', function(e) {
                e.preventDefault();

                var error = validateSingleProductAdd();
                if (!error) {

                    var item_desc = $('#item_dropdown').find('option:selected').text();
                    var item_id = $('#item_dropdown').val();
                    var phase = $('#phase_dropdown').find('option:selected').text();
                    var phase_id = $('#phase_dropdown').val();
                    var qty = $('#txtSQty').val();
                    var gWeight = $('#txtGW').val();
                    var weight = $('#txtWeight').val();
                    var rate = $('#txtRate').val();
                    var amount = $('#txtAmount').val();
                    var perday=$('#txtperday').val();
                    var produced=$('#txtproduced').val();

                    // reset the values of the annoying fields
                    $('#itemid_dropdown').select2('val', '');
                    $('#item_dropdown').select2('val', '');
                    $('#txtSQty').val('');
                    $('#txtGW').val('');
                    $('#phase_dropdown').select2('val', '');
                    $('#txtWeight').val('');
                    $('#stqty_lbl').text('Item');
                    $('#txtRate').val('');
                    $('#txtAmount').val('');
                    $('#txtperday').val('');
                    $('#txtproduced').val('');
                    appendToTable('1', item_desc, item_id, phase, phase_id,gWeight, qty, weight, rate, amount,perday,produced);
                    calculateNetQty(qty, weight, amount);
                } else {
                    alert('Correct the errors!');
                }

            });

            // when btnRowRemove is clicked
            $('#purchase_table').on('click', '.btnRowRemove', function(e) {
                e.preventDefault();
                var qty = $.trim($(this).closest('tr').find('td.qty').text());
                calculateNetQty("-"+qty);
                $(this).closest('tr').remove();
            });
            $('#purchase_table').on('click', '.btnRowEdit', function(e) {
                e.preventDefault();

                // getting values of the cruel row
                var item_id = $.trim($(this).closest('tr').find('td.item').data('item_id'));
                var qty = $.trim($(this).closest('tr').find('td.qty').text());
                var weight = $.trim($(this).closest('tr').find('td.weight').text());
                var phase_id = $.trim($(this).closest('tr').find('td.phase').data('phase_id'));
                var gWeight = $.trim($(this).closest('tr').find('td.gWeight').text());
                var rate = $.trim($(this).closest('tr').find('td.rate').text());
                var amount = $.trim($(this).closest('tr').find('td.amount').text());
                //alert(amount);
                //getItemInfo(item_id);


                $('#itemid_dropdown').select2('val', item_id);
                $('#item_dropdown').select2('val', item_id);
                $('#txtSQty').val(qty);
                $('#phase_dropdown').select2('val', phase_id);
                $('#txtGW').val(gWeight);
                $('#txtWeight').val(weight);
                $('#txtRate').val(rate);
                $('#txtAmount').val(amount);

                calculateNetQty("-"+qty ,"-"+weight, "-"+amount);

                // now we have get all the value of the row that is being deleted. so remove that cruel row
                $(this).closest('tr').remove();	// yahoo removed
            });

            $('#employee_dropdown11').on('change', function() {
                var employeeId = $(this).val();
                fetchStaffPhaseData(employeeId);
            });
            $('#txtVrnoa').on('change', function() {
                fetch($(this).val());
            });
            $('#txtVrnoa').on('keypress', function(e) {

                if (e.keyCode === 13) {
                    e.preventDefault();
                    var vrnoa = $('#txtVrnoa').val();
                    if (vrnoa !== '') {
                        fetch(vrnoa);
                    }
                }
            });

            $('#employeeScanId').on('keypress', function(e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    var employeeId = $(this).val();
                    if (employeeId !== '') {

                        $('#purchase_table tbody tr').remove();
                        $('#employee_dropdown11').select2('val', employeeId).trigger('change');
                    }
                }
            });

            $('#txtPoNo').on('keypress', function(e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    var poNo = $('#txtPoNo').val();
                    if (poNo !== '') {
                        fetchThroughPO(poNo);
                    }
                }
            });

            onChange(1);
            $("input[id$='txtItemSearch']").keydown(function(event){
                var keyCode = event.keyCode;
                if(keyCode == 13)
                {
                    onChange(1);
                    event.preventDefault();
                    return false;
                }
                else if(keyCode == 8 || keyCode == 46)
                {
                    var txt = $("input[id$='txtItemSearch']").val();
                    if(txt.length <= 1)
                    {
                        $("input[id$='txtItemSearch']").val('');
                        onChange(1);
                    }
                }
            });

            purchase.fetchRequestedVr();
        },

        // prepares the data to save it into the database
        initSave : function() {

            var error = validateSave();

            if (!error) {
                var saveObj = getSaveObject();

                var rowsCount = $('#purchase_table').find('tbody tr').length;

                if (rowsCount > 0 ) {
                    save(saveObj);
                } else {
                    alert('No data found to save!');
                }
            }else{
                alert('Correct the errors...');
            }
        },
        fetchRequestedVr : function () {

            var vrnoa = general.getQueryStringVal('vrnoa');
            vrnoa = parseInt( vrnoa );
            $('#txtVrnoa').val(vrnoa);
            $('#txtVrnoaHidden').val(vrnoa);
            if ( !isNaN(vrnoa) ) {
                fetch(vrnoa);
            }else{
                getMaxVrno();
                getMaxVrnoa();
            }
        },
        initSaveAccount : function() {

            var saveObjAccount = getSaveObjectAccount();
            var error = validateSaveAccount();

            if (!error) {
                saveAccount(saveObjAccount);
            } else {
                alert('Correct the errors...');
            }
        },
        initSaveItem : function() {

            var saveObjItem = getSaveObjectItem();
            var error = validateSaveItem();

            if (!error) {
                saveItem(saveObjItem);
            } else {
                alert('Correct the errors...');
            }
        },

        bindModalPartyGrid : function() {


            var dontSort = [];
            $('#party-lookup table thead th').each(function () {
                if ($(this).hasClass('no_sort')) {
                    dontSort.push({ "bSortable": false });
                } else {
                    dontSort.push(null);
                }
            });
            purchase.pdTable = $('#party-lookup table').dataTable({
                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
                "aaSorting": [[0, "asc"]],
                "bPaginate": true,
                "sPaginationType": "full_numbers",
                "bJQueryUI": false,
                "aoColumns": dontSort

            });
            $.extend($.fn.dataTableExt.oStdClasses, {
                "s`": "dataTables_wrapper form-inline"
            });
        },

        bindModalItemGrid : function() {


            var dontSort = [];
            $('#item-lookup table thead th').each(function () {
                if ($(this).hasClass('no_sort')) {
                    dontSort.push({ "bSortable": false });
                } else {
                    dontSort.push(null);
                }
            });
            purchase.pdTable = $('#item-lookup table').dataTable({
                // "sDom": "<'row-fluid table_top_bar'<'span12'>'<'to_hide_phone'>'f'<'>r>t<'row-fluid'>",
                "sDom": "<'row-fluid table_top_bar'<'span12'<'to_hide_phone' f>>>t<'row-fluid control-group full top' <'span4 to_hide_tablet'l><'span8 pagination'p>>",
                "aaSorting": [[0, "asc"]],
                "bPaginate": true,
                "sPaginationType": "full_numbers",
                "bJQueryUI": false,
                "aoColumns": dontSort

            });
            $.extend($.fn.dataTableExt.oStdClasses, {
                "s`": "dataTables_wrapper form-inline"
            });
        },

        // instead of reseting values reload the page because its cruel to write to much code to simply do that
        resetVoucher : function() {
            general.reloadWindow();
        }
    }

};

var purchase = new Purchase();
purchase.init();