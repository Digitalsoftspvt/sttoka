var PrivillagesGroup = function(){

	var validateSignIn = function() {

		var errorFlag = [];
		var name = $.trim($('#txtUsername').val());
		var pass = $.trim($('#txtPassowrd').val());
		var mobcode = $.trim($('#txtMobCode').val());
		//var fn_dropdown = $.trim($('#fn_dropdown').val());


		// remove previous errors
		$('.login-error').remove();

		if ( name === '' || name === null ) {
			$('#txtUsername').addClass('inputerror');
			errorFlag.push("Enter Username.");
		}
		// if ( fn_dropdown === '' || fn_dropdown === null ) {
		// 	$('#fn_dropdown').addClass('inputerror');
		// 	errorFlag.push("Enter Financial Year.");
		// }
		if ( pass === '' || pass === null ) {
			$('#txtPassowrd').addClass('inputerror');
			errorFlag.push("Enter Password.");
		}
		if ( mobcode === '' || mobcode === null ) {
			$('#txtMobCode').addClass('inputerror');
			errorFlag.push("Enter Mobile Code.");
		}

		return errorFlag;
	}

	var validateSignInMobCode = function() {

		var errorFlag = [];
		var name = $.trim($('#txtUsername').val());
		var pass = $.trim($('#txtPassowrd').val());
		

		// remove previous errors
		$('.login-error').remove();

		if ( name === '' || name === null ) {
			$('#txtUsername').addClass('inputerror');
			errorFlag.push("Enter Username.");
		}
		if ( pass === '' || pass === null ) {
			$('#txtPassowrd').addClass('inputerror');
			errorFlag.push("Enter Password.");
		}
		

		return errorFlag;
	}

	var fetch = function(uname, pass, mob_code,fn_dropdown,fn_name,fn_sdate,fn_edate) {
		$.ajax({
			url : base_url + 'index.php/welcome/login',
			type : 'POST',
			data : { uname : uname, pass : pass, mob_code: mob_code,fn_dropdown:fn_dropdown,fn_name:fn_name,fn_sdate:fn_sdate,fn_edate:fn_edate},
			dataType : 'JSON',
			success : function(data) {
				if (data.error == true) {
					var span = "<span class='login-error'>Invalid username or password entered.</span>";
					$(span).appendTo('.errors_section');
				} else {				
					window.location = base_url + 'index.php/wall/dashboard';
				}
			}, error : function(xhr, status, error) {
				window.location = base_url + 'index.php/welcome/login';
				console.log(xhr.responseText);
			}
		});
	}

	var fetchCode = function(uname, pass) {
		$.ajax({
			url : base_url + 'index.php/welcome/loginCode',
			type : 'POST',
			data : { uname : uname, pass : pass },
			dataType : 'JSON',
			success : function(data) {
				if (data == true) {
					alert('Mobile Code Send Successfuly To Your Cell.....');					
				} else {
					alert('code sending error');					
					var span = "<span class='login-error'>Mobile Code Sending Error. Try Again!...</span>";
					$(span).appendTo('.errors_section');
					window.location = base_url + 'index.php/welcome/login';
				}
			}, error : function(xhr, status, error) {
				alert('send error');
				console.log(xhr.responseText);
			}
		});
	}





	return {

		init: function() {
			this.bindUI();
		},

		bindUI: function() {
			var self = this;
			$("#txtUsername").focus()

			$('#login_form input').on('keypress', function(e) {
				if (e.keyCode == 13) {
					$('.btnSignin').trigger('click');
				}
			});

			$('.btnSignin').on('click', function(e) {
				e.preventDefault();
				self.initSignIn();
			});

			$('.btnMobCode').on('click', function(e) {
				 e.preventDefault();
				 self.initSignInCode();
			});
		},

		initSignIn: function() {

			var errors = validateSignIn();

			if (errors.length == 0) {

				var uname = $.trim($('#txtUsername').val());
				var pass = $.trim($('#txtPassowrd').val());
				var mob_code = $.trim($('#txtMobCode').val());
				var fn_dropdown = $.trim($('#fn_dropdown').val());
				var fn_name = $.trim($('#fn_dropdown').find('option:selected').text());
				var fn_sdate = $.trim($('#fn_dropdown').find('option:selected').data('sdate'));
				var fn_edate = $.trim($('#fn_dropdown').find('option:selected').data('edate'));
				// var fn_name = $.trim($('#fn_dropdown').find('option:selected').text());
				fetch(uname, pass, mob_code,fn_dropdown,fn_name,fn_sdate,fn_edate);
			} else {
				alert('enter valid user name or Password or Financial Year');
				var spans = "";
				$.each(errors, function(index, elem) {
					spans += "<span class='login-error'>"+ elem +"</span>";
				});
				// show the errors on the screen
				$(spans).appendTo('.errors_section');
			}
		},

		initSignInCode: function() {

			var errors = validateSignInMobCode();

			if (errors.length == 0) {
				var uname = $.trim($('#txtUsername').val());
				var pass = $.trim($('#txtPassowrd').val());
				fetchCode(uname, pass);
			} else {
				var spans = "";
				$.each(errors, function(index, elem) {
					spans += "<span class='login-error'>"+ elem +"</span>";
				});
				// show the errors on the screen
				$(spans).appendTo('.errors_section');
			}
		},


		resetVoucher: function() {

			$('#txtIdHidden').val('');
			$('#txtName').val('');
			$('input[type="checkbox"]').prop('checked', false);
			getMaxId();
		}
	}

};

var privillagesGroup = new PrivillagesGroup();
privillagesGroup.init();