var PrivillagesGroup = function(){

	var getMaxId = function() {

		$.ajax({
			url: base_url + 'index.php/user/getMaxId',
			type: 'POST',
			dataType: 'JSON',
			success : function(data) {

				$('#txtId').val(data);
				$('#txtMaxIdHidden').val(data);
				$('#txtIdHidden').val(data);
			}, error: function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var getSaveObj = function() {

		var data = {};
		data.uid = $.trim($('#txtIdHidden').val());
		data.uname = $.trim($('#txtUsername').val());
		data.pass = $.trim($('#txtPassowrd').val());
		data.fullname = $.trim($('#txtFullName').val());
		data.email = $.trim($('#txtEmail').val());
		data.mobile = $.trim($('#txtMobileNo').val());
		data.rgid = $.trim($('#role_dropdown').val());
		data.company_id = $.trim($('#company_dropdown').val());
		data.uuid = $.trim($('#uuid').val());

		return data;
	}

	var validateSave = function() {

		var errorFlag = false;
		var name = $.trim($('#txtUsername').val());
		var pass = $.trim($('#txtPassowrd').val());
		var role = $.trim($('#role_dropdown').val());
		// remove the error class first
		$('#txtName').removeClass('inputerror');

		if ( name === '' || name === null ) {
			$('#txtUsername').addClass('inputerror');
			errorFlag = true;
		}
		if ( pass === '' || pass === null ) {
			$('#txtPassowrd').addClass('inputerror');
			errorFlag = true;
		}
		if ( role === '' || role === null ) {
			$('#role_dropdown').addClass('inputerror');
			errorFlag = true;
		}

		return errorFlag;
	}

	var save = function(obj) {
		$.ajax({
			url: base_url + 'index.php/user/save',
			type: 'POST',
			data: {'user': obj},
			dataType: 'JSON',
			success: function(data) {

				if (data.error === 'true') {
					alert('An internal error occured while saving voucher. Please try again.');
				} else {
					alert('User saved successfully.');
					general.reloadWindow();
				}
			}, error: function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var fetch = function(uid) {

		$.ajax({
			url : base_url + 'index.php/user/fetchUser',
			type : 'POST',
			data : { 'uid' : uid },
			dataType : 'JSON',
			success : function(data) {

				if (data === 'false') {
					alert('No data found');
				} else {
					$('.btnSave').attr('disabled', false);
					populateData(data);
				}
			}, error : function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var populateData = function(data) {

		$('#voucher_type_hidden').val('edit');		
		$('#txtIdHidden').val(data.uid);
		$('#txtUsername').val(data.uname);
		$('#txtPassowrd').val(data.pass);
		$('#txtFullName').val(data.fullname);
		$('#txtEmail').val(data.email);
		$('#txtMobileNo').val(data.mobile);
		$('#role_dropdown').val(data.rgid);
		$('#company_dropdown').val(data.company_id);
		$('#user_dropdown').val(data.uuid);
	}

	var openDate = function(date, uid) {

		$.ajax({
			url: base_url + 'index.php/user/openDate',
			type: 'POST',
			data : {'uid': uid, 'date_cl' : date},
			dataType: 'JSON',
			success : function(data) {
				if(data['error'] == 'true'){
					alert('Date is already Open!');
				} else {
					alert('Date is Open!');
				}

			}, error: function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	var closeDate = function(dataObj) {

		$.ajax({
			url: base_url + 'index.php/user/closeDate',
			type: 'POST',
			data : dataObj,
			dataType: 'JSON',
			success : function(data) {
				console.log(data);
				if(data['error'] == 'true'){
					alert('Date is already Closed!');
				} else {
					alert('Date is Closed!');
					general.reloadWindow();

				}

			}, error: function(xhr, status, error) {
				console.log(xhr.responseText);
			}
		});
	}

	return {

		init: function() {
			this.bindUI();
		},

		bindUI: function() {

			var self = this;
			
			$('#voucher_type_hidden').val('new');		
			
			$('.btnSave').on('click', function(e) {
				e.preventDefault();
				var dataObj = {
					'date_cl' : $('#current_date').val(),
					'uid' : $('#uuid').val(),
					'remarks' : ($.trim($('#txtRemarks').val()) == '') ? 'Closed' : $('#txtRemarks').val(),
				};
				if($.trim(dataObj['date_cl']) === '') {
					alert('Date is Empty');
				}else{
					closeDate(dataObj);
				}
				//self.initSave();
			});
			$('.btnOpen').on('click', function(e){
				e.preventDefault();
				var cdate = $('#current_date').val();
				var uid = $('#uuid').val();
				if($.trim(cdate) === '') {
					alert('Date is Empty');
				}else{
					openDate(cdate, uid);
					general.reloadWindow();
				}
				
				
			});

			$('#purchase_table').on('click', '.btnRowEdit', function(e){
				// e.preventDefault();
				var date = $(this).closest('tr').find('td.date_cl').text();
				var remarks = $(this).closest('tr').find('td.remarks').text();
				$('#current_date').val(date);
				$('#txtRemarks').val(remarks);
				$('a[id="tabDate"]').trigger('click');
			});

			$('#txtId').on('keypress', function(e) {

				if (e.keyCode == 13) {
					e.preventDefault();
					var uid = $('#txtId').val();
					fetch(uid);
				}
			});
			$('#txtId').on('change', function(e) {
					e.preventDefault();
					var uid = $('#txtId').val();
					fetch(uid);
			});

			$('.btnReset').on('click', function(e) {
				e.preventDefault();
				self.resetVoucher();
			});
			privillagesGroup.resetVoucher();
			getMaxId();
		},

		initSave: function() {

			var obj = getSaveObj();
			var error = validateSave();

			if (!error) {
				save(obj);
			} else {
				alert('Correct the errors...');
			}
		},

		resetVoucher: function() {

			$('#txtIdHidden').val('');
			$('#txtUsername').val('');
			$('#txtPassowrd').val('');
			$('#txtFullName').val('');
			$('#txtEmail').val('');
			$('#txtMobileNo').val('');
			$('#role_dropdown').val('');

			getMaxId();
			general.setPrivillages();
		}
	}

};

var privillagesGroup = new PrivillagesGroup();
privillagesGroup.init();