<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('accounts');
		$this->load->model('ledgers');
		$this->load->model('levels');
	}


	public function index() {
		redirect('account/add');
	}

	public function add() {

		unauth_secure();
		$data['modules'] = array('setup/account');
		
		$data['names'] = $this->accounts->getDistinctFields('name');
		$data['countries'] = $this->accounts->getDistinctFields('country'); 
		$data['typess'] = $this->accounts->getDistinctFields('etype');
		$data['cities'] = $this->accounts->getDistinctFields('city');
		$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');

		$data['accounts'] = $this->accounts->fetchAll();
		$data['l3s'] = $this->levels->fetchAllLevel3();

		$this->load->view('template/header');

		$this->load->view('setup/addaccount', $data);

		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function getMaxId() {
		
		$maxId = $this->accounts->getMaxId() + 1;
		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($maxId));
	}

	public function getMaxChequeId() {

		if (isset($_POST)) {

			$etype = $_POST['etype'];
			$company_id = $_POST['company_id'];

			$maxId = $this->accounts->getMaxChequeId($etype,$company_id) + 1;
			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($maxId));
		}

	}

	public function save() {

		if (isset($_POST)) {

			$accountDetail = $_POST['accountDetail'];
			
			if(isset($accountDetail['pid']) && $accountDetail['pid']){

				$result = $this->accounts->checkAccountType($accountDetail['pid'], $accountDetail['level3']);
				if($result){

					$accountDetail['account_id'] = $this->levels->genAccStr($accountDetail['level3']);
				}
			}else{

				$accountDetail['account_id'] = $this->levels->genAccStr($accountDetail['level3']);
			}

			$result = $this->accounts->save( $accountDetail );

			$response = array();
			if ($result === false) {
				$response['error'] = true;
			} else {
				$response['error'] = false;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}
	
	public function fetchAccount() {

		if (isset( $_POST )) {

			$pid = $_POST['pid'];
			$result = $this->accounts->fetchAccount($pid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchmultiaccountdata() {

		if (isset( $_POST )) {

			$pid = $_POST['pid'];
			$result = $this->accounts->fetchmultiaccountdata($pid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}
	public function fetchAccountByName() {

		if (isset( $_POST )) {

			$name = $_POST['name'];
			$result = $this->accounts->fetchAccountByName($name);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchAll(){
		$activee=(isset($_POST['active'])? $_POST['active']:-1);
		$typee=(isset($_POST['typee'])? $_POST['typee']:'ALL');
		$result = $this->accounts->fetchAll($activee, $typee);
		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($result));
	}
	


	public function getAllParties() {

		$etype = '';
		if (isset($_POST) && !empty($_POST)) {
			$etype = $_POST['etype'];
		}

		$result = $this->accounts->getAllParties($etype);
		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($result));	
	}

	public function fetchPartyOpeningBalance()
	{
		if (isset($_POST)) {			
			$to = $_POST['to'];
			$party_id = $_POST['pid'];

			$result = $this->accounts->fetchPartyOpeningBalance( $to, $party_id );
			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($result));
		}
	}

	public function fetchRunningTotal()
	{
		if (isset($_POST)) {
			
			$endDate = $_POST['to'];
			$party_id = $_POST['pid'];

			$result = $this->accounts->fetchRunningTotal($endDate, $party_id);
			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($result));
		}	
	}

	public function fetchTurnOver()
	{
		if (isset($_POST)) {
			
			$fromDate = $_POST['from'];
			$endDate = $_POST['to'];
			$party_id = $_POST['pid'];

			$result = $this->accounts->fetchTurnOver($fromDate, $endDate, $party_id);
			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($result));
		}	
	}

	public function getAccLedgerReport() {

		if (isset( $_POST )) {

			$from = $_POST['from'];
			$to = $_POST['to'];
			$pid = $_POST['pid'];
			$result = $this->ledgers->getAccLedgerReport($from, $to, $pid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}
	public function getAccLedgerReports() {

		if (isset( $_POST )) {

			$from = $_POST['from'];
			$to = $_POST['to'];
			$pid = $_POST['pid'];
			$result = $this->ledgers->getAccLedgerReports($from, $to, $pid);
            //mysqli_next_result($this->db->conn_id);
          	$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function getChartOfAccounts()
	{
		$coas = $this->accounts->getChartOfAccounts();
		$json = json_encode($coas);

		echo $json;
	}

	public function fetchClosingBalance()
	{
		$to = $_POST['to'];

		$result = $this->accounts->fetchClosingBalance( $to );

		$json = json_encode($result);
		echo $json;	
	}

	/////////////////////////////////////////////////////
	// Fetches overall opening balance of the COMPANY //
	/////////////////////////////////////////////////////
	public function fetchOpeningBalance()
	{
		$to = $_POST['to'];

		$result = $this->accounts->fetchOpeningBalance( $to );

		$json = json_encode($result);
		echo $json;
	}

	public function fetchPreviousBalance() {

		if (isset( $_POST )) {

			$vrnoa = $_POST['vrnoa'];
			$etype = $_POST['etype'];
			$party_id = $_POST['party_id'];
			$vrdate = $_POST['vrdate'];
			$result = $this->accounts->fetchPreviousBalance($vrdate, $party_id , $vrnoa , $etype);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

}

/* End of file account.php */
/* Location: ./application/controllers/account.php */