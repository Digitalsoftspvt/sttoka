<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Group extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('groups');
	}

	public function index() {
		redirect('group/add');
	}

	public function add() {
		$data['modules'] = array('setup/addgroup');
		$data['groupss'] = $this->groups->fetchAllGroups();
		$this->load->view('template/header');
		$this->load->view('setup/addgroup', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function getMaxGroupId() {
		$result = $this->groups->getMaxGroupId() + 1;
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function saveGroup() {

		if (isset($_POST)) {

			$group = $_POST['group'];
			$result = $this->groups->saveGroup( $group );

			$response = array();
			if ($result === false) {
				$response['error'] = true;
			} else {
				$response['error'] = false;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchGroup() {

		if (isset( $_POST )) {

			$group = $_POST['group'];
			$result = $this->groups->fetchGroup($group);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchAllGroups() {

		$result = $this->groups->fetchAllGroups();

		$response = array();
		if ( $result === false ) {
			$response = 'false';
		} else {			
			$response = $result;
		}

		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($response));
	}

}

/* End of file group.php */
/* Location: ./application/controllers/group.php */