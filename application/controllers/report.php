<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('max_execution_time',100);
class Report extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('accounts');
		$this->load->model('staffs');
		$this->load->model('items');
		$this->load->model('companies');
		$this->load->model('sales');
		$this->load->model('stocks');
		$this->load->model('payments');
		$this->load->model('purchases');
		$this->load->model('orders');
		$this->load->model('departments');
		$this->load->model('users');
		$this->load->model('levels');
	}


	public function getAllCheques()
	{
		$from = $_POST['from'];
		$to = $_POST['to'];
		$type = $_POST['type'];

		$reportData = $this->accounts->getAllCheques($from, $to, $type);

		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($reportData));			 
	}

	public function dailytransactionreport()
	{
		unauth_secure();
		$data['modules'] = array('reports');
		$data['wrapper_class'] = "dailytransaction";
		$data['page'] = "dailytransactionreport";
		$data['etype'] = "dailytransaction";
		$data['requestname']  = 'index.php/report/fetchdailytransactionreportdata';
		$data['requestprint']  = 'index.php/report/fetchdailytransactionreportdata';
		$data['currdate'] = date("Y/m/d");
		$data['setting_configur'] = $this->accounts->getsetting_configur();
		$data['departments'] = $this->departments->fetchAllDepartments();
		$data['view_modals']  = 'true';
		$data['title']  = 'Daily Transaction Report';
		$this->load->view('template/header', $data);
		$this->load->view('reports/reports', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function fetchdailytransactionreportdata()
	{
		$from = $_POST['from'];
		$to   =  $_POST['to'];
		$what = $_POST['what'];
		$type = $_POST['type'];
		$etype = $_POST['etype'];
		$company_id = $this->session->userdata('company_id'); // $_POST['company_id'];
		$crit = isset($_POST["crit"]) ? $_POST["crit"] : "";

		$company_id = $_POST['company_id'];

		$sreportData[0] = $this->accounts->fetchdailytransactionsales($from, $to);
		$sreportData[1] = $this->accounts->fetchdailytransactionpurchases($from, $to);
		$sreportData[2] = $this->accounts->fetchdailytransactionproduction($from, $to);
		$sreportData[3] = $this->accounts->fetchdailytransactionexpense($from, $to);

		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($sreportData));
	}

	public function MonthlySaleComparison()
    {
		unauth_secure();
        $data['modules'] = array('reports/purchase/msalecompar');
		$data['title'] = 'Sales Comparison';
		$data['departments'] = $this->departments->fetchAllDepartments();
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');	
		$data['itemtype'] = $this->items->fetchByCol('type');
		$this->load->view('template/header');
		$this->load->view('reports/purchase/msalecompar',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);

    }
	public function purchase() {
		unauth_secure();
		$data['modules'] = array('reports/purchase/purchase');
		$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "Purchase";

		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
			$data['items'] = $this->items->fetchAll(1);
			$data['categories'] = $this->items->fetchAllCategories('catagory');
			$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
			$data['brands'] = $this->items->fetchAllBrands();
			$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
			$data['cities'] = $this->accounts->getDistinctFields('city');
			$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
			$data['l1s'] = $this->levels->fetchAllLevel1();
			$data['l2s'] = $this->levels->fetchAllLevel2();
			$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		
		$this->load->view('template/header');
		$this->load->view('reports/purchase/purchase',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);

	}
	public function inward() {
		unauth_secure();
		$data['modules'] = array('reports/purchase/outward_inward');
		$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "Inward Gatepass";

		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
			$data['items'] = $this->items->fetchAll(1);
			$data['categories'] = $this->items->fetchAllCategories('catagory');
			$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
			$data['brands'] = $this->items->fetchAllBrands();
			$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
			$data['cities'] = $this->accounts->getDistinctFields('city');
			$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
			$data['l1s'] = $this->levels->fetchAllLevel1();
			$data['l2s'] = $this->levels->fetchAllLevel2();
			$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		
		$this->load->view('template/header');
		$this->load->view('reports/purchase/outward_inward',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);

	}
	public function outward() {
		unauth_secure();
		$data['modules'] = array('reports/purchase/outward_inward');
		$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "Outward Gatepass";

		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
			$data['items'] = $this->items->fetchAll(1);
			$data['categories'] = $this->items->fetchAllCategories('catagory');
			$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
			$data['brands'] = $this->items->fetchAllBrands();
			$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
			$data['cities'] = $this->accounts->getDistinctFields('city');
			$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
			$data['l1s'] = $this->levels->fetchAllLevel1();
			$data['l2s'] = $this->levels->fetchAllLevel2();
			$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		
		$this->load->view('template/header');
		$this->load->view('reports/purchase/outward_inward',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);

	}

	public function purchaseContract() {

		unauth_secure();
		$data['modules'] = array('reports/purchase/purchasecontract');
		$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "purchasecontract";

		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		$data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
			$data['categories'] = $this->items->fetchAllCategories('catagory');
			$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
			$data['brands'] = $this->items->fetchAllBrands();
			$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
			$data['cities'] = $this->accounts->getDistinctFields('city');
			$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
			$data['l1s'] = $this->levels->fetchAllLevel1();
			$data['l2s'] = $this->levels->fetchAllLevel2();
			$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		
		$this->load->view('template/header');
		$this->load->view('reports/purchase/purchasecontract',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}



	public function purchaseorder() {
		unauth_secure();
		$data['modules'] = array('reports/purchase/purchase');
		$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "Purchase Order";

		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		$data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
			$data['categories'] = $this->items->fetchAllCategories('catagory');
			$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
			$data['brands'] = $this->items->fetchAllBrands();
			$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
			$data['cities'] = $this->accounts->getDistinctFields('city');
			$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
			$data['l1s'] = $this->levels->fetchAllLevel1();
			$data['l2s'] = $this->levels->fetchAllLevel2();
			$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		
		$this->load->view('template/header');
		$this->load->view('reports/purchase/purchase',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);

	}
	public function saleorder() {
		unauth_secure();
		$data['modules'] = array('reports/purchase/purchase');
		$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "Sale Order";

		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		$data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
			$data['categories'] = $this->items->fetchAllCategories('catagory');
			$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
			$data['brands'] = $this->items->fetchAllBrands();
			$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
			$data['cities'] = $this->accounts->getDistinctFields('city');
			$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
			$data['l1s'] = $this->levels->fetchAllLevel1();
			$data['l2s'] = $this->levels->fetchAllLevel2();
			$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		
		$this->load->view('template/header');
		$this->load->view('reports/purchase/purchase',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);

	}
	// 
	public function orderParts() {
		unauth_secure();
		$data['modules'] = array('reports/purchase/orders');
		$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "Order Parts";

		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		$data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
			$data['categories'] = $this->items->fetchAllCategories('catagory');
			$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
			$data['brands'] = $this->items->fetchAllBrands();
			$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
			$data['cities'] = $this->accounts->getDistinctFields('city');
			$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
			$data['l1s'] = $this->levels->fetchAllLevel1();
			$data['l2s'] = $this->levels->fetchAllLevel2();
			$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		
		$this->load->view('template/header');
		$this->load->view('reports/purchase/orders',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);

	}
	public function orderLoading() {
		unauth_secure();
		$data['modules'] = array('reports/purchase/orders');
		$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "Order Loading";

		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		$data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
			$data['categories'] = $this->items->fetchAllCategories('catagory');
			$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
			$data['brands'] = $this->items->fetchAllBrands();
			$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
			$data['cities'] = $this->accounts->getDistinctFields('city');
			$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
			$data['l1s'] = $this->levels->fetchAllLevel1();
			$data['l2s'] = $this->levels->fetchAllLevel2();
			$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		
		$this->load->view('template/header');
		$this->load->view('reports/purchase/orders',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);

	}
	// 


	public function purchaseReturn() {
		// unauth_secure();
		// $data['modules'] = array('reports/purchase/purchasereturn');
		// $this->load->view('template/header');
		// $this->load->view('reports/purchase/purchasereturn');
		// $this->load->view('template/mainnav');
		// $this->load->view('template/footer', $data);
		unauth_secure();
		$data['modules'] = array('reports/purchase/purchase');
		$data['wrapper_class'] = "purchase__report";
		$data['page'] = "purchase_report";
		$data['etype'] = "Purchase Return";

		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		$data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
			$data['categories'] = $this->items->fetchAllCategories('catagory');
			$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
			$data['brands'] = $this->items->fetchAllBrands();
			$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
			$data['cities'] = $this->accounts->getDistinctFields('city');
			$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
			$data['l1s'] = $this->levels->fetchAllLevel1();
			$data['l2s'] = $this->levels->fetchAllLevel2();
			$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		
		$this->load->view('template/header');
		$this->load->view('reports/purchase/purchase',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

    public function sale() {
        unauth_secure();
        $data['modules'] = array('reports/purchase/purchase');
        $data['wrapper_class'] = "purchase_report";
        $data['page'] = "purchase_report";
        $data['etype'] = "Sale";

        $data['currdate'] = date("Y/m/d");
        // Advanced
        $data['parties'] = $this->accounts->fetchAll(1);
        $data['departments'] = $this->departments->fetchAllDepartments();
        $data['items'] = $this->items->fetchAll(1);
        $data['userone'] = $this->users->fetchAll();

        // End Advanced
        // Advanced Item
        $data['brands'] = $this->items->fetchAllBrands();
        $data['categories'] = $this->items->fetchAllCategories('catagory');
        $data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
        $data['uoms'] = $this->items->fetchByCol('uom');
        // End Advanced Item
        // Advanced Account
        $data['cities'] = $this->accounts->getDistinctFields('city');
        $data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
        $data['l1s'] = $this->levels->fetchAllLevel1();
        $data['l2s'] = $this->levels->fetchAllLevel2();
        $data['l3s'] = $this->levels->fetchAllLevel3();
        // End Advanced Account
        $this->load->view('template/header');
        $this->load->view('reports/purchase/purchase',$data);
        $this->load->view('template/mainnav');
        $this->load->view('template/footer', $data);
    }
    public function inwardgatepass() {
        unauth_secure();
        $data['modules'] = array('reports/other/gatepass');
        $data['wrapper_class'] = "inwardgatepass_report";
        $data['page'] = "inwardgatepass_report";
        $data['etype'] = "IGP";

        $data['currdate'] = date("Y/m/d");
        // Advanced
        $data['parties'] = $this->accounts->fetchAll(1);
        $data['departments'] = $this->departments->fetchAllDepartments();
        $data['items'] = $this->items->fetchAll(1);
        $data['userone'] = $this->users->fetchAll();
        //End Advance
        $this->load->view('template/header');
        $this->load->view('reports/other/gatepass',$data);
        $this->load->view('template/mainnav');
        $this->load->view('template/footer', $data);
    }
    public function outwardgatepass() {
        unauth_secure();
        $data['modules'] = array('reports/other/gatepass');
        $data['wrapper_class'] = "outgatepass_report";
        $data['page'] = "outgatepass_report";
        $data['etype'] = "OGP";

        $data['currdate'] = date("Y/m/d");
        // Advanced
        $data['parties'] = $this->accounts->fetchAll(1);
        $data['departments'] = $this->departments->fetchAllDepartments();
        $data['items'] = $this->items->fetchAll(1);
        $data['userone'] = $this->users->fetchAll();
        //End Advance
        $this->load->view('template/header');
        $this->load->view('reports/other/gatepass',$data);
        $this->load->view('template/mainnav');
        $this->load->view('template/footer', $data);
    }
    public function fetchGPReportData()
    {
        $what = $_POST['what'];
        $startDate = $_POST['from'];
        $endDate = $_POST['to'];
        $type = $_POST['type'];
        $etype = $_POST['etype'];
        $company_id = $_POST['company_id'];
        $field = $_POST['field'];
        $crit = $_POST['crit'];
        $orderBy = $_POST['orderBy'];
        $groupBy = $_POST['groupBy'];
        $name = $_POST['name'];
        $sreportData = $this->stocks->fetchPurchaseReportData($startDate, $endDate, $what, $type, $company_id, $etype,$field,$crit,$orderBy,$groupBy,$name);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($sreportData));
	}
	
	public function fetchmonthlysalecomparison()
    {
        $dept_id = $_POST['dept_id'];
        $cat_id = $_POST['cat_id'];
        $subcat_id = $_POST['subcat_id'];
        $til = $_POST['till'];
		$type = $_POST['type'];
		$itemtype = $_POST['itemtype'];
        $sreportData = $this->purchases->fetchmonthlysalecomparison($til, $dept_id,$type, $cat_id, $subcat_id,$itemtype);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($sreportData));
    } 
	public function counter_sale() 
	{
		unauth_secure();
		$data['modules'] = array('reports/purchase/purchase');
		$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "counter_sale";

		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		$data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();

		// End Advanced
		// Advanced Item
			$data['brands'] = $this->items->fetchAllBrands();
			$data['categories'] = $this->items->fetchAllCategories('catagory');
			$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
			$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
			$data['cities'] = $this->accounts->getDistinctFields('city');
			$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
			$data['l1s'] = $this->levels->fetchAllLevel1();
			$data['l2s'] = $this->levels->fetchAllLevel2();
			$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		$this->load->view('template/header');
		$this->load->view('reports/purchase/counter_sale',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function saleReturn() {
		unauth_secure();
		$data['modules'] = array('reports/purchase/purchase');
		$data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "Sale Return";

		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		$data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
			$data['categories'] = $this->items->fetchAllCategories('catagory');
			$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
			$data['brands'] = $this->items->fetchAllBrands();
			$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
			$data['cities'] = $this->accounts->getDistinctFields('city');
			$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
			$data['l1s'] = $this->levels->fetchAllLevel1();
			$data['l2s'] = $this->levels->fetchAllLevel2();
			$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		
		$this->load->view('template/header');
		$this->load->view('reports/purchase/purchase', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function saleOrderWise() {
		unauth_secure();
		$data['modules'] = array('reports/sale/saleorderwise');
		$this->load->view('template/header');
		$this->load->view('reports/sale/saleorderwise');
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function waiterCashPending() {
		unauth_secure();
		$data['modules'] = array('reports/sale/waitercashpending');
		$this->load->view('template/header');
		$this->load->view('reports/sale/waitercashpending');
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function fetchSaleReportData()
	{
		$what = $_POST['what'];
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];
		$type = $_POST['type'];

		$sreportData = $this->sales->fetchSaleReportData($startDate, $endDate, $what, $type);
		$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($sreportData));
	}

	public function fetchStockReport()
	{
		$what = $_POST['what'];
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];
		$company_id = $_POST['company_id'];

		$sreportData = $this->stocks->fetchStockReport($startDate, $endDate, $what, $company_id);
		$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($sreportData));
	}

	public function fetchClosingStockReport()
	{
		$what = $_POST['what'];
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];
		$company_id = $_POST['company_id'];
		$crit = $_POST['crit'];
		$godownIds = $_POST['godown_ids'];

		$sreportData = $this->stocks->fetchClosingStockReport($startDate, $endDate, $what, $company_id , $crit, $godownIds);
		$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($sreportData));
	}

	public function fetchDailyVoucherReport()
	{
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];

		$sreportData = $this->stocks->fetchDailyVoucherReport($startDate, $endDate);
		$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($sreportData));
	}

	public function fetchSaleProductionData()
	{
		$what = $_POST['what'];
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];
		$etype = $_POST['etype'];
		$type = $_POST['type'];
		$crit = $_POST['crit'];

		$sreportData = $this->sales->fetchSaleProductionData($startDate, $endDate, $what, $type, $crit,$etype);
		$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($sreportData));
	}

	public function fetchMaterialReturnData()
	{
		$what = $_POST['what'];
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];
		$type = $_POST['type'];

		$sreportData = $this->purchases->fetchMaterialReturnData($startDate, $endDate, $what, $type);
		$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($sreportData));
	}

	public function fetchConsumptionData()
	{
		$what = $_POST['what'];
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];
		$type = $_POST['type'];

		$sreportData = $this->purchases->fetchConsumptionData($startDate, $endDate, $what, $type);
		$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($sreportData));
	}

	public function fetchStockNavigationData()
	{
		$what = $_POST['what'];
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];
		$type = $_POST['type'];
		$crit = $_POST['crit'];

		$sreportData = $this->purchases->fetchStockNavigationData($startDate, $endDate, $what, $type, $crit);
		$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($sreportData));
	}

	public function fetchSaleReturnReportData()
	{
		$what = $_POST['what'];
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];
		$type = $_POST['type'];

		$sreportData = $this->sales->fetchSaleReturnReportData($startDate, $endDate, $what, $type);
		$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($sreportData));
	}

	public function saleReportOrderWise()
	{
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];
		$type = $_POST['type'];

		$sreportData = $this->sales->saleReportOrderWise($startDate, $endDate, $type);
		$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($sreportData));
	}

	public function waiterCashPendingReport()
	{
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];

		$sreportData = $this->sales->waiterCashPendingReport($startDate, $endDate);
		$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($sreportData));
	}

	public function itemLedger() {

		$data['modules'] = array('reports/inventory/itemlegderreport');
		$data['items'] = $this->items->fetchAll();
		$data['departments'] = $this->departments->fetchAllDepartments();

		$this->load->view('template/header');
		$this->load->view('reports/inventory/itemlegderreport', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function production() {

		$data['modules'] = array('reports/inventory/production');
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
		$data['items'] = $this->items->fetchAll(1);
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
		$data['cities'] = $this->accounts->getDistinctFields('city');
		$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
		$data['l1s'] = $this->levels->fetchAllLevel1();
		$data['l2s'] = $this->levels->fetchAllLevel2();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		$this->load->view('template/header');
		$this->load->view('reports/inventory/production', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function headproduction() {

		$data['modules'] = array('reports/inventory/headproduction');
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		$data['staffs'] = $this->staffs->fetchAll();
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
		$data['items'] = $this->items->fetchAll(1);
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
		$data['cities'] = $this->accounts->getDistinctFields('city');
		$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
		$data['l1s'] = $this->levels->fetchAllLevel1();
		$data['l2s'] = $this->levels->fetchAllLevel2();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		$this->load->view('template/header');
		$this->load->view('reports/inventory/headproduction', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function headmoulding() {

		$data['modules'] = array('reports/inventory/headmoulding');
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
		$data['items'] = $this->items->fetchAll(1);
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
		$data['cities'] = $this->accounts->getDistinctFields('city');
		$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
		$data['l1s'] = $this->levels->fetchAllLevel1();
		$data['l2s'] = $this->levels->fetchAllLevel2();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		$this->load->view('template/header');
		$this->load->view('reports/inventory/headmoulding', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function moulding() {

		$data['modules'] = array('reports/inventory/moulding');
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
		$data['items'] = $this->items->fetchAll(1);
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
		$data['cities'] = $this->accounts->getDistinctFields('city');
		$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
		$data['l1s'] = $this->levels->fetchAllLevel1();
		$data['l2s'] = $this->levels->fetchAllLevel2();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account
		$this->load->view('template/header');
		$this->load->view('reports/inventory/moulding', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function stockNavigation() {

		$data['modules'] = array('reports/inventory/stocknavigation');

		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
		$data['items'] = $this->items->fetchAll(1);
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
		$data['cities'] = $this->accounts->getDistinctFields('city');
		$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
		$data['l1s'] = $this->levels->fetchAllLevel1();
		$data['l2s'] = $this->levels->fetchAllLevel2();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		// End Advanced Account

		$this->load->view('template/header');
		$this->load->view('reports/inventory/stocknavigation', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function dailyVoucher() {
		unauth_secure();
		$data['modules'] = array('reports/inventory/dailyvoucher');
		$this->load->view('template/header');
		$this->load->view('reports/inventory/dailyvoucher');
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function stock() {
		unauth_secure();
		$data['modules'] = array('reports/inventory/stock');
		$this->load->view('template/header');
		$this->load->view('reports/inventory/stock');
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function closingStock() {
		unauth_secure();
		$data['modules'] = array('reports/inventory/closingStock');
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		$data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
			$data['categories'] = $this->items->fetchAllCategories('catagory');
			$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
			$data['brands'] = $this->items->fetchAllBrands();
			$data['uoms'] = $this->items->fetchByCol('uom');
		$this->load->view('template/header');
		$this->load->view('reports/inventory/closingStock',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function materialReturn() {
		unauth_secure();
		$data['modules'] = array('reports/inventory/materialreturn');
		$this->load->view('template/header');
		$this->load->view('reports/inventory/materialreturn');
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function consumption() {
		unauth_secure();
		$data['modules'] = array('reports/inventory/consumption');
		$this->load->view('template/header');
		$this->load->view('reports/inventory/consumption');
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	
	public function accountLedger() {
		unauth_secure();
 		$data['modules'] = array('reports/accounts/accountledger');
 		$data['parties'] = $this->accounts->fetchAll(1,'ledger');
 		
		$this->load->view('template/header');

		$this->load->view('reports/accounts/accountledger', $data);

		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
 	}

 	public function chartOfAccounts()
	{
		unauth_secure();
		$data['modules'] = array('reports/accounts/chartofaccounts');
		$data['cities'] = $this->accounts->getDistinctFields('city');
		$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
		$data['l1s'] = $this->levels->fetchAllLevel1();
		$data['l2s'] = $this->levels->fetchAllLevel2();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		$this->load->view('template/header');
		$this->load->view('reports/accounts/chartofaccounts',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function getChartOfAccounts()
	{
		unauth_secure();

		if (isset($_POST)) {

			$status = $_POST['status'];
			$what = $_POST['what'];
			$crit = $_POST['crit'];
	
			$coas = $this->accounts->getChartOfAccounts($status , $what , $crit);
			$json = json_encode($coas);

			echo $json;
		}
	}

	public function cheques()
	{
		unauth_secure();
		$data['modules'] = array('reports/accounts/chequesReport');
		$this->load->view('template/header');
		$this->load->view('reports/accounts/cheques');
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
		public function staffStatus() {

	 		unauth_secure();
	 		$data['modules'] = array('reports/staff/staffstatus');
	 		$data['departments'] = $this->departments->fetchAllDepartments();

			$this->load->view('template/header');

			$this->load->view('reports/staff/staffstatus', $data);

			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
	 	}

	 	public function penalty() {

	 		unauth_secure();
	 		$data['modules'] = array('reports/accounts/penalty');
	 		$data['staffs'] = $this->staffs->fetchAll();
	 		$data['departments'] = $this->departments->fetchAllDepartments();

			$this->load->view('template/header');

			$this->load->view('reports/accounts/penalty', $data);

			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
	 	}
	 	public function loan() {

	 	 		unauth_secure();
	 	 		$data['modules'] = array('reports/accounts/loan');
	 	 		$data['staffs'] = $this->staffs->fetchAll();
	 	 		$data['departments'] = $this->departments->fetchAllDepartments();

	 			$this->load->view('template/header');

	 			$this->load->view('reports/accounts/loan', $data);

	 			$this->load->view('template/mainnav');
	 			$this->load->view('template/footer', $data);
	 	 	}
	 	public function advance() {

	 		unauth_secure();
	 		$data['modules'] = array('reports/accounts/advance');
	 		$data['staffs'] = $this->staffs->fetchAll();

			$this->load->view('template/header');

			$this->load->view('reports/accounts/advance', $data);

			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
	 	}

	 	public function incentive() {

	 		unauth_secure();
	 		$data['modules'] = array('reports/accounts/incentive');
	 		$data['staffs'] = $this->staffs->fetchAll();

			$this->load->view('template/header');

			$this->load->view('reports/accounts/incentive', $data);

			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
	 	}

	 	public function overtime()
	    {
	        $data['modules'] = array('reports/accounts/overtime');

	        $data['staffs'] = $this->staffs->fetchAll();
			if($data['staffs'] == false)
			{
			    $data['staffs'] = array();
			}
			$data['departments'] = $this->departments->fetchAllDepartments();
			if($data['departments'] == false)
			{
			    $data['departments'] = array();
			}

	        $this->load->view('template/header');

			$this->load->view('reports/accounts/overtime', $data);

			$this->load->view('template/mainnav');
			$this->load->view('template/footer', $data);
	    }

 	 	public function eobiContribution() {

 	 		unauth_secure();
 	 		$data['modules'] = array('reports/accounts/eobi');
 	 		$data['departments'] = $this->departments->fetchAllDepartments();
 	 		$data['staffs'] = $this->staffs->fetchAll();

 			$this->load->view('template/header');

 			$this->load->view('reports/accounts/eobi', $data);

 			$this->load->view('template/mainnav');
 			$this->load->view('template/footer', $data);
 	 	}

 	 	public function socialSecurityContribution() {

 	 		unauth_secure();
 	 		$data['modules'] = array('reports/accounts/socialsec');
 	 		$data['staffs'] = $this->staffs->fetchAll();
 	 		$data['departments'] = $this->departments->fetchAllDepartments();

 			$this->load->view('template/header');

 			$this->load->view('reports/accounts/socialsec', $data);

 			$this->load->view('template/mainnav');
 			$this->load->view('template/footer', $data);
 	 	}

	public function getChequeReportData()
	{
		$startDate = $_POST['startDate'];
		$endDate = $_POST['endDate'];
		$type = $_POST['type'];
		$company_id= $_POST['company_id'];

		$reportData = $this->accounts->getChequeReportData($startDate, $endDate, $type,$company_id);

		$json = json_encode($reportData);

		echo $json;
	}

	public function getProfitLossReportData()
	{
		// what may be item, party, voucher
        // filterCrit is whose data is required. It may be an itemName, partyName or nothing at all.

		$what = $_POST['what'];
		$filterCrit = $_POST['filterCrit'];
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];

		$sreportData = $this->sales->fetchProfitLossReportData($what, $startDate, $endDate, $filterCrit);
		$json = json_encode($sreportData);

		echo $json;
	}

	public function profitloss()
	{
		$data['modules'] = array('reports/accounts/profitloss_report');
		$this->load->view('template/header');
		$this->load->view('reports/accounts/profitloss');
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function accounts()
	{
		$data['modules'] = array('reports/accounts/accounts_reports');

		$this->load->view('template/header');
		$this->load->view('reports/accounts/accounts');
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function fetchPayRecvReportData()
	{
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];
		$etype = $_POST['etype'];
		$company_id = $_POST['company_id'];

		$payrecvreportData = $this->accounts->fetchPayRecvReportData($startDate, $endDate, $etype, $company_id);
		$json = json_encode($payrecvreportData);

		echo $json;
	}

	public function fetchDayBoookReportData()
	{
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];
		$what = $_POST['what'];
		$etype = $_POST['etype'];
		$company_id = $_POST['company_id'];

		$dbreportData = $this->accounts->fetchDayBookReportData($startDate, $endDate, $what, $etype,$company_id);
		$json = json_encode($dbreportData);

		echo $json;
	}

	public function fetchExpenseReportData()
	{
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];
		$what = $_POST['what'];
		$etype = $_POST['etype'];
		$company_id = $_POST['company_id'];
		$ereportData = $this->accounts->fetchExpenseReportData($startDate, $endDate, $what, $etype,$company_id);
		$json = json_encode($ereportData);

		echo $json;
	}

	public function fetchJVReportData()
	{
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];
		$what = $_POST['what'];
		$etype = $_POST['etype'];
		$company_id = $_POST['company_id'];

		$jvreportData = $this->payments->fetchJVReportData($startDate, $endDate, $what, $etype,$company_id);
		$json = json_encode($jvreportData);

		echo $json;
	}

	public function fetchCashReportData()
	{
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];
		$what = $_POST['what'];
		$etype = $_POST['etype'];
		$company_id = $_POST['company_id'];

		$creportData = $this->payments->fetchCashReportData($startDate, $endDate, $what, $etype,$company_id);
		$json = json_encode($creportData);

		echo $json;
	}
	// Payables
	public function payables()
	{
		unauth_secure();

		$data['wrapper_class'] = "accounts_reports";
		$data['page'] = "accounts_reports";

		$data['currdate'] = date("Y/m/d");
		$data['companies'] = $this->companies->getAll();

		$data['company_id'] = $this->session->userdata('company_id');

		$data['payables'] = $this->accounts->fetchInvoiceAgingData( $data['currdate'], $data['currdate'], 'payables', null, $data['company_id']);
		$data['receiveables'] = $this->accounts->fetchInvoiceAgingData( $data['currdate'], $data['currdate'], 'receiveables', null, $data['company_id']);

		$this->load->view('template/header', $data);
		$this->load->view('template/mainnav', $data);
		$this->load->view('reports/payables', $data);
		$this->load->view('template/footer');
	}

	public function fetchPayRecvCount()
	{
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];
		$company_id = $_POST['company_id'];
		$etype = $_POST['etype'];

		$payrecvreportData = $this->accounts->fetchPayRecvCount($startDate, $endDate, $company_id, $etype);
		$json = json_encode($payrecvreportData);

		echo $json;
	}

	public function staffAttendanceSheet()
    {

        $data['modules'] = array('reports/staff/staffattendancesheet');
        $data['departments'] = $this->departments->fetchAllDepartments();

        $this->load->view('template/header');
        $this->load->view('template/mainnav');
        $this->load->view('reports/staff/staffattendancesheet',$data);
        $this->load->view('template/footer',$data);
    }

    public function staffAtndncStatusWise()
    {
        $data['modules'] = array('reports/staff/staffattendancestatuswise');
        $data['departments'] = $this->departments->fetchAllDepartments();
        $data['staffs'] = $this->staffs->fetchAll();
        
        $this->load->view('template/header');
        $this->load->view('template/mainnav');
        $this->load->view('reports/staff/staffattendancestatuswise',$data);
        $this->load->view('template/footer',$data);
    }

     public function staffMonthlyAttendanceReport()
    {
    	$data['modules'] = array('reports/staff/staffmonthlyatndncreport');
        $data['departments'] = $this->departments->fetchAllDepartments();
        $data['staffs'] = $this->staffs->fetchAll();
        
        $this->load->view('template/header');
        $this->load->view('template/mainnav');
        $this->load->view('reports/staff/staffmonthlyatndncreport',$data);
        $this->load->view('template/footer',$data);
    	
        // $data['modules'] = array('attendance/reports/staffmonthlyatndncreport');

        // $data['header'] = View::make('layout.header');
        // $data['content'] = View::make('attendance.reports.staffmonthlyatndncreport');
        // $data['mainnav'] = View::make('layout.mainnav');
        // $data['footer'] = View::make('layout.footer');

        // return View::make('layout.default', $data);
    }

     public function itemConversion()
    {
        $data['modules'] = array('reports/inventory/itemconversion');


        $data['wrapper_class'] = "purchase_report";
		$data['page'] = "purchase_report";
		$data['etype'] = "item_conversion";

		$data['currdate'] = date("Y/m/d");
		// Advanced
		$data['parties'] = $this->accounts->fetchAll(1);
		$data['departments'] = $this->departments->fetchAllDepartments();
		$data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();

		// ENd Advanced
		// Advanced Item
			$data['categories'] = $this->items->fetchAllCategories('catagory');
			$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
			$data['brands'] = $this->items->fetchAllBrands();
			$data['uoms'] = $this->items->fetchByCol('uom');
		// End Advanced Item
		// Advanced Account
			$data['cities'] = $this->accounts->getDistinctFields('city');
			$data['cityareas'] = $this->accounts->getDistinctFields('cityarea');
			$data['l1s'] = $this->levels->fetchAllLevel1();
			$data['l2s'] = $this->levels->fetchAllLevel2();
			$data['l3s'] = $this->levels->fetchAllLevel3();

        $this->load->view('template/header');
        $this->load->view('template/mainnav');
        $this->load->view('reports/inventory/itemconversion',$data);
        $this->load->view('template/footer',$data);
    }

    public function receipecosting()
    {
        $data['modules'] = array('reports/other/receipecosting');
        $data['etype'] = 'Receipe Costing';

        $data['items'] = $this->items->fetchAll(1);
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['uoms'] = $this->items->fetchByCol('uom');
		
        $this->load->view('template/header');
        $this->load->view('template/mainnav');
        $this->load->view('reports/other/receipecosting',$data);
        $this->load->view('template/footer',$data);
    }


}

/* End of file report.php */
/* Location: ./application/controllers/report.php */