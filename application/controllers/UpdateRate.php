<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UpdateRate extends CI_Controller {

	public function __construct() {
        parent::__construct();        
        $this->load->model('accounts');
        $this->load->model('items');
        $this->load->model('accounts');
        $this->load->model('departments');
        $this->load->model('salesmen');
        $this->load->model('extradays');
        $this->load->model('phases');
        $this->load->model('ItemphaseRecepies');
    }

    public function index() {
        unauth_secure();
        $data['modules'] = array('setup/addUpdateRate');
        $data['parties'] = $this->accounts->fetchAll();
        $data['items'] = $this->items->fetchAll();

        $this->load->view('template/header');
        $this->load->view('setup/addUpdateRate', $data);
        $this->load->view('template/mainnav');
        $this->load->view('template/footer', $data);
    }

    public function getMaxVrno() {
        $company_id = $_POST['company_id'];
        $result = $this->ItemphaseRecepies->getMaxVrno('itemphaserecipe',$company_id) + 1;
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function getMaxVrnoa() {
        $company_id = $_POST['company_id'];
        $result = $this->ItemphaseRecepies->getMaxVrnoa('itemphaserecipe',$company_id) + 1;
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function save() {

        if (isset($_POST)) {

            $partyRate = $_POST['party_rate'];
            $result = $this->accounts->savePartyRate($partyRate);

            $response = array();
            if ( $result === false ) {
                $response['error'] = 'true';
            } else {
                $response = $result;
            }

            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }

    public function fetch() {

        if (isset( $_POST )) {
            $vrnoa = $_POST['vrnoa'];
            $company_id = $_POST['company_id'];
            $result = $this->ItemphaseRecepies->fetch($vrnoa, 'itemphaserecipe',$company_id);
            $response = "";
            if ( $result === false ) {
                $response = 'false';
            } else {
                $response = $result;
            }
            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }

    public function fetchPartyWiseRate(){

        $partyId = $_POST['party_id'];

        $result = $this->accounts->fetchPartyWiseRate($partyId);
        $response = "";
        if ( $result === false ) {
            $response = 'false';
        } else {
            $response = $result;
        }
        $this->output
             ->set_content_type('application/json')
             ->set_output(json_encode($response));
    }

    public function fetchPartyPurRate(){

        $partyId = $_POST['party_id'];
        $itemId = $_POST['item_id'];

        $result = $this->accounts->fetchPartyPurRate($itemId, $partyId);
        $response = "";
        if ( $result === false ) {
            $response = 'false';
        } else {
            $response = $result;
        }
        $this->output
             ->set_content_type('application/json')
             ->set_output(json_encode($response));
    }

    public function delete() {

        if (isset( $_POST )) {

            $vrnoa = $_POST['vrnoa'];
            $company_id = $_POST['company_id'];
            $result = $this->ItemphaseRecepies->delete($vrnoa, 'itemphaserecipe',$company_id);

            $response = "";
            if ( $result === false ) {
                $response = 'false';
            } else {
                $response = $result;
            }

            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }
}

/* End of file production.php */
/* Location: ./application/controllers/production.php */