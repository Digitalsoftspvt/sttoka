<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jv extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('accounts');
		$this->load->model('ledgers');
		$this->load->model('users');
		$this->load->model('levels');
		$this->load->model('datecloses');
	}

	public function index() {
		unauth_secure();
		$data['modules'] = array('accounts/addjv');
		$data['accounts'] = $this->accounts->fetchAll();
		$data['userone'] = $this->users->fetchAll();
		$data['l3s'] = $this->levels->fetchAllLevel3();

		$this->load->view('template/header');

		$this->load->view('accounts/addjv', $data);

		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function add() {
		
	}

	public function getMaxId() {
		if (isset($_POST)) {
			$company_id = $_POST['company_id'];
			$maxId = $this->ledgers->getMaxId('jv',$company_id) + 1;
			$this->output->set_content_type('application/json')->set_output(json_encode($maxId));
		}
		
	}

	public function save() {

		if (isset($_POST)) {

			$saveObj = $_POST['saveObj'];
			$dcno = $_POST['dcno'];
			$etype = $_POST['etype'];
			$voucher_type_hidden = $_POST['voucher_type_hidden'];

			 ///////////////////// Validation Close Date String
	        $response = array();
	        
	        $chk_date = $_POST['vrdate'];
	        $vrdate = "2016-01-01";
	        $vrdate = $_POST['chk_date'];

	        $DateCloseStatus=false;
	        if($chk_date!=$vrdate &&  $voucher_type_hidden=='edit'){
	            $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
	        }
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
	        
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        ///////////////////// Validation End

			if($voucher_type_hidden == 'new'){

				$dcno = $this->ledgers->getMaxId('jv',$saveObj[0]['company_id']) + 1;
			}
			$result = $this->ledgers->save($saveObj, $dcno, $etype,$voucher_type_hidden);
			$response = array();
			if ( $result === false ) {
				$response['error'] = 'true';
			} else {
				$response['error'] = 'false';
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetch() {

		if (isset($_POST)) {

			$dcno = $_POST['dcno'];
			$company_id=$_POST['company_id'];
			$result = $this->ledgers->fetch($dcno, 'jv',$company_id);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function deleteVoucher() {

		if (isset($_POST)) {

			$dcno = $_POST['dcno'];
			$company_id = $_POST['company_id'];

			  ///////////////////// Validation Close Date String
	        $response = array();
	        
	        $chk_date = $_POST['chk_date'];
	        $vrdate = "2016-01-01";
	        $vrdate = $_POST['vrdate'];

	        $DateCloseStatus=false;
	        if($chk_date!=$vrdate){
	            $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
	        }
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
	        
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        ///////////////////// Validation End

			$result = $this->ledgers->deleteVoucher($dcno, 'jv',$company_id);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = 'true';
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchVoucherRange() {

		if (isset($_POST)) {

			$from = $_POST['from'];
			$to = $_POST['to'];

			$result = $this->ledgers->fetchVoucherRange($from, $to, 'jv');
			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($result));
		}
	}
}

/* End of file jv.php */
/* Location: ./application/controllers/jv.php */