<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Requisition extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('accounts');
		$this->load->model('salesmen');
		$this->load->model('transporters');
		$this->load->model('items');
		$this->load->model('departments');
		$this->load->model('orders');
		$this->load->model('datecloses');
	}

	public function index() {
		$data['modules'] = array('purchase/requisition');
		$data['notedbys'] = $this->orders->fetchAllNotedBy();
		$data['items'] = $this->items->fetchItemsByStock();
		$data['departments'] = $this->departments->fetchAllDepartments();

		$this->load->view('template/header');
		$this->load->view('purchase/requisition', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

    public function getMaxVrno()
    {
        if (isset($_POST))
        {
            $company_id = $_POST['company_id'];
            $data = $this->orders->getMaxVrno('requisition',$company_id) + 1;
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

	public function getMaxVrnoa()
    {
        if (isset($_POST))
        {
            $company_id = $_POST['company_id'];
            $result = $this->orders->getMaxVrnoa('requisition', $company_id) + 1;
            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
	}

	public function save() {

		if (isset($_POST)) {

			$ordermain = $_POST['ordermain'];
			$orderdetail = $_POST['orderdetail'];
			$vrnoa = $_POST['vrnoa'];
			$voucher_type_hidden = $_POST['voucher_type_hidden'];
			if ($voucher_type_hidden=='new'){
				
				$vrnoa = $this->orders->getMaxVrnoa($ordermain['etype'] , $ordermain['company_id']) + 1;
				$ordermain['vrnoa'] = $vrnoa;
			}

			  ///////////////////// Validation Close Date String
	        $response = array();
	        
	        $chk_date = $_POST['vrdate'];
	        $vrdate = "2016-01-01";
	        $vrdate = $ordermain['vrdate'];

	        $DateCloseStatus=false;
	        if($chk_date!=$vrdate &&  $voucher_type_hidden=='edit'){
	            $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
	        }
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
	        
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        ///////////////////// Validation End

			$result = $this->orders->save($ordermain, $orderdetail, $vrnoa, 'requisition');


			$response = array();
			if ( $result === false ) {
				$response['error'] = 'true';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetch() {

		if (isset( $_POST )) {

			$vrnoa = $_POST['vrnoa'];
            $company_id = $_POST['company_id'];
            $result = $this->orders->fetch($vrnoa, 'requisition', $company_id);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function delete() {

		if (isset( $_POST )) {

			$vrnoa = $_POST['vrnoa'];
            $company_id = $_POST['company_id'];

              ///////////////////// Validation Close Date String
	        $response = array();
	        
	        $chk_date = $_POST['chk_date'];
	        $vrdate = "2016-01-01";
	        $vrdate = $_POST['vrdate'];

	        $DateCloseStatus=false;
	        if($chk_date!=$vrdate){
	            $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
	        }
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
	        
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        ///////////////////// Validation End

			$result = $this->orders->delete($vrnoa, 'requisition', $company_id);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}
}

/* End of file requisition.php */
/* Location: ./application/controllers/requisition.php */