<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inwardgatepass extends CI_Controller {

    public function __construct() {
        parent::__construct();        
        $this->load->model('accounts');
        $this->load->model('items');
        $this->load->model('departments');
        $this->load->model('orders');
        $this->load->model('datecloses');
    }

    public function index() {
        ini_set('max_execution_time',100);
        $data['modules'] = array('purchase/inwardgatepass');
        $data['parties'] = $this->accounts->fetchAll();
        $data['notedbys'] = $this->orders->fetchAllNotedBy();
        $data['approveds'] = $this->orders->fetchAllApprovedBy();
        $data['refersby'] = $this->orders->fetchAllreferBy();
        $data['payments'] = $this->orders->fetchAllPayment();
        $data['departments'] = $this->departments->fetchAllDepartments();
        $data['items'] = $this->items->fetchAll();

        $this->load->view('template/header');
        $this->load->view('purchase/inwardgatepass', $data);
        $this->load->view('template/mainnav');
        $this->load->view('template/footer', $data);
    }
    public function outward() {
        ini_set('max_execution_time',100);
        $data['modules'] = array('sale/outwardgatepass');
        $data['parties'] = $this->accounts->fetchAll();
       
        $data['departments'] = $this->departments->fetchAllDepartments();
        $data['items'] = $this->items->fetchAll();

        $this->load->view('template/header');
        $this->load->view('sale/outwardgatepass', $data);
        $this->load->view('template/mainnav');
        $this->load->view('template/footer', $data);
    }

    public function getMaxVrno() {
        $company_id = $this->session->userdata('company_id');
        $etype = $this->input->post('etype');
       
        $result = $this->orders->getMaxVrno($etype,$company_id) + 1;
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function getMaxVrnoa() {
        $company_id = $this->session->userdata('company_id');
         $etype = $this->input->post('etype');
        $result = $this->orders->getMaxVrnoa($etype,$company_id) + 1;
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function save() {

        if (isset($_POST)) {
            $company_id = $this->session->userdata('company_id');
            $ordermain = $_POST['ordermain'];
            $etype = $_POST['etype'];
            $orderdetail = $_POST['orderdetail'];
            $ordermain['company_id'] = $company_id;
            $vrnoa = $_POST['vrnoa'];
            $voucher_type_hidden = $_POST['voucher_type_hidden'];
            ///////////////////// Validation Close Date String
            $response = array();
            
            $chk_date = $_POST['vrdate'];
            $vrdate = "2016-01-01";
            $vrdate = $ordermain['vrdate'];

            $DateCloseStatus=false;
            if($chk_date!=$vrdate &&  $voucher_type_hidden=='edit'){
                $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
            }
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }

            $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
            
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }

            ///////////////////// Validation End

            $result = $this->orders->save_inward($ordermain, $orderdetail, $vrnoa, $etype,$company_id);


            $response = array();
            if ( $result === false ) {
                $response['error'] = 'true';
            } else {
                $response['error'] = $result;
            }

            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }

    public function fetch() {

        if (isset( $_POST )) {

            $vrnoa = $_POST['vrnoa'];
            $etype = $_POST['etype'];
            $company_id = $this->session->userdata('company_id');
            $result = $this->orders->fetchInwardGatePass22($vrnoa, $etype,$company_id);

            $response = "";
            if ( $result === false ) {
                $response = 'false';
            } else {
                $response = $result;
            }

            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }

    public function delete() {

        if (isset( $_POST )) {

            $vrnoa = $_POST['vrnoa'];
            $etype = $_POST['etype'];
            $company_id = $this->session->userdata('company_id');

            ///////////////////// Validation Close Date String
            $response = array();
            
            $chk_date = $_POST['chk_date'];
            $vrdate = "2016-01-01";
            $vrdate = $_POST['vrdate'];

            $DateCloseStatus=false;
            if($chk_date!=$vrdate){
                $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
            }
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }

            $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
            
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }

            ///////////////////// Validation End

            $result = $this->orders->delete($vrnoa, $etype,$company_id);

            $response = "";
            if ( $result === false ) {
                $response = 'false';
            } else {
                $response = $result;
            }

            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }

}

/* End of file inwardgatepass.php */
/* Location: ./application/controllers/inwardgatepass.php */