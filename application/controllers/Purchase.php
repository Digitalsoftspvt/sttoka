<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Purchase extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('accounts');
		$this->load->model('orders');
		$this->load->model('salesmen');
		$this->load->model('transporters');
		$this->load->model('items');
		$this->load->model('ledgers');
		$this->load->model('departments');
		$this->load->model('sales');
		$this->load->model('purchases');
		$this->load->model('users');
		$this->load->model('levels');	
		$this->load->model('datecloses');	
	}

public function fetchChartData()
	{
		$period = $_POST['period'];
		$company_id = $_POST['company_id'];
		$etype = $_POST['etype'];

		$data = $this->purchases->fetchChartData($period, $company_id,$etype);

		$json = json_encode($data);
		echo $json;
	}

	public function index() {
		unauth_secure();
		$cid   					= $this->session->userdata('company_id');
		$data['modules'] 		= array('purchase/addpurchase');
		$data['Attachments'] 	= true;
		$data['tablename'] 		= 'stockmain';
		$data['etype'] 			= 'purchase';
		$data['parties'] 		= $this->accounts->fetchAll(1,'purchase');
		// $data['expenses'] 		= $this->accounts->fetchAll_ExpAccountByLevelOne();

		// $data['salesmen'] 		= $this->salesmen->fetchAll();
		$data['receivers'] 		= $this->purchases->fetchByCol('received_by');
        // $data['issuers'] 		= $this->purchases->fetchByCol('issued_by');
		$data['transporters'] 	= $this->transporters->fetchAll();
		$data['departments'] 	= $this->departments->fetchAllDepartments();
		// $data['userone'] 		= $this->users->fetchAll();
		$data['setting_configur'] = $this->accounts->getsetting_configur();
		$data['l3s'] 			= $this->levels->fetchAllLevel3();
		$data['categories'] 	= $this->items->fetchAllCategories('catagory');
		$data['subcategories'] 	= $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] 		= $this->items->fetchAllBrands();
		$data['types'] 			= $this->items->fetchByCol('barcode');
		$data['orders_running'] = $this->purchases->fetchPOrders();
		$data['contracts'] 		= $this->orders->fetchPendingContracts($cid);


		$this->load->view('template/header',$data);
		$this->load->view('purchase/addpurchase', $data);
		$this->load->view('template/mainnav',$data);
		$this->load->view('template/footer', $data);
	}

public function fetchNetSum()
	{
		$company_id = $_POST['company_id'];
		$etype = $_POST['etype'];
		$sum = $this->purchases->fetchNetSum( $company_id ,$etype);
		
		$json = json_encode($sum);
		echo $json;
	}


	public function getMaxVrno() {
		if (isset($_POST)) {
		$company_id = $_POST['company_id'];
		$data['vrno'] = $this->purchases->getMaxVrno('purchase',$company_id) + 1;
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	}

	public function getMaxVrnoa() {
		if (isset($_POST)) {
			$company_id = $_POST['company_id'];
			$result = $this->purchases->getMaxVrnoa('purchase',$company_id) + 1;
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function save() {

		if (isset($_POST)) {

			$ledger = json_decode($_POST['ledger'], true);
			$ledger = (array)$ledger;
			$stockmain = json_decode($_POST['stockmain'], true);
			$stockmain = (array)$stockmain;
			$stockdetail = json_decode($_POST['stockdetail'], true);
			$stockdetail = (array)$stockdetail;
			$voucher_type_hidden = $_POST['voucher_type_hidden'];
			 ///////////////////// Validation Close Date String
	        $response = array();
	        
	        $chk_date = $_POST['vrdate'];
	        $vrdate = "2016-01-01";
	        $vrdate = $stockmain['vrdate'];

	        $DateCloseStatus=false;
	        if($chk_date!=$vrdate &&  $voucher_type_hidden=='edit'){
	            $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
	        }
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
	        
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        ///////////////////// Validation End

			$vrnoa = $_POST['vrnoa'];
			

			if ($voucher_type_hidden=='new'){
				
				$vrnoa = $this->purchases->getMaxVrnoa($stockmain['etype'] , $stockmain['company_id']) + 1;
				$stockmain['vrnoa'] = $vrnoa;
			}
			// $mobile='03009663902';
			// $message='test messges';
			// $result = $this->purchases->sendMessage( $mobile, $message );
			// consol.log($result);
					
			$result = $this->purchases->save($stockmain, $stockdetail, $vrnoa, 'purchase');
			if ($result) {
				
				$result = $this->ledgers->save($ledger, $vrnoa, 'purchase',$voucher_type_hidden);
			}

			$response = array();
			if ( $result === false ) {
				$response['error'] = 'true';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}
	public function saveItemConversion() {

		if (isset($_POST)) {

			$ledger = $_POST['ledger'];
			$stockmain = $_POST['stockmain'];
			$stockdetail = $_POST['stockdetail'];
			$vrnoa = $_POST['vrnoa'];
			$voucher_type_hidden = $_POST['voucher_type_hidden'];

			 ///////////////////// Validation Close Date String
	        $response = array();
	        
	        $chk_date = $_POST['vrdate'];
	        $vrdate = "2016-01-01";
	        $vrdate = $stockmain['vrdate'];

	        $DateCloseStatus=false;
	        if($chk_date!=$vrdate &&  $voucher_type_hidden=='edit'){
	            $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
	        }
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
	        
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        ///////////////////// Validation End

			// $mobile='03009663902';
			// $message='test messges';
			// $result = $this->purchases->sendMessage( $mobile, $message );
			// consol.log($result);
			if ($voucher_type_hidden=='new'){
				
				$vrnoa = $this->purchases->getMaxVrnoa('item_conversion' , $stockmain['company_id']) + 1;
				$stockmain['vrnoa'] = $vrnoa;
			}
			$result = $this->ledgers->save($ledger, $vrnoa, 'item_conversion',$voucher_type_hidden);
			$result = $this->purchases->save($stockmain, $stockdetail, $vrnoa, 'item_conversion');


			$response = array();
			if ( $result === false ) {
				$response['error'] = 'true';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetch() {

		if (isset( $_POST )) {

			$vrnoa = $_POST['vrnoa'];
			$company_id = $_POST['company_id'];	
			$result = $this->purchases->fetch($vrnoa, 'purchase',$company_id);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function delete() {

		if (isset( $_POST )) {

			$vrnoa = $_POST['vrnoa'];
			$etype = $_POST['etype'];
			$company_id = $_POST['company_id'];

			  ///////////////////// Validation Close Date String
	        $response = array();
	        
	        $chk_date = $_POST['chk_date'];
	        $vrdate = "2016-01-01";
	        $vrdate = $_POST['vrdate'];

	        $DateCloseStatus=false;
	        if($chk_date!=$vrdate){
	            $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
	        }
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
	        
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        ///////////////////// Validation End

			$result = $this->purchases->delete($vrnoa, $etype,$company_id);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchPurchaseReportData()
	{
		$what = $_POST['what'];
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];
		$type = $_POST['type'];
		$etype = $_POST['etype'];
		$company_id = $_POST['company_id'];
		$field = $_POST['field'];
		$crit = $_POST['crit'];
		$orderBy = $_POST['orderBy'];
		$groupBy = $_POST['groupBy'];
		$name = $_POST['name'];

		$sreportData = $this->purchases->fetchPurchaseReportData($startDate, $endDate, $what, $type, $company_id, $etype,$field,$crit,$orderBy,$groupBy,$name);
		$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($sreportData));
	}

	public function fetchContractPurchaseReportData()
	{
		$what = $_POST['what'];
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];
		$type = $_POST['type'];
		$etype = $_POST['etype'];
		$company_id = $_POST['company_id'];
		$field = $_POST['field'];
		$crit = $_POST['crit'];
		$orderBy = $_POST['orderBy'];
		$groupBy = $_POST['groupBy'];
		$name = $_POST['name'];

		$sreportData = $this->purchases->fetchContractPurchaseReportData($startDate, $endDate, $what, $type, $company_id, $etype,$field,$crit,$orderBy,$groupBy,$name);
		$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($sreportData));
	}

	public function fetchImportRangeSum()
	{
		$from = $_POST['from'];
		$to = $_POST['to'];

		$sum = $this->purchases->fetchImportRangeSum( $from, $to );

		$json = json_encode($sum);
		echo $json;
	}

	public function fetchRangeSum()
	{
		$from = $_POST['from'];
		$to = $_POST['to'];

		$sum = $this->purchases->fetchRangeSum( $from, $to );

		$json = json_encode($sum);
		echo $json;
	}
	public function itemConversion()
    {
        $data['modules'] = array('inventory/additemconversion');

        $data['parties'] = $this->accounts->fetchAll(1, 'purchase');
		$data['salesmen'] = $this->salesmen->fetchAll();
		$data['receivers'] = $this->purchases->fetchByCol('received_by');
		$data['transporters'] = $this->transporters->fetchAll();
		$data['departments'] = $this->departments->fetchAllDepartments();
		//$data['items'] = $this->items->fetchAll(1);
		$data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->items->fetchAll();
		$data['setting_configur'] = $this->accounts->getsetting_configur();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['types'] = $this->items->fetchByCol('barcode');
		$data['uoms'] = $this->items->fetchByCol('uom');
		$data['departments'] = (!is_array($data['departments']))? array() : $data['departments'];

        $this->load->view('template/header');
		$this->load->view('inventory/additemconversion', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
    }
    public function getmaxvrnoitemc()
    {
        $company_id = $_POST['company_id'];
        $result = $this->purchases->getMaxVrno('item_conversion', $company_id) + 1;
        $this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($result));
    }

    public function getmaxvrnoaitemc()
    {
        $company_id = $_POST['company_id'];
        $result = $this->purchases->getMaxVrnoa('item_conversion', $company_id) + 1;
        $this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($result));
    }
    public function fetchitemc()
    {
        $vrnoa = $_POST['vrnoa'];
        $company_id = $_POST['company_id'];
        $result = $this->purchases->fetchitemc($vrnoa, 'item_conversion', $company_id);

        $response = "";
        if ($result === false)
        {
            $response = 'false';
        }
        else
        {
            $response = $result;
        }
        $this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
    }
    public function fetchreportdataitemcmain()
    {
        $startDate = $_POST['from'];
        $endDate = $_POST['to'];
        $etype = $_POST['etype'];
        $company_id = $_POST['company_id'];
        $uid = $_POST['uid'];

        $sreportData = $this->purchases->fetchReportDataItemcMain($startDate, $endDate, $company_id, $etype, $uid);
        $this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($sreportData));
    }

    public function fetchItemCReportData()
    {
        $what = $_POST['what'];
        $startDate = $_POST['from'];
        $endDate = $_POST['to'];
        $type = $_POST['type'];
        $etype = $_POST['etype'];
        $company_id = $_POST['company_id'];
        $field = $_POST['field'];
        $crit = $_POST['crit'];
        $orderBy = $_POST['orderBy'];
        $groupBy = $_POST['groupBy'];
        $name = $_POST['name'];

        $sreportData = $this->purchases->fetchitemcReportData($startDate, $endDate, $what, $type, $company_id, $etype, $field, $crit, $orderBy, $groupBy, $name);
        $this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($sreportData));
    }

    public function fetchAllTransactions()
    {
        $from = $_POST['from'];
        $to = $_POST['to'];
        $company_id = $_POST['company_id'];

        $sum = $this->purchases->fetchAllTransactions($from, $to,$company_id);
        $this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($sum));
    }

    public function fetchRemaining() {

		if (isset( $_POST )) {

			$vrnoa = $_POST['vrnoa'];
			$company_id = $_POST['company_id'];
			//$etype = $_POST['etype'];	
			//die(print($vrnoa.' , '.$company_id.' , '.$etype));
			$result = $this->orders->fetchRemaining($vrnoa, 'purchasecontract',$company_id);
			//die(print_r($result));
			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

    
}

/* End of file purchase.php */
/* Location: ./application/controllers/purchase.php */