<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cashslip extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('messages');
		$this->load->model('accounts');
		$this->load->model('cashslips');
		$this->load->model('ledgers');
		$this->load->model('datecloses');
	}

	public function index() {
		//unauth_secure();
        ini_set('max_execution_time',100);
		$data['modules'] 	= array('sale/addcashslip');
		$data['tablename'] 	= 'cashdeposit';
		$data['parties'] 	= $this->accounts->fetchAll(1,'cashslip');
		$data['cashes'] 	= $this->accounts->fetchAll(1,'cashslip_cash');

		//$data['banks'] = $this->accounts->fetchAll(1,'cashslip_bank');
        $data['banks'] = $this->cashslips->fetchAllbanknames();
		$data['setting_configur'] = $this->accounts->getsetting_configur();
		$data['fields'] = $this->accounts->fetchAll(1,'fields');
		//$data['transporters'] = $this->transporters->fetchAll();

		$this->load->view('template/header',$data);
		$this->load->view('sale/addcashslip', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function getMaxVrnoa() {
		if (isset($_POST)) {
			$company_id = $_POST['company_id'];
			$etype = $_POST['etype'];
			$result = $this->cashslips->getMaxVrnoa($etype,$company_id) + 1;
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function save() {

		if (isset($_POST)) {

			$ledger   = $_POST['ledger'];
			$cashmain = $_POST['cashmain'];
			$vrnoa 	  = $_POST['vrnoa'];
			$etype	  = $_POST['etype'];
			$voucher_type_hidden = $_POST['voucher_type_hidden'];

			 ///////////////////// Validation Close Date String
	        $response = array();
	        
	        $chk_date = $_POST['vrdate'];
	        $vrdate = "2016-01-01";
	        $vrdate = $cashmain[0]['vrdate'];

	        $DateCloseStatus=false;
	        if($chk_date!=$vrdate &&  $voucher_type_hidden=='edit'){
	            $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
	        }
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
	        
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        ///////////////////// Validation End

			if($voucher_type_hidden == 'new'){

				$vrnoa = $this->cashslips->getMaxVrnoa($etype,$cashmain[0]['company_id']) + 1;
			}

			$result   = $this->ledgers->save($ledger, $vrnoa, $etype ,$voucher_type_hidden);
 			$result   = $this->cashslips->save( $cashmain, $vrnoa , $etype);

			$response = array();
			if ($result === false) {
				$response['error'] = true;
			} else {
				$response['error'] = false;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetch() {

		if (isset( $_POST )) {

			$vrnoa = $_POST['vrnoa'];
			$company_id = $_POST['company_id'];	
			$etype = $_POST['etype'];
			$result = $this->cashslips->fetch($vrnoa, $etype,$company_id);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchLastLedger() {

		if (isset( $_POST )) {

			$partyId = $_POST['party_id'];
			$endDate = $_POST['to_date'];
			$result = $this->cashslips->fetchLastLedger($partyId, $endDate);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function delete() {

		if (isset( $_POST )) {

			$vrnoa = $_POST['vrnoa'];
			$etype = $_POST['etype'];
			$company_id = $_POST['company_id'];

			  ///////////////////// Validation Close Date String
	        $response = array();
	        
	        $chk_date = $_POST['chk_date'];
	        $vrdate = "2016-01-01";
	        $vrdate = $_POST['vrdate'];

	        $DateCloseStatus=false;
	        if($chk_date!=$vrdate){
	            $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
	        }
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
	        
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        ///////////////////// Validation End

			$result = $this->ledgers->deleteVoucher($vrnoa, $etype,$company_id);
			$result = $this->cashslips->delete($vrnoa, $etype,$company_id);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

}

/* End of file transporter.php */
/* Location: ./application/controllers/transporter.php */