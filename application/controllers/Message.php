<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Message extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('messages');
		$this->load->model('accounts');
	}

	public function index() {
		//unauth_secure();
		
		$data['modules'] = array('setup/addmessage');
		$data['accounts'] = $this->accounts->fetchAll();
		$data['parties'] = $this->accounts->fetchAll();
		
		$this->load->view('template/header');
		$this->load->view('setup/addmessage', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function getMaxId() {
		$result = $this->messages->getMaxId() + 1;
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function save() {

		if (isset($_POST)) {

			$message = $_POST['message'];
			$result = $this->messages->save( $message );

			$response = array();
			if ($result === false) {
				$response['error'] = true;
			} else {
				$response['error'] = false;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetch() {

		if (isset( $_POST )) {

			$message_id = $_POST['message_id'];
			$result = $this->messages->fetch($message_id);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}
	
	public function fetchPartyInfo() {

		$id=$_POST['id'];
		$result = $this->messages->fetchPartyInfo($id);

		$response = array();
		if ( $result === false ) {
			$response = 'false';
		} else {			
			$response = $result;
		}

		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($response));
	}

}

/* End of file message.php */
/* Location: ./application/controllers/message.php */