<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Purchasecontract extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('accounts');
		
		$this->load->model('transporters');
		$this->load->model('items');
		$this->load->model('ledgers');
		$this->load->model('salesmen');
		//$this->load->model('purchasecontracts');
		$this->load->model('orders');
		$this->load->model('purchases');
		$this->load->model('users');
		$this->load->model('levels');
		$this->load->model('datecloses');
	}

public function fetchChartData()
	{
		$period = $_POST['period'];
		$company_id = $_POST['company_id'];
		$etype = $_POST['etype'];

		$data = $this->purchases->fetchChartData($period, $company_id,$etype);

		$json = json_encode($data);
		echo $json;
	}

	public function index() {
		//unauth_secure();
		$data['modules'] 	= array('contract/addpurchasecontract');
		$data['parties'] = $this->accounts->fetchAll(1,'purchasecontract');
		//$data['salesmen'] = $this->salesmen->fetchAll();
		// $data['receivers'] 	= $this->purchases->fetchByCol('received_by');
		// $data['transporters'] = $this->transporters->fetchAll();
		// $data['items'] 		= $this->items->fetchAll(1,'fabric');
		// $data['userone'] 	= $this->users->fetchAll();
		// // $data['setting_configur'] = $this->accounts->getsetting_configur();
		// $data['l3s'] 		= $this->levels->fetchAllLevel3();
		
		// $data['categories'] = $this->items->fetchAllCategories('catagory');
		// $data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		// $data['brands'] 	= $this->items->fetchAllBrands();
		// $data['types'] 		= $this->items->fetchByCol('barcode');
		$data['approvedby'] = $this->orders->fetchAllApprovedBy();
		$data['preparedby'] = $this->orders->fetchAllPreparedBy();
		$data['brokers'] 	= $this->salesmen->fetchAll();
		// $data['worder'] 	= $this->orders->fetchAllSaleOrder();

		// $data['dispatch_addresss'] = $this->orders->fetchAllDistinctMain('dispatch_address');
		// $data['delivery_terms'] = $this->orders->fetchAllDistinctMain('delivery_term');
		// $data['payment_terms'] = $this->orders->fetchAllDistinctMain('payment_term');
		
		
		// $data['reeds'] = $this->orders->fetchAllDistinct('reed');
		// $data['picks'] = $this->orders->fetchAllDistinct('pick');
		// $data['widths'] = $this->orders->fetchAllDistinct('width');
		// $data['counts'] = $this->orders->fetchAllDistinct('count');

		$this->load->view('template/header');
		$this->load->view('contract/addPurchaseContract',$data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer',$data);
	}

public function fetchNetSum()
	{
		$company_id = $_POST['company_id'];
		$etype = $_POST['etype'];
		$sum = $this->purchases->fetchNetSum( $company_id ,$etype);
		
		$json = json_encode($sum);
		echo $json;
	}


	public function getMaxVrno() {
		if (isset($_POST)) {
		$company_id = $_POST['company_id'];//$_POST['company_id'];
		$result = $this->orders->getMaxVrno('purchasecontract',$company_id) + 1;
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	}

	public function getMaxVrnoa() {

		if (isset($_POST)) {
		$company_id = $_POST['company_id'];//$_POST['company_id'];
		$result = $this->orders->getMaxVrnoa('purchasecontract',$company_id) + 1;
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	}

	public function save() {

		if (isset($_POST)) {

			//$ledger = $_POST['ledger'];
			$stockmain = $_POST['stockmain'];
			$stockdetail = $_POST['stockdetail'];
			$vrnoa = $_POST['vrnoa'];
			$voucher_type_hidden = $_POST['voucher_type_hidden'];
			if ($voucher_type_hidden=='new'){
				$vrnoa = $this->orders->getMaxVrnoa('purchasecontract',$stockmain['company_id']) + 1;
				$stockmain['vrnoa'] = $vrnoa;
			}

			  ///////////////////// Validation Close Date String
	        $response = array();
	        
	        $chk_date = $_POST['vrdate'];
	        $vrdate = "2016-01-01";
	        $vrdate = $stockmain['vrdate'];

	        $DateCloseStatus=false;
	        if($chk_date!=$vrdate &&  $voucher_type_hidden=='edit'){
	            $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
	        }
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
	        
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        ///////////////////// Validation End

			//$result = $this->ledgers->save($ledger, $vrnoa, 'sale',$voucher_type_hidden);
			$result = $this->orders->save($stockmain, $stockdetail, $vrnoa,'purchasecontract' );


			$response = array();
			if ( $result === false ) {
				$response['error'] = 'true';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetch() {

		if (isset( $_POST )) {

			$vrnoa = $_POST['vrnoa'];
			$company_id = $_POST['company_id'];//$_POST['company_id'];	
			$result = $this->orders->fetch($vrnoa, 'purchasecontract',$company_id);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function delete() {

		if (isset( $_POST )) {

			$vrnoa = $_POST['vrnoa'];
			$etype = $_POST['etype'];
			$company_id = $_POST['company_id'];//$_POST['company_id'];

			  ///////////////////// Validation Close Date String
	        $response = array();
	        
	        $chk_date = $_POST['chk_date'];
	        $vrdate = "2016-01-01";
	        $vrdate = $_POST['vrdate'];

	        $DateCloseStatus=false;
	        if($chk_date!=$vrdate){
	            $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
	        }
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
	        
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        ///////////////////// Validation End

			$result = $this->orders->delete($vrnoa, $etype,$company_id);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchPurchaseReportData()
	{
		$what = $_POST['what'];
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];
		$type = $_POST['type'];

		$sreportData = $this->purchases->fetchPurchaseReportData($startDate, $endDate, $what, $type);
		$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($sreportData));
	}

	public function fetchImportRangeSum()
	{
		$from = $_POST['from'];
		$to = $_POST['to'];

		$sum = $this->purchases->fetchImportRangeSum( $from, $to );

		$json = json_encode($sum);
		echo $json;
	}

	public function fetchRangeSum()
	{
		$from = $_POST['from'];
		$to = $_POST['to'];

		$sum = $this->purchases->fetchRangeSum( $from, $to );

		$json = json_encode($sum);
		echo $json;
	}

	// public function saveitems() {

	// 	if (isset($_POST)) {

	// 		$result = $this->items->save($_POST);

	// 		$response = array();
	// 		if ($result == false) {
	// 			$response['error'] = true;
	// 		} else {
	// 			$response = $result;
	// 		}

	// 		$this->output
	// 			 ->set_content_type('application/json')
	// 			 ->set_output(json_encode('true'));
	// 	}
	// }

	// public function fetchAllItems(){
	// 	$activee=(isset($_POST['active'])? $_POST['active']:-1);
	// 	$result = $this->items->fetchAll();
	// 	$this->output
	// 		 ->set_content_type('application/json')
	// 		 ->set_output(json_encode($result));
	// }
}

/* End of file purchase.php */
/* Location: ./application/controllers/purchase.php */