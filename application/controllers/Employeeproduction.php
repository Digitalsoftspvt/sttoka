<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employeeproduction extends CI_Controller {

	public function __construct() {
        parent::__construct();        
        $this->load->model('accounts');
        $this->load->model('items');
        $this->load->model('staffs');
        $this->load->model('jobs');
        $this->load->model('departments');
        $this->load->model('purchases');
        $this->load->model('ledgers');
    }

    public function index() {
        unauth_secure();
        $data['modules'] = array('inventory/addemployeeproduction');
        $data['Attachments']    = true;
        $data['tablename']      = 'stockmain';
        $data['etype']          = 'employeeproduction';
        $data['parties'] = $this->accounts->fetchAllEmployee();
        $data['expenseAccount'] = $this->accounts->fetchAll_ExpAccountByLevelOne();
        $data['receivers'] = $this->purchases->fetchByCol('received_by');
        $data['departments'] = $this->departments->fetchAllDepartments();
        $data['items'] = $this->items->fetchAll();
        $data['jobs'] = $this->jobs->fetchAllJobs('job_order');

        $this->load->view('template/header',$data);
        $this->load->view('inventory/addemployeeproduction', $data);
        $this->load->view('template/mainnav');
        $this->load->view('template/footer', $data);
    }

    public function add() {
       
    }

    public function getMaxVrno() {
        $company_id = $_POST['company_id'];
        $result = $this->purchases->getMaxVrno('employeeproduction',$company_id) + 1;
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function getMaxVrnoa() {
        $company_id = $_POST['company_id'];
        $result = $this->purchases->getMaxVrnoa('employeeproduction',$company_id) + 1;
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function save() {

        if (isset($_POST)) {

            $stockmain = $_POST['stockmain'];
            $stockdetail = $_POST['stockdetail'];
            $vrnoa = $_POST['vrnoa'];
            $ledger = json_decode($_POST['ledger'], true);
            $ledger = (array)$ledger;
            $voucher_type_hidden = $_POST['voucher_type_hidden'];
            if ($voucher_type_hidden=='new'){

                $vrnoa = $this->purchases->getMaxVrnoa('employeeproduction', $stockmain['company_id']) + 1;
                $stockmain['vrnoa'] = $vrnoa;
            }

            $result = $this->ledgers->save($ledger, $vrnoa, 'employeeproduction',$voucher_type_hidden);
            $result = $this->purchases->save($stockmain, $stockdetail, $vrnoa, 'employeeproduction');


            $response = array();
            if ( $result === false ) {
                $response['error'] = 'true';
            } else {
                $response = $result;
            }

            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }

    public function fetch() {

        if (isset( $_POST )) {
            $vrnoa = $_POST['vrnoa'];
            $company_id = $_POST['company_id'];
            $result = $this->purchases->employeeFetchProduction($vrnoa, 'employeeproduction',$company_id);
            $response = "";
            if ( $result === false ) {
                $response = 'false';
            } else {
                $response = $result;
            }
            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }


    public function fetchStaffPhaseInfo() {

        if (isset( $_POST )) {
            $employeeId = $_POST['employee_id'];
            $companyId = $_POST['company_id'];
            $item_id = $_POST['item_id'];
            //$result = $this->purchases->fetchStaffPhaseData($employeeId, $companyId);
            $result = $this->staffs->fetchStaffPhaseInfo($employeeId,$item_id);

            $response = "";
            if ( $result === false ) {
                $response = 'false';
            } else {
                $response = $result;
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($response));
        }
    }
    public function fetchStaffPhaseData() {

        if (isset( $_POST )) {
            $employeeId = $_POST['employee_id'];
            $companyId = $_POST['company_id'];
            //$result = $this->purchases->fetchStaffPhaseData($employeeId, $companyId);
            $result = $this->staffs->fetchStaffPhaseData($employeeId);
            
            $response = "";
            if ( $result === false ) {
                $response = 'false';
            } else {
                $response = $result;
            }
            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }

    public function delete() {

        if (isset( $_POST )) {

            $vrnoa = $_POST['vrnoa'];
            $company_id = $_POST['company_id'];
            $result = $this->purchases->delete($vrnoa, 'employeeproduction',$company_id);

            $response = "";
            if ( $result === false ) {
                $response = 'false';
            } else {
                $response = $result;
            }

            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }
}

/* End of file production.php */
/* Location: ./application/controllers/production.php */