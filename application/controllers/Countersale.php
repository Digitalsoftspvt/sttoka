<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Countersale extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('orders');
		$this->load->model('accounts');
		$this->load->model('salesmen');
		$this->load->model('transporters');
		$this->load->model('items');
		$this->load->model('ledgers');
		$this->load->model('purchases');
		$this->load->model('departments');
		$this->load->model('sales');
		$this->load->model('countersales');
		$this->load->model('users');
		$this->load->model('levels');
		$this->load->model('datecloses');

		
		
	}

	public function fetchChartData()
	{
		$period = $_POST['period'];
		$company_id = $_POST['company_id'];
		$etype = $_POST['etype'];

		$data = $this->orders->fetchChartData($period, $company_id,$etype);

		$json = json_encode($data);
		echo $json;
	}

	public function index() {
		unauth_secure();

		$data['modules'] = array('sale/addcountersale');
		// $data['parties'] = $this->accounts->fetchAll(1,'all');
		$data['parties'] = $this->accounts->fetchAll(1, 'countersale');
		$data['salesmen'] = $this->salesmen->fetchAll();
		$data['accountCashs'] = $this->accounts->fetchAll_CashAccount();
		$data['receivers'] = $this->purchases->fetchByCol('received_by');
		$data['transporters'] = $this->transporters->fetchAll();
		$data['departments'] = $this->departments->fetchAllDepartments();
		//$data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();
		$data['setting_configur'] = $this->accounts->getsetting_configur();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['types'] = $this->items->fetchByCol('barcode');
		$data['uoms'] = $this->items->fetchByCol('uom');


		$this->load->view('template/header');
		$this->load->view('sale/addcountersale', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function fetchAllFilterItems(){

		$categoryId = $this->input->post('category_id');
		$subCategoryId = $this->input->post('sub_category_id');
		$brandId = $this->input->post('brand_id');

		$result = $this->countersales->fetchAllFilterItems($categoryId, $subCategoryId, $brandId);
		
        $result = json_encode($result);
    	echo $result;
    	exit();
	}

	public function Sale_Invoice() {
		unauth_secure();

		$data['modules'] = array('sale/addcountersaleInvoice');
		$data['parties'] = $this->accounts->fetchAll(1,'all');
		$data['salesmen'] = $this->salesmen->fetchAll();
		// $data['receivers'] = $this->orders->fetchByCol('received_by');
		$data['transporters'] = $this->transporters->fetchAll();
		$data['departments'] = $this->departments->fetchAllDepartments();
		$data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();
		$data['setting_configur'] = $this->accounts->getsetting_configur();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['types'] = $this->items->fetchByCol('barcode');
		$data['orders_running'] = $this->orders->fetchOrders(date("Y/m/d"), date("Y/m/d"), 'sale');
		$data['uoms'] = $this->items->fetchByCol('uom');


		$this->load->view('template/header');
		$this->load->view('sale/addcountersaleInovoice', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}


	public function partsdetail() {
		
		

		unauth_secure();

		$data['modules'] = array('sale/orderpartsdetail');
		$data['parties'] = $this->accounts->fetchAll(1,'all');
		$data['salesmen'] = $this->salesmen->fetchAll();
		// $data['receivers'] = $this->orders->fetchByCol('received_by');
		$data['transporters'] = $this->transporters->fetchAll();
		$data['departments'] = $this->departments->fetchAllDepartments();
		$data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();
		$data['setting_configur'] = $this->accounts->getsetting_configur();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['types'] = $this->items->fetchByCol('barcode');
		$data['orders_running'] = $this->orders->fetchOrders(date("Y/m/d"), date("Y/m/d"), 'running');


		$this->load->view('template/header');
		$this->load->view('sale/orderpartsdetail', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function partsloading() {
		
		unauth_secure();

		$data['modules'] = array('sale/loadingparts');
		$data['parties'] = $this->accounts->fetchAll(1,'all');
		$data['salesmen'] = $this->salesmen->fetchAll();
		// $data['receivers'] = $this->orders->fetchByCol('received_by');
		$data['transporters'] = $this->transporters->fetchAll();
		$data['departments'] = $this->departments->fetchAllDepartments();
		$data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();
		$data['setting_configur'] = $this->accounts->getsetting_configur();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['types'] = $this->items->fetchByCol('barcode');
		$data['orders_running'] = $this->orders->fetchOrders(date("Y/m/d"), date("Y/m/d"), 'running_loading');
		$data['uoms'] = $this->items->fetchByCol('uom');


		$this->load->view('template/header');
		$this->load->view('sale/loadingparts', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function fetchNetSum(){
		
		$company_id = $_POST['company_id'];
		$etype = $_POST['etype'];
		$sum = $this->orders->fetchNetSum( $company_id ,$etype);
		
		$json = json_encode($sum);
		echo $json;
	}

	public function Loading_Stock()
	{
		$order_no = $_POST['order_no'];
		$company_id = $_POST['company_id'];
		$sum = $this->orders->Loading_Stock( $company_id ,$order_no);
		
		$json = json_encode($sum);
		echo $json;
	}


	public function getMaxVrno() {
		if (isset($_POST)) {
			$company_id = $_POST['company_id'];
			$etype = $_POST['etype'];
			$result = $this->countersales->getMaxVrno($etype ,$company_id) + 1;
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}


	public function getMaxVrnoa() {
		if (isset($_POST)) {
			$company_id = $_POST['company_id'];
			$etype = $_POST['etype'];
			$result = $this->countersales->getMaxVrnoa($etype ,$company_id) + 1;
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}
	public function Validate_Order() {
		if (isset($_POST)) {
			$company_id = $_POST['company_id'];
			$etype = $_POST['etype'];
			$order_no = $_POST['order_no'];
			$status = $_POST['status'];
			$result = $this->orders->Validate_Order($etype ,$company_id, $order_no,$status );
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}
	public function Validate_Order_Loading() {
		if (isset($_POST)) {
			$company_id = $_POST['company_id'];
			$etype = $_POST['etype'];
			$order_no = $_POST['order_no'];
			$status = $_POST['status'];
			$result = $this->orders->Validate_Order_Loading($etype ,$company_id, $order_no,$status );
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function save() {

		if (isset($_POST)) {

			
			$ordermain = $_POST['ordermain'];
			$orderdetail = $_POST['orderdetail'];
			$vrnoa = $_POST['vrnoa'];
			$voucher_type_hidden = $_POST['voucher_type_hidden'];
			$etype = $_POST['etype'];
            
            $requestData = array(
				'ordermain' => json_encode($_POST['ordermain']),
				'orderdetail' => json_encode($_POST['orderdetail']),
				'voucher_type' => $_POST['voucher_type_hidden']
			);
			$requestData['vrnoa'] = $_POST['vrnoa'];
			$requestData['type'] = 'before'; 
			$requestData['log_no'] = 1;
			$this->countersales->saveRequest($requestData);
			
			if ($voucher_type_hidden=='new'){
				
				$vrnoa = $this->countersales->getMaxVrnoa($etype , $ordermain['company_id']) + 1;
				$ordermain['vrnoa'] = $vrnoa;
			}
			$requestData['vrnoa'] = $vrnoa;
			$requestData['type'] = 'after';
			$requestData['log_no'] = 2; 
			$this->countersales->saveRequest($requestData);
			
			if($etype=='sale'){
				
				$ledger = $_POST['ledger'];
				$result = $this->ledgers->save($ledger, $vrnoa, $etype ,$voucher_type_hidden);
			}
			
			if($etype=='order_loading'){
				
				$stockmain = $_POST['stockmain'];
				$stockdetail = $_POST['stockdetail'];
				$result = $this->purchases->save($stockmain, $stockdetail, $vrnoa, $etype);
			}


			$result = $this->countersales->save($ordermain, $orderdetail, $vrnoa, $etype);
			if($result && $etype=='counter_sale') {
                $ledger = $_POST['ledger'];

                if (!empty($ledger))
                {
                $result = $this->ledgers->save($ledger, $vrnoa, $etype, $voucher_type_hidden);
            	}	
            }

			$response = array();
			if ( $result === false ) {
				$response['error'] = 'true';
			} else {
				//$response = $result;
				$response['result'] = $result;
                $response['vrnoa']  = $vrnoa;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}
    
    public function saveCounterSale() {

		if (isset($_POST)) {

			
			$ordermain = $_POST['ordermain'];
			$orderdetail = $_POST['orderdetail'];
			$vrnoa = $_POST['vrnoa'];
			$voucher_type_hidden = $_POST['voucher_type_hidden'];
			$etype = $_POST['etype'];
			$ordermain['voucher_type'] = $_POST['voucher_type_hidden'];

			$requestData = array(
				'ordermain' => json_encode($_POST['ordermain']),
				'orderdetail' => json_encode($_POST['orderdetail']),
				'voucher_type' => $_POST['voucher_type_hidden']
			);
			$requestData['vrnoa'] = $_POST['vrnoa'];
			$requestData['type'] = 'before'; 
			$requestData['log_no'] = 1;
			$this->countersales->saveRequest($requestData);
			/*if ($voucher_type_hidden=='new'){
				
				$requestData['log_no'] = 2;
				$this->countersales->saveRequest($requestData);
				$vrnoa = $this->countersales->getMaxVrnoa($etype , $ordermain['company_id']) + 1;
				$ordermain['vrnoa'] = $vrnoa;
			}
			$requestData['vrnoa'] = $vrnoa;
			$requestData['type'] = 'after';
			$requestData['log_no'] = 3; 
			$this->countersales->saveRequest($requestData);*/

			  ///////////////////// Validation Close Date String
	        $response = array();
	        
	        $chk_date = $_POST['vrdate'];
	        $vrdate = "2016-01-01";
	        $vrdate = $ordermain['vrdate'];

	        $DateCloseStatus=false;
	        if($chk_date!=$vrdate &&  $voucher_type_hidden=='edit'){
	            $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
	        }
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
	        
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        ///////////////////// Validation End
			$result = $this->countersales->save($ordermain, $orderdetail, $vrnoa, $etype);
			if($result){
				
				$vrnoa = $result;
				$ledger = $_POST['ledger'];
				$result = $this->ledgers->save($ledger, $vrnoa, $etype ,$voucher_type_hidden);
			}

			$response = array();
			if ( $result === false ) {
				$response['error'] = 'true';
			} else {
				//$response = $result;
				$response['result'] = $result;
                $response['vrnoa']  = $vrnoa;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}
	
	public function fetch() {

		if (isset( $_POST )) {

			$vrnoa = $_POST['vrnoa'];
			$company_id = $_POST['company_id'];	
			$etype = $_POST['etype'];
			
			$result = $this->countersales->fetch($vrnoa,$etype,$company_id);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}
	public function last_5_srate() {

		if (isset( $_POST )) {

			$party_id = $_POST['party_id'];
			$company_id = $_POST['company_id'];	
			$item_id = $_POST['item_id'];
			$result = $this->orders->last_5_srate($party_id, $item_id,$company_id);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetch_order_stock() {

		if (isset( $_POST )) {

			$vrnoa = $_POST['vrnoa'];
			$company_id = $_POST['company_id'];	
			$etype = $_POST['etype'];
			$result = $this->orders->fetch_order_stock($vrnoa, $etype,$company_id);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchPartsOrder() {

		if (isset( $_POST )) {

			$vrnoa = $_POST['vrnoa'];
			$company_id = $_POST['company_id'];	
			$etype = $_POST['etype'];

			$result_vrnoa = $this->orders->fetch($vrnoa, $etype,$company_id);
			$result_parts = $this->orders->fetchPartsOrder($vrnoa, $etype,$company_id,'parts');
			$result2_spare = $this->orders->fetchPartsOrder($vrnoa, $etype,$company_id,'spare_parts');
			$result3_less= $this->orders->fetchPartsOrder($vrnoa, $etype,$company_id,'less');

			
			if ( $result_vrnoa === false ) {
				$response['vrnoa'] = 'false';
			} else {
				$response['vrnoa'] = $result_vrnoa;
			}

			if ( $result_parts === false ) {
				$response['parts'] = 'false';
			} else {
				$response['parts'] = $result_parts;
			}
			if ( $result2_spare === false ) {
				$response['spare'] = 'false';
			} else {
				$response['spare'] = $result2_spare;
			}
			if ( $result3_less === false ) {
				$response['less'] = 'false';
			} else {
				$response['less'] = $result3_less;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetch_loading_Stock() {

		if (isset( $_POST )) {

			$vrnoa = $_POST['vrnoa'];
			$company_id = $_POST['company_id'];	
			$etype = $_POST['etype'];

			$result_vrnoa = $this->orders->fetch($vrnoa, $etype,$company_id);
			$result_parts = $this->orders->fetch_loading_Stock($vrnoa,$company_id);
			
			if ( $result_vrnoa === false ) {
				$response['vrnoa'] = 'false';
			} else {
				$response['vrnoa'] = $result_vrnoa;
			}

			if ( $result_parts === false ) {
				$response['parts'] = 'false';
			} else {
				$response['parts'] = $result_parts;
			}
			

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}



	public function fetchOrderReportData()
	{
		$what = $_POST['what'];
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];
		$type = $_POST['type'];
		$etype = $_POST['etype'];
		$company_id = $_POST['company_id'];

		$sreportData = $this->orders->fetchOrderReportData($startDate, $endDate, $what, $type, $company_id, $etype);
		$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($sreportData));
	}


	public function delete() {

		if (isset( $_POST )) {

			$vrnoa = $_POST['vrnoa'];
			$etype = $_POST['etype'];
			$company_id = $_POST['company_id'];

			  ///////////////////// Validation Close Date String
	        $response = array();
	        
	        $chk_date = $_POST['chk_date'];
	        $vrdate = "2016-01-01";
	        $vrdate = $_POST['vrdate'];

	        $DateCloseStatus=false;
	        if($chk_date!=$vrdate){
	            $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
	        }
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
	        
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        ///////////////////// Validation End

			$result = $this->countersales->delete($vrnoa, $etype,$company_id);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchPurchaseReportData()
	{
		$what = $_POST['what'];
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];
		$type = $_POST['type'];
		$etype = $_POST['etype'];
		$company_id = $_POST['company_id'];

		$sreportData = $this->orders->fetchPurchaseReportData($startDate, $endDate, $what, $type, $company_id, $etype);
		$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($sreportData));
	}

	public function fetchImportRangeSum()
	{
		$from = $_POST['from'];
		$to = $_POST['to'];

		$sum = $this->orders->fetchImportRangeSum( $from, $to );

		$json = json_encode($sum);
		echo $json;
	}

	public function fetchRangeSum()
	{
		$from = $_POST['from'];
		$to = $_POST['to'];

		$sum = $this->orders->fetchRangeSum( $from, $to );

		$json = json_encode($sum);
		echo $json;
	}
}

/* End of file purchase.php */
/* Location: ./application/controllers/purchase.php */