<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class finanicalyear extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('setting_configurations');
		$this->load->model('finanicalyears');
		$this->load->model('users');
		$this->load->model('accounts');
		
	}

	public function index() {
		unauth_secure();
		$data['title'] = 'Financial Year';
		$data['modules'] = array('setup/addfinancialyear');
		$data['finanicalyears'] = $this->finanicalyears->fetchAll();
		$data['parties'] = $this->accounts->fetchAll(1, 'sale');
		$data['setting'] = $this->setting_configurations->fetch();
		$this->load->view('template/header', $data);
		$this->load->view('setup/addfinancialyear', $data);
		$this->load->view('template/mainnav');	
		$this->load->view('template/footer', $data);
	}


	public function getMaxId() {
		$result = $this->finanicalyears->getMaxId() + 1;
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	public function save() {

		if (isset($_POST)) {
			$transporter = $_POST['transporter'];
			$result = $this->finanicalyears->save( $transporter );

			$response = array();
			if ($result === false) {
				$response['error'] = true;
			} else {
				$response['error'] = false;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
		
	}
	public function fetch() {

		if (isset( $_POST )) {

			$transporter_id = $_POST['transporter_id'];
			$result = $this->finanicalyears->fetch($transporter_id);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}
	public function fetchAll() {

		if (isset( $_POST )) {

			// $transporter_id = $_POST['transporter_id'];
			$result = $this->finanicalyears->fetchAll();

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}
public function delete()
	{
		if (isset($_POST)) {
			$result = $this->finanicalyears->delete($_POST['transporter_id']);			
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}
}

/* End of file transporter.php */
/* Location: ./application/controllers/transporter.php */