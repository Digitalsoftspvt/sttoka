<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FileUploader extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Here we load library 
		$this->load->library('upload');
		
		// Here we load models
		$this->load->model('fileuploaders');
		$this->load->model('orders');
	}

	public function uploadFiles()
	{
		$vrnoa = $_POST['vrnoa'];
        $cond = $this->fileuploaders->isVoucherExist($vrnoa, $_POST['etype'], $_POST['table']);
        $response = array();

		
		if($cond)
		{
			if(!empty($_FILES))
			{
				$_FILES['file']['name'] = 'Vr_'.$_POST['vrnoa']."-".time() ."-". $_FILES['file']['name'];
				$file_name = $_FILES['file']['name'];
				$arr = array(
							 'img_id'=> '',
							 'vrnoa'=> $_POST['vrnoa'],
							 'etype'=> $_POST['etype'],
							 'photo'=> $file_name,
							 'remarks'=> $_POST['remarks'],						 
							);
				$result = $this->fileuploaders->uploadImages($arr);
				if ($result===false)
				{
					$response = false;
				}
				else
				{
					$response = $result;	
				}
				
				// $response['result'] = $this->uploading($result, $_POST['etype']);
				
			}
		}
		if ($cond===false)
		{
			$response = false;
		}
		else
		{
			$response = $cond;
		}

		$this->output
					 ->set_content_type('application/json')
					 ->set_output(json_encode($response));
			
	}


	// Here We Fetch Images From Database By Voucher Number And Etype
	public function fetchImages()
	{
		if (isset( $_POST ))
		{
			$vrnoa 	= $_POST['vrnoa'];
			$etype 	= $_POST['etype'];

			$result = $this->fileuploaders->fetchImages($vrnoa, $etype);

			$response = "";

			if ( $result === false )
			{
				$response = 'false';
			} 
			else
			{
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	// Here We Delete Images From Database By Voucher Number And Etype
	public function deleteImage()
	{
		if (isset($_POST))
		{
			$item_id 	= $_POST['item_id'];
			$etype 		= $_POST['etype'];
			$imgName 	= $_POST['imgName'];

			$path 		=  FCPATH . 'assets/uploads/' . $etype . '/';

		    $result 	= $this->fileuploaders->deleteImage($item_id, $etype, $imgName, $path);
		    
		    $response = "";
		    if($result == false )
		    {
		    	$response =  false;
		    } 
		    else 
		    {
		    	$response = $result;
		    }

		    $this->output
						 ->set_content_type('application/json')
						 ->set_output(json_encode($response));
		}
	}

	public function updateFilesRemarks()
	{
		if(isset($_POST))
		{
			$response = array();
			$vrnoa = $_POST['vrnoa'];
			$etype = $_POST['etype'];
			for ($i = 0; $i < count($_POST['data']); $i++) {
				$response[] = $this->fileuploaders->updateFilesRemarks($vrnoa, $etype, $_POST['data'][$i]);
			}
			$this->output
						 ->set_content_type('application/json')
						 ->set_output(json_encode($response));
		}
	}


	// public function uploading($result, $etype)

	// {
		
	// 	$config['upload_path']   = ($_SERVER['DOCUMENT_ROOT'] . '/assets/uploads/'. $etype .'/'); 
	// 	$config['allowed_types'] = '*';  
	// 	$config['max_size']      = 2048;
	// 	$config['remove_spaces'] = FALSE;

	// 	$response = array();
	// 	if($result == false) {
	// 		$response['result'] = false;
	// 		header("HTTP/1.0 400 Bad Request");
	// 		die('File with this name already exist!');
	// 	}else {

	// 		$this->upload->initialize($config);
	// 		if( ! is_dir($config['upload_path'])) {
	// 			@mkdir($config['upload_path'], 0755, true);
	// 		}
	// 		 if ( ! $this->upload->do_upload('file'))
 //            {
 //                    $error = array('error' => $this->upload->display_errors());
                    
 //                    $response['result'] = $error;
 //            }
 //            else
 //            {

 //                    $data = array('upload_data' => $this->upload->data('file'));
 //                    $response['result'] = $data;
 //            }
	// 	}
	// 	return $response;
	// }
}

/* End of file purchase.php */
/* Location: ./application/controllers/purchase.php */