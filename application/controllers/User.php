<?php

/**
* User controller to handle all the user related tasks
*/
class User extends CI_Controller
{
	public function __construct(){
		parent::__construct();
		$this->load->model('users');
		$this->load->model('companies');
		$this->load->model('purchases');
		$this->load->model('ledgers');
	}

	public function index(){
		redirect('user/dashboard');
	}

	public function savePriviligeGroup()
	{
		// Redirect the user if not logged in
		unauth_secure();

		$privData = $_POST;
		$this->users->savePriviligeGroup($privData);
	}
	public function saveRoleGroup() {

		if (isset($_POST)) {

			$rolegroup = $_POST['data'];
			$result = $this->users->saveRoleGroup( $rolegroup );

			$response = array();
			if ($result === false) {
				$response['error'] = true;
			} else {
				$response['error'] = false;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function dashboard()
	{
		// Redirect the user if not logged in
		 unauth_secure();

		$data['wrapper_class'] = 'dashboard';
		$data['currDate'] = date("F j, Y");
		$data['currDay'] = date("l");
		$login_name = $this->session->userdata('uname');

		if($login_name=='admin'){

			$data['purchases']       = $this->purchases->fetchAllPurchases(1,'purchase');
			$data['purchasereturns']   = $this->purchases->fetchAllPurchases(1,'purchasereturn');
			$data['salereturns'] = $this->purchases->fetchAllPurchases(1,'salereturn');
			$data['sales']           = $this->purchases->fetchAllPurchases(1,'sale');
			// $data['saleOrders']      = $this->sales->fetchAllSales(1,'sale_order');
			$data['paymentss']       = $this->ledgers->fetchAllLedgersPayments(1,'cpv');
			$data['receiptss']       = $this->ledgers->fetchAllLedgersPayments(1,'crv');
			$data['cheqIssues']      = $this->ledgers->fetchAllLedgersPayments(1,'pd_issue');
			$data['chequeReceives']  = $this->ledgers->fetchAllLedgersPayments(1,'pd_receive');
			$data['expensess']  =      $this->ledgers->fetchAllExpenses(1);
		}else{
			$data['purchases']       = [];
			$data['purchasereturns']   = [];
			$data['salereturns'] = [];
			$data['sales']           = [];
			// $data['saleOrders']      = $this->sales->fetchAllSales(1,'sale_order');
			$data['paymentss']       = [];
			$data['receiptss']       = [];
			$data['cheqIssues']      = [];
			$data['chequeReceives']  = [];
			$data['expensess']  = [];
		}
		// $data['companies'] = $this->companies->getAll();

		$this->load->view('template/header', $data);
		$this->load->view('template/mainnav', $data);
		$this->load->view('user/dashboard', $data);
		$this->load->view('template/footer');

	}

	public function userright()
	{


		$this->load->view('template/header');
		$this->load->view('template/mainnav');
		$this->load->view('user/adduserright');
		$this->load->view('template/footer');

	}

	


	public function monitorActiveTime()
	{
		$login_time = $this->session->userdata('login_time');
		$current_time = microtime(true);
		$activeTime = $current_time - $login_time;
		
		// 00:00:00
		$formattedTime = gmdate("H:i:s", $activeTime);

		$parts = explode(':', $formattedTime);

		$finalTimeString = $parts[0] . ' hours and ' . $parts[1] . ' minutes';

		$json = json_encode($finalTimeString);
		echo $json;
	}

	public function priviligeGroup()
	{
		// Redirect the user if not logged in
		unauth_secure();
		if ($this->session->userdata('priviligeGroupAll') === 'false'){
			return;
		}

		$data['wrapper_class'] = 'priviligegroup';
		// $data['maxId'] = $this->users->getMaxPriviligeGroupId() + 1;

		$data['modules'] = array('user/adduser');
		$this->load->view('template/header', $data);
		$this->load->view('template/mainnav', $data);
		$this->load->view('user/privillagesgroup', $data);
		$this->load->view('template/footer');
	}
	public function getMaxRoleGroupId() {

		$result = $this->users->getMaxRoleGroupId() + 1;
		return $this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	public function privillages()
	{
		unauth_secure();
		$data['modules'] = array('user/privillagesgroup');
		$this->load->view('template/header');

		$this->load->view('user/privillagesgroup');

		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function fetchRoleGroup()
	{
		$pgroup_id = $_POST['rgid'];
		$pdata = $this->users->fetchRoleGroup( $pgroup_id );

		$json = json_encode($pdata);
		echo $json;
	}

	public function fetchUser()
	{
		$user_id = $_POST['uid'];
		$user_data = $this->users->fetch($user_id);

		$json = json_encode($user_data);
		echo $json;
	}

	public function valid_username($username){
		if(preg_match("/\\s/", $username)) {
			$this->form_validation->set_message('valid_username', 'Username cannot contain any spaces');
			return false;
		}
		else {
			return true;
		}
	}

	public function _is_unique_curr_user()
	{
		if ($this->session->userdata('uname') === $_POST['uname']) {
			return true;
		} else {
			$username = $_POST['uname'];
			$user_id = $_POST['uid'];

			if( $this->users->alreadyExists($username, $user_id) ) {
				$this->form_validation->set_message('_is_unique_curr_user', 'Username already taken, please chose another one.');
				return false;
			}
			else {
				return true;
			}
		}
	}

	public function _is_unique()
	{
		$username = $_POST['uname'];
		$user_id = $_POST['uid'];

		if( $this->users->alreadyExists($username, $user_id) ) {
			$this->form_validation->set_message('_is_unique', 'Username already taken, please chose another one.');
			return false;
		}
		else {
			return true;
		}
	}

	public function _is_correct_pass()
	{
		$uid = $_POST['uid'];
		$pass = $_POST['pass'];

		if( $this->users->idPassMatch($uid, $pass) ) {
			return true;
		} else {
			$this->form_validation->set_message('_is_correct_pass', 'Invalid old password entered.');
			return false;
		}
	}

	public function addnew()
	{
		// Redirect the user if not logged in
		unauth_secure();
		if ($this->session->userdata('userAll') === 'false'){
			return;
		}

		$data['wrapper_class'] = 'adduser';
		$data['maxid'] = $this->users->getMaxId() + 1;
		$data['currDate'] = date("F j, Y");
		$data['currDay'] = date("l");
		$data['pgroups'] = $this->users->fetchAllRoleGroup();
		
		$this->load->library('form_validation');

		$this->form_validation->set_rules('uid', 'User id', 'required');
		$this->form_validation->set_rules('user_type', 'User type', 'required');
		$this->form_validation->set_rules('uname', 'User Name', 'required|callback_valid_username|callback__is_unique');
		$this->form_validation->set_rules('pass', 'Password', 'required');		
		$this->form_validation->set_rules('company_id', 'Company name', 'required');
		$this->form_validation->set_rules('full_name', 'Full name', 'required');
		$this->form_validation->set_rules('user_email', 'Email', 'required');
		$this->form_validation->set_rules('user_mobile', 'Mobile', 'required');
		$this->form_validation->set_rules('pgroup_id', 'User role', 'required');

		if ($this->form_validation->run() == false) {

			$data['wrapper_class'] = 'adduser';
			$data['maxid'] = $this->users->getMaxId() + 1;
			$data['currDate'] = date("F j, Y");
			$data['currDay'] = date("l");
			$data['companies'] = $this->companies->getAll();
			$data['userone'] = $this->users->fetchAll();

			$data['modules'] = array('user/adduser');
			$this->load->view('template/header', $data);
			$this->load->view('template/mainnav', $data);
			$this->load->view('user/adduser', $data);
			$this->load->view('template/footer');


		} else {
			$this->users->updateUser($_POST);
			redirect('user/addnew');
		}
	}

	public function dateclose()
	{
		// Redirect the user if not logged in
		unauth_secure();
		if ($this->session->userdata('userAll') === 'false'){
			return;
		}

		$data['wrapper_class'] = 'dateclose';
		$data['maxid'] = $this->users->getMaxId() + 1;
		$data['currDate'] = date("F j, Y");
		$data['currDay'] = date("l");
		$data['pgroups'] = $this->users->fetchAllRoleGroup();
		
		$this->load->library('form_validation');

		$this->form_validation->set_rules('uid', 'User id', 'required');
		$this->form_validation->set_rules('user_type', 'User type', 'required');
		$this->form_validation->set_rules('uname', 'User Name', 'required|callback_valid_username|callback__is_unique');
		$this->form_validation->set_rules('pass', 'Password', 'required');		
		$this->form_validation->set_rules('company_id', 'Company name', 'required');
		$this->form_validation->set_rules('full_name', 'Full name', 'required');
		$this->form_validation->set_rules('user_email', 'Email', 'required');
		$this->form_validation->set_rules('user_mobile', 'Mobile', 'required');
		$this->form_validation->set_rules('pgroup_id', 'User role', 'required');

		if ($this->form_validation->run() == false) {

			$data['wrapper_class'] = 'dateclose';
			$data['maxid'] = $this->users->getMaxId() + 1;
			$data['currDate'] = date("F j, Y");
			$data['currDay'] = date("l");
			$data['companies'] = $this->companies->getAll();
			$data['datecloses'] = $this->users->fetchAllDateClose();

			$data['modules'] = array('user/dateclose');
			$this->load->view('template/header', $data);
			$this->load->view('template/mainnav', $data);
			$this->load->view('user/dateclose', $data);
			$this->load->view('template/footer');


		} else {
			$this->users->updateUser($_POST);
			redirect('user/addnew');
		}
	}

	public function getMaxId() {
		if (isset($_POST)) {
		$result = $this->users->getMaxId() + 1;
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	}



public function save() {

		if (isset($_POST)) {

			$user = $_POST['user'];
			$result = $this->users->save($user);
			$response = array();
			if ( $result === false ) {
				$response['error'] = 'true';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function validateDate($date)
	{
	    $sec=strtotime($date);
	    $d = date('Y-m-d', $sec);
	    return $d;
	}

	public function openDate() {

		if (isset($_POST)) {

			$user = $_POST['uid'];
			$newdate = $this->validateDate($_POST['date_cl']);

			$result = $this->users->openDate($user, $newdate);
			$response = array();
			if ( $result === false ) {
				$response['error'] = 'true';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function closeDate() {

		if (isset($_POST)) {

			$newdate = $this->validateDate($_POST['date_cl']);
			$_POST['date_cl'] = $newdate;
			$result = $this->users->closeDate($_POST);
			$response = array();
			if ( $result === false ) {
				$response['error'] = 'true';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}


	public function updateProfile()
	{
		// Redirect the user if not logged in
		unauth_secure();
		/*if ($this->session->userdata('updateProfile') === 'false'){
			return;
		}*/

		$data['uid'] =  $this->session->userdata('uid');
		
		$data['uname'] =  $this->session->userdata('uname');
		$data['full_name'] = $this->session->userdata('full_name');
		$data['user_mobile'] = $this->session->userdata('user_mobile');
		$data['user_email'] = $this->session->userdata('user_email');

		$data['wrapper_class'] = 'updateProfile';
		$data['currDate'] = date("F j, Y");
		$data['currDay'] = date("l");
		
		$this->load->library('form_validation');

		$this->form_validation->set_rules('uid', 'User id', 'required');	
		$this->form_validation->set_rules('uname', 'User Name', 'required|callback_valid_username|callback__is_unique_curr_user');
		$this->form_validation->set_rules('pass', 'Password', 'required|callback__is_correct_pass');
		$this->form_validation->set_rules('full_name', 'Full name', 'required');
		$this->form_validation->set_rules('user_email', 'Email', 'required');
		$this->form_validation->set_rules('user_mobile', 'Mobile', 'required');

		if ($this->form_validation->run() == false) {

			$data['wrapper_class'] = 'updateUserProfile';
			$data['currDate'] = date("F j, Y");
			$data['currDay'] = date("l");

			$this->load->view('template/head', $data);
			$this->load->view('template/navigation', $data);
			$this->load->view('updateProfile', $data);
			$this->load->view('template/foot');
		} else {

			if (trim($_POST['newpass']) !== '') {
				$_POST['pass'] = $_POST['newpass'];
				unset($_POST['newpass']);
			} else {
				unset( $_POST['newpass'] );
			}

			$this->users->updateUser($_POST);
			redirect('user/updateProfile');
		}
	}

	
	public function logout()
	{
		unauth_secure();
		$uname= $this->session->userdata('uname');
		// $msgResult = $this->users->login_user_code_logout($uname );
		$this->session->sess_destroy();	
		redirect('welcome');
	}

	// public function save_db_backup( $action='email' )
	// {
	// 	// No need for this, server would be handling it
	// 	// unauth_secure();

	// 	$DBUSER=$this->db->username;
	// 	$DBPASSWD=$this->db->password;
	// 	$DATABASE=$this->db->database;

	// 	$filename = $DATABASE . "-" . date("Y-m-d_H-i-s") . ".sql.gz";
	// 	$mime = "application/x-gzip";

	// 	// $save_path = $_SERVER['DOCUMENT_ROOT'] . '/application/db_backups/' . $filename;
	// 	$save_path = 'c:/' . $filename;

	// 	// $cmd = "mysqldump -u $DBUSER --password=$DBPASSWD $DATABASE | gzip --best > " . $save_path;
	// 	$cmd = "mysqldump -u $DBUSER --password=$DBPASSWD --no-create-info --complete-insert $DATABASE | gzip --best";

	// 	exec( $cmd );
	// 	passthru( $cmd );

	// 	exit(0);
	// 	// $this->send_mail( $save_path, 'Database Backup - ' . date("d-m-Y_at_H-i"), 'Please find the attached backup file!');
	// 	// echo $cmd;

	// 	// if ($action == 'download') {
	// 	// 	redirect(base_url('/application/db_backups/' . $filename));
	// 	// } else {
	// 	// 	unlink( $save_path );
	// 	// 	exit(0);
	// 	// }
	// }
	public function db_backup()
	{
		if ( $this->session->userdata('db_backup') === 'false' ) {
			return;
		}

		// Redirect the user if not logged in
		unauth_secure();

		$DBUSER=$this->db->username;
		$DBPASSWD=$this->db->password;
		$DATABASE=$this->db->database;

		$filename = $DATABASE . "-" . date("Y-m-d_H-i-s") . ".sql.gz";
		$mime = "application/x-gzip";

		header( "Content-Type: " . $mime );
		header( 'Content-Disposition: attachment; filename="' . $filename . '"' );

		// $cmd = "mysqldump -u $DBUSER --password=$DBPASSWD $DATABASE | gzip --best";   
		$cmd = "mysqldump -u $DBUSER --password=$DBPASSWD --no-create-info --complete-insert $DATABASE | gzip --best";

		passthru( $cmd );

		exit(0);
	}
	

	public function send_mail( $filePath, $subject, $message )
	{
	  $configs = array(
	    'protocol'=>'smtp',
	    'smtp_host'=>'mail.afaqtraders.com',
	    'smtp_user'=>'info@afaqtraders.com',
	    'smtp_pass'=>"info@afaqtraders",
	    'smtp_port'=>'25'
	  );

	  $this->load->library("email", $configs);
	  $this->email->set_newline("\r\n");
	  $this->email->to(array(
	  		'arshadfarouq@hotmail.com',
	  		'info@digitalsofts.com'
	  	));
	  $this->email->from("info@afaqtraders.com", "Afaq Traders");
	  $this->email->subject( $subject );
	  $this->email->message( $message );
	  $this->email->attach( $filePath );
	  if($this->email->send())
	  {
	    // Do nothing
	  }
	  else
	  {
	    echo $this->email->print_debugger();
	  }
	}
	public function getNotifications() {

		if (isset( $_POST )) {

			// $vrnoa = $_POST['vrnoa'];
			$company_id = $_POST['company_id'];	
			$result = $this->users->getNotifications($company_id);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}


}

 ?>