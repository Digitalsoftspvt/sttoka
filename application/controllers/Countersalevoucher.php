<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Countersalevoucher extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('orders');
		$this->load->model('accounts');
		$this->load->model('salesmen');
		$this->load->model('transporters');
		$this->load->model('items');
		$this->load->model('ledgers');
		$this->load->model('purchases');
		$this->load->model('departments');
		$this->load->model('sales');
		$this->load->model('countersales');
		$this->load->model('users');
		$this->load->model('levels');	
	}

	public function index() {
		unauth_secure();

		$data['modules'] = array('sale/addcountersale');
		$data['setting_configur'] = $this->accounts->getsetting_configur();
		$data['parties'] = $this->accounts->fetchAllCashAndBd();
		
		unauth_secure();


		$this->load->view('template/header');
		$this->load->view('sale/countersalevoucher', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function getMaxVrno() {
		if (isset($_POST)) {
			$company_id = $_POST['company_id'];
			$etype = $_POST['etype'];
			$result = $this->countersales->getMaxVrno($etype ,$company_id) + 1;
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}


	public function getMaxVrnoa() {
		if (isset($_POST)) {
			$company_id = $_POST['company_id'];
			$etype = $_POST['etype'];
			$result = $this->countersales->getMaxVrnoa($etype ,$company_id) + 1;
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}
}

/* End of file purchase.php */
/* Location: ./application/controllers/purchase.php */