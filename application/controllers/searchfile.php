<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class searchfile extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('upload');
		// $this->load->model('CheckUserSession');
		$this->load->model('accounts');
		// $this->load->model('addyarns');
		$this->load->model('departments');
		$this->load->model('transporters');
		// $this->load->model('brokers');
		$this->load->model('items');
		$this->load->model('ledgers');
		$this->load->model('salesmen');
		$this->load->model('orders');
		$this->load->model('fileuploaders');
		$this->load->model('purchases');
		$this->load->model('users');
		$this->load->model('levels');
		// if($this->CheckUserSession->check() == FALSE)
        // {
            // die(print('<b>Invalid Url</b>'));
            
        // }

	}

	public function index() {
		unauth_secure();
		$data['modules'] 	= array('contract/searchfile');
		$data['title']  = 'Search File';

		$this->load->view('template/header',$data);
		$this->load->view('contract/searchfile', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function getMaxVrno() {
		if (isset($_POST)) {
		$company_id = $_POST['company_id'];//$_POST['company_id'];
		$result = $this->purchases->getMaxVrno('yarnoutward',$company_id) + 1;
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	}

	public function getMaxVrnoa() {

		if (isset($_POST)) {
			$company_id = $_POST['company_id'];//$_POST['company_id'];
			$result = $this->purchases->getMaxVrnoa('yarnoutward',$company_id) + 1;
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function fetch() {

		if (isset( $_POST )) {
			$searchedTxt = $_POST['searchedTxt'];

			$result = $this->fileuploaders->fetchSearchedFile($searchedTxt);
			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

}

/* End of file purchase.php */
/* Location: ./application/controllers/purchase.php */