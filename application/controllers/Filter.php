<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Filter extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('filters');
	}
	public function fetchAllForComponents()
	{
		if (isset($_POST)) {
			$name  = $_POST['rname'];
			$result = $this->filters->fetchAllForComponents($name);
			$response  =  array();
			if ( $result === false ) {
			$response = 'false';
		} else {			
			$response = $result;
		}

		}
		$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
	}

	public function fetchtablename() {
		if (isset($_POST)) {
		$name = $_POST['rname'];
		$result = $this->filters->fetchfilters($name);
		$filters=array();
		$fil = array();
		if ($result !== false) {
		foreach ($result as $curfilter) 
		{
			$filterdata = $this->filters->fetchfilterdata($curfilter['tablename'],$curfilter['id'],$curfilter['displayvalue'],$curfilter['condition']);
			$fil['classtype']=$curfilter['classtype'];
			$fil['placeholder']=$curfilter['placeholder'];
			$fil['classid']=$curfilter['classid'];
			$fil['filtertype']=$curfilter['filtertype'];
			$fil['filterdata'] = $filterdata; 
			array_push($filters,$fil);
		}
	}

		$response = array();
		if ( $filters === false ) {
			$response = 'false';
		} else {			
			$response = $filters;
		}

		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($response));
		}
	}
	public function getreportdatafromdatabase(){
		$name  = $_POST['name'];
		$data = $this->filters->getreportdatafromdatabase($name);
		$response = array();
		if ( $data === false ) {
			$response = 'false';
		} else {			
			$response = $data;
		}
		$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($data));
	}

}

/* End of file department.php */
/* Location: ./application/controllers/department.php */