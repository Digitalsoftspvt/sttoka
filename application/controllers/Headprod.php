<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time',300);
class Headprod extends CI_Controller {
	public function __construct() {
	        parent::__construct();
	        // $this->load->model('accounts');
	        $this->load->model('items');
	        // $this->load->model('jobs');
	        $this->load->model('departments');
            $this->load->model('purchases');
            $this->load->model('staffdepartments');
            $this->load->model('staffs');
            $this->load->model('ledgers');
            $this->load->model('accounts');
            $this->load->model('datecloses');
	    }

	public function index()
	{
		redirect('headprod/add');
	}

	public function add() {
		$data['modules'] = array('inventory/addheadprod');
        $data['Attachments']    = true;
        $data['tablename']      = 'stockmain';
        $data['etype']          = 'head_production';
        $data['departments'] = $this->departments->fetchAllDepartments();
        $data['staffdepartments'] = $this->staffdepartments->fetchAllDepartments();
        $data['items'] = $this->items->fetchAll();
        $data['accounts'] = $this->accounts->fetchAll();
		//$data['currenceys'] = $this->currenceys->fetchAllCurrencey();
		//$data['userone'] = $this->users->fetchAll();

		$this->load->view('template/header',$data);
		$this->load->view('inventory/addheadprod', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer');
	}

    public function getMaxVrnoa() {
        $company_id = $_POST['company_id'];
        $result = $this->purchases->getMaxVrnoa('head_production',$company_id) + 1;
        $this->output->set_content_type('application/json')
        			->set_output(json_encode($result));
    }

    public function save() {

        if (isset($_POST)) {

            $stockmain = $_POST['stockmain'];
            $stockdetail = json_decode($_POST['stockdetail'],true);
            $ledger =json_decode($_POST['ledger'],true);;
            $voucher_type_hidden = $stockmain['type'];

             ///////////////////// Validation Close Date String
            $response = array();
            
            $chk_date = $_POST['vrdate'];
            $vrdate = "2016-01-01";
            $vrdate = $stockmain['vrdate'];

            $DateCloseStatus=false;
            if($chk_date!=$vrdate &&  $voucher_type_hidden=='edit'){
                $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
            }
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }

            $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
            
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }


            /*$etype = $_POST['etype'];*/

            if ($voucher_type_hidden=='new')
            {
                $vrnoa = $this->purchases->getMaxVrnoa($stockmain['etype'] , $stockmain['company_id']) + 1;
                $stockmain['vrnoa'] = $vrnoa;
            }

            $result = $this->purchases->save($stockmain, $stockdetail, $stockmain['vrnoa'], $stockmain['etype']);
            if ($ledger !=null || $stockmain['etype']='head_production')
            {
                $result = $this->ledgers->save($ledger, $ledger[0]['dcno'], $stockmain['etype'], $voucher_type_hidden);
            }



            $response = array();
            if ( $result === false ) {
                $response['error'] = 'true';
            } else {
                $response = $result;
            }

            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }

    public function fetch() {

        if (isset( $_POST )) {
            $vrnoa = $_POST['vrnoa'];
            $company_id = $_POST['company_id'];
            $result = $this->purchases->fetchProduction($vrnoa, 'head_production',$company_id);
            $response = "";
            if ( $result === false ) {
                $response = 'false';
            } else {
                $response = $result;
            }
            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }

    public function fetchEmployee(){

        if (isset( $_POST )) {
            $vrnoa = $_POST['vrnoa'];
            $company_id = $_POST['company_id'];
            $result = $this->ledgers->fetch($vrnoa, 'head_production',$company_id);
            $response = "";
            if ( $result === false ) {
                $response = 'false';
            } else {
                $response = $result;
            }
            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }

    public function fetch_present_staff(){

        $item_ids = $_POST['item_ids'];
        $itemData = $_POST['item_data'];
        $dept_id = $_POST['dept_id'];
        $phase_id = $_POST['phase_id'];
        $date = $_POST['date'];
        $result = $this->staffs->fetch_present_staff($item_ids, $itemData, $dept_id, $phase_id, $date);

        $response = "";
            if ( $result === false ) {
                $response = 'false';
            } else {
                $response = $result;
            }

            $this->output->set_content_type('application/json')
                        ->set_output(json_encode($response));

    }

    public function delete() {

        if (isset( $_POST )) {

            $vrnoa = $_POST['vrnoa'];
            $company_id = $_POST['company_id'];

              ///////////////////// Validation Close Date String
            $response = array();
            
            $chk_date = $_POST['chk_date'];
            $vrdate = "2016-01-01";
            $vrdate = $_POST['vrdate'];

            $DateCloseStatus=false;
            if($chk_date!=$vrdate){
                $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
            }
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }

            $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
            
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }

            ///////////////////// Validation End

            $result = $this->purchases->delete($vrnoa, 'head_production', $company_id);

            $response = "";
            if ( $result === false ) {

                $response = 'false';
            } else {

                $response = $result;
            }

            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($response));
        }
    }

}

/* End of file headprod.php */
/* Location: ./application/controllers/headprod.php */