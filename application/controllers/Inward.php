<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('max_execution_time',100);
class Inward extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->load->model('accounts');
		$this->load->model('salesmen');
		$this->load->model('transporters');
		$this->load->model('items');
		$this->load->model('ledgers');
		$this->load->model('departments');
		$this->load->model('sales');
		$this->load->model('purchases');
		$this->load->model('orders');
		$this->load->model('users');
		$this->load->model('levels');
		$this->load->model('datecloses');
		
	}

public function fetchChartData()
	{
		$period = $_POST['period'];
		$company_id = $_POST['company_id'];
		$etype = $_POST['etype'];

		$data = $this->purchases->fetchChartData($period, $company_id,$etype);

		$json = json_encode($data);
		echo $json;
	}

	public function  outward(){

		unauth_secure();

		$data['modules'] = array('sale/outward');
		$data['parties'] = $this->accounts->fetchAll(1,'all');
		$data['vehicles'] = $this->purchases->fetchByCol_inward('prepared_by');
		$data['salesmen'] = $this->salesmen->fetchAll();
		$data['receivers'] = $this->purchases->fetchByCol('received_by');
		$data['transporters'] = $this->transporters->fetchAll();
		$data['departments'] = $this->departments->fetchAllDepartments();
		//$data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();
		$data['setting_configur'] = $this->accounts->getsetting_configur();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['types'] = $this->items->fetchByCol('barcode');
		// $data['orders_running'] = $this->orders->fetchOrders(date("Y/m/d"), date("Y/m/d"), 'running_loading');
		$data['uoms'] = $this->items->fetchByCol('uom');


		$this->load->view('template/header');
		$this->load->view('sale/outward', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function index() {
			unauth_secure();

		$data['modules'] = array('sale/inward');
		$data['parties'] = $this->accounts->fetchAll(1,'all');
		$data['salesmen'] = $this->salesmen->fetchAll();
		$data['receivers'] = $this->purchases->fetchByCol('received_by');
		$data['vehicles'] = $this->purchases->fetchByCol_inward('prepared_by');
		

		$data['transporters'] = $this->transporters->fetchAll();
		$data['departments'] = $this->departments->fetchAllDepartments();
		//$data['items'] = $this->items->fetchAll(1);
		$data['userone'] = $this->users->fetchAll();
		$data['setting_configur'] = $this->accounts->getsetting_configur();
		$data['l3s'] = $this->levels->fetchAllLevel3();
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['types'] = $this->items->fetchByCol('barcode');
		// $data['orders_running'] = $this->orders->fetchOrders(date("Y/m/d"), date("Y/m/d"), 'running_loading');
		$data['uoms'] = $this->items->fetchByCol('uom');


		$this->load->view('template/header');
		$this->load->view('sale/inward', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

public function fetchLfiveRates() {

		if (isset( $_POST )) {

			$item_id = $_POST['item_id'];
			$company_id = $_POST['company_id'];	
			$etype = $_POST['etype'];
			$crit = $_POST['crit'];	
			// $result = $this->orders->fetchLfiveRates($item_id,$etype,$company_id);
			$result = $this->purchases->last_5_srate($item_id,$etype,$company_id,$crit);

			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

public function fetchNetSum()
	{
		$company_id = $_POST['company_id'];
		$etype = $_POST['etype'];
		$sum = $this->purchases->fetchNetSum( $company_id ,$etype);
		
		$json = json_encode($sum);
		echo $json;
	}


	public function getMaxVrno() {
		if (isset($_POST)) {
		$company_id = $_POST['company_id'];
		$etype = $_POST['etype'];
		$result = $this->purchases->getMaxVrno($etype,$company_id) + 1;
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	}

	public function getMaxVrnoa() {
		if (isset($_POST)) {
			$company_id = $_POST['company_id'];
			$etype = $_POST['etype'];
			$result = $this->purchases->getMaxVrnoa($etype,$company_id) + 1;
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function save() {

		if (isset($_POST)) {

			// $ledger = $_POST['ledger'];
			$stockmain = $_POST['stockmain'];
			$stockdetail = $_POST['stockdetail'];
			$vrnoa = $_POST['vrnoa'];
			$etype = $_POST['etype'];
			$voucher_type_hidden = $_POST['voucher_type_hidden'];

			 ///////////////////// Validation Close Date String
	        $response = array();
	        
	        $chk_date = $_POST['vrdate'];
	        $vrdate = "2016-01-01";
	        $vrdate = $stockmain['vrdate'];

	        $DateCloseStatus=false;
	        if($chk_date!=$vrdate &&  $voucher_type_hidden=='edit'){
	            $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
	        }
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
	        
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        ///////////////////// Validation End

			if ($voucher_type_hidden=='new'){
				
				$vrnoa = $this->purchases->getMaxVrnoa($etype , $stockmain['company_id']) + 1;
				$stockmain['vrnoa'] = $vrnoa;
			}

			if($etype == 'outward'){

				$ledger = $_POST['ledger'];
				$result = $this->ledgers->save($ledger, $vrnoa, $etype,$voucher_type_hidden);
			}
			
			$result = $this->purchases->save($stockmain, $stockdetail, $vrnoa, $etype);
			$response = array();
			if ( $result === false ) {
				$response['error'] = 'true';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetch() {

		if (isset( $_POST )) {

			$vrnoa = $_POST['vrnoa'];
			$company_id = $_POST['company_id'];	
			$etype = $_POST['etype'];	
			$result = $this->purchases->fetch($vrnoa, $etype ,$company_id);
			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}
			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function delete() {

		if (isset( $_POST )) {

			$vrnoa = $_POST['vrnoa'];
			$etype = $_POST['etype'];
			$company_id = $_POST['company_id'];

			  ///////////////////// Validation Close Date String
	        $response = array();
	        
	        $chk_date = $_POST['chk_date'];
	        $vrdate = "2016-01-01";
	        $vrdate = $_POST['vrdate'];

	        $DateCloseStatus=false;
	        if($chk_date!=$vrdate){
	            $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
	        }
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
	        
	        if($DateCloseStatus==true){
	            $response['error'] = 'date close';
	            return $this->output->set_content_type('application/json')->set_output(json_encode($response));
	        }

	        ///////////////////// Validation End

			$result = $this->purchases->delete($vrnoa, $etype,$company_id);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchPurchaseReportData()
	{
		$what = $_POST['what'];
		$startDate = $_POST['from'];
		$endDate = $_POST['to'];
		$type = $_POST['type'];
		$etype = $_POST['etype'];
		$company_id = $_POST['company_id'];
		$field = $_POST['field'];
		$crit = $_POST['crit'];
		$orderBy = $_POST['orderBy'];
		$groupBy = $_POST['groupBy'];
		$name = $_POST['name'];

		$sreportData = $this->purchases->fetchPurchaseReportData($startDate, $endDate, $what, $type, $company_id, $etype,$field,$crit,$orderBy,$groupBy,$name);
		$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($sreportData));
	}

	public function fetchImportRangeSum()
	{
		$from = $_POST['from'];
		$to = $_POST['to'];

		$sum = $this->purchases->fetchImportRangeSum( $from, $to );

		$json = json_encode($sum);
		echo $json;
	}

	public function fetchRangeSum()
	{
		$from = $_POST['from'];
		$to = $_POST['to'];

		$sum = $this->purchases->fetchRangeSum( $from, $to );

		$json = json_encode($sum);
		echo $json;
	}
}

/* End of file purchase.php */
/* Location: ./application/controllers/purchase.php */