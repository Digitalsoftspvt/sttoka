<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Userright extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('currenceys');
		$this->load->model('users');
		$this->load->model('userrights');
	}

	public function index() {
		redirect('currency/add');
	}

	public function add() {
		$data['modules'] = array('user/adduserright');
		$data['levels'] = $this->userrights->fetchAllLevel1();
		//$data['levelss'] = $this->userrights->fetchAllLevels();
		$data['users'] = $this->userrights->fetchAllUsers();

		$this->load->view('template/header');
		$this->load->view('user/adduserright', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function getMaxCurrenceyId() {
		$result = $this->currenceys->getMaxCurrenceyId() + 1;
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function save() {

		if (isset($_POST))
        {
            $userId = $_POST['user_id'];
			$rights = $_POST['rights'];

			$result = $this->userrights->save($userId, $rights);

			$response = array();
			if ($result === false) {
				$response['error'] = true;
			} else {
				$response['error'] = false;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchCurrencey() {

		if (isset( $_POST )) {

			$id = $_POST['id'];
			$result = $this->currenceys->fetchCurrencey($id);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchAllCurrencey() {

		$result = $this->currenceys->fetchAllCurrencey();

		$response = array();
		if ( $result === false ) {
			$response = 'false';
		} else {			
			$response = $result;
		}

		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($response));
	}

    public function fetchlevel2()
    {
        if (isset($_POST))
        {
            $userId = $_POST['user_id'];
            $l1 = $_POST['l1'];

            $result = $this->userrights->fetchLevel2($userId, $l1);

            $response = "";
            if ( $result === false )
            {
                $response = 'false';
            }
            else
            {
                $response = $result;
            }

            $this->output->set_content_type('application/json')->set_output(json_encode($response));
        }
    }
}

/* End of file currency.php */
/* Location: ./application/controllers/currency.php */