<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stocknavigation extends CI_Controller {

	public function __construct() {
        parent::__construct();        
        $this->load->model('accounts');
        $this->load->model('items');
        $this->load->model('jobs');
        $this->load->model('departments');
        $this->load->model('purchases');
        $this->load->model('users');
        $this->load->model('ledgers');
        $this->load->model('transporters');
        $this->load->model('datecloses');
    }

    public function index() {
        redirect('stocknavigation/add');
    }

    public function add() {
    	//unauth_secure();
        $data['modules'] = array('inventory/addstocknavigation');
        $data['Attachments']    = true;
        $data['tablename']      = 'stockmain';
        $data['etype']          = 'navigation';
        $data['parties'] = $this->accounts->fetchAll();
        $data['receivers'] = $this->purchases->fetchByCol('received_by');
        $data['issuers'] = $this->purchases->fetchByCol('issued_by');
        $data['departments'] = $this->departments->fetchAllDepartments();
        $data['transporter'] = $this->transporters->fetchAll();
        //$data['items'] = $this->items->fetchAll();
        $data['jobs'] = $this->jobs->fetchAllJobs('job_order');
        $data['userone'] = $this->users->fetchAll();
        $data['routes'] = $this->items->fetchAllRoutes();
        $data['accountCashs'] = $this->accounts->fetchAll(1,'cashslip_cash');

        $this->load->view('template/header',$data);
        $this->load->view('inventory/addstocknavigation', $data);
        $this->load->view('template/mainnav');
        $this->load->view('template/footer', $data);
    }

    public function getMaxVrno() {
        $company_id = $_POST['company_id'];
        $result = $this->purchases->getMaxVrno('navigation', $company_id) + 1;
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function getMaxVrnoa() {
        $company_id = $_POST['company_id'];
        $result = $this->purchases->getMaxVrnoa('navigation', $company_id) + 1;
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function save() {

        if (isset($_POST)) {

            $ledger = json_decode($_POST['ledger'], true);
            $stockmain = json_decode($_POST['stockmain'], true);
            $stockdetail = json_decode($_POST['stockdetail'], true);
            $vrnoa = $_POST['vrnoa'];
            $voucher_type_hidden = $_POST['voucher_type_hidden'];

             ///////////////////// Validation Close Date String
            $response = array();
            
            $chk_date = $_POST['vrdate'];
            $vrdate = "2016-01-01";
            $vrdate = $stockmain['vrdate'];

            $DateCloseStatus=false;
            if($chk_date!=$vrdate &&  $voucher_type_hidden=='edit'){
                $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
            }
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }

            $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
            
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }

            ///////////////////// Validation End

            if ($voucher_type_hidden=='new'){
                
                $vrnoa = $this->purchases->getMaxVrnoa('navigation' , $stockmain['company_id']) + 1;
                $stockmain['vrnoa'] = $vrnoa;
            }
            
            $result = $this->purchases->save($stockmain, $stockdetail, $vrnoa, 'navigation');
            if($result){
                $result = $this->ledgers->save($ledger, $vrnoa, 'navigation',$voucher_type_hidden);
            }

            $response = array();
            if ( $result === false ) {
                $response['error'] = 'true';
            } else {

                $response['result'] = $result;
                $response['vrnoa']  = $vrnoa;
                //$response = $result;
            }

            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }

    public function fetch() {

        if (isset( $_POST )) {

            $vrnoa = $_POST['vrnoa'];
            $company_id = $_POST['company_id'];
            $result = $this->purchases->fetchNavigation($vrnoa, 'navigation',$company_id);

            $response = "";
            if ( $result === false ) {
                $response = 'false';
            } else {
                $response = $result;
            }

            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }

    public function delete() {

        if (isset( $_POST )) {

            $vrnoa = $_POST['vrnoa'];
            $company_id = $_POST['company_id'];

              ///////////////////// Validation Close Date String
            $response = array();
            
            $chk_date = $_POST['chk_date'];
            $vrdate = "2016-01-01";
            $vrdate = $_POST['vrdate'];

            $DateCloseStatus=false;
            if($chk_date!=$vrdate){
                $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
            }
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }

            $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
            
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }

            ///////////////////// Validation End

            $result = $this->purchases->delete($vrnoa, 'navigation',$company_id);

            $response = "";
            if ( $result === false ) {
                $response = 'false';
            } else {
                $response = $result;
            }

            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }

}

/* End of file stocknavigation.php */
/* Location: ./application/controllers/stocknavigation.php */