<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Item extends CI_Controller {


	public function __construct() {
		parent::__construct();
		$this->load->model('accounts');
		$this->load->model('items');
		$this->load->model('companies');
	}

	public function index() {
		unauth_secure();
		$data['modules'] = array('setup/additem');
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['uoms'] = $this->items->fetchByCol('uom');
		$data['types'] = $this->items->fetchByCol('barcode');
		//$data['parties'] = $this->accounts->fetchAll();
		$data['items'] = $this->items->fetchAll();
		$data['phases'] = $this->items->fetchAllPhase();

		$this->load->view('template/header');
		$this->load->view('setup/additem', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function updateitemrate() {
		unauth_secure();
		$data['modules'] = array('setup/updateitemrate');
		$data['categories'] = $this->items->fetchAllCategories('catagory');
		$data['subcategories'] = $this->items->fetchAllSubCategories('sub_catagory');
		$data['brands'] = $this->items->fetchAllBrands();
		$data['uoms'] = $this->items->fetchByCol('uom');
		$data['types'] = $this->items->fetchByCol('barcode');
		//$data['parties'] = $this->accounts->fetchAll();
		$data['items'] = $this->items->fetchAll();
		$data['phases'] = $this->items->fetchAllPhase();

		$this->load->view('template/header');
		$this->load->view('setup/updateitemrate', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}


	public function ChartOfItems()
	{
		$data['modules'] = array('reports/inventory/ChartOfItems');

		$this->load->view('template/header');
		$this->load->view('reports/inventory/ChartOfItemsv');
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}


	public function recipeCosting() {
		unauth_secure();
		$data['modules'] = array('setup/addrecipe');
		$data['items'] = $this->items->fetchAll();
		// $data['finishitems'] = $this->items->fetchAllFinishItems();
		// $data['recipes'] = $this->items->fetchAllRecipes();

		$this->load->view('template/header');
		$this->load->view('setup/addrecipe', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function getMaxId() {

		$result = $this->items->getMaxId() + 1;
		return $this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function getMaxCatId() {

		$result = $this->items->getMaxCatId() + 1;
		return $this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function getMaxSubCatId() {

		$result = $this->items->getMaxSubCatId() + 1;
		return $this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function getMaxBrandId() {
		$result = $this->items->getMaxBrandId() + 1;
		return $this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function getMaxRouteId() {
		$result = $this->items->getMaxRouteId() + 1;
		return $this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function getMaxRecipeId() {
		$result = $this->items->getMaxRecipeId() + 1;
		return $this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function getAllItems() {
		$crit = $_POST['q'];
		$result = $this->items->fetchAllItem($crit);
		return $this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function category() {
		$data['modules'] = array('setup/category');
		$data['categories'] = $this->items->fetchAllCategories();

		$this->load->view('template/header');
		$this->load->view('setup/addcategory', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function subcategory() {
		$data['modules'] = array('setup/subcategory');
		$data['subcategories'] = $this->items->fetchAllSubCategories();
		$data['categories'] = $this->items->fetchAllCategories();

		$this->load->view('template/header');
		$this->load->view('setup/addsubcategory', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function brand() {
		$data['modules'] = array('setup/brand');
		$data['brands'] = $this->items->fetchAllBrands();

		$this->load->view('template/header');
		$this->load->view('setup/addbrand', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}
	public function route() {
		$data['modules'] = array('setup/route');
		$data['routes'] = $this->items->fetchAllRoutes();

		$this->load->view('template/header');
		$this->load->view('setup/addroute', $data);
		$this->load->view('template/mainnav');
		$this->load->view('template/footer', $data);
	}

	public function save() {

		if (isset($_POST)) {

			$result = $this->items->save($_POST);

			$response = array();
			if ($result == false) {
				$response['error'] = true;
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode('true'));
		}
	}
	public function updateitemsRate() {

		if (isset($_POST)) {
			
			$itemDetail = json_decode($_POST['item_detail'],true);
			$itemDetail = (array)$itemDetail;
			$status = $_POST['status'];
			$updateItemHistory = $_POST['update_item_history'];
		    $result = $this->items->updateItemsByIds($itemDetail, $status, $updateItemHistory);

			$response = array();
			if ($result == false) {
				$response['error'] = true;
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode('true'));
		}
	}


	public function saveCategory() {

		if (isset($_POST)) {

			$category = $_POST['category'];
			$error = $this->items->isCategoryAlreadySaved($category);

			if (!$error) {

				$result = $this->items->saveCategory($category);
				$this->output
					 ->set_content_type('application/json')
					 ->set_output(json_encode('true'));
			} else {
				$this->output
					 ->set_content_type('application/json')
					 ->set_output(json_encode('duplicate'));
			}
		}
	}

	
	public function saveSubCategory() {

		if (isset($_POST)) {

			$category = $_POST['category'];
			$error = $this->items->isSubCategoryAlreadySaved($category);

			if (!$error) {

				$result = $this->items->saveSubCategory($category);
				$this->output
					 ->set_content_type('application/json')
					 ->set_output(json_encode('true'));
			} else {
				$this->output
					 ->set_content_type('application/json')
					 ->set_output(json_encode('duplicate'));
			}
		}
	}

	public function saveBrand() {

		if (isset($_POST)) {

			$brand = $_POST['brand'];
			$error = $this->items->isBrandAlreadySaved($brand);

			if (!$error) {

				$result = $this->items->saveBrand($brand);
				$this->output
					 ->set_content_type('application/json')
					 ->set_output(json_encode('true'));
			} else {
				$this->output
					 ->set_content_type('application/json')
					 ->set_output(json_encode('duplicate'));
			}
		}
	}

	public function saveRoute() {

		if (isset($_POST)) {

			$route = $_POST['route'];
			$error = $this->items->isRouteAlreadySaved($route);

			if (!$error) {

				$result = $this->items->saveRoute($route);
				$this->output
					 ->set_content_type('application/json')
					 ->set_output(json_encode('true'));
			} else {
				$this->output
					 ->set_content_type('application/json')
					 ->set_output(json_encode('duplicate'));
			}
		}
	}

	public function saveRecipe() {

		if (isset($_POST)) {

			$recipe = $_POST['recipe'];
			$recipedetail = $_POST['recipedetail'];
			$rid = $_POST['rid'];
			
			if($_POST['voucher_type'] == 'new'){

				$rid = $this->items->getMaxRecipeId() + 1;
			}

			if ($this->items->recipeExists($rid, $recipe['item_id'])) {

				$response['error'] = 'duplicate';
			} else {				
				
				$result = $this->items->saveRecipe($recipe, $recipedetail, $rid);
				$response = array();
				if ( $result === false ) {
					$response['error'] = 'true';
				} else {
					$response = $result;
				}
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetch() {

		if (isset( $_POST )) {

			$item_id = $_POST['item_id'];
			$result = $this->items->fetch($item_id);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	

	public function fetchItemByBrand() {

		if (isset( $_POST )) {

			$crit = $_POST['crit'];
			$result = $this->items->fetchItemByBrand($crit);
			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	
	public function fetchAll(){
		$activee=(isset($_POST['active'])? $_POST['active']:-1);
		$result = $this->items->fetchAll();
		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($result));
	}

	public function fetchAll_Items() {

		if (isset( $_POST )) {

			$from = $_POST['from'];
			$to = $_POST['to'];
			$orderby = $_POST['orderby'];
			$status = $_POST['status'];
			$result = $this->items->fetchAll_report($from,$to,$orderby,$status);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}
			$json = json_encode($response);
			echo $json;
		}
	}


	public function fetchCategory() {

		if (isset( $_POST )) {

			$catid = $_POST['catid'];
			$result = $this->items->fetchCategory($catid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchSubCategory() {

		if (isset( $_POST )) {

			$subcatid = $_POST['subcatid'];
			$result = $this->items->fetchSubCategory($subcatid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchBrand() {

		if (isset( $_POST )) {

			$bid = $_POST['bid'];
			$result = $this->items->fetchBrand($bid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchRoute() {

		if (isset( $_POST )) {

			$route_id = $_POST['route_id'];
			$result = $this->items->fetchRoute($route_id);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchItemOpeningStock()
	{
		$to = $_POST['to'];
		$item_id = $_POST['item_id'];
		$godown_id = $_POST['godown_id'];
		$company_id = $_POST['company_id'];

		$openingStock = $this->items->fetchItemOpeningStock($to, $item_id, $company_id , $godown_id);
		
		$json = json_encode($openingStock);
		echo $json;
	}

	public function fetchItemLedgerReport() {

		if (isset( $_POST )) {

			$item_id = $_POST['item_id'];
			$godown_id = $_POST['godown_id'];
			$from = $_POST['from'];
			$to = $_POST['to'];
			$company_id = $_POST['company_id'];
			$result = $this->items->fetchItemLedgerReport($from, $to, $item_id ,$company_id ,$godown_id);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchRecipe() {

		if (isset( $_POST )) {
			$rid = $_POST['rid'];
			$result = $this->items->fetchRecipe($rid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function deleteRecipe() {

		if (isset( $_POST )) {
			$rid = $_POST['rid'];
			$result = $this->items->deleteRecipe($rid);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}
	public function stockNotifications()
		{
			unauth_secure();

			$data['wrapper_class'] = "stock_notifs";
			$data['page'] = "stock_notifs";

			$data['currdate'] = date("Y/m/d");
			$data['companies'] = $this->companies->getAll();
			$data['notifications'] = $this->items->getMinStockNotifs( $this->session->userdata('company_id') );
			$data['company_id'] = $this->session->userdata('company_id');

			$this->load->view('template/header', $data);
			$this->load->view('reports/inventory/stocknotifications', $data);
			$this->load->view('template/mainnav', $data);
			$this->load->view('template/footer');
		}
	public function fetchStockOrderCount()
	{
		unauth_secure();

		$company_id = $_POST['company_id'];
		$stockOrderCount = $this->items->fetchStockOrderCount( $company_id );
		
		echo $stockOrderCount;
	}

	public function paging() {

		if (isset( $_POST )) {

			$page_number = $_POST['page_number'];
            $search = $_POST['search'];
			$result['total'] = $this->items->total_records($search);
			$result['records'] = $this->items->paging($page_number, $search);

			$response = "";
			if ( $result['total'] === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}
            $result = json_encode($response);
			echo $this->replace($result);
		}
	}

    public function getiteminfo()
    {
        if (isset( $_POST ))
        {
            $itemId = $_POST['item_id'];

            $result['records'] = $this->items->getIteminfo($itemId);

            $response = "";
            if($result['records'] === false) {
                $response = 'false';
            } else {
                $response = $result;
            }
            $result = json_encode($response);
            echo $this->replace($result);
        }
    }

    public function searchItem()
    {
        $search = $_POST['search'];

        $result = $this->items->searchItem($search);
        $result = json_encode($result);
        //die(print_r($this->replace($result)));
        //echo $this->replace($result);
    	echo $result;
    }

    public function getItemInfoById()
    {
        $itemId = trim($_POST['item_id']);

        $result = $this->items->getItemInfoById($itemId);

        $response = "";
        if($result === false) {
            $response = 'false';
        } else {
            $response = $result;
        }
        $result = json_encode($response);
        //echo $this->replace($result);
    	echo $result;
    }

    public function replace($json)
    {
        $json = str_replace('\r\n','',$json);
        $json = str_replace('<.*?>','',$json);
        $json = str_replace('\\','',$json);
        $json = str_replace('\/','/',$json);
        $json = str_replace('null','""',$json);
        $json = str_replace('\n','',$json);
        $json = str_replace('</br>','',$json);
        $json = str_replace('u2013','-',$json);
        return $json;
    }

    public function fetchreceipereportdata()
    {
    	$crit= $_POST['crit'];
        $sreportData = $this->items->fetchreceipereportdata($crit);
        $this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($sreportData));
    }
	
}

/* End of file item.php */
/* Location: ./application/controllers/item.php */