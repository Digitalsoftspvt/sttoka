<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Opening_balance extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('accounts');
        $this->load->model('salesmen');
        $this->load->model('transporters');
        $this->load->model('items');
        $this->load->model('ledgers');
        $this->load->model('departments');
        $this->load->model('sales');
        $this->load->model('purchases');
        $this->load->model('users');
        $this->load->model('levels');
        $this->load->model('orders');
        $this->load->model('datecloses');
    }

    public function index()
    {

        $data['modules'] = array('accounts/addopeningbalance');
        $data['l3s'] = $this->levels->fetchAllLevel3();
        $data['accounts'] = $this->accounts->fetchAll();
        $data['userone'] = $this->users->fetchAll();


        $this->load->view('template/header');
        $this->load->view('accounts/addopeningbalance', $data);
        $this->load->view('template/mainnav');
        $this->load->view('template/footer', $data);
    }

    public function add()
    {

    }

    public function getMaxId()
    {
        $company_id = $_POST['company_id'];
        $maxId = $this->ledgers->getMaxId('openingbalance', $company_id) + 1;
        $this->output->set_content_type('application/json')->set_output(json_encode($maxId));
    }

    public function save()
    {
        $saveObj = json_decode($_POST['saveObj'], true);
        $dcno = $_POST['dcno'];
        $etype = $_POST['etype'];
        $voucher_type_hidden = $_POST['voucher_type_hidden'];

         ///////////////////// Validation Close Date String
            $response = array();
            
            $chk_date = $_POST['vrdate'];
            $vrdate = "2016-01-01";
            $vrdate = $_POST['chk_date'];

            $DateCloseStatus=false;
            if($chk_date!=$vrdate &&  $voucher_type_hidden=='edit'){
                $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
            }
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }

            $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
            
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }

            ///////////////////// Validation End

        $result = $this->ledgers->save($saveObj, $dcno, $etype, $voucher_type_hidden);
        $response = array();
        if ($result === false)
        {
            $response['error'] = 'true';
        }
        else
        {
            $response['error'] = 'false';
        }
        $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
    }

    public function fetch()
    {
        $dcno = $_POST['dcno'];
        $company_id = $_POST['company_id'];
        $result = $this->ledgers->fetch($dcno, 'openingbalance', $company_id);

        $response = "";
        if ($result === false)
        {
            $response = 'false';
        }
        else
        {
            $response = $result;
        }
        $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
    }

    public function deleteVoucher()
    {
        $dcno = $_POST['dcno'];
        $company_id = $_POST['company_id'];

          ///////////////////// Validation Close Date String
            $response = array();
            
            $chk_date = $_POST['chk_date'];
            $vrdate = "2016-01-01";
            $vrdate = $_POST['vrdate'];

            $DateCloseStatus=false;
            if($chk_date!=$vrdate){
                $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
            }
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }

            $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
            
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }

            ///////////////////// Validation End

        $result = $this->ledgers->deleteVoucher($dcno, 'openingbalance', $company_id);

        $response = "";
        if ($result === false)
        {
            $response = 'false';
        }
        else
        {
            $response = 'true';
        }
        $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
    }

    public function fetchVoucherRange()
    {
        $from = $_POST['from'];
        $to = $_POST['to'];

        $result = $this->ledgers->fetchVoucherRange($from, $to, 'openingbalance');
        $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($result));
    }
}
