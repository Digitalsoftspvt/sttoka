<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sparepart extends CI_Controller {

	public function __construct() {
        parent::__construct();        
        $this->load->model('accounts');
        $this->load->model('items');
        $this->load->model('orders');
        $this->load->model('jobs');
        $this->load->model('departments');
        $this->load->model('ledgers');
        $this->load->model('phases');
        $this->load->model('datecloses');
    }

    public function index() {
        unauth_secure();
        $data['modules'] = array('inventory/addsparepart');
        $data['Attachments']    = true;
        $data['tablename']      = 'ordermain';
        $data['etype']          = 'spare_parts';
        $data['parties'] = $this->accounts->fetchAll();
        //$data['employees'] = $this->accounts->fetchAllEmployee();
        $data['employees'] = $this->accounts->fetchAllAssetEmployee();
        $data['expenseAccount'] = $this->accounts->fetchAll_ExpAccountByLevelOne();
        $data['phases'] = $this->phases->fetchAllPhases();
        $data['receivers'] = $this->orders->fetchByCol('received_by');
        $data['departments'] = $this->departments->fetchAllDepartments();
        $data['items'] = $this->items->fetchAll();
        $data['jobs'] = $this->jobs->fetchAllJobs('job_order');

        $this->load->view('template/header',$data);
        $this->load->view('inventory/addsparepart', $data);
        $this->load->view('template/mainnav');
        $this->load->view('template/footer', $data);
    }

    public function add() {
       
    }

    public function getMaxVrno() {
        $company_id = $_POST['company_id'];
        $result = $this->orders->getMaxVrno('spare_parts',$company_id) + 1;
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function getMaxVrnoa() {
        $company_id = $_POST['company_id'];
        $result = $this->orders->getMaxVrnoa('spare_parts',$company_id) + 1;
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function save() {

        if (isset($_POST)) {

            $stockmain = $_POST['stockmain'];
            $stockdetail = $_POST['stockdetail'];
            $vrnoa = $_POST['vrnoa'];
            $voucher_type_hidden = $_POST['voucher_type_hidden'];
            $ledger = $_POST['ledger'];

            if ($voucher_type_hidden=='new'){

                $vrnoa = $this->orders->getMaxVrnoa('spare_parts', $stockmain['company_id']) + 1;
                $stockmain['vrnoa'] = $vrnoa;
            }
            
              ///////////////////// Validation Close Date String
            $response = array();
            
            $chk_date = $_POST['vrdate'];
            $vrdate = "2016-01-01";
            $vrdate = $stockmain['vrdate'];

            $DateCloseStatus=false;
            if($chk_date!=$vrdate &&  $voucher_type_hidden=='edit'){
                $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
            }
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }

            $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
            
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }

            ///////////////////// Validation End        

            $result = $this->orders->save($stockmain, $stockdetail, $vrnoa, 'spare_parts');
            if($result){
                
                $result = $this->ledgers->save($ledger, $vrnoa, 'spare_parts' ,$voucher_type_hidden);
            }

            $response = array();
            if ( $result === false ) {
                $response['error'] = 'true';
            } else {
                $response = $result;
            }

            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }

    public function fetch() {

        if (isset( $_POST )) {
            $vrnoa = $_POST['vrnoa'];
            $company_id = $_POST['company_id'];
            $result = $this->orders->fetchSparParts($vrnoa, 'spare_parts',$company_id);
            $response = "";
            if ( $result === false ) {
                $response = 'false';
            } else {
                $response = $result;
            }
            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }

    public function delete() {

        if (isset( $_POST )) {

            $vrnoa = $_POST['vrnoa'];
            $company_id = $_POST['company_id'];

              ///////////////////// Validation Close Date String
            $response = array();
            
            $chk_date = $_POST['chk_date'];
            $vrdate = "2016-01-01";
            $vrdate = $_POST['vrdate'];

            $DateCloseStatus=false;
            if($chk_date!=$vrdate){
                $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
            }
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }

            $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
            
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }

            ///////////////////// Validation End

            $result = $this->orders->delete($vrnoa, 'spare_parts',$company_id);

            $response = "";
            if ( $result === false ) {
                $response = 'false';
            } else {
                $response = $result;
            }

            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }
}

/* End of file spareparts.php */
/* Location: ./application/controllers/spareparts.php */