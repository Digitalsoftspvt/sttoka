<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Syncdatabase extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('sync_database');
    }

    public function index()
    {
        unauth_secure();
        $data['modules'] = array('syncdatabase/syncdatabase');

        $this->load->view('template/header.php');
        $this->load->view('syncdatabase/syncdatabase.php', $data);
        $this->load->view('template/mainnav.php');
        $this->load->view('template/footer.php', $data);
    }
    public function syncdb()
    {
        if (isset($_POST)) {

            $result = $this->sync_database->sync_db();
            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($result));
        }
    }
}