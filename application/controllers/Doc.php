<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Doc extends CI_Controller
{
  function __construct()
  {
    parent::__construct();

    $this->load->library('pdf'); // Load library
    $this->pdf->fontpath = 'font'; // Specify font folder

    $this->load->model('accounts');
    $this->load->model('ledgers');
    $this->load->model('payments');
    $this->load->model('purchases');
    $this->load->model('stocks');
    $this->load->model('items');
    $this->load->model('orders');
    $this->load->model('ItemphaseRecepies');
  }

  public function convert_number_to_words($number)
  {

    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
      0                   => 'zero',
      1                   => 'one',
      2                   => 'two',
      3                   => 'three',
      4                   => 'four',
      5                   => 'five',
      6                   => 'six',
      7                   => 'seven',
      8                   => 'eight',
      9                   => 'nine',
      10                  => 'ten',
      11                  => 'eleven',
      12                  => 'twelve',
      13                  => 'thirteen',
      14                  => 'fourteen',
      15                  => 'fifteen',
      16                  => 'sixteen',
      17                  => 'seventeen',
      18                  => 'eighteen',
      19                  => 'nineteen',
      20                  => 'twenty',
      30                  => 'thirty',
      40                  => 'fourty',
      50                  => 'fifty',
      60                  => 'sixty',
      70                  => 'seventy',
      80                  => 'eighty',
      90                  => 'ninety',
      100                 => 'hundred',
      1000                => 'thousand',
      1000000             => 'million',
      1000000000          => 'billion',
      1000000000000       => 'trillion',
      1000000000000000    => 'quadrillion',
      1000000000000000000 => 'quintillion'
    );

    if (!is_numeric($number)) {
      return false;
    }

    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
      // overflow
      trigger_error(
        'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
        E_USER_WARNING
      );
      return false;
    }

    if ($number < 0) {
      return $negative . convert_number_to_words(abs($number));
    }

    $string = $fraction = null;

    if (strpos($number, '.') !== false) {
      list($number, $fraction) = explode('.', $number);
    }

    switch (true) {
      case $number < 21:
        $string = $dictionary[$number];
        break;
      case $number < 100:
        $tens   = ((int) ($number / 10)) * 10;
        $units  = $number % 10;
        $string = $dictionary[$tens];
        if ($units) {
          $string .= $hyphen . $dictionary[$units];
        }
        break;
      case $number < 1000:
        $hundreds  = $number / 100;
        $remainder = $number % 100;
        $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
        if ($remainder) {
          $string .= $conjunction . $this->convert_number_to_words($remainder);
        }
        break;
      default:
        $baseUnit = pow(1000, floor(log($number, 1000)));
        $numBaseUnits = (int) ($number / $baseUnit);
        $remainder = $number % $baseUnit;
        $string = $this->convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
        if ($remainder) {
          $string .= $remainder < 100 ? $conjunction : $separator;
          $string .= $this->convert_number_to_words($remainder);
        }
        break;
    }

    if (null !== $fraction && is_numeric($fraction)) {
      $string .= $decimal;
      $words = array();
      foreach (str_split((string) $fraction) as $number) {
        $words[] = $dictionary[$number];
      }
      $string .= implode(' ', $words);
    }

    return $string;
  }

  public function pdf_doublecheque($etype, $vrnoa, $company_id)
  {
    $this->load->library('wkhtmltopdf');
    $data = array();

    if (!$etype) {
      redirect('user/dashboard');
    }

    $vr = $this->accounts->fetchChequeVoucher($vrnoa, $etype, $company_id);
    $data['vrdetail'] = $vr[0];
    $data['title'] = ($etype === 'pd_issue') ? 'Cheque Issue' : 'Cheque Received';

    if (empty($data['vrdetail'])) {
      redirect('user/dashboard');
    }

    $data['header_img'] = base_url('application/assets/uploads/header_imgs/' . $this->session->userdata('header_img'));

    $this->wkhtmltopdf->addPage($this->load->view('reportPrints/doublecheque_pdf', $data, true));
    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'landscape'
    ));
    // $this->wkhtmltopdf->send( strtolower($etype) . 'Cheque.pdf');
    $this->wkhtmltopdf->send();
  }

  public function send_mail($filePath, $subject, $message)
  {
    $configs = array(
      'protocol' => 'smtp',
      'smtp_host' => 'server1.digitalsofts.co.uk',
      'smtp_user' => 'info@afaqtraders.com',
      'smtp_pass' => "info@afaqtraders",
      'smtp_port' => '465'
    );

    $this->load->library("email", $configs);
    $this->email->set_newline("\r\n");
    $this->email->to("arshadfarouq@hotmail.com");
    $this->email->from("info@afaqtraders.com", "Afaq Traders");
    $this->email->subject($subject);
    $this->email->message($message);
    $this->email->attach($filePath);
    if ($this->email->send()) {
      // echo "Done!";
    } else {
      // echo $this->email->print_debugger();    
    }
  }


  public function Account_Flow($dt1, $dt2, $party_id, $company_id, $email_pdf = -1, $user_print, $account_name, $account_id)
  {
    $this->load->library('wkhtmltopdf');
    $data = array();

    $balData = $this->accounts->fetchOpeningBalance_Accounts($dt1, $party_id, $company_id);
    $data['previousBalance'] = $balData[0]['OPENING_BALANCE'];

    $pledger_1 = $this->ledgers->getCashFlowStatement($dt1, $dt2, $party_id);;

    $data['pledger'] = $pledger_1;
    $data['title'] = 'Daily Cash Statement';
    $data['account'] = urldecode($account_name);
    $data['accountCode'] = $account_id;
    $data['user'] = $user_print;
    $date_between = ' For ' . (string) date_format(new DateTime($dt1), 'd-M-Y') . ' To ' . (string) date_format(new DateTime($dt2), 'd-M-Y');

    $data['date_between'] = $date_between;

    // if (empty($data['pledger'])) {
    //     redirect('payment');
    // }



    // $balData = $this->accounts->fetchOpeningBalance_Accounts($dt1, $party_id,$company_id );
    // $data['previousBalance'] = $balData[0]['OPENING_BALANCE'];


    $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));

    $this->wkhtmltopdf->addPage($this->load->view('reportprints/AccountFlowPdfs', $data, true));
    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait'
    ));

    if ($email_pdf == 1) {
      $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
      $this->wkhtmltopdf->saveAs($save_path);
      sleep(5);
      $this->send_mail($save_path);
    } else {
      $this->wkhtmltopdf->send();
    }
  }



  public function salevoucherprint($vrnoa, $company_id, $etype)
  {
    $this->load->library('wkhtmltopdf');
    $data = array();
    $data['vrdetail'] = $this->orders->fetchforsaleprint($vrnoa, $etype, $company_id);
    $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], intval($data['vrdetail'][0]['party_id']), $vrnoa, $etype);
    $data['previousBalance'] = $balData;
    $this->wkhtmltopdf->addPage($this->load->view('reportprints/quotation_voucherpdf', $data, true));
    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait'
    ));
    $this->wkhtmltopdf->send();
  }




  public function pdf_TrialBalance6($dt1, $dt2, $company_id, $email_pdf = -1, $user_print, $l1, $l2, $l3)
  {
    $this->load->library('wkhtmltopdf');

    $data = array();

    $data['pledger'] = $this->accounts->fetchTrialBalanceData6($dt1, $dt2, $company_id, $l1, $l2, $l3);
    $data['title'] = 'Trial Balance';
    // $data['etype'] = $etype;
    $data['user'] = $user_print;
    $date_between = (string) $dt1 . ' To ' . (string) $dt2;

    $data['date_between'] = $date_between;

    // if (empty($data['pledger'])) {
    //     redirect('payment');
    // }

    // $data['amtInWords'] = $this->convert_number_to_words( intval($data['vrdetail'][0]['namount']) );

    // $balData = $this->accounts->fetchOpeningBalance_Accounts($dt1, $party_id,$company_id );
    // $data['previousBalance'] = $balData[0]['OPENING_BALANCE'];


    $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));

    $this->wkhtmltopdf->addPage($this->load->view('reportprints/TrialBalancePdf6', $data, true));
    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait',
      'margin-left' => '5',
      'margin-right' => '5',
      'footer-spacing' => '3.0',
      'footer-center' => '[page]/[toPage]',
      'footer-font-size' => '9'
    ));

    if ($email_pdf == 1) {
      $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
      $this->wkhtmltopdf->saveAs($save_path);
      sleep(5);
      $this->send_mail($save_path);
    } else {
      $this->wkhtmltopdf->send();
    }
  }

  public function pdf_TrialBalance($dt1, $dt2, $company_id, $email_pdf = -1, $user_print, $l1, $l2, $l3)
  {
    $this->load->library('wkhtmltopdf');

    $data = array();

    $data['pledger'] = $this->accounts->fetchTrialBalanceData6($dt1, $dt2, $company_id, $l1, $l2, $l3);
    $data['title'] = 'Trial Balance';
    // $data['etype'] = $etype;
    $data['user'] = $user_print;
    $date_between = (string) $dt1 . ' To ' . (string) $dt2;

    $data['date_between'] = $date_between;

    // if (empty($data['pledger'])) {
    //     redirect('payment');
    // }

    // $data['amtInWords'] = $this->convert_number_to_words( intval($data['vrdetail'][0]['namount']) );

    // $balData = $this->accounts->fetchOpeningBalance_Accounts($dt1, $party_id,$company_id );
    // $data['previousBalance'] = $balData[0]['OPENING_BALANCE'];


    $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));

    $this->wkhtmltopdf->addPage($this->load->view('reportprints/TrialBalancePdf', $data, true));
    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait',
      'margin-left' => '5',
      'margin-right' => '5',
      'footer-spacing' => '3.0',
      'footer-center' => '[page]/[toPage]',
      'footer-font-size' => '9'
    ));

    if ($email_pdf == 1) {
      $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
      $this->wkhtmltopdf->saveAs($save_path);
      sleep(5);
      $this->send_mail($save_path);
    } else {
      $this->wkhtmltopdf->send();
    }
  }

  public function Stock_Pdf($dt1, $dt2, $what, $company_id, $email_pdf = -1, $user_print, $hd)
  {
    $this->load->library('wkhtmltopdf');
    $data = array();
    $bdate = $this->stocks->fetchStockReport($dt1, $dt2, $what, $company_id);
    $data['vrdetail'] = $bdate;
    $data['what'] = $what;

    $data['title'] = 'Stock Report';
    $data['user'] = $user_print;
    $date_between = (string) $dt1 . ' To ' . (string) $dt2;

    $data['date_between'] = $date_between;

    // if (empty($data['pledger'])) {
    //     redirect('payment');
    // }

    // $data['amtInWords'] = $this->convert_number_to_words( intval($data['vrdetail'][0]['namount']) );

    // $balData = $this->accounts->fetchOpeningBalance_Accounts($dt1, $party_id,$company_id );
    // $data['previousBalance'] = $balData[0]['OPENING_BALANCE'];

    if ($hd == 1) {
      $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));
    } else {
      $data['header_img'] = '';
    }
    $this->wkhtmltopdf->addPage($this->load->view('reportprints/Stock_Pdf', $data, true));
    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait'
    ));

    if ($email_pdf == 1) {
      $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
      $this->wkhtmltopdf->saveAs($save_path);
      sleep(5);
      $this->send_mail($save_path);
    } else {
      $this->wkhtmltopdf->send();
    }
  }

  public function vouchers_reports_pdf($dt1, $dt2, $etype, $company_id, $email_pdf = -1, $user_print)
  {
    $this->load->library('wkhtmltopdf');

    $data = array();

    $data['vrdetail'] = $this->purchases->fetchPurchaseReportData($dt1, $dt2, 'voucher', 'detailed', $company_id, $etype);
    if ($etype == 'purchase') {
      $data['title'] = 'Purchase Report';
    } elseif ($etype == 'purchasereturn') {
      $data['title'] = 'Purchase Return Report';
    } elseif ($etype == 'sale') {
      $data['title'] = 'Sale Report';
    } elseif ($etype == 'salereturn') {
      $data['title'] = 'Sale Return Report';
    }

    // $data['etype'] = $etype;
    $data['user'] = $user_print;
    $date_between = (string) $dt1 . ' To ' . (string) $dt2;

    $data['date_between'] = $date_between;

    // if (empty($data['pledger'])) {
    //     redirect('payment');
    // }

    // $data['amtInWords'] = $this->convert_number_to_words( intval($data['vrdetail'][0]['namount']) );

    // $balData = $this->accounts->fetchOpeningBalance_Accounts($dt1, $party_id,$company_id );
    // $data['previousBalance'] = $balData[0]['OPENING_BALANCE'];


    $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));

    $this->wkhtmltopdf->addPage($this->load->view('reportprints/vouchers_reports_pdf', $data, true));
    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait'
    ));

    if ($email_pdf == 1) {
      $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
      $this->wkhtmltopdf->saveAs($save_path);
      sleep(5);
      $this->send_mail($save_path);
    } else {
      $this->wkhtmltopdf->send();
    }
  }

  public function Item_Ledger_Pdf($dt1, $dt2, $item_id, $company_id, $email_pdf = -1, $user_print, $godown_id, $godown, $Opening_Qty, $Opening_Weight, $balance_qty, $balance_weight)
  {
    $this->load->library('wkhtmltopdf');

    $data = array();

    $data['vrdetail'] = $this->items->fetchItemLedgerReport($dt1, $dt2, $item_id, $company_id, $godown_id);

    $data['title'] = 'Item Ledger Report';
    $data['user'] = $user_print;
    $date_between = (string) $dt1 . ' To ' . (string) $dt2;
    $data['date_between'] = $date_between;
    $data['godown'] = $godown;
    $data['Opening_Qty'] = $Opening_Qty;
    $data['Opening_Weight'] = $Opening_Weight;
    $data['balance_qty'] = $balance_qty;
    $data['balance_weight'] = $balance_weight;
    // $result = $this->items->fetchItemLedgerReport($from, $to, $item_id ,$company_id);
    // $data['OpeningQty']=$result[0]['OPENING_QTY']
    // $data['OpeningWeight']=$result[0]['OPENING_WEIGHT']
    // $balData = $this->accounts->fetchOpeningBalance_Accounts($dt1, $party_id,$company_id );
    // $data['previousBalance'] = $balData[0]['OPENING_BALANCE'];


    $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));

    $this->wkhtmltopdf->addPage($this->load->view('reportprints/ItemLedgerPdf', $data, true));
    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait'
    ));

    if ($email_pdf == 1) {
      $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
      $this->wkhtmltopdf->saveAs($save_path);
      sleep(5);
      $this->send_mail($save_path);
    } else {
      $this->wkhtmltopdf->send();
    }
  }


  public function pdf_ledger($dt1, $dt2, $party_id, $company_id, $email_pdf = -1, $user_print)
  {
    $this->load->library('wkhtmltopdf');

    $data = array();

    $data['pledger'] = $this->ledgers->getAccLedgerReport($dt1, $dt2, $party_id);
    $data['title'] = 'Account Ledger';
    // $data['etype'] = $etype;
    $data['user'] = $user_print;
    $date_between = (string) $dt1 . ' To ' . (string) $dt2;

    $data['date_between'] = $date_between;

    // if (empty($data['pledger'])) {
    //     redirect('payment');
    // }

    // $data['amtInWords'] = $this->convert_number_to_words( intval($data['vrdetail'][0]['namount']) );

    // $balData = $this->accounts->fetchPartyOpeningBalance($dt1, $party_id );
    $data['previousBalance'] = 0; //$balData[0]['OPENING_BALANCE'];


    $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));

    $this->wkhtmltopdf->addPage($this->load->view('reportprints/acc_ledger', $data, true));
    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait'
    ));

    if ($email_pdf == 1) {
      $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
      $this->wkhtmltopdf->saveAs($save_path);
      sleep(5);
      $this->send_mail($save_path);
    } else {
      $this->wkhtmltopdf->send();
    }
  }



  public function pdf_singlecheque($etype, $vrnoa, $company_id, $email_pdf, $user_print)
  {
    $this->load->library('wkhtmltopdf');
    $data = array();

    // if ( !$etype ) {
    //   redirect('user/dashboard');
    // }
    $vr = $this->accounts->fetchChequeVoucher($vrnoa, $etype, $company_id);
    $data['pos_pd_cheque'] = $vr[0];
    $data['title'] = ($etype === 'pd_issue') ? 'Cheque Issue' : 'Cheque Received';

    if (empty($data['pos_pd_cheque'])) {
      redirect('user/dashboard');
    }
    $data['user'] = $user_print;

    //$data['header_img'] = base_url('application/assets/uploads/header_imgs/' . $this->session->userdata('header_img'));
    $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));

    $this->wkhtmltopdf->addPage($this->load->view('reportprints/ChequePdf', $data, true));
    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait'
    ));
    // $this->wkhtmltopdf->send( strtolower($etype) . 'Cheque.pdf');
    $this->wkhtmltopdf->send();
  }

  public function genVoucherPdf($etype, $vrnoa, $company_id, $email_pdf = -1)
  {
    $filename =  "Voucher-" . date("Y-m-d_H-i-s") . ".pdf";
    $save_path = $_SERVER['DOCUMENT_ROOT'] . '/application/assets/documents/' . $filename;

    $generator = base_url("index.php/doc/$etype/$vrnoa/$company_id/$email_pdf");

    exec("wkhtmltopdf64 $generator $save_path");
    $pdf = file_get_contents($save_path);

    header('Content-Type: application/pdf');
    header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
    header('Pragma: public');
    header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    header('Content-Length: ' . strlen($pdf));
    header('Content-Disposition: inline; filename="' . basename($save_path) . '";');
    ob_clean();

    // echo $pdf;
  }


  public function Print_Order_Voucher($etype, $vrnoa, $company_id, $email_pdf = -1, $user, $pre_bal_print, $hd = 1, $prnt = 'lg', $wrate = '')
  {
    $this->load->library('wkhtmltopdf');
    $data = array();
    $data['vrdetail'] = $this->orders->fetch($vrnoa, $etype, $company_id);

    if ($etype == 'pur_order') {
      $data['title'] = 'Purchase Order';
    } elseif ($etype == 'sale_order') {
      $data['title'] = 'Sale Order';
    } elseif ($etype == 'sale') {
      $data['title'] = 'Sale Invoice';
    } elseif ($etype == 'order_parts') {
      $data['title'] = 'Parts Detail';
    } elseif ($etype == 'order_loading') {
      $data['title'] = 'Gate Pass';
    } elseif ($etype == 'purchasecontract') {
      $data['title'] = 'Purchase Contract Voucher';
    }
    $data['pre_bal_print'] = $pre_bal_print;
    $data['user'] = $user;
    $data['hd'] = $hd;
    // // if (empty($data['vrdetail'])) {
    // //     redirect('user/dashboard');
    // // }
    // $balData = $this->accounts->fetchOpeningBalance_Accounts($data['vrdetail'][0]['vrdate'], intval($data['vrdetail'][0]['party_id']),$company_id );
    // $data['previousBalance'] = $balData[0]['OPENING_BALANCE'];
    $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], intval($data['vrdetail'][0]['party_id']), $vrnoa, $etype);
    $data['previousBalance'] = $balData;
    $data['amtInWords'] = $this->convert_number_to_words(intval($data['vrdetail'][0]['namount']));
    if ($hd == 1) {
      $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));
    } else {
      $data['header_img'] = '';
    }
    if ($etype == 'order_parts' || $etype == 'order_loading') {
      $this->wkhtmltopdf->addPage($this->load->view('reportprints/voucher_ordergp_pdf', $data, true));
    } else if ($etype == 'sale_order') {
      $this->wkhtmltopdf->addPage($this->load->view('reportprints/saleOrderPrint', $data, true));
    } else {
      if ($prnt == 'sm') {
        if ($wrate == 'wrate') {
          $this->wkhtmltopdf->addPage($this->load->view('reportprints/voucher_order_pdf_sm2', $data, true));
        } else {
          $this->wkhtmltopdf->addPage($this->load->view('reportprints/voucher_order_pdf_sm', $data, true));
        }
      } else {
        $this->wkhtmltopdf->addPage($this->load->view('reportprints/voucher_order_pdf', $data, true));
      }
    }


    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait'
    ));

    if ($email_pdf == 1) {

      $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
      $this->wkhtmltopdf->saveAs($save_path);
      sleep(5);
      $this->send_mail($save_path);
    } else {
      $this->wkhtmltopdf->send();
    }
  }

  public function Print_Order_Vouchers($etype, $vrnoa, $company_id, $email_pdf = -1, $user, $pre_bal_print, $hd = '', $prnt = 'lg', $wrate = '')
  {
    $this->load->library('wkhtmltopdf');
    $data = array();
    $data['vrdetail'] = $this->orders->fetch($vrnoa, $etype, $company_id);

    if ($etype == 'pur_order') {
      $data['title'] = 'Purchase Order';
    } elseif ($etype == 'sale_order') {
      $data['title'] = 'Quotation';
    } elseif ($etype == 'sale') {
      $data['title'] = 'Sale Invoice';
    } elseif ($etype == 'order_parts') {
      $data['title'] = 'Parts Detail';
    } elseif ($etype == 'order_loading') {
      $data['title'] = 'Gate Pass';
    }
    $data['pre_bal_print'] = $pre_bal_print;
    $data['user'] = $user;
    $data['hd'] = $hd;
    // // if (empty($data['vrdetail'])) {
    // //     redirect('user/dashboard');
    // // }
    $balData = $this->accounts->fetchOpeningBalance_Accounts($data['vrdetail'][0]['vrdate'], intval($data['vrdetail'][0]['party_id']), $company_id);
    $data['previousBalance'] = $balData[0]['OPENING_BALANCE'];
    $data['amtInWords'] = $this->convert_number_to_words(intval($data['vrdetail'][0]['namount']));
    if ($hd == 1) {
      $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));
    } else {
      $data['header_img'] = '';
    }
    if ($etype == 'order_parts' || $etype == 'order_loading') {
      $this->wkhtmltopdf->addPage($this->load->view('reportprints/voucher_ordergp_pdf', $data, true));
    } else {
      if ($prnt == 'sm') {
        if ($wrate == 'wrate') {
          $this->wkhtmltopdf->addPage($this->load->view('reportprints/voucher_order_pdf_sm2', $data, true));
        } else {
          $this->wkhtmltopdf->addPage($this->load->view('reportprints/voucher_order_pdf_sm', $data, true));
        }
      } else {
        $this->wkhtmltopdf->addPage($this->load->view('reportprints/voucher_order_pdfs', $data, true));
      }
    }


    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait'
    ));

    if ($email_pdf == 1) {

      $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
      $this->wkhtmltopdf->saveAs($save_path);
      sleep(5);
      $this->send_mail($save_path);
    } else {
      $this->wkhtmltopdf->send();
    }
  }

  public function Print_Voucher($etype, $vrnoa, $company_id, $email_pdf = -1, $user, $pre_bal_print, $hd = 1, $prnt = 'lg', $wrate = '')
  {
    $this->load->library('wkhtmltopdf');
    $data = array();
    // if ( !$etype ) {
    //   redirect('user/dashboard');
    // }
    if ($prnt == 'undefined') {
      $prnt = 'lg';
    }
    if ($wrate = 'undefined') {
      $wrate = '';
    }
    if ($etype == 'purchase') {
      $data['title'] = 'Purchase Invoice';
    } elseif ($etype == 'purchasereturn') {
      $data['title'] = 'Purchase Return Invoice';
    } elseif ($etype == 'sale') {
      $data['title'] = 'Sale Invoice';
    } elseif ($etype == 'salereturn') {
      $data['title'] = 'Sale Return Invoice';
    } elseif ($etype == 'moulding') {
      $data['title'] = 'Moulding Sheet';
    } elseif ($etype == 'navigation') {
      $data['title'] = 'Stock Navigation';
    } elseif ($etype == 'outward') {
      $data['title'] = 'Outward ';
      $data['etype'] = 'outward';
    } elseif ($etype == 'inward') {
      $data['title'] = 'Inward ';
      $data['etype'] = 'inward';
    } elseif ($etype == 'headmoulding') {
      $data['title'] = 'Head Moulding ';
    }

    $data['pre_bal_print'] = $pre_bal_print;
    $data['user'] = $user;
    $data['hd'] = $hd;
    // if (empty($data['vrdetail'])) {
    //     redirect('user/dashboard');
    // }



    if ($etype == 'navigation') {
      $data['vrdetail'] = $this->purchases->fetchNavigation($vrnoa, $etype, $company_id);
      $balData = 0;
      $data['previousBalance'] = 0;
      $data['amtInWords'] = '';
    } else {
      $data['vrdetail'] = $this->purchases->fetchVoucher($vrnoa, $etype, $company_id);
      if ($etype === 'headmoulding') {
        $data['vrdetail_detail'] = $this->purchases->fetchDetail($vrnoa, $etype, $company_id);
      }
      // $balData = $this->accounts->fetchOpeningBalance_Accounts($data['vrdetail'][0]['vrdate'], intval($data['vrdetail'][0]['party_id']),$company_id );
      // $data['previousBalance'] = $balData[0]['OPENING_BALANCE'];
      $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], intval($data['vrdetail'][0]['party_id']), $vrnoa, $etype);
      $data['previousBalance'] = $balData;
      $data['amtInWords'] = $this->convert_number_to_words(intval($data['vrdetail'][0]['namount']));
    }


    if ($hd == 1) {
      $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));
    } else {
      $data['header_img'] = '';
    }

    if ($etype == 'moulding') {
      $this->wkhtmltopdf->addPage($this->load->view('reportprints/moulding_sheet_pdf', $data, true));
    } elseif ($etype == 'headmoulding') {
      $this->wkhtmltopdf->addPage($this->load->view('reportprints/head_moulding_sheet_pdf', $data, true));
    } elseif ($etype == 'navigation') {
      $this->wkhtmltopdf->addPage($this->load->view('reportprints/StockNavigation', $data, true));
    } else {
      if ($prnt == 'sm') {
        if ($wrate == 'wrate') {
          $this->wkhtmltopdf->addPage($this->load->view('reportprints/vocher_pdf_sm2', $data, true));
        } else {
          $this->wkhtmltopdf->addPage($this->load->view('reportprints/vocher_pdf_sm', $data, true));
        }
      } else {

        $this->wkhtmltopdf->addPage($this->load->view('reportprints/voucher_pdf', $data, true));
      }
    }
    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait'
    ));

    if ($email_pdf == 1) {

      $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
      $this->wkhtmltopdf->saveAs($save_path);
      sleep(5);
      $this->send_mail($save_path);
    } else {
      $this->wkhtmltopdf->send();
    }
  }
  public function print_emp_pro_voucher($etype, $vrnoa, $company_id, $email_pdf = -1, $user, $pre_bal_print, $hd = 1)
  {
    $this->load->library('wkhtmltopdf');
    $data = array();
    // if ( !$etype ) {
    //   redirect('user/dashboard');
    // }

    if ($etype == 'employeeproduction') {
      $data['title'] = 'Employee Production Voucher';
    } elseif ($etype == 'consumption') {
      $data['title'] = 'Issuance Voucher';
    } elseif ($etype == 'materialreturn') {
      $data['title'] = 'Material Return Voucher';
    }

    $data['pre_bal_print'] = $pre_bal_print;
    $data['user'] = $user;
    $data['hd'] = $hd;
    if ($etype == 'employeeproduction') {
      $data['vrdetail'] = $this->purchases->employeeFetchProduction($vrnoa, $etype, $company_id);
    } else {
      $data['vrdetail'] = $this->purchases->fetch($vrnoa, $etype, $company_id);
    }

    $balData = 0;
    $data['previousBalance'] = 0;
    $data['amtInWords'] = '';
    if ($hd == 1) {
      $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));
    } else {
      $data['header_img'] = '';
    }

    if ($etype == 'employeeproduction') {
      $this->wkhtmltopdf->addPage($this->load->view('reportprints/production_print', $data, true));
    } else {
      $this->wkhtmltopdf->addPage($this->load->view('reportprints/print_pro_voucher', $data, true));
    }
    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait'
    ));

    if ($email_pdf == 1) {

      $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
      $this->wkhtmltopdf->saveAs($save_path);
      sleep(5);
      $this->send_mail($save_path);
    } else {
      $this->wkhtmltopdf->send();
    }
  }
  public function print_pro_voucher($etype, $vrnoa, $company_id, $email_pdf = -1, $user, $pre_bal_print, $hd = 1)
  {
    $this->load->library('wkhtmltopdf');
    $data = array();
    // if ( !$etype ) {
    //   redirect('user/dashboard');
    // }

    if ($etype == 'production') {
      $data['title'] = 'Production Voucher';
    } elseif ($etype == 'consumption') {
      $data['title'] = 'Issuance Voucher';
    } elseif ($etype == 'materialreturn') {
      $data['title'] = 'Material Return Voucher';
    } elseif ($etype == 'head_production') {
      $data['title'] = 'Head Production Voucher';
    }

    $data['pre_bal_print'] = $pre_bal_print;
    $data['user'] = $user;
    $data['hd'] = $hd;
    if ($etype == 'production') {
      $data['vrdetail'] = $this->purchases->fetchProduction($vrnoa, $etype, $company_id);
    } else if ($etype == 'head_production') {
      $data['vrdetail'] = $this->purchases->fetchProduction($vrnoa, $etype, $company_id);
    } else {
      $data['vrdetail'] = $this->purchases->fetch($vrnoa, $etype, $company_id);
    }

    $balData = 0;
    $data['previousBalance'] = 0;
    $data['amtInWords'] = '';
    if ($hd == 1) {
      $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));
    } else {
      $data['header_img'] = '';
    }

    if ($etype == 'production') {
      $this->wkhtmltopdf->addPage($this->load->view('reportprints/production_print', $data, true));
    } else if ($etype == 'head_production') {
      $this->wkhtmltopdf->addPage($this->load->view('reportprints/head_production_print', $data, true));
    } else {
      $this->wkhtmltopdf->addPage($this->load->view('reportprints/print_pro_voucher', $data, true));
    }
    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait'
    ));

    if ($email_pdf == 1) {

      $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
      $this->wkhtmltopdf->saveAs($save_path);
      sleep(5);
      $this->send_mail($save_path);
    } else {
      $this->wkhtmltopdf->send();
    }
  }
  public function print_pro_vouchers($etype, $vrnoa, $company_id, $email_pdf = -1, $user, $pre_bal_print, $hd = 1)
  {
    $this->load->library('wkhtmltopdf');
    $data = array();
    // if ( !$etype ) {
    //   redirect('user/dashboard');
    // }

    if ($etype == 'production') {
      $data['title'] = 'Production Voucher';
    } elseif ($etype == 'consumption') {
      $data['title'] = 'Issuance Voucher';
    } elseif ($etype == 'materialreturn') {
      $data['title'] = 'Material Return Voucher';
    }

    $data['pre_bal_print'] = $pre_bal_print;
    $data['user'] = $user;
    $data['hd'] = $hd;

    $data['vrdetail'] = $this->purchases->fetchProduction($vrnoa, $etype, $company_id);
    $balData = 0;
    $data['previousBalance'] = 0;
    $data['amtInWords'] = '';
    if ($hd == 1) {
      $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));
    } else {
      $data['header_img'] = '';
    }



    $this->wkhtmltopdf->addPage($this->load->view('reportprints/print_pro_vouchers', $data, true));

    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait'
    ));

    if ($email_pdf == 1) {

      $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
      $this->wkhtmltopdf->saveAs($save_path);
      sleep(5);
      $this->send_mail($save_path);
    } else {
      $this->wkhtmltopdf->send();
    }
  }
  public function print_itemphaserecipe_vouchers($etype, $vrnoa, $company_id, $email_pdf = -1, $user, $pre_bal_print, $hd = 1)
  {
    $this->load->library('wkhtmltopdf');
    $data = array();
    // if ( !$etype ) {
    //   redirect('user/dashboard');
    // }

    if ($etype == 'itemphaserecipe') {
      $data['title'] = 'Item Phase Recipe Voucher';
    }

    $data['pre_bal_print'] = $pre_bal_print;
    $data['user'] = $user;
    $data['hd'] = $hd;

    $data['vrdetail'] = $this->ItemphaseRecepies->fetch($vrnoa, $etype, $company_id);
    $balData = 0;
    $data['previousBalance'] = 0;
    $data['amtInWords'] = '';
    if ($hd == 1) {
      $data['header_img'] = base_url('assets/img/pic1.png/');
    } else {
      $data['header_img'] = '';
    }



    $this->wkhtmltopdf->addPage($this->load->view('reportprints/print_itemphaserecipe_vouchers', $data, true));

    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait'
    ));

    if ($email_pdf == 1) {

      $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
      $this->wkhtmltopdf->saveAs($save_path);
      sleep(5);
      $this->send_mail($save_path);
    } else {
      $this->wkhtmltopdf->send();
    }
  }


  // Using WKHTML - Working Perfectly
  public function pdf_singlevoucher($etype, $vrnoa, $company_id, $email_pdf = -1)
  {
    $this->load->library('wkhtmltopdf');

    $data = array();

    if (!$etype) {
      redirect('user/dashboard');
    } else if ($etype == 'sale') {

      $data['vrdetail'] = $this->purchases->fetch($vrnoa, $company_id);
      $data['title'] = 'Sale';

      if (empty($data['vrdetail'])) {
        redirect('user/dashboard');
      }

      $data['amtInWords'] = $this->convert_number_to_words(intval($data['vrdetail'][0]['namount']));

      $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['smainpid'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
      $data['previousBalance'] = $balData[0]['RTotal'];
    } else if ($etype == 'purchase') {

      $data['vrdetail'] = $this->purchases->fetchVoucher($vrnoa, $company_id);
      $data['title'] = 'Purchase';

      if (empty($data['vrdetail'])) {
        redirect('user/dashboard');
      }

      $data['amtInWords'] = $this->convert_number_to_words(intval($data['vrdetail'][0]['namount']));

      $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['smainpid'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
      $data['previousBalance'] = $balData[0]['RTotal'];
    } else if ($etype == 'purchasereturn') {
      $data['vrdetail'] = $this->purchasereturns->fetchVoucher($vrnoa, $company_id);
      $data['title'] = 'Purchase Return';

      if (empty($data['vrdetail'])) {
        redirect('user/dashboard');
      }

      $data['amtInWords'] = $this->convert_number_to_words(intval($data['vrdetail'][0]['namount']));

      $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['smainpid'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
      $data['previousBalance'] = $balData[0]['RTotal'];
    } else if ($etype == 'purchaseorder') {
      $data['vrdetail'] = $this->purchases->fetchOrderVoucher($vrnoa, $company_id);
      $data['title'] = 'Purchase Order';

      $etype = 'quotation';

      if (empty($data['vrdetail'])) {
        redirect('user/dashboard');
      }

      $data['amtInWords'] = $this->convert_number_to_words(intval($data['vrdetail'][0]['namount']));

      $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['smainpid'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
      $data['previousBalance'] = $balData[0]['RTotal'];
    } else if ($etype == 'salereturn') {
      $data['vrdetail'] = $this->salereturns->fetchVoucher($vrnoa, $company_id);
      $data['title'] = 'Sale Return';

      if (empty($data['vrdetail'])) {
        redirect('user/dashboard');
      }

      $data['amtInWords'] = $this->convert_number_to_words(intval($data['vrdetail'][0]['namount']));

      $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['smainpid'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
      $data['previousBalance'] = $balData[0]['RTotal'];
    } else if ($etype == 'saleorder') {
      $data['vrdetail'] = $this->sales->fetchOrderVoucher($vrnoa, $company_id);
      $data['title'] = 'Sale Quotation';

      if (empty($data['vrdetail'])) {
        redirect('user/dashboard');
      }

      $data['amtInWords'] = $this->convert_number_to_words(intval($data['vrdetail'][0]['namount']));

      $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['smainpid'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
      $data['previousBalance'] = $balData[0]['RTotal'];
    }

    $data['header_img'] = base_url('application/assets/uploads/header_imgs/' . $this->session->userdata('header_img'));

    $this->wkhtmltopdf->addPage($this->load->view('reportPrints/voucher_pdf', $data, true));
    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait'
    ));

    if ($email_pdf == 1) {

      $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
      $this->wkhtmltopdf->saveAs($save_path);
      sleep(5);
      $this->send_mail($save_path);
    } else {
      $this->wkhtmltopdf->send();
    }
  }

  public function JvVocuherPrintPdf($etype, $vrnoa, $company_id, $email_pdf = -1, $user_print)
  {
    $this->load->library('wkhtmltopdf');

    $data = array();


    $data['pledger'] = $this->ledgers->fetch($vrnoa, $etype, $company_id);
    // $data['title'] = 'Journal Voucher';
    if ($etype == 'jv') {
      $data['title'] = 'Journal Voucher';
    } else {
      $data['title'] = 'Opening Balance';
    }
    $data['etype'] = $etype;
    $data['user'] = $user_print;

    // if (empty($data['pledger'])) {
    //     redirect('payment');
    // }

    // $data['amtInWords'] = $this->convert_number_to_words( intval($data['vrdetail'][0]['namount']) );

    // $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['smainpid'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
    // $data['previousBalance'] = $balData[0]['RTotal'];


    $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));

    $this->wkhtmltopdf->addPage($this->load->view('reportprints/JvVoucherPrintPdf', $data, true));
    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait'
    ));

    if ($email_pdf == 1) {
      $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
      $this->wkhtmltopdf->saveAs($save_path);
      sleep(5);
      $this->send_mail($save_path);
    } else {
      $this->wkhtmltopdf->send();
    }
  }
  public function openingStockVocuherPrintPdf($etype, $vrnoa, $company_id, $email_pdf = -1, $user_print)
  {
    $this->load->library('wkhtmltopdf');

    $data = array();


    $data['pledger'] = $this->purchases->fetch($vrnoa, $etype, $company_id);
    $data['title'] = 'Opening Stock';

    $data['etype'] = $etype;
    $data['user'] = $user_print;

    // if (empty($data['pledger'])) {
    //     redirect('payment');
    // }

    // $data['amtInWords'] = $this->convert_number_to_words( intval($data['vrdetail'][0]['namount']) );

    // $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['smainpid'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
    // $data['previousBalance'] = $balData[0]['RTotal'];


    $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));

    $this->wkhtmltopdf->addPage($this->load->view('reportprints/openingStockPrint', $data, true));
    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait'
    ));

    if ($email_pdf == 1) {
      $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
      $this->wkhtmltopdf->saveAs($save_path);
      sleep(5);
      $this->send_mail($save_path);
    } else {
      $this->wkhtmltopdf->send();
    }
  }

  public function CashVocuherPrintPdf($etype, $vrnoa, $company_id, $email_pdf = -1, $user_print, $pr = 1, $prnt = 'lg')
  {
    $this->load->library('wkhtmltopdf');

    $data = array();


    $data['pledger'] = $this->ledgers->fetch($vrnoa, $etype, $company_id);
    if ($etype == 'cpv' || $etype == 'foreigncashpayment') {
      $data['title'] = 'Payment Voucher';
    } else {
      $data['title'] = 'Receipt Voucher';
    }
    $data['etype'] = $etype;
    $data['user'] = $user_print;

    // if (empty($data['pledger'])) {
    //     redirect('payment');
    // }

    // $data['amtInWords'] = $this->convert_number_to_words( intval($data['vrdetail'][0]['namount']) );

    // $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['smainpid'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
    // $data['previousBalance'] = $balData[0]['RTotal'];

    if ($pr == 1) {
      $data['header_img'] = base_url('assets/img/pic1.png/' . $this->session->userdata('header_img'));
    } else {
      $data['header_img'] = '';
    }

    if ($prnt == 'sm') {
      $this->wkhtmltopdf->addPage($this->load->view('reportprints/paymentsm', $data, true));
    } else {
      $this->wkhtmltopdf->addPage($this->load->view('reportprints/CashVoucherPrintPdf', $data, true));
    }

    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'portrait'
    ));

    if ($email_pdf == 1) {

      $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
      $this->wkhtmltopdf->saveAs($save_path);
      sleep(5);
      $this->send_mail($save_path);
    } else {
      $this->wkhtmltopdf->send();
    }
  }


  public function print_Requisition_Voucher($etype, $vrnoa, $company_id, $email_pdf = -1, $user, $pre_bal_print, $hd = 1, $prnt = 'lg', $wrate = '')
  {

    $this->load->library('wkhtmltopdf');
    $data = array();

    $data['user'] = $user;
    $data['hd'] = $hd;
    if ($etype == 'requisition') {
      $data['title'] = 'Requisition Voucher';
      $etype = 'requisition';
      $data['vrdetail'] = $this->orders->fetch($vrnoa, $etype, $company_id);
      $balData = $this->accounts->fetchOpeningBalance_Accounts($data['vrdetail'][0]['vrdate'], intval($data['vrdetail'][0]['party_id']), $company_id);
      $data['previousBalance'] = $balData[0]['OPENING_BALANCE'];
      $data['amtInWords'] = $this->convert_Number_To_Words(intval($data['vrdetail'][0]['namount']));
    }
    if ($hd == 1) {
      $data['header_img'] = base_url('assets/img/pic1.png');
    } else {
      $data['header_img'] = '';
    }

    if ($prnt == 'sm') {
      if ($wrate == 'wrate') {
        $this->wkhtmltopdf->addPage($this->load->view('reportprints/requisition_pdf_sm2', $data, true));
      } else {
        $this->wkhtmltopdf->addPage($this->load->view('reportprints/requisition_pdf_sm', $data, true));
      }
    } else {
      $this->wkhtmltopdf->addPage($this->load->view('reportprints/requisition_pdf', $data, true));
    }
    if ($email_pdf == 1) {

      $save_path = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/documents/' . microtime() . 'Voucher.pdf');
      $this->wkhtmltopdf->saveAs($save_path);
      sleep(5);
      $this->send_mail($save_path);
    } else {
      $this->wkhtmltopdf->send();
    }
  }

  // Using WKHTML - Working Perfectly
  public function pdf_doublevoucher($etype, $vrnoa, $company_id)
  {
    $this->load->library('wkhtmltopdf');

    $data = array();

    if (!$etype) {
      redirect('user/dashboard');
    } else if ($etype == 'sale') {

      $data['vrdetail'] = $this->sales->fetchVoucher($vrnoa, $company_id);
      $data['title'] = 'Sale';

      if (empty($data['vrdetail'])) {
        redirect('user/dashboard');
      }

      $data['amtInWords'] = $this->convert_number_to_words(intval($data['vrdetail'][0]['namount']));
      $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['smainpid'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
      $data['previousBalance'] = $balData[0]['RTotal'];
    } else if ($etype == 'purchase') {
      $data['vrdetail'] = $this->purchases->fetchVoucher($vrnoa, $company_id);
      $data['title'] = 'Purchase';

      if (empty($data['vrdetail'])) {
        redirect('user/dashboard');
      }

      $data['amtInWords'] = $this->convert_number_to_words(intval($data['vrdetail'][0]['namount']));
      $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['smainpid'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
      $data['previousBalance'] = $balData[0]['RTotal'];
    } else if ($etype == 'purchasereturn') {
      $data['vrdetail'] = $this->purchasereturns->fetchVoucher($vrnoa, $company_id);
      $data['title'] = 'Purchase Return';

      if (empty($data['vrdetail'])) {
        redirect('user/dashboard');
      }

      $data['amtInWords'] = $this->convert_number_to_words(intval($data['vrdetail'][0]['namount']));

      $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['smainpid'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
      $data['previousBalance'] = $balData[0]['RTotal'];
    } else if ($etype == 'purchaseorder') {
      $data['vrdetail'] = $this->purchases->fetchOrderVoucher($vrnoa, $company_id);
      $data['title'] = 'Purchase Quotation';

      $etype = 'quotation';

      if (empty($data['vrdetail'])) {
        redirect('user/dashboard');
      }

      $data['amtInWords'] = $this->convert_number_to_words(intval($data['vrdetail'][0]['namount']));
      $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['smainpid'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
      $data['previousBalance'] = $balData[0]['RTotal'];
    } else if ($etype == 'salereturn') {
      $data['vrdetail'] = $this->salereturns->fetchVoucher($vrnoa, $company_id);
      $data['title'] = 'Sale Return';

      if (empty($data['vrdetail'])) {
        redirect('user/dashboard');
      }

      $data['amtInWords'] = $this->convert_number_to_words(intval($data['vrdetail'][0]['namount']));
      $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['smainpid'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
      $data['previousBalance'] = $balData[0]['RTotal'];
    } else if ($etype == 'saleorder') {
      $data['vrdetail'] = $this->sales->fetchOrderVoucher($vrnoa, $company_id);
      $data['title'] = 'Quotation';

      if (empty($data['vrdetail'])) {
        redirect('user/dashboard');
      }

      $data['amtInWords'] = $this->convert_number_to_words(intval($data['vrdetail'][0]['namount']));
      $balData = $this->accounts->fetchPreviousBalance($data['vrdetail'][0]['vrdate'], $data['vrdetail'][0]['smainpid'], $data['vrdetail'][0]['vrnoa'], $company_id, $etype);
      $data['previousBalance'] = $balData[0]['RTotal'];
    }

    $data['header_img'] = base_url('application/assets/uploads/header_imgs/' . $this->session->userdata('header_img'));

    // $this->load->view('reportPrints/doublevoucher_pdf2', $data);

    $this->wkhtmltopdf->addPage($this->load->view('reportPrints/doublevoucher_pdf2', $data, true));
    $this->wkhtmltopdf->setOptions(array(
      'orientation' => 'landscape'
    ));

    $this->wkhtmltopdf->send();
  }

  public function tcpdf()
  {
    $this->load->helper('pdf_helper');
    /*
        ---- ---- ---- ----
        your code here
        ---- ---- ---- ----
    */
    $this->load->view('pdfvr');
  }

  public function mpdf()
  {
    $filename = 'first_file';
    // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
    $pdfFilePath = FCPATH . "/downloads/reports/$filename.pdf";
    $data['page_title'] = 'Hello world'; // pass data to the view

    if (file_exists($pdfFilePath) == FALSE) {
      ini_set('memory_limit', '32M'); // boost the memory limit if it's low <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley"> 
      $html = $this->load->view('reportPrints/pdfvoucher.php', $data, true); // render the view into HTML

      $this->load->library('my_mpdf');
      $pdf = $this->my_mpdf->load();
      $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley"> 
      $pdf->WriteHTML($html); // write the HTML into the PDF
      $pdf->Output($pdfFilePath, 'I'); // save to file because we can
    }

    // redirect("/downloads/reports/$filename.pdf");
  }

  public function voucher($etype, $vrnoa)
  {
    $company_id = $this->session->userdata('company_id');
    $vrDetail = $this->sales->fetchVoucher($vrnoa, $company_id);

    // Generate PDF by saying hello to the world
    $this->pdf->AddPage('P', 'A4');

    // Header
    $this->pdf->SetFont('Arial', 'B', 16);
    $this->pdf->Image(base_url('application/assets/img/logos/logo1.png'), 150, 10, 50);

    $this->pdf->ln();

    // Left Fields
    $this->pdf->SetFont('Arial', 'B', 11);
    $this->pdf->Cell(25, 10, 'Inv#');

    $this->pdf->SetFont('Arial', '', 11);
    $this->pdf->Cell(25, 10, $vrDetail[0]['vrnoa']);

    $this->pdf->ln(6);

    $this->pdf->SetFont('Arial', 'B', 11);
    $this->pdf->Cell(25, 10, 'Date');

    $this->pdf->SetFont('Arial', '', 11);
    $this->pdf->Cell(25, 10,  $vrDetail[0]['vrdate']);

    $this->pdf->ln(6);

    $this->pdf->SetFont('Arial', 'B', 11);
    $this->pdf->Cell(25, 10, 'Customer');

    $this->pdf->SetFont('Arial', '', 11);
    $this->pdf->Cell(25, 10,   $vrDetail[0]['custparty'] . ' / ' . $vrDetail[0]['custname']);

    $this->pdf->ln(6);

    $this->pdf->SetFont('Arial', 'B', 11);
    $this->pdf->Cell(25, 10, 'Mobile');

    $this->pdf->SetFont('Arial', '', 11);
    $this->pdf->Cell(25, 10,  $vrDetail[0]['custmobile']);

    $this->pdf->SetFont('Arial', 'B', 20);
    $this->pdf->Cell(0, 10, 'Sale', 0, 0, 'R');

    $this->pdf->ln(15);

    ////////////////
    // Table Head //
    ////////////////
    $this->pdf->SetFont('Arial', 'B', 9);

    $this->pdf->Cell(7, 7, '#', 1, 0, 'C');
    $this->pdf->Cell(122, 7, 'Item', 1, 0, 'C');
    $this->pdf->Cell(15, 7, 'Qty', 1, 0, 'C');
    $this->pdf->Cell(15, 7, 'Rate', 1, 0, 'C');
    $this->pdf->Cell(30, 7, 'Amount', 1, 0, 'C');
    ////////////////////
    // End Table Head //
    ////////////////////

    $this->pdf->ln(7);

    ////////////////
    // Rows Start //
    ////////////////

    foreach ($vrDetail as $row) {
      $this->pdf->SetFont('Arial', '', 9);

      $this->pdf->Cell(7, 20, '1', 1, 0, 'L');
      $this->pdf->Cell(122, 7, trim($row['description']), 1, 0, 'L');
      $this->pdf->Cell(15, 20, trim($row['qty']), 1, 0, 'R');
      $this->pdf->Cell(15, 20, trim($row['rate']), 1, 0, 'R');
      $this->pdf->Cell(30, 20, trim($row['amount']), 1, 0, 'R');

      $this->pdf->ln(7);

      $this->pdf->setX(17);
      $this->pdf->Cell(122, 7, trim($row['specs']), 1, 0, 'L');

      $this->pdf->ln(7);
      $this->pdf->setX(17);
      $this->pdf->Cell(122, 6, trim($row['serial']), 1, 0, 'L');

      $this->pdf->ln(7);
    }


    //////////////
    // End Rows //
    //////////////

    $this->pdf->ln(7);

    $this->pdf->SetFont('Arial', 'B', 9);
    $this->pdf->Cell(30, 7, 'In words: ');

    $this->pdf->SetFont('Arial', '', 9);
    $this->pdf->Cell(100, 7, 'Seven thousand seven hundred point zero zero only.');

    $this->pdf->Cell(0, 7, '|||||||', 0, 0, 'R');

    $this->pdf->ln(5);

    $this->pdf->SetFont('Arial', 'B', 9);
    $this->pdf->Cell(30, 7, 'Prev Balance: ');


    $this->pdf->SetFont('Arial', '', 9);
    $this->pdf->Cell(100, 7, '12,122,122/=');

    $this->pdf->ln(15);

    //////////////////////
    // Signature Fields //
    //////////////////////

    $this->pdf->Cell(47, 7, 'Prepared By', 'T', 0, 'C');
    $this->pdf->Cell(20, 7, ' ', 0, 0, 'C');
    $this->pdf->Cell(48, 7, 'Accountant', 'T', 0, 'C');
    $this->pdf->Cell(20, 7, ' ', 0, 0, 'C');
    $this->pdf->Cell(47, 7, 'Received By', 'T', 0, 'C');

    $this->pdf->Output();

    //////////////////////////
    // END Signature fields //
    //////////////////////////

  }

  public function sample()
  {
    // Generate PDF by saying hello to the world
    $this->pdf->AddPage('P', 'A4');

    // Header
    $this->pdf->SetFont('Arial', 'B', 16);
    $this->pdf->Image(base_url('application/assets/img/logos/logo1.png'), 150, 10, 50);

    $this->pdf->ln();

    // Left Fields
    $this->pdf->SetFont('Arial', 'B', 11);
    $this->pdf->Cell(25, 10, 'Inv#');

    $this->pdf->SetFont('Arial', '', 11);
    $this->pdf->Cell(25, 10, '12');

    $this->pdf->ln(6);

    $this->pdf->SetFont('Arial', 'B', 11);
    $this->pdf->Cell(25, 10, 'Date');

    $this->pdf->SetFont('Arial', '', 11);
    $this->pdf->Cell(25, 10, 'Apr 16, 2014');

    $this->pdf->ln(6);

    $this->pdf->SetFont('Arial', 'B', 11);
    $this->pdf->Cell(25, 10, 'Customer');

    $this->pdf->SetFont('Arial', '', 11);
    $this->pdf->Cell(25, 10, 'AA Traders/Asim');

    $this->pdf->ln(6);

    $this->pdf->SetFont('Arial', 'B', 11);
    $this->pdf->Cell(25, 10, 'Mobile');

    $this->pdf->SetFont('Arial', '', 11);
    $this->pdf->Cell(25, 10, '0321-1019291');

    $this->pdf->SetFont('Arial', 'B', 20);
    $this->pdf->Cell(0, 10, 'Sale', 0, 0, 'R');

    $this->pdf->ln(15);

    ////////////////
    // Table Head //
    ////////////////
    $this->pdf->SetFont('Arial', 'B', 9);

    $this->pdf->Cell(7, 7, '#', 1, 0, 'C');
    $this->pdf->Cell(122, 7, 'Item', 1, 0, 'C');
    $this->pdf->Cell(15, 7, 'Qty', 1, 0, 'C');
    $this->pdf->Cell(15, 7, 'Rate', 1, 0, 'C');
    $this->pdf->Cell(30, 7, 'Amount', 1, 0, 'C');
    ////////////////////
    // End Table Head //
    ////////////////////

    $this->pdf->ln(7);

    ////////////////
    // Rows Start //
    ////////////////

    $this->pdf->SetFont('Arial', '', 9);

    $this->pdf->Cell(7, 20, '1', 1, 0, 'L');
    $this->pdf->Cell(122, 7, 'Some large item name to go here.', 1, 0, 'L');
    $this->pdf->Cell(15, 20, '12', 1, 0, 'R');
    $this->pdf->Cell(15, 20, '120', 1, 0, 'R');
    $this->pdf->Cell(30, 20, '12,000', 1, 0, 'R');

    $this->pdf->ln(7);
    $this->pdf->setX(17);
    $this->pdf->Cell(122, 7, 'Specs to go here.', 1, 0, 'L');

    $this->pdf->ln(7);
    $this->pdf->setX(17);
    $this->pdf->Cell(122, 6, 'Serial to go here.', 1, 0, 'L');

    //////////////
    // End Rows //
    //////////////

    $this->pdf->ln(7);

    $this->pdf->SetFont('Arial', 'B', 9);
    $this->pdf->Cell(30, 7, 'In words: ');

    $this->pdf->SetFont('Arial', '', 9);
    $this->pdf->Cell(100, 7, 'Seven thousand seven hundred point zero zero only.');

    $this->pdf->Cell(0, 7, '|||||||', 0, 0, 'R');

    $this->pdf->ln(5);

    $this->pdf->SetFont('Arial', 'B', 9);
    $this->pdf->Cell(30, 7, 'Prev Balance: ');


    $this->pdf->SetFont('Arial', '', 9);
    $this->pdf->Cell(100, 7, '12,122,122/=');

    $this->pdf->ln(15);

    //////////////////////
    // Signature Fields //
    //////////////////////

    $this->pdf->Cell(47, 7, 'Prepared By', 'T', 0, 'C');
    $this->pdf->Cell(20, 7, ' ', 0, 0, 'C');
    $this->pdf->Cell(48, 7, 'Accountant', 'T', 0, 'C');
    $this->pdf->Cell(20, 7, ' ', 0, 0, 'C');
    $this->pdf->Cell(47, 7, 'Received By', 'T', 0, 'C');

    $this->pdf->Output();

    //////////////////////////
    // END Signature fields //
    //////////////////////////
  }
  // More methods goes here
}
