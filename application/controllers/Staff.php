<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staff extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('items');
		$this->load->model('accounts');
		$this->load->model('departments');
		$this->load->model('staffdepartments');
		$this->load->model('shifts');
		$this->load->model('staffs');
		$this->load->model('ledgers');
		$this->load->model('levels');
        $this->load->model('settings');

	}

	public function index() {
		unauth_secure();
	}

	public function add() {

		unauth_secure();
		$data['modules'] = array('setup/staff');
		$data['types'] = $this->staffs->getAllTypes();
		$data['agreements'] = $this->staffs->getAllAgreements();
		$data['religions'] = $this->staffs->getAllReligions();
		$data['banks'] = $this->staffs->getAllBankNames();
		$data['desigs'] = $this->staffs->distincts_fields('designation');
		$data['departments'] = $this->departments->fetchAllDepartments();
		$data['staffdepartments'] = $this->staffdepartments->fetchAllDepartments();
		$data['shiftGroups'] = $this->shifts->fetchAllShiftGroups();
		$data['staffs'] = $this->staffs->fetchAll();
		$data['items'] = $this->items->fetchAll();
		$data['phases'] = $this->items->fetchAllPhase();

		$this->load->view('template/header.php');

		$this->load->view('setup/addstaff.php', $data);

		$this->load->view('template/mainnav.php');
		$this->load->view('template/footer.php', $data);
	}

	public function salarySheet() {

		unauth_secure();
		$data['modules'] = array('salary/preparesalary');
		$data['accounts1'] = $this->accounts->fetchAll(1,'salary');
		$data['accounts2'] = $this->departments->fetchAllDepartments('');
        $data['salaryPlan'] = $this->settings->get();

		$this->load->view('template/header.php');
		$this->load->view('salary/preparesalary.php', $data);
		$this->load->view('template/mainnav.php');
		$this->load->view('template/footer.php', $data);
	}

	public function overtime() {

		unauth_secure();
		$data['modules'] = array('overtime/overtime');
		$data['staffs'] = $this->staffs->fetchAll();
		$data['overtimes'] = $this->staffs->fetchAllOvertime();

		$this->load->view('template/header.php');

		$this->load->view('overtime/overtime.php', $data);

		$this->load->view('template/mainnav.php');
		$this->load->view('template/footer.php', $data);
	}

	public function getMaxId() {

		$result = $this->staffs->getMaxId() + 1;

		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($result));
	}

	public function getMaxOvertimeId() {
		$result = $this->staffs->getMaxOvertimeId() + 1;

		$this->output
			 ->set_content_type('application/json')
			 ->set_output(json_encode($result));
	}

	public function getMaxSalaryId() {

		if (isset($_POST)) {
			$etype = $_POST['etype'];

			$result = $this->staffs->getMaxSalaryId($etype) + 1;

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($result));
		}
	}

	public function save() {

		if (isset($_POST)) {

			$partyDetail = json_decode(html_entity_decode($_POST['acc'], true));
			$partyDetail = (array)$partyDetail;
			$ldetail = $this->levels->getLevel3ByName('ASSET EMPLOYEES');
			
			$partyDetail['level3'] = $ldetail['l3'];
			$pid = $this->accounts->save($partyDetail);

			$staid = $this->staffs->save($_POST, $pid);

			$salary = (array)(json_decode(html_entity_decode($_POST['salary'], true)));
			$this->staffs->saveSalary($salary);

			$qualifications = (array)(json_decode(html_entity_decode($_POST['quali'], true)));
			$this->staffs->saveQualification($qualifications, $staid);

			$experiences = (array)(json_decode(html_entity_decode($_POST['exp'], true)));
			$this->staffs->saveExperience($experiences, $staid);

			$phases = (array)(json_decode(html_entity_decode($_POST['phase'], true)));
			$this->staffs->savePhases($phases, $staid);

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode('true'));
		}
	}

	public function saveOvertime() {

		if (isset($_POST)) {

			$overtime = $_POST['overtime'];
			$result = $this->staffs->saveOvertime($overtime);

			$response = array();
			if ( $result === false ) {
				$response['error'] = 'true';
			} else {
				$response['error'] = 'false';
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function saveSalarySheet() {

		if (isset($_POST)) {

			$pledgers = json_decode($_POST['pledgers'],true);
			$salarysheet = json_decode($_POST['salarysheet'],true);
			$dcno = $_POST['dcno'];
			$etype = $_POST['etype'];
			$voucher_type_hidden = $_POST['voucher_type_hidden'];			
			if($voucher_type_hidden == 'new'){

				$dcno = $this->staffs->getMaxSalaryId($etype) + 1;
			}
			$result = $this->staffs->saveSalarySheet($salarysheet, $dcno);
			$result = $this->ledgers->save($pledgers, $dcno, $etype,$voucher_type_hidden);

			$response = array();
			if ( $result === false ) {
				$response['error'] = 'true';
			} else {
				$response['error'] = 'false';
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchStaff() {

		if (isset( $_POST )) {

			$staid = $_POST['staid'];

			$staff = $this->staffs->fetchStaff($staid);
			$salary = $this->staffs->fetchStaffSalary($staid);
			$quali = $this->staffs->fetchStaffQualification($staid);
			$exp = $this->staffs->fetchStaffExperience($staid);
			$phase = $this->staffs->fetchStaffPhase($staid);

			$response = array();
			if ( $staff === false ) {
				$response = 'false';
			} else {
				$response['staff'] = $staff;
				$response['salary'] = $salary;
				$response['quali'] = $quali;
				$response['exp'] = $exp;
				$response['phase'] = $phase;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchOvertime() {

		if (isset( $_POST )) {

			$dcno = $_POST['dcno'];
			$result = $this->staffs->fetchOvertime($dcno);

			$response = array();
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}	
	}

	public function fetchSalarySheet() {

		if (isset( $_POST )) {

			$dcno = $_POST['dcno'];
			$etype = $_POST['etype'];
			$result = $this->staffs->fetchSalarySheet($dcno,$etype);

			$response = array();
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = $result;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}	
	}
	public function fetchOvertimereport()
    {
        $pid =  $_POST['pid'];
        $did =  $_POST['did'];
        $from = $_POST['from'];
        $to =   $_POST['to'];

        $result = $this->staffs->fetchOvertimeReport($from, $to, $pid, $did);
        $this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($result));
    }
	public function fetchStaffReportByStatus() {

		if (isset( $_POST )) {

			$status = $_POST['status'];
			$did = $_POST['did'];

			$results = $this->staffs->fetchStaffReportByStatus($status, $did);

			$response = array();
			if ( $results === false ) {
				$response = 'false';
			} else {
				$response = $results;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function fetchByDepartment() {

		if (isset( $_POST )) {

			$did = $_POST['did'];
			$results = $this->staffs->fetchByDepartment($did);

			$response = array();
			if ( $results === false ) {
				$response = 'false';
			} else {
				$response = $results;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function getSalary() {

		if (isset( $_POST )) {

			$from = $_POST['from'];
			$to = $_POST['to'];
			$did = $_POST['warehouse'];

			$results = $this->staffs->getSalary($from, $to, $did);

			$response = array();
			if ( $results === false ) {
				$response = 'false';
			} else {
				$response = $results;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function deleteOvertime() {

		if (isset($_POST)) {

			$result = $this->staffs->deleteOvertime($_POST['dcno']);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = 'true';
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function deleteSalarySheet() {

		if (isset($_POST)) {

			$dcno = $_POST['dcno'];
			$etype = $_POST['etype'];
            $companyId = $_POST['cid'];
			
			$result = $this->ledgers->deleteVoucher($dcno, $etype, $companyId);
			$result = $this->staffs->deleteSalarySheet($_POST['dcno'],$etype);

			$response = "";
			if ( $result === false ) {
				$response = 'false';
			} else {
				$response = 'true';
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}

	public function salarySheet_contract() {

		unauth_secure();
		$data['modules'] = array('salary/preparesalary_contract');
		$data['accounts1'] = $this->accounts->fetchAll(1,'salary');
		$data['accounts2'] = $this->departments->fetchAllDepartments('');
        $data['salaryPlan'] = $this->settings->get();

		$this->load->view('template/header.php');
		$this->load->view('salary/preparesalary_contract.php', $data);
		$this->load->view('template/mainnav.php');
		$this->load->view('template/footer.php', $data);
	}

	public function getSalary_contract() {

		if (isset( $_POST )) {

			$from = $_POST['from'];
			$to = $_POST['to'];
			$did = $_POST['warehouse'];

			$results = $this->staffs->getSalary_contract($from, $to, $did);

			$response = array();
			if ( $results === false ) {
				$response = 'false';
			} else {
				$response = $results;
			}

			$this->output
				 ->set_content_type('application/json')
				 ->set_output(json_encode($response));
		}
	}
}