<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Opening_stock extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('accounts');
        $this->load->model('salesmen');
        $this->load->model('transporters');
        $this->load->model('items');
        $this->load->model('ledgers');
        $this->load->model('departments');
        $this->load->model('sales');
        $this->load->model('purchases');
        $this->load->model('users');
        $this->load->model('levels');
        $this->load->model('orders');
        $this->load->model('datecloses');
    }

    public function index()
    {

        $data['modules'] = array('inventory/addopeningstock');
        $data['l3s'] = $this->levels->fetchAllLevel3();
        $data['accounts'] = $this->accounts->fetchAll();
        $data['userone'] = $this->users->fetchAll();
        $data['departments'] = $this->departments->fetchAllDepartments();
        

        $this->load->view('template/header');
        $this->load->view('inventory/addopeningstock', $data);
        $this->load->view('template/mainnav');
        $this->load->view('template/footer', $data);
    }

    public function add()
    {

    }

    public function getMaxId()
    {
        $company_id = $_POST['company_id'];
        $maxId = $this->purchases->getMaxVrnoa('openingstock', $company_id) + 1;
        $this->output->set_content_type('application/json')->set_output(json_encode($maxId));
    }

   public function save() {

        if (isset($_POST)) {

            $stockmain = json_decode($_POST['stockmain'], true);
            $stockmain = (array)$stockmain;
            $stockdetail = json_decode($_POST['stockdetail'], true);
            $stockdetail = (array)$stockdetail;

            $vrnoa = $_POST['vrnoa'];
            $voucher_type_hidden = $_POST['voucher_type_hidden'];

            if ($voucher_type_hidden=='new'){
                
                $vrnoa = $this->purchases->getMaxVrnoa('openingstock', $stockmain['company_id']) + 1;
                $stockmain['vrnoa'] = $vrnoa;
            }

             ///////////////////// Validation Close Date String
            $response = array();
            
            $chk_date = $_POST['vrdate'];
            $vrdate = "2016-01-01";
            $vrdate = $stockmain['vrdate'];

            $DateCloseStatus=false;
            if($chk_date!=$vrdate &&  $voucher_type_hidden=='edit'){
                $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
            }
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }

            $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
            
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }

            ///////////////////// Validation End

            $result = $this->purchases->save($stockmain, $stockdetail, $vrnoa, 'openingstock');


            $response = array();
            if ( $result === false ) {
                $response['error'] = 'true';
            } else {
                $response = $result;
            }

            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }

    public function fetch() {

        if (isset( $_POST )) {

            $vrnoa = $_POST['vrnoa'];
            $company_id = $_POST['company_id']; 
            $result = $this->purchases->fetch($vrnoa, 'openingstock',$company_id);

            $response = "";
            if ( $result === false ) {
                $response = 'false';
            } else {
                $response = $result;
            }

            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }

    public function deletevoucher() {

        if (isset( $_POST )) {

            $vrnoa = $_POST['vrnoa'];
            $company_id = $_POST['company_id'];

              ///////////////////// Validation Close Date String
            $response = array();
            
            $chk_date = $_POST['chk_date'];
            $vrdate = "2016-01-01";
            $vrdate = $_POST['vrdate'];

            $DateCloseStatus=false;
            if($chk_date!=$vrdate){
                $DateCloseStatus = $this->datecloses->CheckDateClose($chk_date);    
            }
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }

            $DateCloseStatus = $this->datecloses->CheckDateClose($vrdate);
            
            if($DateCloseStatus==true){
                $response['error'] = 'date close';
                return $this->output->set_content_type('application/json')->set_output(json_encode($response));
            }

            ///////////////////// Validation End

            $result = $this->purchases->delete($vrnoa, 'openingstock' ,$company_id);

            $response = "";
            if ( $result === false ) {
                $response = 'false';
            } else {
                $response = $result;
            }

            $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($response));
        }
    }

    public function fetchVoucherRange()
    {
        $from = $_POST['from'];
        $to = $_POST['to'];

        $result = $this->purchases->fetchVoucherRange($from, $to, 'openingstock');
        $this->output
                 ->set_content_type('application/json')
                 ->set_output(json_encode($result));
    }
}
