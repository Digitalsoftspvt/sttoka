<!-- main content -->
<div id="main_wrapper">

    <div class="page_bar">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page_title">Inward Gatepass Voucher</h1>
            </div>
        </div>
    </div>

    <div class="page_content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <form action="">

                                        <div class="row">
                                            <div class="col-lg-2">
                                                <div class="input-group">
                                                    <span class="input-group-addon id-addon">IGP#</span>
                                                    <input type="number" class="form-control num" id="txtVrnoa" >
                                                    <input type="hidden" id="txtMaxVrnoaHidden">
                                                    <input type="hidden" id="txtVrnoaHidden">
                                                    <input type="hidden" id="voucher_type_hidden">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon txt-addon">Date</span>
                                                    <input class="form-control ts_datepicker" type="text" id="current_date">
                                                    <input class="form-control ts_datepicker" type="hidden" id="vrdate">
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="input-group">
                                                    <span class="input-group-addon txt-addon">Vr#</span>
                                                    <input type="text" class="form-control" id="txtVrno" readonly='true'>
                                                    <input type="hidden" id="txtMaxVrnoHidden">
                                                    <input type="hidden" id="txtVrnoHidden">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">

                                                <div class="input-group">
                                                    <span class="input-group-addon txt-addon">Party Name</span>
                                                    <select class="form-control select2" id="party_dropdown">
                                                        <option value="" disabled="" selected="">Choose party</option>
                                                        <?php foreach ($parties as $party): ?>
                                                            <option value="<?php echo $party['pid']; ?>"  data-uname="<?php echo $party['uname']; ?>" data-uaddress="<?php echo $party['uaddress']; ?>"><?php echo $party['name']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>

                                            </div>

                                            <!-- <div class="col-lg-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon txt-addon">Country</span>
                                                    <input type="text" class="form-control" id="txtCountry" readonly='true'>
                                                </div>
                                            </div>

                                            <div class="col-lg-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon txt-addon">City</span>
                                                    <input type="text" class="form-control" id="txtCity" readonly='true'>
                                                </div>
                                            </div> -->
                                        </div>

                                      

                                        

                                        <div class="row">
                                            
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon txt-addon">Remarks</span>
                                                    <input type="text" class="form-control" id="txtRemarks">
                                                </div>
                                            </div>
                                           
                                        </div>

                                        <div class="row"></div>

                                        <div class="container-wrap">
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <div class="input-group">
                                                        <span class="input-group-addon" style='min-width: 50px;'><span class="fa fa-barcode"></span></span>
                                                        <select class="form-control select2" id="itemid_dropdown">
                                                            <option value="" disabled="" selected="">Item Id</option>
                                                            <?php foreach ($items as $item): ?>
                                                                <option value="<?php echo $item['item_id']; ?>" data-uom="<?php echo $item['uom']; ?>"><?php echo $item['item_id']; ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <select class="form-control select2" id="item_dropdown">
                                                        <option value="" disabled="" selected="">Item description</option>
                                                        <?php foreach ($items as $item): ?>
                                                            <option value="<?php echo $item['item_id']; ?>" data-uom="<?php echo $item['uom']; ?>"><?php echo $item['item_des']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3">
                                                    <select class="form-control select2" id="dept_dropdown">
                                                        <option value="" disabled="" selected="">Department</option>
                                                        <?php foreach ($departments as $department): ?>
                                                            <option value="<?php echo $department['did']; ?>"><?php echo $department['name']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                                <div class="col-lg-2" style='width: 12%;'>
                                                    <div class="input-group">
                                                        <span class="input-group-addon" style='min-width: 50px;'>Qty</span>
                                                        <input type="text" class="form-control num" id="txtSQty">
                                                    </div>
                                                </div>
                                                <div class="col-lg-2" style='width: 12%;'>
                                                    <div class="input-group">
                                                        <span class="input-group-addon" style='min-width: 55px;'>Weight</span>
                                                        <input type="text" class="form-control num" id="txtWeight">
                                                    </div>
                                                </div>
                                                <div class="col-lg-1">
                                                    <div class="input-group">
                                                        <a href="" class="btn btn-primary" id="btnAdd">+</a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row"></div>
                                        <div class="row"></div>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <table class="table table-striped" id="purchase_table">
                                                    <thead>
                                                        <tr>
                                                            <th>Sr#</th>
                                                            <th>Item Name</th>
                                                            <th>Department</th>
                                                            <th>Weight</th>
                                                            <th>Qty</th>
                                                           
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </form> <!-- end of form -->

                                </div>  <!-- end of panel-body -->
                            </div>  <!-- end of panel -->
                        </div>  <!-- end of col -->
                    </div>  <!-- end of row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-lg-5">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset</a>
                                                    <a class="btn btn-primary btnSave"><i class="fa fa-save"></i> Save Changes</a>
                                                    <a class="btn btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete</a>
                                                    <a class="btn btn-default btnPrint"><i class="fa fa-print"></i> Print</a>
                                                </div>
                                            </div>
                                        </div>  <!-- end of col -->

                                        <div class="col-lg-7">
                                            <div class="row">
                                                <div class="col-lg-9">                                                    
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="input-group">
                                                        <span class="input-group-addon fancy-addon" style='min-width:50px;'>Qty</span>
                                                        <input type="text" class="form-control num" id="txtGQty" readonly="true">
                                                        <input type="hidden" class="form-control num" id="txtGweight" readonly="true">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>  <!-- end of row -->
                                </div>
                            </div>
                        </div>
                    </div>

                </div>  <!-- end of level 1-->
            </div>
        </div>
    </div>
</div>