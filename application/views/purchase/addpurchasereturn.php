<?php

    $desc = $this->session->userdata('desc');
    $desc = json_decode($desc);
    $desc = objectToArray($desc);

    $vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">
<div id="AccountAddModel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="AccountAddModelLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header" style="background:#2477a4;color:white;padding-bottom:20px;">
                    <button type="button" class="modal-button cellRight modal-close pull-right btn-close" data-dismiss="modal"><span class="fa fa-times" style="font-size:26px; "></span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="AccountAddModelLabel">Add New Account</h4>
                </div>
               <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-lg-9 col-lg-offset-1">
                                <form role="form">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label for="exampleInputEmail1">Name</label>
                                                <input type="text" id="txtAccountName" class="form-control" placeholder="Account Name" maxlength="50" tabindex="1">
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Acc Type3</label>
                                                <select class="form-control "  id="txtLevel3" tabindex="2">
                                                    <option value="" disabled="" selected="">Choose Account Type</option>
                                                    <?php foreach ($l3s as $l3): ?>
                                                        <option value="<?php echo $l3['l3']; ?>" data-level2="<?php echo $l3['level2_name']; ?>" data-level1="<?php echo $l3['level1_name']; ?>"><?php echo $l3['level3_name'] ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-12">
                                                <span><b>Type 2 &rarr; </b><span id="txtselectedLevel2"> </span></span> <span><b>Type 1 &rarr; </b><span id="txtselectedLevel1"> </span></span>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>       
                </div>
                <div class="modal-footer">
                    <div class="pull-right">
                        <a class="btn btn-success btnSaveM btn-sm" data-insertbtn="1"><i class="fa fa-save"></i> Save</a>
                        <a class="btn btn-warning btnResetM btn-sm"><i class="fa fa-refresh"></i> Reset</a>
                        <a class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="ItemAddModel" class="modal hide fade" role="dialog" aria-labelledby="ItemAddModelLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header" style="background:#2477a4;color:white;padding-bottom:20px;">
                    <button type="button" class="modal-button cellRight modal-close pull-right btn-close" data-dismiss="modal"><span class="fa fa-times" style="font-size:26px; "></span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="ItemAddModelLabel">Add New Item</h4>
                </div>
               <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-lg-9 col-lg-offset-1">
                                <form role="form">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label for="exampleInputEmail1">Description</label>
                                                <input type="text" id="txtItemName" class="form-control" placeholder="Account Name" maxlength="50" tabindex="1">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label>Category</label>
                                                <select class="form-control select2" id="category_dropdown" tabindex="2">
                                                    <option value="" disabled="" selected="">Choose Category</option>
                                                    <?php foreach ($categories as $category): ?>
                                                        <option value="<?php echo $category['catid']; ?>"><?php echo $category['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Sub Catgeory</label>
                                                <select class="form-control select2" id="subcategory_dropdown" tabindex="3">
                                                    <option value="" disabled="" selected="">Choose sub category</option>
                                                    <?php foreach ($subcategories as $subcategory): ?>
                                                        <option value="<?php echo $subcategory['subcatid']; ?>"><?php echo $subcategory['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>    
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label>Brand</label>
                                                <select class="form-control select2" id="brand_dropdown" tabindex="4">
                                                    <option value="" disabled="" selected="">Choose brand</option>
                                                    <?php foreach ($brands as $brand): ?>
                                                        <option value="<?php echo $brand['bid']; ?>"><?php echo $brand['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Type</label>
                                                <input type="text" list='type' class="form-control" id="txtBarcode" tabindex="5" />
                                                <datalist id='type'>
                                                    <?php foreach ($types as $type): ?>
                                                        <option value="<?php echo $type['barcode']; ?>">
                                                    <?php endforeach ?>
                                                </datalist>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <label>Sale Price</label>
                                                <input class="form-control num" type="text" id="txtSalePrice" tabindex="6">
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Pur Price</label>
                                                <input class="form-control num" type="text" id="txtPurPrice" tabindex="7">
                                            </div>
                                            <div class="col-lg-6">
                                                <label>UOM</label>
                                                <input type="text" class='form-control' tabindex="8" placeholder="Uom" id="uom_dropdown" list='uoms'>
                                                <datalist id="uoms">
                                                    <?php foreach ($uoms as $uom): ?>
                                                        <?php if ($uom['uom'] !== ''): ?>
                                                            <option value="<?php echo $uom['uom']; ?>">
                                                        <?php endif ?>
                                                    <?php endforeach ?>
                                                </datalist>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>       
                </div>
                <div class="modal-footer">
                    <div class="pull-right">
                        <a class="btn btn-success btnSaveMItem btn-sm" data-insertbtn="1" tabindex="8"><i class="fa fa-save"></i> Save</a>
                        <a class="btn btn-warning btnResetMItem btn-sm" tabindex="9"><i class="fa fa-refresh"></i> Reset</a>
                        <a class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="GodownAddModel" class="modal hide fade" role="dialog" aria-labelledby="GodownAddModelLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header" style="background:#2477a4;color:white;padding-bottom:20px;">
                    <button type="button" class="modal-button cellRight modal-close pull-right btn-close" data-dismiss="modal"><span class="fa fa-times" style="font-size:26px; "></span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="GodownAddModelLabel">Add New Department</h4>
                </div>
               <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-lg-9 col-lg-offset-1">
                                <form role="form">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label for="exampleInputEmail1">Description</label>
                                                <input type="text" id="txtNameGodownAdd" class="form-control" placeholder="Department Name" maxlength="50" tabindex="1">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>       
                </div>
                <div class="modal-footer">
                    <div class="pull-right">
                        <a class="btn btn-success btnSaveMGodown btn-sm" data-insertbtn="1" tabindex="8"><i class="fa fa-save"></i> Save</a>
                        <a class="btn btn-warning btnResetMGodown btn-sm" tabindex="9"><i class="fa fa-refresh"></i> Reset</a>
                        <a class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page_bar">
        <div class="row">
            <div class="col-md-5">
                <h1 class="page_title">Purchase Return Voucher</h1>
            </div>
            <div class="col-md-7">
                <a class="btn btn-sm btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                                                <a class="btn btn-sm btn-default btnSave1" data-savegodownbtn='<?php echo $vouchers['warehouse']['insert']; ?>' data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['purchasereturnvoucher']['insert']; ?>' data-updatebtn='<?php echo $vouchers['purchasereturnvoucher']['update']; ?>' data-deletebtn='<?php echo $vouchers['purchasereturnvoucher']['delete']; ?>' data-printbtn='<?php echo $vouchers['purchasereturnvoucher']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
                                                <a class="btn btn-sm btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>
                                                <!-- <a class="btn btn-sm btn-default btnPrint"><i class="fa fa-print"></i> Print F9</a>                                     -->
                                                <div class="btn-group">
                                                  <button type="button" class="btn btn-primary btn-lg btnPrint" ><i class="fa fa-save"></i>Print F9</button>
                                                  <button type="button" class="btn btn-primary btn-lg dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                   <span class="caret"></span>
                                                   <span class="sr-only">Toggle Dropdown</span>
                                                 </button>
                                                 <ul class="dropdown-menu" role="menu">
                                                   <li ><a href="#" class="btnprintThermal">Thermal Print</a></li>
                                                   <!-- <li ><a href="#" class="btnprintHeader"> Print with header</li>
                                                   <li ><a href="#" class="btnprintwithOutHeader"> Print with Out header</li>
                                                   <li ><a href="#" class="btnprint_sm">Small</li>
                                                   <li ><a href="#" class="btnprint_sm_withOutHeader">Small with out header </li>
                                                   <li ><a href="#" class="btnprint_sm_rate">Small with out rate</li>
                                                   <li ><a href="#" class="btnprint_sm_withOutHeader_rate">Small with out header and rate </li> -->
                                                 </ul>
                                                </div>
                                                <a href="#party-lookup" data-toggle="modal" class="btn btn-sm btn-default btnsearchparty "><i class="fa fa-search"></i>&nbsp;Account Lookup F1</a>
                                                <!-- <a href="#item-lookup" data-toggle="modal" class="btn btn-sm btn-default btnsearchitem" style="color:black;"><i class="fa fa-search"></i>&nbsp;Item Lookup F2</a> -->
            </div>
        </div>
    </div>

    <div class="page_content">
        <div class="container-fluid">
            <div class="row ">
                <div class="col-lg-12">
                    <ul class="nav nav-pills">
                        <li class="active"><a href="#Main" data-toggle="tab">Main</a></li>
                        <li><a href="#Attachments" data-toggle="tab">Attachments</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div class="row">
                        <div class="col-lg-10">
                            <div class="tab-content">
                                <div class="tab-pane active fade in" id="Main">
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <form action="">

                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon id-addon">Sr#</span>
                                                    <input type="number" class="form-control " id="txtVrnoa" >
                                                    <input type="hidden" id="txtMaxVrnoaHidden">
                                                    <input type="hidden" id="txtVrnoaHidden">
                                                    <input type="hidden" id="voucher_type_hidden">

                                                    <input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                                                    <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                                                    <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">

                                                    <input type="hidden" id="purchasereturnid" value="<?php echo $setting_configur[0]['purchasereturn']; ?>">
                                                    <input type="hidden" id="discountid" value="<?php echo $setting_configur[0]['discount']; ?>">
                                                    <input type="hidden" id="expenseid" value="<?php echo $setting_configur[0]['expenses']; ?>">
                                                    <input type="hidden" id="taxid" value="<?php echo $setting_configur[0]['tax']; ?>">
                                                    <input type="hidden" id="cashid" value="<?php echo $setting_configur[0]['cash']; ?>">

                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="input-group">
                                                    <span class="input-group-addon">Vr#</span>
                                                    <input type="text" class="form-control " id="txtVrno" readonly='true'>
                                                    <input type="hidden" id="txtMaxVrnoHidden">
                                                    <input type="hidden" id="txtVrnoHidden">
                                                </div>
                                            </div>                                               
                                            <div class="col-lg-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon">Date</span>
                                                    <input class="form-control  ts_datepicker" type="text" id="current_date">
                                                    <input class="form-control  ts_datepicker" type="hidden" id="vrdate">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            
                                            <div class="col-lg-4">                                                
                                                <label id="balance_lbl">Party Name</label>
                                                <div class="input-group" >
                                                <select class="form-control  select2" id="party_dropdown">
                                                    <option value="" disabled="" selected="">Choose party</option>
                                                    <?php foreach ($parties as $party): ?>
                                                        <option value="<?php echo $party['pid']; ?>" data-uname="<?php echo $party['uname']; ?>"><?php echo $party['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                                <a  tabindex="-1" class="input-group-addon btn btn-primary active" style="min-width:40px !important;" id="A2" data-target="#AccountAddModel" data-toggle="modal" href="#addCategory" rel="tooltip"
                                                data-placement="top" data-original-title="Add Category" data-toggle="tooltip" data-placement="bottom" title="Add New Account Quick (F3)"><i class="fa fa-plus"></i></a>
                                                </div>                                                
                                            </div>
                                            <div class="col-lg-3">                                                
                                                <label>Warehouse</label>
                                                <div class="input-group" >
                                                <select class="form-control select2" id="dept_dropdown">
                                                    <option value="" selected="" disabled="">Choose Warehouse</option>
                                                    <?php foreach ($departments as $department): ?>
                                                        <option value="<?php echo $department['did']; ?>"><?php echo $department['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                                <a  tabindex="-1" class="input-group-addon btn btn-primary active" style="min-width:40px !important;" id="A2" data-target="#GodownAddModel" data-toggle="modal" href="#addCategory" rel="tooltip"
                                                data-placement="top" data-original-title="Add Department" data-toggle="tooltip" data-placement="bottom" title="Add New Department Quick"><i class="fa fa-plus"></i></a>
                                                </div> 
                                                                           
                                            </div>
                                            <div class="col-lg-2">                                                
                                                <label>Received By</label>
                                                <input class='form-control ' type='text' list="receivers" id='receivers_list'>
                                                <datalist id='receivers'>
                                                    <?php foreach ($receivers as $receiver): ?>
                                                        <option value="<?php echo $receiver['received_by']; ?>">
                                                    <?php endforeach ?>
                                                </datalist>                                                
                                            </div>                                    
                                                                                 
                                            <div class="col-lg-3">                                                
                                                <label>Through</label>
                                                <select class="form-control  select2" id="transporter_dropdown">
                                                    <option value="" disabled="" selected="">Choose transporter</option>
                                                    <?php foreach ($transporters as $transporter): ?>
                                                        <option value="<?php echo $transporter['transporter_id']; ?>"><?php echo $transporter['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>                                               
                                            </div>
                                        </div>                                        

                                        <div class="row">
                                            <div class="col-lg-2">                                                
                                                <label>Inv#</label>
                                                <input type="text" class="form-control num" id="txtInvNo">                                                
                                            </div>
                                            <div class="col-lg-2">                                                
                                                <label>Due Date</label>
                                                <input class="form-control  ts_datepicker" type="text" id="due_date">                                                
                                            </div>                                           
                                            <div class="col-lg-3 hide">                                                
                                                <label>PO#</label>
                                                <input type="text" class="form-control" id="txtOrderNo">    
                                            </div>                                                                                
                                            <div class="col-lg-8">                                            
                                                <label>Remarks</label>
                                                <input type="text" class="form-control" id="txtRemarks">                                                                                                                                                                             
                                            </div>
                                        </div>
                                        <!-- <div class="row"></div> -->

                                        <div class="container-wrap" style="margin-top:20px;">
                                            <div class="row">
                                                <!-- <div class="col-lg-2" >
                                                    <label for="">Item Code</label>
                                                    <div class="input-group">
                                                        
                                                        <span class="input-group-addon" style='min-width: 0px;'><span class="fa fa-barcode"></span></span>
                                                        <select class="form-control select2" id="itemid_dropdown">
                                                            <option value="" disabled="" selected="">Item Code</option>
                                                            <?php foreach ($items as $item): ?>
                                                                <option value="<?php echo $item['item_id']; ?>" data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>" data-stqty="<?php echo $item['stqty']; ?>" data-stweight="<?php echo $item['stweight']; ?>"><?php echo $item['item_id']; ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3" >
                                                    <label for="" id="stqty_lbl" >Item Description</label>
                                                    <div class="input-group" >
                                                    <select class="form-control select2" id="item_dropdown">
                                                        <option value="" disabled="" selected="">Item description</option>
                                                        <?php foreach ($items as $item): ?>
                                                            <option value="<?php echo $item['item_id']; ?>" data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>" data-stqty="<?php echo $item['stqty']; ?>" data-stweight="<?php echo $item['stweight']; ?>" ><?php echo $item['item_des']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                    <a class="input-group-addon btn btn-primary active"   tabindex="-1" style="min-width:40px !important;" id="A3" data-target="#ItemAddModel" data-toggle="modal" href="#addItem" rel="tooltip"
                                                    data-placement="top" data-original-title="Add Item" data-toggle="tooltip" data-placement="bottom" title="Add New Account Quick (F3)"><i class="fa fa-plus"></i></a>
                                                    <a class="input-group-addon btn btn-primary active btnsearchitem" href="#item-lookup" data-toggle="modal"  style="min-width:40px !important;"><i class="fa fa-search"></i></a>
                                                    </div>
                                                </div> -->

                                                <div class="col-lg-5" >
                                                    <label for="">Item Code List <img id="imgItemLoader" class="hide" src="<?php echo base_url(); ?>/assets/img/loader.gif"></label>
                                                    <input type="text" class="form-control" id="txtItemId">
                                                    <input id="hfItemId" type="hidden" value="" />
                                                    <input id="hfItemSize" type="hidden" value="" />
                                                    <input id="hfItemBid" type="hidden" value="" />
                                                    <input id="hfItemUom" type="hidden" value="" />
                                                    <input id="hfItemPrate" type="hidden" value="" />
                                                    <input id="hfItemGrWeight" type="hidden" value="" />
                                                    <input id="hfItemStQty" type="hidden" value="" />
                                                    <input id="hfItemStWeight" type="hidden" value="" />
                                                    <input id="hfItemLength" type="hidden" value="" />
                                                    <input id="hfItemCatId" type="hidden" value="" />
                                                    <input id="hfItemSubCatId" type="hidden" value="" />
                                                    <input id="hfItemDesc" type="hidden" value="" />
                                                    <input id="hfItemUname" type="hidden" value="" />
                                                    <input id="hfItemShortCode" type="hidden" value="" />
                                                </div>
                                                <div class="col-lg-5">
                                                    <label for="" id="stqty_lbl">Item</label>
                                                    <input type="text" class="form-control" id="itemDesc" readonly="" tabindex="-1">
                                                </div>
                                                <div class="col-lg-1">
                                                    <label for="">Qty</label>                                                    
                                                    <input type="text" class="form-control num" id="txtQty">                                                    
                                                </div>
                                                 <div class="col-lg-1">
                                                    <label for="">GW</label>                                                    
                                                    <input type="text" class="form-control readonly num" id="txtGWeight" readonly="" tabindex="-1">                                                    
                                                </div>
                                            </div>
                                            <div class="row"> 
                                                <div class="col-lg-1">
                                                    <label for="">Uom</label>                                                    
                                                    <input type="text" class="form-control readonly num" id="txtUom" readonly="" tabindex="-1">                                                    
                                                </div>
                                                <div class="col-lg-1">
                                                    <label for="">Weight</label>                                                    
                                                    <input type="text" class="form-control num" id="txtWeight">                                                    
                                                </div>
                                                <div class="col-lg-1">
                                                    <label for="">Rate</label>                                                    
                                                    <input type="text" class="form-control num" id="txtPRate">                                                    
                                                </div>
                                                <div class="col-lg-2">
                                                    <label for="">Amount</label>                                                    
                                                    <input type="text" class="form-control readonly num" id="txtAmount" readonly="true" tabindex="-1">                                                    
                                                </div>
                                                <div class="col-lg-1" style='margin-top: 30px;'>                                                    
                                                    <a href="" class="btn btn-primary" id="btnAdd"><i class="fa fa-plus"></i></a>
                                                </div>                                                
                                            </div>
                                        </div>
                                        <!-- <div class="row"></div> -->
                                        <!-- <div class="row"></div> -->

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div id="no-more-tables">
                                                <table class="col-lg-12 table table-bordered table-striped table-condensed cf" id="purchasereturn_table">
                                                    <thead class="cf">
                                                        <tr>
                                                            <th class="numeric">Sr#</th>
                                                            <th>Item Detail</th>
                                                            <th>Warehouse</th>
                                                            <th class="numeric">Qty</th>
                                                            <th class="numeric">Weight</th>
                                                            <th class="numeric">Rate</th>
                                                            <th class="numeric">Amount</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                   <!--  <tfoot style='border-top:1px solid !important;'>
                                                        <tr>
                                                            <td></td>
                                                            <td style='color:red !important; text-align:right;'>Total:</td>                                                            
                                                            <td style='color:red !important;'></td>
                                                            <td style='color:red !important;'></td>
                                                            <td></td>                                                                  
                                                            <td style='color:red !important;'></td>
                                                            <td></td>
                                                        </tr>
                                                    </tfoot> -->

                                                </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="party-lookup" class="modal fade modal-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                        <h3 id="myModalLabel"><i class='fa fa-search'></i> Party Lookup</h3>
                                                    </div>

                                                        <div class="modal-body mrgtop">
                                                        <table class="table table-striped modal-table">
                                                        <!-- <table class="table table-bordered table-striped modal-table"> -->
                                                        <thead>
                                                        <tr style="font-size:16px;">
                                                        <th>Id</th>
                                                        <th>Name</th>
                                                        <th>Mobile</th>
                                                        <th>Address</th>
                                                        <th style='width:3px;'>Actions</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach ($parties as $party): ?>
                                                        <tr>
                                                        <td width="14%;">
                                                        <?php echo $party['account_id']; ?>
                                                        <input type="hidden" name="hfModalPartyId" value="<?php echo $party['pid']; ?>">
                                                        </td>
                                                        <td><?php echo $party['name']; ?></td>
                                                        <td><?php echo $party['mobile']; ?></td>
                                                        <td><?php echo $party['address']; ?></td>
                                                        <td><a href="#" data-dismiss="modal" class="btn btn-primary populateAccount"><i class="glyphicon glyphicon-ok"></i></a></td>
                                                        </tr>
                                                        <?php endforeach ?>
                                                        </tbody>
                                                        </table>
                                                        </div>
                                                        <div class="modal-footer">
                                                        <!-- <button class="btn btn-danger delete-modal-del">Delete</button> -->
                                                        <button class="btn btn-primary pull-right" data-dismiss="modal">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="item-lookup" class="modal fade modal-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                        <h3 id="myModalLabel">item Lookup</h3>
                                                    </div>

                                                        <div class="modal-body">
                                                        <div style="width:250px; float: right !important;top:-10px; position: relative; bottom:30px;">
                                                            <label for="">Search</label>
                                                            <input type="text" class="form-control" id="txtItemSearch">
                                                        </div>
                                                        <table class="table" id="tbItems">
                                                        <!-- <table class="table table-bordered table-striped modal-table"> -->
                                                        <thead>
                                                        <tr style="font-size:16px;">
                                                            <th>Id</th>
                                                            <th>Code</th>
                                                            <th>Description</th>
                                                            <th>Uom</th>
                                                            <th style='width:3px;'>Actions</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                        </table>
                                                        <div id="divNoRecord" class="divNoRecord"></div>
                                                            <div id="paging"></div>
                                                        </div>
                                                        <div class="modal-footer">
                                                        <!-- <button class="btn btn-danger delete-modal-del">Delete</button> -->
                                                        <button class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </form> <!-- end of form -->

                                </div>  <!-- end of panel-body -->
                            </div>  <!-- end of panel -->
                            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">                                    
                                        <div class="col-lg-3">                                                    
                                            <label>Total Weight</label>
                                            <input type="text" class="form-control readonly num" id="txtTotalWeight" readonly="true" tabindex="-1">
                                        </div>                                                
                                        <div class="col-lg-3">                                                    
                                            <label>Total Qty</label>
                                            <input type="text" class="form-control readonly num" id="txtTotalQty" readonly="true" tabindex="-1">
                                        </div>                                                
                                        <div class="col-lg-3">                                                    
                                            <label>Total Amount</label>
                                            <input type="text" class="form-control readonly num" id="txtTotalAmount" readonly="true" tabindex="-1">
                                        </div>
                                        <div class="col-lg-3">                                                    
                                            <label>Paid</label>
                                            <input type="text" class="form-control num" id="txtPaid">
                                        </div>                                                
                                    </div>
                                    <div class="row">                                           
                                        <div class="col-lg-1">                                                    
                                            <label>Discount%</label>
                                            <input type="text" class=" form-control num"  id="txtDiscount">
                                        </div>                                                
                                        <div class="col-lg-2">                                                    
                                            <label>DiscAmount</label>
                                            <input type="text" class=" form-control num"  id="txtDiscAmount">
                                        </div>
                                        <div class="col-lg-1 hide">                                                    
                                            <label>Expense%</label>
                                            <input type="text" class=" form-control num"  id="txtExpense">
                                        </div>                                                
                                        <div class="col-lg-2 hide">                                                    
                                            <label>Exp Amount</label>
                                            <input type="text" class=" form-control num"  id="txtExpAmount">
                                        </div>
                                        <div class="col-lg-1 hide">                                                    
                                            <label>Tax%</label>
                                            <input type="text" class=" form-control num"  id="txtTax">
                                        </div>                                                
                                        <div class="col-lg-2 hide">                                                    
                                            <label>TaxAmount</label>
                                            <input type="text" class=" form-control num"  id="txtTaxAmount">
                                        </div>

                                        <div class="col-lg-3">                                                    
                                            <label>Net Amount</label>
                                            <input type="text" class="form-control readonly " id='txtNetAmount' readonly="" tabindex="-1">
                                            <!-- input type="text" class="form-control readonly num" id="txtUom" >                                                     -->
                                        </div>
                                        
                                    </div>
                                        <div class="row">                                                                                    
                                            <div class="col-lg-10">
                                                <a class="btn btn-sm btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                                                <a class="btn btn-sm btn-default btnSave" data-savegodownbtn='<?php echo $vouchers['warehouse']['insert']; ?>' data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['purchasereturnvoucher']['insert']; ?>' data-updatebtn='<?php echo $vouchers['purchasereturnvoucher']['update']; ?>' data-deletebtn='<?php echo $vouchers['purchasereturnvoucher']['delete']; ?>' data-printbtn='<?php echo $vouchers['purchasereturnvoucher']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
                                                <a class="btn btn-sm btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>
                                                <!-- <a class="btn btn-sm btn-default btnPrint"><i class="fa fa-print"></i> Print F9</a>                                     -->
                                                <div class="btn-group">
                                                  <button type="button" class="btn btn-primary btn-lg btnPrint" ><i class="fa fa-save"></i>Print F9</button>
                                                  <button type="button" class="btn btn-primary btn-lg dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                   <span class="caret"></span>
                                                   <span class="sr-only">Toggle Dropdown</span>
                                                 </button>
                                                 <ul class="dropdown-menu" role="menu">
                                                   <li ><a href="#" class="btnprintThermal">Thermal Print</a></li>
                                                   <!-- <li ><a href="#" class="btnprintHeader"> Print with header</li>
                                                   <li ><a href="#" class="btnprintwithOutHeader"> Print with Out header</li>
                                                   <li ><a href="#" class="btnprint_sm">Small</li>
                                                   <li ><a href="#" class="btnprint_sm_withOutHeader">Small with out header </li>
                                                   <li ><a href="#" class="btnprint_sm_rate">Small with out rate</li>
                                                   <li ><a href="#" class="btnprint_sm_withOutHeader_rate">Small with out header and rate </li> -->
                                                 </ul>
                                                </div>
                                                <a href="#party-lookup" data-toggle="modal" class="btn btn-sm btn-default btnsearchparty "><i class="fa fa-search"></i>&nbsp;Account Lookup F1</a>
                                                <!-- <a href="#item-lookup" data-toggle="modal" class="btn btn-sm btn-default btnsearchitem" style="color:black;"><i class="fa fa-search"></i>&nbsp;Item Lookup F2</a> -->
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">                                                                
                                                    <div class="input-group">
                                                      <span class="switch-addon">Pre Bal?</span>
                                                      <input type="checkbox" checked="" class="bs_switch" id="switchPreBal">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3" >
                                                <div class="input-group">
                                                    <span class="input-group-addon">User: </span>
                                                    <select class="form-control " disabled="" id="user_dropdown">
                                                        <option value="" disabled="" selected="">...</option>
                                                        <?php foreach ($userone as $user): ?>
                                                            <option value="<?php echo $user['uid']; ?>"><?php echo $user['uname']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>                                             
                                        </div>
                                    </div>  <!-- end of row -->
                                </div>
                            </div>
                        </div>`
                        </div>
                        <div class="tab-pane fade in Attachmentspanel" id="Attachments">
                                <div class="row">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <input type="number" class="form-control" id="txtIdImg" style="background: #fff;" readonly />
                                        </div>
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-primary" id="viewImages">View Attachments <i class="fa fa-eye"></i></button>
                                            <button type="button" class="btn btn-success" id="uploadFiles" data-insert='<?php echo $vouchers['uploadFiles']['insert']; ?>' data-update='<?php echo $vouchers['uploadFiles']['update']; ?>' data-delete='<?php echo $vouchers['uploadFiles']['delete']; ?>' >Update Files <i class="fa fa-upload"></i></button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                         <form enctype="multipart/form-data" class="dropzone" id="image-upload" method="POST">
                                                <div>
                                                    <h3>Attach Files to Upload</h3>
                                                    <div class="fallback">
                                                        '<input name="file" type="file" />
                                                    </div>
                                                </div>
                                            </form>
                                            <small style="color: #555;"> Supported Files: (.bmp, .gif, .jpg, .jpeg, .png, .pdf, .doc, .docx, .txt, .xls, .xlsx, .rtf)</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            </div>



                        </div>
                        </div>  <!-- end of col -->

                        <div class="row col-lg-2" style="background-color: #E7F0EF;">
                            <div class="row">
                                <label>Stock Detail</label>
                                <table class="laststockLocation_table table-striped" id="laststockLocation_table" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th>Location</th>
                                                <th>Qty</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                </table>
                            </div>
                        </div>
                        
                    </div>  <!-- end of row -->

                    
                    </div>

                </div>  <!-- end of level 1-->
            </div>
        </div>
    </div>
</div>