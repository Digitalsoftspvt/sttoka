<?php

    $desc = $this->session->userdata('desc');
    $desc = json_decode($desc);
    $desc = objectToArray($desc);

    $vouchers = $desc['vouchers'];
?>
<!-- main content -->
<div id="main_wrapper">

    <div class="page_bar">
        <div class="row">
            <div class="col-md-4">
                <h1 class="page_title">Requisition/Demand  Voucher</h1>
            </div>
            <div class="col-md-8">
                <div class="pull-right">
                    <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset</a>
                    <a class="btn btn-primary btnSave" data-savegodownbtn='<?php echo $vouchers['warehouse']['insert']; ?>' data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['requisition']['insert']; ?>' data-updatebtn='<?php echo $vouchers['requisition']['update']; ?>' data-deletebtn='<?php echo $vouchers['requisition']['delete']; ?>' data-printbtn='<?php echo $vouchers['requisition']['print']; ?>' ><i class="fa fa-save"></i> Save Changes</a>
                    <a class="btn btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete</a>
                    <a class="btn btn-default btnPrint"><i class="fa fa-print"></i> Print</a>
                </div>
            </div>
        </div>
    </div>

    <div class="page_content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <form action="">

                                        <div class="row">
                                            <div class="col-lg-2">
                                                <div class="input-group">
                                                    <span class="input-group-addon id-addon">Requisition#</span>
                                                    <input type="hidden" id="voucher_type_hidden">
                                                    <input type="number" class="form-control" id="txtVrnoa" >
                                                    <input type="hidden" id="txtMaxVrnoaHidden">
                                                    <input type="hidden" id="txtVrnoaHidden">
                                                    <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
                                                    <input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                                                    <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon txt-addon">Date</span>
                                                    <input class="form-control ts_datepicker" type="text" id="current_date">
                                                    <input class="form-control ts_datepicker" type="hidden" id="vrdate">
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="input-group">
                                                    <span class="input-group-addon txt-addon">Vr#</span>
                                                    <input type="text" class="form-control" id="txtVrno" readonly='true'>
                                                    <input type="hidden" id="txtMaxVrnoHidden">
                                                    <input type="hidden" id="txtVrnoHidden">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon txt-addon">Noted By</span>
                                                    <input type="text" list='notedby' id='txtNotedBy' class="form-control">
                                                    <datalist id='notedby'>
                                                        <?php foreach ($notedbys as $notedby): ?>
                                                            <option value="<?php echo $notedby['noted_by']; ?>">
                                                        <?php endforeach ?>
                                                    </datalist>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon txt-addon">Demand#</span>
                                                    <input type="text" class="form-control num" id="txtDemandNo">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon txt-addon">Remarks</span>
                                                    <input type="text" class="form-control" id="txtRemarks">
                                                </div>
                                            </div>

                                            <div class="col-lg-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon txt-addon">Location</span>
                                                    <select class="form-control select2" id="dept_dropdown">
                                                        <option value="" disabled="" selected="">Choose location</option>
                                                        <?php foreach ($departments as $department): ?>
                                                            <option value="<?php echo $department['did']; ?>"><?php echo $department['name']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row"></div>
                                        <div class="row"></div>

                                        <div class="container-wrap">
                                            <div class="row">
                                                <div class="col-lg-1">
                                                        <label>Item Id</label>
                                                        <select class="form-control select2" id="itemid_dropdown">
                                                            <option value="" disabled="" selected="">Item Id</option>
                                                            <?php foreach ($items as $item): ?>
                                                                <option value="<?php echo $item['item_id']; ?>" data-stock="<?php echo $item['stock']; ?>" data-prate="<?php echo $item['prate']; ?>"><?php echo $item['item_code']; ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                   
                                                </div>
                                                <div class="col-lg-4">
                                                    <label>Item description</label>
                                                    <select class="form-control select2" id="item_dropdown">
                                                        <option value="" disabled="" selected="">Item description</option>
                                                        <?php foreach ($items as $item): ?>
                                                            <option value="<?php echo $item['item_id']; ?>" data-stock="<?php echo $item['stock']; ?>" data-prate="<?php echo $item['prate']; ?>"><?php echo $item['description']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                                <div class="col-lg-1">
                                                        <label>Stock</label>
                                                        
                                                        <input type="text" class="form-control num" id="txtStk" style="width: 45px;">
                                                    
                                                </div>
                                                <div class="col-lg-1">
                                                        <label>PRate</label>
                                                        
                                                        <input type="text" class="form-control num" id="txtPRate">
                                                    
                                                </div>
                                                <div class="col-lg-1">
                                                        <label>Qty</label>
                                                        
                                                        <input type="text" class="form-control num" id="txtSQty">
                                                   
                                                </div>
                                                <div class="col-lg-3">
                                                        <label>Remarks</label>
                                                        
                                                        <input type="text" class="form-control" id="txtSRemarks">
                                                  
                                                </div>
                                                <div class="col-lg-1">
                                                        <label>.</label>
                                                        <a href="" class="btn btn-primary" id="btnAdd">+</a>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row"></div>
                                        <div class="row"></div>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <table class="table table-striped" id="requisition_table">
                                                    <thead>
                                                        <tr>
                                                            <th>Sr#</th>
                                                            <th>Item Code</th>
                                                            <th>Item Detail</th>
                                                            <th>Location</th>
                                                            <th>Stock</th>
                                                            <th>PRate</th>
                                                            <th>Qty</th>
                                                            <th>Remarks</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </form> <!-- end of form -->

                                </div>  <!-- end of panel-body -->
                            </div>  <!-- end of panel -->
                        </div>  <!-- end of col -->
                    </div>  <!-- end of row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-lg-5">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset</a>
                                                    <a class="btn btn-primary btnSave" data-savegodownbtn='<?php echo $vouchers['warehouse']['insert']; ?>' data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['requisition']['insert']; ?>' data-updatebtn='<?php echo $vouchers['requisition']['update']; ?>' data-deletebtn='<?php echo $vouchers['requisition']['delete']; ?>' data-printbtn='<?php echo $vouchers['requisition']['print']; ?>'><i class="fa fa-save"></i> Save Changes</a>
                                                    <a class="btn btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete</a>
                                                    <a class="btn btn-default btnPrint"><i class="fa fa-print"></i> Print</a>
                                                </div>
                                            </div>
                                        </div>  <!-- end of col -->
                                        <div class="col-lg-5"></div>
                                        <div class="col-lg-2">
                                            <div class="pull-right">
                                                <div class="input-group">
                                                    <span class="input-group-addon fancy-addon" style='min-width:0px;'>Qty</span>
                                                    <input type="text" class="form-control num" id="txtGQty" readonly="true">
                                                </div>
                                            </div>
                                        </div>
                                    </div>  <!-- end of row -->
                                </div>
                            </div>
                        </div>
                    </div>

                </div>  <!-- end of level 1-->
            </div>
        </div>
    </div>
</div>