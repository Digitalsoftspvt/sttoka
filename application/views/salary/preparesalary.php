<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);

	$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-lg-3">
				<h1 class="page_title">Salary Sheet</h1>
			</div>
			<div class="col-lg-9">
				<div class="pull-right">
                    <a class="btn btn-default btnPrint" data-printtbtn='<?php echo $vouchers['salary_sheet']['print']; ?>'><i class="fa fa-print"></i> Print</a>
                	<a class="btn btn-default btnSave" data-insertbtn='<?php echo $vouchers['salary_sheet']['insert']; ?>' data-updatebtn='<?php echo $vouchers['salary_sheet']['update']; ?>' data-deletebtn='<?php echo $vouchers['salary_sheet']['delete']; ?>' data-printbtn='<?php echo $vouchers['salary_sheet']['print']; ?>'><i class="fa fa-save"></i> Save Changes</a>
                    <a href="" class="btn btn-default btnDelete" data-deletetbtn='<?php echo $vouchers['salary_sheet']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete</a>
                    <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset</a>
                </div>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">
	
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">
							
							<div class="row">
								<div class="col-lg-2">
                                  	<div class="input-group">
                                      	<span class="input-group-addon id-addon">Vr#</span>
                                      	<input type="number" class="form-control num txtidupdate" data-txtidupdate='<?php echo $vouchers['salary_sheet']['update']; ?>' id="txtId">
                                      	<input type="hidden" id="txtMaxIdHidden">
                                      	<input type="hidden" id="txtIdHidden">
                                      	<input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
															<input type="hidden" id="voucher_type_hidden">
															<input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                                                            <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                                	</div>
								</div>
                            	<div class="col-lg-3">
                                	<div class="input-group">
                                    	<span class="input-group-addon txt-addon">From Month</span>
                                    	<input class="form-control ts_datepicker" type="text" id="from_date" >
                                	</div>
                                </div>

                                <div class="col-lg-3">
                                	<div class="input-group">
                                    	<span class="input-group-addon txt-addon">To Month</span>
                                    	<input class="form-control ts_datepicker" type="text" id="to_date" >
                                	</div>
                                </div>
								<div class="col-lg-2">
                                	<select class="form-control" id="warehouse">
                                    	<option class="form-control"value="">Choose Stock location</option>
										<?php foreach($accounts2 as $accounts): ?>
										<option class="form-control" value="<?php echo $accounts['did']; ?>"><?php echo $accounts['name']; ?></option>
										<?php endforeach; ?>
									</select>
                                	
                                </div>
                                <div class="col-lg-2">
									<div class="pull-right">
										<a class="btn btn-default btnSearch"><i class="fa fa-search"></i> Show</a>
									</div>
								</div>
							</div>

							<div class="row hide">
								<div class="col-lg-3">									
									<select class="form-control" id="name_dropdown">
	                                	<?php foreach ($accounts1 as $account): ?>
                                          	<option value="<?php echo $account['pid']; ?>"><?php echo $account['name']; ?></option>
                                      	<?php endforeach ?>
	                                </select>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>	<!-- end of configuration -->

			<div class="row">
				<div class="col-lg-12">

					<div class="panel panel-default">
						<div class="panel-body">
							

							<div class="row" style='overflow-y: auto;'>
								<div class="col-lg-12">									
									<table class="table table-striped table-hover" id="salary_table">
										
										<thead>
											<!--<th>Sr#</th>
											<th>Department</th>
											<th>Employee Name</th>
											<th>Designation</th>
											<th>Basic Salary</th>
											<th>Absents</th>
											<th>Leave WP</th>
											<th>Leave WOP</th>
											<th>Rest Days</th>
											<th>Work Days</th>
											<th>Paid Days</th>
											<th>Gross Salary</th>
											<th>Overtime</th>
											<th>OT Rate</th>
											<th>OT Amount</th>
											<th>Advance</th>
											<th>Loan Deduction</th>
											<th>Remaining Balance</th>
											<th>Incentive</th>
											<th>Penalty</th>
											<th>EOBI</th>
											<th>Insurance</th>
											<th>Social Security</th>
											<th>Net Salary</th>-->
                                            <th>Sr#</th>
                                            <th>Stock Location</th>
                                            <th>Employee Name</th>
                                            <th>Designation</th>
                                            <th>Basic Salary</th>
                                            <th>Absents</th>
                                            <th>Rest Days</th>
                                            <th>Work Days</th>
                                            <th>Paid Days</th>
                                            <th>Gross Salary</th>
                                            <th>Overtime</th>
                                            <th>OT Rate</th>
                                            <th>OT Amount</th>
                                            <th>Advance</th>
                                            <th>Loan Deduction</th>
                                            <th>Remaining Balance</th>
                                            <th>Incentive</th>
                                            <th>Penalty</th>
                                            <th>EOBI</th>
                                            <th>Net Salary</th>
										</thead>
										<tbody>
											
										</tbody>
                                        <tfoot class="tfoot_tbl">
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>

                                            <td class="text-left" colspan="1">Totals</td>

                                            <td class="text-left"  id="txtBasicSalary"></td>
                                            <td class="text-left"  id="txtAbsent"></td>
                                            <td class="text-left"  id="txtRestDay"></td>
                                            <td class="text-left"  id="txtWorkDay"></td>
                                            <td class="text-left"  id="txtPaidDay"></td>
                                            <td class="text-left"  id="txtGrossSalary"></td>
                                            <td class="text-left"  id="txtOverTime"></td>
                                            <td></td>
                                            <td class="text-left"  id="txtOtAmount"></td>
                                            <td class="text-left"  id="txtAdvance"></td>
                                            <td class="text-left"  id="txtLoanDeduction"></td>
                                            <td class="text-left"  id="txtRemainingBalance"></td>
                                            <td class="text-left"  id="txtIncentive"></td>
                                            <td class="text-left"  id="txtPenalty"></td>
                                            <td class="text-left"  id="txtEobi"></td>
                                            <td class="text-left"  id="txtNetSalary"></td>

                                        </tr>
                                        </tfoot>
									</table>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-12">
									<div class="pull-right">
										<a class="btn btn-default btnPrintSlips" data-printtbtn='<?php echo $vouchers['salary_sheet']['print']; ?>'><i class="fa fa-print"></i> Print Slips</a>
					                    <a class="btn btn-default btnPrint" data-printtbtn='<?php echo $vouchers['salary_sheet']['print']; ?>'><i class="fa fa-print"></i> Print</a>
					                	<a class="btn btn-default btnSave" data-insertbtn='<?php echo $vouchers['salary_sheet']['insert']; ?>' data-updatebtn='<?php echo $vouchers['salary_sheet']['update']; ?>' data-deletebtn='<?php echo $vouchers['salary_sheet']['delete']; ?>' data-printbtn='<?php echo $vouchers['salary_sheet']['print']; ?>'><i class="fa fa-save"></i> Save Changes</a>
					                    <a href="" class="btn btn-default btnDelete" data-deletetbtn='<?php echo $vouchers['salary_sheet']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete</a>
					                    <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset</a>
					                </div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>
<input id="hfSalaryPlane" type="hidden" value="<?php echo $salaryPlan; ?>">