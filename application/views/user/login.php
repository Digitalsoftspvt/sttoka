<img src="<?php echo base_url('assets/img/dm.png') ?>" alt="digitalmanagercamp" id='loginpagelogo'>

<div class="login_container">
	<form id="login_form" action="" method='post'>
		<h1 class="login_heading">Sign In</h1>
		<div class="form-group hide">
            <select name="fn_dropdown" id="fn_dropdown" class="form-control select2">
        <option value="" disabled="disabled" selected="selected">Chose Financial Year</option>  
            <?php if (count($finanicalyears) > 0): 
            ?>
            <?php $counter = 1; foreach ($finanicalyears as $transporter): ?> 
            <?php $selectedParty = ''; ?>
        <option <?php echo $selectedParty; ?> value="<?php echo $transporter['fn_id']?>" data-edate="<?php echo substr($transporter['end_date'],0,10) ?>" data-sdate="<?php echo substr($transporter['start_date'],0,10) ?>"><?php echo $transporter['fname'];?></option>           <?php endforeach;?>
    <?php endif;?>

            </select>
        </div>
		<div class="form-group">
			<input type="text" class="form-control " id="txtUsername" placeholder='Username'>
		</div>
		<div class="form-group">
			<input type="password" class="form-control " id="txtPassowrd" placeholder='Password'>
		</div>
		<div class="form-group">
			<input type="text" class="form-control num" id="txtMobCode" placeholder='Mobile Code' title="We won't send you any marketing material."  >
		</div>
		<!-- <div class="submit_section1"> -->
			<div class="row" > 
			<!-- <a class="btn btn-lg btn-success btn-block btnSignin"><i class="glyphicon glyphicon-user"></i>Sign In</a> -->
				<div class="col-lg-6" >
					<button class="btn btn-sm btn-default btnSignin"><i class="glyphicon glyphicon-user"></i>User Sign In</button>
				</div>
				<div class="col-lg-6" >
					<button class="btn btn-sm btn-default btnMobCode"><i class="glyphicon glyphicon-phone-alt"></i>Send Mobile Code</button>
				</div>
			</div>
		<!-- </div> -->
		
		<div class="errors_section"> 
			
		</div>
		<div class="builtby">

		</div>
	</form>
</div>
