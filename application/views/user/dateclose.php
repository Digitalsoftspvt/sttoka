<?php
	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);
	$vouchers = $desc['vouchers'];
?>
<!-- main content -->

<div id="main_wrapper">
  	<div class="page_bar">
    	<div class="row">
      		<div class="col-md-12">
        		<h1 class="page_title"><i class="fa fa-user"></i> Date Close</h1>
      		</div>
    	</div><br>
    	<ul class="nav nav-tabs">
		  <li class="active"><a data-toggle="tab" id="tabDate" href="#dateclose">Date Close</a></li>
		  <li><a data-toggle="tab" id="tabView" href="#view">View</a></li>
		</ul>

		<div class="tab-content">
		  <div id="dateclose" class="tab-pane fade in active">
		    <form action="">
								<div class="row">
									<div class="panel panel-default">
										<div class="panel-body">

											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
						                            	<span class="input-group-addon btn btn-primary">Date</span>
		                                                    <input class="form-control  ts_datepicker" type="text" id="current_date">
					                                    
					                                    <input type="hidden" name="uuid" id="uuid" value="<?php echo $this->session->userdata('uid'); ?>">
		                                                <input type="hidden" name="uuname" id="uuname" value="<?php echo $this->session->userdata('uname'); ?>">
		                                                <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
							                        </div>
												</div>										
											</div>
											<div class="row">
												<div class="col-lg-4">
						                          	<div class="input-group">
						                            	<span class="input-group-addon btn btn-primary">Remarks</span>
						                            	<input type="text" class="form-control" id="txtRemarks">
							                        </div>
						                        </div>
											</div>
											<div class="row">
												<div class="col-lg-3" >
				                                    <div class="input-group">
				                                        <span class="input-group-addon">User: </span>
				                                        <select class="form-control " disabled="" id="user_dropdown">
				                                            <option value="" disabled="" selected="">...</option>
				                                            <?php foreach ($userone as $user): ?>
				                                                <option value="<?php echo $user['uid']; ?>"><?php echo $user['uname']; ?></option>
				                                            <?php endforeach; ?>
				                                        </select>
				                                    </div>
		                                        </div>
											</div>

											<div class="row">
												<div class="col-lg-12">
													<div class="pull-right">
														<!-- <a href='' class="btn btn-default btnSave" data-insertbtn='<?php echo $vouchers['add_new_user']['insert']; ?>'> <i class="fa fa-save"></i> -->
														<a href='' class="btn btn-default btnSave"><i class="fa fa-lock"></i>
															Close F10
														</a>
														<a href='' class="btn btn-primary btnOpen"> <i class="fa fa-unlock"></i>
															Open F5
														</a>
														<a href='' class="btn btn-default btnReset"> <i class="fa fa-refresh"></i>
															Reset F5
														</a>
													</div>
												</div>
											</div> <!-- end of row -->

										</div>
									</div>

								</div>

				        	</form>   <!-- end of form -->
		  </div>
		  <div id="view" class="tab-pane fade">
		    <div class="row">
				<div class="panel panel-default">
					<div class="panel-body">
						<table class="table table-bordered table-responsive" id="purchase_table">
							<thead>
								<tr>
									<th>Sr#</th>
									<th>Date Close</th>
									<th>Remarks</th>
									<th>Date Time</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $sr = 1; foreach ($datecloses as $dateclose): ?>
									<tr>
										<td><?php echo $sr; ?></td>
										<td class="date_cl"><?php echo $dateclose['date_cl']; ?> <input type="hidden" value="<?php echo $dateclose['uid']; ?>"></td>
										<td class="remarks"><?php echo $dateclose['remarks']; ?></td>
										<td class="datetime"><?php echo $dateclose['date_time']; ?></td>
										<td><button class="btn btn-primary btnRowEdit" type="buttom" id="btnEdit"><i class="fa fa-edit"></i></button></td>
									</tr>
									<?php $sr++; ?>
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		  </div>
		</div>
  	</div>
</div>