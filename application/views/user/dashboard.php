<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);
	$reports = $desc['reports'];
	$vouchers = $desc['vouchers'];
?>



<script id="cheque-row" type="text/x-handlebars-template">
	<tr>
	  <td>{{VRDATE}}</td>
	  <td>{{MATURE_DATE}}</td>
	  <td>{{DCNO}}</td>
	  <td>{{CHEQUE_NO}}</td>
	  <td>{{PARTY}}</td>
	  <td>{{BANK}}</td>
	  <td>{{AMOUNT}}</td>
	</tr>
</script>


<!-- //////////////////////////////// Start daily template ////////////////////////////////// -->

<script id="sp-daily-head-template" type="text/x-handlebars-template">
  <tr>
	 <th>Vr#</th>
	 <th>Account</th>
	 <th>Amount</th>
  </tr>
</script> 
<script id="sp-daily-template" type="text/x-handlebars-template">
  <tr>
	 <td>{{VRNOA}}</td>
	 <td class="account">{{ACCOUNT}}</td>
	 <td class="amount">{{NAMOUNT}}</td>
  </tr>
</script> 

<!-- ///////////////////////////// End Daily Template ////////////////////////// -->

<!-- ///////////////////// Start Weekly and Monthly Template /////////////////// -->
<script id="sp-weekly-monthly-head-template" type="text/x-handlebars-template">
  <tr>
	 <th>Mon</th>
	 <th>Tue</th>
	 <th>Wed</th>
	 <th>Thu</th>
	 <th>Fri</th>
	 <th>Sat</th>
	 <th>Sun</th>
  </tr>
</script>
<script id="sp-weekly-monthly-template" type="text/x-handlebars-template">
  <tr>
	 <td>{{Monday}}</td>
	 <td>{{Tuesday}}</td>
	 <td>{{Wednesday}}</td>
	 <td>{{Thursday}}</td>
	 <td>{{Friday}}</td>
	 <td>{{Saturday}}</td>
	 <td>{{Sunday}}</td>
  </tr>
</script>
<!-- /////////////////// End Weekly and Monthly Template ////////////////// -->

<!-- ///////////////////// Start Weekly and Monthly Template /////////////////// -->
<script id="sp-monthly-head-template" type="text/x-handlebars-template">
  <tr>
	 <th>Jan</th>
	 <th>Feb</th>
	 <th>Mar</th>
	 <th>Apr</th>
	 <th>May</th>
	 <th>Jun</th>
	 <th>Jul</th>
	 <th>Aug</th>
	 <th>Sep</th>
	 <th>Oct</th>
	 <th>Nov</th>
	 <th>Dec</th>
  </tr>
</script>
<script id="sp-monthly-template" type="text/x-handlebars-template">
  <tr>
	 <td>{{Jan}}</td>
	 <td>{{Feb}}</td>
	 <td>{{Mar}}</td>
	 <td>{{Apr}}</td>
	 <td>{{May}}</td>
	 <td>{{Jun}}</td>
	 <td>{{Jul}}</td>
	 <td>{{Aug}}</td>
	 <td>{{Sep}}</td>
	 <td>{{Oct}}</td>
	 <td>{{Nov}}</td>
	 <td>{{Dec}}</td>
  </tr>
</script>
<!-- /////////////////// End Weekly and Monthly Template ////////////////// -->


<!-- ///////////////////// Start Yearly Template /////////////////// -->
<script id="sp-yearly-head-template" type="text/x-handlebars-template">
	<tr>
		<th>Year</th>
		<th>Month</th>
		<th>Total Sale</th>
	</tr>
</script>
<script id="sp-yearly-template" type="text/x-handlebars-template">
	<tr>
		<td>{{Year}}</td>
		<td class="tdMonth">{{Month}}</td>
		<td class="tdMonthAmount" style="text-align:right;">{{TotalAmount}}</td>
	</tr>
</script>
<!-- /////////////////// End Yearly Template ////////////////// -->
	<link href="<?php echo base_url('assets/css/style_dashbord.css'); ?>" rel="stylesheet" media="screen">
	<style>
		.table.dataTable .sorting_asc {
	padding-right: 0px !important;
	white-space: nowrap;
		}
	</style>
	<br>
	<br>
	<div class="container-fluid" id="content">
		<div id="main">
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left" style="padding-left: 7%;">
						<h1 style="font-family:open sans;"><i class="fa fa-sitemap"></i> <?php echo $this->session->userdata('company_name'); ?> <small>(Business Management Software)</small></h1>
					</div>
					<input type="hidden" name="cid" class="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
				</div><!-- page-headrer -->

				<!--.................................TOP TILES .........................................-->

				<div class="row" style="margin:-20px 0px 0px 90px;">
					<div class="col-sm-12">
						<div class="metro-nav">
							<div class="metro-nav-block nav-block-orange">
								<a data-original-title="" href="<?php echo ($vouchers['item']['item'] == 1)? base_url('index.php/item'):''; ?>">
									<i class="fa fa-barcode"></i>
									<div class="info"><?php //echo $getmaxStaffid; ?></div>
									<div class="status"><?php echo ($vouchers['item']['item'] == 1)? "Add New Item" :''; ?></div>
								</a>
							</div><!-- metro-nav -->
							<div class="metro-nav-block nav-olive">
								<a data-original-title="" href="<?php echo ($vouchers['account']['account'] == 1) ?  base_url('index.php/account/add'):''; ?>">
									<i class="fa fa-users"></i>
									<div class="info"><?php  ?></div>
									<div class="status"><?php echo ($vouchers['account']['account'] == 1)? "Add New Account" :''; ?></div>
								</a>
							</div><!-- metro-nav -->
							<div class="metro-nav-block nav-block-yellow">
								<a data-original-title="" href="<?php echo ($vouchers['purchasevoucher']['purchasevoucher'] == 1)? base_url('index.php/purchase') :''; ?>">
									<i class="fa fa-shopping-cart"></i>
									<div class="info"><?php //echo $getmaxAppProcedureid; ?></div>
									<div class="status"><?php echo ($vouchers['purchasevoucher']['purchasevoucher'] == 1)? "Purchase Voucher" :''; ?></div>
								</a>
							</div><!-- metro-nav -->
							<div class="metro-nav-block nav-block-green double">
								<a data-original-title="" href="<?php echo (isset($vouchers['purchaseorder']['purchaseorder']) && $vouchers['purchaseorder']['purchaseorder'] == 1)? base_url('index.php/purchaseorder') :''; ?>">
					  			<i class="fa fa-shopping-cart"></i>
									<div class="info"><?php //echo $getmaxApplicationid; ?></div>
									<div class="status"><?php echo (isset($vouchers['purchaseorder']['purchaseorder']) && $vouchers['purchaseorder']['purchaseorder'] == 1)? "Purchase Order" :''; ?></div>
								</a>
							</div><!-- metro-nav -->
							<div class="metro-nav-block nav-block-red">
								<a data-original-title="" href="<?php //echo ($vouchers['consumptionvoucher']['consumptionvoucher'] == 1)? base_url('index.php/consumption') :''; ?>">
									<i class="fa fa-trello"></i>
									<div class="info"><?php //echo $getmaxCityid; ?></div>
									<div class="status"><?php //echo ($vouchers['consumptionvoucher']['consumptionvoucher'] == 1)? "Issuance Voucher" :''; ?></div>
								</a>
							</div><!-- metro-nav -->
						</div><!-- metro-nav -->
						<div class="metro-nav">
							<div class="metro-nav-block nav-block-grey ">
								<a data-original-title="" href="<?php echo ($vouchers['salevoucher']['salevoucher'] == 1)? base_url('index.php/saleorder/Sale_Invoice') :''; ?>">
									<i class="fa fa-pencil-square-o"></i>
									<div class="info"></div>
									<div class="status"><?php echo ($vouchers['salevoucher']['salevoucher'] == 1)? "Sale Voucher" :''; ?></div>
								</a>
							</div><!-- metro-nav -->
							<div class="metro-nav-block nav-light-purple">
								<a data-original-title="" href="<?php echo ($vouchers['saleordervoucher']['saleordervoucher'] == 1)? base_url('index.php/saleorder') :''; ?>">
									<i class="fa fa-file-text"></i>
									<div class="info"><?php //echo $getmaxappstatussid; ?></div>
									<div class="status"><?php echo ($vouchers['saleordervoucher']['saleordervoucher'] == 1)? "Sale Order" :''; ?></div>
								</a>
							</div><!-- metro-nav -->
							<div class="metro-nav-block nav-light-blue double">
								<a data-original-title="" href="<?php echo ($vouchers['cash_payment_receipt']['cash_payment_receipt'] == 1)? base_url('index.php/payment') :''; ?>">
									<i class="fa fa-money"></i>
									<div class="info"><?php //echo $getmaxapptypesid; ?></div>
									<div class="status"><?php echo ($vouchers['cash_payment_receipt']['cash_payment_receipt'] == 1)? "Cash Payment/Receipt" :''; ?></div>
								</a>
							</div><!-- metro-nav -->
							<div class="metro-nav-block nav-light-green">
								<a data-original-title="" href="<?php echo ($vouchers['jvvoucher']['jvvoucher'] == 1)? base_url('index.php/jv') :''; ?>">
									<i class="ion-social-buffer"></i>
									<div class="info"><?php //echo $getmaxusersid; ?></div>
									<div class="status"><?php echo ($vouchers['jvvoucher']['jvvoucher'] == 1)? "Journal Voucher" :''; ?></div>
								</a>
							</div><!-- metro-nav -->
							<div class="metro-nav-block nav-light-brown">
								<a data-original-title="" href="<?php //echo ($vouchers['inwardvoucher']['inwardvoucher'] == 1)? base_url('index.php/inward') :''; ?>">
									<i class="fa fa-truck"></i>
									<div class="info"><!-- <?php //echo $getmaxDealid; ?> --></div>
									<div class="status"><?php //echo ($vouchers['inwardvoucher']['inwardvoucher'] == 1)? "Inward GatePass" :''; ?></div>
								</a>
							</div><!-- metro-nav -->
						</div><!-- metro-nav -->
						<div class="metro-nav">
							<div class="metro-nav-block nav-block-green double">
								<a data-original-title="" href="<?php echo ($reports['account_ledger'] == 1)? base_url('index.php/report/accountLedger') :''; ?>">
									<i class="fa fa-book"></i>
									<div class="info"><?php //echo $getmaxtokenid; ?></div>
									<div class="status"><?php echo ($reports['account_ledger'] == 1)? "Account Ledger" :''; ?></div>
								</a>
							</div><!-- metro-nav -->
							<div class="metro-nav-block nav-block-orange">
								<a data-original-title="" href="<?php echo ($reports['itemledger'] == 1)? base_url('index.php/report/itemLedger') :''; ?>">
									<i class="fa fa-calendar"></i>
									<div class="info"><?php //echo $getmaxclassid; ?></div>
									<div class="status"><?php echo ($reports['itemledger'] == 1)? "Item Ledger" :''; ?></div>
								</a>
							</div><!-- metro-nav -->
							<div class="metro-nav-block nav-block-red">
								<a data-original-title="" href="<?php echo ($reports['stockreport'] == 1)? base_url('index.php/report/stock') :''; ?>">
									<i class="fa fa-bar-chart-o"></i>
									<div class="info"><?php //echo $getmaxbankid; ?></div>
									<div class="status"><?php echo ($reports['stockreport'] == 1)? "Stock Report" :''; ?></div>
								</a>
							</div><!--metro-nav-block-->
							<div class="metro-nav-block nav-olive">
								<a data-original-title="" href="<?php //echo ($reports['vendorledgerreport'] == 1)? base_url('index.php/report/issuereceivefromVendor') :''; ?>">
									<i class="ion-ios7-personadd"></i>
									<div class="info"><!--<?php //echo $getmaxTaskid; ?>--></div>
									<div class="status"><?php //echo ($reports['vendorledgerreport'] == 1)? "Vendor Ledger" :''; ?></div>
								</a>
							</div><!-- metro-nav -->
							<div class="metro-nav-block nav-block-yellow">
								<a data-original-title="" href="<?php //echo ($vouchers['outwardvoucher']['outwardvoucher'] == 1)? base_url('index.php/inward/outward') :''; ?>">
									<i class="fa fa-exchange"></i>
									<div class="info"><?php //echo $getmaxappfeeid; ?></div>
									<div class="status"><?php //echo ($vouchers['outwardvoucher']['outwardvoucher'] == 1)? "Outward GatePass" :''; ?></div>
								</a>
							</div><!-- metronav -->
						</div><!-- metro-nav -->
					</div><!-- end of col -->
				</div><!-- end of row -->


				<!--...................................ACCORDIANS FOR TRANSCTIONS..................................-->


				<div class="row-fluid" style="margin:-30px 0px 0px 90px;">
					<div class="row-fluid">
						<div class="col-md-12">
							<h4 class="text-primary" style="font-family:open sans;"><i class="fa fa-industry"></i> <b>I</b>nventory</h4>

							<!--.....................................PURCHASE ACCORDIANS.......................................-->

							<div class="panel-group" id="accordion_a">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_1" style="background: #5A6062 !important; color:white;font-family:open sans;height: 40px;">
												<div class="row">
													<div class="col-lg-9">
														<span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-shopping-cart" style="color:#90906F;"></i></span> Purchase
													</div>
													<div class="col-lg-3" style="font-size:12px !important;">
														<span>Total:</span> : <span class="purchases-sum"> 00</span>
													</div>
												</div>
											</a>
										</h4>
									</div>
									<div id="accordion_a_1" class="panel-collapse collapse">
										<div class="panel-body">
										  <div id="no-more-tables">
										  <table class="table table-striped table-hover">
												<thead class="cf">
													<tr>
														<th class="numeric" style='background: #368EE0;max-width:20px !important;'>Vr#</th>
														<th style='background: #368EE0;'>Time</th>
														<th style='background: #368EE0;'>User</th>
														<th style='background: #368EE0;'>Party</th>
														<th class="numeric"style='background: #368EE0;' class="text-left">Discount</th>
														<th class="numeric"style='background: #368EE0;' class="text-left">Commission</th>
														<th class="numeric"style='background: #368EE0;' class="text-left">Tax</th>
														<th class="numeric"style='background: #368EE0;' class="text-right">Net Amount</th>
													</tr>
												</thead>
												<tbody>
													<?php $counter = 1; $tot=0; foreach ($purchases as $Yarnpurchase): $tot +=$Yarnpurchase['namount']; ?>
														<tr>
															<td class="numeric" data-title="Vr#"><a href= "<?php echo base_url() . "index.php/purchase?vrnoa=" . $Yarnpurchase['vrnoa'] ;?>"> </a> <?php echo $Yarnpurchase['vrnoa']; ?></td>
															<td data-title="Time"><?php echo $Yarnpurchase['date_time']; ?></td>
															<td data-title="User"><?php echo $Yarnpurchase['user_name']; ?></td>
															<td data-title="Account"><?php echo $Yarnpurchase['party_name']; ?></td>
															<td class="numeric" data-title="Disc">
																<span class="text-primary"><b><?php echo $Yarnpurchase['discp']; ?>%</b></span></li>&nbsp;&nbsp;&nbsp;
																<span class="text-danger text-right"><b><?php echo $Yarnpurchase['discount']; ?></b></span></li>
															</td>
															<td class="numeric" data-title="Comm">
																<span class="text-primary"><b><?php echo $Yarnpurchase['exppercent']; ?>%</b></span>&nbsp;&nbsp;&nbsp;
																<span class="text-danger text-right"><b><?php echo $Yarnpurchase['expense']; ?></b></span>
															</td>
															<td class="numeric" data-title="Tax">
																<span class="text-primary"><b><?php echo $Yarnpurchase['taxpercent']; ?>%</b></span></li>&nbsp;&nbsp;&nbsp;
																<span class="text-danger text-right"><b><?php echo $Yarnpurchase['tax']; ?></b></span></li>
															</td>
															<td class="numeric" data-title="NetAmount" style="text-align:right;"><?php echo $Yarnpurchase['namount']; ?></td>
														</tr>
													<?php endforeach  ?>
												</tbody>
												<tfoot class="hidden">
													<tr>
														<td><span></span> : <span class="purchases-sum-val"><?php echo $tot; ?></span></td>
													</tr>
												</tfoot>
											</table>
											</div>
										</div>
									</div>
								</div>

							<!--......................................PURCHASE YARN ACCORDIANS..................................-->

								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_2" style="background:#747672 !important; color:white;font-family:open sans;height: 40px;">
												<div class="row">
													<div class="col-lg-9">
														<span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-shopping-cart" style="color:#90906F;"></i></span>Purchase Return
													</div>
													<div class="col-lg-3" style="font-size:12px !important;">
														<span>Total:</span> : <span class="yarnpurchases-sum"> 00</span>

													</div>
												</div>
											</a>
										</h4>
									</div>
									<div id="accordion_a_2" class="panel-collapse collapse">
										<div class="panel-body">
											<div id="no-more-tables">
											<table class="table table-striped table-hover">
												<thead class="cf">
													<tr>
														<th class="numeric" style='background: #368EE0;max-width:20px !important;'>Vr#</th>
														<th style='background: #368EE0;'>Time</th>
														<th style='background: #368EE0;'>User</th>
														<th style='background: #368EE0;'>Party</th>
														<th class="numeric" style='background: #368EE0;' class="text-left">Discount</th>
														<th class="numeric" style='background: #368EE0;' class="text-left">Commission</th>
														<th class="numeric" style='background: #368EE0;' class="text-left">Tax</th>
														<th class="numeric" style='background: #368EE0;' class="text-right">Net Amount</th>
													</tr>
												</thead>
												<tbody>
													<?php $counter = 1; $tot=0; foreach ($purchasereturns as $returns): $tot +=$returns['namount']; ?>
														<tr>
															<td class="numeric" data-title="Vr#"><a href= "<?php echo base_url() . "index.php/returns?vrnoa=" . $returns['vrnoa'] ;?>"></a> <?php echo $returns['vrnoa']; ?></td>
															<td data-title="Time"><?php echo $returns['date_time']; ?></td>
															<td data-title="User"><?php echo $returns['user_name']; ?></td>
															<td data-title="Account"><?php echo $returns['party_name']; ?></td>
															<td data-title="Disc"class="numeric">
																<span class="text-primary"><b><?php echo $returns['discp']; ?>%</b></span></li>&nbsp;&nbsp;&nbsp;
																<span class="text-danger text-right"><b><?php echo $returns['discount']; ?></b></span></li>
															</td>
															<td data-title="Comm"class="numeric">
																<span class="text-primary"><b><?php echo $returns['exppercent']; ?>%</b></span>&nbsp;&nbsp;&nbsp;
																<span class="text-danger text-right"><b><?php echo $returns['expense']; ?></b></span>
															</td>
															<td data-title="Tax" class="numeric">
																<span class="text-primary"><b><?php echo $returns['taxpercent']; ?>%</b></span></li>&nbsp;&nbsp;&nbsp;
																<span class="text-danger text-right"><b><?php echo $returns['tax']; ?></b></span></li>
															</td>
															<td class="numeric" data-title="NetAmount" style="text-align:right;"><?php echo $returns['namount']; ?></td>
														</tr>
													<?php endforeach  ?>
												</tbody>
												<tfoot class="hidden">
													<tr>
														<td><span>Total Yarn Purchase</span> : <span class="yarnpurchases-sum-val"><?php echo $tot; ?></span></td>
													</tr>
												</tfoot>
											</table>
											</div>
										</div>
									</div>
								</div>

							<!--.................................FABRIC PURCHASE ACCORDIANS .....................................-->

								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_3" style="background:#8D9389 !important; color:white;font-family:open sans;height: 40px;">
												<div class="row">
													<div class="col-lg-9">
														<span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-shopping-cart" style="color:#90906F;"></i></span> Sale Return
													</div>
													<div class="col-lg-3" style="font-size:12px !important;">
														<span>Total:</span> : <span class="fabricpurchases-sum"> 00</span>
													</div>
												</div>
											</a>
										</h4>
									</div>
									<div id="accordion_a_3" class="panel-collapse collapse">
										<div class="panel-body">
											<div id="no-more-tables">
											<table class="table table-striped table-hover">
											<thead class="cf">
													<tr>
														<th style='background: #368EE0;max-width:20px !important;'>Vr#</th>
														<th style='background: #368EE0;'>Time</th>
														<th style='background: #368EE0;'>User</th>
														<th style='background: #368EE0;'>Party</th>
														<th style='background: #368EE0;' class="text-left">Discount</th>
														<th style='background: #368EE0;' class="text-left">Commission</th>
														<th style='background: #368EE0;' class="text-left">Tax</th>
														<th style='background: #368EE0;' class="text-right">Net Amount</th>
													</tr>
												</thead>
												<tbody>
													<?php $counter = 1; $tot=0; foreach ($salereturns as $returns): $tot +=$returns['namount']; ?>
														<tr>
															<td data-title="Vr#" ><a href= "<?php echo base_url() . "index.php/fabricPurchase?vrnoa=" . $returns['vrnoa'] ;?>"></a> <?php echo $returns['vrnoa']; ?></td>
															<td data-title="Time"><?php echo $returns['date_time']; ?></td>
															<td data-title="User"><?php echo $returns['user_name']; ?></td>
															<td data-title="Account"><?php echo $returns['party_name']; ?></td>
															<td data-title="Disc"class="numeric">
																<span class="text-primary"><b><?php echo $returns['discp']; ?>%</b></span></li>&nbsp;&nbsp;&nbsp;
																<span class="text-danger text-right"><b><?php echo $returns['discount']; ?></b></span></li>
															</td>
															<td data-title="Comm"class="numeric">
																<span class="text-primary"><b><?php echo $returns['exppercent']; ?>%</b></span>&nbsp;&nbsp;&nbsp;
																<span class="text-danger text-right"><b><?php echo $returns['expense']; ?></b></span>
															</td>
															<td data-title="Tax" class="numeric">
																<span class="text-primary"><b><?php echo $returns['taxpercent']; ?>%</b></span></li>&nbsp;&nbsp;&nbsp;
																<span class="text-danger text-right"><b><?php echo $returns['tax']; ?></b></span></li>
															</td>
															<td class="numeric" data-title="NetAmount" style="text-align:right;"><?php echo $returns['namount']; ?></td>
														</tr>
													<?php endforeach  ?>
												</tbody>
												<tfoot class="hidden">
													<tr>
														<td><span></span> : <span class="fabricpurchases-sum-val"><?php echo $tot; ?></span></td>
													</tr>
												</tfoot>
										</table>
										</div>
									</div>
								</div>
							</div>

							<!--...........................................SALES ACCORDIANS...............................-->

							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_4" style="background:#B1B8AD !important; color:white;font-family:open sans;height: 40px;">
											<div class="row">
												<div class="col-lg-9">
													<span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-pencil-square-o" style="color:#90906F;"></i></span> Sale
												</div>
												<div class="col-lg-3" style="font-size:12px !important;">
													<span>Total:</span> : <span class="sales-sum"> 00</span>
												</div>
											</div>
										</a>
									</h4>
								</div>
								<div id="accordion_a_4" class="panel-collapse collapse">
									<div class="panel-body">
										<div id="no-more-tables">
										<table class="table table-striped table-hover">
											<thead class="cf">
													<tr>
														<th class="numeric" style='background: #368EE0;max-width:20px !important;'>Vr#</th>
														<th style='background: #368EE0;'>Time</th>
														<th style='background: #368EE0;'>User</th>
														<th style='background: #368EE0;'>Party</th>
														<th class="numeric" style='background: #368EE0;' class="text-left">Discount</th>
														<th class="numeric" style='background: #368EE0;' class="text-left">Commission</th>
														<th style='background: #368EE0;' class="text-left numeric">Tax</th>
														<th style='background: #368EE0;' class="text-right numeric">Net Amount</th>
													</tr>
												</thead>
												<tbody>
													<?php $counter = 1; $tot=0; foreach ($sales as $Yarnpurchase): $tot +=$Yarnpurchase['namount']; ?>
														<tr>
															<td data-title="Vr#" class="numeric"><a href= "<?php echo base_url() . "index.php/saleorder/Sale_Invoice?vrnoa=" . $Yarnpurchase['vrnoa'] ;?>"></a> <?php echo $Yarnpurchase['vrnoa']; ?></td>
															<td data-title="Time" ><?php echo $Yarnpurchase['date_time']; ?></td>
															<td data-title="User" ><?php echo $Yarnpurchase['user_name']; ?></td>
															<td data-title="Account" ><?php echo $Yarnpurchase['party_name']; ?></td>
															<td data-title="Disc" class="numeric">
																<span class="text-primary"><b><?php echo $Yarnpurchase['discp']; ?>%</b></span></li>&nbsp;&nbsp;&nbsp;
																<span class="text-danger text-right"><b><?php echo $Yarnpurchase['discount']; ?></b></span></li>
															</td>
															<td data-title="Comm" class="numeric">
																<span class="text-primary"><b><?php echo $Yarnpurchase['exppercent']; ?>%</b></span>&nbsp;&nbsp;&nbsp;
																<span class="text-danger text-right"><b><?php echo $Yarnpurchase['expense']; ?></b></span>
															</td>
															<td data-title="Tax" class="numeric">
																<span class="text-primary"><b><?php echo $Yarnpurchase['taxpercent']; ?>%</b></span></li>&nbsp;&nbsp;&nbsp;
																<span class="text-danger text-right"><b><?php echo $Yarnpurchase['tax']; ?></b></span></li>
															</td>
															<td data-title="NetAmount" class="numeric" style="text-align:right;"><?php echo $Yarnpurchase['namount']; ?></td>
														</tr>
													<?php endforeach  ?>
												</tbody>
												<tfoot class="hidden">
													<tr>
														<td><span></span> : <span class="sales-sum-val"><?php echo $tot; ?></span></td>
													</tr>
												</tfoot>
										</table>
										</div>
									</div>
								</div>
							</div>

							<!--......................................SALE ORDER ACCORDIANS.................................

							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_5" style="background:#CAD0C6 !important; color:white;font-family:open sans;height: 40px;">
											<div class="row">
												<div class="col-lg-9">
													<span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-file-text" style="color:#90906F;"></i></span> <span style="font-size:15px !important;">Sale Order</span>
												</div>
												<div class="col-lg-3" style="font-size:12px !important;">
													<span>Total:</span> : <span class="saleOrder-sum"> 00</span>
												</div>
											</div>
										</a>
									</h4>
								</div>
								<div id="accordion_a_5" class="panel-collapse collapse">
									<div class="panel-body">
										<div id="no-more-tables">
										<table class="table table-striped table-hover">
											<thead class="cf">
													<tr>
														<th class="numeric" style='background: #368EE0;max-width:20px !important;'>Vr#</th>
														<th style='background: #368EE0;'>Time</th>
														<th style='background: #368EE0;'>User</th>
														<th style='background: #368EE0;'>Party</th>
														<th class="numeric" style='background: #368EE0;' class="text-left">Discount</th>
														<th class="numeric" style='background: #368EE0;' class="text-left">Commission</th>
														<th style='background: #368EE0;' class="text-left numeric">Tax</th>
														<th style='background: #368EE0;' class="text-right numeric">Net Amount</th>
													</tr>
												</thead>
												<tbody>
													<?php $counter = 1; $tot=0; foreach ($saleOrders as $Yarnpurchase): $tot +=$Yarnpurchase['namount']; ?>
														<tr>
															<td data-title="Vr#" class="numeric" ><a href= "<?php echo base_url() . "index.php/saleorder?vrnoa=" . $Yarnpurchase['vrnoa'] ;?>"></a> <?php echo $Yarnpurchase['vrnoa']; ?></td>
															<td data-title="Time"><?php echo $Yarnpurchase['date_time']; ?></td>
															<td data-title="User"><?php echo $Yarnpurchase['user_name']; ?></td>
															<td data-title="Account"><?php echo $Yarnpurchase['party_name']; ?></td>
															<td data-title="Disc" class="numeric" >
																<span class="text-primary"><b><?php echo $Yarnpurchase['discp']; ?>%</b></span></li>&nbsp;&nbsp;&nbsp;
																<span class="text-danger text-right"><b><?php echo $Yarnpurchase['discount']; ?></b></span></li>
															</td>
															<td data-title="Comm" class="numeric" >
																<span class="text-primary"><b><?php echo $Yarnpurchase['exppercent']; ?>%</b></span>&nbsp;&nbsp;&nbsp;
																<span class="text-danger text-right"><b><?php echo $Yarnpurchase['expense']; ?></b></span>
															</td>
															<td data-title="Tax" class="numeric" >
																<span class="text-primary"><b><?php echo $Yarnpurchase['taxpercent']; ?>%</b></span></li>&nbsp;&nbsp;&nbsp;
																<span class="text-danger text-right"><b><?php echo $Yarnpurchase['tax']; ?></b></span></li>
															</td>
															<td data-title="NetAmount" class="numeric"  style="text-align:right;"><?php echo $Yarnpurchase['namount']; ?></td>
														</tr>
													<?php endforeach  ?>
												</tbody>
												<tfoot class="hidden">
													<tr>
														<td><span></span> : <span class="saleOrder-sum-val"><?php echo $tot; ?></span></td>
													</tr>
												</tfoot>
										</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
				<div class="row-fluid">
					<div class="col-md-12">
						<h4 class="text-primary" style="font-family:open sans;"><i class="fa fa-building"></i> <b>A</b>ccounts</h4>
						<div class="panel-group" id="accordion_a">-->

							<!--..........................................PAYMENTS ACCORDIANS................................-->

								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_6" style="background: #CAD0C6 !important; color:white;font-family:open sans;height: 40px;">
												<div class="row">
													<div class="col-lg-9">
														<span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-money" style="color:#CECE28;"></i></span> <span style="font-size:15px !important;"> Payments
													</div>
													<div class="col-lg-3" style="font-size:12px !important;">
														<span>Total :</span> : <span class="sum_payments">0</span>
													</div>
												</div>
											</a>
										</h4>
									</div>
									<div id="accordion_a_6" class="panel-collapse collapse">
										<div class="panel-body">
											<div id="no-more-tables">
											<table class="table table-striped table-hover">
												<thead class="cf">
													<tr>
														<th class="numeric" style='background: #368EE0;' >Vr#</th>
														<th style='background: #368EE0;' >Time</th>
														<th style='background: #368EE0;' >User</th>
														<th style='background: #368EE0;'>Account Name</th>
														<th style='background: #368EE0;'>Remarks</th>
														<th class="numeric" style='background: #368EE0;'>Inv#</th>
														<th class="numeric" style='background: #368EE0;'>WO#</th>
														<th style='background: #368EE0;' class="text-right numeric">Debit</th>
														<th style='background: #368EE0;' class="text-right numeric">Credit</th>
													</tr>
												</thead>
												<tbody>
													<?php $counter = 1;$tot=0; foreach ($paymentss as $receipt):$tot +=$receipt['debit']; ?>
														<tr>
															<td data-title="Vr#" class="numeric"><a href= "<?php echo base_url() . "index.php/payment?vrnoa=" . $receipt['dcno'] . "&etype=cpv" ;?>"></a> <?php echo $receipt['dcno']; ?></td>
															<td data-title="Time" ><?php echo $receipt['date_time']; ?></td>
															<td data-title="User" ><?php echo $receipt['user_name']; ?></td>
															<td data-title="Account" ><?php echo $receipt['party_name']; ?></td>
															<td data-title="Remarks" ><?php echo $receipt['description']; ?></td>
															<td class="numeric" data-title="Inv#" ><?php echo $receipt['invoice']; ?></td>
															<td class="numeric" data-title="Wo#" ><?php echo $receipt['wo']; ?></td>
															<td data-title="Debit"  class="text-primary text-right numeric"><b><?php echo $receipt['debit']; ?></b></td>
															<td data-title="Credit"  class="text-danger text-right numeric"><b><?php echo $receipt['credit']; ?></b></td>
															
														</tr>
													<?php endforeach ?>
												</tbody>
												<tfoot class="hidden">
													<tr>
														<td><span></span> : <span class="sum_payments-val"><?php echo $tot; ?></span></td>
													</tr>
												</tfoot>
											</table>
											</div>
										</div>
									</div>
								</div>

								<!--...........................................RECEIPTS ACCORDIANS..............................-->

								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_7" style="background:#B1B8AD !important; color:white;font-family:open sans;height: 40px;">
												<div class="row">
													<div class="col-lg-9">
														<span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-money" style="color:#CECE28;"></i></span> <span style="font-size:15px !important;"> Receipts
													</div>
													<div class="col-lg-3" style="font-size:12px !important;">
														<span>Total:</span> : <span class="sum_receipts"> </span>
													</div>
												</div>
											</a>
										</h4>
									</div>
									<div id="accordion_a_7" class="panel-collapse collapse">
										<div class="panel-body">
											<div id="no-more-tables">
											<table class="table table-striped table-hover">
												<thead class="cf">
													<tr>
														<th class="numeric" style='background: #368EE0;' >Vr#</th>
														<th style='background: #368EE0;' >Time</th>
														<th style='background: #368EE0;' >User</th>
														<th style='background: #368EE0;'>Account Name</th>
														<th style='background: #368EE0;'>Remarks</th>
														<th class="numeric" style='background: #368EE0;'>Inv#</th>
														<th class="numeric" style='background: #368EE0;'>WO#</th>
														<th style='background: #368EE0;' class="text-right numeric">Debit</th>
														<th style='background: #368EE0;' class="text-right numeric">Credit</th>
													</tr>
												</thead>
												<tbody>
													<?php $counter = 1;$tot=0; foreach ($receiptss as $receipt): $tot +=$receipt['debit']; ?>
														<tr>
															<td data-title="Vr#" class="numeric" ><a href= "<?php echo base_url() . "index.php/payment?vrnoa=" . $receipt['dcno'] . "&etype=crv" ;?>"></a> <?php echo $receipt['dcno']; ?></td>
															<td data-title="Time" ><?php echo $receipt['date_time']; ?></td>
															<td data-title="User" ><?php echo $receipt['user_name']; ?></td>
															<td data-title="Account" ><?php echo $receipt['party_name']; ?></td>
															<td data-title="Remarks" ><?php echo $receipt['description']; ?></td>
															<td class="numeric" data-title="Inv#" ><?php echo $receipt['invoice']; ?></td>
															<td class="numeric" data-title="Wo#" ><?php echo $receipt['wo']; ?></td>
															<td data-title="Debit"  class="text-primary text-right numeric"><b><?php echo $receipt['debit']; ?></b></td>
															<td data-title="Credit"  class="text-danger text-right numeric"><b><?php echo $receipt['credit']; ?></b></td>
															
														</tr>
													<?php endforeach ?>
												</tbody>
												<tfoot class="hidden">
													<tr>
														<td><span></span> : <span class="sum_receipts-val"><?php echo $tot; ?></span></td>
													</tr>
												</tfoot>
											</table>
											</div>
										</div>
									</div>
								</div>

								<!--.............................................CHEQUE ISSUE ACCORDIANS ...........................

								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_bpv" style="background:#8D9389 !important; color:white;font-family:open sans;height: 40px;">
												<div class="row">
													<div class="col-lg-9">
														<span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-building" style="color:#CECE28;"></i></span> <span style="font-size:15px !important;"> Bank Payments
													</div> 
													<div class="col-lg-3" style="font-size:12px !important;">
														<span>Total:</span> : <span class="bpv-sum">0</span>
													</div>
												</div>
											</a>
										</h4>
									</div>
									<div id="accordion_a_bpv" class="panel-collapse collapse">
										<div class="panel-body">
											<div id="no-more-tables">
											<table class="table table-striped table-hover">
												<thead class="cf">
													<tr>
														<th class="numeric" style='background: #368EE0;' >Vr#</th>
														<th style='background: #368EE0;' >Time</th>
														<th style='background: #368EE0;' >User</th>
														<th style='background: #368EE0;'>Account Name</th>
														<th style='background: #368EE0;'>Remarks</th>
														<th class="numeric" style='background: #368EE0;'>Chq#</th>
														<th class="numeric" style='background: #368EE0;'>ChqDate</th>
														<th class="numeric" style='background: #368EE0;'>Inv#</th>
														<th class="numeric" style='background: #368EE0;'>WO#</th>
														<th style='background: #368EE0;' class="text-right numeric">Debit</th>
														<th style='background: #368EE0;' class="text-right numeric">Credit</th>
													</tr>
												</thead>
												<tbody>
													<?php $counter = 1;$tot=0; foreach ($bpvs as $receipt): $tot +=$receipt['debit']; ?>
														<tr>
															<td data-title="Vr#" class="numeric"><a href= "<?php echo base_url() . "index.php/jv/bpv?vrnoa=" . $receipt['dcno'] ;?>"></a> <?php echo $receipt['dcno']; ?></td>
															<td data-title="Time" ><?php echo $receipt['date_time']; ?></td>
															<td data-title="User" ><?php echo $receipt['user_name']; ?></td>
															<td data-title="Account" ><?php echo $receipt['party_name']; ?></td>
															<td data-title="Remarks" ><?php echo $receipt['description']; ?></td>
															<td class="numeric"  data-title="Chq#" ><?php echo $receipt['chq_no']; ?></td>
															<td class="numeric"  data-title="ChqDate" ><?php echo $receipt['chq_date']; ?></td>
															<td class="numeric"  data-title="Inv#" ><?php echo $receipt['invoice']; ?></td>
															<td class="numeric"  data-title="Wo#" ><?php echo $receipt['wo']; ?></td>
															<td data-title="Debit"  class="text-primary text-right numeric"><b><?php echo $receipt['debit']; ?></b></td>
															<td data-title="Credit"  class="text-danger text-right numeric"><b><?php echo $receipt['credit']; ?></b></td>
															
														</tr>
													<?php endforeach ?>
												</tbody>
												<tfoot class="hidden">
													<tr>
														<td><span></span> : <span class="bpv-sum-val"><?php echo $tot; ?></span></td>
													</tr>
												</tfoot>
											</table>
											</div>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_brv" style="background:#8D9389 !important; color:white;font-family:open sans;height: 40px;">
												<div class="row">
													<div class="col-lg-9">
														<span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-building" style="color:#CECE28;"></i></span> <span style="font-size:15px !important;"> Bank Receipts
													</div> 
													<div class="col-lg-3" style="font-size:12px !important;">
														<span>Total:</span> : <span class="brv-sum">0</span>
													</div>
												</div>
											</a>
										</h4>
									</div>
									<div id="accordion_a_brv" class="panel-collapse collapse">
										<div class="panel-body">
											<div id="no-more-tables">
											<table class="table table-striped table-hover">
												<thead class="cf">
													<tr>
														<th class="numeric" style='background: #368EE0;' >Vr#</th>
														<th style='background: #368EE0;' >Time</th>
														<th style='background: #368EE0;' >User</th>
														<th style='background: #368EE0;'>Account Name</th>
														<th style='background: #368EE0;'>Remarks</th>
														<th class="numeric" style='background: #368EE0;'>Chq#</th>
														<th class="numeric" style='background: #368EE0;'>ChqDate</th>
														<th class="numeric" style='background: #368EE0;'>Inv#</th>
														<th class="numeric" style='background: #368EE0;'>WO#</th>
														<th style='background: #368EE0;' class="text-right numeric">Debit</th>
														<th style='background: #368EE0;' class="text-right numeric">Credit</th>
													</tr>
												</thead>
												<tbody>
													<?php $counter = 1;$tot=0; foreach ($brvs as $receipt): $tot +=$receipt['debit']; ?>
														<tr>
															<td numeric data-title="Vr#" ><a href= "<?php echo base_url() . "index.php/jv/brv?vrnoa=" . $receipt['dcno'] ;?>"></a> <?php echo $receipt['dcno']; ?></td>
															<td data-title="Time" ><?php echo $receipt['date_time']; ?></td>
															<td data-title="User" ><?php echo $receipt['user_name']; ?></td>
															<td data-title="Account" ><?php echo $receipt['party_name']; ?></td>
															<td data-title="Remarks" ><?php echo $receipt['description']; ?></td>
															<td class="numeric"  data-title="Chq#" ><?php echo $receipt['chq_no']; ?></td>
															<td class="numeric"  data-title="ChqDate" ><?php echo $receipt['chq_date']; ?></td>
															<td class="numeric"  data-title="Inv#" ><?php echo $receipt['invoice']; ?></td>
															<td class="numeric"  data-title="Wo#" ><?php echo $receipt['wo']; ?></td>
															<td   data-title="Debit"  class="text-primary text-right numeric"><b><?php echo $receipt['debit']; ?></b></td>
															<td   data-title="Credit"  class="text-danger text-right numeric"><b><?php echo $receipt['credit']; ?></b></td>
															
														</tr>
													<?php endforeach ?>
												</tbody>
												<tfoot class="hidden">
													<tr>
														<td><span></span> : <span class="brv-sum-val"><?php echo $tot; ?></span></td>
													</tr>
												</tfoot>
											</table>
											</div>
										</div>
									</div>
								</div>-->
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_8" style="background:#8D9389 !important; color:white;font-family:open sans;height: 40px;">
												<div class="row">
													<div class="col-lg-9">
														<span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-newspaper-o" style="color:#CECE28;"></i></span> <span style="font-size:15px !important;"> Cheque Issue
													</div> 
													<div class="col-lg-3" style="font-size:12px !important;">
														<span>Total:</span> : <span class="pd_issue-sum">0</span>
													</div>
												</div>
											</a>
										</h4>
									</div>
									<div id="accordion_a_8" class="panel-collapse collapse">
										<div class="panel-body">
											<div id="no-more-tables">
											<table class="table table-striped table-hover">
												<thead class="cf">
													<tr>
														<th class="numeric" style='background: #368EE0;' >Vr#</th>
														<th style='background: #368EE0;' >Time</th>
														<th style='background: #368EE0;' >User</th>
														<th style='background: #368EE0;'>Account Name</th>
														<th style='background: #368EE0;'>Remarks</th>
														<th class="numeric" style='background: #368EE0;'>Chq#</th>
														<th class="numeric"  style='background: #368EE0;'>ChqDate</th>
														<th class="numeric" style='background: #368EE0;'>Inv#</th>
														<th class="numeric" style='background: #368EE0;'>WO#</th>
														<th style='background: #368EE0;' class="text-right numeric">Debit</th>
														<th style='background: #368EE0;' class="text-right numeric">Credit</th>
													</tr>
												</thead>
												<tbody>
													<?php $counter = 1;$tot=0; foreach ($cheqIssues as $receipt): $tot +=$receipt['debit']; ?>
														<tr>
															<td data-title="Vr#" class="numeric"><a href= "<?php echo base_url() . "index.php/payment/chequeIssue?vrnoa=" . $receipt['dcno'] ;?>"></a> <?php echo $receipt['dcno']; ?></td>
															<td data-title="Time"><?php echo $receipt['date_time']; ?></td>
															<td data-title="User"><?php echo $receipt['user_name']; ?></td>
															<td data-title="Account"><?php echo $receipt['party_name']; ?></td>
															<td data-title="Remarks"><?php echo $receipt['description']; ?></td>
															<td data-title="chq#" class="numeric"><?php echo $receipt['chq_no']; ?></td>
															<td data-title="ChqDate"><?php echo $receipt['chq_date']; ?></td>
															<td data-title="Inv#" class="numeric"><?php echo $receipt['invoice']; ?></td>
															<td data-title="Wo" class="numeric"><?php echo $receipt['wo']; ?></td>
															<td data-title="Debit" class="text-primary text-righ numerict"><b><?php echo $receipt['debit']; ?></b></td>
															<td data-title="Credit" class="text-danger text-right numeric"><b><?php echo $receipt['credit']; ?></b></td>
															
														</tr>
													<?php endforeach ?>
												</tbody>
												<tfoot class="hidden">
													<tr>
														<td><span></span> : <span class="pd_issue-sum-val"><?php echo $tot; ?></span></td>
													</tr>
												</tfoot>
											</table>
											</div>
										</div>
									</div>
								</div>

								<!--.........................................CHEQUE RECEIVE ACCORDIANS...............................-->

								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_9" style="background:#747672 !important; color:white;font-family:open sans;height: 40px;">
												<div class="row">
													<div class="col-lg-9">
														<span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-newspaper-o" style="color:#CECE28;"></i></span> <span style="font-size:15px !important;"> Cheque Receives
													</div>
													<div class="col-lg-3" style="font-size:12px !important;">
														<span>Total:</span> : <span class="pd_receive-sum"> 0</span>
													</div>
												</div>
											</a>
										</h4>
									</div>
									<div id="accordion_a_9" class="panel-collapse collapse">
										<div class="panel-body">
											<div id="no-more-tables">
											<table class="table table-striped table-hover">
												<thead class="cf">
													<tr>
														<th class="numeric"style='background: #368EE0;' >Vr#</th>
														<th style='background: #368EE0;' >Time</th>
														<th style='background: #368EE0;' >User</th>
														<th style='background: #368EE0;'>Account Name</th>
														<th style='background: #368EE0;'>Remarks</th>
														<th class="numeric" style='background: #368EE0;'>Chq#</th>
														<th class="numeric" style='background: #368EE0;'>ChqDate</th>
														<th class="numeric" style='background: #368EE0;'>Inv#</th>
														<th class="numeric" style='background: #368EE0;'>WO#</th>
														<th style='background: #368EE0;' class="text-right numeric">Debit</th>
														<th style='background: #368EE0;' class="text-right numeric">Credit</th>
													</tr>
												</thead>
												<tbody>
													<?php $counter = 1;$tot=0; foreach ($chequeReceives as $receipt): $tot +=$receipt['debit']; ?>
														<tr>
															<td data-title="Vr#" class="numeric"><a href= "<?php echo base_url() . "index.php/payment/chequeReceive?vrnoa=" . $receipt['dcno'] ;?>"></a> <?php echo $receipt['dcno']; ?></td>
															<td data-title="Time"><?php echo $receipt['date_time']; ?></td>
															<td data-title="User"><?php echo $receipt['user_name']; ?></td>
															<td data-title="Account"><?php echo $receipt['party_name']; ?></td>
															<td data-title="Remarks"><?php echo $receipt['description']; ?></td>
															<td data-title="chq#" class="numeric"><?php echo $receipt['chq_no']; ?></td>
															<td data-title="ChqDate"><?php echo $receipt['chq_date']; ?></td>
															<td data-title="Inv#" class="numeric"><?php echo $receipt['invoice']; ?></td>
															<td data-title="Wo" class="numeric"><?php echo $receipt['wo']; ?></td>
															<td data-title="Debit" class="text-primary text-righ numerict"><b><?php echo $receipt['debit']; ?></b></td>
															<td data-title="Credit" class="text-danger text-right numeric"><b><?php echo $receipt['credit']; ?></b></td>
															
														</tr>
													<?php endforeach ?>
												</tbody>
												<tfoot class="hidden">
													<tr>
														<td><span></span> : <span class="pd_receive-sum-val"><?php echo $tot; ?></span></td>
													</tr>
												</tfoot>
										</table>
										</div>
									</div>
								</div>
							</div>

							<!--....................................EXPENSES ACCORDIANS...............................-->

							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_10" style="background:#5A6062 !important; color:white;font-family:open sans;height: 40px;">
											<div class="row">
												<div class="col-lg-9">
													<span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-xing-square" style="color:#CECE28;"></i></span> <span style="font-size:15px !important;"> Expenses
												</div>
												<div class="col-lg-3" style="font-size:12px !important;">
													<span>Total:</span> : <span class="expenses-sum">0</span>
												</div>
											</div>
										</a>
									</h4>
								</div>
								<div id="accordion_a_10" class="panel-collapse collapse">
									<div class="panel-body">
										<div id="no-more-tables">
										<table class="table table-striped table-hover">
											<thead class="cf">
												<tr>
													<th class="numeric" style='background: #368EE0;'>Vr#</th>
													<th style='background: #368EE0;'>Party Name</th>
													<th class="numeric" style='background: #368EE0;'>Amount</th>
												</tr>
											</thead>
											<tbody>
												<?php $counter = 1;$tot=0; foreach ($expensess as $expenses): $tot +=$expenses['AMOUNT']; ?>
													<tr>
														<td data-title="Vr#" class="numeric"><?php echo $counter++; ?></td>
														<td data-title="Account"><?php echo $expenses['NAME']; ?></td>
														<td data-title="Amount" class="text-primary numeric"><b><?php echo $expenses['AMOUNT']; ?></b></td>
													</tr>
												<?php endforeach ?>
											</tbody>
											<tfoot class="hidden">
												<tr>
													<td><span></span> : <span class="expenses-sum-val"><?php echo $tot; ?></span></td>
												</tr>
											</tfoot>
										</table>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- container-fluid -->
	</div><!-- main -->
</div><!-- container-fluid -->
<script src=" <?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/app_modules/general.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/app_modules/dashboard.js'); ?>"></script>