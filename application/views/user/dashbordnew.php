<?php
function makeHref($etype , $vrnoa){
  
  $voucher_type = '';

  if ( strtolower($etype) == 'sale' ) {
      $voucher_type = base_url() . '/index.php/saleorder/sale_invoice?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'saleorder' ) {
      $voucher_type = base_url() . '/index.php/saleorder?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'jv' ) {
      $voucher_type = base_url() . '/index.php/jv?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'pur_order' ) {
      $voucher_type = base_url() . '/index.php/purchaseorder?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'openingstock' ) {
      $voucher_type = base_url() . '/index.php/openingstock?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'quotationorder' ) {
      $voucher_type = base_url() . '/index.php/quotationorder?vrnoa=' . $vrnoa;
  } else if ( ( strtolower($etype) == 'cpv' ) || ( strtolower($etype) == 'crv' ) ) {
      $voucher_type = base_url() . '/index.php/payment?vrnoa=' . $vrnoa . '&etype=' . strtolower($etype);
  } else if ( ( strtolower($etype) == 'pd_issue' ) ) {
      $voucher_type = base_url() . '/index.php/payment/chequeissue?vrnoa=' . $vrnoa;
  } else if ( ( strtolower($etype) == 'pd_receive' ) ) {
      $voucher_type = base_url() . '/index.php/payment/chequereceive?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'purchase' ) {
      $voucher_type = base_url() . '/index.php/purchase?vrnoa=' . $vrnoa ;
  } else if ( strtolower($etype) == 'saleorder' ) {
      $voucher_type = base_url() . '/index.php/saleorder?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'salereturn' ) {
      $voucher_type = base_url() . '/index.php/salereturn?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'purchasereturn' ) {
      $voucher_type = base_url() . '/index.php/purchasereturn?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'pur_import' ) {
      $voucher_type = base_url() . '/index.php/purchase/import?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'assembling' ) {
      $voucher_type = base_url() . '/index.php/item/assdeass?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'navigation' ) {
      $voucher_type = base_url() . '/index.php/stocknavigation?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'production' ) {
      $voucher_type = base_url() . '/index.php/production?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'consumption' ) {
      $voucher_type = base_url() . '/index.php/consumption?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'materialreturn' ) {
      $voucher_type = base_url() . '/index.php/materialreturn?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'moulding' ) {
      $voucher_type = base_url() . '/index.php/moulding?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'order_loading' ) {
      $voucher_type = base_url() . '/index.php/saleorder/partsloading?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'outwardvoucher' ) {
      $voucher_type = base_url() . '/index.php/outwardvoucher?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'inwardvoucher' ) {
      $voucher_type = base_url() . '/index.php/inwardvoucher?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'requisition' ) {
      $voucher_type = base_url() . '/index.php/requisition?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'item_conversion' ) {
      $voucher_type = base_url() . '/index.php/purchase/itemconversion?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'openingbalance' ) {
      $voucher_type = base_url() . '/index.php/openingbalance?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'penalty' ) {
      $voucher_type = base_url() . '/index.php/charge/penalty?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'loan' ) {
      $voucher_type = base_url() . '/index.php/loan?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'advance' ) {
      $voucher_type = base_url() . '/index.php/payment/advance?vrnoa=' . $vrnoa;
  } else if ( strtolower($etype) == 'incentive' ) {
      $voucher_type = base_url() . '/index.php/payment/incentive?vrnoa=' . $vrnoa;
  }
  else {
      $voucher_type = $vrnoa . '-' . $etype;
  }

  return $voucher_type;

}


function abbrivateEtypes( $etype ){

    if( $etype == 'pur_order' ){
      $etype = 'PO';
    }else if ( $etype == 'purchase' ){
      $etype = 'PI';
    }else if ( $etype == 'purchasereturn' ){
      $etype = 'PR';
    }else if ( $etype == 'quotationorder' ){
      $etype = 'QV';
    }else if ( $etype == 'saleorder' ){
      $etype = 'SO';
    }else if ( $etype == 'outwardvoucher' ){
      $etype = 'OGP';
    }else if ( $etype == 'sale' ){
      $etype = 'SI';
    }else if ( $etype == 'salereturn' ){
      $etype = 'SR';
    }else if ( $etype == 'outwardvoucher' ){
      $etype = 'OGP';
    }else if ( $etype == 'requisition' ){
      $etype = 'RV';
    }else if ( $etype == 'inwardvoucher' ){
      $etype = 'IGP';
    }else if ( $etype == 'consumption' ){
      $etype = 'CNS';
    }else if ( $etype == 'materialreturn' ){
      $etype = 'MR';
    }else if ( $etype == 'navigation' ){
      $etype = 'NV';
    }else if ( $etype == 'production' ){
      $etype = 'PR';
    }else if ( $etype == 'cpv' ){
      $etype = 'CPV';
    }else if ( $etype == 'crv' ){
      $etype = 'CRV';
    }else if ( $etype == 'pd_issue' ){
      $etype = 'CHI';
    }else if ( $etype == 'pd_receive' ){
      $etype = 'CHR';
    }else if ( $etype == 'jv' ){
      $etype = 'JV';
    }else if ( $etype == 'openingbalance' ){
      $etype = 'OB';
    }else if ( $etype == 'openingstock' ){
      $etype = 'OS';
    } else if ( $etype == 'item_conversion' ){
      $etype = 'ITC';
    } else if ( $etype == 'penalty' ){
      $etype = 'PNL';
    } else if ( $etype == 'loan' ){
      $etype = 'LN';
    } else if ( $etype == 'advance' ){
      $etype = 'ADV';
    } else if ( $etype == 'incentive' ){
      $etype = 'INC';
    }
    
    return $etype;
  }
?>



<link href="<?php echo base_url(). 'assets/css/style_dashbord.css'; ?>" rel="stylesheet" media="screen">
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
  <div id="container" class="row-fluid">
    <div id="main-content">
      <div class="row-fluid Page_header">
        <div class="col-sm-12">
          <h1 class="heading"><i class="fa fa-cogs" style='font-size:30px;'></i> Foundry </h1>
          <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
        </div><br>
      </div><!-- row-fluid -->
      <!-- <br><br> -->
      <div class="container-fluid dashbord1_container hide">      
        <div class="row-fluid">
          <div class="metro-nav">
            <div class="metro-nav-block nav-block-orange">
              <a data-original-title="" href="<?php echo base_url() . 'index.php/item'; ?>">
                <i class="fa fa-user"></i>
                <div class="info"><?php //echo $getmaxStaffid; ?></div>
                <div class="status">Add New Item</div>
              </a>
            </div><!-- metro-nav -->
            <div class="metro-nav-block nav-olive">
              <a data-original-title="" href="<?php echo base_url() . 'index.php/payment/chequeissue'; ?>">
                <i class="fa fa-home"></i>
                <div class="info"><?php //echo $getmaxDepartmentid; ?></div>
                <div class="status">Cheque Paid</div>
              </a>
            </div><!-- metro-nav -->
            <div class="metro-nav-block nav-block-yellow">
              <a data-original-title="" href="<?php echo base_url() . 'index.php/payment/chequereceive'; ?>">
                <i class="fa fa-credit-card"></i>
                <div class="info"><?php //echo $getmaxAppProcedureid; ?></div>
                <div class="status">Cheque Received</div>
              </a>
            </div><!-- metro-nav -->
            <div class="metro-nav-block nav-block-green double">
              <a data-original-title="" href="<?php echo base_url() . 'index.php/warehouse/add'; ?>">
                <i class="fa fa-calendar"></i>
                <div class="info"><?php //echo $getmaxApplicationid; ?></div>
                <div class="status">Stock Location</div>
              </a>
            </div><!-- metro-nav -->
            <div class="metro-nav-block nav-block-red">
              <a data-original-title="" href="<?php echo base_url() . 'index.php/salesman'; ?>">
                <i class="fa fa-pagelines"></i>
                <div class="info"><?php //echo $getmaxCityid; ?></div>
                <div class="status">Add New Salesman</div>
              </a>
            </div><!-- metro-nav -->
          </div><!-- metro-nav -->
          <div class="metro-nav">
            <div class="metro-nav-block nav-block-grey ">
              <a data-original-title="" href="<?php echo base_url() . 'index.php/account/add'; ?>">
                <i class="fa fa-money"></i>
                <div class="info"><?php //echo $getmaxAppnameid; ?></div>
                <div class="status">New Account</div>
              </a>
            </div><!-- metro-nav -->
            <div class="metro-nav-block nav-light-purple">
              <a data-original-title="" href="<?php echo base_url() . 'index.php/saleorder/sale_invoice'; ?>">
                <i class="ion-ios7-keypad-outline"></i>
                <div class="info"><?php //echo $getmaxappstatussid; ?></div>
                <div class="status">Sale Voucher</div>
              </a>
            </div><!-- metro-nav -->
            <div class="metro-nav-block nav-light-blue double">
              <a data-original-title="" href="<?php echo base_url() . 'index.php/saleorder'; ?>">
                <i class="ion-ios7-people"></i>
                <div class="info"><?php //echo $getmaxapptypesid; ?></div>
                <div class="status">Sale Order</div>
              </a>
            </div><!-- metro-nav -->
            <div class="metro-nav-block nav-light-green">
              <a data-original-title="" href="<?php echo base_url() . 'index.php/purchase'; ?>">
                <i class="fa fa-th-large"></i>
                <div class="info"><?php //echo $getmaxusersid; ?></div>
                <div class="status">Purchase Voucher</div>
              </a>
            </div><!-- metro-nav -->
            <div class="metro-nav-block nav-light-brown">
              <a data-original-title="" href="<?php echo base_url() . 'index.php/purchasereturn'; ?>">
                <i class="fa fa-cloud-upload"></i>
                <div class="info"><!-- <?php //echo $getmaxDealid; ?> --></div>
                <div class="status">Purchase Return</div>
              </a>
            </div><!-- metro-nav -->
          </div><!-- metro-nav -->
          <div class="metro-nav">
            <div class="metro-nav-block nav-block-green double">
              <a data-original-title="" href="<?php echo base_url() . 'index.php/report/sale'; ?>">
                <i class="fa fa-globe"></i>
                <div class="info"><?php //echo $getmaxtokenid; ?></div>
                <div class="status">Sale Report</div>
              </a>
            </div><!-- metro-nav -->
            <div class="metro-nav-block nav-block-orange">
              <a data-original-title="" href="<?php echo base_url() . 'index.php/report/salereturn'; ?>">
                <i class="fa fa-user"></i>
                <div class="info"><?php //echo $getmaxclassid; ?></div>
                <div class="status">Sale Return Report</div>
              </a>
            </div><!-- metro-nav -->
            <div class="metro-nav-block nav-block-red">
              <a data-original-title="" href="<?php echo base_url() . 'index.php/openingstock'; ?>">
                <i class="fa fa-list"></i>
                <div class="info"><?php //echo $getmaxbankid; ?></div>
                <div class="status">Opening Stock</div>
              </a>
            </div><!--metro-nav-block-->
            <div class="metro-nav-block nav-olive">
              <a data-original-title="" href="<?php echo base_url() . 'index.php/report/accountledger'; ?>">
                <i class="ion-ios7-personadd"></i>
                <div class="info"><!--<?php //echo $getmaxTaskid; ?>--></div>
                <div class="status">Account Ledger</div>
              </a>
            </div><!-- metro-nav -->
            <div class="metro-nav-block nav-block-yellow">
              <a data-original-title="" href="<?php echo base_url() . 'index.php/trial_balance'; ?>">
                <i class="fa fa-home"></i>
                <div class="info"><?php //echo $getmaxappfeeid; ?></div>
                <div class="status">Trial Balance</div>
              </a>
            </div><!-- metronav -->
          </div><!-- metro-nav -->
        </div><!-- row-fluid -->
      </div><!-- container-fluid -->

    <!--......................................... ALL VOUCHER TRANSCATIONS ...........................-->

    <div class="row-fluid" style="margin: -30px 56px 0px 55px;">
    <br><br><br><br><br>
      <div class="row-fluid">
        <div class="col-lg-12">
          <div class="panel-body">
            <div class="form-group">
              <div class="row">
                <div class="col-lg-3">
                  <label>From</label>
                  <input class="form-control ts_datepicker" type="text" id="from_date">
                </div>
                <div class="col-lg-3">
                  <label>TO</label>
                  <input class="form-control ts_datepicker" type="text" id="to_date">
                </div>
                <div class="col-lg-1" style="margin-top:30px;">
                  <a class="btn btn-success" id="btnshow"><i class="fa fa-search"></i> SHOW</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row-fluid">
                <div class="col-md-12">
                  <h4 class="text-primary" style="font-family:open sans;color:black;"><i class="ion-android-note"></i> <b>A</b>ccounts</h4>
                  <div class="panel-group" id="accordion_a">

                  <!--.............................PAYMENTS VOUCHER TRANSCATION..........................-->

                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_16" style="background: #282828  !important; color:white;font-family:open sans;height: 40px;">
                            <div class="row">
                              <div class="col-lg-9">
                                <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-money" style="color:#CECE28;"></i></span> <span style="font-size:15px !important;"> Payments
                              </div>
                              <div class="col-lg-3" style="font-size:12px !important;">
                                <span>Total :</span> : <span class="sum_cashpayments"><?php echo round($totalcashpayments[0]['totalaccount']);?></span>
                              </div>
                            </div>
                          </a>
                        </h4>
                      </div>
                      <div id="accordion_a_16" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="pull-right">
                                <button class="btn btn-default" id="btnTable16"><i class="fa fa-th-list"></i> List </button>
                                <button class="btn btn-default" id="btnGraph16"><i class="ion-pie-graph"></i> Chart</button>
                              </div>
                            </div>
                          </div><br>
                          <div id="no-more-tables">
                            <table class="table table-striped table-hover payment_tbl">
                              <thead class="cf">
                                <tr>
                                  <th class="numeric" style='background: #368EE0;' >Vr#</th>
                                  <th style='background: #368EE0;' >Time</th>
                                  <th style='background: #368EE0;' >User</th>
                                  <th style='background: #368EE0;'>Account Name</th>
                                  <th class="numeric" style='background: #368EE0;'>Inv#</th>
                                  <th style='background: #368EE0;' class="text-right numeric">Debit</th>
                                  <th style='background: #368EE0;' class="text-right numeric">Credit</th>
                                </tr>
                              </thead>
                              <tbody class="payment_tbody">
                                <?php $counter = 1; foreach ($paymentss as $payment): ?>
                                <tr>
                                  <td><a href="<?php echo makeHref($payment['etype'],$payment['dcno']) ; ?>"><?php echo $payment['dcno'] .'-'. abbrivateEtypes($payment['etype']); ?></a></td>
                                  <td><?php echo $payment['date_time']; ?></td>
                                  <td><?php echo $payment['user_name']; ?></td>
                                  <td><?php echo $payment['party_name']; ?></td>
                                  <td><?php echo $payment['invoice']; ?></td>
                                  <td><?php echo $payment['debit']; ?></td>
                                  <td class="text-right"><?php echo $payment['credit']; ?></td>
                                </tr>
                                <?php endforeach ?>
                              </tbody>
                              <tfoot class="hidden">
                                <tr>
                                  <td><span></span> : <span class="sum_payments-val"><?php //echo $tot; ?></span></td>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="tabs_payment hide">
                                <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                                  <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                                  <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                                  <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                                </ul>
                                <div class="tab-content">
                                  <div role="tabpanel" class="tab-pane active" id="home">...</div>
                                  <div role="tabpanel" class="tab-pane" id="profile">...</div>
                                  <div role="tabpanel" class="tab-pane" id="messages">...</div>
                                  <div role="tabpanel" class="tab-pane" id="settings">...</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <!--...................................RECEIPTS VOUCHET TRANSCTION......................-->

                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_17" style="background:#282828  !important; color:white;font-family:open sans;height: 40px;">
                            <div class="row">
                              <div class="col-lg-9">
                                <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-money" style="color:#CECE28;"></i></span> <span style="font-size:15px !important;"> Receipts
                              </div>
                              <div class="col-lg-3" style="font-size:12px !important;">
                                <span>Total:</span> : <span class="sum_cashreceipts"> <?php echo round($totalcashreceipts[0]['totalaccount']);?></span>
                              </div>
                            </div>
                          </a>
                        </h4>
                      </div>
                      <div id="accordion_a_17" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="pull-right">
                                <button class="btn btn-default" id="btnTable17"><i class="fa fa-th-list"></i> List </button>
                                <button class="btn btn-default" id="btnGraph17"><i class="ion-pie-graph"></i> Chart</button>
                              </div>
                            </div>
                          </div><br>
                          <div id="no-more-tables">
                            <table class="table table-striped table-hover receipt_tbl">
                              <thead class="cf">
                                <tr>
                                <th class="numeric" style='background: #368EE0;' >Vr#</th>
                                  <th style='background: #368EE0;' >Time</th>
                                  <th style='background: #368EE0;' >User</th>
                                  <th style='background: #368EE0;'>Account Name</th>
                                  <th class="numeric" style='background: #368EE0;'>Inv#</th>
                                  <th style='background: #368EE0;' class="text-right numeric">Debit</th>
                                  <th style='background: #368EE0;' class="text-right numeric">Credit</th>
                                </tr>
                              </thead>
                              <tbody class="receipt_tbody">
                                <?php $counter = 1; foreach ($receiptss as $receipt): ?>
                                  <tr>
                                    <td><a href="<?php echo makeHref($receipt['etype'],$receipt['dcno']) ; ?>"><?php echo $receipt['dcno'] .'-'. abbrivateEtypes($receipt['etype']); ?></a></td>
                                    <td><?php echo $receipt['date_time']; ?></td>
                                    <td><?php echo $receipt['user_name']; ?></td>
                                    <td><?php echo $receipt['party_name']; ?></td>
                                    <td><?php echo $receipt['invoice']; ?></td>
                                    <td><?php echo $receipt['debit']; ?></td>
                                    <td class="text-right"><?php echo $receipt['credit']; ?></td>
                                  </tr>
                                <?php endforeach ?>
                              </tbody>
                              <tfoot class="hidden">
                                <tr>
                                  <td><span></span> : <span class="sum_receipts-val"><?php //echo $tot; ?></span></td>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="tabs_receipt hide">
                                <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                                  <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                                  <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                                  <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                                </ul>
                                <div class="tab-content">
                                  <div role="tabpanel" class="tab-pane active" id="home">...</div>
                                  <div role="tabpanel" class="tab-pane" id="profile">...</div>
                                  <div role="tabpanel" class="tab-pane" id="messages">...</div>
                                  <div role="tabpanel" class="tab-pane" id="settings">...</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <!--....................................CHEQUE ISSUE VOUCHER TRANSCTION  ...........-->

                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_18" style="background:#282828  !important; color:white;font-family:open sans;height: 40px;">
                            <div class="row">
                              <div class="col-lg-9">
                                <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-money" style="color:#CECE28;"></i></span> <span style="font-size:15px !important;"> Cheque Issue
                              </div> 
                              <div class="col-lg-3" style="font-size:12px !important;">
                                <span>Total:</span> : <span class="bpv-sum-voucher"><?php echo round($totalbankpayments[0]['totalaccount']);?></span>
                              </div>
                            </div>
                          </a>
                        </h4>
                      </div>
                      <div id="accordion_a_18" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="pull-right">
                                <button class="btn btn-default" id="btnTable18"><i class="fa fa-th-list"></i> List </button>
                                <button class="btn btn-default" id="btnGraph18"><i class="ion-pie-graph"></i> Chart</button>
                              </div>
                            </div>
                          </div><br>
                          <div id="no-more-tables">
                            <table class="table table-striped table-hover chequeissue_tbl">
                              <thead class="cf">
                                <tr>
                                  <th class="numeric" style='background: #368EE0;' >Vr#</th>
                                  <th style='background: #368EE0;' >Time</th>
                                  <th style='background: #368EE0;' >User</th>
                                  <th style='background: #368EE0;'>Account Name</th>
                                  <th class="numeric" style='background: #368EE0;'>Inv#</th>
                                  <th style='background: #368EE0;' class="text-right numeric">Debit</th>
                                  <th style='background: #368EE0;' class="text-right numeric">Credit</th>
                                </tr>
                              </thead>
                              <tbody class="chequeissue_tbody">
                                <?php $counter = 1; foreach ($chqissuess as $chqissue): ?>
                                <tr>
                                  <td><a href="<?php echo makeHref($chqissue['etype'],$chqissue['dcno']) ; ?>"><?php echo $chqissue['dcno'] .'-'. abbrivateEtypes($chqissue['etype']); ?></a></td>
                                  <td><?php echo $chqissue['date_time']; ?></td>
                                  <td><?php echo $chqissue['user_name']; ?></td>
                                  <td><?php echo $chqissue['party_name']; ?></td>
                                  <td><?php echo $chqissue['invoice']; ?></td>
                                  <td><?php echo $chqissue['debit']; ?></td>
                                  <td class="text-right"><?php echo $chqissue['credit']; ?></td>
                                </tr>
                              <?php endforeach ?>
                            </tbody>
                            <tfoot class="hidden">
                              <tr>
                                <td><span></span> : <span class="bpv-sum-val"><?php //echo $tot; ?></span></td>
                              </tr>
                            </tfoot>
                          </table>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                              <div class="tabs_chqisue hide">
                                <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                                  <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                                  <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                                  <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                                </ul>
                                <div class="tab-content">
                                  <div role="tabpanel" class="tab-pane active" id="home">...</div>
                                  <div role="tabpanel" class="tab-pane" id="profile">...</div>
                                  <div role="tabpanel" class="tab-pane" id="messages">...</div>
                                  <div role="tabpanel" class="tab-pane" id="settings">...</div>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>

                  <!--...................................CHEQUE RECEIPT TRANSCATION.........................-->

                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_19" style="background:#282828 !important; color:white;font-family:open sans;height: 40px;">
                          <div class="row">
                            <div class="col-lg-9">
                              <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-money" style="color:#CECE28;"></i></span> <span style="font-size:15px !important;"> Cheque Receipts
                            </div> 
                            <div class="col-lg-3" style="font-size:12px !important;">
                              <span>Total:</span> : <span class="brv-sum-voucher"><?php echo round($totalbankreceipts[0]['totalaccount']);?></span>
                            </div>
                          </div>
                        </a>
                      </h4>
                    </div>
                    <div id="accordion_a_19" class="panel-collapse collapse">
                      <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                              <div class="pull-right">
                                <button class="btn btn-default" id="btnTable19"><i class="fa fa-th-list"></i> List </button>
                                <button class="btn btn-default" id="btnGraph19"><i class="ion-pie-graph"></i> Chart</button>
                              </div>
                            </div>
                          </div><br>
                        <div id="no-more-tables">
                          <table class="table table-striped table-hover chequereceipt_tbl">
                            <thead class="cf">
                              <tr>
                                <th class="numeric" style='background: #368EE0;' >Vr#</th>
                                <th style='background: #368EE0;' >Time</th>
                                <th style='background: #368EE0;' >User</th>
                                <th style='background: #368EE0;'>Account Name</th>
                                <th class="numeric" style='background: #368EE0;'>Inv#</th>
                                <th style='background: #368EE0;' class="text-right numeric">Debit</th>
                                <th style='background: #368EE0;' class="text-right numeric">Credit</th>
                              </tr>
                            </thead>
                            <tbody class="chequereceipt_tbody">
                              <?php $counter = 1; foreach ($chqreceiptss as $chqreceipt): ?>
                                <tr>
                                  <td><a href="<?php echo makeHref($chqreceipt['etype'],$chqreceipt['dcno']) ; ?>"><?php echo $chqreceipt['dcno'] .'-'. abbrivateEtypes($chqreceipt['etype']); ?></a></td>
                                  <td><?php echo $chqreceipt['date_time']; ?></td>
                                  <td><?php echo $chqreceipt['user_name']; ?></td>
                                  <td><?php echo $chqreceipt['party_name']; ?></td>
                                  <td><?php echo $chqreceipt['invoice']; ?></td>
                                  <td><?php echo $chqreceipt['debit']; ?></td>
                                  <td class="text-right"><?php echo $chqreceipt['credit']; ?></td>
                                </tr>
                              <?php endforeach ?>
                            </tbody>
                            <tfoot class="hidden">
                              <tr>
                                <td><span></span> : <span class="brv-sum-val"><?php //echo $tot; ?></span></td>
                              </tr>
                            </tfoot>
                          </table>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                              <div class="tabs_chqrecip hide">
                                <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                                  <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                                  <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                                  <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                                </ul>
                                <div class="tab-content">
                                  <div role="tabpanel" class="tab-pane active" id="home">...</div>
                                  <div role="tabpanel" class="tab-pane" id="profile">...</div>
                                  <div role="tabpanel" class="tab-pane" id="messages">...</div>
                                  <div role="tabpanel" class="tab-pane" id="settings">...</div>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div> 
             
                  <!--...................................JOURNAL VOUCHER TRANSCATION.......................-->

                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_20" style="background:#282828 !important; color:white;font-family:open sans;height: 40px;">
                          <div class="row">
                            <div class="col-lg-9">
                              <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-money" style="color:#CECE28;"></i></span> <span style="font-size:15px !important;"> Journal Voucher
                            </div> 
                            <div class="col-lg-3" style="font-size:12px !important;">
                              <span>Total:</span> : <span class="jv-sum"><?php echo round($totaljvs[0]['totalaccount']);?></span>
                            </div>
                          </div>
                        </a>
                      </h4>
                    </div>
                    <div id="accordion_a_20" class="panel-collapse collapse">
                      <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                              <div class="pull-right">
                                <button class="btn btn-default" id="btnTable20"><i class="fa fa-th-list"></i> List </button>
                                <button class="btn btn-default" id="btnGraph20"><i class="ion-pie-graph"></i> Chart</button>
                              </div>
                            </div>
                          </div><br>
                        <div id="no-more-tables">
                          <table class="table table-striped table-hover jv_tbl">
                            <thead class="cf">
                              <tr>
                                <th class="numeric" style='background: #368EE0;' >Vr#</th>
                                <th style='background: #368EE0;' >Time</th>
                                <th style='background: #368EE0;' >User</th>
                                <th style='background: #368EE0;'>Account Name</th>
                                <th class="numeric" style='background: #368EE0;'>Inv#</th>
                                <th style='background: #368EE0;' class="text-right numeric">Debit</th>
                                <th style='background: #368EE0;' class="text-right numeric">Credit</th>
                              </tr>
                            </thead>
                            <tbody class="jv_tbody">
                              <?php $counter = 1; foreach ($jvss as $jv): ?>
                                <tr>
                                  <td><a href="<?php echo makeHref($jv['etype'],$jv['dcno']) ; ?>"><?php echo $jv['dcno'] .'-'. abbrivateEtypes($jv['etype']); ?></a></td>
                                  <td><?php echo $jv['date_time']; ?></td>
                                  <td><?php echo $jv['user_name']; ?></td>
                                  <td><?php echo $jv['party_name']; ?></td>
                                  <td><?php echo $jv['invoice']; ?></td>
                                  <td><?php echo $jv['debit']; ?></td>
                                  <td class="text-right"><?php echo $jv['credit']; ?></td>
                                </tr>
                              <?php endforeach ?>
                            </tbody>
                            <tfoot class="hidden">
                              <tr>
                                <td><span></span> : <span class="brv-sum-val"><?php //echo $tot; ?></span></td>
                              </tr>
                            </tfoot>
                          </table>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                              <div class="tabs_jv hide">
                                <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                                  <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                                  <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                                  <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                                </ul>
                                <div class="tab-content">
                                  <div role="tabpanel" class="tab-pane active" id="home">...</div>
                                  <div role="tabpanel" class="tab-pane" id="profile">...</div>
                                  <div role="tabpanel" class="tab-pane" id="messages">...</div>
                                  <div role="tabpanel" class="tab-pane" id="settings">...</div>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div> 

                  <!--...................................OPENING BALANCE TRANSCATION.......................-->

                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_21" style="background:#282828 !important; color:white;font-family:open sans;height: 40px;">
                          <div class="row">
                            <div class="col-lg-9">
                              <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-money" style="color:#CECE28;"></i></span> <span style="font-size:15px !important;"> Opening Balance
                            </div> 
                            <div class="col-lg-3" style="font-size:12px !important;">
                              <span>Total:</span> : <span class="ob-sum"><?php echo round($totalopeningbalances[0]['totalaccount']);?></span>
                            </div>
                          </div>
                        </a>
                      </h4>
                    </div>
                    <div id="accordion_a_21" class="panel-collapse collapse">
                      <div class="panel-body">
                        <div class="row">
                          <div class="col-lg-12">
                            <div class="pull-right">
                              <button class="btn btn-default" id="btnTable21"><i class="fa fa-th-list"></i> List </button>
                              <button class="btn btn-default" id="btnGraph21"><i class="ion-pie-graph"></i> Chart</button>
                            </div>
                          </div>
                        </div><br>
                        <div id="no-more-tables">
                          <table class="table table-striped table-hover opening_tbl">
                            <thead class="cf">
                              <tr>
                                <th class="numeric" style='background: #368EE0;' >Vr#</th>
                                <th style='background: #368EE0;' >Time</th>
                                <th style='background: #368EE0;' >User</th>
                                <th style='background: #368EE0;'>Account Name</th>
                                <th class="numeric" style='background: #368EE0;'>Inv#</th>
                                <th style='background: #368EE0;' class="text-right numeric">Debit</th>
                                <th style='background: #368EE0;' class="text-right numeric">Credit</th>
                              </tr>
                            </thead>
                            <tbody class="opening_tbody">
                              <?php $counter = 1; foreach ($openingbalancess as $openingbalance): ?>
                                <tr>
                                  <td><a href="<?php echo makeHref($openingbalance['etype'],$openingbalance['dcno']) ; ?>"><?php echo $openingbalance['dcno'] .'-'. abbrivateEtypes($openingbalance['etype']); ?></a></td>
                                  <td><?php echo $openingbalance['date_time']; ?></td>
                                  <td><?php echo $openingbalance['user_name']; ?></td>
                                  <td><?php echo $openingbalance['party_name']; ?></td>
                                  <td><?php echo $openingbalance['invoice']; ?></td>
                                  <td><?php echo $openingbalance['debit']; ?></td>
                                  <td class="text-right"><?php echo $openingbalance['credit']; ?></td>
                                </tr>
                              <?php endforeach ?>
                            </tbody>
                            <tfoot class="hidden">
                              <tr>
                                <td><span></span> : <span class="brv-sum-val"><?php //echo $tot; ?></span></td>
                              </tr>
                            </tfoot>
                          </table>
                        </div>
                        <div class="row">
                          <div class="col-lg-12">
                            <div class="tabs_opbalan hide">
                              <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                                <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                                <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                              </ul>
                              <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">...</div>
                                <div role="tabpanel" class="tab-pane" id="profile">...</div>
                                <div role="tabpanel" class="tab-pane" id="messages">...</div>
                                <div role="tabpanel" class="tab-pane" id="settings">...</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
      <div class="row-fluid">
        <div class="col-lg-12">

        <!--.......................................PURCHASE VOUCHER TRANSCTION ..............................-->

          <h4 class="text-primary" style="font-family:open sans;color:black;"><i class="fa fa-shopping-cart"></i> <b>P</b>urchase</h4>
          <div class="panel-group" id="accordion_a">

          <!--..................................PURCHASE ORDER TRANSTION ............................-->

          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_1" style="background: #282828  !important; color:white;font-family:open sans;height: 40px;">
                  <div class="row">
                    <div class="col-lg-9">
                      <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-shopping-cart" style="color:#90906F;"></i></span> Purchase Order
                    </div>
                    <div class="col-lg-3" style="font-size:12px !important;">
                      <span>Total:</span> : <span class="purchases-sum-order"> 0</span>
                    </div>
                  </div>
                </a>
              </h4>
            </div>
            <div id="accordion_a_1" class="panel-collapse collapse">
              <div class="panel-body">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="pull-right">
                      <button class="btn btn-default" id="btnTable1"><i class="fa fa-th-list"></i> List </button>
                      <button class="btn btn-default" id="btnGraph1"><i class="ion-pie-graph"></i> Chart</button>
                    </div>
                  </div>
                </div><br>
                <div id="no-more-tables">
                  <table class="table table-striped table-hover purchaseorder_tbl">
                    <thead class="cf">
                      <tr>
                        <th style='background: #368EE0;max-width:20px !important;'>Vr#</th>
                        <th style='background: #368EE0;'>Time</th>
                        <th style='background: #368EE0;'>User</th>
                        <th style='background: #368EE0;'>Party</th>
                        <th style='background: #368EE0;' class="text-right">Discount</th>
                        <th style='background: #368EE0;' class="text-right">Tax</th>
                        <th style='background: #368EE0;' class="text-right">Net Amount</th>
                      </tr>
                    </thead>
                    <tbody class="purchaseorder_tbody">
                      <?php $counter = 1; foreach ($purchaseOrderss as $purchaseorder): ?>
                        <tr>
                        <td><a href="<?php echo makeHref($purchaseorder['etype'],$purchaseorder['vrnoa']) ; ?>"><?php echo $purchaseorder['vrnoa'] .'-'. abbrivateEtypes($purchaseorder['etype']); ?></a></td>
                          <td><?php echo $purchaseorder['date_time']; ?></td>
                          <td><?php echo $purchaseorder['user_name']; ?></td>
                          <td><?php echo $purchaseorder['party_name']; ?></td>
                          <td class="text-right"><?php echo $purchaseorder['discount']; ?></td>
                          <td class="text-right"><?php echo $purchaseorder['tax']; ?></td>
                          <td class="text-right"><?php echo $purchaseorder['namount']; ?></td>
                        </tr>
                      <?php endforeach ?>
                    </tbody>
                    <tfoot class="hidden">
                      <tr>
                        <td><span></span> : <span class="purchases-sum-val"><?php //echo $tot; ?></span></td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <div class="tabs_purchase hide">
                      <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                        <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                      </ul>
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">...</div>
                        <div role="tabpanel" class="tab-pane" id="profile">...</div>
                        <div role="tabpanel" class="tab-pane" id="messages">...</div>
                        <div role="tabpanel" class="tab-pane" id="settings">...</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!--..................................PURCHASE TRANSTION ............................-->

          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_2" style="background: #282828  !important; color:white;font-family:open sans;height: 40px;">
                  <div class="row">
                    <div class="col-lg-9">
                      <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-shopping-cart" style="color:#90906F;"></i></span> Purchase Voucher
                    </div>
                    <div class="col-lg-3" style="font-size:12px !important;">
                      <span>Total:</span> : <span class="purchases-sum-voucher"><?php echo round($totalpurchases[0]['totalpurchase']);?> </span>
                    </div>
                  </div>
                </a>
              </h4>
            </div>
            <div id="accordion_a_2" class="panel-collapse collapse">
              <div class="panel-body">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="pull-right">
                      <button class="btn btn-default" id="btnTable2"><i class="fa fa-th-list"></i> List </button>
                      <button class="btn btn-default" id="btnGraph2"><i class="ion-pie-graph"></i> Chart</button>
                    </div>
                  </div>
                </div><br>
                <div id="no-more-tables">
                  <table class="table table-striped table-hover purchasetrans_tbl">
                    <thead class="cf">
                      <tr>
                        <th style='background: #368EE0;max-width:20px !important;'>Vr#</th>
                        <th style='background: #368EE0;'>Time</th>
                        <th style='background: #368EE0;'>User</th>
                        <th style='background: #368EE0;'>Party</th>
                        <th style='background: #368EE0;' class="text-left">Discount</th>
                        <th style='background: #368EE0;' class="text-left">Tax</th>
                        <th style='background: #368EE0;' class="text-right">Net Amount</th>
                      </tr>
                    </thead>
                    <tbody class="purchase_tbody">
                      <?php $counter = 1; foreach ($purchases as $purchase): ?>
                        <tr>
                        <td><a href="<?php echo makeHref($purchase['etype'],$purchase['vrnoa']) ; ?>"><?php echo $purchase['vrnoa'] .'-'. abbrivateEtypes($purchase['etype']); ?></a></td>
                          <td><?php echo $purchase['date_time']; ?></td>
                          <td><?php echo $purchase['user_name']; ?></td>
                          <td><?php echo $purchase['party_name']; ?></td>
                          <td><?php echo $purchase['discount']; ?></td>
                          <td><?php echo $purchase['tax']; ?></td>
                          <td class="text-right"><?php echo $purchase['namount']; ?></td>
                        </tr>
                      <?php endforeach ?>
                    </tbody>
                    <tfoot class="hidden">
                      <tr>
                        <td><span></span> : <span class="purchases-sum-val"><?php //echo $tot; ?></span></td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <div class="tabs_purchaseret hide">
                      <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                        <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                      </ul>
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">...</div>
                        <div role="tabpanel" class="tab-pane" id="profile">...</div>
                        <div role="tabpanel" class="tab-pane" id="messages">...</div>
                        <div role="tabpanel" class="tab-pane" id="settings">...</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!--......................................PURCHASE RETURN TRANSCATIONS............................-->
          
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_3" style="background: #282828  !important; color:white;font-family:open sans;height: 40px;">
                  <div class="row">
                    <div class="col-lg-9">
                      <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-shopping-cart" style="color:#90906F;"></i></span> Purchase Return
                    </div>
                    <div class="col-lg-3" style="font-size:12px !important;">
                      <span>Total:</span> : <span class="purchases-sum-return"> <?php echo round($totalpurchasereturns[0]['totalpurchase']);?></span>
                    </div>
                  </div>
                </a>
              </h4>
            </div>
            <div id="accordion_a_3" class="panel-collapse collapse">
              <div class="panel-body">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="pull-right">
                      <button class="btn btn-default" id="btnTable3"><i class="fa fa-th-list"></i> List </button>
                      <button class="btn btn-default" id="btnGraph3"><i class="ion-pie-graph"></i> Chart</button>
                    </div>
                  </div>
                </div><br>
                <div id="no-more-tables">
                  <table class="table table-striped table-hover purchasereturntrans_tbl">
                    <thead class="cf">
                      <tr>
                        <th style='background: #368EE0;max-width:20px !important;'>Vr#</th>
                        <th style='background: #368EE0;'>Time</th>
                        <th style='background: #368EE0;'>User</th>
                        <th style='background: #368EE0;'>Party</th>
                        <th style='background: #368EE0;' class="text-left">Discount</th>
                        <th style='background: #368EE0;' class="text-left">Tax</th>
                        <th style='background: #368EE0;' class="text-right">Net Amount</th>
                      </tr>
                    </thead>
                    <tbody class="purchasereturn_tbody">
                      <?php $counter = 1; foreach ($purchasesreturn as $purchasereturn): ?>
                        <tr>
                          <td><a href="<?php echo makeHref($purchasereturn['etype'],$purchasereturn['vrnoa']) ; ?>"><?php echo $purchasereturn['vrnoa'] .'-'. abbrivateEtypes($purchasereturn['etype']); ?></a></td>
                          <td><?php echo $purchasereturn['date_time']; ?></td>
                          <td><?php echo $purchasereturn['user_name']; ?></td>
                          <td><?php echo $purchasereturn['party_name']; ?></td>
                          <td><?php echo $purchasereturn['discount']; ?></td>
                          <td><?php echo $purchasereturn['tax']; ?></td>
                          <td class="text-right"><?php echo $purchasereturn['namount']; ?></td>
                        </tr>
                      <?php endforeach ?>
                    </tbody>
                    <tfoot class="hidden">
                      <tr>
                        <td><span></span> : <span class="purchases-sum-val"><?php //echo $tot; ?></span></td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <div class="tabs_purchasereturn hide">
                      <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                        <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                      </ul>
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">...</div>
                        <div role="tabpanel" class="tab-pane" id="profile">...</div>
                        <div role="tabpanel" class="tab-pane" id="messages">...</div>
                        <div role="tabpanel" class="tab-pane" id="settings">...</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!--..........................REQUISITION VOUCHER TRANSCATION...............................-->

                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_10" style="background:#282828 !important; color:white;font-family:open sans;height: 40px;">
                            <div class="row">
                              <div class="col-lg-9">
                                <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-bar-chart-o" style="color:#90906F;"></i></span> Requisition Voucher
                              </div>
                              <div class="col-lg-3" style="font-size:12px !important;">
                                <span>Total:</span> : <span class="requisition-sum"> 0</span>
                              </div>
                            </div>
                          </a>
                        </h4>
                      </div>
                      <div id="accordion_a_10" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="pull-right">
                                <button class="btn btn-default" id="btnTable10"><i class="fa fa-th-list"></i> List </button>
                                <button class="btn btn-default" id="btnGraph10"><i class="ion-pie-graph"></i> Chart</button>
                              </div>
                            </div>
                          </div><br>
                          <div id="no-more-tables">
                            <table class="table table-striped table-hover requisition_tbl">
                              <thead class="cf">
                                <tr>
                                  <th style='background: #368EE0;max-width:20px !important;'>Vr#</th>
                                  <th style='background: #368EE0;'>Time</th>
                                  <th style='background: #368EE0;'>User</th>
                                  <th style='background: #368EE0;'>Party</th>
                                  <th style='background: #368EE0;' class="text-left">Discount</th>
                                  <th style='background: #368EE0;' class="text-left">Tax</th>
                                  <th style='background: #368EE0;' class="text-right">Net Amount</th>
                                </tr>
                              </thead>
                              <tbody class="requisition_tbody">
                                <?php $counter = 1; foreach ($requisitionss as $requisition): ?>
                                  <tr>
                                    <td><a href="<?php echo makeHref($requisition['etype'],$requisition['vrnoa']) ; ?>"><?php echo $requisition['vrnoa'] .'-'. abbrivateEtypes($requisition['etype']); ?></a></td>
                                    <td><?php echo $requisition['date_time']; ?></td>
                                    <td><?php echo $requisition['user_name']; ?></td>
                                    <td><?php echo $requisition['party_name']; ?></td>
                                    <td><?php echo $requisition['discount']; ?></td>
                                    <td><?php echo $requisition['tax']; ?></td>
                                    <td class="text-right"><?php echo $requisition['namount']; ?></td>
                                  </tr>
                                <?php endforeach ?>
                              </tbody>
                              <tfoot class="hidden">
                                <tr>
                                  <td><span></span> : <span class="sales-sum-val"><?php //echo $tot; ?></span></td>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="tabs_requsition hide">
                                <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                                  <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                                  <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                                  <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                                </ul>
                                <div class="tab-content">
                                  <div role="tabpanel" class="tab-pane active" id="home">...</div>
                                  <div role="tabpanel" class="tab-pane" id="profile">...</div>
                                  <div role="tabpanel" class="tab-pane" id="messages">...</div>
                                  <div role="tabpanel" class="tab-pane" id="settings">...</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
          
        </div>
      </div>

      <div class="col-lg-12">
        <h4 class="text-primary" style="font-family:open sans;color:black;"><i class="fa fa-shopping-cart"></i> <b>S</b>ale</h4>
          <div class="panel-group" id="accordion_a">

          <!--................................QUOTATION VOUCHER TRANSCATION................................-->


           <!--  <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_5" style="background:#282828 !important; color:white;font-family:open sans;height: 40px;">
                    <div class="row">
                      <div class="col-lg-9">
                        <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-shopping-cart" style="color:#90906F;"></i></span> Quotation Voucher
                      </div>
                      <div class="col-lg-3" style="font-size:12px !important;">
                        <span>Total:</span> : <span class="quotationorder-sum"> <?php echo round($totalquotationorders[0]['totalsale']);?></span>
                      </div>
                    </div>
                  </a>
                </h4>
              </div>
              <div id="accordion_a_5" class="panel-collapse collapse">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="pull-right">
                        <button class="btn btn-default" id="btnTable5"><i class="fa fa-th-list"></i> List </button>
                        <button class="btn btn-default" id="btnGraph5"><i class="ion-pie-graph"></i> Chart</button>
                      </div>
                    </div>
                  </div><br>
                  <div id="no-more-tables">
                    <table class="table table-striped table-hover quotationtrans_tbl">
                      <thead class="cf">
                        <tr>
                          <th style='background: #368EE0;max-width:20px !important;'>Vr#</th>
                          <th style='background: #368EE0;'>Time</th>
                          <th style='background: #368EE0;'>User</th>
                          <th style='background: #368EE0;'>Party</th>
                          <th style='background: #368EE0;' class="text-left">Discount</th>
                          <th style='background: #368EE0;' class="text-left">Tax</th>
                          <th style='background: #368EE0;' class="text-right">Net Amount</th>
                        </tr>
                      </thead>
                      <tbody class="quotation_tbody">
                        <?php $counter = 1; foreach ($quotationss as $quotation): ?>
                          <tr>
                            <td><a href="<?php echo makeHref($quotation['etype'],$quotation['vrnoa']) ; ?>"><?php echo $quotation['vrnoa'] .'-'. abbrivateEtypes($quotation['etype']); ?></a></td>
                            <td><?php echo $quotation['date_time']; ?></td>
                            <td><?php echo $quotation['user_name']; ?></td>
                            <td><?php echo $quotation['party_name']; ?></td>
                            <td><?php echo $quotation['discount']; ?></td>
                            <td><?php echo $quotation['tax']; ?></td>
                            <td class="text-right"><?php echo $quotation['namount']; ?></td>
                          </tr>
                        <?php endforeach ?>
                        </tbody>
                        <tfoot class="hidden">
                          <tr>
                            <td><span></span> : <span class="fabricpurchases-sum-val"><?php //echo $tot; ?></span></td>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                    <div class="row">
                      <div class="col-lg-12">
                        <div class="tabs_quotation hide">
                          <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                            <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                          </ul>
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">...</div>
                            <div role="tabpanel" class="tab-pane" id="profile">...</div>
                            <div role="tabpanel" class="tab-pane" id="messages">...</div>
                            <div role="tabpanel" class="tab-pane" id="settings">...</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div> -->

              <!--................................SALE ORDER VOUCHER TRANSCATION................................-->


              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_6" style="background:#282828  !important; color:white;font-family:open sans;height: 40px;">
                      <div class="row">
                        <div class="col-lg-9">
                          <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-shopping-cart" style="color:#90906F;"></i></span> Sale Order
                        </div>
                        <div class="col-lg-3" style="font-size:12px !important;">
                          <span>Total:</span> : <span class="saleorder-sum"> <?php echo round($totalsaleorders[0]['totalsale']);?></span>
                        </div>
                      </div>
                    </a>
                  </h4>
                </div>
                <div id="accordion_a_6" class="panel-collapse collapse">
                  <div class="panel-body">
                    <div class="row">
                      <div class="col-lg-12">
                        <div class="pull-right">
                          <button class="btn btn-default" id="btnTable6"><i class="fa fa-th-list"></i> List </button>
                          <button class="btn btn-default" id="btnGraph6"><i class="ion-pie-graph"></i> Chart</button>
                        </div>
                      </div>
                    </div><br>
                    <div id="no-more-tables">
                      <table class="table table-striped table-hover saleordertrans_tbl">
                        <thead class="cf">
                          <tr>
                            <th style='background: #368EE0;max-width:20px !important;'>Vr#</th>
                            <th style='background: #368EE0;'>Time</th>
                            <th style='background: #368EE0;'>User</th>
                            <th style='background: #368EE0;'>Party</th>
                            <th style='background: #368EE0;' class="text-left">Discount</th>
                            <th style='background: #368EE0;' class="text-left">Tax</th>
                            <th style='background: #368EE0;' class="text-right">Net Amount</th>
                          </tr>
                        </thead>
                        <tbody class="saleorder_tbody">
                          <?php $counter = 1; foreach ($saleorderss as $saleorder): ?>
                            <tr>
                              <td><a href="<?php echo makeHref($saleorder['etype'],$saleorder['vrnoa']) ; ?>"><?php echo $saleorder['vrnoa'] .'-'. abbrivateEtypes($saleorder['etype']); ?></a></td>
                              <td><?php echo $saleorder['date_time']; ?></td>
                              <td><?php echo $saleorder['user_name']; ?></td>
                              <td><?php echo $saleorder['party_name']; ?></td>
                              <td><?php echo $saleorder['discount']; ?></td>
                              <td><?php echo $saleorder['tax']; ?></td>
                              <td class="text-right"><?php echo $saleorder['namount']; ?></td>
                            </tr>
                          <?php endforeach ?>
                          </tbody>
                          <tfoot class="hidden">
                            <tr>
                              <td><span></span> : <span class="fabricpurchases-sum-val"><?php //echo $tot; ?></span></td>
                            </tr>
                          </tfoot>
                        </table>
                      </div>
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="tabs_saleorder hide">
                            <ul class="nav nav-tabs" role="tablist">
                              <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                              <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                              <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                              <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                            </ul>
                            <div class="tab-content">
                              <div role="tabpanel" class="tab-pane active" id="home">...</div>
                              <div role="tabpanel" class="tab-pane" id="profile">...</div>
                              <div role="tabpanel" class="tab-pane" id="messages">...</div>
                              <div role="tabpanel" class="tab-pane" id="settings">...</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <!--................................OGP VOUCHER TRANSCATION................................-->

                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_7" style="background:#282828  !important; color:white;font-family:open sans;height: 40px;">
                        <div class="row">
                          <div class="col-lg-9">
                            <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-shopping-cart" style="color:#90906F;"></i></span> OGP Gate Pass
                          </div>
                          <div class="col-lg-3" style="font-size:12px !important;">
                            <span>Total:</span> : <span class="totalogp-sum"> 0</span>
                          </div>
                        </div>
                      </a>
                    </h4>
                  </div>
                  <div id="accordion_a_7" class="panel-collapse collapse">
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="pull-right">
                            <button class="btn btn-default" id="btnTable7"><i class="fa fa-th-list"></i> List </button>
                            <button class="btn btn-default" id="btnGraph7"><i class="ion-pie-graph"></i> Chart</button>
                          </div>
                        </div>
                      </div><br>
                      <div id="no-more-tables">
                        <table class="table table-striped table-hover ogptrans_tbl">
                          <thead class="cf">
                            <tr>
                              <th style='background: #368EE0;max-width:20px !important;'>Vr#</th>
                              <th style='background: #368EE0;'>Time</th>
                              <th style='background: #368EE0;'>User</th>
                              <th style='background: #368EE0;'>Party</th>
                              <th style='background: #368EE0;' class="text-left">Discount</th>
                              <th style='background: #368EE0;' class="text-left">Tax</th>
                              <th style='background: #368EE0;' class="text-right">Net Amount</th>
                            </tr>
                          </thead>
                          <tbody class="ogp_tbody">
                            <?php $counter = 1; foreach ($gatepasses as $gatepass): ?>
                              <tr>
                                <td><a href="<?php echo makeHref($gatepass['etype'],$gatepass['vrnoa']) ; ?>"><?php echo $gatepass['vrnoa'] .'-'. abbrivateEtypes($gatepass['etype']); ?></a></td>
                                <td><?php echo $gatepass['date_time']; ?></td>
                                <td><?php echo $gatepass['user_name']; ?></td>
                                <td><?php echo $gatepass['party_name']; ?></td>
                                <td><?php echo $gatepass['discount']; ?></td>
                                <td><?php echo $gatepass['tax']; ?></td>
                                <td class="text-right"><?php echo $gatepass['namount']; ?></td>
                              </tr>
                            <?php endforeach ?>
                            </tbody>
                            <tfoot class="hidden">
                              <tr>
                                <td><span></span> : <span class="fabricpurchases-sum-val"><?php //echo $tot; ?></span></td>
                              </tr>
                            </tfoot>
                          </table>
                        </div>
                        <div class="row">
                          <div class="col-lg-12">
                            <div class="tabs_ogp hide">
                              <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                                <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                                <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                              </ul>
                              <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">...</div>
                                <div role="tabpanel" class="tab-pane" id="profile">...</div>
                                <div role="tabpanel" class="tab-pane" id="messages">...</div>
                                <div role="tabpanel" class="tab-pane" id="settings">...</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
            
                  <!--.............................SALE VOUCHER TRANSCATION..........................-->

                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_8" style="background:#282828  !important; color:white;font-family:open sans;height: 40px;">
                          <div class="row">
                            <div class="col-lg-9">
                              <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-shopping-cart" style="color:#90906F;"></i></span> Sale Invoice
                            </div>
                            <div class="col-lg-3" style="font-size:12px !important;">
                              <span>Total:</span> : <span class="totalsale-sum"> <?php echo round($totalsales[0]['totalsale']);?></span>
                            </div>
                          </div>
                        </a>
                      </h4>
                    </div>
                    <div id="accordion_a_8" class="panel-collapse collapse">
                      <div class="panel-body">
                        <div class="row">
                          <div class="col-lg-12">
                            <div class="pull-right">
                              <button class="btn btn-default" id="btnTable8"><i class="fa fa-th-list"></i> List </button>
                              <button class="btn btn-default" id="btnGraph8"><i class="ion-pie-graph"></i> Chart</button>
                            </div>
                          </div>
                        </div><br>
                        <div id="no-more-tables">
                          <table class="table table-striped table-hover saletrans_tbl">
                            <thead class="cf">
                              <tr>
                                <th class="numeric" style='background: #368EE0;max-width:20px !important;'>Vr#</th>
                                <th style='background: #368EE0;'>Time</th>
                                <th style='background: #368EE0;'>User</th>
                                <th style='background: #368EE0;'>Party</th>
                                <th class="numeric" style='background: #368EE0;' class="text-left">Discount</th>
                                <th class="numeric" style='background: #368EE0;' class="text-left">Tax</th>
                                <th class="numeric" style='background: #368EE0;' class="text-right">Net Amount</th>
                              </tr>
                            </thead>
                            <tbody class="sale_tbody">
                              <?php $counter = 1; foreach ($sales as $sale): ?>
                                <tr>
                                  <td><a href="<?php echo makeHref($sale['etype'],$sale['vrnoa']) ; ?>"><?php echo $sale['vrnoa'] .'-'. abbrivateEtypes($sale['etype']); ?></a></td>
                                  <td><?php echo $sale['date_time']; ?></td>
                                  <td><?php echo $sale['user_name']; ?></td>
                                  <td><?php echo $sale['party_name']; ?></td>
                                  <td><?php echo $sale['namount']; ?></td>
                                  <td><?php echo $sale['tax']; ?></td>
                                  <td class="text-right"><?php echo $sale['namount']; ?></td>
                                </tr>
                              <?php endforeach ?>
                            </tbody>
                            <tfoot class="hidden">
                              <tr>
                                <td><span>Total Yarn Purchase</span> : <span class="yarnpurchases-sum-val"><?php //echo $tot; ?></span></td>
                              </tr>
                            </tfoot>
                          </table>
                        </div>
                        <div class="row">
                          <div class="col-lg-12">
                            <div class="tabs_sales hide">
                              <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                                <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                                <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                              </ul>
                              <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">...</div>
                                <div role="tabpanel" class="tab-pane" id="profile">...</div>
                                <div role="tabpanel" class="tab-pane" id="messages">...</div>
                                <div role="tabpanel" class="tab-pane" id="settings">...</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!--................................SALE RETURN VOUCHER TRANSCATION...................-->


                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_9" style="background:#282828 !important; color:white;font-family:open sans;height: 40px;">
                          <div class="row">
                            <div class="col-lg-9">
                              <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-shopping-cart" style="color:#90906F;"></i></span> Sale Return
                            </div>
                            <div class="col-lg-3" style="font-size:12px !important;">
                              <span>Total:</span> : <span class="salereturn-sum"> <?php echo round($totalsalereturns[0]['totalsale']);?></span>
                            </div>
                          </div>
                        </a>
                      </h4>
                    </div>
                    <div id="accordion_a_9" class="panel-collapse collapse">
                      <div class="panel-body">
                        <div class="row">
                          <div class="col-lg-12">
                            <div class="pull-right">
                              <button class="btn btn-default" id="btnTable9"><i class="fa fa-th-list"></i> List </button>
                              <button class="btn btn-default" id="btnGraph9"><i class="ion-pie-graph"></i> Chart</button>
                            </div>
                          </div>
                        </div><br>
                        <div id="no-more-tables">
                          <table class="table table-striped table-hover salereturn_tbl">
                            <thead class="cf">
                              <tr>
                                <th style='background: #368EE0;max-width:20px !important;'>Vr#</th>
                                <th style='background: #368EE0;'>Time</th>
                                <th style='background: #368EE0;'>User</th>
                                <th style='background: #368EE0;'>Party</th>
                                <th style='background: #368EE0;' class="text-left">Discount</th>
                                <th style='background: #368EE0;' class="text-left">Tax</th>
                                <th style='background: #368EE0;' class="text-right">Net Amount</th>
                              </tr>
                            </thead>
                            <tbody class="salereturn_tbody">
                              <?php $counter = 1; foreach ($salereturns as $salereturn): ?>
                                <tr>
                                  <td><a href="<?php echo makeHref($salereturn['etype'],$salereturn['vrnoa']) ; ?>"><?php echo $salereturn['vrnoa'] .'-'. abbrivateEtypes($salereturn['etype']); ?></a></td>
                                  <td><?php echo $salereturn['date_time']; ?></td>
                                  <td><?php echo $salereturn['user_name']; ?></td>
                                  <td><?php echo $salereturn['party_name']; ?></td>
                                  <td><?php echo $salereturn['discount']; ?></td>
                                  <td><?php echo $salereturn['tax']; ?></td>
                                  <td class="text-right"><?php echo $salereturn['namount']; ?></td>
                                </tr>
                              <?php endforeach ?>
                              </tbody>
                              <tfoot class="hidden">
                                <tr>
                                  <td><span></span> : <span class="fabricpurchases-sum-val"><?php //echo $tot; ?></span></td>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="tabs_salesreturn hide">
                                <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                                  <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                                  <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                                  <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                                </ul>
                                <div class="tab-content">
                                  <div role="tabpanel" class="tab-pane active" id="home">...</div>
                                  <div role="tabpanel" class="tab-pane" id="profile">...</div>
                                  <div role="tabpanel" class="tab-pane" id="messages">...</div>
                                  <div role="tabpanel" class="tab-pane" id="settings">...</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md-12">
                  <h4 class="text-primary" style="font-family:open sans;color:black;"><i class="fa fa-bar-chart-o"></i> <b>I</b>nventory</h4>
                  <div class="panel-group" id="accordion_a">
  
              

                    <!--..........................IGP INWARD VOUCHER TRANSCATION...............................-->

                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_11" style="background:#282828  !important; color:white;font-family:open sans;height: 40px;">
                            <div class="row">
                              <div class="col-lg-9">
                                <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-bar-chart-o" style="color:#90906F;"></i></span> IGP Inward Gatepass
                              </div>
                              <div class="col-lg-3" style="font-size:12px !important;">
                                <span>Total:</span> : <span class="inward-sum"> 0</span>
                              </div>
                            </div>
                          </a>
                        </h4>
                      </div>
                      <div id="accordion_a_11" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="pull-right">
                                <button class="btn btn-default" id="btnTable11"><i class="fa fa-th-list"></i> List </button>
                                <button class="btn btn-default" id="btnGraph11"><i class="ion-pie-graph"></i> Chart</button>
                              </div>
                            </div>
                          </div><br>
                          <div id="no-more-tables">
                            <table class="table table-striped table-hover igp_tbl">
                              <thead class="cf">
                                <tr>
                                  <th style='background: #368EE0;max-width:20px !important;'>Vr#</th>
                                  <th style='background: #368EE0;'>Time</th>
                                  <th style='background: #368EE0;'>User</th>
                                  <th style='background: #368EE0;'>Party</th>
                                  <th style='background: #368EE0;' class="text-left">Discount</th>
                                  <th style='background: #368EE0;' class="text-left">Tax</th>
                                  <th style='background: #368EE0;' class="text-right">Net Amount</th>
                                </tr>
                              </thead>
                              <tbody class="igp_tbody">
                                <?php $counter = 1; foreach ($inwardss as $inward): ?>
                                  <tr>
                                    <td><a href="<?php echo makeHref($inward['etype'],$inward['vrnoa']) ; ?>"><?php echo $inward['vrnoa'] .'-'. abbrivateEtypes($inward['etype']); ?></a></td>
                                    <td><?php echo $inward['date_time']; ?></td>
                                    <td><?php echo $inward['user_name']; ?></td>
                                    <td><?php echo $inward['party_name']; ?></td>
                                    <td><?php echo $inward['discount']; ?></td>
                                    <td><?php echo $inward['tax']; ?></td>
                                    <td class="text-right"><?php echo $inward['namount']; ?></td>
                                  </tr>
                                <?php endforeach ?>
                              </tbody>
                              <tfoot class="hidden">
                                <tr>
                                  <td><span></span> : <span class="sales-sum-val"><?php //echo $tot; ?></span></td>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="tabs_inward hide">
                                <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                                  <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                                  <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                                  <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                                </ul>
                                <div class="tab-content">
                                  <div role="tabpanel" class="tab-pane active" id="home">...</div>
                                  <div role="tabpanel" class="tab-pane" id="profile">...</div>
                                  <div role="tabpanel" class="tab-pane" id="messages">...</div>
                                  <div role="tabpanel" class="tab-pane" id="settings">...</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <!--..........................CONSUMPTION VOUCHER TRANSCATION...............................-->

                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_12" style="background:#282828 !important; color:white;font-family:open sans;height: 40px;">
                            <div class="row">
                              <div class="col-lg-9">
                                <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-bar-chart-o" style="color:#90906F;"></i></span> Issuance Consumption Voucher
                              </div>
                              <div class="col-lg-3" style="font-size:12px !important;">
                                <span>Total:</span> : <span class="consumption-sum"> 0</span>
                              </div>
                            </div>
                          </a>
                        </h4>
                      </div>
                      <div id="accordion_a_12" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="pull-right">
                                <button class="btn btn-default" id="btnTable12"><i class="fa fa-th-list"></i> List </button>
                                <button class="btn btn-default" id="btnGraph12"><i class="ion-pie-graph"></i> Chart</button>
                              </div>
                            </div>
                          </div><br>
                          <div id="no-more-tables">
                            <table class="table table-striped table-hover consumption_tbl">
                              <thead class="cf">
                                <tr>
                                  <th style='background: #368EE0;max-width:20px !important;'>Vr#</th>
                                  <th style='background: #368EE0;'>Time</th>
                                  <th style='background: #368EE0;'>User</th>
                                  <th style='background: #368EE0;'>Party</th>
                                  <th style='background: #368EE0;' class="text-left">Discount</th>
                                  <th style='background: #368EE0;' class="text-left">Tax</th>
                                  <th style='background: #368EE0;' class="text-right">Net Amount</th>
                                </tr>
                              </thead>
                              <tbody class="consumption_tbody">
                                <?php $counter = 1; foreach ($consumptionss as $consumption): ?>
                                  <tr>
                                    <td><a href="<?php echo makeHref($consumption['etype'],$consumption['vrnoa']) ; ?>"><?php echo $consumption['vrnoa'] .'-'. abbrivateEtypes($consumption['etype']); ?></a></td>
                                    <td><?php echo $consumption['date_time']; ?></td>
                                    <td><?php echo $consumption['user_name']; ?></td>
                                    <td><?php echo $consumption['party_name']; ?></td>
                                    <td><?php echo $consumption['discount']; ?></td>
                                    <td><?php echo $consumption['tax']; ?></td>
                                    <td class="text-right"><?php echo $consumption['namount']; ?></td>
                                  </tr>
                                <?php endforeach ?>
                              </tbody>
                              <tfoot class="hidden">
                                <tr>
                                  <td><span></span> : <span class="sales-sum-val"><?php //echo $tot; ?></span></td>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="tabs_consumption hide">
                                <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                                  <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                                  <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                                  <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                                </ul>
                                <div class="tab-content">
                                  <div role="tabpanel" class="tab-pane active" id="home">...</div>
                                  <div role="tabpanel" class="tab-pane" id="profile">...</div>
                                  <div role="tabpanel" class="tab-pane" id="messages">...</div>
                                  <div role="tabpanel" class="tab-pane" id="settings">...</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <!--..........................MATERIAL RETURN VOUCHER TRANSCATION.......................-->

                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_13" style="background:#282828 !important; color:white;font-family:open sans;height: 40px;">
                            <div class="row">
                              <div class="col-lg-9">
                                <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-bar-chart-o" style="color:#90906F;"></i></span> Material Return Voucher
                              </div>
                              <div class="col-lg-3" style="font-size:12px !important;">
                                <span>Total:</span> : <span class="materialreturn-sum"> 0</span>
                              </div>
                            </div>
                          </a>
                        </h4>
                      </div>
                      <div id="accordion_a_13" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="pull-right">
                                <button class="btn btn-default" id="btnTable13"><i class="fa fa-th-list"></i> List </button>
                                <button class="btn btn-default" id="btnGraph13"><i class="ion-pie-graph"></i> Chart</button>
                              </div>
                            </div>
                          </div><br>
                          <div id="no-more-tables">
                            <table class="table table-striped table-hover materialreturn_tbl">
                              <thead class="cf">
                                <tr>
                                  <th style='background: #368EE0;max-width:20px !important;'>Vr#</th>
                                  <th style='background: #368EE0;'>Time</th>
                                  <th style='background: #368EE0;'>User</th>
                                  <th style='background: #368EE0;'>Party</th>
                                  <th style='background: #368EE0;' class="text-left">Discount</th>
                                  <th style='background: #368EE0;' class="text-left">Tax</th>
                                  <th style='background: #368EE0;' class="text-right">Net Amount</th>
                                </tr>
                              </thead>
                              <tbody class="materialreturn_tbody">
                                <?php $counter = 1; foreach ($materialreturnss as $materialreturn): ?>
                                  <tr>
                                    <td><a href="<?php echo makeHref($materialreturn['etype'],$materialreturn['vrnoa']) ; ?>"><?php echo $materialreturn['vrnoa'] .'-'. abbrivateEtypes($materialreturn['etype']); ?></a></td>
                                    <td><?php echo $materialreturn['date_time']; ?></td>
                                    <td><?php echo $materialreturn['user_name']; ?></td>
                                    <td><?php echo $materialreturn['party_name']; ?></td>
                                    <td><?php echo $materialreturn['discount']; ?></td>
                                    <td><?php echo $materialreturn['tax']; ?></td>
                                    <td class="text-right"><?php echo $materialreturn['namount']; ?></td>
                                  </tr>
                                <?php endforeach ?>
                              </tbody>
                              <tfoot class="hidden">
                                <tr>
                                  <td><span></span> : <span class="sales-sum-val"><?php //echo $tot; ?></span></td>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="tabs_material hide">
                                <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                                  <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                                  <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                                  <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                                </ul>
                                <div class="tab-content">
                                  <div role="tabpanel" class="tab-pane active" id="home">...</div>
                                  <div role="tabpanel" class="tab-pane" id="profile">...</div>
                                  <div role="tabpanel" class="tab-pane" id="messages">...</div>
                                  <div role="tabpanel" class="tab-pane" id="settings">...</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
              
                    <!--..........................ITEM CONVERSION VOUCHER TRANSCATION.......................-->

                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">

                          <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_14" style="background:#282828 !important; color:white;font-family:open sans;height: 40px;">


                            <div class="row">
                              <div class="col-lg-9">
                                <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-bar-chart-o" style="color:#90906F;"></i></span> Item Conversion
                              </div>
                              <div class="col-lg-3" style="font-size:12px !important;">
                                <span>Total:</span> : <span class="itemconversion-sum"> 0</span>
                              </div>
                            </div>
                          </a>
                        </h4>
                      </div>

                      <div id="accordion_a_14" class="panel-collapse collapse">


                        <div class="panel-body">
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="pull-right">
                                <button class="btn btn-default" id="btnTable14"><i class="fa fa-th-list"></i> List </button>
                                <button class="btn btn-default" id="btnGraph14"><i class="ion-pie-graph"></i> Chart</button>
                              </div>
                            </div>
                          </div><br>
                          <div id="no-more-tables">
                            <table class="table table-striped table-hover itemconversion_tbl">
                              <thead class="cf">
                                <tr>
                                  <th style='background: #368EE0;max-width:20px !important;'>Vr#</th>
                                  <th style='background: #368EE0;'>Time</th>
                                  <th style='background: #368EE0;'>User</th>
                                  <th style='background: #368EE0;'>Party</th>
                                  <th style='background: #368EE0;' class="text-left">Discount</th>
                                  <th style='background: #368EE0;' class="text-left">Tax</th>
                                  <th style='background: #368EE0;' class="text-right">Net Amount</th>
                                </tr>
                              </thead>
                              <tbody class="itemconversion_tbody">
                                <?php $counter = 1; foreach ($itemconversionss as $itemconversion): ?>
                                  <tr>
                                    <td><a href="<?php echo makeHref($itemconversion['etype'],$itemconversion['vrnoa']) ; ?>"><?php echo $itemconversion['vrnoa'] .'-'. abbrivateEtypes($itemconversion['etype']); ?></a></td>
                                    <td><?php echo $itemconversion['date_time']; ?></td>
                                    <td><?php echo $itemconversion['user_name']; ?></td>
                                    <td><?php echo $itemconversion['party_name']; ?></td>
                                    <td><?php echo $itemconversion['discount']; ?></td>
                                    <td><?php echo $itemconversion['tax']; ?></td>
                                    <td class="text-right"><?php echo $itemconversion['namount']; ?></td>
                                  </tr>
                                <?php endforeach ?>
                              </tbody>
                              <tfoot class="hidden">
                                <tr>
                                  <td><span></span> : <span class="sales-sum-val"><?php //echo $tot; ?></span></td>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="tabs_itemcon hide">
                                <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                                  <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                                  <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                                  <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                                </ul>
                                <div class="tab-content">
                                  <div role="tabpanel" class="tab-pane active" id="home">...</div>
                                  <div role="tabpanel" class="tab-pane" id="profile">...</div>
                                  <div role="tabpanel" class="tab-pane" id="messages">...</div>
                                  <div role="tabpanel" class="tab-pane" id="settings">...</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <!--..................................OPENING STOCK TRANSTION ............................-->

          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_4" style="background: #282828  !important; color:white;font-family:open sans;height: 40px;">
                  <div class="row">
                    <div class="col-lg-9">
                      <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-shopping-cart" style="color:#90906F;"></i></span> Opening Stock
                    </div>
                    <div class="col-lg-3" style="font-size:12px !important;">
                      <span>Total:</span> : <span class="openingstock-sum"> <?php echo round($totalopeningstocks[0]['totalpurchase']);?></span>
                    </div>
                  </div>
                </a>
              </h4>
            </div>
            <div id="accordion_a_4" class="panel-collapse collapse">
              <div class="panel-body">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="pull-right">
                      <button class="btn btn-default" id="btnTable4"><i class="fa fa-th-list"></i> List </button>
                      <button class="btn btn-default" id="btnGraph4"><i class="ion-pie-graph"></i> Chart</button>
                    </div>
                  </div>
                </div><br>
                <div id="no-more-tables">
                  <table class="table table-striped table-hover openingstocktrans_tbl">
                    <thead class="cf">
                      <tr>
                        <th style='background: #368EE0;max-width:20px !important;'>Vr#</th>
                        <th style='background: #368EE0;'>Time</th>
                        <th style='background: #368EE0;'>User</th>
                        <th style='background: #368EE0;'>Party</th>
                        <th style='background: #368EE0;' class="text-left">Discount</th>
                        <th style='background: #368EE0;' class="text-left">Tax</th>
                        <th style='background: #368EE0;' class="text-right">Net Amount</th>
                      </tr>
                    </thead>
                    <tbody class="openingstock_tbody">
                      <?php $counter = 1; foreach ($openingstocks as $openingstock): ?>
                        <tr>
                        <td><a href="<?php echo makeHref($openingstock['etype'],$openingstock['vrnoa']) ; ?>"><?php echo $openingstock['vrnoa'] .'-'. abbrivateEtypes($openingstock['etype']); ?></a></td>
                          <td><?php echo $openingstock['date_time']; ?></td>
                          <td><?php echo $openingstock['user_name']; ?></td>
                          <td><?php echo $openingstock['party_name']; ?></td>
                          <td><?php echo $openingstock['discount']; ?></td>
                          <td><?php echo $openingstock['tax']; ?></td>
                          <td class="text-right"><?php echo $openingstock['namount']; ?></td>
                        </tr>
                      <?php endforeach ?>
                    </tbody>
                    <tfoot class="hidden">
                      <tr>
                        <td><span></span> : <span class="purchases-sum-val"><?php //echo $tot; ?></span></td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <div class="tabs_openeingstock hide">
                      <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                        <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                      </ul>
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">...</div>
                        <div role="tabpanel" class="tab-pane" id="profile">...</div>
                        <div role="tabpanel" class="tab-pane" id="messages">...</div>
                        <div role="tabpanel" class="tab-pane" id="settings">...</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!--..........................PRODUCTION VOUCHER TRANSCATION...............................-->

                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_15" style="background:#282828  !important; color:white;font-family:open sans;height: 40px;">
                            <div class="row">
                              <div class="col-lg-9">
                                <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-cogs" style="color:#90906F;"></i></span> Production
                              </div>
                              <div class="col-lg-3" style="font-size:12px !important;">
                                <span>Total:</span> : <span class="production-sum"> 0</span>
                              </div>
                            </div>
                          </a>
                        </h4>
                      </div>
                      <div id="accordion_a_15" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="pull-right">
                                <button class="btn btn-default" id="btnTable15"><i class="fa fa-th-list"></i> List </button>
                                <button class="btn btn-default" id="btnGraph15"><i class="ion-pie-graph"></i> Chart</button>
                              </div>
                            </div>
                          </div><br>
                          <div id="no-more-tables">
                            <table class="table table-striped table-hover production_tbl">
                              <thead class="cf">
                                <tr>
                                  <th style='background: #368EE0;max-width:20px !important;'>Vr#</th>
                                  <th style='background: #368EE0;'>Time</th>
                                  <th style='background: #368EE0;'>User</th>
                                  <th style='background: #368EE0;'>Party</th>
                                  <th style='background: #368EE0;' class="text-left">Discount</th>
                                  <th style='background: #368EE0;' class="text-left">Tax</th>
                                  <th style='background: #368EE0;' class="text-right">Net Amount</th>
                                </tr>
                              </thead>
                              <tbody class="production_tbody">
                                <?php $counter = 1; foreach ($productions as $production): ?>
                                  <tr>
                                    <td><a href="<?php echo makeHref($production['etype'],$production['vrnoa']) ; ?>"><?php echo $production['vrnoa'] .'-'. abbrivateEtypes($production['etype']); ?></a></td>
                                    <td><?php echo $production['date_time']; ?></td>
                                    <td><?php echo $production['user_name']; ?></td>
                                    <td><?php echo $production['party_name']; ?></td>
                                    <td><?php echo $production['discount']; ?></td>
                                    <td><?php echo $production['tax']; ?></td>
                                    <td class="text-right"><?php echo $production['namount']; ?></td>
                                  </tr>
                                <?php endforeach ?>
                              </tbody>
                              <tfoot class="hidden">
                                <tr>
                                  <td><span></span> : <span class="sales-sum-val"><?php //echo $tot; ?></span></td>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="tabs_product hide">
                                <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                                  <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                                  <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                                  <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                                </ul>
                                <div class="tab-content">
                                  <div role="tabpanel" class="tab-pane active" id="home">...</div>
                                  <div role="tabpanel" class="tab-pane" id="profile">...</div>
                                  <div role="tabpanel" class="tab-pane" id="messages">...</div>
                                  <div role="tabpanel" class="tab-pane" id="settings">...</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>


                <!-- <div class="col-md-12">
                  <h4 class="text-primary" style="font-family:open sans;color:black;"><i class="fa fa-cogs"></i> <b>P</b>roduction</h4>
                  <div class="panel-group" id="accordion_a">
        
                    
                  </div>
                </div> -->
                <div class="col-md-12">
                  <h4 class="text-primary" style="font-family:open sans;color:black;"><i class="fa fa-cogs"></i> <b>P</b>ayroll</h4>
                  <div class="panel-group" id="accordion_a">
        
                    <!--..........................PENALTY TRANSCATION...............................-->
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_22" style="background:#282828  !important; color:white;font-family:open sans;height: 40px;">
                            <div class="row">
                              <div class="col-lg-9">
                                <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-cogs" style="color:#90906F;"></i></span> Penalty
                              </div>
                              <div class="col-lg-3" style="font-size:12px !important;">
                                <span>Total:</span> : <span class="penalty-sum"> <?php echo round($totalpenaltys[0]['totalaccount']);?></span>
                              </div>
                            </div>
                          </a>
                        </h4>
                      </div>
                      <div id="accordion_a_22" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="pull-right">
                                <button class="btn btn-default" id="btnTable22"><i class="fa fa-th-list"></i> List </button>
                                <button class="btn btn-default" id="btnGraph22"><i class="ion-pie-graph"></i> Chart</button>
                              </div>
                            </div>
                          </div><br>
                          <div id="no-more-tables">
                            <table class="table table-striped table-hover penalty_tbl">
                              <thead class="cf">
                                <tr>
                                  <th style='background: #368EE0;max-width:20px !important;'>Vr#</th>
                                  <th style='background: #368EE0;'>Time</th>
                                  <th style='background: #368EE0;'>User</th>
                                  <th style='background: #368EE0;'>Party</th>
                                  <th style='background: #368EE0;' class="text-left">Discount</th>
                                  <th style='background: #368EE0;' class="text-left">Tax</th>
                                  <th style='background: #368EE0;' class="text-right">Net Amount</th>
                                </tr>
                              </thead>
                              <tbody class="penalty_tbody">
                                <?php $counter = 1; foreach ($penaltys as $penalty): ?>
                                  <tr>
                                    <td><a href="<?php echo makeHref($penalty['etype'],$penalty['vrnoa']) ; ?>"><?php echo $penalty['vrnoa'] .'-'. abbrivateEtypes($penalty['etype']); ?></a></td>
                                    <td><?php echo $penalty['date_time']; ?></td>
                                    <td><?php echo $penalty['user_name']; ?></td>
                                    <td><?php echo $penalty['party_name']; ?></td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td class="text-right"><?php echo $penalty['namount']; ?></td>
                                  </tr>
                                <?php endforeach ?>
                              </tbody>
                              <tfoot class="hidden">
                                <tr>
                                  <td><span></span> : <span class="sales-sum-val"><?php //echo $tot; ?></span></td>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="tabs_penalty hide">
                                <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                                  <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                                  <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                                  <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                                </ul>
                                <div class="tab-content">
                                  <div role="tabpanel" class="tab-pane active" id="home">...</div>
                                  <div role="tabpanel" class="tab-pane" id="profile">...</div>
                                  <div role="tabpanel" class="tab-pane" id="messages">...</div>
                                  <div role="tabpanel" class="tab-pane" id="settings">...</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!--..........................LOAN TRANSCATION...............................-->
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_23" style="background:#282828  !important; color:white;font-family:open sans;height: 40px;">
                            <div class="row">
                              <div class="col-lg-9">
                                <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-cogs" style="color:#90906F;"></i></span> Loan
                              </div>
                              <div class="col-lg-3" style="font-size:12px !important;">
                                <span>Total:</span> : <span class="loan-sum"> <?php echo round($totalloans[0]['totalaccount']);?></span>
                              </div>
                            </div>
                          </a>
                        </h4>
                      </div>
                      <div id="accordion_a_23" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="pull-right">
                                <button class="btn btn-default" id="btnTable23"><i class="fa fa-th-list"></i> List </button>
                                <button class="btn btn-default" id="btnGraph23"><i class="ion-pie-graph"></i> Chart</button>
                              </div>
                            </div>
                          </div><br>
                          <div id="no-more-tables">
                            <table class="table table-striped table-hover loan_tbl">
                              <thead class="cf">
                                <tr>
                                  <th style='background: #368EE0;max-width:20px !important;'>Vr#</th>
                                  <th style='background: #368EE0;'>Time</th>
                                  <th style='background: #368EE0;'>User</th>
                                  <th style='background: #368EE0;'>Party</th>
                                  <th style='background: #368EE0;' class="text-left">Discount</th>
                                  <th style='background: #368EE0;' class="text-left">Tax</th>
                                  <th style='background: #368EE0;' class="text-right">Net Amount</th>
                                </tr>
                              </thead>
                              <tbody class="loan_tbody">
                                <?php $counter = 1; foreach ($loans as $loan): ?>
                                  <tr>
                                    <td><a href="<?php echo makeHref($loan['etype'],$loan['vrnoa']) ; ?>"><?php echo $loan['vrnoa'] .'-'. abbrivateEtypes($loan['etype']); ?></a></td>
                                    <td><?php echo $loan['date_time']; ?></td>
                                    <td><?php echo $loan['user_name']; ?></td>
                                    <td><?php echo $loan['party_name']; ?></td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td class="text-right"><?php echo $loan['namount']; ?></td>
                                  </tr>
                                <?php endforeach ?>
                              </tbody>
                              <tfoot class="hidden">
                                <tr>
                                  <td><span></span> : <span class="sales-sum-val"><?php //echo $tot; ?></span></td>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="tabs_loan hide">
                                <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                                  <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                                  <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                                  <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                                </ul>
                                <div class="tab-content">
                                  <div role="tabpanel" class="tab-pane active" id="home">...</div>
                                  <div role="tabpanel" class="tab-pane" id="profile">...</div>
                                  <div role="tabpanel" class="tab-pane" id="messages">...</div>
                                  <div role="tabpanel" class="tab-pane" id="settings">...</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!--..........................ADVANCE TRANSCATION...............................-->
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_24" style="background:#282828  !important; color:white;font-family:open sans;height: 40px;">
                            <div class="row">
                              <div class="col-lg-9">
                                <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-cogs" style="color:#90906F;"></i></span> Advance
                              </div>
                              <div class="col-lg-3" style="font-size:12px !important;">
                                <span>Total:</span> : <span class="advance-sum"> <?php echo round($totaladvances[0]['totalaccount']);?></span>
                              </div>
                            </div>
                          </a>
                        </h4>
                      </div>
                      <div id="accordion_a_24" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="pull-right">
                                <button class="btn btn-default" id="btnTable24"><i class="fa fa-th-list"></i> List </button>
                                <button class="btn btn-default" id="btnGraph24"><i class="ion-pie-graph"></i> Chart</button>
                              </div>
                            </div>
                          </div><br>
                          <div id="no-more-tables">
                            <table class="table table-striped table-hover advance_tbl">
                              <thead class="cf">
                                <tr>
                                  <th style='background: #368EE0;max-width:20px !important;'>Vr#</th>
                                  <th style='background: #368EE0;'>Time</th>
                                  <th style='background: #368EE0;'>User</th>
                                  <th style='background: #368EE0;'>Party</th>
                                  <th style='background: #368EE0;' class="text-left">Discount</th>
                                  <th style='background: #368EE0;' class="text-left">Tax</th>
                                  <th style='background: #368EE0;' class="text-right">Net Amount</th>
                                </tr>
                              </thead>
                              <tbody class="advance_tbody">
                                <?php $counter = 1; foreach ($advances as $advance): ?>
                                  <tr>
                                    <td><a href="<?php echo makeHref($advance['etype'],$advance['vrnoa']) ; ?>"><?php echo $advance['vrnoa'] .'-'. abbrivateEtypes($advance['etype']); ?></a></td>
                                    <td><?php echo $advance['date_time']; ?></td>
                                    <td><?php echo $advance['user_name']; ?></td>
                                    <td><?php echo $advance['party_name']; ?></td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td class="text-right"><?php echo $advance['namount']; ?></td>
                                  </tr>
                                <?php endforeach ?>
                              </tbody>
                              <tfoot class="hidden">
                                <tr>
                                  <td><span></span> : <span class="sales-sum-val"><?php //echo $tot; ?></span></td>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="tabs_advance hide">
                                <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                                  <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                                  <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                                  <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                                </ul>
                                <div class="tab-content">
                                  <div role="tabpanel" class="tab-pane active" id="home">...</div>
                                  <div role="tabpanel" class="tab-pane" id="profile">...</div>
                                  <div role="tabpanel" class="tab-pane" id="messages">...</div>
                                  <div role="tabpanel" class="tab-pane" id="settings">...</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!--..........................INCENTIVE TRANSCATION...............................-->
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion_a" href="#accordion_a_25" style="background:#282828  !important; color:white;font-family:open sans;height: 40px;">
                            <div class="row">
                              <div class="col-lg-9">
                                <span class="badge badge-primary" style="padding: 0px 6px 5px 6px;background:white !important;"><i class="fa fa-cogs" style="color:#90906F;"></i></span> Incentive
                              </div>
                              <div class="col-lg-3" style="font-size:12px !important;">
                                <span>Total:</span> : <span class="incentive-sum"> <?php echo round($totalincentives[0]['totalaccount']);?></span>
                              </div>
                            </div>
                          </a>
                        </h4>
                      </div>
                      <div id="accordion_a_25" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="pull-right">
                                <button class="btn btn-default" id="btnTable25"><i class="fa fa-th-list"></i> List </button>
                                <button class="btn btn-default" id="btnGraph25"><i class="ion-pie-graph"></i> Chart</button>
                              </div>
                            </div>
                          </div><br>
                          <div id="no-more-tables">
                            <table class="table table-striped table-hover incentive_tbl">
                              <thead class="cf">
                                <tr>
                                  <th style='background: #368EE0;max-width:20px !important;'>Vr#</th>
                                  <th style='background: #368EE0;'>Time</th>
                                  <th style='background: #368EE0;'>User</th>
                                  <th style='background: #368EE0;'>Party</th>
                                  <th style='background: #368EE0;' class="text-left">Discount</th>
                                  <th style='background: #368EE0;' class="text-left">Tax</th>
                                  <th style='background: #368EE0;' class="text-right">Net Amount</th>
                                </tr>
                              </thead>
                              <tbody class="incentive_tbody">
                                <?php $counter = 1; foreach ($incentives as $incentive): ?>
                                  <tr>
                                    <td><a href="<?php echo makeHref($incentive['etype'],$incentive['vrnoa']) ; ?>"><?php echo $incentive['vrnoa'] .'-'. abbrivateEtypes($incentive['etype']); ?></a></td>
                                    <td><?php echo $incentive['date_time']; ?></td>
                                    <td><?php echo $incentive['user_name']; ?></td>
                                    <td><?php echo $incentive['party_name']; ?></td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td class="text-right"><?php echo $incentive['namount']; ?></td>
                                  </tr>
                                <?php endforeach ?>
                              </tbody>
                              <tfoot class="hidden">
                                <tr>
                                  <td><span></span> : <span class="sales-sum-val"><?php //echo $tot; ?></span></td>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="tabs_incentive hide">
                                <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Area Chart</a></li>
                                  <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Line Chart</a></li>
                                  <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Bar Chart</a></li>
                                  <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Donut Chart</a></li>
                                </ul>
                                <div class="tab-content">
                                  <div role="tabpanel" class="tab-pane active" id="home">...</div>
                                  <div role="tabpanel" class="tab-pane" id="profile">...</div>
                                  <div role="tabpanel" class="tab-pane" id="messages">...</div>
                                  <div role="tabpanel" class="tab-pane" id="settings">...</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div><!-- main-content --> 
        </div><!-- end of container -->

<!--///////////////////////////////////////....END OF DASHBORD....///////////////////////////////////-->


