<!doctype html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Voucher</title>

	    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
	    <link rel="stylesheet" href="../../assets/css/bootstrap-responsive.min.css">
	    <link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">

		<style>
			 * { margin: 0; padding: 0; font-family: tahoma !important; }
			 body { font-size:10px !important; }
			 p { margin: 0 !important; /* line-height: 17px !important; */ }
			 .field { font-size:12px !important; font-weight: bold !important; display: inline-block !important; width: 100px !important; } 
			 .field1 { font-size:12px !important; font-weight: bold !important; display: inline-block !important; width: 150px !important; } 
			 .voucher-table{ border-collapse: none !important; }
			 .voucher-table-summary{ border-collapse: none !important; }
			 table { width: 100% !important; border: 0px solid black !important; border-collapse:collapse !important; table-layout:fixed !important; margin-left:1px}
			 th {  padding: 5px !important; }
			 td { /*text-align: center !important;*/ vertical-align: top !important;  }
			 td:first-child {  }
			 .voucher-table thead th {background: #ccc !important; } 
			 tfoot {border-top: 0px solid black !important; } 
			 .bold-td { font-weight: bold !important; border-bottom: 0px solid black !important;}
			 .nettotal { font-weight: bold !important; font-size: 11px !important; border-top: 0px solid black !important; }
			 .invoice-type { border-bottom: 0px solid black !important; }
			 .relative { position: relative !important; }
			 .signature-fields{ font-size: 10px; border: none !important; border-spacing: 20px !important; border-collapse: separate !important;} 
			 .signature-fields th {border: 0px !important; border-top: 1px solid black !important; border-spacing: 10px !important; }
			 .inv-leftblock { width: 280px !important; }
			 .text-left { text-align: left !important; }
			 .text-right { text-align: right !important; }
			 td {font-size: 12px !important; font-family: tahoma !important; line-height: 14px !important; padding: 4px !important; } 
			 .rcpt-header { width: 550px !important; margin: auto !important; display: block !important; }
			 .inwords, .remBalInWords { text-transform: uppercase !important; }
			 .barcode { margin: auto !important; }
			 h3.invoice-type {font-size: 16px !important; line-height: 24px !important;}
			 .extra-detail span { background: #7F83E9 !important; color: white !important; padding: 5px !important; margin-top: 17px !important; display: block !important; margin: 5px 0px !important; font-size: 12px !important; text-transform: uppercase !important; letter-spacing: 1px !important;}
			 .nettotal { color: red !important; font-size: 12px !important;}
			 .remainingBalance { font-weight: bold !important; color: blue !important;}
			 .centered { margin: auto !important; }
			 p { position: relative !important; font-size: 12px !important; }
			 thead th { font-size: 12px !important; font-weight: bold !important; padding: 10px !important; }
			 .fieldvalue { font-size:12px !important; position: absolute !important; width: 497px !important; }

			 @media print {
			 	.noprint, .noprint * { display: none !important; }
			 }
			 .pl20 { padding-left: 20px !important;}
			 .pl40 { padding-left: 40px !important;}
				
			.barcode { float: right !important; }
			.item-row td { font-size: 12px !important; padding: 10px !important; border-top: 0px solid black !important;}
			.footer_company td { font-size: 8px !important; padding: 10px !important; border-top: 0px solid black !important;}

			.rcpt-header { width: 305px !important; margin: 0px !important; display: inline !important; position: absolute !important; top: 0px !important; right: 0px !important; }
			h3.invoice-type { border: none !important; margin: 0px !important; position: relative !important; top: 34px !important; }
			tfoot tr td { font-size: 10px !important; padding: 10px !important;  }
			.subtotalend { font-size: 12px !important; font-weight: bold !important;text-align: right !important; }
			.nettotal, .subtotal, .vrqty,.vrweight { font-size: 12px !important; font-weight: bold !important;text-align: right !important; color: red;}
		</style>
	</head>
	<body>
		<div class="container-fluid" style="">
			<div class="row-fluid">
				<div class="span12 centered">
					<!-- <div class="row-fluid top-img-head" style="display:none; padding-top: 15px; padding-bottom: 15px;">
						<div class="span12">
							<img src="#" class="top-head" alt="">
						</div>
					</div> -->
					<div class="row-fluid relative">
						<div class="span12">
								<div class="block pull-left inv-leftblock" style="width:250px !important; display:inline !important;">
									<p><span class="field">Invoice#</span><span class="fieldvalue inv-vrnoa"><?php echo $vrdetail[0]['vrnoa'];?></span></p>									
									<p><span class="field">Date:</span><span class="fieldvalue inv-date"><?php echo date('d-M-y', strtotime($vrdetail[0]['vrdate']));;?></span></p>
									<p><span class="field ">Customer:</span><span class="fieldvalue cust-name"><?php echo $vrdetail[0]['party_name'];?></span></p>
									<p><span class="field">Remarks:</span><span class="fieldvalue cust-mobile"><?php echo $vrdetail[0]['remarks'];?></span></p>
									<!-- <p><span class="field">Receipt By</span><span class="fieldvalue rcptBy">[Receipt By]</span></p> -->
								</div>
								<div class="block pull-right" style="width:280px !important; float: right; display:inline !important;">
									
									<div class="span12"><img style="float:right; width:280px !important;" class="rcpt-header logo-img" src="<?php echo $header_img;?>" alt=""></div>
									<h3 class="invoice-type text-right" style="border:none !important; margin: 0px !important; position: relative; top: 12px !important; "><?php echo $title; ?></h3>
								</div>
						</div>
					</div>
					<br>
					<br>
					<br>
					
					<div class="row-fluid">
						<table class="voucher-table">
							<thead>
								<tr>
									<th style=" width: 10px; " >Sr#</th>
									<th style=" width: 100px; text-align:left; ">Description</th>
									<th style=" width: 10px; ">Uom</th>
									<th style=" width: 15px; " class='text-right'>Qty</th>
									<th style=" width: 18px; " class='text-right'>Weight</th>
									<th style=" width: 15px; " class='text-right'>Rate</th>
									<th style=" width: 30px; " class='text-right'>Amount</th>
									<th style=" width: 15px; " class='text-right'>FRate</th>
									<th style=" width: 30px; " class='text-right'>FAmount</th>
								</tr>
							</thead>

							<tbody>
								
								<?php 
									$serial = 1;
									$netQty = 0;
									$netAmount=0;

									$FnetAmount=0;
									$FGrossAmount=0;

									$netWeight=0;

									$typee='';
									$typee22='';
                                    $totalAdd = 0;
                                    $totalLess = 0;

									foreach ($vrdetail as $row):
										

										if ($row['type']!==$typee){
											if($serial!=1){ ?>
											<tr class="foot-comments">
												<td class="subtotalend bold-td text-right" colspan="3">Subtotal:</td>
												<td class="subtotalend bold-td text-right"><?php echo number_format($netQty,2);?></td>
												<td class="subtotalend bold-td text-right"><?php echo number_format($netWeight,2);?></td>
												<td class="subtotalend bold-td text-right"></td>
												<td class="subtotalend bold-td text-right"><?php echo number_format($netAmount,2);?></td>
												<td class="subtotalend bold-td text-right"></td>
												<td class="subtotalend bold-td text-right"><?php echo number_format($FGrossAmount,2);?></td>
											</tr>
											<?php
											}
												?>
													<td class="bold-td text-left" colspan="9"><?php echo ($row['type'] == 'add')?'Order':'Deduction';?></td>
												<?php
												$typee=$row['type'];
												$netQty = 0;
												$netAmount=0;
												$netWeight=0;
												$FGrossAmount=0;
											
										}
								?>
									<tr  class="item-row">
									   <td class='text-left'><?php echo $serial++;?></td>
									   <td class='text-left'><?php echo $row['item_name'];?></td>
									   <td class='text-centre'><?php echo $row['uom'];?></td>
									   <td class='text-right'><?php echo number_format(abs($row['qty']),2);?></td>
									   <td class='text-right'><?php echo number_format(abs($row['weight']),2);?></td>
									   <td class='text-right'><?php echo number_format(($row['rate']),2);?></td>
									   <td class="text-right"><?php echo number_format(($row['amount']),2);?></td>
									   <td class='text-right'><?php echo number_format(($row['fedrate']),2);?></td>
									   <td class="text-right"><?php echo number_format(($row['fedamount']),2);?></td>
									</tr>
									
								<?php 
										$netQty += abs($row['qty']);
										$netAmount += $row['amount'];
										$FnetAmount += $row['fedamount'];
										$FGrossAmount += $row['fedamount'];

										$netWeight += abs($row['weight']);
                                        if($row['type'] == 'add')
                                        {
                                            $totalAdd += $row['amount'];
                                        }
                                        else
                                        {
                                            $totalLess += $row['amount'];
                                        }
								endforeach?>
							</tbody>
							<tfoot>
								<tr class="foot-comments">
									<td class="subtotalend bold-td text-right" colspan="3">Subtotal:</td>
									<td class="subtotalend bold-td text-right"><?php echo number_format($netQty,2);?></td>
									<td class="subtotalend bold-td text-right"><?php echo number_format($netWeight,2);?></td>
									<td class="subtotalend bold-td text-right"></td>
									<td class="subtotalend bold-td text-right"><?php echo number_format($netAmount,2);?></td>
									<td class="subtotalend bold-td text-right"></td>
									<td class="subtotalend bold-td text-right"><?php echo number_format($FGrossAmount,2);?></td>
								</tr>
								
								<?php if(intval($vrdetail[0]['discount'])!=0){?>
								<tr>
									<td class="subtotal bold-td text-right discount-td " colspan="5">Discount:</td>
									<td class="subtotal bold-td text-right"><?php echo number_format(($vrdetail[0]['discp']),2) . '% ';?></td>
									<td class="subtotal bold-td text-right"><?php echo number_format(($vrdetail[0]['discount']),2);?></td>
								</tr>
								<?php }?>
								<?php if(intval($vrdetail[0]['expense'])!=0){?>
								<tr>
									<td class="subtotal bold-td text-right discount-td " colspan="5">Expense:</td>
									<td class="subtotal bold-td text-right"><?php echo number_format(($vrdetail[0]['exppercent']),2) . '% ';?></td>
									<td class="subtotal bold-td text-right"><?php echo number_format(($vrdetail[0]['expense']),2);?></td>
								</tr>
								<?php }?>
								<?php if(intval($vrdetail[0]['tax'])!=0){?>
								<tr>
									<td class="subtotal bold-td text-right discount-td " colspan="5">Tax:</td>
									<td class="subtotal bold-td text-right"><?php echo number_format(($vrdetail[0]['taxpercent']),2) . '% ';?></td>
									<td class="subtotal bold-td text-right"><?php echo number_format(($vrdetail[0]['tax']),2);?></td>
								</tr>
								<?php }?>
								<?php if(!(intval($vrdetail[0]['tax'])==0 && intval($vrdetail[0]['discount'])==0 && intval($vrdetail[0]['expense'])==0 && intval($vrdetail[0]['paid'])==0 && intval($FnetAmount)==0) ){?>
								<tr>
									<td class="subtotal bold-td text-right discount-td " colspan="7">NetAmount:</td>
									<td class="subtotal add-lower text-right"></td>
									<td class="subtotal bold-td text-right"><?php echo number_format($vrdetail[0]['namount'],2);?></td>
									
								</tr>
								<?php }?>


							</tfoot>
						</table>
					</div>
					<div class="row-fluid">
						<div class="span12 add-on-detail1" style="margin-top: 10px;">
							<p class="" style="text-transform1: uppercase;">
								<?php if($title == 'Sale Invoice'){ ;?>
								<strong>In words: </strong> <span class="inwords"></span><?php echo convert_number_to_words(intval(($totalAdd - $totalLess - $vrdetail[0]['discount'] + $vrdetail[0]['expense'] + $vrdetail[0]['tax'])));?>ONLY <br>
								<?php } else { ?>
								<strong>In words: </strong> <span class="inwords"></span><?php echo convert_number_to_words(intval($vrdetail[0]['namount']));?> Only <br>
								<?php } ?>
								<br>
								<?php if ( $pre_bal_print==1  ){?>
								<div class="row-fluid">
									
									<table style="border:none !important;font-size:14px !important;font-weight:bold !important">
										<tbody >
											<tr>
												<td style=" width: 20px; " >Previous Balance:</td>
												<td style=" width: 15px; text-align:right;"><?php echo number_format($previousBalance,0);?></td>
												<td style=" width: 100px; "></td>
											</tr>
											<tr>
												<td style=" width: 20px; " >This Invoice:</td>
												<td style=" width: 20px; text-align:right;"><?php echo number_format($vrdetail[0]['namount']);?></td>
												<td style=" width: 100px; "></td>
											</tr>
											<tr>
												<td style=" width: 20px; " >Current Balance:</td>
												<td style=" width: 15px; text-align:right;"><?php echo number_format($previousBalance+$vrdetail[0]['namount'],2) ;?></td>
												<td style=" width: 100px; "></td>
											</tr>
										</tbody>
									</table>
									
								</div>

									<!-- <p><span class="field1">Previous Balance:</span><span class="fieldvalue inv-vrnoa"><?php echo number_format($previousBalance,0);?></span></p>
									<?php if($title == 'Sale Invoice'){ ;?>
									<p><span class="field1">This Invoice:</span><span class="fieldvalue inv-date text-right" style"text"><?php echo number_format($vrdetail[0]['namount']);?></span></p>
									<p><span class="field1">Current Balance:</span><span class="fieldvalue cust-name"><?php echo number_format(($totalAdd - $totalLess - $vrdetail[0]['discount'] + $vrdetail[0]['expense'] + $vrdetail[0]['tax'])+$previousBalance,2) ;?></span></p>
									<?php } else { ?>
									<p><span class="field1">This Invoice:</span><span class="fieldvalue inv-date"><?php echo number_format($totalAdd+$FnetAmount - $totalLess,0);?></span></p>
									<p><span class="field1">Current Balance:</span><span class="fieldvalue cust-name"><?php echo number_format(($totalAdd - $totalLess)+$previousBalance,2) ;?></span></p> -->
								<?php }};?>
							</p>
						</div>
					</div>
					<!-- End row-fluid -->
					<br>
					<br>
					<div class="row-fluid">
						<div class="span12">
							<table class="signature-fields">
								<thead>
									<tr>
										<th>Approved By</th>
										<th>Accountant</th>
										<th>Received By</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
					<div class="row-fluid">
						<p>
							<span class="footer_company">User:<?php echo $user;?></span><br>
							<!-- <span class="footer_company">Sofware By: www.digitalsofts.com, Mob:03218661765</span> -->
						</p>
					</div>
				</div>
			</div>
		</div>
	</body>
	</html>
<?php

function convert_number_to_words($number) {

    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
        0                   => 'zero',
        1                   => 'one',
        2                   => 'two',
        3                   => 'three',
        4                   => 'four',
        5                   => 'five',
        6                   => 'six',
        7                   => 'seven',
        8                   => 'eight',
        9                   => 'nine',
        10                  => 'ten',
        11                  => 'eleven',
        12                  => 'twelve',
        13                  => 'thirteen',
        14                  => 'fourteen',
        15                  => 'fifteen',
        16                  => 'sixteen',
        17                  => 'seventeen',
        18                  => 'eighteen',
        19                  => 'nineteen',
        20                  => 'twenty',
        30                  => 'thirty',
        40                  => 'fourty',
        50                  => 'fifty',
        60                  => 'sixty',
        70                  => 'seventy',
        80                  => 'eighty',
        90                  => 'ninety',
        100                 => 'hundred',
        1000                => 'thousand',
        1000000             => 'million',
        1000000000          => 'billion',
        1000000000000       => 'trillion',
        1000000000000000    => 'quadrillion',
        1000000000000000000 => 'quintillion'
    );

    if (!is_numeric($number)) {
        return false;
    }

    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }

    $string = $fraction = null;

    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }

    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }

    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }

    return $string;
}
?>