<!doctype html>
<html>

	<head>
		<!-- <link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">-->
		
		<link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../../assets/css/bootstrap-responsive.min.css">

		<style type='text/css'>
		* { margin: 0; padding: 0; font-family: tahoma; }
		 body { font-size:12px; color: #000; }
		 p { margin: 0; /* line-height: 17px; */ }
		table { width: 100%; border: 1px solid black; border-collapse:collapse; table-layout:fixed; border-collapse: collapse; }
		th { border: 1px solid black; padding: 5px; }
		td { /*text-align: center;*/ vertical-align: center; /*padding: 5px 10px;*/ border-top: none; border-bottom: none}
		@media print {
		 	.noprint, .noprint * { display: none; }
		 	/* thead, tfoot { display: table-row-group !important;} */
		 	
		 	
		 }
		 
		 .centered { margin: auto; }
		 /*@page{margin:10px auto !important; }*/
		 @page 
		    {	
		        size: auto !important;   /* auto is the initial value */
		        margin: 0mm !important;  /* this affects the margin in the printer settings */
		    }

		 .rcpt-header { margin: auto; display: block; }
		 td:first-child { text-align: left; }
	
		.subsum_tr td, .netsum_tr td { border-top:1px solid black !important; border-bottom:1px solid black; }

		.hightlight_tr td {border-top: 1px solid black; border-left:0 !important; border-right: 0 !important; border-bottom: 1px solid black; background: rgb(226, 226, 226); color: black; }
		.finalsum td {border-top: 1px solid black; border-left:0 !important; border-right: 0 !important; border-bottom: 1px solid black; background: rgb(250, 250, 250); color: black; }
		 .field {font-weight: bold; display: inline-block; width: 80px; } 
		 .voucher-table thead th {background: #ccc; padding:3px; text-align: center; font-size: 12px;} 
		 tfoot {border-top: 1px solid black; } 
		 tfoot td{text-align: left !important;}
		 .bold-td { font-weight: bold; border-bottom: 1px solid black;}
		 .nettotal { font-weight: bold; font-size: 14px; border-top: 1px solid black; }
		 .invoice-type { border-bottom: 1px solid black; }
		 .relative { position: relative; }
		 .signature-fields{ border: none; border-spacing: 20px; border-collapse: separate;} 
		 .signature-fields th {border: 0px; border-top: 1px solid black; border-spacing: 10px; }
		 .inv-leftblock { width: 280px; }
		 .text-left { text-align: left !important; }
		 .text-right { text-align: right !important; }
		 td {font-size: 10px; font-family: tahoma; line-height: 14px; padding: 4px;  text-transform: uppercase;} 
		 .rcpt-header { width: 450px; margin: auto; display: block; }
		 .inwords, .remBalInWords { text-transform: uppercase; }
		 .barcode { margin: auto; }
		 h3.invoice-type {font-size: 20px; width: 209px; line-height: 24px;}
		 .extra-detail span { background: #7F83E9; color: white; padding: 5px; margin-top: 17px; display: block; } 
		 .nettotal { color: red; }
		 .remainingBalance { font-weight: bold; color: blue;}
		 .centered { margin: auto; }
		 p { position: relative; }
		 .fieldvalue.cust-name {position: absolute; width: 497px; } 
		 .shadowhead { border-bottom: 0px solid black; padding-bottom: 5px; } 
		 .AccName { border-bottom: 0px solid black; padding-bottom: 5px; font-size: 16px; } 

		 .txtbold { font-weight: bolder; }
		 .myrows tr td{
		 	font-size: 12px !important;
		 } 
		 tfoot tr td{
		 	font-size: 12px !important;
		 } 
		 @media print {
		    table tbody tr td:before,
		    table tbody tr td:after {
		        content : "" ;
		        height : 4px ;
		        display : block ;
		    }
		
		</style>
	</head>

	<body>
		<div class="container" style="margin-top:10px;">
			<div class="row-fluid">
				<div class="span12">
					
					<div class="row-fluid">
						<div class="span12">
							<span>
								<h3 class="text-center" style="font-weight:bold;font-family: 'Open Sans', sans-serif;"><b>Ledger Account</b></h3>
							</span>
						</div><!-- end of col -->
					</div><!-- end of row -->
					<div class="row-fluid">
						<div class="span4"></div>
						<div class="span4">
							<h5 class="text-center">
								<b><p>Account Code :<span class="account_code"></span> To <span class="account_code"></span></p></b>
							</h5>
							<h5 class="text-center">
								<b><p><span class="date_from"></span> To <span class="date_to"></span></p></b>
							</h5>
						</div>
						<div class="span4"></div>
					</div><!-- end of row-fluid -->

					<div class="row-fluid">
						<div class="span12">
							<table class="table">
								<thead>
									<tr>
										<th style="width: 150px;"><b>Voucher No.</b></th>
										<th><b>Date</b></th>
										<th style='text-align: center; !important;'><b>Debit</b></th>
										<th style='text-align: center; !important;'><b>Credit</b></th>
										<th style='text-align: center; !important;'><b>Balance</b></th>
										<th colspan="2"><b>Description</b></th>
									</tr>
								
									<tr style="background:#808080; font-weight: bold; font-size: 18px; color: #000;">
										<td class="account_code" colspan="2" style="font-size: 12px;"></td>
										<td colspan="3" class="party_data" style="font-size: 12px;"></td>
										<!-- <td style="font-size: 12px;"></td> -->
										<td style="font-size: 12px;">Turn Over</td>
										<td style="font-size: 12px;" class="turnOver"></td>
									</tr>
									<tr style="border-bottom:none !important;">
										<td colspan="4" ><b>Opening Balance</b></td>
										<td  class="opening_balance" style="font-size: 12px;"></td>
										<td></td>
										<td></td>
										<!-- <td></td> -->
									</tr>
								
								</thead>
								<tbody class="myrows"  style="margin-top: 100px !important;">	
								</tbody>
								<tfoot style="background:#F3EEEE;">
									<tr style='text-align:center;border-top:1px solid !important;'>
										
										<td colspan="2"><b>Total For :</b> <span class="account_code"></span> <span class="party_data"></span></td>
										<td class="total_debit" style="text-align: center !important;"></td>
										<td class="total_credit" style=" text-align: center !important;"></td>
										<td class="closing_balance" style=" text-align: center !important;"></td>
										<td colspan="2"></td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>	
				</div><!-- end of col -->
			</div><!-- row-fluid -->
		</div><!--container-fluid -->
		<script type="text/javascript" src="../../../assets/js/jquery.min.js"></script>
		<script type="text/javascript" src="../../../assets/lib/moment-js/moment.min.js"></script>
		<script src="../../../assets/js/handlebars.js"></script>
		<script type="text/javascript">
		$(function(){
			
			var opener = window.opener;
			        
	          var vrdate = "";
	          var voucher = "";
	          var qty = "";
	          var weight = "";
	          var item_code = "";
	          var rate = "";
	          var amount = "";
	         opener.$('#datatable_example').find('tbody tr').each(function(index, elem)
	         {
	         	var temp = $.trim($(elem).find('td.voucher').text())
                 if (temp !='') {
                     var vrDates = moment($.trim($(elem).find('td.vrdate').text())).format('DD-MMM-YYYY');

                     voucher += "<tr><td>" + $.trim($(elem).find('td.voucher').text()) + "</td>" +
                         "<td>" + vrDates + "</td>" +
                             /*"<td colspan='2'>" + $.trim($(elem).find('td.qty').text()) + "</td>" +*/

                         "<td style='text-align: center !important;' >" + $.trim($(elem).find('td.weight').text()) + "</td>" +
                         "<td style='text-align: center !important;'>" + $.trim($(elem).find('td.rate').text()) + "</td>" +
                         "<td style='text-align: center !important;'>" + $.trim($(elem).find('td.amount').text()) + "-" + $.trim($(elem).find('td.amountre').text()) + "</td> " +
                         "<td colspan='2'>" + $.trim($(elem).find('td.remarks').text()) + "</td></tr>";
                 }

	         	 // "<td> </td>";
	         });

	          $(".myrows").empty();
	          var htmls = "<tr style='text-align:center;'>";
	          
	          htmls +=  voucher;
	          htmls += vrdate;
	          htmls +=  weight ;
	          htmls += rate;
	          htmls +=  amount;
	          htmls += qty;
	          // htmls += "<td class='text-right'></td>";
	          htmls += "</tr>";
	          $(".myrows").append(htmls);
	          var htmls = "";
	          htmls += "<tr class='tbody'>";
	          htmls += "</tr>";
	          $(".myrows").append(htmls);



			$('.account_code').text(opener.$('#name_dropdown').find('option:selected').data('accountid'));
			var vrDatesFrom = moment((opener.$('#from_date').val()).substr(0,10)).format('DD-MMM-YYYY');	
			$('.date_from').text(vrDatesFrom);
			var vrDatesTo = moment((opener.$('#to_date').val()).substr(0,10)).format('DD-MMM-YYYY');	
			$('.date_to').html(vrDatesTo);
			$('.party_data').html(opener.$('#name_dropdown').find('option:selected').text());
			// $('.remarks_data').html(opener.$('#txtRemarks').val());
			$('.total_debit').html(opener.$('.net-debit').text());
			$('.total_credit').html(opener.$('.net-credit').text());
			$('.closing_balance').html((opener.$('.running-total').text() > 0) ? opener.$('.running-total').text()+'-Dr' : opener.$('.running-total').text()+'-Cr' );
			$('.opening_balance').html(opener.$('.opening-bal').text());
			$('.turnOver').html(opener.$('.turnOver').text());

			});
		</script>
	</body>
</html>
