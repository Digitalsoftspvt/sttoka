<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Untitled Document</title>

    <style type="text/css">
        body{
        }
        .container{
            width:390px;
            height: auto;
            font-size: 22px;
            margin:0 auto;
        }
        .header{
            float:left;
        }
        .header h1{
            text-align:center;
            margin-bottom: 0;
        }
        .header p{
            width:175px;
            float:left;
            text-align:right;
        }
        .table1{

            width: 390px;
        }
        td{
            width:86px;
        }
        .row-head{
            border-top: 1px solid #000;
            border-bottom: 1px solid #000;
        }

        .footer{
            margin-top:10px;
            width: 100%;
            font-size: 20px;
        }
        .footer h3{
            text-align:right;
        }
        body, h1, h2, h3, h4, h5, h6, p, span,table,tbody,tr,td,th,tfoot, input, title {
            font-family: Jameel Noori Nastaleeq, Nafees Web Naskh, Arial, Urdu Naskh Asiatype, Tahoma, Unicode MS !important;
        }body, h1, h2, h3, h4, h5, h6, p, span,table,tbody,tr,td,th,tfoot, input, title {
             font-family: Jameel Noori Nastaleeq, Nafees Web Naskh, Arial, Urdu Naskh Asiatype, Tahoma, Unicode MS !important;
         }
        .text-left{
            text-align: left;
        }
        table {
            border-collapse: collapse;
        }
    </style>
</head>

<body>
<div class="container">
    <div class="header">
        <h1 class='titlee'>گیٹ پاس ان ورڈ</h1>
        <p style="width:50%;margin-bottom: 0"><span class="inv_data"></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:واؤچر نمبر</p>
        <p style="width:50%;margin-bottom: 0"><span class="date_data"></span>&nbsp;&nbsp;&nbsp;&nbsp;:تاریخ</p>
        <p style="width:100%; text-align:right;margin-bottom: 0;direction: rtl;">پارٹی:&nbsp;&nbsp;&nbsp;&nbsp;<span  class="party_data" style="text-decoration:underline; width:88%;"></span></p>
        <p style="width:100%;direction: rtl;">تفصیل:&nbsp;&nbsp;&nbsp;&nbsp;<span class="remarks_data" style="text-decoration:underline;text-align:right; width:85%;"></span></p>
    </div>
    <div class="table1">
        <table DIR="RTL" style="width: 100%">
            <thead>
            <tr class="row-head">
                <td style="width: 80%"><b>آئٹم تفصیل</b></td>
                <td style="width: 25%"><b>گودام</b></td>
                <td style="width: 20%;" class="text-left"><b>تعداد</b></td>
                <td style="width: 20%;" class="text-left"><b>وزن</b></td>


            </tr>
            </thead>
            <tbody class="myrows">

            </tbody>
            <tfoot>
            <tr class="row-head">
                <td style="font-weight: bold;">کل ٹوٹل:</td>
                <td></td>
                <td class="total-qty text-right" style="font-weight: normal;"></td>
                <td class="total-wgt text-left" style="font-weight: normal;"></td>
            </tr>
            <tr class="row-head" style="border: none;">
                <td></td>
                <td style="font-weight: bold;">کل اماؤنٹ:</td>
                <td></td>
                <td class="total-amount text-left" style="font-weight: bold;"></td>



            </tr>
            </tfoot>
        </table>
    </div>
    <div class="footer">
        <h3>ٹرانسپورٹر</h3>
        <p>Prepared By:&nbsp;&nbsp;&nbsp;<span>______________________</span></p>
        <p>Received By:&nbsp;&nbsp;&nbsp;<span>______________________</span></p>
        <p>Approved By:&nbsp;&nbsp;&nbsp<span>______________________</span></p>
    </div>
</div>
<script type="text/javascript" src="../../../assets/js/jquery.min.js"></script>
<script src="../../../assets/js/handlebars.js"></script>
<script type="text/javascript">
    $(function(){

        var opener = window.opener;


        var item_des = "";
        var qty = "";
        var weight = "";
        var godown = "";
        var rate = "";
        var amount = "";
        var html = '';
        
        var page_title = opener.$('.page_title').text();

        opener.$('#purchase_table').find('tbody tr').each(function(index, elem)
        {
            // item_code += $.trim($(elem).find('td.item_desc').data('item_code')) + "<br>";
            /*item_des += $.trim($(elem).find('td.item_desc').data('uitem_des')) + "<br>";
            // godown += opener.$('#dept_dropdown').find('option:selected').text() + "<br>";
            godown += $.trim($(elem).find('td.godown').text()) + "<br>";

            qty += $.trim($(elem).find('td.qty').text()) + "<br>";
            weight += $.trim($(elem).find('td.weight').text()) + "<br>";*/
        if(page_title=='Outward Gatepass Voucher' || page_title=='Inward Gatepass Voucher' )
        {
           
           html += "<tr class='row-content'><td>"+$.trim($(elem).find('td.item').text()) + "</td>";
        }
        else
        {
            alert(page_title);
       html += "<tr class='row-content'><td>"+$.trim($(elem).find('td.item_desc').data('uitem_des')) + "</td>";

        }
            
            html += "<td class='text-right' style='font-size: 17px;'>"+$.trim($(elem).find('td.godown').text()) + "</td>";
            html += "<td class='text-left' style='font-size: 17px;'>"+$.trim($(elem).find('td.qty').text()) + "</td>";
            html += "<td class='text-left' style='font-size: 17px;'>"+$.trim($(elem).find('td.weight').text()) + "</td></tr>";
        });

        $('.myrows').empty();
        // var htmls = "<tr class='row-content'>";

        // htmls += "<td>" +  weight + "</td>";
        // htmls += "<td>" +  qty + "</td>";
        // htmls += "<td class='text-right'>" + godown + "</td>";
        // htmls += "<td class='text-right'>" + item_des + "</td>";
        // htmls += "</tr>";
        $(".myrows").append(html);
        /*var htmls = "";
        htmls += "<tr class='tbody'>";
        htmls += "</tr>";
        $(".myrows").append(htmls);*/
        var party_data;
        
        if(page_title=='Inward Gatepass Voucher' || page_title =='Outward Gatepass Voucher')
        {

            party_data = opener.$('#party_dropdown').find('option:selected').data('uname')+ ' ' + opener.$('#party_dropdown').find('option:selected').data('uaddress');
            console.log('if');
        }
        else{
            party_data = opener.$('#party_dropdown11').find('option:selected').data('uname')+ ' ' +opener.$('#party_dropdown11').find('option:selected').data('uaddress');
        }
        if(page_title=='Outward Gatepass Voucher')
        {
            $('.titlee').text('آوٹ ورڈ');
        }
        else if(page_title=='Inward Gatepass Voucher')
        {
            $('.titlee').text('ان ورڈ');

        }


        // $('.shadowhead').html('Recipe Costing');
        $('.inv_data').text(opener.$('#txtVrnoa').val());
        $('.date_data').text((opener.$('#current_date').val()).substr(0,10));
        $('.party_data').text(party_data);
        $('.noted_by').html(opener.$('#txtNotedBy').val());
        $('.remarks_data').text(opener.$('#txtRemarks').val());
    
        if(page_title=='Outward Gatepass Voucher' || page_title=='Inward Gatepass Voucher' )
        {
            $('.total-qty').html(opener.$('#txtGQty').val());
        }
        else
        {
        $('.total-qty').html(opener.$('#txtTotalQty').val());
        $('.total-wgt').html(opener.$('#txtTotalWeight').val());

        }

        $('.total-amount').html(opener.$('#txtTotalAmount').val());
        // $('.cbalance').html(opener.$('#party_dropdown11').find('option:selected').data('pbalance'));
        // $('.tbalance').html(opener.$('#txtNetAmount').val());
        window.print();
    });
</script>
</body>
</html>
