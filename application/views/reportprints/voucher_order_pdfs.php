<!doctype html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Voucher</title>

	    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
	    <link rel="stylesheet" href="../../assets/css/bootstrap-responsive.min.css">
	    <link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">

		<style>
			 * { margin: 0; padding: 0; font-family: tahoma !important; }
			 body { font-size:10px !important; }
			 p { margin: 0 !important; /* line-height: 17px !important; */ }
			 .field { font-size:12px !important; font-weight: bold !important; display: inline-block !important; width: 100px !important; } 
			 .field1 { font-size:12px !important; font-weight: bold !important; display: inline-block !important; width: 150px !important; } 
			 .voucher-table{ border-collapse: none !important;border-left:none !important;; }
			 .voucher-table tbody td{
			 	border: none !important;
			 }
			 table { width: 100% !important; border: 0px solid black !important; border-collapse:collapse !important; table-layout:fixed !important; margin-left:1px}
			 th {  padding: 5px !important; }
			 td { /*text-align: center !important;*/ vertical-align: top !important;  }
			 td:first-child {  }
			 .voucher-table thead th {background: white !important; } 
			 tfoot {border-top: 0px solid black !important; } 
			 .bold-td { font-weight: bold !important; border-bottom: 0px solid black !important;}
			 .nettotal { font-weight: bold !important; font-size: 11px !important; border-top: 0px solid black !important; }
			 .invoice-type { border-bottom: 0px solid black !important; }
			 .relative { position: relative !important; }
			 .signature-fields{ font-size: 10px; border: none !important; border-spacing: 20px !important; border-collapse: separate !important;} 
			 .signature-fields th {border: 0px !important; border-top: 1px solid black !important; border-spacing: 10px !important; }
			 .inv-leftblock { width: 280px !important; }
			 .text-left { text-align: left !important; }
			 .text-right { text-align: right !important; }
			 td {font-size: 12px !important; font-family: tahoma !important; line-height: 14px !important; padding: 4px !important; } 
			 .rcpt-header { width: 550px !important; margin: auto !important; display: block !important; }
			 .inwords, .remBalInWords { text-transform: uppercase !important; }
			 .barcode { margin: auto !important; }
			 h3.invoice-type {font-size: 16px !important; line-height: 24px !important;}
			 .extra-detail span { background: #7F83E9 !important; color: white !important; padding: 5px !important; margin-top: 17px !important; display: block !important; margin: 5px 0px !important; font-size: 12px !important; text-transform: uppercase !important; letter-spacing: 1px !important;}
			 .nettotal { color: red !important; font-size: 12px !important;}
			 .remainingBalance { font-weight: bold !important; color: blue !important;}
			 .centered { margin: auto !important; }
			 p { position: relative !important; font-size: 12px !important; }
			 thead th { font-size: 12px !important; font-weight: bold !important; padding: 10px !important; }
			 .fieldvalue { font-size:12px !important; position: absolute !important; width: 497px !important; }

			 @media print {
			 	.noprint, .noprint * { display: none !important; }
			 }
			 .pl20 { padding-left: 20px !important;}
			 .pl40 { padding-left: 40px !important;}
				
			.barcode { float: right !important; }
			.item-row td { font-size: 12px !important; padding: 10px !important; border-top: 0px solid black !important;}
			.footer_company td { font-size: 8px !important; padding: 10px !important; border-top: 0px solid black !important;}

			.rcpt-header { width: 305px !important; margin: 0px !important; display: inline !important; position: absolute !important; top: 0px !important; right: 0px !important; }
			h3.invoice-type { border: none !important; margin: 0px !important; position: relative !important; top: 34px !important; }
			tfoot tr td { font-size: 10px !important; padding: 10px !important;  }
			.subtotalend { font-size: 12px !important; font-weight: bold !important;text-align: right !important; }
			.nettotal, .subtotal, .vrqty,.vrweight { font-size: 12px !important; font-weight: bold !important;text-align: right !important; color: red;}
		</style>
	</head>
	<body>
		<div class="container-fluid" style="">
			<div class="row-fluid">
				<div class="span12 centered">
					<!-- <div class="row-fluid top-img-head" style="display:none; padding-top: 15px; padding-bottom: 15px;">
						<div class="span12">
							<img src="#" class="top-head" alt="">
						</div>
					</div> -->
					<div class="row-fluid relative">
					<div class="row-fluid">
						<div class="span4" style="margin-left:600px;">
							<h3 class="text-center shadowhead txtbold new" style="font-size:30px !important;"><?php echo $title;?></h3>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<table style="border:none !important;">
								<tr>
									<td colspan="10">
										<p><span class="fieldvalue cust-name"><?php echo $vrdetail[0]['party_name'];?></span></p>
									</td>
									<td>
										<p><span class="field">Invoice No:</span><span class="fieldvalue inv-date"><?php echo substr($vrdetail[0]['vrnoa'], 0, 10);?></span></p>
									</td>
								</tr>
								<tr>
									<td colspan="10">
										<p><span class="field">Address:</span><span class="fieldvalue cust-mobile"><?php echo $vrdetail[0]['address'];?></span></p>
									</td>
									<td>
										<p><span class="field">Date:</span><span class="fieldvalue inv-date"><?php echo date('d-M-y', strtotime($vrdetail[0]['vrdate']));;?></span></p>
									</td>
								</tr>



							</table>
						</div>
					</div><br>
						<div class="span12">

								
								<div class="block pull-right" style="width:280px !important; float: right; display:inline !important;">
									
									<div class="span12"><img style="float:right; width:280px !important;" class="rcpt-header logo-img" src="<?php echo $header_img;?>" alt=""></div>
									<h3 class="invoice-type text-right" style="border:none !important; margin: 0px !important; position: relative; top: 12px !important; "><?php echo $title;?></h3>
								</div>
						</div>
					</div>
					<br>
					<br>
					<br>
					
					<div class="row-fluid">
						<table class="voucher-table">
							<thead style="border-bottom:1px solid;">
								<tr>
									<th style=" width: 50px; " >Item Code</th>
									<th style=" width: 150px; text-align:left; ">Item Name</th>
									<th style=" width: 15px; " class='text-right'>Qty</th>
									<th style=" width: 18px; " class='text-right'>Weight</th>
									<th style=" width: 15px; " class='text-right'>Rate</th>
									<th style=" width: 30px; " class='text-right'>Amount</th>
								</tr>
							</thead>

							<tbody>
								
								<?php 
									$serial = 1;
									$netQty = 0;
									$netAmount=0;
									$netWeight=0;

									$typee='';
									$typee22='';
									foreach ($vrdetail as $row):
										

										if ($row['type']!==$typee){
											if($serial!=1){ ?>
											<tr class="foot-comments">
												<td class="subtotalend bold-td text-right" colspan="2">Subtotal:</td>
												<td class="subtotalend bold-td text-right"><?php number_format($netQty,2);?></td>
												<td class="subtotalend bold-td text-right"><?php echo number_format($netWeight,2);?></td>
												<td class="subtotalend bold-td text-right"></td>
												<td class="subtotalend bold-td text-right"><?php echo number_format($netAmount,2);?></td>
											</tr>
											<?php
											}
												?>
													<td class="bold-td text-left" colspan="6"><?php echo $row['type'];?></td>
												<?php
												$typee=$row['type'];
												$netQty = 0;
												$netAmount=0;
												$netWeight=0;
											
										}
								?>
									<tr  class="item-row">
									   <td class='text-left'><?php echo $row['item_code'];?></td>
									   <td class='text-left'><?php echo $row['item_name'];?></td>
									   <td class='text-right'><?php echo number_format(abs($row['qty']),2);?></td>
									   <td class='text-right'><?php echo number_format(abs($row['weight']),2);?></td>
									   <td class='text-right'><?php echo number_format(($row['rate']),2);?></td>
									   <td class="text-right"><?php echo number_format(($row['amount']),2);?></td>
									</tr>
									
								<?php 
										$netQty += abs($row['qty']);
										$netAmount += $row['amount'];
										$netWeight += abs($row['weight']);
								endforeach?>
							</tbody>
							<tfoot>
								<tr class="foot-comments">
									<td class="subtotalend bold-td text-right" colspan="2">Subtotal:</td>
									<td class="subtotalend bold-td text-right"><?php number_format($netQty,2);?></td>
									<td class="subtotalend bold-td text-right"><?php echo number_format($netWeight,2);?></td>
									<td class="subtotalend bold-td text-right"></td>
									<td class="subtotalend bold-td text-right"><?php echo number_format($netAmount,2);?></td>
								</tr>
								
								<?php if(intval($vrdetail[0]['discount'])!=0){?>
								<tr>
									<td class="subtotal bold-td text-right discount-td " colspan="5">Discount:</td>
									<td class="subtotal bold-td text-right"><?php echo number_format(($vrdetail[0]['discp']),2) . '% ';?></td>
									<td class="subtotal bold-td text-right"><?php echo number_format(($vrdetail[0]['discount']),2);?></td>
								</tr>
								<?php }?>
								<?php if(intval($vrdetail[0]['expense'])!=0){?>
								<tr>
									<td class="subtotal bold-td text-right discount-td " colspan="5">Expense:</td>
									<td class="subtotal bold-td text-right"><?php echo number_format(($vrdetail[0]['exppercent']),2) . '% ';?></td>
									<td class="subtotal bold-td text-right"><?php echo number_format(($vrdetail[0]['expense']),2);?></td>
								</tr>
								<?php }?>
								<?php if(intval($vrdetail[0]['tax'])!=0){?>
								<tr>
									<td class="subtotal bold-td text-right discount-td " colspan="5">Tax:</td>
									<td class="subtotal bold-td text-right"><?php echo number_format(($vrdetail[0]['taxpercent']),2) . '% ';?></td>
									<td class="subtotal bold-td text-right"><?php echo number_format(($vrdetail[0]['tax']),2);?></td>
								</tr>
								<?php }?>
								<?php if(!(intval($vrdetail[0]['tax'])==0 && intval($vrdetail[0]['discount'])==0 && intval($vrdetail[0]['expense'])==0 && intval($vrdetail[0]['paid'])==0) ){?>
								<tr>
									<td class="subtotal bold-td text-right discount-td " colspan="5">NetAmount:</td>
									<td class="subtotal add-lower text-right"></td>
									<td class="subtotal bold-td text-right"><?php echo number_format(($vrdetail[0]['namount']),2);?></td>
								</tr>
								<?php }?>


							</tfoot>
						</table>
					</div>
					<div class="row-fluid"  style="margin:0px 0px 0px 500px !important;">
						<div class="span12 add-on-detail1">
							<p class="" style="text-transform1: uppercase;">
								<!-- <strong>In words: </strong> <span class="inwords"></span><?php echo $amtInWords;?>ONLY <br> -->
								<br>
								<?php if ( $pre_bal_print==1  ){?>
									<p><span class="field1">Previous Balance:</span><span class="fieldvalue inv-vrnoa"><?php echo number_format($previousBalance,0);?></span></p>
									<p><span class="field1">This Invoice:</span><span class="fieldvalue inv-date"><?php echo number_format($vrdetail[0]['namount'],0);?></span></p>
									<p><span class="field1">Current Balance:</span><span class="fieldvalue cust-name"><?php echo number_format($vrdetail[0]['namount']+$previousBalance,2) ;?></span></p>
								<?php };?>
							</p>
						</div>
					</div>
					<!-- End row-fluid -->
					<br>
					<br>
					<div class="row-fluid" style="margin-top:70px;">
						<div class="span12">
							<table class="signature-fields">
								<thead>
									<tr>
										<th>Approved By</th>
										<th style="border:none !important;"></th>
										<th>Accountant</th>
										<th style="border:none !important;"></th>
										<th>Received By</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
					<!-- <div class="row-fluid">
						<p>
							<span class="footer_company">User:<?php echo $user;?></span><br>
							<span class="footer_company">Sofware By: www.digitalsofts.com, Mob:03218661765</span>
						</p>
					</div> -->
				</div>
			</div>
		</div>
	</body>
	</html>