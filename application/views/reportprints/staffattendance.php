<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Staff Status Report</title>

	<link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
	<style>
		body {
			color: #333;
			font-family: tahoma !important;
			font-size: 10px;
		}
		.xs-small {
			font-size: 8px;
		}
		.small {
			font-size: 10px;
		}
		.medium {
			font-size: 11px;
		}
		#wrap {
			margin-top: 20px;
			margin-bottom: 20px;
		}
		.center{
			text-align: center;
		}
		.table {
			width: 100%;
		}
		.table td {
			padding-left: 5px !important;
			padding-top: 0px !important;
			padding-bottom: 0px !important;
			padding-right: 0px !important;
			border-top: none;
			border: 1px solid #333;
		}
		.table tr {
			border: 1px solid #333;
		}
		.table .thead {
			background: rgb(215, 215, 230);
		}
		.paddingspace {
			padding: 3px !important;
		}
		.bold {
			font-weight: bold;
		}
		#printdate {
			display: block;
			text-align: right;
			padding-right: 145px;
			font-size: 14px;
			padding-bottom: 6px;
		}
		#heading{
			background: #eee !important;
			margin-bottom: 3px;
		}
		.headingtitle {
			text-align: center;
			display: block;
			font-size: 16px;
			padding: 5px 0px 5px 0px;
		}
		.dept_name_head {
			padding-left: 32px;
			font-weight: bold;
			padding-top: 6px;
			padding-bottom: 6px;
		}
		.spec {
			background: rgba(90, 96, 111, 0.5) !important;
			color: #fff !important;
		}

		@media print {
			.btnPdfDownload {
				display: none;
			}
			@page {
				size: landscape;
			}
			body {
				-webkit-print-color-adjust: exact;
				-moz-print-color-adjust: exact;
			}
			.spec {
				background: rgba(90, 96, 111, 0.5) !important;
				color: #fff !important;
			}
			.headingtitle {
				text-align: center;
				display: block;
				font-size: 16px;
				padding: 5px 0px 5px 0px;
			}
			#heading{
				background: #eee !important;
				margin-bottom: 3px;
			}
		}
	</style>
</head>
<body>

	<div id="wrap">
		<div class="container-fluid">

			<div class="row divider hide">
				<div class="col-lg-12">
					<a class="btn btn-default btnPdfDownload"><i class="fa fa-download"></i> PDF Download</a>
				</div>
			</div>

			<div id='heading'>
				<span class='headingtitle'></span>
			</div>
			<div id="subheading">
				<span class='headingtitle'></span>
			</div>

			<div class="atnd-container">
				<table id="atnd" border='1px' style='width: 100%;'>
				</table>
			</div>
	</div>


	<script src='../../../assets/js/jquery.min.js'></script>
	<script src='../../../assets/bootstrap/js/bootstrap.min.js'></script>

	<script>

		var opener = window.opener;
		$(opener.$('#atnd-table').html()).appendTo('#atnd');
		$('td').addClass('center');
		$('th').addClass('center');
		$('th').addClass('paddingspace');
		$('#atnd thead tr th').addClass('spec');
		$('.headingtitle').text(opener.$('.page_title').text().trim());
		$('#subheading .headingtitle').text(opener.$('.month_year_picker').val());
		$(document).attr('title', opener.$('.page_title').text().trim());

		window.print();

		$('.btnPdfDownload').on('click', function(e) {
			e.preventDefault();
		});
	</script>
</body>
</html>