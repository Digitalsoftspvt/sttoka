<!DOCTYPE html>
<html lang = "en" >
    <head >
    <meta charset = "utf-8" >
    <title > Payment Report </title>
		<link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="../../../assets/css/bootstrap-responsive.min.css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet'>
    <style>
    body {       
     color: #333;
         /*font-family: tahoma !important;*/
         font-size: 10px;
     }
         #wrap {
                padding-left: 0px!important;
                overflow: hidden!important;
                height: auto!important;
                width:1000px!important;
                margin: auto!important;
            }
            .paidopt {
                margin-bottom: 2px;
                text-align: right;
                margin-right: 2px;
            }
            .paiddate {
                font-size: 12px;
                font-weight: bold;
                /*font-family: sans-serif;*/
            }
            .paiddatevalue {
                width: 118px;
                border-bottom: 1px solid #000;
         display: inline-block;
         margin-left: 5px;
         }
         .salaryslip {
         text-align: left !important;
         display: inline-block;
         width: 340px;
         padding: 0px 0px 0px 10px  !important;
         margin-right: 0px;
         margin-bottom: 0px;
         }
         tr {
         line-height: 15px;
         }
         .about {
         font-weight: bold;
         width: 115px !important;
         text-align:left;
         width:100px !important;
         display: block;
         font-size: 20px !important;
         /*font-family: sans-serif;*/
         }
         .title {
         background: blue;
         color: #fff;
                padding: 5px 10px 10px 6px;
                font-size: 12px;
                width: 345px!important;
                border-radius: 2px;
                margin-bottom: 10px;
                display: block;
                text-align: center;
            }
            .year {
                color: 2px solid white!important;
            }
            .empinf,
        .salaryinfo {
            width: 780px!important;
    height: 340px!important;
    margin-bottom: 8px;
            border: 5px double rgba(2, 7, 10, 0.89) !important;
            /*font-size: 40px !important;*/
          /* padding: 20px; */
        }
        .empinf table tr td {

            font-size: 25px!important;
            padding:15px;
        }
        .value {
            font-weight: bold!important;
            font-size: 20px;
            text-align:right;
            width:500px !important;
            /*font-family: sans-serif;*/
            text-align: right!important;
            padding: 0px 18px 4px 10px!important;
        }
        .netsalhead {
            font-size: 12px;
            font-weight: bold;
            /*font-family: sans-serif;*/
            display: inline-block;
            margin-right: 5px;
        }
        .netsalval {
            font-size: 16px;
            font-weight: bold;
            /*font-family: sans-serif;*/
        }

        @media print {
            @media print {

                .salaryslip {
                    margin-left:200px !important;
                    margin-top: 200px!important;
                    display: block!important;
                    page-break-after: always!important;
                }

		body, h1, h2, h3, h4, h5, h6, p,b, span,table,tbody,tr,td,th,tfoot, input, title {
         font-family: Jameel Noori Nastaleeq, Nafees Web Naskh, Arial, Urdu Naskh Asiatype, Tahoma, Unicode MS !important;
        }

            }

            .btnPdfDownload {
                display: none;
                margin: 0px!important;
            }
            .month {
                color: white!important;
            }
            .year {
                color: white!important;
            }

            body {
                -webkit-print-color-adjust:exact; 
                -moz-print-color-adjust:exact;

            }
            .title {
                background: blue!important;
                color: #fff!important;
                padding: 5px 6px 5px 6px;
                font-size: 12px;
                border-radius: 2px;
                margin-bottom: 10px;
                display: block;
                text-align: center;
            }

        }
        @media print {
            @page {
                size: landscape;
            }
        } 

		body, h1, h2, h3, h4, h5, h6, p,b, span,table,tbody,tr,td,th,tfoot, input, title {
         font-family: Jameel Noori Nastaleeq, Nafees Web Naskh, Arial, Urdu Naskh Asiatype, Tahoma, Unicode MS !important;
        }
    </style> 
    </head> 
        <body>

        <div id = "wrap" >
        <div class = "container-fluid" >

        <div class = "row divider hide" >
        <div class = "col-lg-12" >
        <a class = "btn btn-default btnPdfDownload" > <i class = "fa fa-download" > </i> PDF Download</a >
        </div> 
    </div>

                     <div class='salaryslip'> 
                        <div class='empinf' dir='rtl' > 
                        <table> 
                        <tr> 
                        <td><span class='about'><b>نام</b></span></td> 
                        <td><span class='value'><b>  Name  </b></span></td> 
                        <td><span class='about' ><b>25487</b></span></td> 
                        <td><span class='about' ><b>  #Inv  </b></span></td> 
                        </tr> 
                        <tr> 
                        <td><span class='about'>ایڈریس</span></td> 
                        <td><span colspan="3" class='value' style='font-size:14px !important;'><b>  address </b></span></td> 
                        </tr> 
                        <tr> 
                        <td><span class='about'>رابطہ نمبر</span></td> 
                        <td><span class='value'>  mobile  </span></td>
                        <td><span class='about'>رابطہ نمبر</span></td> 
                        <td><span class='value'>  mobile  </span></td> 
                        </tr> 
                        </table> 
                        </div> 
                        </div>
        <div class = "salaryslips-container2" >

        </div>
        </div> 
        </div> 
        <script src= '../../../assets/js/jquery.min.js' > </script> 
        <script src = '../../../assets/bootstrap/js/bootstrap.min.js' > </script>

        <script>
        var opener = window.opener;
        var base_url = opener.$('#base_url').val();
        $(opener.$('.report-table').html()).appendTo('#report');
        var accid = '';
        opener.$('#cash_table tbody tr').each(function(index, elem) {
            accid += $.trim($(this).find('td.pid').text()) + ',';
        });
        accid = accid.slice(0, -1);
        var invoicedetail = '';
        $.ajax({
            url: base_url + 'index.php/account/fetchmultiaccountdata',
            type: 'POST',
            data: {
                'pid': accid
            },
            dataType: 'JSON',
            success: function(data) {
                $.each(data, function(index, elem) {
                    invoicedetail += "<div class='salaryslip'>" +
                        "<div class='empinf' dir='rtl'>" +
                        "<table >" +
                        "<tr>" +
                        "<td><span class='about'><b>نام</b></span></td>" +
                        "<td><span class='value'><b>" + elem.uname + "</b></span></td>" +
                        "<td><span class='about'><b></b></span></td>" +
                        "<td><span class='value'><b>" + elem.uname + "</b></span></td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td><span class='about'>ایڈریس</span></td>" +
                        "<td><span class='value' style='font-size:14px !important;'><b>" + elem.uaddress + "</b></span></td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td><span class='about'>رابطہ نمبر</span></td>" +
                        "<td><span class='value'>" + elem.mobile + "</span></td>" +
                        "</tr>" +
                        "</table>" +
                        "</div>" +
                        "</div>";
                });
                $(invoicedetail).appendTo('.salaryslips-container')
            },
            error: function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
        $('#headingtitle').text(opener.$('.page_title').text().trim());

        $('#report tr').find('td').last().remove();
        $('#report tr').find('th').last().remove();

        $('td').addClass('center');
        $('th').addClass('center');
        $('th').addClass('paddingspace');

        var from = opener.$('#from_date').val().trim();
        var to = opener.$('#to_date').val().trim();

        $('#print_date').text(from + " To " + to);

        // window.print();

        // $('.btnPdfDownload').on('click', function(e) {
        //     e.preventDefault();
        // }); 
    </script> 
</body>
</html