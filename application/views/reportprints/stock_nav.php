<!DOCTYPE html>
<html>
	<head>
		<title>Application Report</title>
		<link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="../../../assets/css/bootstrap-responsive.min.css">
		<style>
			@page{margin:0px auto;width: 380px;}
        @media print {
          body{ width: 380px;margin: 0 auto; }
          .numeric{font-weight: normal;}
         }
			@media screen {
    	.main {margin: 8px 0px 0px 0px !important;}
			}
			@media print {
    	.main { margin: 0px 0px 0px -40px !important;}
			}
		 * { margin: 0; padding: 0; font-family: tahoma; }
			body { font-size:14px; }
			th{font-size: 14px; border:none !important;}
			td{border:none !important;}
			table{border:none !important;}
			.token_header{margin:40px 0px 0px 0px !important;}
			.report_title{font-size:20px; font-weight: bold;margin:15px 0px 0px 15px;}
			.fromDate .token_no{font-weight: normal;}
			.footer{  margin: 50px 0px 0px -19px !important; }
			.mid_header{border-bottom:1px solid;}
			.received{border-top:1px solid !important;}
			.disp{
				display: none;
			}
			.tables tr td{
				border-bottom:1px solid black !important;
			}

		</style>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-4 main" style="width: 400px;">
					<div class="row">
						<div class="col-lg-12">
							<p class="text-center report_title"><span style="text-decoration:underline;">Stock Navigation</span></p>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<table class="table">
								<tr>
									<td><b>Sr#</b></td>
									<td class="sr_no"></td>
									<td style="width:140px;"><b>Date :</b></td>
									<td class="current_Date"></td>
								</tr>
								<tr>
									<td><b>Rec By :</b></td>
									<td class="rec_by" colspan="2"></td>
								</tr>
								<tr>
									<td style="width: 140px;"><b>Issued By :</b></td>
									<td class="issue_by" colspan="2"></td>
								</tr>
								<tr>
									<td><b>Through :</b></td>
									<td class="through" colspan="2"></td>
								</tr>
								<tr>
									<td><b>Remarks :</b></td>
									<td class="remarks"></td>
								</tr>
							</table>
						</div><!-- end of col -->
					</div><!-- end of row -->
					<div class="row">
						<div class="col-lg-12">
							<table id="table">
								<thead style="border-top:1px solid #969696;border-bottom:1px solid #969696;">
									<tr>
										<th style="width:300px;">Description</th>
										<th style="width:80px;">Uom</th>
										<th style="width:80px;">From</th>
										<th style="width:80px;">Qty</th>
										<th style="width:80px;">Weight</th>
										<th>To</th>
									</tr>
								</thead>
								<tbody class="tables">
									
								</tbody>
								<tfoot>
									
								</tfoot>
							</table>
						</div>
					</div><br>
					<div class="row">
						<div class="col-lg-12">
							<table class="table">
								<tr> 
									<td> <strong style="border-bottom:1px solid;padding: 4px 144px 0px 0px;">Prepared By :</strong></td>
									<td></td>
									<td class="applicant_name"></td>
								</tr>
								<tr>
									<td><strong style="border-bottom:1px solid;padding: 4px 144px 0px 0px;">Received By :</strong></td>
									<td></td>
									<td class="applicantion_subject"></td>
								</tr>
								<tr>
									<td><strong style="border-bottom:1px solid;padding: 4px 144px 0px 0px;">Approved By :</strong></td>
									<td></td>
									<td class="applicantion_Type"></td>
								</tr>
							</table>
						</div><!-- end of col -->
					</div><!-- end of row -->
					<!-- <div class='row footer'>
						<div class="col-lg-12">
							<div class="foter_style">
								<p class="text-center"><strong>Thank You For Shopping Here</strong> </p>
								<p class="text-center"><strong>Ph# : </strong> 041-8543827,8714167 
							</div>footer
						</div>end of col
					</div>end of row -->
				</div><!--end of col-->
			</div><!-- end of row -->
		</div><!-- container-fluid -->

		<script type="text/javascript" src="../../../assets/js/jquery.min.js"></script>
		<script src="../../../assets/js/handlebars.js"></script>
		<script type="text/javascript">
			$(function(){
				var opener = window.opener;
				//opener.$('.hideme').addClass('hidden');
				var srNo = opener.$('#txtVrnoa').val();
				var currentDate = opener.$('#current_date').val();
				var received_by = opener.$('#receivers_list').val();
				var issued_by = opener.$('#issuers_list').val();
				var through = opener.$('#transport_dropdown').find('option:selected').text();
				var remrks = opener.$('#txtRemarks').val();

				var descriptionOfGoods = "";
				var uoms = '';
				var deptfroms = '';
				var qty = '';
				var weight = '';
				var deptto = '';
		        var html = '';
		        opener.$('#purchase_table').find('tbody tr').each(function(index, elem)
		        {
		            html += "<tr><td>"+$.trim($(elem).find('td.item').text()) + "</td>";
		            html += "<td>"+$.trim($(elem).find('td.uom').text()) + "</td>";
		            html += "<td>"+$.trim($(elem).find('td.deptfrom').text()) + "</td>";
		            html += "<td>"+$.trim($(elem).find('td.qty').text()) + "</td>";
		            html += "<td>"+$.trim($(elem).find('td.weight').text()) + "</td>";
		            html += "<td>"+$.trim($(elem).find('td.deptto').text()) + "</td></tr>";
		        });

		        $("#table tbody").append(html);

		        var html1 = "<tr style='border-top:1px solid #969696;border-bottom:1px solid #969696;'><td colspan='2'><b>Grand Total:</b></td><td></td><td>"+ opener.$('#txtGQty').val() +"</td><td>"+ opener.$('#txtGWeight').val() +"</td><td></td></tr>";
				 $("#table tfoot").append(html1);

				$('.sr_no').html(srNo);
				$('.current_Date').html(currentDate);
				$('.rec_by').html(received_by);
				$('.issue_by').html(issued_by);
				$('.through').html(through);
				$('.remarks').html(remrks);
				window.print();
			});
		</script>
	</body>
</html>