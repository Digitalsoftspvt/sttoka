<!doctype html>
<html>

	<head>
		<link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="../../../assets/css/bootstrap-responsive.min.css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet'>
		<style type='text/css'>
		.table{
			border: 0px !important;
		}
		.table thead tr th{
			border-bottom: 1px solid black !important;
			border-top: 1px solid black !important;
			font-weight: normal !important;
		}
		.table tbody tr td{
			border-top: 0px !important;
		}
		.table tfoot tr td{
			border-bottom: 1px solid black !important;
			border-top: 1px solid black !important;
		}
		.tbody {
			display: block;
			height: 430px;
			overflow: auto;
		}
		
		</style>
	</head>

	<body>
		<div class="container" style="margin-top:30px;">
			<div class="row">
				<div class="col-xs-12">
					
					<div class="row">
						<div class="col-xs-12">
							<span>
								<h3 class="text-center" style="font-weight:bold;font-family: 'Open Sans', sans-serif;"><b>SALE RETURN INVOICE</b></h3>
							</span>
						</div><!-- end of col -->
					</div><!-- end of row -->
					<div class="row">
						<div class="col-lg-12">
							<span class="party_data"></span>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-8"></div>
						<div class="col-xs-4">
							<div class="pull-right">
								<span style="font-weight:bold;font-family: 'Open Sans', sans-serif;" class='pull-right'><b>Invoice# :</b> <span class='pull-right inv_data' style='width:100px;text-align:right;'></span></span><br>
								<span style="font-weight:bold;font-family: 'Open Sans', sans-serif;" class='pull-right'><b>Dated : </b><span class='pull-right date_data' style='width:100px;text-align:right;'></span></span>
							</div>
						</div>
					</div><!-- end of row-fluid -->

					<div class="row">
						<div class="col-lg-12">
							<span class="remarks_data"></span>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<table class="table">
								<thead style="background:#E8E8E8;" class="myhead">
									
								</thead>
								<tbody class="myrows">
									
								</tbody>
								<tfoot>
									<tr>
										<td colspan="2"><b>Grand Total:</b></td>
										<td class="text-right total-qty"></td>
										<td class="text-right total-weight"></td>
										<td class="text-right"></td>
										<td class="text-right total-amount"></td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>	
				</div><!-- end of col -->
			</div><!-- row-fluid -->
			<div class="row">
                <div class="col-xs-2">
                    <div class="pull-right text-left">
                        <span class='text-right' ></span><br>
                        <span class='text-right' ></span><br>
                    </div>
                </div>
                <div class="col-xs-2">
                    <span class="expenseaccount"></span><br>
                    <span class="expenseamount"></span><br>
                </div>
                <div class="col-xs-4">
					<div class="pull-right text-right">
						<span class='text-right' >Previous Balance:</span><br>
						<span class='text-right' >This Invoice:</span><br>
						<span class='text-right' >Current Balance:</span>
					</div>
				</div>

				<div class="col-xs-4">
					<span class="pbalance">00000</span><br>
					<span class="tbalance">00000</span><br>
					<span class="cbalance">00000</span>
					
				</div>

			</div>
			<br><br>
			<div class="row-fluid">
				<div class="col-xs-3" style="border-top:1px solid black;text-align:center;">
					<span>Prepared By:</span>
				</div>
				<div class="col-xs-3 col-xs-offset-2" style="border-top:1px solid black;text-align:center;">
					<span>Checked By:</span>
				</div>
				<div class="col-xs-3 col-xs-offset-1" style="border-top:1px solid black;text-align:center;">
					<span>Approved By:</span>
				</div>
			</div>
		</div><!--container-fluid -->
		<script type="text/javascript" src="../../../assets/js/jquery.min.js"></script>
		<script src="../../../assets/js/handlebars.js"></script>
		<script type="text/javascript">
		$(function(){
			
			var opener = window.opener;
			
	        var po = "";
	        var item_des = "";
	        var uoms = "";
	        var qty = "";
	        var weight = "";

	        var html = "<tr>";
	          
	          html += "<th style='width:150px;' class='text-left'>Item Code</th>";
	          html += "<th class='text-left' style='width: 350px;'>Item Name</th>";
	          html += "<th style='width: 80px;' class='text-right'>Quantity</th>";
	          html += "<th style='width:80px;' class='text-right'>Weight</th>";
	          html += "<th style='width:80px;' class='text-right'>Rate</th>";
	          html += "<th style='width:120px;' class='text-right'>Amount</th>";
	          html += "</tr>";
								
	          $(".myhead").append(html);
	          var item_des = "";
	          var qty = "";
	          var weight = "";
	          var item_code = "";
	          var rate = "";
	          var amount = "";
	         opener.$('#salereturn_table').find('tbody tr').each(function(index, elem)
	         {
              item_code += $.trim($(elem).find('td.item_desc').data('item_code')) + "<br>";
              item_des += $.trim($(elem).find('td.item_desc').text()) + "<br>";
              qty += $.trim($(elem).find('td.qty').text()) + "<br>";
              weight += $.trim($(elem).find('td.weight').text()) + "<br>";
              rate += $.trim($(elem).find('td.rate').text()) + "<br>";
              amount += $.trim($(elem).find('td.amount').text()) + "<br>";
	         });

	          $('.myrows').empty();
	          var htmls = "<tr>";
	          
	          htmls += "<td>" +  item_code + "</td>";
	          htmls += "<td>" +  item_des + "</td>";
	          htmls += "<td class='text-right'>" + qty + "</td>";
	          htmls += "<td class='text-right'>" + weight + "</td>";
	          htmls += "<td class='text-right'>" + rate + "</td>";
	          htmls += "<td class='text-right'>" + amount + "</td>";
	          htmls += "</tr>";
	          $(".myrows").append(htmls);
	          var htmls = "";
	          htmls += "<tr class='tbody'>";
	          htmls += "</tr>";
	          $(".myrows").append(htmls);


		    var party_data = opener.$('#party_dropdown').find('option:selected').text() + ' ' +opener.$('#party_dropdown').find('option:selected').data('address');
			// $('.shadowhead').html('Recipe Costing');
			$('.inv_data').text(opener.$('#txtVrnoa').val());

			$('.party_data').html(party_data);
			$('.noted_by').html(opener.$('#txtNotedBy').val());
			$('.remarks_data').html(opener.$('#txtRemarks').val());
			$('.total-qty').html(opener.$('#txtTotalQty').val());
			$('.total-weight').html(opener.$('#txtTotalWeight').val());
			$('.total-amount').html(opener.$('#txtTotalAmount').val());
			

			var cbalance = (parseFloat(opener.$('#txtpre_balance').val()) - parseFloat(opener.$('#txtNetAmount').val())).toFixed(2);
	        var pbalance = parseFloat(opener.$('#txtpre_balance').val()).toFixed(2);
			var dr_cr_cbalance = "";
	        var dr_cr_pbalance = "";
	        if (cbalance < 0) {
	            dr_cr_cbalance = " Cr";
	        }else {
	            dr_cr_cbalance = " Dr";
	        }
	        if (pbalance < 0) {
	            dr_cr_pbalance = " Cr";
	        }else {
	            dr_cr_pbalance = " Dr";
	        }

			$('.pbalance').html(opener.$('#txtpre_balance').val() + dr_cr_pbalance);
			$('.tbalance').html(opener.$('#txtNetAmount').val());

           $('.cbalance').html((parseFloat(opener.$('#txtpre_balance').val()) - parseFloat(opener.$('#txtNetAmount').val())).toFixed(0) +dr_cr_pbalance);
            $('.expenseamount').html(opener.$('#txtExpAmount').val());
            $('.expenseaccount').html(opener.$('#expense_dropdown').find('option:selected').text());
            var m_names = new Array("Jan", "Feb", "Mar",
                "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                "Oct", "Nov", "Dec");
            var d = new Date(opener.$('#current_date').val());

            



            var curr_date = d.getDate();
            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
            $('.date_data').text(curr_date + "-" + m_names[curr_month]+ "-" + curr_year);
			});
			window.print();
		</script>
	</body>
</html>
