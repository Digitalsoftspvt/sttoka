<!DOCTYPE html>
<html>
	<head>
		<title>Cash Slip</title>
		<link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="../../../assets/css/bootstrap-responsive.min.css">
		<style>
			@page{margin:0px auto;width: 380px;}
        @media print {
          body{ width: 380px;margin: 0 auto; }
          .numeric{font-weight: normal;}
         }
			@media screen {
    	.main {margin: 8px 0px 0px 31px !important;}
			}
			@media print {
    	.main { margin: 40px 0px 0px 0px !important;}
			}
		 * { margin: 0; padding: 0; font-family: tahoma; }
		 tr{border: 1px solid !important;}
			body { font-size:20px; }
			th{font-size: 12px; border:none !important;}
			td{border:none !important;}
			table{border:1px solid !important;}
			.token_header{margin:40px 0px 0px 0px !important;}
			.report_title{font-size:18px; font-weight: bold;margin:0px 0px 0px 0px;}
			.fromDate .token_no{font-weight: normal;}
			.footer{  margin: 50px 0px 0px -19px !important; }
			.mid_header{border-bottom:1px solid;}
			.received{border-top:1px solid !important;}
			.trans_table tr td{
				border: 1px solid !important;text-align: center;width: 100px;
			}
			.table>tbody>tr>td{
				  padding: 2px;
			}
		</style>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-4 main" style="width: 350px;">
					<div class="row">
						<div class="col-lg-12">
							<table class="table">
								<tr>
									<td colspan="4">
										<p class="text-center report_title">CASH DEPOSIT SLIP</p>
									</td>
								</tr>
								<tr>
									<td><b>Vr#</b></td>
									<td><span class="vrno"></span></td>
									<td><b>DATE :</b></td>
									<td><span class="dates"></span></td>
								</tr>
								<tr>
									<td colspan="4" style="text-align:center;"><b>CUSTOMER NAME</b></td>
								</tr>
								<tr>
									<td colspan="4" style="text-align:center;" ><span class="customer"></span></td>
								</tr>
							</table>
							<table class="table trans_table" style="margin-top:-21px;" id="cash_table">
								<tbody>
									
								</tbody>
								<tfoot style="margin-top:10px;">
									<tr>
										<td colspan="2"><b>G.TOTAL CASH</b></td>
										<td><span class="total"></span></td>
									</tr>
								</tfoot>
								
							</table>
						</div>
					</div>
				</div><!--end of col-->
			</div><!-- end of row -->
		</div><!-- container-fluid -->

		<script type="text/javascript" src="../../../assets/js/jquery.min.js"></script>
		<script src="../../../assets/js/handlebars.js"></script>
		<script type="text/javascript">
			$(function(){
				var opener = window.opener;
				title="";
                if (opener.$("#cpv").is(":checked"))
                {
                    title="Cash Payment Slip";
                }
                else if (opener.$("#crv").is(":checked"))
                {
                    title="Cash Receipt Slip";
                }
                console.log(title)
                $(".report_title").text(title);
				var srNo = opener.$('#txtVrnoa').val();
				var currentDate = opener.$('#current_date').val();
				var customer = opener.$('#cus_dropdown').find('option:selected').text();
				var total = opener.$('#txtTotal').val();

				var html = '';
				opener.$('#conversionTable').find('tbody tr').each(function(index, elem)
		        {
		            html += "<tr><td>"+$.trim($(elem).find('td.firsrtTd').find('input').val()) + "</td>";
		            html += "<td>"+$.trim($(elem).find('td.secondTd').find('input').val()) + "</td>";
		            html += "<td>"+$.trim($(elem).find('td.thirdTd').find('input').val()) + "</td></tr>";
		        });
		        
		        $("#cash_table tbody").append(html);

				$('.vrno').html(srNo);
				$('.dates').html(currentDate);
				$('.customer').html(customer);
				$('.total').html(total);
				window.print();
			});
		</script>
	</body>
</html>