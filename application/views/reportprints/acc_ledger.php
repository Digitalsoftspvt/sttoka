<!doctype html>
<html>

	<head>
		<link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="../../../assets/css/bootstrap-responsive.min.css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet'>
		<style type='text/css'>
		*{
			font-size: 10px !important;
		}
		@media print {
        .table thead tr td{
          background:rgb(231, 231, 231) !important;
        }
      }
  		@media print {
        .table tfoot tr td{
          background:rgb(231, 231, 231) !important;
        }
      }	
		.table{
			border: 0px !important;
		}
		.table thead tr th {
			border: 1px solid black !important;
			font-weight: normal !important;
		}
		.table thead tr td{
			border: none !important;
			font-weight: normal !important;
		}
		.table tbody tr td{
			border: none !important;
			font-weight: normal !important;
		}
		.table tfoot tr td{
			border: none !important;
			border-top:1px solid !important;
			border-bottom:1px solid !important;
			font-weight: normal !important;
		}
		tbody{
			 /*overflow:auto; height:600px;display:fixed;*/
		}
		.table > thead > tr > th{
			  padding: 0px;
		}
		</style>
	</head>

	<body>
		<div class="container" style="margin-top:30px;">
			<div class="row">
				<div class="col-xs-12">
					
					<div class="row">
						<div class="col-xs-12">
							<span>
								<h3 class="text-center" style="font-weight:bold;font-family: 'Open Sans', sans-serif;"><b>Ledger Account</b></h3>
							</span>
						</div><!-- end of col -->
					</div><!-- end of row -->
					<div class="row">
						<div class="col-xs-4"></div>
						<div class="col-xs-4">
							<h5 class="text-center">
								<b><p>Account Code :<span class="account_code"></span> To <span class="account_code"></span></p></b>
							</h5>
							<h5 class="text-center">
								<b><p><span class="date_from"></span> To <span class="date_to"></span></p></b>
							</h5>
						</div>
						<div class="col-xs-4"></div>
					</div><!-- end of row-fluid -->

					<div class="row">
						<div class="col-lg-12">
							<table class="table table-bordered" id="new_table">
								<thead>
									<tr>
										<th rowspan="2" style="text-align:left;padding-bottom:10px;">Voucher No.</th>
										<th class="vouc" rowspan="2" style="border-right:none !important;text-align:left;padding-bottom:10px;">Voucher Date</th>
							    		<th colspan="2" style="text-align:center;">Debit</th>
							    		<th colspan="2" style="text-align:center;">Credit</th>
							    		<th colspan="2" style="text-align:center;">Balance</th>
							    		<th rowspan="2" style="text-align:center;padding-bottom:10px;">Narration</th>
							  		</tr>
							  		<tr>
							    		<th style="text-align:center;width: 101px;">RS.</th>
							    		<th style="text-align:center;">Foreign</th>
							    		<th style="text-align:center;width: 101px;">RS.</th>
							    		<th style="text-align:center;">Foreign</th>
							    		<th style="text-align:center;width: 101px;">RS.</th>
							    		<th style="text-align:center;">Foreign</th>
							  		</tr>
									<tr style="background:rgb(236, 236, 236);">
										<td class="account_code"></td>
										<td colspan="7" class="party_data"></td>
										<td>Turnover</td>
									</tr>
									
								</thead>
								<tbody class="myrows">
							  		<tr>
										<td colspan="6"><b>Opening Balance :</b></td>
										<td class="opening_balance" style="text-align:right;"></td>
										<td style="text-align:right;">0.00</td>
										<td></td>
									</tr>
								</tbody>
								<tfoot>
									<tr style="background:rgb(236, 236, 236);">
										<td><b>Total For :</b></td>
							  			<td class="account_code"></td>
							    		<td style="text-align:right;width: 101px;" class="total-debit"></td>
							    		<td style="text-align:right;" class="total-fdebit"></td>
							    		<td style="text-align:right;" class="total-credit"></td>
							    		<td style="text-align:right;width: 101px;" class="total-fcredit"></td>
							    		<td style="text-align:right;" class="closing_balance"></td>
							    		<td style="text-align:right;" class="closing_fbalance"></td>
							    		<td class="party_data"></td>
							  		</tr>
								</tfoot>
							</table>
						</div>
					</div>	
				</div><!-- end of col -->
			</div><!-- row-fluid -->
		</div><!--container-fluid -->
		<script type="text/javascript" src="../../../assets/js/jquery.min.js"></script>
		<script src="../../../assets/js/handlebars.js"></script>
		<script type="text/javascript">
		$(function(){
			
			var opener = window.opener;
			        
	          var vrdate = "";
	          var voucher = "";
	          var qty = "";
	          var weight = "";
	          var item_code = "";
	          var rate = "";
	          var amount = "";
	          var ftamount = "";
	          var famount = "";
	          var famount1 = 0;
	          var famount2 = 0;
	          var netDebit = 0;
			  var netCredit = 0;
			  var netRTotal = 0;
			  var netRFTotal = 0;
			  var netRFD = 0;
			  var netRFC = 0;
			  
	         
			
			$.ajax({
				url : 'http://localhost/waheed-engineering/index.php/account/getAccLedgerReports',
				type : 'POST',
				data : {'from': opener.$('#from_date').val(), 'to' : opener.$('#to_date').val(), 'pid' : opener.$('#name_dropdown').find('option:selected').val()},
				dataType : 'JSON',
				success : function(data) {

					// removes all rows
					$('#new-table').find('tbody tr').remove();

					if (data == 'false') {
						//showRunningTotal(_to, _pid);
					} else {
						$(data).each(function(index,elem){
							netDebit += parseFloat(elem.DEBIT);
							netCredit += parseFloat(elem.CREDIT);
							if (index == (data.length - 1)) {
								netRTotal = elem.RTotal;
								netRFTotal = elem.RFTotal;
							};
							var drcr='';
							if (parseFloat(elem.RTotal) > 0) {
								drcr = "Dr";
							} else {
								drcr = "Cr";
							}
							var fdrcr='';
							if (parseFloat(elem.RFTotal) > 0) {
								fdrcr = "Dr";
							} else {
								fdrcr = "Cr";
							}

							if (parseFloat(elem.FAMOUNT) > 0) {
								netRFD += parseFloat(elem.FAMOUNT);
							} else {
								netRFC += parseFloat(elem.FAMOUNT);
							}
							voucher = $.trim(elem.VRNOA + '-' + elem.ETYPE);
			                vrdate = $.trim(elem.VRDATE);
			              	qty = $.trim(elem.DESCRIPTION);
			              	weight = $.trim(elem.DEBIT);
			              	rate = $.trim(elem.CREDIT);
			              	amount = $.trim(elem.RTotal);
			              	ftamount = $.trim(elem.RFTotal);
			              	famount = $.trim(elem.FAMOUNT);

			              	if (weight == '0.00') {
			              		famount1 = '0.00';
			              		famount2 = famount;
			              	}
			              	if (rate == '0.00') {
			              		famount2 = '0.00';
			              		famount1 = famount;
			              	}
			              	
			              var htmls = "<tr>";
	          
				          htmls += "<td>" +  voucher + "</td>";
				          htmls += "<td>" +  (vrdate).substr(0,10) + "</td>";
				          htmls += "<td class='text-right'>" + weight + "</td>";
				          htmls += "<td class='text-right'>" + Math.abs(famount1) +"</td>";
				          htmls += "<td class='text-right'>" + rate + "</td>";
				          htmls += "<td class='text-right'>" + Math.abs(famount2)+"</td>";
				          htmls += "<td class='text-right'>" + Math.abs(amount) + ' ' + drcr + "</td>";
				          htmls += "<td class='text-right'>" + Math.abs(ftamount) + ' ' + fdrcr + "</td>";
				          htmls += "<td class='text-left'>" + qty + "</td>";
				          htmls += "</tr>";
				          $(".myrows").append(htmls);
				          var htmls = "";
				          htmls += "<tr class='tbody'>";
				          htmls += "</tr>";
				          $(".myrows").append(htmls);
			              	
			              	
						});
						var ddrcr='';
						var cdrcr='';
						
						if (parseFloat(netRTotal) > 0) {
							ddrcr = "Dr";
						} else {
							ddrcr = "Cr";
						}
						if (parseFloat(netRFTotal) > 0) {
							cdrcr = "Dr";
						} else {
							cdrcr = "Cr";
						}
						$('.total-debit').html(Math.abs(netDebit));
						$('.total-fdebit').html(Math.abs(netRFD));
						$('.total-credit').html(Math.abs(netCredit));
						$('.total-fcredit').html(Math.abs(netRFC));
						$('.closing_balance').html(Math.abs(netRTotal) + ddrcr);
						$('.closing_fbalance').html(Math.abs(netRFTotal) + cdrcr);
						
					}
				}, error : function(xhr, status, error) {
					console.log(xhr.responseText);
				}
			});

		   
			$('.account_code').text(opener.$('#name_dropdown').find('option:selected').data('accountid'));
			$('.date_from').text((opener.$('#from_date').val()).substr(0,10));
			$('.date_to').html((opener.$('#to_date').val()).substr(0,10));
			$('.party_data').html(opener.$('#name_dropdown').find('option:selected').text());
			// $('.remarks_data').html(opener.$('#txtRemarks').val());
			
			$('.closing_balance').html(opener.$('.running-total').text());
			$('.opening_balance').html(opener.$('.opening-bal').text());

			});
		</script>
	</body>
</html>
