<!doctype html>
<html>

	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="../../../assets/css/bootstrap-responsive.min.css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet'>
		<style type='text/css'>
		.table{
			border: 0px !important;
		}
		.table thead tr th{
			border-bottom: 1px solid black !important;
			border-top: 1px solid black !important;
			font-weight: normal !important;
		}
		.table tbody tr td{
			border-top: 0px !important;
		}
		body{
			direction: rtl;
		}
		.table tfoot tr td{
			border-bottom: 1px solid black !important;
			border-top: 1px solid black !important;
		}
		tbody{
			 overflow:auto; height:600px;display:fixed;
		}
		body, h1, h2, h3, h4, h5, h6, p, span,table,tbody,tr,td,th,tfoot, input, title {
		  font-family: Jameel Noori Nastaleeq, Nafees Web Naskh, Arial, Urdu Naskh Asiatype, Tahoma, Unicode MS !important;
		}
		span{
			font-size: 20px;
		}
		th {
			font-size: 18px !important;
		}
		td {
			font-size: 18px !important;
		}
		h1{
			font-size: 40px;
		}
		.tbody { display: block; height: 320px; overflow: auto;} 
		</style>
	</head>

	<body>
		<div class="container" style="margin-top:30px;">
			<div class="row">
				<div class="col-xs-12">
					
					<div class="row">
						<div class="col-xs-12">
							<span>
								<h1 class="text-center" style="font-weight:bold;font-family: 'Open Sans', sans-serif;"><b>کوٹیشن</b></h1>
							</span>
						</div><!-- end of col -->
					</div><!-- end of row -->
					<div class="row">
						
						<div class="col-xs-6">
							<div class="pull-left">
								<span style="font-weight:bold;font-family: 'Open Sans', sans-serif;" class='pull-right'> <span class='pull-left inv_data' style='width:120px;text-align:left;'></span><b>انوائس نمبر :</b> </span><br>
								<span style="font-weight:bold;font-family: 'Open Sans', sans-serif;" class='pull-left'><b>تاریخ : </b><span class='pull-left date_data' style='width:120px;text-align:left;'></span></span>
							</div>
						</div>
						<div class="col-xs-6">
							<span class="party_data"></span><br>
							<span></span><br>
							<span class="remarks_data"></span>
						</div>
					</div><!-- end of row-fluid -->

					<div class="row">
						<div class="col-lg-12">
							<table class="table">
								<thead style="background:#E8E8E8;">
									<tr style="font-family: 'Open Sans', sans-serif;"> 
										<th style="width:100px;" class="text-right">ائٹم کوڈ</th>
										<th style="width:420px !important;" class="text-right">ائٹم تفصیل</th>
										<th style="width: 100px !important;;" class="text-right">تعداد</th>
										<th style="width:100px !important;;" class="text-right">وزن</th>
										<th style="width:100px !important;;" class="text-right">ریٹ</th>
										<th style="width:120px !important;;" class="text-right">رقم</th>
									</tr>
								</thead>
								<tbody style="min-height:400px fixed !important;" id="myrows">
									
									
								</tbody>
								<tfoot>
									<tr>
										<td colspan="2"><b>کل ٹوٹل:</b></td>
										<td class="text-right total-qty"></td>
										<td class="text-right total-weight"></td>
										<td class="text-right"></td>
										<td class="text-right total-amount"></td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>	
				</div><!-- end of col -->
			</div><!-- row-fluid -->
			<div class="row">
				<div class="col-xs-7">
					<div class="pull-right text-right">
						<span class='text-right'>:سا بقہ بیلنس</span><br>
						<span class='text-right'>:ہ انوائس</span><br>
						<span class='text-right'>:مو جودہ بیلنس</span>
					</div>
				</div>
				<div class="col-xs-5">
					<div class="pull-left" style="direction: ltr !important;">
						<span class='text-left pbalance'></span><br>
						<span class='text-left tbalance'></span><br>
						<span class='text-left cbalance'></span>
					</div>
					
				</div>
			</div>
			<br><br>
			<div class="row-fluid">
				<div class="col-xs-3" style="border-top:1px solid black;text-align:center;">
					<span><b>:Prepared By</b></span>
				</div>
				<div class="col-xs-3 col-xs-offset-2" style="border-top:1px solid black;text-align:center;">
					<span><b>:Checked By</b></span>
				</div>
				<div class="col-xs-3 col-xs-offset-1" style="border-top:1px solid black;text-align:center;">
					<span><b>:Approved By</b></span>
				</div>
			</div>


		</div><!--container-fluid -->
		<script type="text/javascript" src="../../../assets/js/jquery.min.js"></script>
		<script src="../../../assets/js/handlebars.js"></script>
		<script type="text/javascript">
		$(function(){
			
			var opener = window.opener;
			        
	          var item_des = "";
	          var qty = "";
	          var weight = "";
	          var item_code = "";
	          var rate = "";
	          var amount = "";
	         opener.$('#purchase_table').find('tbody tr').each(function(index, elem)
	         {
              item_code += $.trim($(elem).find('td.item_desc').data('item_code')) + "<br>";
              item_des += $.trim($(elem).find('td.item_desc').data('uitem_des')) + "<br>";
              qty += $.trim($(elem).find('td.qty').text()) + "<br>";
              weight +=  $.trim($(elem).find('td.weight').text()) + "<br>";
              rate += $.trim($(elem).find('td.rate').text()) + "<br>";
              amount += $.trim($(elem).find('td.amount').text()) + "<br>";
	         });

	          $('#myrows').empty();
	          var htmls = "<tr>" +
	          "<td> <b>Order</b> </td>" +
	          "<td></td>" +
	          "<td></td>" +
	          "<td></td>" +
	          "<td></td>" +
	          "<td></td>" +
	          "</tr>" +

	          "<tr>"+
	          "<td>" +  item_code + "</td>"+
	          "<td>" +  item_des + "</td>"+
	          "<td class='text-right'>" + qty + "</td>"+
	          "<td class='text-right'>" + weight + "</td>"+
	          "<td class='text-right'>" + rate + "</td>"+
	          "<td class='text-right'>" + amount + "</td>"+
	          "</tr>";
	          $(htmls).appendTo("#myrows");


	          var item_des_less = "";
	          var qty_less = "";
	          var weight_less = "";
	          var item_code_less = "";
	          var rate_less = "";
	          var amount_less = "";
	         opener.$('#purchase_table_less').find('tbody tr').each(function(index, elem)
	         {
              item_code_less += $.trim($(elem).find('td.item_desc').data('item_code')) + "<br>";
              item_des_less += $.trim($(elem).find('td.item_desc').data('uitem_des')) + "<br>";
              qty_less += $.trim($(elem).find('td.qty').text()) + "<br>";
              weight_less += $.trim($(elem).find('td.weight').text()) + "<br>";
              rate_less += $.trim($(elem).find('td.rate').text()) + "<br>";
              amount_less += $.trim($(elem).find('td.amount').text()) + "<br>";
	         });

	          var htmls_less = "<tr>" +
	          "<td> <b>Deduction</b> </td>" +
	          "<td></td>" +
	          "<td></td>" +
	          "<td></td>" +
	          "<td></td>" +
	          "<td></td>" +
	          "</tr>" +

	          "<tr>"+
	          "<td>" +  item_code_less + "</td>"+
	          "<td>" +  item_des_less + "</td>"+
	          "<td class='text-right'>" + qty_less + "</td>"+
	          "<td class='text-right'>" + weight_less + "</td>"+
	          "<td class='text-right'>" + rate_less + "</td>"+
	          "<td class='text-right'>" + amount_less + "</td>"+
	          "</tr>";
	          $(htmls_less).appendTo("#myrows");


	        // var pbalance = parseFloat(opener.$('#party_dropdown').find('option:selected').data('pbalance')).toFixed(2);
	        // var cbalance = (parseFloat(opener.$('#party_dropdown').find('option:selected').data('pbalance')) + parseFloat(opener.$('#txtNetAmount').val())).toFixed(2);

	        var pbalance = parseFloat(opener.$('#txtpre_balance').val()).toFixed(0);
	        var cbalance = (parseFloat(opener.$('#txtpre_balance').val()) + parseFloat(opener.$('#txtNetAmount').val())).toFixed(0);
	        
	        var dr_cr_pbalance = "";
	        var dr_cr_cbalance = "";
			if (pbalance < 0) {
			    dr_cr_pbalance = " Cr";
			}else {
			    dr_cr_pbalance = " Dr";
			}
			if (cbalance < 0) {
			    dr_cr_cbalance = " Cr";
			}else {
			    dr_cr_cbalance = " Dr";
			}


		    var party_data = opener.$('#party_dropdown').find('option:selected').data('uname') + ' ' +opener.$('#party_dropdown').find('option:selected').data('uaddress');
			var party_mobile = opener.$('#party_dropdown').find('option:selected').data('phone');
			// $('.shadowhead').html('Recipe Costing');
			$('.inv_data').text(opener.$('#txtVrnoa').val());
			$('.date_data').text((opener.$('#current_date').val()).substr(0,10));
			$('.party_data').html(party_data);
			$('.noted_by').html(opener.$('#txtNotedBy').val());
			$('.remarks_data').html(party_mobile);
			$('.total-qty').html(opener.$('#txtTotalQty').val());
			$('.total-weight').html(opener.$('#txtTotalWeight').val());
			$('.total-amount').html(parseFloat(opener.$('#txtTotalAmount').val()) - parseFloat(opener.$('#txtPaid').val()));
			//$('.cbalance').html(opener.$('#party_dropdown').find('option:selected').data('pbalance'));
			//$('.tbalance').html(opener.$('#txtNetAmount').val());

            $('.pbalance').html(pbalance + dr_cr_pbalance);
            $('.tbalance').html(opener.$('#txtNetAmount').val());
            $('.cbalance').html(cbalance + dr_cr_cbalance);

        });
		</script>
	</body>
</html>
