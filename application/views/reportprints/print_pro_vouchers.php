<!doctype html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Voucher</title>

	    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
	    <link rel="stylesheet" href="../../assets/css/bootstrap-responsive.min.css">
	    <link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">

		<style>
			 * { margin: 0; padding: 0; font-family: tahoma !important; }
			 body { font-size:16px !important; }
			 p { margin: 0 !important; /* line-height: 17px !important; */ }
			 .field { font-size:25px !important; font-weight: bold !important; display: inline-block !important; width: 180px !important; } 
			 .field1 { font-size:18px !important; font-weight: bold !important; display: inline-block !important; width: 150px !important; } 
			 .voucher-table{ border: none !important; }
			 table { width: 100% !important; border: 2px solid black !important; border-collapse:collapse !important; table-layout:fixed !important; margin-left:1px}
			 th {  padding: 5px !important; }
			 td { /*text-align: center !important;*/ vertical-align: top !important;  }
			 td:first-child { text-align: left !important; }
			 .voucher-table thead th {border-top:1px solid #C0C0C0 !important;font-size:22px !important; border-bottom: 1px solid #C0C0C0 !important; }
			 .voucher-table tbody td{font-size:22px !important;} 
			 .voucher-table tfoot td{font-size:22px !important;} 
			 tfoot {border-top: 1px solid #C0C0C0 !important;border-bottom: 1px solid #C0C0C0; } 
			 .bold-td { font-weight: bold !important; border-bottom: 0px solid black !important;}
			 .nettotal { font-weight: bold !important; font-size: 17px !important; border-top: 1px solid black !important; }
			 .invoice-type { border-bottom: 1px solid black !important; }
			 .relative { position: relative !important; }
			 .signature-fields{ font-size: 66px; border: none !important; border-spacing: 20px !important; border-collapse: separate !important;} 
			 .signature-fields th {border: 0px !important; border-bottom: 1px solid black !important; border-spacing: 10px !important; }
			 .inv-leftblock { width: 280px !important; }
			 .text-left { text-align: left !important; }
			 .text-right { text-align: right !important; }
			 td {font-size: 18px !important; font-family: tahoma !important; line-height: 14px !important; padding: 4px !important; } 
			 .rcpt-header { width: 550px !important; margin: auto !important; display: block !important; }
			 .inwords, .remBalInWords { text-transform: uppercase !important; }
			 .barcode { margin: auto !important; }
			 h3.invoice-type {font-size: 22px !important; line-height: 24px !important;}
			 .extra-detail span { background: #7F83E9 !important; color: white !important; padding: 5px !important; margin-top: 17px !important; display: block !important; margin: 5px 0px !important; font-size: 18px !important; text-transform: uppercase !important; letter-spacing: 1px !important;}
			 .nettotal { color: red !important; font-size: 18px !important;}
			 .remainingBalance { font-weight: bold !important; color: blue !important;}
			 .centered { margin: auto !important; }
			 p { position: relative !important; font-size: 18px !important; }
			 thead th { font-size: 18px !important; font-weight: bold !important; padding: 10px !important; }
			 .fieldvalue { font-size:24px !important; position: absolute !important; width: 200px !important; }
			 .fieldvalue1 { font-size:24px !important; position: absolute !important; width: 1200px !important; }

			 @media print {
			 	.noprint, .noprint * { display: none !important; }
			 }
			 .pl20 { padding-left: 20px !important;}
			 .pl40 { padding-left: 40px !important;}
				
			.barcode { float: right !important; }
			.item-row td { font-size: 18px !important; padding: 10px !important; }
			.footer_company td { font-size: 14px !important; padding: 10px !important; border-top: 1px solid black !important;}

			.rcpt-header { width: 305px !important; margin: 0px !important; display: inline !important; position: absolute !important; top: 0px !important; right: 0px !important; }
			h3.invoice-type { border: none !important; margin: 0px !important; position: relative !important; top: 34px !important; }
			tfoot tr td { font-size: 16px !important; padding: 10px !important;  }
			.nettotal, .subtotal, .vrqty,.vrweight { font-size: 18px !important; font-weight: bold !important;}
			 .shadowhead { border: 0px solid black; padding-bottom: 5px;} 
			 .txtbold { font-weight: bolder; } 
			 .new{border: 1px solid black;width:400px;padding:0px 0px 0px 70px !important; }
			 .sizes{font-size: 24px !important;}
		</style>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12 centered">
					<div class="row-fluid">
						<div class="span4" style="margin-left:450px;">
							<h3 class="text-center shadowhead txtbold new" style="font-size:36px !important;"><?php echo $title;?></h3>
						</div>
					</div>
					<br><br><br>
					<div class="row-fluid relative">
						<div class="span10">
								<div class="block pull-left inv-leftblock" style="width:250px !important; display:inline !important;">
									<p><span class="field" >VrI#</span><span style="border-bottom:1px solid;" class="fieldvalue "><span ><?php echo $vrdetail[0]['vrnoa'];?></span></span></p>
																	
									<p style="margin-top:17px !important;"><span class="field">Received By:</span><span style="border-bottom:1px solid;height:30px !important;" class="fieldvalue1 inv-date"><span style="height:40px !important;"><?php echo substr($vrdetail[0]['received_by'], 0, 10);?></span></span></p>
									
									<p style="margin-top:17px !important;"><span class="field">Remarks:</span><span style="border-bottom:1px solid;" class="fieldvalue1 cust-mobile"><span style=""><?php echo $vrdetail[0]['remarks'];?></span></span></p>
									
								</div>
								
						</div>
						<div class="block pull-right span2" style="margin-top:-160px !important;width:300px !important; float: right; display:inline !important;">
									
									<div class="span12"><span class="field">Date:</span><span class="fieldvalue inv-date" style="border-bottom:1px solid;"><?php echo date('d-M-y', strtotime($vrdetail[0]['vrdate']));;?></span></div>
						</div>
					</div>
					<br>
					<br>
					<br>
					
					<div class="row-fluid">
						<table class="voucher-table">
							<thead>
								<tr>
									<th style=" width: 20px; text-align:left; ">Sr#</th>
									<th style=" width: 40px; text-align:left;">Item Detail</th>
									<th style=" width: 200px; text-align:left; ">Description</th>
									<th style=" width: 40px; text-align:left;">Stock Location</th>
									<th style=" width: 50px; text-align:right;">Qty</th>
									<th style=" width: 30px; text-align:right; ">Weight</th>
									
									<!-- <th style=" width: 40px; text-align:left;">To</th> -->
								</tr>
							</thead>

							<tbody>
								
								<?php 
									$serial = 1;
									$netQty = 0;
									$netAmount=0;
									$netWeight=0;
									foreach ($vrdetail as $row):
										$netQty += abs($row['s_qty']);
										// $netAmount += $row['s_amount'];
										$netWeight += abs($row['weight']);
										$check = abs($row['weight']);
										if($check == 0){
											$check = '-';
										}
										else {
											$check = number_format(abs($row['weight']),2);
										}
								?>
									<tr  class="item-row">
									   <td class='text-left sizes'><?php echo $serial++;?></td>
									   <td class='text-left'><?php echo $row['item_code'];?></td>
									   <td class='text-centre'><?php echo $row['item_name'];?></td>
									   <td class='text-left'><?php echo (($row['dept_name']));?></td>
									   <td class='text-right'><?php echo number_format(abs($row['s_qty']),2);?></td>
									   <td class='text-right'><?php echo $check;?></td>
									   
									   
									</tr>
									
								<?php endforeach?>
							</tbody>
						</table>
						<table>
							<tfoot>
								<tr class="foot-comments">

									<td class="subtotal bold-td text-right" colspan="3"></td>
									<td class="subtotal bold-td text-right" >Total</td>
									<td class="subtotal bold-td text-right"><?php echo number_format($netQty,2);?></td>
									<td class="subtotal bold-td text-right"><?php echo number_format($netWeight,2);?></td>
								</tr>
							</tfoot>
						</table>
					</div>
					<!-- <div class="row-fluid">
						<div class="span12 add-on-detail1" style="margin-top: 10px;">
							<p class="" style="text-transform1: uppercase;">
								<strong>In words: </strong> <span class="inwords"></span><?php echo $amtInWords;?>ONLY <br>
								<br>
								<?php if ( $pre_bal_print==1  ){?>
									<p><span class="field1">Previous Balance:</span><span class="fieldvalue inv-vrnoa"><?php echo number_format($previousBalance,0);?></span></p>
									<p><span class="field1">This Invoice:</span><span class="fieldvalue inv-date"><?php echo number_format($vrdetail[0]['namount'],0);?></span></p>
									<p><span class="field1">Current Balance:</span><span class="fieldvalue cust-name"><?php echo number_format($vrdetail[0]['namount']+$previousBalance,2) ;?></span></p>
								<?php };?>
							</p>
						</div>
					</div> -->
					<!-- End row-fluid -->
					<br>
					<br><br>
					<br><br>
					<br><br>
					<br>
					<div class="row-fluid">
						<div class="span12">
							<table class="signature-fields">
								<thead>
									<tr>
										<th style="text-align:left !important;font-size:24px !important;">Received By:</th>
										<th style="border:none !important;"></th>
										<th style="text-align:left !important;font-size:24px !important;">Checked By:</th>
										<th style="border:none !important;"></th>
										<th style="text-align:left !important;font-size:24px !important;">Approved By:</th>
										
										
									</tr>
								</thead>
							</table>
						</div>
					</div>
					<!-- <div class="row-fluid">
						<p>
							<span class="footer_company">User:<?php echo $user;?></span><br> -->
							<!-- <span class="footer_company">Sofware By: www.digitalsofts.com, Mob:03218661765</span> -->
						<!-- </p>
					</div> -->
				</div>
			</div>
		</div>
	</body>
</html>