<!doctype html>
<html>
<head>
    <link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../assets/css/bootstrap-responsive.min.css">
    <style type='text/css'>
        .table{
            border: 0px !important;
        }
        .table thead tr th{
            border-bottom: 1px solid black !important;
            border-top: 1px solid black !important;
            font-weight: normal !important;
        }
        .table tbody tr td{
            border-top: 0px !important;
        }
        .table tfoot tr td{
            border-bottom: 1px solid black !important;
            border-top: 1px solid black !important;
        }
        tbody{
            overflow:auto; height:600px;display:fixed;
        }
        span{
            font-size: 15px;
        }
        td{
            font-size: 15px;
        }
        th{
            font-size: 15px;
        }
    </style>
</head>

<body>
<div class="container" style="margin-top:30px;">
    <div class="row">
        <div class="col-xs-12">

            <div class="row">
                <div class="col-xs-12">
							<span>
								<h4 class="text-center" style="font-weight:bold;font-family: 'Open Sans', sans-serif;"><b>Quotation</b></h4>
							</span>
                </div><!-- end of col -->
            </div><!-- end of row -->
            <div class="row">
                <div class="col-lg-12">
                    <span class="party_data"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <span class="party_address"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-8"></div>
                <div class="col-xs-4">
                    <div class="pull-right">
                        <span style="font-weight:bold;font-family: 'Open Sans', sans-serif;" class='pull-right'><b>Invoice# :</b> <span class='pull-right inv_data' style='width:120px;text-align:right;'></span></span><br>
                        <span style="font-weight:bold;font-family: 'Open Sans', sans-serif;" class='pull-right'><b>Dated : </b><span class='pull-right date_data' style='width:120px;text-align:right;'></span></span>
                    </div>
                </div>
            </div><!-- end of row-fluid -->

            <div class="row">
                <div class="col-lg-12">
                    <span > </span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <table class="table" id="mytable">
                        <thead style="background:#E8E8E8;">
                        <tr style="font-family: 'Open Sans', sans-serif;">
                            <th style="width:110px;" class="text-left">Item Code</th>
                            <th style="width:550px;" class="text-left">Item Name</th>
                            <th style="width:100px;" class="text-center">UOM</th>
                            <th style="width:100px;" class="text-right">Quantity</th>
                            <th style="width:100px;" class="text-right">Weight</th>
                            <th style="width:100px;" class="text-right">Rate</th>
                            <th style="width:120px;" class="text-right">Amount</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="3"><b>Grand Total:</b></td>
                            <td class="text-right total-qty"></td>
                            <td class="text-right total-weight"></td>
                            <td class="text-right"></td>
                            <td class="text-right total-amount"></td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div><!-- end of col -->
    </div><!-- row-fluid -->
    <div class="row">
        <div class="col-xs-7">
            <div class="pull-right text-right">
                <span class='text-right '>Previouse Balance:</span><br>
                <span class='text-right '>This Invoice:</span><br>
                <span class='text-right '>Current Balance:</span>
            </div>
        </div>
        <div class="col-xs-5">
            <span class="pbalance"></span><br>
            <span class="tbalance"></span><br>
            <span class="cbalance"></span>

        </div>
    </div>
    <br><br>
    <div class="row-fluid">
        <div class="col-xs-3" style="border-top:1px solid black;text-align:center;">
            <span>Prepared By:</span>
        </div>
        <div class="col-xs-3 col-xs-offset-2" style="border-top:1px solid black;text-align:center;">
            <span>Checked By:</span>
        </div>
        <div class="col-xs-3 col-xs-offset-1" style="border-top:1px solid black;text-align:center;">
            <span>Approved By:</span>
        </div>
    </div>


</div><!--container-fluid -->



<script type="text/javascript" src="../../../assets/js/jquery.min.js"></script>
<script src="../../../assets/js/handlebars.js"></script>
<script type="text/javascript">
    // 	$(function(){
    // 		var opener = window.opener;
    // 		var invoice_No = opener.$('#txtVrnoa').val();
    // 		var currentDate = opener.$('#current_date').val();
    // 		var party = opener.$('#party_dropdown').find('option:selected').text();



    // 		var received_by = opener.$('#receivers_list').val();
    // 		var remrks = opener.$('#txtRemarks').val();

    // 		var appType = opener.$('#type_dropdown').find('option:selected').text();

    // 		$('.sr_no').html(invoice_No);
    // 		$('.current_Date').html(currentDate);
    // 		$('.party_drop').html(party);


    // 		$('.rec_by').html(received_by);
    // 		$('.remarks').html(remrks);
    // 		$('.applicantion_Type').html(appType);
    // 	});
</script>
<script type="text/javascript">
    $(function(){

        var opener = window.opener;
        var type = "Order";
        var item_des = "";
        var uom = "";
        var qty = "";
        var weight = "";
        var item_code = "";
        var rate = "";
        var amount = "";
//            rate_less += $.trim((isNaN($(elem).find('td.rate').text())==true)?'':parseInt($(elem).find('td.rate').text())) + "<br>";
        opener.$('#purchase_table').find('tbody tr').each(function(index, elem)
        {
            item_code += $.trim($(elem).find('td.item_desc').data('item_code')) + "<br>";
            item_des += $.trim($(elem).find('td.item_desc').text()) + "<br>";
            uom += $.trim($(elem).find('td.item_desc').data('item_uom')) + "<br>";
            qty += $.trim($(elem).find('td.qty').text()) + "<br>";
            weight += $.trim($(elem).find('td.weight').text()) + "<br>";
            rate += parseFloat( $.trim($(elem).find('td.rate').text())).toFixed(0) + "<br>";
            amount += $.trim($(elem).find('td.amount').text()) + "<br>";
        });

        //$('.myrows').empty();
        var htmls = "<tr>" +
            "<td style='font-size: 17px;'> <b>Order</b> </td>" +
            "<td></td>" +
            "<td></td>" +
            "<td></td>" +
            "<td></td>" +
            "<td></td>" +
            "<td></td>" +
            "</tr>" +

            "<tr>"+
            "<td>" +  item_code + "</td>"+
            "<td>" +  item_des + "</td>"+
            "<td class='text-center'>" + uom + "</td>"+
            "<td class='text-right'>" + qty + "</td>"+
            "<td class='text-right'>" + weight + "</td>"+
            "<td class='text-right'>" + rate + "</td>"+
            "<td class='text-right'>" + amount + "</td>"+
            "</tr>";
        $(htmls).appendTo('#mytable tbody');


        var item_des_less = "";
        var uom_less = "";
        var qty_less = "";
        var weight_less = "";
        var item_code_less = "";
        var rate_less = "";
        var amount_less = "";

        opener.$('#purchase_table_less').find('tbody tr').each(function(index, elem)
        {
            item_code_less += $.trim($(elem).find('td.item_desc').data('item_code')) + "<br>";
            item_des_less += $.trim($(elem).find('td.item_desc').text()) + "<br>";
            uom_less += $.trim($(elem).find('td.item_desc').data('item_uom')) + "<br>";
            qty_less += $.trim($(elem).find('td.qty').text()) + "<br>";
            weight_less += $.trim($(elem).find('td.weight').text()) + "<br>";
            rate_less += $.trim((isNaN($(elem).find('td.rate').text())==true)?'':parseInt($(elem).find('td.rate').text())) + "<br>";
            amount_less += $.trim($(elem).find('td.amount').text()) + "<br>";
        });

        //$('.myrows').empty();
        var htmls_less = "<tr>" +
            "<td style='font-size: 17px;'> <b>Deduction</b> </td>" +
            "<td></td>" +
            "<td></td>" +
            "<td></td>" +
            "<td></td>" +
            "<td></td>" +
            "<td></td>" +
            "</tr>" +

            "<tr>"+
            "<td>" +  item_code_less + "</td>"+
            "<td>" +  item_des_less + "</td>"+
            "<td class='text-center'>" + uom_less + "</td>"+
            "<td class='text-right'>" + qty_less + "</td>"+
            "<td class='text-right'>" + weight_less + "</td>"+
            "<td class='text-right'>" + rate_less + "</td>"+
            "<td class='text-right'>" + amount_less + "</td>"+
            "</tr>";
        $(htmls_less).appendTo("#mytable tbody");
        // var htmls = "";
        // htmls += "<tr class='tbody'>";
        // htmls += "</tr>";
        // $(".myrows").append(htmls);

        var party_data = opener.$('#party_dropdown').find('option:selected').text();
        var party_address = opener.$('#party_dropdown').find('option:selected').data('address');
        var cbalance = (parseFloat(opener.$('#txtpre_balance').val()) + parseFloat(opener.$('#txtNetAmount').val())).toFixed(2);
        var pbalance = parseFloat(opener.$('#txtpre_balance').val()).toFixed(2);

        var dr_cr_cbalance = "";
        var dr_cr_pbalance = "";
        if (cbalance < 0) {
            dr_cr_cbalance = " Cr";
        }else {
            dr_cr_cbalance = " Dr";
        }
        if (pbalance < 0) {
            dr_cr_pbalance = " Cr";
        }else {
            dr_cr_pbalance = " Dr";
        }


        // $('.shadowhead').html('Recipe Costing');
        $('.inv_data').text(opener.$('#txtVrnoa').val());
        $('.date_data').text((opener.$('#current_date').val()).substr(0,10));
        $('.party_data').html(party_data);
        $('.party_address').html(party_address);
        //$('.noted_by').html(opener.$('#txtNotedBy').val());
        //$('.remarks_data').html(opener.$('#txtRemarks').val());
        $('.total-qty').html(opener.$('#txtTotalQty').val());
        $('.total-weight').html(opener.$('#txtTotalWeight').val());
        $('.total-amount').html(opener.$('#txtNetAmount').val());
        $('.pbalance').html(pbalance + dr_cr_pbalance);
        $('.tbalance').html(opener.$('#txtNetAmount').val());
        $('.cbalance').html(cbalance + dr_cr_cbalance);


    });
</script>
</body>
</html>
