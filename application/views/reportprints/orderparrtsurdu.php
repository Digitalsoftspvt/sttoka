<!doctype html>
<html>

	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
	    <link rel="stylesheet" href="../../assets/css/bootstrap-responsive.min.css">
	    <link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet'>
		<style type='text/css'>
		.table{
			border: 0px !important;
		}
		.table thead tr th{
			border-bottom: 1px solid black !important;
			border-top: 1px solid black !important;
			font-weight: normal !important;
		}
		.table tbody tr td{
			border-top: 0px !important;
		}
		body{
			direction: rtl;
		}
		.table tfoot tr td{
			border-bottom: 1px solid black !important;
			border-top: 1px solid black !important;
		}
		tbody{
			 overflow:auto; height:600px;display:fixed;
		}
		body, h1, h2, h3, h4, h5, h6, p, span,table,tbody,tr,td,th,tfoot, input, title {
		  font-family: Jameel Noori Nastaleeq, Nafees Web Naskh, Arial, Urdu Naskh Asiatype, Tahoma, Unicode MS !important;
		}
		span{
			font-size: 25px;
		}
		th {
			font-size: 25px !important;
		}
		td {
			font-size: 25px !important;
		}
		h1{
			font-size: 50px;
		}
		.tbody { display: block; height: 320px; overflow: auto;} 
		.newone{
			border-bottom: 1px solid black;
			display: block;
			width: 70%;
			position: relative;
			top: -25px;
			right: 60px
		}
		.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td{
			padding: 1px !important;
		}
		.newone1{
			border-bottom: 1px solid black;
			display: block;
			width: 144%;
			position: relative;
			top: -10px;
			right: 60px
		}
		</style>
	</head>

	<body>
		<div class="container" style="margin-top:30px;">
			<div class="row">
				<div class="col-xs-12">
					
					<div class="row">
						<div class="col-xs-12">
							<span>
								<h1 class="text-center" style="font-weight:bold;font-family: 'Open Sans', sans-serif;"><b>گیٹ پاس</b></h1>
							</span>
						</div><!-- end of col -->
					</div><!-- end of row -->
					<div class="row">
						
						<div class="col-xs-4">
							<div class="pull-left">
								<span style="font-weight:bold;font-family: 'Open Sans', sans-serif;" class='pull-left'><b>تاریخ </b><span class='pull-left dates' style='width:130px;margin-right:20px;text-align:right;'>2014 / 11 / 01</span></span><br>
								<span style="font-weight:bold;font-family: 'Open Sans', sans-serif;" class='pull-left'> <span class='pull-left orders' style='width:130px;margin-right:20px;text-align:right;'></span><b>آرڈر نمبر</b> </span><br>
								<span style="font-weight:bold;font-family: 'Open Sans', sans-serif;" class='pull-right'> <span class='pull-left vrno' style='width:130px;margin-right:20px;text-align:right;'></span><b>گیٹ پاس نمبر</b> </span>
								<br>
								
								
							</div>
						</div>
						<div class="col-xs-8"><br><br>
							<span><span>پارٹی</span><span class='newone linebrk1'></span></span>
							<span><span>تفصیل</span><span class='newone1 linebrk'></span></span>
							<span><span>گڈز</span><span class='newone1 transporterName'></span></span>

						</div>
					</div><!-- end of row-fluid -->

					<div class="row">
						<div class="col-lg-12">
							<table class="table" id='mytable'>
								<thead style="background:#E8E8E8;" class="myhead">
									<tr style="font-family: 'Open Sans', sans-serif;"> 
										<th style="width:100px;" class="text-right">آئٹم کوڈ</th>
										<th style="width:420px !important;" class="text-right">آئٹم تفصیل</th>
										<th style="width:200px !important;" class="text-right">گودام</th>
										<th style="width: 100px !important;;" class="text-right">تعداد</th>
										<th style="width:100px !important;;" class="text-right">وزن</th>
									</tr>
								</thead>
								<tbody style="min-height:400px fixed !important;" class="myrows">
								
								</tbody>
								<tfoot>
									<tr>
										<td colspan="3"><b>کل ٹوٹل:</b></td>
										<td class="text-right qty" style="font-weight: bold;"></td>
										<td class="text-right weight" style="font-weight: bold;"></td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>	
				</div><!-- end of col -->
			</div><!-- row-fluid -->
			<!-- <div class="row">
				<div class="col-xs-7">
					<div class="pull-right text-right">
						<span class='text-right'>:سا بقہ بیلنس</span><br>
						<span class='text-right'>:ہ انوائس</span><br>
						<span class='text-right'>:مو جودہ بیلنس</span>
					</div>
				</div>
				<div class="col-xs-5">
					<div class="pull-left">
						<span class='text-left'>00000</span><br>
						<span class='text-left'>00000</span><br>
						<span class='text-left'>00000</span>
					</div>
					
				</div>
			</div> -->
			<br><br><br><br>
			<div class="row-fluid">
				<div class="col-xs-3" style="border-top:1px solid black;text-align:center;">
					<span><b>:Prepared By</b></span>
				</div>
				<div class="col-xs-3 col-xs-offset-2" style="border-top:1px solid black;text-align:center;">
					<span><b>:Checked By</b></span>
				</div>
				<div class="col-xs-3 col-xs-offset-1" style="border-top:1px solid black;text-align:center;">
					<span><b>:Approved By</b></span>
				</div>
			</div>
		</div><!--container-fluid -->
		<script type="text/javascript" src="../../../assets/js/jquery.min.js"></script>
		<script src="../../../assets/js/handlebars.js"></script>
		<script type="text/javascript">
		$(function(){
			
			var opener = window.opener;
			
	       
	        var html = '';
	        var totaqty = 0;
	        html += "<tr><td colspan='5'><b>پارٹس</b></td></tr>";
	       	opener.$('#purchase_table').find('tbody tr').each(function(index, elem)
	        {
	        	var type = $.trim($(elem).find('td.type').text());
	        	if(type == 'parts'){

		            html += "<tr><td style='font-size: 18px !important;'>" + $.trim($(elem).find('td.item_code').text()) + "</td>";
		            html += "<td>" + $.trim($(elem).find('td.item_desc').data('uitem_des')) + "</td>";
		            html += "<td>" + opener.$('#dept_dropdown').find('option:selected').text() + "</td>";
		            html += "<td class='text-right'>" + $.trim($(elem).find('td.qty').text()) + "</td>";
		            html += "<td class='text-right'>" + $.trim($(elem).find('td.weight').text()) + "</td></tr>";
	            }
	        });
	        
	        $("#mytable tbody").append(html);

	        var htmls = '';
	        htmls += "<tr><td colspan='5'><b>سپیئر پارٹس</b></td></tr>";

	       	opener.$('#purchase_table').find('tbody tr').each(function(index, elem)
	        {
	        	var type = $.trim($(elem).find('td.type').text());
	        	if(type == 'spare parts'){

		            htmls += "<tr><td style='font-size: 18px !important;  '>" + $.trim($(elem).find('td.item_code').text()) + "</td>";
		            htmls += "<td>" + $.trim($(elem).find('td.item_desc').data('uitem_des')) + "</td>";
		            htmls += "<td>" + opener.$('#dept_dropdown').find('option:selected').text() + "</td>";
		            htmls += "<td class='text-right'>" + $.trim($(elem).find('td.qty').text()) + "</td>";
		            htmls += "<td class='text-right'>" + $.trim($(elem).find('td.weight').text()) + "</td></tr>";

	        	}

	        });

	        $("#mytable tbody").append(htmls);




	        var htmls_less = '';
	        htmls_less += "<tr><td colspan='5'><b>كم آئٹمز</b></td></tr>";

	       	opener.$('#purchase_table_less').find('tbody tr').each(function(index, elem)
	        {
		        htmls_less += "<tr><td>" + $.trim($(elem).find('td.item_code').text()) + "</td>";
		        htmls_less += "<td>" + $.trim($(elem).find('td.item_desc').data('uitem_des')) + "</td>";
		        htmls_less += "<td>" + opener.$('#dept_dropdown').find('option:selected').text() + "</td>";
		        htmls_less += "<td class='text-right'>" + $.trim($(elem).find('td.qty').text()) + "</td>";
		        htmls_less += "<td class='text-right'>" + $.trim($(elem).find('td.weight').text()) + "</td></tr>";


	        });

	        $("#mytable tbody").append(htmls_less);








	        $('.qty').html(opener.$('#txtTotalQty').val());
	        $('.weight').html(opener.$('#txtTotalWeight').val());
	        $('.linebrk1').html(opener.$('#party_dropdown').find('option:selected').data('uname'));
	        $('.linebrk').html(opener.$('#txtRemarks').val());
	        $('.transporterName').html(opener.$('#transporter_dropdown').find('option:selected').data('uname'));

	        $('.orders').html(opener.$('#txtOrderNo').val());
	        $('.vrno').html(opener.$('#txtVrnoa').val());
	        $('.dates').html(opener.$('#current_date').val());

			});
		</script>
	</body>
</html>
