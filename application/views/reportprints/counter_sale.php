<!DOCTYPE html>
<html>
	<head>
		<title>Counter Sale</title>
		<link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="../../../assets/css/bootstrap-responsive.min.css">
		<style>
			@page{margin:0px auto;width: 380px;}
        @media print {
          /*body{ width: 380px;margin: 0 auto; }*/
          body{ width: 390px;margin: 0 auto; }
          .numeric{font-weight: normal;}
         }
			@media screen {
    	.main {margin: 8px 0px 0px 31px !important;}
			}
			@media print {
    	.main { margin: 0px 0px 0px -50px !important;}
			}
		 * { margin: 0; padding: 0; font-family: tahoma; }
			body { font-size:20px; }
			th{font-size: 12px; border:none !important;}
			td{border:none !important;}
			table{border:none !important;}
			.token_header{margin:40px 0px 0px 0px !important;}
			.report_title{font-size:18px; font-weight: bold;margin:15px 0px 0px 15px;}
			.fromDate .token_no{font-weight: normal;}
			.footer{  margin: 4px 0px 0px -19px !important; }
			.mid_header{border-bottom:1px solid;}
			.table>tbody>tr>td{padding: 0px;}
			.received{border-top:1px solid !important;}
		</style>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-4 main" style="width: 420px;">
					<div class="row">
						<div class="col-lg-12">
							<p class="text-center report_title"><span style="text-decoration:underline;">Counter Sale Voucher</span></p>
						</div>
					</div><br>
					<div class="row">
						<div class="col-lg-12">
							<table class="table">
								<tr>
									<td><b>Sr#</b> &nbsp;&nbsp;&nbsp; <b class="sr_no"></b></td>
									
									<td style="text-align: right !important;" ><b>Date :</b></td>
									<td class="current_Date" style="text-align: left;"></td>
								</tr>
								<tr>
									<td><b>Account :</b></td>
									<td class="party_drop" colspan="3" style="border-bottom:1px solid !important;"></td>
								</tr>
								<tr>
									<td style="width: 140px;"><b>Customer :</b></td>
									<td class="cust_drop" colspan="3" style="border-bottom:1px solid !important;"></td>
								</tr>
								<tr>
									<td style="width: 140px;"><b>Phone :</b></td>
									<td class="phone_drop" colspan="3" style="border-bottom:1px solid !important;"></td>
								</tr>
								<tr>
									<td><b>Location :</b></td>
									<td class="location_drop" colspan="3" style="border-bottom:1px solid !important;"></td>
								</tr>
							</table>
						</div><!-- end of col -->
					</div><!-- end of row -->
					<div class="row">
						<div class="col-lg-12">
							<table class="table" id="add">
								<thead style="border-top:1px solid #A7ABA4;border-bottom:1px solid #A7ABA4;">
									<tr>
										<th class="text-left" >Item</th>
										<th class="text-right">Qty</th>
										<th class="text-right">Weight</th>
										<th class="text-right" style="width:50px;">Rate</th>
										<th class="text-right">Amount</th>
									</tr>
									
								</thead>
								<tbody>
									
									
								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<h3 style="text-align:center;">This Invoice: <b><span class="thisInvioce"></span></b></h3>
							
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<table class="table">
								<tr> 
									<td> <strong style="border-bottom:1px solid;padding: 4px 144px 0px 0px;">Received By :</strong></td>
									<td></td>
									<td class="applicant_name"></td>
								</tr>
							
							</table>
						</div><!-- end of col -->
					</div><!-- end of row -->

					<div class='row footer'>
						<div class="col-lg-12">
							<div class="foter_style">
								<p class="text-center"><strong>Thank You For Shopping Here</strong> </p>
								<p class="text-center" style="margin-top:-10px; font-size: 16px;"><strong>Ph# : </strong> 041-8543827,8714167,8541363,8541938
							</div>
						</div>
					</div>
				</div><!--end of col-->
			</div><!-- end of row -->
		</div><!-- container-fluid -->

		<script type="text/javascript" src="../../../assets/js/jquery.min.js"></script>
		<script src="../../../assets/js/handlebars.js"></script>
		<script type="text/javascript">
			$(function(){

				var opener = window.opener;
				var base_url = opener.$('#hfBaseUrl').val();
				var srNo = opener.$('#txtVrnoa').val();
				$.ajax({

					url : base_url + 'index.php/countersale/fetch',
					type : 'POST',
					data : { 'vrnoa' : srNo , 'company_id': opener.$('#cid').val(),'etype':'counter_sale'},
					dataType : 'JSON',
					success : function(data) {

						
						if (data === 'false') {
							alert('No data found.');
							//general.reloadWindow();
						} else {
							//console.log(data);
							var currentDate = data[0]['vrdate'].substr(0, 10);
							var received_by = data[0]['received_by'];
							var remrks = data[0]['remarks'];
							// var godown11 = opener.$('.godown').text();
							var Customer_list = data[0]['customer_name'];
							var phone_number = data[0]['customer_mobile'];
							var txtNetAmounts = opener.$('#txtNetAmounts').val();

							var party = data[0]['party_name'];
							var appType = opener.$('#type_dropdown').find('option:selected').text();

							var descriptionOfGoods = "";
							var qty = '';
							var weight = '';
							var rate = '';
							var amount = '';

							var html = '';
							var qty = 0;
							var weight = 0;
							var amount = 0;
							var godown11='';
							if(parseFloat(opener.$('#txtTotalQty').val())!=0){
								html += "<tr><td><b>IN</b></td><td></td><td></td><td></td><td></td></tr>";
								//html += "<tr><td><b><br></td><td></td><td></td><td></td><td></td></tr>";
							}

							$.each(data, function(index, elem) {
					     		
					     		if(elem.type && elem.type == 'add'){
									
									if(parseFloat($.trim(elem.qty))!=0 ){


							            html += "<tr><td>"+$.trim(elem.item_name) + "</td>";
							            html += "<td class='text-right' style='font-size: 17px;'>"+$.trim(elem.qty) + "</td>";
							            html += "<td class='text-right' style='font-size: 17px;'>"+parseFloat($.trim(elem.weight)).toFixed(2) + "</td>";
							            html += "<td class='text-right' style='font-size: 17px;'>"+parseFloat($.trim(elem.rate)).toFixed(0) + "</td>";
							            html += "<td class='text-right'>"+parseFloat($.trim(elem.amount)).toFixed(0) + "</td></tr>";
						            }
						            //godown11 += $.trim($(elem).find('td.godown').text());
						            qty += parseFloat($.trim(elem.qty));
						            weight += parseFloat($.trim(elem.weight));
						            amount += parseFloat($.trim(elem.amount));
					     		}
					        });

					        if(parseFloat(opener.$('#txtTotalQty').val())!=0){
					        	html += "<tr><td></td><td class='text-right'><b>"+ qty.toFixed(0) +"</b></td><td class='text-right'><b>"+ weight.toFixed(2) +"</b></td><td></td><td class='text-right'><b>"+ amount.toFixed(0) +"</b></td></tr>";
					    	}
					        $("#add tbody").append(html);

					        var qty1 = 0;
							var weight1 = 0;
							var amount1 = 0;
							var html1 = '';
							if(parseFloat(opener.$('#txtTotalQtyLess').val())!=0){
								html1 += "<tr><td><br></td><td></td><td></td><td></td><td></td></tr>";
								html1 += "<tr><td><b>OUT</b></td><td></td><td></td><td></td><td></td></tr>";
							}

							$.each(data, function(index, elem) {
					      	
					      		if(elem.type && elem.type == 'less'){
						        	if (parseFloat( $.trim(elem.qty))!=0 ){

							            html1 += "<tr><td>"+$.trim(elem.item_name) + "</td>";
							            html1 += "<td class='text-right' style='font-size: 17px;'>"+$.trim(Math.abs(elem.qty)) + "</td>";
							            html1 += "<td class='text-right' style='font-size: 17px;'>"+parseFloat($.trim(elem.weight)).toFixed(2) + "</td>";
							            html1 += "<td class='text-right' style='font-size: 17px;'>"+parseFloat($.trim(elem.rate)).toFixed(0) + "</td>";
							            html1 += "<td class='text-right'>"+parseFloat($.trim(elem.amount)).toFixed(0) + "</td></tr>";
						        	}
						        	if(index == 0){

						        		godown11 += $.trim(elem.dept_name);
						        	}
						        	
						            qty1 += parseFloat($.trim(Math.abs(elem.qty)));
						            weight1 += parseFloat($.trim(elem.weight));
						            amount1 += parseFloat($.trim(elem.amount));
						        }
					        });

					        if(parseFloat(opener.$('#txtTotalQtyLess').val())!=0){
					        	html1 += "<tr><td></td><td class='text-right'><b>"+ qty1.toFixed(0) +"</b></td><td class='text-right'><b>"+ weight1.toFixed(2) +"</b></td><td></td><td class='text-right'><b>"+ amount1.toFixed(0) +"</b></td></tr>";	
					        }
					        
					        html1 += "<tr style='border-top:1px solid;border-bottom:1px solid;'><td><b>Sub Total</b></td><td class='text-right'></td><td class='text-right'></td><td></td><td class='text-right'><b>"+ (amount1-amount).toFixed(0) +"</b></td></tr>";
					        $("#add tbody").append(html1);


							$('.sr_no').html(srNo);
							$('.current_Date').html(currentDate);
							$('.rec_by').html(received_by);
							$('.remarks').html(remrks);
							$('.party_drop').html(party);
							$('.cust_drop').html(Customer_list);
							$('.location_drop').html(godown11);
							$('.phone_drop').html(phone_number);
							
							$('.thisInvioce').html(parseFloat(txtNetAmounts).toFixed(0));
							$('.applicantion_Type').html(appType);
							window.print();
						}

					}, error : function(xhr, status, error) {
						console.log(xhr.responseText);
					}
				});
				/*var currentDate = opener.$('#current_date').val();
				var received_by = opener.$('#receivers_list').val();
				var remrks = opener.$('#txtRemarks').val();
				// var godown11 = opener.$('.godown').text();
				var Customer_list = opener.$('#Customer_list').val();
				var txtNetAmounts = opener.$('#txtNetAmounts').val();

				var party = opener.$('#party_dropdown').find('option:selected').text();
				var appType = opener.$('#type_dropdown').find('option:selected').text();

				var descriptionOfGoods = "";
				var qty = '';
				var weight = '';
				var rate = '';
				var amount = '';

				var html = '';
				var qty = 0;
				var weight = 0;
				var amount = 0;
				var godown11='';
				if(parseFloat(opener.$('#txtTotalQty').val())!=0){
				html += "<tr><td><b>IN</b></td><td></td><td></td><td></td><td></td></tr>";
				//html += "<tr><td><b><br></td><td></td><td></td><td></td><td></td></tr>";
				}
				opener.$('#purchase_table').find('tbody tr').each(function(index, elem)
		        {
		        	if(parseFloat($.trim($(elem).find('td.qty').text()))!=0 ){
			            html += "<tr><td>"+$.trim($(elem).find('td.item_desc').text()) + "</td>";
			            html += "<td class='text-right' style='font-size: 17px;'>"+$.trim($(elem).find('td.qty').text()) + "</td>";
			            html += "<td class='text-right' style='font-size: 17px;'>"+$.trim($(elem).find('td.weight').text()) + "</td>";
			            html += "<td class='text-right' style='font-size: 17px;'>"+parseFloat($.trim($(elem).find('td.rate').text())).toFixed(0) + "</td>";
			            html += "<td class='text-right'>"+parseFloat($.trim($(elem).find('td.amount').text())).toFixed(0) + "</td></tr>";
		            }
		            //godown11 += $.trim($(elem).find('td.godown').text());
		            qty += parseFloat($.trim($(elem).find('td.qty').text()));
		            weight += parseFloat($.trim($(elem).find('td.weight').text()));
		            amount += parseFloat($.trim($(elem).find('td.amount').text()));
		        });
		        if(parseFloat(opener.$('#txtTotalQty').val())!=0){
		        	html += "<tr><td></td><td class='text-right'><b>"+ qty.toFixed(0) +"</b></td><td class='text-right'><b>"+ weight.toFixed(2) +"</b></td><td></td><td class='text-right'><b>"+ amount.toFixed(0) +"</b></td></tr>";
		    	}
		        $("#add tbody").append(html);

		        var qty1 = 0;
				var weight1 = 0;
				var amount1 = 0;
				var html1 = '';
				if(parseFloat(opener.$('#txtTotalQtyLess').val())!=0){
					html1 += "<tr><td><br></td><td></td><td></td><td></td><td></td></tr>";
					html1 += "<tr><td><b>OUT</b></td><td></td><td></td><td></td><td></td></tr>";
				}
				opener.$('#purchase_table_less').find('tbody tr').each(function(index, elem)
		        {	
		        	if (parseFloat( $.trim($(elem).find('td.qty').text()))!=0 ){

			            html1 += "<tr><td>"+$.trim($(elem).find('td.item_desc').text()) + "</td>";
			            html1 += "<td class='text-right' style='font-size: 17px;'>"+$.trim($(elem).find('td.qty').text()) + "</td>";
			            html1 += "<td class='text-right' style='font-size: 17px;'>"+$.trim($(elem).find('td.weight').text()) + "</td>";
			            html1 += "<td class='text-right' style='font-size: 17px;'>"+parseFloat($.trim($(elem).find('td.rate').text())).toFixed(0) + "</td>";
			            html1 += "<td class='text-right'>"+parseFloat($.trim($(elem).find('td.amount').text())).toFixed(0) + "</td></tr>";
		        	}
		        	if(index == 0){

		        		godown11 += $.trim($(elem).find('td.godown').text());
		        	}
		        	
		            qty1 += parseFloat($.trim($(elem).find('td.qty').text()));
		            weight1 += parseFloat($.trim($(elem).find('td.weight').text()));
		            amount1 += parseFloat($.trim($(elem).find('td.amount').text()));
		        });
		        
		        if(parseFloat(opener.$('#txtTotalQtyLess').val())!=0){
		        	html1 += "<tr><td></td><td class='text-right'><b>"+ qty1.toFixed(0) +"</b></td><td class='text-right'><b>"+ weight1.toFixed(2) +"</b></td><td></td><td class='text-right'><b>"+ amount1.toFixed(0) +"</b></td></tr>";	
		        }
		        
		        html1 += "<tr style='border-top:1px solid;border-bottom:1px solid;'><td><b>Sub Total</b></td><td class='text-right'></td><td class='text-right'></td><td></td><td class='text-right'><b>"+ (amount1-amount).toFixed(0) +"</b></td></tr>";
		        $("#add tbody").append(html1);


				$('.sr_no').html(srNo);
				$('.current_Date').html(currentDate);
				$('.rec_by').html(received_by);
				$('.remarks').html(remrks);
				$('.party_drop').html(party);
				$('.cust_drop').html(Customer_list);
				$('.location_drop').html(godown11);
				
				$('.thisInvioce').html(txtNetAmounts);
				$('.applicantion_Type').html(appType);
				window.print();*/
			});
		</script>
	</body>
</html>