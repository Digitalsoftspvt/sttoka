<!doctype html>
<html>

	<head>
		<link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="../../../assets/css/bootstrap-responsive.min.css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet'>
		<style type='text/css'>
			/*.table thead tr th{
				border: none !important;
			}
*/
			.bodr-botm{
				border-bottom: 1px solid black;
				width: 150%;
			}
			.bodr-botm1{
				width:170px;border-bottom:1px solid;display:block;
			}
			.bodr-botm2{
				width:170px;
				margin-left: 30px;
				border-bottom:1px solid;display:block;
			}
			.wid_td{
				width: 15% !important;
			}
			.new1{
				border-bottom: 0px !important;
				width: 10%;
			}
			thead tr th{
				border-bottom: 1px solid #ddd !important;
				border-top: 1px solid #ddd !important;
			}
			tfoot tr td{
				border-bottom: 1px solid #ddd !important;
				border-top: 1px solid #ddd !important;
			}
			.width_100px{
				width: 100px;
			}
		</style>
	</head>

	<body>
		<div class="container" style="margin-top:40px;">
			<div class="row">
				<div class="col-xs-12">
					<h3 class='text-center' ><span style="border-bottom:1px solid black;">Recipe Costing</span></h3>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<table>
						<tr class='botm-margn'>
							<td class='wid_td'><b>Vr#</b></td>
							<td><span class='bodr-botm1'></span></td>
							<td class='pull-right wid_td'><b>Date:</b></td>
							<td><span class='bodr-botm2' style=''></span></td>
						</tr>
						<br>
						<tr class='botm-margn'>
							<td class='wid_td'><b>Finish Item:</b></td>
							<td colspan="3" class='bodr-botm finish_data'></td>
						</tr>
						<tr class='botm-margn'>
							<td class='wid_td'><b>Noted By:</b></td>
							<td colspan="3" class='bodr-botm noted_by'></td>
						</tr>
						<div></div>
						<tr class='botm-margn'>
							<td class='wid_td'><b>Remarks:</b></td>
							<td colspan="3" class='bodr-botm remarks_by'></td>
						</tr>
					</table>
				</div>
			</div>
			<br>
			<br>
			<div class="row">
				<div class="col-xs-12">
					<div class="row">
						<div class="col-lg-12">
							<table class="table">
								<thead class="myhead">
									
								</thead>
								<tbody class="myrows">
									

								</tbody>
								<tfoot>
									<tr>
										<td colspan="3"></td>
										<td class='text-right total-qty'></td>
										<td class='text-right total-weight'></td>
										<td class='text-right'></td>
										<td class='text-right'></td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>	
				</div><!-- end of col -->
			</div><!-- row-fluid -->
			<div class="row">
				<div class="col-xs-12">
					<div class="pull-right">
						<table>
							<tbody>
								<tr>
									<td class='width_100px'><b>Sub Total:</b></td>
									<td class='width_100px text-right'>0.00</td>
								</tr>
								<tr>
									<td class='width_100px'><b>Finish Qty:</b></td>
									<td class='width_100px text-right finish_qty'></td>
								</tr>
								<tr>
									<td class='width_100px'><b>Finish Weight:</b></td>
									<td class='width_100px text-right finish_weight'></td>
								</tr>
								<tr>
									<td class='width_100px'><b>Cost Price:</b></td>
									<td class='width_100px text-right'>0.00</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div style="border-bottom:1px solid #ddd;border-top:1px solid #ddd;">
						<table>
							<tr>
								<td style="width:80px;"><b>Total:</b></td>
								<td>zero Only</td>
							</tr>
						</table>
					</div>
				</div>
			</div>

		</div><!--container-fluid -->
		<script type="text/javascript" src="../../../assets/js/jquery.min.js"></script>
		<script src="../../../assets/js/handlebars.js"></script>
		<script type="text/javascript">
		$(function(){
			
			
			var opener = window.opener;
			
				
			var etype = opener.$('.page_title').text();
			
	        var po = "";
	        var item_des = "";
	        var uoms = "";
	        var qty = "";
	        var weight = "";
	        opener.$('#recipe_table').find('thead tr').each(function(index, elem)
	         {
              item_des += $.trim($(elem).find('th.hitem').text()) + "<br>";
              uoms += $.trim($(elem).find('th.huom').text()) + "<br>";
              qty += $.trim($(elem).find('th.hqty').text()) + "<br>";
              weight += $.trim($(elem).find('th.hweight').text()) + "<br>";
	         });

	          var html = "<tr>";
	          
	          html += "<th style='width:90px;' class='text-left'>Sr#</th>";
	          html += "<th class='text-left'>" +  item_des + "</th>";
	          html += "<th style='width: 110px;' class='text-right'>" + uoms + "</th>";
	          html += "<th style='width:120px;' class='text-right'>"+ qty + "</th>";
	          html += "<th style='width:120px;' class='text-right'>" + weight + "</th>";
	          html += "<th style='width:120px;' class='text-right'>Rate<th>";
	          html += "<th style='width:120px;' class='text-right'>Amount</th>";
	          html += "</tr>";
								
	          $(".myhead").append(html);
	          var item_des = "";
	          var uoms = "";
	          var qty = "";
	          var weight = "";
	          var srno = "";
	        opener.$('#recipe_table').find('tbody tr').each(function(index, elem)
	         {
              srno += $.trim($(elem).find('td.srno').text()) + "<br>";
              item_des += $.trim($(elem).find('td.item_desc').text()) + "<br>";
              uoms += $.trim($(elem).find('td.uom').text()) + "<br>";
              qty += $.trim($(elem).find('td.qty').text()) + "<br>";
              weight += $.trim($(elem).find('td.weight').text()) + "<br>";
	         });

	          $('.myrows').empty();
	          var htmls = "<tr>";
	          
	          htmls += "<td>" +  srno + "</td>";
	          htmls += "<td>" +  item_des + "</td>";
	          htmls += "<td class='text-right'>" + uoms + "</td>";
	          htmls += "<td class='text-right'>" + qty + "</td>";
	          htmls += "<td class='text-right'>" + weight + "</td>";
	          htmls += "<td class='text-right'>-</td>";
	          htmls += "<td class='text-right'>-</td>";
	          htmls += "</tr>";
	          $(".myrows").append(htmls);
		    

			
			$('.shadowhead').html('Recipe Costing');
			$('.bodr-botm1').html(opener.$('#txtId').val());
			$('.bodr-botm2').html((opener.$('#vrdate').val()).substr(0,10));
			$('.finish_data').html(opener.$('#recipe_dropdown').find('option:selected').text());
			$('.noted_by').html(opener.$('#txtNotedBy').val());
			$('.remarks_by').html(opener.$('#txtRemarks').val());
			$('.finish_qty').html(opener.$('#txtFQty').val());
			$('.finish_weight').html(opener.$('#txtFWeight').val());
			$('.total-qty').html(opener.$('#txtTotalQty').val());
			$('.total-weight').html(opener.$('#txtTotalWeight').val());

			
		});
	</script>
	</body>
</html>

