		<!-- jQuery -->
		<script src=" <?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
		<script src=" <?php echo base_url('assets/js/shortcut.js'); ?>"></script>

		<script src="<?php echo base_url('assets/js/jquery.mask.js'); ?>"></script>
		<!-- easing -->
		<script src=" <?php echo base_url('assets/js/jquery.easing.1.3.min.js'); ?>"></script>
		<!-- bootstrap js plugins -->
		<script src=" <?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
		<!-- perfect scrollbar -->
		<script src=" <?php echo base_url('assets/lib/perfect-scrollbar/min/perfect-scrollbar-0.4.8.with-mousewheel.min.js'); ?>"></script>		
		<!-- common functions -->
		<script src=" <?php echo base_url('assets/js/tisa_common.js'); ?>"></script>
		<script src="<?php echo base_url('application/dashboardlib/jquery-ui.min.js'); ?>"></script>

		<!-- datatables -->
		<script src=" <?php echo base_url('assets/lib/DataTables/media/js/jquery.dataTables.min.js'); ?>"></script>
		<script src=" <?php echo base_url('assets/lib/DataTables/media/js/dataTables.bootstrap.js'); ?>"></script>
		<!--  timepicker -->
		<script src="<?php echo base_url('assets/lib/bootstrap-timepicker/js/bootstrap-timepicker.js') ?>"></script>
		<!--  datepicker -->
		<script src="<?php echo base_url('assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>"></script>		
		<!--  bootstrap switches -->
		<script src="<?php echo base_url('assets/lib/bootstrap-switch/build/js/bootstrap-switch.min.js'); ?>"></script>
		<!-- multiselect, tagging -->
		<script src="<?php echo base_url('assets/lib/select2/select2.min.js'); ?>"></script>
		<!-- DropZone File Upload -->
		<script src="<?php echo base_url('assets/js/plugins/dropzone/dropzone.min.js'); ?>"></script>
		<!-- handlebar.js  -->		
		<script src="<?php echo base_url('application/dashboardlib/application.min.js'); ?>"></script>
		
		
		<script src="<?php echo base_url('application/dashboardlib/demonstration.min.js'); ?>"></script>
		<script src="<?php echo base_url('application/dashboardlib/jquery.bootbox.js'); ?>"></script>
		<script src="<?php echo base_url('application/dashboardlib/jquery.flot.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/jquery.buttonLoader.js'); ?>"></script>

		<script src="<?php echo base_url('assets/js/handlebars.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/morris.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/raphael-min.js'); ?>"></script>
		
		<!-- custom javascript -->
		<script src="<?php echo base_url('assets/js/app_modules/general.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/app_modules/custom.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/app_modules/components.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/sweet-alert.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/plugins/paginator/smartpaginator.js'); ?>"></script>
        <script src="<?php echo base_url('assets/lib/autocomplete/jquery.auto-complete.js');?> "></script>
        <?php if (isset($Attachments)): ?>
        	
		<?php if ($Attachments=='true' AND $Attachments!=='' ) { ?>
			<script src="<?php echo base_url('assets/js/app_modules/attachments.js'); ?>"></script>
		<?php } ?>
        <?php endif ?>
		<?php if (isset($modules)): ?>			
			<?php foreach ($modules as $module): ?>
				<script src="<?php echo base_url('assets/js/app_modules/' . $module . '.js'); ?>"></script>
			<?php endforeach; ?>
		<?php endif; ?>

    </body>
</html>
