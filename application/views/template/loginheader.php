<?php

$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
$this->output->set_header('Pragma: no-cache');

?>

<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<title>Digital Manager ERP</title>
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">

		<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png'); ?>">

		<!-- bootstrap framework -->
		<link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" media="screen">
		<!-- custom css -->
		<link href="<?php echo base_url('assets/css/plugins/datepicker/datepicker.css'); ?>" rel="stylesheet" media="screen">

		<link href="<?php echo base_url('assets/css/custom.css'); ?>" rel="stylesheet" media="screen">
		<link href="<?php echo base_url('assets/css/login.css') ?>" rel="stylesheet">

		<script>
        	var base_url = '<?php echo base_url(); ?>';
    	</script>
    </head>
    <body style="background: #368EE0;">