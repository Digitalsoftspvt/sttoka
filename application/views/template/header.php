<?php

$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
$this->output->set_header('Pragma: no-cache');

?>

<!DOCTYPE html>
<html class="zoom">
    <head>
		<meta charset="UTF-8">
		<title>Digital Manager ERP</title>
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">

		<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png'); ?>">

		<!-- bootstrap framework -->
		<link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" media="screen">

		<!-- custom icons -->
		<!-- font awesome icons -->
		 <link href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" media="screen">
		<!-- <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet" media="screen"> -->
		<!-- ionicons -->
		<link href="<?php echo base_url('assets/icons/ionicons/css/ionicons.min.css'); ?>" rel="stylesheet" media="screen">


		<!-- datatables -->
		<link rel="stylesheet" href="<?php echo base_url('assets/lib/DataTables/media/css/jquery.dataTables.min.css'); ?>">
		<link rel="stylesheet" href="<?php echo base_url('assets/lib/DataTables/extensions/TableTools/css/dataTables.tableTools.min.css'); ?>">
		<link rel="stylesheet" href="<?php echo base_url('assets/lib/DataTables/extensions/Scroller/css/dataTables.scroller.min.css'); ?>">
		<!-- flags -->
		<link rel="stylesheet" href="<?php echo base_url('assets/icons/flags/flags.css'); ?>">
		<!-- datepicker -->
		<link rel="stylesheet" href="<?php echo base_url('assets/lib/bootstrap-datepicker/css/datepicker3.css'); ?>">
		<!-- bootstrap switches -->
		<link href="<?php echo base_url('assets/lib/bootstrap-switch/build/css/bootstrap3/bootstrap-switch.css'); ?>" rel="stylesheet">
		<!-- multiselect, tagging -->
		<link rel="stylesheet" href="<?php echo base_url('assets/lib/select2/select2.css'); ?>">

		<!-- main stylesheet -->
		<link rel="stylesheet" href="<?php echo base_url('assets\js\plugins\dropzone\dropzone.min.css'); ?>">

		<link href="<?php echo base_url('assets/css/style_22.css'); ?>" rel="stylesheet" media="screen">
		<link href="<?php echo base_url('assets/css/sweet-alert.css'); ?>" rel="stylesheet" media="screen">
		<!-- custom css -->
		<link href="<?php echo base_url('assets/css/custom.css'); ?>" rel="stylesheet" media="screen">
		<link rel="stylesheet" href="<?php echo base_url('assets/css/morris.css'); ?>">
		<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/paginator/smartpaginator.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/lib/autocomplete/jquery.auto-complete.css'); ?>">
		<script>
        	var base_url = '<?php echo base_url(); ?>';
    	</script>

    </head>
    <body>
    <body>
    	<div class="loader">
	        <div class="loader_overlay"></div>
	        <img class="loader_img" src="<?php echo base_url('assets/img/ajaxloader.png'); ?>" alt="">
    	</div>
		<!-- top bar -->
		<!-- <header class="navbar navbar-fixed-top" role="banner">
			<div class="container-fluid">
				<div class="navbar-header">
					<a href="<?php echo base_url('index.php/'); ?>" class="navbar-brand"><img src="<?php echo base_url('assets/img/header-logo.png'); ?>" alt="Logo"></a>
				</div>
				<ul class="nav navbar-nav navbar-right" style='margin-top: 6px;'>
					<li><a href="<?php echo base_url('index.php/user/logout') ?>"><span class="navbar_el_icon ion-log-out"></span> <span class="navbar_el_title">Logout</span></a></li>
				</ul>
			</div>
		</header> -->
		<header class="navbar navbar-fixed-top" role="banner">
			<div class="container-fluid">
				<div class='row'>
					<div class='col-lg-12 visible-lg' >
						<a href="<?php echo base_url('index.php/'); ?>" class="navbar-brand"><img src="<?php echo base_url('assets/img/header-logo.png'); ?>" alt="Logo"></a>
						<ul class='main-nav wall_btn' style='display:inline-flex;color:white;margin-top:10px;list-style: none;'>
							<li class="active hide">
								<a href="<?php echo base_url('index.php/wall'); ?>" style='color:white !important;'><i class="fa fa-rss"></i> Wall</a>
							</li>

							<li class="active">
								<a href="<?php echo base_url('index.php/user/dashboard'); ?>" style='color:white !important;'><i class="fa fa-home"></i> Home</a>
							</li>
							
							<li class="active login_info">
								<a href="" style='color:white !important;'><i class="fa fa-user"></i><?php echo ' Login as ' . $this->session->userdata('uname') . '. From: ' . $this->session->userdata('company_name') ; ?></a>
							</li>
						</ul>
						<ul class="nav navbar-nav navbar-right log_out" style='margin-top: 6px;'>
							<li><a href="<?php echo base_url('index.php/user/logout') ?>"><span class="navbar_el_icon ion-log-out"></span> <span class="navbar_el_title">Logout</span></a></li>
						</ul>
						<ul id="nav" class="nav navbar-nav navbar-right trans" style='margin-top: 6px;'>
													<!--<li class="payables">
														<a class="notif-nav-btn" title="Payables" data-toggle="tooltip" href="<?php echo base_url('index.php/report/payables#payablesTab'); ?>"><img src="<?php echo base_url('assets/img/icon-payable.png') ?>" alt=""><span class="payable-result-count notif-count-lbl" style="visibility: hidden;">0</span></a>
													</li>
													<li class="receivables">
														<a class="notif-nav-btn" title="Receivables" data-toggle="tooltip" href="<?php echo base_url('index.php/report/payables#receiveablesTab') ?>"><img src="<?php echo base_url('assets/img/icon-receiveable.png') ?>" alt=""><span class="receiveable-result-count notif-count-lbl" style="visibility: hidden;">0</span></a>
													</li>-->
													<li class="stocknotifs">
														<a href="<?php echo base_url('index.php/item/stockNotifications'); ?>" class="notif-nav-btn" title="Stock Notifications" data-toggle="tooltip"><img src="<?php echo base_url('assets/img/icon-stock.png') ?>" alt=""><span class="stocknotif-result-count notif-count-lbl" style="visibility: hidden;">0</span></a>
													</li>
									
												</ul>
					</div>
				</div>
				<!-- <div class="navbar-header">
					
				</div> -->
<input type="hidden" name="unameh" 	id="unameh" value="<?php echo $this->session->userdata('uname'); ?>">
<input type="hidden" name="cidh" 	id="cidh" value="<?php echo $this->session->userdata('company_id'); ?>">
<input type="hidden" id="sdate" value="<?php echo $this->session->userdata('fn_sdate');?>">
<input type="hidden" id="isadmin" value="<?php echo $this->session->userdata('isadmin');?>">
<input type="hidden" id="edate" value="<?php echo $this->session->userdata('fn_edate');?>">
<input type="hidden" id="txtetype" 	value="<?php echo isset($etype) 	?  $etype 		: '' ?>">
<input type="hidden" id="tablename" value="<?php echo isset($tablename) ?  $tablename 	: '' ?>">

			</div>
		</header>