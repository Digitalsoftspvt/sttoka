		<!-- jQuery -->
		<script src=" <?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
		<script src=" <?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>

		<script src="<?php echo base_url('assets/js/app_modules/general.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/app_modules/dashboard.js'); ?>"></script>
		<!-- common functions -->
		<script src=" <?php echo base_url('assets/js/tisa_common.js'); ?>"></script>
		<!-- custom javascript -->
		<script src="<?php echo base_url('assets/js/custom.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/app_modules/general.js'); ?>"></script>
		<script src='<?php echo base_url('assets/js/app_modules/user/login.js'); ?>'></script>
    </body>
</html>
