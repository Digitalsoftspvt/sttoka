
<?php

$desc = $this->session->userdata('desc');
$desc = json_decode($desc);
$desc = objectToArray($desc);

$vouchers = $desc['vouchers'];
$reports = $desc['reports'];
?>
<!-- side navigation -->
<nav id="side_nav">
    <ul>
        <!-- <li>
            <?php // if ($reports['dashboard'] == 1): ?>
                <a href="<?php echo base_url(). 'index.php/wall/dashboard'; ?>">
                    <span class="ion-speedometer"></span>
                    <span class="nav_title">Dashboard</span>
                </a>
            <?php // endif ?>

        </li> -->
        <!-- <li>
            <a href="<?php echo base_url() . 'index.php/wall'; ?>">
                <span class="ion-android-note"></span>
                <span class="nav_title">Wall</span>
            </a>
        </li> -->
        <li class='voucher-container'>
            <a href="#">
                <span class="ion-ios7-cog"></span>
                <span class="nav_title">Setup</span>
            </a>
            <div class="sub_panel">
                <div class="side_inner">
                    <!-- <ul> -->
                    <!-- <h4 class="panel_heading">Add New</h4> -->
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <h4 class="panel_heading panlhed">Add New</h4>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        <?php if ($vouchers['account']['account'] == 1): ?>
                                            <li class='voucher account'> <a href="<?php echo base_url('index.php/account/add'); ?>"><span class="side_icon ion-ios7-personadd-outline"></span> New Party</a> </li>
                                        <?php endif ?>
                                       
                                        <?php if ($vouchers['level']['level'] == 1): ?>
                                            <li class='voucher level'> <a href="<?php echo base_url('index.php/level/add'); ?>"><span class="side_icon ion-levels"></span> Account Level</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['item']['item'] == 1): ?>
                                            <li class='voucher item'> <a href="<?php echo base_url('index.php/item'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> New Item</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['catagory']['catagory'] == 1): ?>
                                            <li class='voucher catagory'> <a href="<?php echo base_url('index.php/item/category'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Catagory</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['subcatagory']['subcatagory'] == 1): ?>
                                            <li class='voucher subcatagory'> <a href="<?php echo base_url('index.php/item/subcategory'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Sub Category</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['brand']['brand'] == 1): ?>
                                            <li class='voucher brand'> <a href="<?php echo base_url('index.php/item/brand'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Brand</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['route']['route'] == 1): ?>
                                            <li class='voucher route'> <a href="<?php echo base_url('index.php/item/route'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Route</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['itemphaseRecepie']['itemphaseRecepie'] == 1): ?>
                                            <li class='voucher itemphaseRecepie'> <a href="<?php echo base_url('index.php/itemphaseRecepie'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Item Phase Recepie</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['transporter']['transporter'] == 1): ?>
                                            <li class='voucher transporter'> <a href="<?php echo base_url('index.php/transporter'); ?>"><span class="side_icon glyphicon glyphicon-pushpin"></span> New Transporter</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['currency']['currency'] == 1): ?>
                                            <li class='voucher currency'> <a href="<?php echo base_url('index.php/currency'); ?>"><span class="side_icon glyphicon glyphicon-pushpin"></span> New Currency</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['salesman']['salesman'] == 1): ?>
                                            <li class='voucher salesman'> <a href="<?php echo base_url('index.php/salesman'); ?>"><span class="side_icon ion-ios7-person-outline"></span> New Sales Man</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['warehouse']['warehouse'] == 1): ?>
                                            <li class='voucher warehouse'> <a href="<?php echo base_url('index.php/department') ?>"><span class="side_icon glyphicon glyphicon-tasks"></span> Warehouse</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['group']['group'] == 1): ?>
                                            <li class='voucher group'> <a href="<?php echo base_url('index.php/group') ?>"><span class="side_icon glyphicon glyphicon-tasks"></span> New Group</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['recipee']['recipee'] == 1): ?>
                                            <li class='voucher recipee'> <a href="<?php echo base_url('index.php/item/recipeCosting') ?>"><span class="side_icon ion-ios7-person-outline"></span> Recipe Costing</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['phase']['phase'] == 1): ?>
                                            <li class='voucher phase'> <a href="<?php echo base_url('index.php/phase') ?>"><span class="side_icon ion-ios7-person-outline"></span> New Phase</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['setting_configuration']['setting_configuration'] == 1): ?>
                                            <li class='voucher setting_configuration'> <a href="<?php echo base_url('index.php/setting_configuration') ?>"><span class="side_icon ion-ios7-person-outline"></span>Setting Configuration</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['sendmessage']['sendmessage'] == 1): ?>
                                            <li class='voucher sendmessage'> <a href="<?php echo base_url('index.php/message') ?>"><span class="side_icon ion-ios7-person-outline"></span>Send Message</a> </li>
                                        <?php endif ?>
                                        <?php //if ($vouchers['updateitemrate']['updateitemrate'] == 1): ?>
                                        <li class='voucher updateitemrate'> <a href="<?php echo base_url('index.php/item/updateitemrate') ?>"><span class="fa fa-tasks" aria-hidden="true"></span>  Update Item Rate</a> </li>
                                        <?php //endif ?>
                                        <?php if ($vouchers['uploadFiles']['uploadFiles'] == 1): ?>
                                            <li class='voucher uploadFiles hide'>  Attachment Files </li>
                                        <?php endif ?>
                                        <li class='voucher updateitemrate'> <a href="<?php echo base_url('index.php/UpdateRate') ?>"><span class="fa fa-tasks" aria-hidden="true"></span>  Party Puchase Rate</a> </li>
                                        <li class='voucher searchfile'><a href="<?php echo base_url('index.php/searchfile'); ?>"><span class="side_icon ion-ios7-plus-empty"></span>Search For File </a> </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <h4 class="panel_heading panlhed">Views</h4>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <ul>
                                        <?php if ($reports['coi'] == 1): ?>
                                            <li class='report coi'> <a href="<?php echo base_url('index.php/item/ChartOfItems') ?>"> Chart Of Items</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['coa'] == 1): ?>
                                            <li class='report coa'> <a href="<?php echo base_url('index.php/report/chartOfAccounts') ?>"> Chart Of Accounts</a> </li>
                                        <?php endif ?>
                                        <?php if (isset($reports['receipecostingreport']) && $reports['receipecostingreport'] == 1): ?>
                                            <li class='report receipecostingreport'> <a href="<?php echo base_url('index.php/report/receipecosting'); ?>"><span class=""></span> Receipe Costing Report</a> </li>
                                        <?php endif ?>
                                    </ul>
                                    <!-- </ul> -->
                                </div>
                            </div>
        </li>

        <li class='voucher-container'>
            <a href="#">
                <span class="ion-ios7-cart"></span>
                <span class="nav_title">Purchase</span>
            </a>
            <div class="sub_panel">
                <div class="side_inner">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsePurOne" aria-expanded="true" aria-controls="collapseOne">
                                        <h4 class="panel_heading panlhed">Vouchers</h4>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapsePurOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        <?php if (isset($vouchers['purchasecontract']['purchasecontract']) && $vouchers['purchasecontract']['purchasecontract'] == 1): ?>
                                            <li class='voucher purchasecontract'><a href="<?php echo base_url('index.php/purchasecontract'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Purchase Contract</a> </li>
                                        <?php endif ?>
                                        <?php //if ($vouchers['purchaseorder']['purchaseorder'] == 1): ?>
                                        <!-- <li class='voucher purchaseorder'><a href="<?php //echo base_url('index.php/purchaseorder'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Purchase Order</a> </li> -->
                                        <?php //endif ?>
                                        <?php if ($vouchers['purchasevoucher']['purchasevoucher'] == 1): ?>
                                            <li class='voucher purchasevoucher'><a href="<?php echo base_url('index.php/purchase'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Purchase Voucher</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['purchasereturnvoucher']['purchasereturnvoucher'] == 1): ?>
                                            <li class='voucher purchasereturnvoucher'><a href="<?php echo base_url('index.php/purchasereturn'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Purchase Return</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['inwardgatepass']['inwardgatepass'] == 1): ?>
                                            <li class='voucher inwardgatepass'><a href="<?php echo base_url('index.php/inwardgatepass'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Inward Gate Pass</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['requisition']['requisition'] == 1): ?>
                                            <li class='voucher requisition'><a href="<?php echo base_url('index.php/requisition'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Demand/Requisition Voucher</a> </li>
                                        <?php endif ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsePurTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <h4 class="panel_heading panlhed">Reports</h4>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapsePurTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <ul>
                                        <?php if ($reports['purchasereport'] == 1): ?>
                                            <li class='report purchasereport'> <a href="<?php echo base_url('index.php/report/purchase'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Purchase</a> </li>
                                        <?php endif ?>
                                        <?php if (isset($reports['purchasecontractreport']) && $reports['purchasecontractreport'] == 1): ?>
                                            <li class='report purchasecontractreport'> <a href="<?php echo base_url('index.php/report/purchaseContract'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Purchase Contract</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['purchasereturnreport'] == 1): ?>
                                            <li class='report purchasereturnreport'> <a href="<?php echo base_url('index.php/report/purchaseReturn'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Purchase Return</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['inwardgatepass'] == 1): ?>
                                            <li class='report inwardgatepass'><a href="<?php echo base_url('index.php/report/inwardgatepass'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Inward Gate Pass</a> </li>
                                        <?php endif ?>
                                        <?php //if ($reports['purchaseorderreport'] == 1): ?>
                                        <!-- <li class='report purchaseorderreport'> <a href="<?php //echo base_url('index.php/report/purchaseorder'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Purchase Order</a> </li> -->
                                        <?php //endif ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>

        <!-- <li class='voucher-container hide'>
            <a href="#">
                <span class="ion-ios7-cog"></span>
                <span class="nav_title">Order</span>
            </a>
            <div class="sub_panel">
                <div class="side_inner">
                    <h4 class="panel_heading">Vouchers</h4>
                    <ul>
                    </ul>
                    <h4 class="panel_heading">Reports</h4>
                    <ul>
                    </ul>
                </div>
            </div>
        </li> -->

        <li class='voucher-container'>
            <a href="#">
                <span class="ion-compose"></span>
                <span class="nav_title">Sale</span>
            </a>
            <div class="sub_panel">
                <div class="side_inner">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSaleOne" aria-expanded="true" aria-controls="collapseOne">
                                        <h4 class="panel_heading panlhed">Vouchers</h4>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseSaleOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        <?php if ($vouchers['saleordervoucher']['saleordervoucher'] == 1): ?>
                                            <li class='voucher saleordervoucher'> <a href="<?php echo base_url('index.php/saleorder'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Sale Order</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['orderselectionvoucher']['orderselectionvoucher'] == 1): ?>
                                            <li class='voucher orderselectionvoucher'> <a href="<?php echo base_url('index.php/order/selection'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Order Selection</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['orderpartsvoucher']['orderpartsvoucher'] == 1): ?>
                                            <li class='voucher orderpartsvoucher'> <a href="<?php echo base_url('index.php/saleorder/partsdetail'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Order Parts Detail</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['outwardgatepass']['outwardgatepass'] == 1): ?>
                                            <li class='voucher outwardgatepass'> <a href="<?php echo base_url('index.php/inwardgatepass/outward'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Outward Gate Pass</a> </li>
                                        <?php endif ?>
                                        <?php // if ($vouchers['orderloadingvoucher']['orderloadingvoucher'] == 1): ?>
                                        <li class='voucher orderloadingvoucher hide'> <a href="<?php echo base_url('index.php/saleorder/partsloading'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Outward Voucher</a> </li>
                                        <?php // endif ?>
                                        <?php if ($vouchers['salevoucher']['salevoucher'] == 1): ?>
                                            <li class='voucher salevoucher'> <a href="<?php echo base_url('index.php/saleorder/Sale_Invoice'); ?>"><span class="fa fa-ambulance"></span> Sale Invoice</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['salereturnvoucher']['salereturnvoucher'] == 1): ?>
                                            <li class='voucher salereturnvoucher'> <a href="<?php echo base_url('index.php/salereturn'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Sale Return</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['countersale']['countersale'] == 1): ?>
                                            <li class='voucher countersale'> <a href="<?php echo base_url('index.php/countersale'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Counter Sale Voucher</a> </li>
                                        <?php endif ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSaleTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <h4 class="panel_heading panlhed">Reports</h4>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseSaleTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <ul>

                                        <?php if ($reports['orderpartsreport'] == 1): ?>
                                            <li class='report orderpartsreport'> <a href="<?php echo base_url('index.php/report/orderParts'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Order Parts</a></li>
                                        <?php endif ?>
                                        <?php if ($reports['orderloadingreport'] == 1): ?>
                                            <li class='report orderloadingreport'> <a href="<?php echo base_url('index.php/report/orderLoading'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Order Loading</a></li>
                                        <?php endif ?>
                                        <?php if ($reports['salereport'] == 1): ?>
                                            <li class='report salereport'> <a href="<?php echo base_url('index.php/report/sale'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Sale</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['salereport'] == 1): ?>
                                            <li class='report salereport'> <a href="<?php echo base_url('index.php/report/counter_sale'); ?>"><span class="side_icon ion-ios7-plus-empty"></span>Counter Sale</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['salereturnreport'] == 1): ?>
                                            <li class='report salereturnreport'> <a href="<?php echo base_url('index.php/report/saleReturn'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Sale Return</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['saleorderreport'] == 1): ?>
                                            <li class='report saleorderreport'> <a href="<?php echo base_url('index.php/report/saleorder'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Sale Order</a> </li>
                                        <?php endif ?>

                                        <?php if ($reports['salecomparison'] == 1): ?>
                                            <li class='report salecomparison'> <a href="<?php echo base_url('index.php/report/MonthlySaleComparison'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Sale Comparison</a> </li>
                                        <?php endif ?>

                                        <?php if ($reports['outwardgatepass'] == 1): ?>
                                            <li class='report outwardgatepass'> <a href="<?php echo base_url('index.php/report/outwardgatepass'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Outward Gate Pass</a> </li>
                                        <?php endif ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <!-- <li class='voucher-container'>
            <a href="#">
                <span class="ion-ios7-cog"></span>
                <span class="nav_title">Job</span>
            </a>
            <div class="sub_panel">
                <div class="side_inner">
                    <ul>
                        <li class='voucher account'> <a href="<?php //echo base_url('index.php/job'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Define Job</a> </li>
                        <li class='voucher account'> <a href="<?php //echo base_url('index.php/jobexpense'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Job Expenses</a> </li>
                    </ul>
                </div>
            </div>
        </li> -->

        <li class='voucher-container'>
            <a href="#">
                <span class="ion-stats-bars"></span>
                <span class="nav_title">Inventory</span>
            </a>
            <div class="sub_panel">
                <div class="side_inner">

                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseInvOne" aria-expanded="true" aria-controls="collapseOne">
                                        <h4 class="panel_heading panlhed">Voucher</h4>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseInvOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        <?php if ($vouchers['productionvoucher']['productionvoucher'] == 1): ?>
                                            <li class='voucher productionvoucher'> <a href="<?php echo base_url('index.php/production'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Production Voucher</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['employeeproductionvoucher']['employeeproductionvoucher'] == 1): ?>
                                            <li class='voucher employeeproductionvoucher'> <a href="<?php echo base_url('index.php/employeeproduction'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Employee Production Voucher</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['sparepartproduction']['sparepartproduction'] == 1): ?>
                                            <li class='voucher sparepartproduction'> <a href="<?php echo base_url('index.php/sparepart'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Spare Part Production</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['mouldingvoucher']['mouldingvoucher'] == 1): ?>
                                            <li class='voucher mouldingvoucher'> <a href="<?php echo base_url('index.php/moulding'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Moulding Voucher</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['headmouldingvoucher']['headmouldingvoucher'] == 1): ?>
                                            <li class='voucher headmouldingvoucher'> <a href="<?php echo base_url('index.php/headmoulding'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Head Moulding Voucher</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['headproduction']['headproduction'] == 1): ?>
                                            <li class='voucher headproduction'> <a href="<?php echo base_url('index.php/headprod'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Head Production</a> </li>
                                        <?php endif ?>
                                        <?php //if ($vouchers['consumptionvoucher']['consumptionvoucher'] == 1): ?>
                                        <!-- <li class='voucher consumptionvoucher'> <a href="<?php //echo base_url('index.php/consumption'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Issuance Voucher</a> </li> -->
                                        <?php //endif ?>
                                        <?php //if ($vouchers['materialreturnvoucher']['materialreturnvoucher'] == 1): ?>
                                        <!-- <li class='voucher materialreturnvoucher'> <a href="<?php //echo base_url('index.php/materialreturn'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Material Return Voucher</a> </li> -->
                                        <?php //endif ?>
                                        <?php if ($vouchers['navigationvoucher']['navigationvoucher'] == 1): ?>
                                            <li class='voucher navigationvoucher'> <a href="<?php echo base_url('index.php/stocknavigation'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Navigation Voucher</a> </li>
                                        <?php endif ?>
                                        <!--<?php //if ($vouchers['mouldingsheet']['mouldingsheet'] == 1): ?> -->
                                        <!-- <li class='voucher mouldingsheet'> <a href="<?php //echo base_url('index.php/moulding'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Moulding Sheet</a> </li> -->
                                        <!--<?php //endif ?>-->
                                        <?php if ($vouchers['itemconversion']['itemconversion'] == 1): ?>
                                            <li class='voucher itemconversion'> <a href="<?php echo base_url('index.php/purchase/itemconversion'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Item Conversion Voucher</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['inwardservice']['inwardservice'] == 1): ?>
                                            <li class='voucher inwardservice'> <a href="<?php echo base_url('index.php/inward'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Inward Services Voucher</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['outwardservice']['outwardservice'] == 1): ?>
                                            <li class='voucher outwardservice'> <a href="<?php echo base_url('index.php/inward/outward'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Outward Service Gate Pass</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['openingstock']['openingstock'] == 1): ?>
                                            <li class='voucher openingstock'> <a href="<?php echo base_url('index.php/opening_stock'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Opening Stock</a> </li>
                                        <?php endif ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseInvTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <h4 class="panel_heading panlhed">Reports</h4>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseInvTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <ul>
                                        <?php if ($reports['itemledger'] == 1): ?>
                                            <li class='report itemledger'> <a href="<?php echo base_url('index.php/report/itemLedger'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Item Ledger</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['productionreport'] == 1): ?>
                                            <li class='report productionreport'> <a href="<?php echo base_url('index.php/report/production'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Production</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['headproductionreport'] == 1): ?>
                                            <li class='report headproductionreport'> <a href="<?php echo base_url('index.php/report/headproduction'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Head Production</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['headmouldingreport'] == 1): ?>
                                            <li class='report headmouldingreport'> <a href="<?php echo base_url('index.php/report/headmoulding'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Head Moulding</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['mouldingreport'] == 1): ?>
                                            <li class='report mouldingreport'> <a href="<?php echo base_url('index.php/report/moulding'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Moulding</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['stocknavigationreport'] == 1): ?>
                                            <li class='report stocknavigationreport'> <a href="<?php echo base_url('index.php/report/stockNavigation'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Stock Navigation</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['dailyvouchersreport'] == 1): ?>
                                            <li class='report dailyvouchersreport'> <a href="<?php echo base_url('index.php/report/dailyVoucher'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Daily Voucher</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['itemconversionreport'] == 1): ?>
                                            <li class='report itemconversionreport'> <a href="<?php echo base_url('index.php/report/itemconversion') ; ?>"><span class="side_icon ion-ios7-plus-empty"></span> Item Conversion Report </a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['stockreport'] == 1): ?>
                                            <li class='report stockreport'> <a href="<?php echo base_url('index.php/report/stock'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Stock</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['closingStock'] == 1): ?>
                                            <li class='report closingStock'> <a href="<?php echo base_url('index.php/report/closingStock'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Closing Stock In Hand</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['materialreturnreport'] == 1): ?>
                                            <li class='report materialreturnreport'> <a href="<?php echo base_url('index.php/report/materialReturn'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Material Return</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['consumptionreport'] == 1): ?>
                                            <li class='report consumptionreport'> <a href="<?php echo base_url('index.php/report/consumption'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Consumption</a> </li>
                                        <?php endif ?>
                                        <?php //if ($reports['productionreport'] == 1): ?>
                                        <!-- <li class='report productionreport'> <a href="<?php //echo base_url('index.php/report/production'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Production</a> </li> -->
                                        <?php //endif ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </li>
        <li class='voucher-container'>
            <a href="#">
                <span class="ion-arrow-graph-up-right"></span>
                <span class="nav_title">Accounts</span>
            </a>
            <div class="sub_panel">
                <div class="side_inner">

                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseAccOne" aria-expanded="true" aria-controls="collapseOne">
                                        <h4 class="panel_heading panlhed">Vouchers</h4>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseAccOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        <?php if ($vouchers['cash_payment_receipt']['cash_payment_receipt'] == 1): ?>
                                            <li class='voucher cash_payment_receipt'> <a href="<?php echo base_url('index.php/payment'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Cash Payment/Receipt</a> </li>
                                        <?php endif ?>
                                        <?php //if ($vouchers['cash_payment_receipt_multi']['cash_payment_receipt_multi'] == 1): ?>
                                        <!--<li class='voucher cash_payment_receipt_multi'> <a href="<?php echo base_url('index.php/payment/cashpaymentMulti'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Cash Payment/Receipt <small>(Multi)</small></a> </li>-->
                                        <?php //endif ?>
                                        <?php if ($vouchers['foreigncash_payment_receipt']['foreigncash_payment_receipt'] == 1): ?>
                                            <li class='voucher foreigncash_payment_receipt'> <a href="<?php echo base_url('index.php/payment/foreigncash'); ?>"><span class="side_icon ion-ios7-plus-empty"></span>Foreign Cash</a> </li>
                                        <?php endif ?>
                                        <?php //if ($vouchers['chequepaidvoucher']['chequepaidvoucher'] == 1): ?>
                                        <!--<li class='voucher chequepaidvoucher'> <a href="<?php echo base_url('index.php/payment/chequeIssue'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Cheque Paid/Receive </a> </li> -->
                                        <?php //endif ?>
                                        <?php //if ($vouchers['chequereceiptvoucher']['chequereceiptvoucher'] == 1): ?>
                                        <!-- <li class='voucher chequereceiptvoucher'> <a href="<?php //echo base_url('index.php/payment/chequeReceive'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Cheque Receive</a> </li>  -->
                                        <?php //endif ?>
                                        <?php if ($vouchers['cashslip']['cashslip'] == 1): ?>
                                            <li class='voucher cashslip'> <a href="<?php echo base_url('index.php/cashslip?etype=csrv'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Cash Slip</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['jvvoucher']['jvvoucher'] == 1): ?>
                                            <li class='voucher jvvoucher'> <a href="<?php echo base_url('index.php/jv'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Journal Voucher</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['openingbalance']['openingbalance'] == 1): ?>
                                            <li class='voucher openingbalance'> <a href="<?php echo base_url('index.php/opening_balance'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Opening Balance</a> </li>
                                        <?php endif ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseAccTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <h4 class="panel_heading panlhed">Reports</h4>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseAccTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <ul>
                                        <?php if ($reports['account_ledger'] == 1): ?>
                                            <li class='report account_ledger'> <a href="<?php echo base_url('index.php/report/accountLedger'); ?>"> Account Ledger</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['trial_balance'] == 1): ?>
                                            <li class='report trial_balance'> <a href="<?php echo base_url('index.php/trial_balance'); ?>"> Trial Balance</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['accountreports'] == 1): ?>
                                            <li class='report accountreports'> <a href="<?php echo base_url('index.php/report/accounts'); ?>"> Account Reports</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['coas'] == 1): ?>
                                            <li class='report coas'> <a href="<?php echo base_url('index.php/report/chartOfAccounts'); ?>"> Chart of Accounts</a> </li>
                                        <?php endif ?>
                                        <?php //if ($reports['chequereports'] == 1): ?>
                                        <!-- <li class='report chequereports'> <a href="<?php //echo base_url('index.php/report/cheques'); ?>"> Cheque Reports</a> </li> -->
                                        <?php //endif ?>
                                        <?php if ($reports['plsitem'] == 1): ?>
                                            <li class='report plsitem'> <a href="<?php echo base_url('index.php/report/profitloss'); ?>"> Item Wise Profit/Loss</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['dailytransaction'] == 1): ?>
                                            <li class='report dailytransaction'> <a href="<?php echo base_url('index.php/report/dailytransactionreport'); ?>"> Daily Transaction Report</a> </li>
                                        <?php endif ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>




                </div>
            </div>
        </li>
        <li class='voucher-container'>
            <a href="#">
                <span class="fa fa-bell"></span>
                <span class="nav_title">PayRoll</span>
            </a>
            <div class="sub_panel">
                <div class="side_inner">

                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseAccOneHrSetup" aria-expanded="true" aria-controls="collapseOne">
                                        <h4 class="panel_heading panlhed">Setup</h4>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseAccOneHrSetup" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        <?php if ($vouchers['staff']['staff'] == 1): ?>
                                            <li class='voucher staff'> <a href="<?php echo base_url('index.php/staff/add') ?>"><span class="side_icon ion-ios7-person-outline"></span> Staff</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['shift']['shift'] == 1): ?>
                                            <li class='voucher shift'> <a href="<?php echo base_url('index.php/shift/add') ?>"><span class="side_icon ion-clock"></span> Shift</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['shift_group']['shift_group'] == 1): ?>
                                            <li class='voucher shift_group'> <a href="<?php echo base_url('index.php/shift/addGroup') ?>"><span class="side_icon ion-arrow-swap"></span> Shift Group</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['allot_shift_group']['allot_shift_group'] == 1): ?>
                                            <li class='voucher allot_shift_group'> <a href="<?php echo base_url('index.php/shift/allotGroup') ?>"><span class="side_icon ion-arrow-swap"></span> Allot Shift Group</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['setting']['setting'] == 1): ?>
                                            <li class='voucher setting'> <a href="<?php echo base_url('index.php/setting'); ?>"><span class="side_icon ion-levels"></span> Settings</a> </li>
                                        <?php endif ?>
                                        <?php if (isset($vouchers['staffdepartment']['staffdepartment']) && $vouchers['staffdepartment']['staffdepartment'] == 1): ?>
                                            <li class='voucher staffdepartment'> <a href="<?php echo base_url('index.php/staffdepartment'); ?>"><span class="side_icon ion-levels"></span> Staff Department</a> </li>
                                        <?php endif ?>
                                    </ul>

                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseAccTwoAttendance" aria-expanded="false" aria-controls="collapseTwo">
                                        <h4 class="panel_heading panlhed">Attandance</h4>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseAccTwoAttendance" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">

                                    <ul>
                                        <?php if ($vouchers['staff_attendance']['staff_attendance'] == 1): ?>
                                            <li class='voucher staff_attendance'> <a href="<?php echo base_url('index.php/attendance/staff'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Staff Attendance</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['update_attendance_status']['update_attendance_status'] == 1): ?>
                                            <li class='voucher update_attendance_status'> <a href="<?php echo base_url('index.php/attendance/update'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Update Attendance Status</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['extradayvoucher']['extradayvoucher'] == 1): ?>
                                            <li class='voucher extradayvoucher'> <a href="<?php echo base_url('index.php/extraday'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Extra Days Voucher</a> </li>
                                        <?php endif ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseAccTwoHr" aria-expanded="false" aria-controls="collapseTwo">
                                        <h4 class="panel_heading panlhed">HR</h4>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseAccTwoHr" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">


                                    <ul>
                                        <?php //if ($vouchers['overtime']['overtime'] == 1): ?>
                                        <!-- <li class='voucher overtime'> <a href="<?php echo base_url('index.php/staff/overtime') ?>"><span class="side_icon ion-ios7-person-outline"></span> Overtime</a> </li> -->
                                        <?php //endif ?>
                                        <?php //if ($vouchers['cash_payment_receipt']['cash_payment_receipt'] == 1): ?>
                                        <!-- <li class='voucher cash_payment_receipt'> <a href="<?php //echo base_url('index.php/payment'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Salary Payment</a> </li> -->
                                        <?php //endif ?>
                                        <?php //if ($vouchers['penalty']['penalty'] == 1): ?>
                                        <!-- <li class='voucher penalty'> <a href="<?php echo base_url('index.php/charge/penalty'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Penalty</a> </li> -->
                                        <?php //endif ?>
                                        <?php if ($vouchers['loan']['loan'] == 1): ?>
                                            <li class='voucher loan'> <a href="<?php echo base_url('index.php/loan'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Loan</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['advance']['advance'] == 1): ?>
                                            <li class='voucher advance'> <a href="<?php echo base_url('index.php/payment/advance'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Advance</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['incentive']['incentive'] == 1): ?>
                                            <li class='voucher incentive'> <a href="<?php echo base_url('index.php/payment/incentive'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Incentive</a> </li>
                                        <?php endif ?>
                                        <?php //if ($vouchers['salary_sheet']['salary_sheet'] == 1): ?>
                                        <li class='voucher salary_sheet'> <a href="<?php echo base_url('index.php/staff/salarySheet'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Salary Sheet</a> </li>
                                        <?php //endif ?>
                                        <?php //if ($vouchers['salary_sheet_contract']['salary_sheet_contract'] == 1): ?>
                                        <li class='voucher salary_sheet'> <a href="<?php echo base_url('index.php/staff/salarySheet_contract'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Salary Sheet Contract</a> </li>
                                        <?php //endif ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseAccTwoStaffReports" aria-expanded="false" aria-controls="collapseTwo">
                                        <h4 class="panel_heading panlhed">Staff Reports</h4>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseAccTwoStaffReports" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">


                                    <ul>
                                        <?php if ($reports['staff_status'] == 1): ?>
                                            <li class='report staff_status'> <a href="<?php echo base_url('index.php/report/staffStatus'); ?>"> Status</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['staff_attendance_sheet'] == 1): ?>
                                            <li class='report staff_attendance_sheet'> <a href="<?php echo base_url('index.php/report/staffattendancesheet'); ?>"><i class="ion-document-text"></i> Staff Leave Status</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['staff_attendance_status_wise'] == 1): ?>
                                            <li class='report staff_attendance_status_wise'> <a href="<?php echo base_url('index.php/report/staffatndncstatuswise'); ?>"><i class="ion-document-text"></i> Staff Attendance Report</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['staff_monthly_attendance_report'] == 1): ?>
                                            <li class='report staff_monthly_attendance_report'> <a href="<?php echo base_url('index.php/report/staffmonthlyattendancereport'); ?>"><i class="ion-document-text"></i> Staff Monthly Attendance Register</a> </li>
                                        <?php endif ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseAccTwoHrReports" aria-expanded="false" aria-controls="collapseTwo">
                                        <h4 class="panel_heading panlhed">HR Reports</h4>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseAccTwoHrReports" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">


                                    <ul>
                                        <?php if ($reports['penalty_to_staff'] == 1): ?>
                                            <li class='report penalty_to_staff'> <a href="<?php echo base_url('index.php/report/penalty'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Penalty to Staff</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['overtime_to_staff'] == 1): ?>
                                            <li class='report overtime_to_staff'> <a href="<?php echo base_url('index.php/report/overtime'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Overtime to Staff</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['loan_to_staff'] == 1): ?>
                                            <li class='report loan_to_staff'> <a href="<?php echo base_url('index.php/report/loan'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Loan to Staff</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['advance_to_staff'] == 1): ?>
                                            <li class='report advance_to_staff'> <a href="<?php echo base_url('index.php/report/advance'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Advance to Staff</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['incentive_to_staff'] == 1): ?>
                                            <li class='report incentive_to_staff'> <a href="<?php echo base_url('index.php/report/incentive'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Incentive to Staff</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['eobi_contribution'] == 1): ?>
                                            <li class='report eobi_contribution'> <a href="<?php echo base_url('index.php/report/eobiContribution'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Eobi Contribution</a> </li>
                                        <?php endif ?>
                                        <?php if ($reports['social_security_contribution'] == 1): ?>
                                            <li class='report social_security_contribution'> <a href="<?php echo base_url('index.php/report/socialSecurityContribution'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Social Security Contribution</a> </li>
                                        <?php endif ?>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
        </li>
        <li class='voucher-container'>
            <a href="#">
                <span class="ion-settings"></span>
                <span class="nav_title">Utilities</span>
            </a>
            <div class="sub_panel">
                <div class="side_inner">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseUtilitiOne" aria-expanded="true" aria-controls="collapseOne">
                                        <h4 class="panel_heading panlhed">DataBase</h4>
                                    </a>
                                </h4>
                            </div>

                            <div id="collapseUtilitiOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        <?php if ($reports['backupdatabase'] == 1): ?>
                                            <li class='report backupdatabase'> <a href="<?php echo base_url('index.php/user/db_backup'); ?>"><span class="side_icon ion-ios7-plus-empty"></span> Backup DataBase</a> </li>
                                        <?php endif ?>

                                    </ul>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseUtilitiTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <h4 class="panel_heading panlhed">User</h4>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseUtilitiTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <ul>
                                        <?php if ($vouchers['user']['user'] == 1): ?>
                                            <li class='voucher user'> <a href="<?php echo base_url('index.php/user/addnew') ?>"><span class="side_icon ion-ios7-person-outline"></span>Add New User</a> </li>
                                        <?php endif ?>

                                        <?php if ($vouchers['user_right']['user_right'] == 1): ?>
                                            <li class='voucher user_right'> <a href="<?php echo base_url('index.php/userright/add') ?>"><span class="side_icon ion-ios7-person-outline"></span>User Rights</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['previllages']['previllages'] == 1): ?>
                                            <li class='voucher previllages'> <a href="<?php echo base_url('index.php/user/privillages'); ?>"><span class="side-icon fa fa-wrench"></span> Previllages</a> </li>
                                        <?php endif ?>
                                        <?php if ($vouchers['dateclose']['dateclose'] == 1): ?>
                                            <li class='voucher dateclose'> <a href="<?php echo base_url('index.php/user/dateclose'); ?>"><span class="side-icon fa fa-wrench"></span> Date Close</a> </li>
                                        <?php endif ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </li>

    </ul>
</nav>