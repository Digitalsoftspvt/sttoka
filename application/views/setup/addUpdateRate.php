<?php

    $desc = $this->session->userdata('desc');
    $desc = json_decode($desc);
    $desc = objectToArray($desc);

    $vouchers = $desc['vouchers'];
?>
<!-- main content -->
<div id="main_wrapper">

    <div class="page_bar">
        <div class="row">
            <div class="col-md-5">
                <h1 class="page_title"> Update Party Rate</h1>
            </div>
            <div class="col-md-7">
               <div class="pull-right">
                    <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                    <a class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['itemphaseRecepie']['insert']; ?>' data-updatebtn='<?php echo $vouchers['itemphaseRecepie']['update']; ?>' data-deletebtn='<?php echo $vouchers['itemphaseRecepie']['delete']; ?>' data-printbtn='<?php echo $vouchers['itemphaseRecepie']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
               </div>
            </div>
        </div>
    </div>

    <div class="page_content">
        <div class="container-fluid">
            <div class="row">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form action="">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label id="">Party Name</label>
                                            <select class="form-control select2" id="partyDropdown">
                                                <option value="" disabled="" selected="">Choose Party</option>
                                                <?php foreach ($parties as $partie): ?>
                                                    <option value="<?php echo $partie['pid']; ?>"><?php echo $partie['name']; ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row"></div>
                                    <div class="container-wrap">
                                        <div class="row">  
                                            <div class="col-lg-4">
                                                <label id="">Item Name</label>
                                                <select class="form-control select2" id="itemDropdown">
                                                    <option value="" disabled="" selected="">Choose Item</option>
                                                    <?php foreach ($items as $item): ?>
                                                        <option value="<?php echo $item['item_id']; ?>" data-uom="<?php echo $item['uom']; ?>" data-stqty="<?php echo $item['stqty']; ?>" data-purrate="<?php echo $item['cost_price']; ?>" data-stweight="<?php echo $item['stweight']; ?>"><?php echo $item['item_des']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-2" >
                                                <label>Rate</label>
                                                <input type="text" class="form-control num" id="txtRate" >
                                            </div>
                                            <div class="col-lg-2" style="margin-top:30px;">
                                                <!-- <label>Add</label>                                                     -->
                                                <a href="" class="btn btn-primary" id="btnAdd"><i class="fa fa-plus"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <table class="table table-striped" id="updateRateTbl">
                                                <thead>
                                                    <tr>
                                                        <th style="width:100px;">Sr#</th>
                                                        <th>Item</th>
                                                        <th style="width:200px;">Rate</th>
                                                        <th style="width:130px;">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form> <!-- end of form -->
                            </div>  <!-- end of panel-body -->
                        </div>  <!-- end of panel -->
                    </div>  <!-- end of col -->
                </div>  <!-- end of row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="col-lg-12">
                                            <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                                            <a class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['itemphaseRecepie']['insert']; ?>' data-updatebtn='<?php echo $vouchers['itemphaseRecepie']['update']; ?>' data-deletebtn='<?php echo $vouchers['itemphaseRecepie']['delete']; ?>' data-printbtn='<?php echo $vouchers['itemphaseRecepie']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
                                        </div>
                                    </div>  <!-- end of col -->
                                </div>  <!-- end of row -->
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </div>
</div>