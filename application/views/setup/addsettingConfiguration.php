<?php

    $desc = $this->session->userdata('desc');
    $desc = json_decode($desc);
    $desc = objectToArray($desc);

    $vouchers = $desc['vouchers'];
?>
<!-- main content -->
<div id="main_wrapper">
  <div class="page_bar">
    <div class="row-fluid">
      <div class="col-lg-6">
        <h1 class="page_title"><span class="ion-android-storage"></span> Setting Configuration</h1>
      </div>
      <div class="col-lg-6">
        <div class="pull-right top-btns">
          <a class="btn btn-default btnSave" data-insertbtn='<?php echo $vouchers['setting_configuration']['insert']; ?>' data-updatebtn='<?php echo $vouchers['setting_configuration']['update']; ?>'><i class="fa fa-save"></i> Save F10</a>
          <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
        </div>
      </div>
    </div><hr>
  </div>
  <div class="page_content">
    <div class="container-fluid">
      <div class="col-md-12">
        <div class="row">
          <div class="col-lg-12">
            <form action="" class="frst-form">
              <div class="form-group">
                <div class="row">
                </div>
              </div><!-- form-group -->  
              <div class="form-group">
                <div class="row">
                <input type="hidden" id="VoucherTypeHidden">
                  <div class="col-lg-2">
                    <label>Sale</label>
                   <select class="form-control select2" id="sale">
                      <option value="" selected="" disabled="">Choose ...</option>
                      <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                 <div class="col-lg-2">
                    <label>Purchase</label>
                   <select class="form-control select2" id="purchase">
                      <option value="" selected="" disabled="">Choose ...</option>
                      <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="col-lg-2">
                    <label>Sale Return</label>
                   <select class="form-control select2" id="saleReturn">
                      <option value="" selected="" disabled="">Choose ...</option>
                      <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="col-lg-2">
                    <label>Purchase Return</label>
                   <select class="form-control select2" id="purchaseReturn">
                      <option value="" selected="" disabled="">Choose ...</option>
                      <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>

                <div class="col-lg-2">
                  <label>Item Discount</label>
                  <select class="form-control select2" id="itemdiscount">
                    <option value="" selected="" disabled="">Choose ...</option>
                    <?php foreach ($party as $partys): ?>
                    <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-2">
                    <label>Discount</label>
                   <select class="form-control select2" id="discount">
                      <option value="" selected="" disabled="">Choose ...</option>
                      <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="col-lg-2">
                    <label>Expenses</label>
                   <select class="form-control select2" id="expenses">
                      <option value="" selected="" disabled="">Choose ...</option>
                      <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                 <div class="col-lg-2">
                    <label>Tax</label>
                   <select class="form-control select2" id="tax">
                      <option value="" selected="" disabled="">Choose ...</option>
                      <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="col-lg-2">
                    <label>Cash</label>
                   <select class="form-control select2" id="cash">
                      <option value="" selected="" disabled="">Choose ...</option>
                      <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                <!--   <div class="col-lg-2">
                    <label>Freight</label>
                   <select class="form-control select2" id="freight">
                      <option value="" selected="" disabled="">Choose ...</option>
                      <?php foreach ($party as $partys): ?>
                      <option value="<?php echo $partys['pid']; ?>"><?php echo $partys['name']; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div> -->
                 

                </div>
              </div><!-- form-group -->
            </form><!-- end of form -->
          </div><!-- end of col -->
        </div><!-- end of row -->
        <div class="row">
          <div class="col-lg-12">
            <div class="pull-right">
              <a class="btn btn-default btnSave" data-insertbtn='<?php echo $vouchers['setting_configuration']['insert']; ?>' data-updatebtn='<?php echo $vouchers['setting_configuration']['update']; ?>'><i class="fa fa-save"></i> Save F10</a>
              <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
            </div><!-- pull-right -->
          </div><!-- end of col --> 
        </div><!-- end of row -->
      </div><!-- end of col -->
    </div><!-- container-fluid -->
  </div><!-- page-content -->
</div><!--main-wrapper -->