<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);
	$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">
  <div class="page_bar">
    <div class="row-fluid">
      <div class="col-md-12">
        <h1 class="page_title">Add Account</h1>
      </div>
    </div>
  </div>
  <div class="page_content" style="margin: -59px 0px 0px 0px;">
    <div class="container-fluid">
      <div class="col-md-12">
        <form action="">
          <ul class="nav nav-pills">
            <li class="active"><a href="#basicInformation" data-toggle="tab">Basic Information</a></li>
            <li><a href="#detailedInformation" data-toggle="tab">Detailed Information</a></li>
            <li><a href="#allaccounts" data-toggle="tab">All Accounts</a></li>
          </ul>
        	<div class="tab-content">
            <div class="tab-pane active fade in" id="basicInformation">
	            <div class="row">
	              <div class="col-lg-12">
	                <div class="panel panel-default">
	                 	<div class="panel-body">
			                <div class="row">
			                  <div class="col-lg-3">
			                    <div class="input-group">
			                      <span class="input-group-addon id-addon">Id (Auto)</span>
			                      <input type="number" class="form-control input-sm num" id="txtAccountId">
			                      <input type="hidden" id="txtMaxAccountIdHidden">
									<input type="hidden" id="txtAccountIdHidden">
									<input type="hidden" id="VoucherTypeHidden">
			                    </div>
			                  </div>
			                  <div class="col-lg-3">
			                    <div class="form-group">
			                      <div class="col-lg-3">
					                    <div class="input-group">
					                      <span class="switch-addon">Is active?</span>
					                      <input type="checkbox" checked="" class="bs_switch" id="switchGender">
					                    </div>
					                  </div>
			                    </div>
			                  </div>
			                  <div class="col-lg-3">
			                  	
			                  </div>
			                  <div class="col-lg-3">
			                    <div class="input-group">
			                      <span class="input-group-addon txt-addon">Account Id</span>
			                      <select class="form-control input-sm"  id="drpacid">
			                        <option value="" disabled="" selected="">Choose Account Id</option>
															<?php foreach ($accounts as $account): ?>
															<option value="<?php echo $account['pid']; ?>" ><?php echo $account['account_id'] ?></option>
															<?php endforeach ?>
			                      </select>
			                    </div>
			                  </div>
			                </div>
			                <div class="row">
			                  <div class="col-lg-5">
			                    <div class="input-group">
			                      <span class="input-group-addon txt-addon">Name</span>
	                          <input type="text" class="form-control input-sm" id="txtName">
	                          <input type="hidden" class="form-control input-sm" id="txtNameHidden">
			                    </div>
			                  </div>
			                </div>
	                    <div class="row hide">
	                      <div class="col-lg-5">
	                        <div class="input-group">
	                          <select class="form-control input-sm"  id="allNames">
	                            <?php foreach ($names as $name): ?>
	                            <option value="<?php echo $name['name']; ?>"><?php echo $name['name']; ?></option>
	                            <?php endforeach ?>
	                            </select>
	                          </div>
	                        </div>
	                    	</div>
	                      <div class="row">
	                        <div class="col-lg-5">
	                          <div class="input-group">
	                            <span class="input-group-addon txt-addon">Urdu Name</span>
	                            <input type="text" class="form-control input-sm txtUrduRight" placeholder="پارٹی کا نام" style="font-size:20px !important;"  id="txtUrduName">
	                            <input type="hidden" class="form-control input-sm" id="txtUrduNameHidden">
	                          </div>
	                        </div>
	                      </div>
			                <div class="row">
								<div class="col-lg-5">
									<div class="input-group">
		                            	<span class="input-group-addon txt-addon">Address</span>
		                            	<input type="text" class="form-control input-sm" placeholder="Address of Party" id="txtAddress">
			                      	</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-5">
									<div class="input-group">
		                            	<span class="input-group-addon txt-addon">Ur Address</span>
		                            	<input type="text" class="form-control input-sm txtUrduRight" style="font-size:20px !important;" placeholder="پارٹی کا پتہ" id="txtUrduAddress">
		                            	<input type="hidden" class="form-control input-sm" id="txtUrduAddressHidden">
			                      	</div>
								</div>
							</div>
		                    <div class="row">
		                      <div class="col-lg-5">
		                        <div class="input-group">
		                          <span class="input-group-addon txt-addon">Mobile #</span>
                            	<input type="text" class="form-control input-sm" id="txtMobileNo">
		                        </div>
		                      </div>
		                    </div>
		                    <div class="row">
		                      <div class="col-lg-5">
		                        <div class="input-group">
		                          <span class="input-group-addon txt-addon">Credit Limit</span>
                            	<input type="text" class="form-control input-sm num" id="txtLimit">
		                        </div>
		                      </div>
		                    </div>
			                  <div class="row">
			                    <div class="col-lg-5">
			                      <div class="input-group">
			                        <span class="input-group-addon txt-addon">Acc Type</span>
			                        <select class="form-control input-sm"  id="txtLevel3">
			                          <option value="" disabled="" selected="">Choose Account Type</option>
																<?php foreach ($l3s as $l3): ?>
																<option value="<?php echo $l3['l3']; ?>" data-level2="<?php echo $l3['level2_name']; ?>" data-level1="<?php echo $l3['level1_name']; ?>"><?php echo $l3['level3_name'] ?></option>
																<?php endforeach ?>
			                        </select>
			                     	</div>
			                    </div>
			                    <div class="col-lg-6">
			                      <span><b>Type 2 &rarr; </b><span id="txtselectedLevel2"> </span></span> <span><b>Type 1 &rarr; </b><span id="txtselectedLevel1"> </span></span>
			                    </div>
			                  </div>
			                  <div class="row">
			                    <div class="col-lg-2">
			                      <a href="" class="btn btn-sm btn-primary" id="addMoreInf"><i class="fa fa-th-list"></i> Add More Detail</a>
			                    </div>
			                  </div>
												<div id="party-lookup" class="modal fade modal-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    											<div class="modal-dialog modal-lg">
        										<div class="modal-content">
            									<div class="modal-header">
								                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								                <h3 id="myModalLabel"><i class='fa fa-search'></i> Party Lookup</h3>
            									</div>
							                <div class="modal-body">
							                	<table class="table table-striped modal-table">
							                	<!-- <table class="table table-bordered table-striped modal-table"> -->
							                		<thead>
							                			<tr style="font-size:16px;">
							                				<th>Id</th>
							                				<th>Name</th>
							                				<th>Urdu Name</th>
							                				<th>Mobile</th>
							                				<th>Address</th>
							                				<th style='width:3px;'>Actions</th>
							                			</tr>
                									</thead>
                									<tbody>
                										<?php foreach ($accounts as $party): ?>
                										<tr>
											                <td width="14%;">
											                <?php echo $party['pid']; ?>
											                <input type="hidden" name="hfModalPartyId" value="<?php echo $party['pid']; ?>">
											                </td>
											                <td><?php echo $party['name']; ?></td>
											                <td><?php echo $party['uname']; ?></td>
											                <td><?php echo $party['mobile']; ?></td>
											                <td><?php echo $party['address']; ?></td>
											                <td><a href="#" data-dismiss="modal" class="btn btn-primary populateAccount"><i class="glyphicon glyphicon-ok"></i></a></td>
											              </tr>
                										<?php endforeach ?>
									                </tbody>
									              </table>
									            </div>
									            <div class="modal-footer">
									              <!-- <button class="btn btn-danger delete-modal-del">Delete</button> -->
									              <button class="btn btn-primary" data-dismiss="modal">Cancel</button>
									            </div>
									        	</div>
									    		</div>
												</div>
			                  <div class="row">
						            	<div class="col-lg-12">
				                   	<div class="pull-right">
															<a class="btn btn-sm btn-default btnSave" data-insertbtn='<?php echo $vouchers['account']['insert']; ?>' data-updatebtn='<?php echo $vouchers['account']['update']; ?>'><i class="fa fa-save"></i> Save F10</a>
															<a class="btn btn-sm btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
															<a href="#party-lookup" data-toggle="modal" class="btn btn-sm btn-default btnsearchparty "><i class="fa fa-search"></i>&nbsp;Account Lookup F1</a>
														</div><!-- pull-right -->
						            	</div><!-- end of col -->
						        		</div><!-- end of button row -->
			                </div><!-- panel-body -->
	                 	</div><!-- end of panel -->
	                </div><!-- end of col -->
	            	</div><!-- end of row 1 of basic information -->
            	</div><!-- end of basicInformation -->
            	<div class="tab-pane fade" id="detailedInformation">
								<div class="row">
									<div class="col-lg-12">
										<div class="panel panel-default">
											<div class="panel-body">
												<div class="row">
													<div class="col-lg-5">
														<div class="input-group">
			                        <span class="input-group-addon txt-addon">Contact Person</span>
			                        <input type="text" class="form-control input-sm"  placeholder="Contact Person" id="txtContactPerson">
			                      </div>
													</div>
													<div class="col-lg-5">
			                      <div class="input-group">
			                        <span class="input-group-addon txt-addon" placeholder="City Area">City Area</span>
			                        <input type="text" class='form-control input-sm' placeholder="City Area" id="txtCityArea" list='areas'>
			                        <datalist id="areas">
			                          <?php foreach ($cityareas as $cityarea): ?>
			                          <?php if ($cityarea['cityarea'] !== ''): ?>
			                          <option value="<?php echo $cityarea['cityarea']; ?>"></option>
			                          <?php endif ?>
			                          <?php endforeach ?>
			                        </datalist>
			                      </div>
			                    </div>
												</div>
												<div class="row">
													<div class="col-lg-5">
														<div class="input-group">
			                        <span class="input-group-addon txt-addon">Email</span>
			                        <input type="text" class="form-control input-sm" placeholder="Email Address" id="txtEmail">
			                      </div>
													</div>
													<div class="col-lg-5">
														<div class="input-group">
                            	<span class="input-group-addon txt-addon">CNIC</span>
                            	<input type="text" class="form-control input-sm num" placeholder="CNIC"  id="txtCNIC">
                            </div>
													</div>
												</div>
												<div class="row">
													<div class="col-lg-5">
														<div class="input-group">
			                        <span class="input-group-addon txt-addon">Phone</span>
			                        <input type="text" class="form-control input-sm num" placeholder="Phone Office" id="txtPhoneNo">
			                      </div>
													</div>
													<div class="col-lg-5">
														<div class="input-group">
			                        <span class="input-group-addon txt-addon">NTN</span>
			                        <input type="text" class="form-control input-sm num" placeholder="National Tax Number" id="txtNTN">
			                      </div>
													</div>
												</div>
												<div class="row">
			                    <div class="col-lg-5">
			                      <div class="input-group">
			                        <span class="input-group-addon txt-addon" placeholder="City">City</span>
			                        <input type="text" class='form-control input-sm' placeholder="City" id="txtCity" list='cities'>
			                        <datalist id="cities">
                              	<?php foreach ($cities as $city): ?>
                              	<?php if ($city['city'] !== ''): ?>
                              	<option value="<?php echo $city['city']; ?>"></option>
                              	<?php endif ?>
                              	<?php endforeach ?>
                              </datalist>
                          	</div>
                      		</div>
	                        <div class="col-lg-5">
	                          <div class="input-group">
	                           	<span class="input-group-addon txt-addon">Type</span>
	                            <input type="text" class='form-control input-sm' placeholder="Type" id="txtType" list='types'>
	                            <datalist id="types">
	                             	<?php foreach ($typess as $typee): ?>
	                              <?php if ($typee['etype'] !== ''): ?>
	                              <option value="<?php echo $typee['etype']; ?>"></option>
	                              <?php endif ?>
	                              <?php endforeach ?>
	                            </datalist>
	                          </div>
	                        </div>
	                    	</div>
												<div class="row">
													<div class="col-lg-5">
														<div class="input-group">
                            	<span class="input-group-addon txt-addon">Fax</span>
                            	<input type="text" class="form-control input-sm num" placeholder="Fax Number" id="txtFax">
                            </div>
													</div>
			                    <div class="col-lg-5">
                            <div class="input-group">
                              <span class="input-group-addon txt-addon">Country</span>
                              <input type="text" class='form-control input-sm' placeholder="Country" id="txtCountry" list='countries'>
                              <datalist id="countries">
                               	<?php foreach ($countries as $country): ?>
                                <?php if ($country['country'] !== ''): ?>
                                <option value="<?php echo $country['country']; ?>"></option>
                                <?php endif ?>
                                <?php endforeach ?>
                              </datalist>
                            </div><!-- input-group -->
                        	</div><!-- end of col -->
												</div><!-- end of row -->
											</div><!-- panel-body -->
										</div><!-- end of panel -->
									</div><!-- end of col -->
								</div><!-- end of row -->
            	</div><!-- end of salary -->
            	<div class="tab-pane fade" id="allaccounts">
								<div class="row">
									<div class="col-lg-12">
										<div class="panel panel-default">
											<div class="panel-body">
												<div class="pull-right"><a href='#' class="btn btn-sm btn-default btnPrint"> <i class="fa fa-print"></i>Print F9</a></div><br>
												<br>
												<table class="table table-striped table-hover" id="allPartyTable">
													<thead class="dthead">
														<tr>
															<th class="srno">Sr#</th>
															<th>Id</th>
															<th>Name</th>
															<th>Mobile#</th>
															<th>Address</th>
															<th>Level 3</th>
															<th class="action"></th>
														</tr>
													</thead>
													<tbody>
														<?php $counter = 1; foreach ($accounts as $account): ?>
															<tr>
																<td class="srno"><?php echo $counter++; ?></td>
																<td><?php echo $account['pid']; ?></td>
																<td><?php echo $account['name']; ?></td>
																<td><?php echo $account['mobile']; ?></td>
																<td><?php echo $account['address']; ?></td>
																<td><?php echo $account['level3_name']; ?></td>
																<td class="action"><a href="" class="btn btn-sm btn-primary btn-edit-account showallupdatebtn" data-pid="<?php echo $account['pid']; ?>" data-showallupdatebtn=<?php echo $vouchers['account']['update']; ?>><span class="fa fa-edit"></span></a></td>
															</tr>
														<?php endforeach ?>
													</tbody>
												</table>
												
											</div><!-- panel-body -->
										</div><!-- end of panel -->
									</div><!-- end of col -->
								</div><!-- end of row -->
           	 	</div><!-- end of admissionTest -->
         	 	</div><!-- tab-content -->
       	 	</form><!-- end of form -->
      	</div><!-- end of col -->
   	 	</div><!-- end of container fluid -->
  	</div><!-- end of page_content -->
	</div><!-- main-wrapper -->

<style>
    .well-sm
    {
        display: block !important;
        background: transparent !important;
    }
    .clearfix
    {
        height: 54px;
    }
    .well
    {
        margin-bottom: -5px !important;
    }
    td:contains-selector(span.hide) { display:none !important; }
</style>