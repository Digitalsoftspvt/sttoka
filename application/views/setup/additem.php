<?php

    $desc = $this->session->userdata('desc');
    $desc = json_decode($desc);
    $desc = objectToArray($desc);

    $vouchers = $desc['vouchers'];
?>
<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-4">
				<h1 class="page_title"><i class="fa fa-bars"></i> Add New Item</h1>
			</div>
			<div class="col-md-8">
				<div class="pull-right">
					<a class="btn btn-sm btn-default btnSave" data-insertbtn='<?php echo $vouchers['item']['insert']; ?>' data-updatebtn='<?php echo $vouchers['item']['update']; ?>'><i class="fa fa-save"></i> Save F10</a>
					<a href='' class="btn btn-sm btn-default btnReset">
						<i class="fa fa-refresh"></i>
					Reset F5</a>
					<a href="#item-lookup" data-toggle="modal" class="btn btn-sm btn-primary btnsearchitem "><i class="fa fa-search"></i>&nbsp;Item Lookup F2</a>
					<a href='' class="btn btn-sm btn-default btnPrint"> <i class="fa fa-print"></i>Print F9</a>
				</div>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="col-md-12">

				<form action="" style="margin-top:-10px;">

					<ul class="nav nav-pills" style="margin-bottom:-10px;">
			            <li class="active"><a href="#additem" data-toggle="tab">Add Item</a></li>
			            <li><a href="#viewall" data-toggle="tab">View All</a></li>
			            <li><a href="#importitem_excel" data-toggle="tab">Import Items</a></li>
			            <li><a href="#phase" data-toggle="tab">Phase</a></li>
			        </ul>

			        <div class="tab-content">
            			<div class="tab-pane active fade in" id="additem">

							<div class="row" style="margin-left:0px !important;">

								<div class="panel panel-default">
									<div class="panel-body">
										<!-- <button type="button" class="alert-message" data-dismiss="alert">
										  <span aria-hidden="true">&times;</span>
										  <span class="sr-only">Close</span>
										</button> -->
										<div class="row">
											
											<div class="col-lg-9">

												<div class="row">
													<div class="col-lg-2">
															<label>Id</label>
															<input type="number" class="form-control num" id="txtId">
															<input type="hidden" id="txtMaxIdHidden">
															<input type="hidden" id="txtIdHidden">
															<input type="hidden" id="VoucherTypeHidden">

															<input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
		                                                    <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
		                                                    <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
													</div>
													<div class="col-lg-1">
														
													</div>
													<div class="col-lg-3">
														<label>Is active?</label>
														<input type="checkbox" checked="" class="bs_switch active_switch" id="active">
													</div>
													<div class="col-lg-3">
														<label>Date</label>
														<input class="form-control ts_datepicker" type="text" id="current_date">
													</div>
													<div class="col-lg-3">
														<label>Item Code</label>
														<select class="form-control select2" id="ic_dropdown">
															<option value="" disabled="" selected="">Choose Item Code</option>
															<?php foreach ($items as $item): ?>
																<option value="<?php echo $item['item_id']; ?>"><?php echo $item['item_code']; ?></option>
															<?php endforeach ?>
														</select>
													</div>
												</div>

												<div class="row">
													<div class="col-lg-3">
														<label>Category</label>
														<select class="form-control select2" id="category_dropdown">
															<option value="" disabled="" selected="">Choose Category</option>
															<?php foreach ($categories as $category): ?>
																<option value="<?php echo $category['catid']; ?>"><?php echo $category['name']; ?></option>
															<?php endforeach ?>
														</select>
													</div>
													<div class="col-lg-3">
														<label>Sub Catgeory</label>
														<select class="form-control select2" id="subcategory_dropdown">
															<option value="" disabled="" selected="">Choose sub category</option>
															<?php foreach ($subcategories as $subcategory): ?>
																<option value="<?php echo $subcategory['subcatid']; ?>"><?php echo $subcategory['name']; ?></option>
															<?php endforeach ?>
														</select>
													</div>
													<div class="col-lg-3">
														<label>Brand</label>
														<select class="form-control select2" id="brand_dropdown">
															<option value="" disabled="" selected="">Choose brand</option>
															<?php foreach ($brands as $brand): ?>
																<option value="<?php echo $brand['bid']; ?>"><?php echo $brand['name']; ?></option>
															<?php endforeach ?>
														</select>
														
													</div>


													<div class="col-lg-3">
														
															<label>Type</label>
															<input type="text" list='type' class="form-control" id="txtBarcode"/>
															<datalist id='type'>
																<?php foreach ($types as $type): ?>
																	<option value="<?php echo $type['barcode']; ?>">
																<?php endforeach ?>
															</datalist>
														
													</div>

												</div>

												<div class="row">
													<div class="col-lg-6">
														<label>Description</label>
														<input type="text" class="form-control" id="txtDescription"/>
														<input type="hidden" class="form-control" id="txtDescriptionHidden"/>
													</div>
                                                    <div class="col-lg-6">
                                                        <label>Urdu Description</label>
                                                        <input type="text" class="form-control txtUrduRight" id="txtUrduDescription">
                                                        <input type="hidden" class="form-control" id="txtUrduDescriptionHidden">
                                                    </div>
												</div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label>Remarks</label>
                                                        <input type="text" class="form-control" id="txtRemarks"/>
                                                    </div>
                                                </div>

												<div class="row hide">
													<select class="form-control" id="alldescription_dropdown">
														<?php foreach ($items as $item): ?>
															<option value="<?php echo $item['description']; ?>"><?php echo $item['description']; ?></option>
														<?php endforeach ?>
													</select>
												</div>

												<div class="row hide">
													<select class="form-control" id="allcodes_dropdown">
														<?php foreach ($items as $item): ?>
															<option value="<?php echo $item['item_code']; ?>"><?php echo $item['item_code']; ?></option>
														<?php endforeach ?>
													</select>
												</div>

											</div>
											<div class="col-lg-3">
												<div style="background: #F5F6F7;padding: 15px;border: 1px solid #ccc;box-shadow: 1px 1px 1px #000;">
													<div class="row">
														<div class="col-lg-12">
															<div class="studentImageWrap">
																<img src="<?php echo base_url('assets/img/blank_image.png'); ?>" alt="Item Image" style="margin: auto;display: block;" id="itemImageDisplay">
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-lg-12">
															<input type="file" id="itemImage">
														</div>
													</div>
												</div>
											</div>
										</div>

									</div>
								</div>

							</div>

							<div class="row" style="margin-left:0px !important;">
								<div class="col-lg-10">
									<div class="panel panel-default">
										<div class="panel-body">
									
												
												<div class="row">
													<div class="col-lg-3">
														<label>Min Level</label>
														<input class="form-control num" type="text" id="txtMinLevel">
													</div>
													<div class="col-lg-3">
														<label>Max Level</label>
														<input class="form-control num" type="text" id="txtMaxLevel">
													</div>
													<div class="col-lg-3">
														<label>UOM</label>
														<input type="text" class='form-control' placeholder="Uom" id="uom_dropdown" list='uoms'>
						                                <datalist id="uoms">
						                                	<?php foreach ($uoms as $uom): ?>
						                                		<?php if ($uom['uom'] !== ''): ?>
						                                			<option value="<?php echo $uom['uom']; ?>">
						                                		<?php endif ?>
						                                	<?php endforeach ?>
						                                </datalist>
													</div>
													<div class="col-lg-3">
														<label>Pur Price</label>
														<input class="form-control num" type="text" id="txtPurPrice">
													</div>
												</div>

												<div class="row">
													<div class="col-lg-3">
														<label>Net Weight</label>
														<input class="form-control num" type="text" id="txtNetWeight">
													</div>
													<div class="col-lg-3">
														<label>Gr Weight</label>
														<input class="form-control num" type="text" id="txtGrWeight">
													</div>
													<div class="col-lg-3">
														<label>Dealer</label>
														<input class="form-control num" type="text" id="txtSalePrice">
													</div>

													<div class="col-lg-3">
														<label>Deduction</label>
														<input class="form-control num" type="text" id="txtDiscount">
													</div>
												</div>
												<div class="row">
													<div class="col-lg-3">
														<label>Retail</label>
														<input class="form-control num" type="text" id="txtComm">
													</div>
													<div class="col-lg-3">
														<label>Sale Price 4</label>
														<input class="form-control num" type="text" id="txtPacking">
													</div>
													<div class="col-lg-3">
														<label>Mould Rate</label>
														<input class="form-control num" type="text" id="txtMould">
													</div>
													<div class="col-lg-3">
														<label>Dharry Rate</label>
														<input class="form-control num" type="text" id="txtRate">
													</div>
												</div>
											
									</div>
								</div>
								</div>
								<div class="col-lg-2">
									<div class="panel panel-default	">
										<div class="panel-body">
											<div class="row">
												<div class="col-lg-12">
													<label>Self Freight Rate</label>
													<input class="form-control" type="text" id="txtSelfFrightRate"> 
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<label>Bilty Freight Rate</label>
													<input class="form-control" type="text" id="txtBiltyFrightRate">
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<label>Gari Freight Rate</label>
													<input class="form-control" type="text" id="txtGariFrightRate">
												</div>
											</div>
										</div>
									</div>
								</div>
								
							</div>

							<div class="row hide">
								<div class="panel panel-default">
									<div class="panel-body">


										<div class="row">
											<div class="col-lg-4">
												<div class="input-group">
													<span class="input-group-addon">Debit</span>
													<select class="form-control" id="debit_dropdown">
														<option value="" disabled="" selected="">Choose debit party</option>
														<?php foreach ($parties as $party): ?>
															<option value="<?php echo $party['pid']; ?>"><?php echo $party['name']; ?></option>
														<?php endforeach ?>
													</select>
												</div>
											</div>

											<div class="col-lg-4">
												<div class="input-group">
													<span class="input-group-addon">Credit</span>
													<select class="form-control" id="credit_dropdown">
														<option value="" disabled="" selected="">Choose credit party</option>
														<?php foreach ($parties as $party): ?>
															<option value="<?php echo $party['pid']; ?>"><?php echo $party['name']; ?></option>
														<?php endforeach ?>
													</select>
												</div>
											</div>

										</div>
									</div>
								</div>
							</div>
							<div id="item-lookup" class="modal fade modal-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							    <div class="modal-dialog modal-lg">
							        <div class="modal-content">
							            <div class="modal-header">
							                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							                <h3 id="myModalLabel"><i class='fa fa-search'></i> Item Lookup</h3>
							            </div>

							                <div class="modal-body">
							                <table class="table table-striped modal-table">
							                <!-- <table class="table table-bordered table-striped modal-table"> -->
							                <thead>
							                <tr style="font-size:14px;">
							                <th>Id</th>
							                <th>Description</th>
							                <th style='background: #368EE0;'>Urdu Description</th>
							                <th>Catagory</th>
							                <th>Sub Catagory</th>
							                <th>Brand</th>
							                <th>Uom</th>
							                <th width="5px">Actions</th>
							                </tr>
							                </thead>
							                <tbody>
							                <?php foreach ($items as $item): ?>
							                <tr>
							                <td width="5%;">
							                <?php echo $item['item_id']; ?>
							                <input type="hidden" name="hfModalitemId" value="<?php echo $item['item_id']; ?>">
							                </td>
							                <td><?php echo $item['item_des']; ?></td>
							                <td><?php echo $item['uitem_des']; ?></td>
							                <td><?php echo $item['category_name']; ?></td>
							                <td><?php echo $item['subcategory_name']; ?></td>
							                <td><?php echo $item['brand_name']; ?></td>
							                <td><?php echo $item['uom']; ?></td>
							                <td width="5%;"><a href="#" data-dismiss="modal" class="btn btn-primary populateItem"><i class="glyphicon glyphicon-ok"></i></a></td>
							                </tr>
							                <?php endforeach ?>
							                </tbody>
							                </table>
							                </div>
							                <div class="modal-footer">
							                <!-- <button class="btn btn-danger delete-modal-del">Delete</button> -->
							                <button class="btn btn-primary" data-dismiss="modal">Cancel</button>
							            </div>
							        </div>
							    </div>
							</div>
							<br>
							<div class="row">
								<div class="pull-right">
									<a class="btn btn-sm btn-default btnSave" data-insertbtn='<?php echo $vouchers['item']['insert']; ?>' data-updatebtn='<?php echo $vouchers['item']['update']; ?>'><i class="fa fa-save"></i> Save F10</a>
									<a href='' class="btn btn-sm btn-default btnReset">
										<i class="fa fa-refresh"></i>
									Reset F5</a>
									<a href="#item-lookup" data-toggle="modal" class="btn btn-sm btn-primary btnsearchitem "><i class="fa fa-search"></i>&nbsp;Item Lookup F2</a>
									<!-- <a href='' class="btn btn-sm btn-default btnPrint"> <i class="fa fa-print"></i>Print F9</a> -->
								</div>

							</div>    <!-- end of a href='' row -->
            			</div>
            			<div class="tab-pane fade" id="viewall">

            				<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-default">
										<div class="panel-body">
											<table class="table table-striped table-hover" id="datatable_example">
												<thead class="dthead">
													<tr>
														<th style='background: #368EE0;'class="srno">Sr#</th>
														<th style='background: #368EE0;'>Category</th>
														<th style='background: #368EE0;'>Sub Category</th>
														<th style='background: #368EE0;'>Brand</th>
														<th style='background: #368EE0;' class="description">Description</th>
                                                        <th style='background: #368EE0;'>Urdu Description</th>
														<th style='background: #368EE0;'>SRate</th>
														<th style='background: #368EE0;'>PRate</th>
														<th style='background: #368EE0;' class="action"></th>
													</tr>
												</thead>
												<tbody>
													<?php $counter = 1; foreach ($items as $item): ?>
														<tr>
															<td><?php echo $counter++; ?></td>
															<td><?php echo $item['category_name']; ?></td>
															<td><?php echo $item['subcategory_name']; ?></td>
															<td><?php echo $item['brand_name']; ?></td>
															<td><?php echo $item['item_des']; ?></td>
                                                            <td><?php echo $item['uitem_des']; ?></td>
															<td><?php echo round(floatval($item['srate']), 2); ?></td>
															<td><?php echo round(floatval($item['cost_price']), 2); ?></td>
															<td class="action"><a href="" class="btn btn-sm btn-primary btn-edit-item" data-itemid="<?php echo $item['item_id']; ?>"><span class="fa fa-edit"></span></a></td>
														</tr>
													<?php endforeach ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
            			</div>
            			<div class="tab-pane fade" id="importitem_excel">
							<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-default">
										<div class="panel-body">
											<div class="row">
												<div class="col-lg-9">
													<div class="row">
														<div class="col-lg-3">
															<label>Item Type</label>
															<select class="form-control" id="itemntype_dropdown">
																<option></option>
																<?php foreach ($itemtypes as $type): ?>
																	<option value="<?php echo $type['id']; ?>"><?php echo $type['name']; ?></option>
																<?php endforeach ?>
															</select>
														</div>
														<div class="col-lg-3">
															<label>Catagory</label>
															<select class="form-control" id="itemcat_dropdown">
																<option></option>
																<?php foreach ($categories as $category): ?>
																	<option value="<?php echo $category['catid']; ?>"><?php echo $category['name']; ?></option>
																<?php endforeach ?>
															</select>
														</div>
														<div class="col-lg-3">
															<label>Sub-Catagory</label>
															<select class="form-control" id="itemsub_dropdown">
																<option></option>
															</select>
														</div>
														<div class="col-lg-3">
															<label>Brand</label>
															<select class="form-control" id="itembrand_dropdown">
																<option></option>
																<?php foreach ($brands as $brand): ?>
																	<option value="<?php echo $brand['bid']; ?>"><?php echo $brand['name']; ?></option>
																<?php endforeach ?>
															</select>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-3">
															<label>Short Code</label>
															<select class="form-control db_field" id="itemscode_dropdown">
															</select>
														</div>
														<div class="col-lg-3">
															<label>L</label>
															<select class="form-control db_field" id="iteml_dropdown">
															</select>
														</div>
														<div class="col-lg-3">
															<label>W</label>
															<select class="form-control db_field" id="itemw_dropdown">
															</select>
														</div>
														<div class="col-lg-3">
															<label>G</label>
															<select class="form-control db_field" id="itemg_dropdown">
															</select>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-6">
															<label>Description</label>
															<select class="form-control db_field" id="itemdesc_dropdown">
															</select>
														</div>
			                                            <div class="col-lg-6">
			                                                
			                                                    <label>Urdu Description</label>
			                                                    <input type="text" class="form-control txtUrduRight" id="txtUrduDescription">
			                                                    <input type="hidden" class="form-control" id="txtUrduDescriptionHidden">
			                                                
			                                            </div>
														
													</div>
													<div class="row">
														<div class="col-lg-6">
															<label>Remarks</label>
															<select class="form-control db_field" id="itemremark_dropdown">
															</select>
														</div>
														<div class="col-lg-3">
															<label>Min-Level</label>
															<select class="form-control db_field" id="itemminlevel_dropdown">
															</select>
														</div>
														<div class="col-lg-3">
															<label>Max-Level</label>
															<select class="form-control db_field" id="itemmaxlevel_dropdown">
															</select>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-3">
															<label>UOM</label>
															<select class="form-control db_field" id="itemuom_dropdown">
															</select>
														</div>
														<div class="col-lg-3">
															<label>Pur Price</label>
															<select class="form-control db_field" id="itempurprice_dropdown">
															</select>
														</div>
														<div class="col-lg-3">
															<label>Net Weight</label>
															<select class="form-control db_field" id="itemnweight_dropdown">
															</select>
														</div>
														<div class="col-lg-3">
															<label>Gr. Weight</label>
															<select class="form-control db_field" id="itemgrweight_dropdown">
															</select>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-3">
															<label>Sale Price1</label>
															<select class="form-control db_field" id="itemsaleprice1_dropdown">
															</select>
														</div>
														<div class="col-lg-3">
															<label>Sale Price2</label>
															<select class="form-control db_field" id="itemsaleprice2_dropdown">
															</select>
														</div>
														<div class="col-lg-3">
															<label>Sale Price3</label>
															<select class="form-control db_field" id="itemsaleprice3_dropdown">
															</select>
														</div>
														<div class="col-lg-3">
															<label>Sale Price4</label>
															<select class="form-control db_field" id="itemsaleprice4_dropdown">
															</select>
														</div>
														
													</div>
												</div>
											</form>
												<div class="col-lg-3">
													<form action="<?php echo base_url('upload/do_upload');?> "  method="post" enctype="multipart/form-data" id="upload_file" class="upload_files">
														<div style="background: #F5F6F7;padding: 15px;border: 1px solid #ccc;box-shadow: 1px 1px 1px #000;">
															<div class="row">
																<div class="col-lg-12">
																	<div class="studentImageWrap">
																		<img src="<?php echo base_url('assets/img/excel.png'); ?>" alt="excel Image" style="margin: auto;display: block;" id="excelImageDisplay">
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-lg-12">
																	<input type="file" id="userfile" name="userfile">
																</div>
															</div>
															<div class="row">
																<div class="col-lg-12">
																	<input type="submit" value="Submit" id="submit">
																	<input type="hidden" id="excel_path">
																</div>
															</div>
														</div>
													</form>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12 pull-right">
													<div class="pull-right">
														<a class="btn btn-success showBtn"><i class="fa fa-save"></i> Show</a>
														<a class="btn btn-primary importBtn"><i class="fa fa-table"></i> Import</a>
														<a class="btn btn-danger resetBtn"><i class="fa fa-refresh"></i> Reset</a>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<div id="no-more-tables">
														<input type="hidden" class="num_rows_table">
														<table class="col-md-12 table-bordered table-striped table-condensed cf" id="import_tbl">
															<thead class="cf">
																<tr class="head_res">
																</tr>
															</thead>
															<tbody id="body_rows" class="body_res">
															</tbody>
														</table>
													</div>
												</div>
											</div>			
										</div><!-- panel-body -->
									</div><!-- panel -->
								</div><!-- end of col -->
							</div><!-- end of row -->
			            </div><!-- end view all -->
			            <div class="tab-pane fade" id="phase">
			            	<div class="row-fluid">
								<div class="panel panel-default">
									<div class="panel-body">
										<div class="row">
											<div class="col-lg-12">
												<div class="row">
													<div class="col-lg-3">
														<label>Item</label>
														<select class="form-control select2" id="category_dropdown">
															<option value="" disabled="" selected="">Choose Item</option>
															<?php foreach ($items as $item): ?>
																<option value="<?php echo $item['item_id']; ?>"><?php echo $item['item_des']; ?></option>
															<?php endforeach ?>
														</select>
													</div>
													<div class="col-lg-3">
														<label>Phase</label>
														<select class="form-control select2" id="subcategory_dropdown">
															<option value="" disabled="" selected="">Choose Phase</option>
															<?php foreach ($phases as $phase): ?>
																<option value="<?php echo $phase['id']; ?>"><?php echo $phase['name']; ?></option>
															<?php endforeach ?>
														</select>
													</div>
													<div class="col-lg-1" style="width:140px;">
														<label>Item Rate</label>
														<input type="text" class='form-control'>
													</div>
													<div class="col-lg-3">
														<label>Calc Method</label>
														<input type="text" list='type' class="form-control" id="txtCalc"/>
														<!-- <datalist id='type'>
															<?php //foreach ($types as $type): ?>
																<option value="<?php //echo $type['barcode']; ?>">
															<?php //endforeach ?>
														</datalist> -->
													</div>
													<div class="col-lg-1" style='margin-top: 30px;'>                                                    
	                                                    <a href="" class="btn btn-primary" id="btnAdd"><i class="fa fa-plus"></i></a>
	                                                </div> 
												</div>
											</div>
										</div>
									</div>
								</div>
			            	</div>
            				<div class="row">
								<div class="col-lg-12" style="margin-top:-20px;">
									<div class="panel panel-default">
										<div class="panel-body">
											<table class="table table-striped table-hover">
												<thead>
													<tr>
														<th style='background: #368EE0;'>Sr#</th>
														<th style='background: #368EE0;'>Item</th>
														<th style='background: #368EE0;'>Phase</th>
														<th style='background: #368EE0;'>Item Rate</th>
														<th style='background: #368EE0;'>Method</th>
														<th style='background: #368EE0;'>Action</th>
													</tr>
												</thead>
												<tbody>
													<!-- <?php $counter //= 1; foreach ($items as $item): ?>
														<tr>
															<td><?php //echo $counter++; ?></td>
															<td><?php //echo $item['category_name']; ?></td>
															<td><?php //echo $item['subcategory_name']; ?></td>
															<td><?php //echo $item['brand_name']; ?></td>
															<td><?php //echo $item['item_des']; ?></td>
															<td><a href="" class="btn btn-sm btn-primary btn-edit-item" data-itemid="<?php //echo $item['item_id']; ?>"><span class="fa fa-edit"></span></a>
															<a href="" class="btn btn-sm btn-primary btn-edit-item" data-itemid="<?php //echo $item['item_id']; ?>"><span class="fa fa-edit"></span></a></td>
														</tr>
													<?php //endforeach ?> -->
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
            			</div>
						<!-- end view all -->

				</form>   <!-- end of form -->

			</div>  <!-- end of col -->
		</div>  <!-- end of container fluid -->
	</div>   <!-- end of page_content -->
</div>

<div id="UOM" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="model-contentwrapper">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="myModalLabel">Add UOM</h3>
		</div>
		<div class="modal-body">

			<div class="input-group">
				<span class="input-group-addon">UOM</span>
				<input type="text" class="form-control" id="txtNewUOM">
			</div>
		</div>
		<div class="modal-footer">
			<div class="pull-right">
				<a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
				<a class="btn btn-sm btn-default btnNewUOM"><i class="fa fa-plus"></i> Add</a>
			</div>
		</div>
	</div>
</div>

