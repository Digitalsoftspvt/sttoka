<?php
$desc = $this->session->userdata('desc');
$desc = json_decode($desc);
$desc = objectToArray($desc);
$vouchers = $desc['vouchers'];
?>
<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page_title">Add Transporter</h1>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="col-md-12">
				<ul class="nav nav-tabs" id="tabs_a">
					<li class="active"><a data-toggle="tab" href="#add_transporter">Add Update Transporter</a></li>
					<li class=""><a data-toggle="tab" href="#view_all">View All</a></li>
				</ul>
				<div class="tab-content">
					<div id="add_transporter" class="tab-pane fade active in">

						<div class="row">
							<div class="col-lg-12">
								<div class="panel panel-default">
									<div class="panel-body">

										<form action="">

											<div class="row">
												<div class="col-lg-2">
													<div class="input-group">
														<div class="input-group-addon id-addon">Vr#</div>
														<input type="number" class="form-control input-sm  num txtidupdate" id="txtId">
														<input type="hidden" id="txtIdHidden">
														<input type="hidden" id="txtMaxIdHidden">
														<input type="hidden" id="vouchertypehidden">
														<input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
														<input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
														<input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
														
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-4">
													<div class="input-group">
														<div class="input-group-addon txt-addon">Name</div>
														<input type="text" class="form-control input-sm " id="txtName">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-4">
													<div class="input-group">
														<div class="input-group-addon txt-addon">Urdud Name</div>
														<input type="text" class="form-control txtUrduRight" id="txtUrduDescription">
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-4">
													<div class="input-group">
														<div class="input-group-addon txt-addon">Contact Person</div>
														<input type="text" class="form-control input-sm " id="txtContact">
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-4">
													<div class="input-group">
														<div class="input-group-addon txt-addon">Phone</div>
														<input type="text" class="form-control input-sm " id="txtPhone">
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-4">
													<div class="input-group">
														<div class="input-group-addon txt-addon">Rate</div>
														<input type="text" class="form-control input-sm num" id="txtAreaCover">
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-12">
													<div class="pull-right">
														<a class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['transporter']['insert']; ?>' data-updatebtn='<?php echo $vouchers['transporter']['update']; ?>' data-deletebtn='<?php echo $vouchers['transporter']['delete']; ?>' data-printbtn='<?php echo $vouchers['transporter']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
														<a class="btn btn-sm btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
													</div>
												</div> 	<!-- end of col -->
											</div>	<!-- end of row -->
										</form>	<!-- end of form -->

									</div>	<!-- end of panel-body -->
								</div>	<!-- end of panel -->
							</div>  <!-- end of col -->
						</div>	<!-- end of row -->

					</div>	<!-- end of add_branch -->
					<div id="view_all" class="tab-pane fade">

						<div class="row">
							<div class="col-lg-12">
								<div class="panel panel-default">
									<div class="panel-body">
										<table class="table table-striped table-hover ar-datatable">
											<thead>
												<tr>
													<th style='background: #368EE0;'>Sr#</th>
													<th style='background: #368EE0;'>Name</th>
													<th style='background: #368EE0;'>Contact Person</th>
													<th style='background: #368EE0;'>Phone</th>
													<th style='background: #368EE0;'>Rate</th>
													<th style='background: #368EE0;'></th>
												</tr>
											</thead>
											<tbody>
												<?php if (count($transporters) > 0): ?>
													<?php $counter = 1; foreach ($transporters as $transporter): ?>
													<tr>
														<td><?php echo $counter++; ?></td>
														<td><?php echo $transporter['name']; ?></td>
														<td><?php echo $transporter['contact']; ?></td>
														<td><?php echo $transporter['phone']; ?></td>
														<td><?php echo $transporter['area_covers']; ?></td>
														<td><a href="" class="btn btn-sm btn-primary btn-edit-dept showallupdatebtn" data-transporter_id="<?php echo $transporter['transporter_id']; ?>"><span class="fa fa-edit"></span></a></td>
													</tr>
												<?php endforeach ?>
											<?php endif ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

				</div> <!-- end of search_branch -->
			</div>
		</div>
	</div>
</div>
</div>