<?php

$desc = $this->session->userdata('desc');
$desc = json_decode($desc);
$desc = objectToArray($desc);

$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page_title">Add Staff Department</h1>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="col-md-12">
				<ul class="nav nav-tabs" id="tabs_a">
					<li class="active"><a data-toggle="tab" href="#add_dept">Add Update Staff Department</a></li>
					<li class=""><a data-toggle="tab" href="#view_all">View All</a></li>
				</ul>
				<div class="tab-content">
					<div id="add_dept" class="tab-pane fade active in">

						<div class="row">
							<div class="col-lg-12">
								<div class="panel panel-default">
									<div class="panel-body">

										<form action="">

											<div class="row">
												<div class="col-lg-2">
													<div class="input-group">
														<div class="input-group-addon id-addon">Dept. Id</div>
														<input type="number" class="form-control input-sm num" id="txtId">
														<input type="hidden" id="txtIdHidden">
														<input type="hidden" id="txtMaxIdHidden">
														<input type="hidden" id="vouchertypehidden">
														
														<input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
														<input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
														<input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">

													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<div class="input-group-addon txt-addon">Name</div>
														<input type="text" class="form-control input-sm" id="txtName">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-4">
													<div class="input-group">
														<div class="input-group-addon txt-addon">Description</div>
														<input type="text" class="form-control input-sm" id="txtDescription">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-4">
													<div class="input-group">
														<div class="input-group-addon txt-addon">Salary Acc.</div>
														<select class="form-control select2" id="salaryAccount" >
	                                                        <option value="" disabled="" selected="">Choose party</option>
	                                                        <?php foreach ($salary_accounts as $party): ?>
	                                                            <option value="<?php echo $party['pid']; ?>" data-address="<?php echo $party['address']; ?>" data-uaddress="<?php echo $party['uaddress']; ?>" data-uname="<?php echo $party['uname']; ?>" data-pbalance="<?php echo $party['pbalance']; ?>"><?php echo $party['name']; ?></option>
	                                                        <?php endforeach ?>
	                                                    </select>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-12">
													<div class="pull-right">
														<a class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['warehouse']['insert']; ?>' data-updatebtn='<?php echo $vouchers['warehouse']['update']; ?>' data-deletebtn='<?php echo $vouchers['warehouse']['delete']; ?>' data-printbtn='<?php echo $vouchers['warehouse']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
														<a class="btn btn-sm btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
													</div>
												</div> 	<!-- end of col -->
											</div>	<!-- end of row -->
										</form>	<!-- end of form -->

									</div>	<!-- end of panel-body -->
								</div>	<!-- end of panel -->
							</div>  <!-- end of col -->
						</div>	<!-- end of row -->

					</div>	<!-- end of add_branch -->
					<div id="view_all" class="tab-pane fade">

						<div class="row">
							<div class="col-lg-12">
								<div class="panel panel-default">
									<div class="panel-body">
										<table class="table table-striped table-hover ar-datatable">
											<thead>
												<tr>
													<th style='background: #368EE0;'>Sr#</th>
													<th style='background: #368EE0;'>Name</th>
													<th style='background: #368EE0;'>Description</th>
													<th style='background: #368EE0;'>Salary Account</th>
													<th style='background: #368EE0;'></th>
												</tr>
											</thead>
											<tbody>
												<?php $counter = 1; foreach ($staffdepartments as $departments): ?>
												<tr>
													<td><?php echo $counter++; ?></td>
													<td><?php echo $departments['name']; ?></td>
													<td><?php echo $departments['description']; ?></td>
													<td data-pid="<?= $departments['pid'] ?>"><?php echo $departments['party_name']; ?></td>
													<td><a href="" class="btn btn-sm btn-primary btn-edit-dept" data-deptid="<?php echo $departments['deptid']; ?>"><span class="fa fa-edit"></span></a></td>
												</tr>
											<?php endforeach ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

				</div> <!-- end of search_branch -->
			</div>
		</div>
	</div>
</div>
</div>