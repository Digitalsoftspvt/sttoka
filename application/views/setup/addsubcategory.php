<?php

    $desc = $this->session->userdata('desc');
    $desc = json_decode($desc);
    $desc = objectToArray($desc);

    $vouchers = $desc['vouchers'];
?>
<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page_title">Add Sub Category</h1>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="col-md-12">
				<ul class="nav nav-tabs" id="tabs_a">
					<li class="active"><a data-toggle="tab" href="#add_category">Add/Update Sub Category</a></li>
					<li class=""><a data-toggle="tab" href="#view_all">View All</a></li>
				</ul>
				<div class="tab-content">
					<div id="add_category" class="tab-pane fade active in">

						<div class="row">
							<div class="col-lg-12">
								<div class="panel panel-default">
									<div class="panel-body">

										<form action="">

											<div class="row">
												<div class="col-lg-2">
													<div class="input-group">
														<div class="input-group-addon id-addon">Id (Auto)</div>
														<input type="number" class="form-control input-sm num" id="txtId">
														<input type="hidden" id="txtIdHidden">
														<input type="hidden" id="txtMaxIdHidden">
														<input type="hidden" id="vouchertypehidden">
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-4">
													<div class="input-group">
														<div class="input-group-addon txt-addon">Name</div>
														<input type="text" class="form-control input-sm" id="txtName">
													</div>
												</div>
											</div>
	
											<div class="row">
												<div class="col-lg-4">
													<div class="input-group">
														<span class="input-group-addon">Category</span>
														<select class="form-control input-sm input-sm" id="category_dropdown">
															<option value="" disabled="" selected="">Choose Category</option>
															<?php foreach ($categories as $category): ?>
																<option value="<?php echo $category['catid']; ?>"><?php echo $category['name']; ?></option>
															<?php endforeach ?>
														</select>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-4">
													<div class="input-group">
														<div class="input-group-addon txt-addon">Description</div>
														<input type="text" class="form-control input-sm" id="txtDescription">
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-12">
													<div class="pull-right">
														<!-- <a class="btn btn-default btn-sm btnSave"><i class="fa fa-save"></i> Save Changes</a> -->
														<a class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['subcatagory']['insert']; ?>' data-updatebtn='<?php echo $vouchers['subcatagory']['update']; ?>' data-deletebtn='<?php echo $vouchers['subcatagory']['delete']; ?>' data-printbtn='<?php echo $vouchers['subcatagory']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
														<a class="btn btn-default btn-sm btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
													</div>
												</div> 	<!-- end of col -->
											</div>	<!-- end of row -->
										</form>	<!-- end of form -->

									</div>	<!-- end of panel-body -->
								</div>	<!-- end of panel -->
							</div>  <!-- end of col -->
						</div>	<!-- end of row -->

					</div>	<!-- end of add_branch -->
					<div id="view_all" class="tab-pane fade">

						<div class="row">
							<div class="col-lg-12">
								<div class="panel panel-default">
									<div class="panel-body">
										<table class="table table-striped table-hover ar-datatable">
											<thead>
												<tr>
													<th style='background: #368EE0;'>Sr#</th>
													<th style='background: #368EE0;'>Category</th>
													<th style='background: #368EE0;'>Subcategory</th>
													<th style='background: #368EE0;'>Description</th>
													<th style='background: #368EE0;'></th>
												</tr>
											</thead>
											<tbody>
												<?php if (isset($subcategories)): ?>
													<?php $counter = 1; foreach ($subcategories as $subcategory): ?>
														<tr>
															<td><?php echo $counter++; ?></td>
															<td><?php echo $subcategory['category_name']; ?></td>
															<td><?php echo $subcategory['name']; ?></td>
															<td><?php echo $subcategory['description']; ?></td>
															<td><a href="" class="btn btn-sm btn-primary btn-edit-cat" data-subcatid="<?php echo $subcategory['subcatid']; ?>"><span class="fa fa-edit"></span></a></td>
														</tr>
													<?php endforeach ?>
												<?php endif ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

					</div> <!-- end of search_branch -->
				</div>
			</div>
		</div>
	</div>
</div>