<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);

	$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-lg-3">
				<h1 class="page_title">Allot Shift Group</h1>
			</div>
			<div class="col-lg-9">
				<div class="pull-right">
					<a class="btn btn-default btnSave" data-insertbtn='<?php echo $vouchers['allot_shift_group']['insert']; ?>' data-updatebtn='<?php echo $vouchers['allot_shift_group']['update']; ?>'><i class="fa fa-save"></i> Save Changes</a>
					<a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset</a>
				</div>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="col-md-12">
				<ul class="nav nav-tabs" id="tabs_a">
					<li class=""><a data-toggle="tab" href="#add_allotshiftgroup">Add Update Allot Shift Group</a></li>
					<li class="active"><a data-toggle="tab" href="#view_all">List</a></li>
				</ul>
				<div class="tab-content">
					<div id="add_allotshiftgroup" class="tab-pane fade">

						<div class="row">
							<div class="col-lg-12">
								<div class="panel panel-default">
									<div class="panel-body">

										<form action="">

											<div class="row">
												<div class="col-lg-2">
													<div class="input-group">
														<div class="input-group-addon id-addon">Id</div>
														<input type="number" class="form-control num txtidupdate" data-txtidupdate='<?php echo $vouchers['allot_shift_group']['update']; ?>' id="txtId">
														<input type="hidden" id="txtIdHidden">
														<input type="hidden" id="txtMaxIdHidden">
														<input type="hidden" id="voucher_type_hidden">
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-3">
				                                    <div class="input-group">
				                                        <span class="input-group-addon txt-addon">Date</span>
				                                        <input class="form-control " type="date" id="cur_date" value="<?php echo date('Y-m-d'); ?>">
				                                    </div>
				                                </div>
			                                </div>
			                                
											<div class="row">
				                                <div class="col-lg-3">
				                                   <div class="input-group">
				                                        <span class="input-group-addon txt-addon shiftgroup-addon" data-gid=''>Shift Group</span>
				                                        <select class="form-control" id="shiftgroup_dropdown">
				                                          	<option value="" disabled="" selected="">Choose shift group</option>
				                                          	<?php foreach ($shiftGroups as $shiftGroup): ?>
				                                              	<option value="<?php echo $shiftGroup['gid']; ?>"><?php echo $shiftGroup['name']; ?></option>
				                                          	<?php endforeach ?>
				                                        </select>
				                                    </div>
				                                </div>
			                               	</div>

											<div class="row">
				                                <div class="col-lg-3">
				                                   <div class="input-group">
				                                        <span class="input-group-addon txt-addon">Shift</span>
				                                        <select class="form-control" id="shift_dropdown">
				                                          	<option value="" disabled="" selected="">Choose shift</option>
				                                          	<?php foreach ($shifts as $shift): ?>
				                                              	<option value="<?php echo $shift['shid']; ?>"><?php echo $shift['name']; ?></option>
				                                          	<?php endforeach ?>
				                                        </select>
				                                    </div>
				                                </div>
			                                </div>


											<div class="row">
												<div class="col-lg-12">
													<div class="pull-right">
														<a class="btn btn-default btnSave" data-insertbtn='<?php echo $vouchers['allot_shift_group']['insert']; ?>'><i class="fa fa-save"></i> Save Changes</a>
														<a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset</a>
													</div>
												</div> 	<!-- end of col -->
											</div>	<!-- end of row -->
										</form>	<!-- end of form -->

									</div>	<!-- end of panel-body -->
								</div>	<!-- end of panel -->
							</div>  <!-- end of col -->
						</div>	<!-- end of row -->

					</div>	<!-- end of add_branch -->
					<div id="view_all" class="tab-pane fade active in">

						<div class="row">
							<div class="col-lg-12">
								<div class="panel panel-default">
									<div class="panel-body">
										<table class="table table-striped table-hover ar-datatable">
											<thead>
												<tr>
													<th>Sr#</th>
													<th>Shift Group</th>
													<th>Shift</th>
													<th>Time In</th>
													<th>Time Out</th>
													<th>Rest Time Out</th>
													<th>Rest Time Out</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<?php $counter = 1; foreach ($allotShiftGroups as $allotShiftGroup): ?>
													<tr>
														<td><?php echo $counter++; ?></td>
														<td><?php echo substr($allotShiftGroup['shiftgroup_name'], 0, 10); ?></td>
														<td><?php echo $allotShiftGroup['shift_name']; ?></td>
														<td><?php echo $allotShiftGroup['tin']; ?></td>
														<td><?php echo $allotShiftGroup['tout']; ?></td>
														<td><?php echo $allotShiftGroup['resin']; ?></td>
														<td><?php echo $allotShiftGroup['resout']; ?></td>
														<!-- <td><a href="" class="btn btn-primary btn-edit-allotShiftGroup showallupdatebtn" data-showallupdatebtn=<?php echo $vouchers['allot_shift_group']['update']; ?> data-id="<?php echo $allotShiftGroup['id']; ?>"><span class="fa fa-edit"></span></a></td> -->
														<td><a href="" class="btn btn-primary btn-edit-allotShiftGroup showallupdatebtn" data-showallupdatebtn="" data-id="<?php echo $allotShiftGroup['id']; ?>"><span class="fa fa-edit"></span></a></td>
													</tr>
												<?php endforeach ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

					</div> <!-- end of search_branch -->
				</div>
			</div>
		</div>
	</div>
</div>