<?php
    $desc = $this->session->userdata('desc');
    $desc = json_decode($desc);
    $desc = objectToArray($desc);
    $vouchers = $desc['vouchers'];
?>
<style>

</style>	
<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page_title">Add Financial Year</h1>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="col-md-12">
				<ul class="nav nav-tabs" id="tabs_a">
					<?php $dataentry=""; $viewall=''; 
          		$dataentry="active";
          			?>
					<li class="<?php echo $dataentry;?>"><a data-toggle="tab" href="#add_transporter">Add Update Financial Year</a></li>
					<li class="<?php echo $viewall;?>"><a data-toggle="tab" href="#view_all">View All</a></li>
				</ul>
				<div class="tab-content">
					<div id="add_transporter" class="tab-pane <?php echo $dataentry;?> fade  in ">

						<div class="row">
							<div class="col-lg-12">
								<div class="panel panel-default">
									<div class="panel-body">

										<form action="">

											<div class="row">
												<div class="col-lg-2">
													<div class="input-group">
														<div class="input-group-addon id-addon">Vr#</div>
														<input type="number" class="form-control input-sm  num txtidupdate" id="txtId">
														<input type="hidden" id="txtIdHidden">
														<input type="hidden" id="txtMaxIdHidden">
														<input type="hidden" id="vouchertypehidden">
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-4">
													<div class="input-group">
														<div class="input-group-addon txt-addon">Year Name</div>
														
														<input type="text" class="form-control input-sm " id="txtName">
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-4">
													<div class="input-group">
														<div class="input-group-addon txt-addon">Start Date</div>
														<input type="text" class="form-control input-sm ts_datepicker" id="txtContact">
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-4">
													<div class="input-group">
														<div class="input-group-addon txt-addon">End Date</div>
														<input type="text" class="form-control input-sm ts_datepicker" id="txtPhone">
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-4">
													<div class="input-group">
														<div class="input-group-addon txt-addon">Remarks</div>
														<input type="text" class="form-control input-sm " id="txtAreaCover">
													</div>
												</div>
											</div>
										
											<div class="row">
												<div class="col-lg-12">
													<div class="pull-right">
														<a class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['finanicalyear']['insert']; ?>' data-updatebtn='<?php echo $vouchers['finanicalyear']['update']; ?>' data-deletebtn='<?php echo $vouchers['finanicalyear']['delete']; ?>' data-printbtn='<?php echo $vouchers['finanicalyear']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
														<a class="btn btn-sm btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
													</div>
												</div> 	<!-- end of col -->
											</div>	<!-- end of row -->
										</form>	<!-- end of form -->

									</div>	<!-- end of panel-body -->
								</div>	<!-- end of panel -->
							</div>  <!-- end of col -->
						</div>	<!-- end of row -->

					</div>	<!-- end of add_branch -->
					<div id="view_all" class="tab-pane <?php echo $viewall;?> fade  in ">

						<div class="row">
							<div class="col-lg-12">
								<div class="panel panel-default">
									<div class="panel-body">
										<table class="table table-striped table-hover ">
											<thead>
												<tr>
													<th style='background: #368EE0;'>Sr#</th>
													<th style='background: #368EE0;'>Financial Name</th>
													<th style='background: #368EE0;'>Start Date</th>
													<th style='background: #368EE0;'>End Date</th>
													<th style='background: #368EE0;'>Remarks</th>
													<th style='background: #368EE0;'></th>
												</tr>
											</thead>
											<tbody>
												<?php if (count($finanicalyears) > 0): ?>
													<?php $counter = 1; foreach ($finanicalyears as $transporter): ?> 
														<tr>
															<td><?php echo $counter++; ?></td>
															<td><?php echo $transporter['fname']; ?></td>
															<td><?php echo substr($transporter['start_date'],0,10); ?></td>
															<td><?php echo substr($transporter['end_date'], 0,10); ?></td>
															<td><?php echo $transporter['remarks']; ?></td>
															<td><a href="" class="btn btn-sm btn-primary btn-edit-dept showallupdatebtn" data-transporter_id="<?php echo $transporter['fn_id']; ?>" style="width:40px !important;"><span class="fa fa-edit"></span></a></td>
														</tr>
													<?php endforeach ?>
												<?php endif ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

					</div> <!-- end of search_branch -->
				</div>
			</div>
		</div>
	</div>
</div>