<?php

    $desc = $this->session->userdata('desc');
    $desc = json_decode($desc);
    $desc = objectToArray($desc);

    $vouchers = $desc['vouchers'];
?>
<!-- main content -->
<div id="main_wrapper">

    <div class="page_bar">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page_title">Recipe Costing</h1>
            </div>
        </div>
    </div>

    <div class="page_content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <form action="">

                                        <div class="row">
                                            <div class="col-lg-2">
                                                <div class="input-group">
                                                    <span class="input-group-addon id-addon">Sr#</span>
                                                    <input type="number" class="form-control" id="txtId" >
                                                    <input type="hidden" id="txtMaxIdHidden">
                                                    <input type="hidden" id="txtIdHidden">
                                                    <input type="hidden" id="vouchertypehidden">
                                                    <input type="hidden" id="vrdate">
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="input-group">
                                                    <span class="input-group-addon id-addon">Recipe#</span>
                                                    <input type="text" class="form-control" id="txtReceipeId" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">                                                
                                                <label>Finished Item *</label>
                                                <select class="form-control select2" id="recipe_dropdown">
                                                    <option value="" disabled="" selected="">Choose item</option>
                                                    <?php foreach ($items as $item): ?>
                                                        <option value="<?php echo $item['item_id']; ?>"><?php echo $item['item_des']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                                <input type="hidden" class="form-control" id="txtRecipeHidden">
                                            </div>
                                            <div class="col-lg-3">                                                
                                                <label>Noted By</label>
                                                <input type="text" class="form-control" id="txtNotedBy">                                                
                                            </div>
                                            <div class="col-lg-5">
                                                <label>Remarks</label>
                                                <input type="text" class="form-control" id="txtRemarks">
                                            </div>
                                        </div>

                                        <div class="row"></div>

                                        <div class="container-wrap">
                                            <div class="row">
                                                <div class="col-lg-2">                                     
                                                    <label>Item Code</label>
                                                    <select class="form-control select2" id="itemid_dropdown">
                                                        <option value="" disabled="" selected="">Item Id</option>
                                                        <?php foreach ($items as $item): ?>
                                                            <option value="<?php echo $item['item_id']; ?>" data-grweight="<?php echo $item['grweight']; ?>" data-uom="<?php echo $item['uom']; ?>"><?php echo $item['item_code']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                                <div class="col-lg-4">
                                                    <label>Description</label>
                                                    <select class="form-control select2" id="item_dropdown">
                                                        <option value="" disabled="" selected="">Item description</option>
                                                        <?php foreach ($items as $item): ?>
                                                            <option value="<?php echo $item['item_id']; ?>" data-grweight="<?php echo $item['grweight']; ?>"  data-uom="<?php echo $item['uom']; ?>"><?php echo $item['item_des']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                                <div class="col-lg-1">
                                                    <label>Uom</label>
                                                    <input type="text" class="form-control" id="txtUom" readonly="" tabindex="-1">
                                                </div>
                                                <div class="col-lg-1">
                                                    <label>Qty</label>                                                    
                                                    <input type="text" class="form-control num" id="txtQty">                                                    
                                                </div>
                                                 <div class="col-lg-1">
                                                    <label>GW</label>                                                    
                                                    <input type="text" class="form-control readonly num" id="txtGWeight" readonly="" tabindex="-1">
                                                </div>
                                                <div class="col-lg-1">
                                                    <label>Weight</label>
                                                    <input type="text" class="form-control num" id="txtWeight">
                                                </div>
                                                <div class="col-lg-1" >
                                                    <label>Add</label>
                                                    <a href="" class="btn btn-sm btn-primary" id="btnAdd"><i class="fa fa-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row"></div>
                                        <div class="row"></div>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <table class="table table-striped" id="recipe_table">
                                                    <thead>
                                                        <tr>
                                                            <th class="hsr">Sr#</th>
                                                            <th class="hitem">Item Detail</th>
                                                            <th class="huom">Uom</th>
                                                            <th class='text-right hqty'>Qty</th>
                                                            <th class='text-right hweight'>Weight</th>
                                                            <th class='text-center'>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="saleRows">

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </form> <!-- end of form -->

                                </div>  <!-- end of panel-body -->
                            </div>  <!-- end of panel -->
                        </div>  <!-- end of col -->
                    </div>  <!-- end of row -->
<div id="party-lookup" class="modal fade modal-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h3 id="myModalLabel"><i class='fa fa-search'></i> Party Lookup</h3>
            </div>

                <div class="modal-body">
                <table class="table table-striped modal-table">
                <!-- <table class="table table-bordered table-striped modal-table"> -->
                <thead>
                <tr style="font-size:16px;">
                <th>Id</th>
                <th>Name</th>
                <th>Mobile</th>
                <th>Address</th>
                <th style='width:3px;'>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($parties as $party): ?>
                <tr>
                <td width="14%;">
                <?php echo $party['account_id']; ?>
                <input type="hidden" name="hfModalPartyId" value="<?php echo $party['pid']; ?>">
                </td>
                <td><?php echo $party['name']; ?></td>
                <td><?php echo $party['mobile']; ?></td>
                <td><?php echo $party['address']; ?></td>
                <td><a href="#" data-dismiss="modal" class="btn btn-primary populateAccount"><i class="glyphicon glyphicon-ok"></i></a></td>
                </tr>
                <?php endforeach ?>
                </tbody>
                </table>
                </div>
                <div class="modal-footer">
                <!-- <button class="btn btn-danger delete-modal-del">Delete</button> -->
                <button class="btn btn-primary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div id="item-lookup" class="modal fade modal-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h3 id="myModalLabel"><i class='fa fa-search'></i> Item Lookup</h3>
            </div>

                <div class="modal-body">
                <table class="table table-striped modal-table">
                <!-- <table class="table table-bordered table-striped modal-table"> -->
                <thead>
                <tr style="font-size:16px;">
                <th>Id</th>
                <th>Description</th>
                <th>Code</th>
                <th>Uom</th>
                <th style='width:3px;'>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($items as $item): ?>
                <tr>
                <td width="14%;">
                <?php echo $item['item_id']; ?>
                <input type="hidden" name="hfModalitemId" value="<?php echo $item['item_id']; ?>">
                </td>
                <td><?php echo $item['item_des']; ?></td>
                <td><?php echo $item['item_code']; ?></td>
                <td><?php echo $item['uom']; ?></td>
                <td><a href="#" data-dismiss="modal" class="btn btn-primary populateItem"><i class="glyphicon glyphicon-ok"></i></a></td>
                </tr>
                <?php endforeach ?>
                </tbody>
                </table>
                </div>
                <div class="modal-footer">
                <!-- <button class="btn btn-danger delete-modal-del">Delete</button> -->
                <button class="btn btn-primary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-2">                                                    
                                            <label>Total Qty</label>
                                            <input type="text" class="form-control num" id="txtTotalQty">                                                    
                                        </div>
                                        <div class="col-lg-2">
                                            <label>Total Weight</label>
                                            <input type="text" class="form-control num" id="txtTotalWeight">
                                        </div>
                                        <!-- <div class="col-lg-4"></div> -->
                                        <div class="col-lg-2">
                                            <label>Finished Qty</label>
                                            <input type="text" class="form-control num" id="txtFQty">
                                        </div>
                                        <div class="col-lg-2">
                                            <label>Finished Weight</label>
                                            <input type="text" class="form-control num" id="txtFWeight">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <!-- <div class="row"></div>
                                            <div class="row"></div>
                                            <div class="row"></div>
                                            <div class="row"></div> -->
                                            <a class="btn btn-sm btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                                            <a class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['recipee']['insert']; ?>' data-updatebtn='<?php echo $vouchers['recipee']['update']; ?>' data-deletebtn='<?php echo $vouchers['recipee']['delete']; ?>' data-printbtn='<?php echo $vouchers['recipee']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
                                            <a class="btn btn-sm btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>
                                            <a class="btn btn-sm btn-default btnPrint"><i class="fa fa-print"></i> Print F9</a>
                                            <!-- <a href="#party-lookup" data-toggle="modal" class="btn btn-sm btn-default btnsearchparty "><i class="fa fa-search"></i>&nbsp;Account Lookup F1</a> -->
                                            <a href="#item-lookup" data-toggle="modal" class="btn btn-sm btn-default btnsearchitem "><i class="fa fa-search"></i>&nbsp;Item Lookup F2</a>
                                        </div>
                                    </div>  <!-- end of row -->
                                </div>
                            </div>
                        </div>
                    </div>

                </div>  <!-- end of level 1-->
            </div>
        </div>
    </div>
</div>