<?php

    $desc = $this->session->userdata('desc');
    $desc = json_decode($desc);
    $desc = objectToArray($desc);

    $vouchers = $desc['vouchers'];
?>
<!-- main content -->
<div id="main_wrapper">

    <div class="page_bar">
        <div class="row">
            <div class="col-md-5">
                <h1 class="page_title"> Item Phase Recepie</h1>
            </div>
            <div class="col-md-7">
               <div class="pull-right">
                    <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                    <a class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['itemphaseRecepie']['insert']; ?>' data-updatebtn='<?php echo $vouchers['itemphaseRecepie']['update']; ?>' data-deletebtn='<?php echo $vouchers['itemphaseRecepie']['delete']; ?>' data-printbtn='<?php echo $vouchers['itemphaseRecepie']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
                    <a class="btn btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>
                    <!-- <div class="btn-group">
                            <button type="button" class="btn btn-primary btn-sm btnPrint" ><i class="fa fa-save"></i>Print F9</button>
                            <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li ><a href="#" class="btnPrint"> Print with header</li>
                                <li ><a href="#" class="btnprintwithOutHeader"> Print with Out header</li>
                            </ul>
                    </div> -->
               </div>
            </div>
        </div>
    </div>

    <div class="page_content">
        <div class="container-fluid">

            <div class="row">

                

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <form action="">

                                        <div class="row">
                                            <div class="col-lg-2">
                                                <div class="input-group">
                                                    <span class="input-group-addon id-addon">Sr#</span>
                                                    <input type="number" class="form-control" id="txtVrnoa" >
                                                    <input type="hidden" id="txtMaxVrnoaHidden">
                                                    <input type="hidden" id="txtVrnoaHidden">
                                                    <input type="hidden" id="voucher_type_hidden">

                                                    <input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                                                    <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                                                    <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-3 col-lg-offset-1">
                                                <div class="input-group">
                                                    <span class="input-group-addon txt-addon">Date</span>
                                                    <input class="form-control ts_datepicker" type="text" id="current_date">
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="input-group">
                                                    <span class="input-group-addon txt-addon">Vr#</span>
                                                    <input type="text" class="form-control" id="txtVrno" readonly='true'>
                                                    <input type="hidden" id="txtMaxVrnoHidden">
                                                    <input type="hidden" id="txtVrnoHidden">
                                                </div>
                                            </div> 
                                        
                                        </div>
                                        <div class="row">
                                            <!--<div class="col-lg-3">
                                                    <label id="stqty_lbl" >Item From</label>
                                                    <select class="form-control select2" id="itemFrom_dropdown">
                                                        <option value="" disabled="" selected="">Item description</option>
                                                        <?php foreach ($items as $item): ?>
                                                            <option value="<?php echo $item['item_id']; ?>" data-uom="<?php echo $item['uom']; ?>" data-stqty="<?php echo $item['stqty']; ?>" data-stweight="<?php echo $item['stweight']; ?>"><?php echo $item['item_des']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>-->
                                            <div class="col-lg-3">
                                                    <label id="stqty_lbl" >Item To</label>
                                                    <select class="form-control select2" id="itemTo_dropdown">
                                                        <option value="" disabled="" selected="">Item description</option>
                                                        <?php foreach ($items as $item): ?>
                                                            <option value="<?php echo $item['item_id']; ?>" data-uom="<?php echo $item['uom']; ?>" data-stqty="<?php echo $item['stqty']; ?>" data-stweight="<?php echo $item['stweight']; ?>"><?php echo $item['item_des']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                           
                                        </div>

                                        <!-- <div class="row">
                                            
                                        </div> -->
                                        <div class="row"></div>
                                        <div class="container-wrap">
                                            <div class="row">
                                                
                                                <div class="col-lg-4">
                                                    <label id="stqty_lbl" >Phase</label>
                                                    <select class="form-control select2" id="phase_dropdown">
                                                    <option value="" disabled="" selected="">Choose Phase</option>
                                                    <?php foreach ($phases as $phas): ?>
                                                    <option value="<?php echo $phas['id']; ?>"><?php echo $phas['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                                </div>
                                                <!-- <div class="col-lg-2">
                                                    <label>Department</label>
                                                    <select class="form-control select2" id="dept_dropdown">
                                                        <option value="" disabled="" selected="">Department</option>
                                                        <?php foreach ($departments as $department): ?>
                                                            <option value="<?php echo $department['did']; ?>"><?php echo $department['name']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div> -->
                                                <div class="col-lg-2" >
                                                    <label>Phase#</label>
                                                    <input type="text" class="form-control num" id="txtPhaseNo" >
                                                </div>
                                                <!-- <div class="col-lg-2" >
                                                    <label>Rate</label>
                                                    <input type="text" class="form-control num" id="txtRate">
                                                </div> -->
                                                <!-- <div class="col-lg-2" >
                                                    <label>Amount</label>
                                                    <input type="text" class="form-control num" id="txtAmount">
                                                </div> -->
                                                <div class="col-lg-2" style="margin-top:30px;">
                                                    <!-- <label>Add</label>                                                     -->
                                                    <a href="" class="btn btn-primary" id="btnAdd"><i class="fa fa-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                       <!--  <div class="row"></div>
                                        <div class="row"></div> -->

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <table class="table table-striped" id="purchase_table">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:100px;">Sr#</th>
                                                            <th>Phase</th>
                                                            <th style="width:200px;">Phase#</th>
                                                            <th style="width:130px;">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                    
                                                </table>
                                            </div>
                                        </div>

                                    </form> <!-- end of form -->

                                </div>  <!-- end of panel-body -->
                            </div>  <!-- end of panel -->
                        </div>  <!-- end of col -->
                    </div>  <!-- end of row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="col-lg-12">
                                                <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                                                <a class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['itemphaseRecepie']['insert']; ?>' data-updatebtn='<?php echo $vouchers['itemphaseRecepie']['update']; ?>' data-deletebtn='<?php echo $vouchers['itemphaseRecepie']['delete']; ?>' data-printbtn='<?php echo $vouchers['itemphaseRecepie']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
                                                <a class="btn btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>
                                        
                                            </div>
                                        </div>  <!-- end of col -->
                                    </div>  <!-- end of row -->
                                </div>
                            </div>
                        </div>
                    </div>

                
                 <div class="col-md-2">
                </div>
            </div>
        </div>
    </div>
</div>