<?php

    $desc = $this->session->userdata('desc');
    $desc = json_decode($desc);
    $desc = objectToArray($desc);

    $vouchers = $desc['vouchers'];
?>
<!-- main content -->
<div id="main_wrapper">

    <div class="page_bar">
        <div class="row">
            <div class="col-md-4">
                <h1 class="page_title">Extra Days Voucher</h1>
            </div>
            <div class="col-md-8">
                <div class="pull-right">
                    <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                    <a class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['extradayvoucher']['insert']; ?>' data-updatebtn='<?php echo $vouchers['extradayvoucher']['update']; ?>' data-deletebtn='<?php echo $vouchers['extradayvoucher']['delete']; ?>' data-printbtn='<?php echo $vouchers['extradayvoucher']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
                    <a class="btn btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary btn-sm btnPrint" ><i class="fa fa-save"></i>Print F9</button>
                       <!--  <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                         <span class="caret"></span>
                         <span class="sr-only">Toggle Dropdown</span>
                       </button>
                       <ul class="dropdown-menu" role="menu">
                           <li ><a href="#" class="btnPrint"> Print with header</li>
                           <li ><a href="#" class="btnprintwithOutHeader"> Print with Out header</li>
                       </ul> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page_content">
        <div class="container-fluid">

            <div class="row">

                

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <form action="">

                                        <div class="row">
                                            <div class="col-lg-2">
                                                <div class="input-group">
                                                    <span class="input-group-addon id-addon">Vr#</span>
                                                    <input type="number" class="form-control" id="txtVrnoa" >
                                                    <input type="hidden" id="txtMaxVrnoaHidden">
                                                    <input type="hidden" id="txtVrnoaHidden">
                                                    <input type="hidden" id="voucher_type_hidden">

                                                    <input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                                                    <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                                                    <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-3 col-lg-offset-1">
                                                <div class="input-group">
                                                    <span class="input-group-addon txt-addon">Date</span>
                                                    <input class="form-control ts_datepicker" type="text" id="current_date">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <label id="stqty_lbl" > Department</label>
                                                <select class="form-control select2" id="dept_dropdown">
                                                    <option value="" disabled="" selected="">Department</option>
                                                    <?php foreach ($staffdepartments as $department): ?>
                                                        <option value="<?php echo $department['deptid']; ?>"><?php echo $department['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-3">
                                                <label id="stqty_lbl" > Godown</label>
                                                <select class="form-control select2" id="godown_dropdown">
                                                    <option value="" disabled="" selected="">Godown</option>
                                                    <?php foreach ($departments as $department): ?>
                                                        <option value="<?php echo $department['did']; ?>"><?php echo $department['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                            <!-- <div class="col-lg-3">
                                                <label id="stqty_lbl" >Employee</label>
                                                <select class="form-control select2" id="employee_dropdown">
                                                    <option value="" disabled="" selected="">Choose Employee</option>
                                                    <?php foreach ($employee as $emp): ?>
                                                    <option value="<?php echo $emp['pid']; ?>"><?php echo $emp['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div> -->
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-9">
                                                <label>Remarks</label>
                                                <input type="text" class="form-control" id="txtRemarks">
                                            </div>
                                        </div>
                                        <div class="row"></div>
                                        <div class="container-wrap">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <label id="stqty_lbl" >Employee name</label>
                                                    <select class="form-control select2" id="employee1_dropdown">
                                                    <option value="" disabled="" selected="">Choose Employee</option>
                                                    <?php foreach ($employee as $emp): ?>
                                                    <option value="<?php echo $emp['pid']; ?>" data-per_day_rate="<?php echo $emp['per_day_rate'] ?>"><?php echo $emp['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                                </div>
                                                <div class="col-lg-2" >
                                                    <label>Extra Days</label>
                                                    <input type="text" class="form-control num" id="txtExtraDays" >
                                                </div>
                                                <div class="col-lg-1">
                                                    <label>Rate</label>
                                                    <input type="text" class="form-control num" id="txtRate">
                                                </div>
                                                <div class="col-lg-1" style="width:140px;">
                                                    <label>Amount</label>
                                                    <input type="text" class="form-control num" id="txtAmount">
                                                </div>
                                                <div class="col-lg-2">
                                                    <label>Add</label>                                                    
                                                    <a href="" class="btn btn-primary" id="btnAdd"><i class="fa fa-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <table class="table table-striped" id="purchase_table">
                                                    <thead>
                                                        <tr>
                                                            <th>Sr#</th>
                                                            <th>Employee Name</th>
                                                            <th class="text-right">Extra Days</th>
                                                            <th class="text-right">Rate</th>
                                                            <th class="text-right">Amount</th>
                                                            <th class='text-center'>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                    <tfoot>
                                                        <td></td>
                                                        <td style="color:red; float:right"><b>Total</b></td>
                                                        <td id="txtTotextradays"class="text-right" style="color:red;"></td>
                                                        <td></td>
                                                        <td id="txtTotAmount" class="text-right" style="color:red;"></td>
                                                        <td></td>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>

                                    </form> <!-- end of form -->

                                </div>  <!-- end of panel-body -->
                            </div>  <!-- end of panel -->
                        </div>  <!-- end of col -->
                    </div>  <!-- end of row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-lg-8" style="margin-top:25px;">
                                            <div class="col-lg-12">
                                                <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                                                <a class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['extradayvoucher']['insert']; ?>' data-updatebtn='<?php echo $vouchers['extradayvoucher']['update']; ?>' data-deletebtn='<?php echo $vouchers['extradayvoucher']['delete']; ?>' data-printbtn='<?php echo $vouchers['extradayvoucher']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
                                                <a class="btn btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary btn-sm btnPrint" ><i class="fa fa-save"></i>Print F9</button>
                                                   <!--  <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                     <span class="caret"></span>
                                                     <span class="sr-only">Toggle Dropdown</span>
                                                   </button>
                                                   <ul class="dropdown-menu" role="menu">
                                                       <li ><a href="#" class="btnPrint"> Print with header</li>
                                                       <li ><a href="#" class="btnprintwithOutHeader"> Print with Out header</li>
                                                       </ul> -->
                                                </div>
                                            </div>
                                        </div>  <!-- end of col -->
                                       <!--  <div class="col-lg-2">
                                            <div class="input-group">
                                                <span class="input-group-addon id-addon" >Qty</span>
                                                <input type="text" class="form-control num" id="txtGQty" readonly="true">
                                            </div>
                                        </div> -->
                                        <!-- <div class="col-lg-2">
                                            <div class="input-group">
                                                <span class="input-group-addon id-addon" >Weight</span>
                                                <input type="text" class="form-control num" id="txtGWeight" readonly="true">
                                            </div>
                                        </div> -->
                                        <div class="col-lg-4">
                                                    <label id="stqty_lbl" >Expense Account</label>
                                                    <select class="form-control select2" id="expense_dropdown">
                                                    <option value="" disabled="" selected=""> Expense Account</option>
                                                    <?php foreach ($accounts as $acc): ?>
                                                    <option value="<?php echo $acc['pid']; ?>"><?php echo $acc['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                                </div>
                                    </div>  <!-- end of row -->
                                </div>
                            </div>
                        </div>
                    </div>

                
                 <div class="col-md-2">
                </div>
            </div>
        </div>
    </div>
</div>