s<?php
    $desc = $this->session->userdata('desc');
    $desc = json_decode($desc);
    $desc = objectToArray($desc);
    $vouchers = $desc['vouchers'];

    // echo '<script type="text/javascript">alert("'.$desc.'")</script>';
?>
<!-- main content -->
<div id="main_wrapper">
    <div class="page_bar">
        <div class="row">
            <div class="col-md-5">
                <h1 class="page_title">Head Moulding Voucher</h1>
            </div>  
            <div class="col-md-7">
                <div class="pull-right">
                    <a class="btn btn-sm btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                    <a class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['headmouldingvoucher']['insert']; ?>' data-updatebtn='<?php echo $vouchers['headmouldingvoucher']['update']; ?>' data-deletebtn='<?php echo $vouchers['headmouldingvoucher']['delete']; ?>' data-printbtn='<?php echo $vouchers['headmouldingvoucher']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
                    <a class="btn btn-sm btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary btn-sm btnPrint" ><i class="fa fa-save"></i>Print F9</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page_content">
        <div class="container-fluid">
            <div class="row ">
                <div class="col-lg-12">
                    <ul class="nav nav-pills">
                        <li class="active"><a href="#Main" data-toggle="tab">Main</a></li>
                        <li><a href="#Attachments" data-toggle="tab">Attachments</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="tab-content">
                                <div class="tab-pane active fade in" id="Main">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <form action="">
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <div class="input-group">
                                                    <span class="input-group-addon id-addon">Sr#</span>
                                                    <input type="number" class="form-control " id="txtVrnoa" >
                                                    <input type="hidden" id="txtMaxVrnoaHidden">
                                                    <input type="hidden" id="txtVrnoaHidden">
                                                    <input type="hidden" id="voucher_type_hidden">

                                                    <input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                                                    <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                                                    <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">

                                                    <input type="hidden" id="purchaseid" value="<?php echo $setting_configur[0]['purchase']; ?>">
                                                    <input type="hidden" id="discountid" value="<?php echo $setting_configur[0]['discount']; ?>">
                                                    <input type="hidden" id="expenseid" value="<?php echo $setting_configur[0]['expenses']; ?>">
                                                    <input type="hidden" id="taxid" value="<?php echo $setting_configur[0]['tax']; ?>">
                                                    <input type="hidden" id="cashid" value="<?php echo $setting_configur[0]['cash']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-lg-offset-1">
                                                <div class="input-group">
                                                    <span class="input-group-addon">Date</span>
                                                    <input class="form-control  ts_datepicker" type="text" id="current_date">
                                                    <input class="form-control  ts_datepicker" type="hidden" id="vrdate">
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="input-group">
                                                    <span class="input-group-addon">Vr#</span>
                                                    <input type="text" class="form-control " id="txtVrno" readonly='true'>
                                                    <input type="hidden" id="txtMaxVrnoHidden">
                                                    <input type="hidden" id="txtVrnoHidden">
                                                </div>
                                            </div>                                                
                                        </div>
                                        <div class="row">   
                                           <!--  <div class="col-lg-3">                                                
                                                <label>Employee Name</label>
                                                <div class="input-group" >
                                                    <select class="form-control select2" id="employee_dropdown">
                                                        <option value="" disabled="" selected="">Choose employee</option>
                                                        <?php foreach ($accountEmployee as $employee): ?>
                                                            <option value="<?php echo $employee['pid']; ?>"><?php echo $employee['name']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>   
                                                    <a href="#party-lookup" data-toggle="modal" class="input-group-addon btn btn-primary active" style="min-width:40px !important;"><i class="fa fa-search"></i></a>
                                                    
                                                </div>
                                                                                             
                                            </div> -->
                                            <div class="col-lg-3">                                                
                                                <label>Warehouse</label>
                                                <select class="form-control select2" id="dept_dropdown">
                                                    <option value="" selected="" disabled="">Choose Warehouse</option>
                                                    <?php foreach ($departments as $department): ?>
                                                        <option value="<?php echo $department['did']; ?>"><?php echo $department['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>                            
                                            </div>
                                            <div class="col-lg-3">                                                
                                                <label>Received By</label>
                                                <input class='form-control ' type='text' list="receivers" id='receivers_list'>
                                                <datalist id='receivers'>
                                                    <?php foreach ($receivers as $receiver): ?>
                                                        <option value="<?php echo $receiver['received_by']; ?>">
                                                    <?php endforeach ?>
                                                </datalist>                                                
                                            </div>                                    
                                           <!--  <div class="col-lg-1">                                                
                                                <label>Inv#</label>
                                                <input type="text" class="form-control  num" id="txtInvNo">                                                
                                            </div>   -->                                   
                                            <!-- <div class="col-lg-3">                                                
                                                <label>Through</label>
                                                <select class="form-control  select2" id="transporter_dropdown">
                                                    <option value="" disabled="" selected="">Choose transporter</option>
                                                    <?php //foreach ($transporters as $transporter): ?>
                                                        <option value="<?php //echo $transporter['transporter_id']; ?>"><?php //echo $transporter['name']; ?></option>
                                                    <?php //endforeach ?>
                                                </select>                                               
                                            </div> -->
                                        </div>                                        
                                        <div class="row">                                                                        
                                            <div class="col-lg-9">                                            
                                                <label>Remarks</label>
                                                <input type="text" class="form-control " id="txtRemarks">                                                                                                                                                                             
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <ul class="nav nav-pills">
                                                    <li class="active"><a href="#itemadd" data-toggle="tab">Raw Item</a></li>
                                                    <li><a href="#itemless" data-toggle="tab">Costing</a></li>
                                                    <li><a href="#overhead" data-toggle="tab" id="overHads">Over Head</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">                        
                                                <div class="tab-content" style="margin-top: -30px !important;">
                                                    <div class="tab-pane active fade in" id="itemadd">
                                                        <div class="container-wrap">
                                                            <div class="row">
                                                                <!-- <div class="col-lg-2">
                                                                    <label for="">Item Code</label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon" style='min-width: 0px;'><span class="fa fa-barcode"></span></span>
                                                                        <select class="form-control select2" id="itemid_dropdown">
                                                                            <option value="" disabled="" selected="">Item Code</option>
                                                                            <?php foreach ($items as $item): ?>
                                                                                <option value="<?php echo $item['item_id']; ?>" data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-srate="<?php echo $item['srate']; ?>" data-grweight="<?php echo $item['grweight']; ?>" data-stqty="<?php echo $item['stqty']; ?>" data-stweight="<?php echo $item['stweight']; ?>"><?php echo $item['item_code']; ?></option>
                                                                            <?php endforeach ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4" >
                                                                    <label for="">Item Description</label>
                                                                    <div class="input-group" >
                                                                    <select class="form-control select2" id="item_dropdown">
                                                                        <option value="" disabled="" selected="">Item description</option>
                                                                        <?php foreach ($items as $item): ?>
                                                                            <option value="<?php echo $item['item_id']; ?>" data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>"><?php echo $item['item_des']; ?></option>
                                                                        <?php endforeach ?>
                                                                    </select>
                                                                    <a id="it_raw" href="#item-lookup" data-toggle="modal" class="input-group-addon btn btn-primary active btnsearchitem" style="min-width:40px !important;"><i class="fa fa-search"></i></a>
                                                                    <input type="hidden" id="hiddenFieldValue" >
                                                                    </div>
                                                                </div> -->
                                                                <div class="col-lg-4" >
                                                                    <label for="">Item Code List <img id="imgItemLoader" class="hide" src="<?php echo base_url(); ?>/assets/img/loader.gif"></label>
                                                                    <input type="text" class="form-control" id="txtItemId">
                                                                    <input id="hfItemId" type="hidden" value="" />
                                                                    <input id="hfItemSize" type="hidden" value="" />
                                                                    <input id="hfItemBid" type="hidden" value="" />
                                                                    <input id="hfItemUom" type="hidden" value="" />
                                                                    <input id="hfItemPrate" type="hidden" value="" />
                                                                    <input id="hfItemGrWeight" type="hidden" value="" />
                                                                    <input id="hfItemStQty" type="hidden" value="" />
                                                                    <input id="hfItemStWeight" type="hidden" value="" />
                                                                    <input id="hfItemLength" type="hidden" value="" />
                                                                    <input id="hfItemCatId" type="hidden" value="" />
                                                                    <input id="hfItemSubCatId" type="hidden" value="" />
                                                                    <input id="hfItemDesc" type="hidden" value="" />
                                                                    <input id="hfItemUname" type="hidden" value="" />
                                                                    <input id="hfItemShortCode" type="hidden" value="" />
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <label for="" id="stqty_lbl">Item</label>
                                                                    <input type="text" class="form-control" id="itemDesc" readonly="">
                                                                </div>
                                                                
                                                                <div class="col-lg-1">
                                                                    <label for="">Mould Qty</label>                                                    
                                                                    <input type="text" class="form-control num" id="txtQty">                                                    
                                                                </div>
                                                                <div class="col-lg-1">
                                                                    <label for="">GW</label>                                                    
                                                                    <input type="text" class="form-control readonly num" id="txtGWeight" readonly="" tabindex="-1">                                                    
                                                                </div>
                                                                <div class="col-lg-1">
                                                                    <label for="">Uom</label>                                                    
                                                                    <input type="text" class="form-control readonly num" id="txtUom" readonly="" tabindex="-1">                                                    
                                                                </div>
                                                                <!-- <div class="col-lg-1 wid_130">
                                                                    <label for="">Mould Weight</label>                                                    
                                                                    <input type="text" class="form-control num" id="txtWeight">                                                    
                                                                </div> -->
                                                                <div class="col-lg-1 wid_130">
                                                                    <label for="">T.M.Weight</label>                                                    
                                                                    <input type="text" class="form-control num" id="txtTotWeight">                                                    
                                                                </div>
                                                                                                               
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-1">
                                                                    <label for="">Actual Qty</label>                                                    
                                                                    <input type="text" class="form-control num" id="txtAQty">                                                    
                                                                </div>
                                                                <div class="col-lg-1">
                                                                    <label for="">Actual Wt</label>                                                    
                                                                    <input type="text" class="form-control num" id="txtAWeight">                                                    
                                                                </div>
                                                                <div class="col-lg-1">
                                                                    <label for="">Waste Qty</label>                                                    
                                                                    <input type="text" class="form-control num" id="txtWQty">                                                    
                                                                </div>
                                                                <div class="col-lg-1" style="">
                                                                    <label for="">Waste Wt</label>                                                    
                                                                    <input type="text" class="form-control num" id="txtWWeight">                                                    
                                                                </div>
                                                               
                                                                <div class="col-lg-1" style='margin-top: 30px;'>                                                    
                                                                    <a href="" class="btn btn-primary" id="btnAdd"><i class='fa fa-plus'></i></a>
                                                                </div> 
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div id="no-more-tables">
                                                                    <table class="col-lg-12 table table-bordered table-striped table-condensed cf" id="purchase_table">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="numeric">Sr#</th>
                                                                                <th >Item Detail</th>
                                                                                <th class="numeric">M.Qty</th>  
                                                                                <!-- <th class="numeric">M.Weight</th> -->
                                                                                <th class="numeric">M.T.Weight</th>
                                                                                <th class="numeric">Actual Qty</th>  
                                                                                <th class="numeric">Actual Weight</th>
                                                                                <th class="numeric">Weast Qty</th>
                                                                                <th class="numeric">Weast Weight</th>
                                                                                <th>Action</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="itemless">
                                                        <div class="container-wrap">
                                                            <div class="row">
                                                                <!-- <div class="col-lg-2">
                                                                    <label for="">Item Code</label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon" style='min-width: 0px;'><span class="fa fa-barcode"></span></span>
                                                                        <select class="form-control select2" id="itemid_dropdownL">
                                                                            <option value="" disabled="" selected="">Item Code</option>
                                                                            <?php foreach ($items as $item): ?>
                                                                                <option value="<?php echo $item['item_id']; ?>" data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-srate="<?php echo $item['srate']; ?>" data-grweight="<?php echo $item['grweight']; ?>" data-stqty="<?php echo $item['stqty']; ?>" data-stweight="<?php echo $item['stweight']; ?>" data-mould="<?php echo $item['srate3']; ?>" data-dharry="<?php echo $item['srate4']; ?>" data-newitem="<?php echo $item['item_des']; ?>"><?php echo $item['item_code']; ?></option>
                                                                            <?php endforeach ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <label for="" id="stqty_lbl">Item Description</label>                      
                                                                    <div class="input-group" >
                                                                        <select class="form-control select2" id="item_dropdownL">
                                                                            <option value="" disabled="" selected="">Item description</option>
                                                                            <?php foreach ($items as $item): ?>
                                                                                <option value="<?php echo $item['item_id']; ?>" data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-srate="<?php echo $item['srate']; ?>"  data-grweight="<?php echo $item['grweight']; ?>" data-stqty="<?php echo $item['stqty']; ?>" data-stweight="<?php echo $item['stweight']; ?>" data-mould="<?php echo $item['srate3']; ?>" data-dharry="<?php echo $item['srate4']; ?>" ><?php echo $item['item_des']; ?></option>
                                                                            <?php endforeach ?>
                                                                        </select>
                                                                        <a id="it_costing" href="#item-lookup" data-toggle="modal" class="input-group-addon btn btn-primary active btnsearchitem" style="min-width:40px !important;"><i class="fa fa-search"></i></a>
                                                                    </div>
                                                                </div> -->
                                                                <div class="col-lg-4" >
                                                                    <label for="">Item Code List <img id="imgItemLoaderL" class="hide" src="<?php echo base_url(); ?>/assets/img/loader.gif"></label>
                                                                    <input type="text" class="form-control" id="txtItemIdL">
                                                                    <input id="hfItemIdL" type="hidden" value="" />
                                                                    <input id="hfItemSizeL" type="hidden" value="" />
                                                                    <input id="hfItemBidL" type="hidden" value="" />
                                                                    <input id="hfItemUomL" type="hidden" value="" />
                                                                    <input id="hfItemPrateL" type="hidden" value="" />
                                                                    <input id="hfItemGrWeightL" type="hidden" value="" />
                                                                    <input id="hfItemStQtyL" type="hidden" value="" />
                                                                    <input id="hfItemStWeightL" type="hidden" value="" />
                                                                    <input id="hfItemLengthL" type="hidden" value="" />
                                                                    <input id="hfItemCatIdL" type="hidden" value="" />
                                                                    <input id="hfItemSubCatIdL" type="hidden" value="" />
                                                                    <input id="hfItemDescL" type="hidden" value="" />
                                                                    <input id="hfItemUnameL" type="hidden" value="" />
                                                                    <input id="hfItemShortCodeL" type="hidden" value="" />
                                                                    <input id="hfItemMouldRate" type="hidden" value="" />
                                                                    <input id="hfItemDharyRate" type="hidden" value="" />
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <label for="" id="stqty_lblL">Item</label>
                                                                    <input type="text" class="form-control" id="itemDescL" readonly="" tabindex="-1">
                                                                </div>
                                                                <div class="col-lg-1 wid_130">
                                                                    <label for="">Qty</label>                                                    
                                                                    <input type="text" class="form-control num" id="txtQtyL">                                                    
                                                                    <input type="hidden" id="txtHiddenSpareQty">
                                                                    <input type="hidden" id="txtHiddenSpareWeight">
                                                                    <input type="hidden" id="txtHiddenStockdetailArray">
                                                                </div>
                                                                <div class="col-lg-1">
                                                                    <label for="">GW</label>                                                    
                                                                    <input type="text" class="form-control readonly num" id="txtGWeightL" readonly="" tabindex="-1">                                                    
                                                                </div>
                                                                <div class="col-lg-1">
                                                                    <label for="">Uom</label>                                                    
                                                                    <input type="text" class="form-control readonly num" id="txtUomL" readonly="" tabindex="-1">                                                    
                                                                </div>
                                                                <div class="col-lg-1 wid_130">
                                                                    <label for="">Weight</label>                                                    
                                                                    <input type="text" class="form-control num" id="txtWeightL">                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-1">
                                                                    <label for="">Mould@</label>                                                    
                                                                    <input type="text" class="form-control num" id="txtPRateL">                                                    
                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <label for="">M Amount</label>                                                    
                                                                    <input type="text" class="form-control readonly num" id="txtAmountL" readonly="true" tabindex="-1">                                                    
                                                                </div>
                                                                <div class="col-lg-1">
                                                                    <label for="">Dhary@</label>                                                    
                                                                    <input type="text" class="form-control num" id="txtDharyRateL">
                                                                </div>
                                                                <div class="col-lg-2 wid_130">
                                                                    <label for="">D Amount</label>                                                    
                                                                    <input type="text" class="form-control readonly num" id="txtDharyAmountL" readonly="true" tabindex="-1">
                                                                </div>
                                                                <div class="col-lg-1" style='margin-top: 30px;'>                                                    
                                                                    <a href="" class="btn btn-primary" id="less_btnAdd"><i class="fa fa-plus"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <table class="table table-striped" id="moulding_tableL">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Sr#</th>
                                                                            <th>Item Detail</th>
                                                                            <th>Qty</th>
                                                                            <th>Weight</th>
                                                                            <th>Mould@</th>
                                                                            <th>M Amount</th>
                                                                            <th>Dhary@</th>
                                                                            <th>D Amount</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="overhead">
                                                        <!-- <div class="container-wrap">
                                                            
                                                        </div> -->
                                                        <div class="row">
                                                            <div class="col-lg-9">
                                                                <h4 class="text-muted">Cost This Bhatti</h4>
                                                                <table class="table " id="conversionTable">
                                                                    <thead>
                                                                        <tr>
                                                                            <td>Serial</td>
                                                                            <td>Item</td>
                                                                            <td>Rate</td>
                                                                            <td>Weight/Qty</td>
                                                                            <td>Value</td>
                                                                        </tr>
                                                                    </thead>
                                                                  <tbody>
                                                                    <tr>
                                                                        <td>1</td>
                                                                        <td>Daig</td>
                                                                      <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" ></td>
                                                                      <td class="secondTd"><input type="text" class="form-control text-center secVal" id="totalWeights"></td>
                                                                      <td class="thirdTd"><input type="text" class="form-control text-center"  disabled="true" ></td>
                                                                      <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" ></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>2</td>
                                                                        <td>Coal **</td>
                                                                      <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" ></td>
                                                                      <td class="secondTd"><input type="text" class="form-control text-center secVal"  ></td>
                                                                      <td class="thirdTd"><input type="text" class="form-control text-center"  disabled="true"></td>
                                                                      <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" ></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>3</td>
                                                                        <td>Gola **</td>
                                                                      <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" ></td>
                                                                      <td class="secondTd"><input type="text" class="form-control text-center secVal"  ></td>
                                                                      <td class="thirdTd"><input type="text" class="form-control text-center"  disabled="true"></td>
                                                                      <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" ></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>4</td>
                                                                        <td>Silicon **</td>
                                                                      <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" ></td>
                                                                      <td class="secondTd"><input type="text" class="form-control text-center secVal"  ></td>
                                                                      <td class="thirdTd"><input type="text" class="form-control text-center"  disabled="true" disabled="true"></td>
                                                                      <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" ></td>
                                                                    </tr>
                                                                    <tr>
                                                                         <td>5</td>
                                                                        <td>Electricity </td>
                                                                      <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" ></td>
                                                                      <td class="secondTd"><input type="text" class="form-control text-center secVal"  ></td>
                                                                      <td class="thirdTd"><input type="text" class="form-control text-center"  disabled="true"></td>
                                                                      <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" ></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>6</td>
                                                                        <td>Red Clay </td>
                                                                      <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" ></td>
                                                                      <td class="secondTd"><input type="text" class="form-control text-center secVal"  ></td>
                                                                      <td class="thirdTd"><input type="text" class="form-control text-center"  disabled="true"></td>
                                                                      <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" ></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>7</td>
                                                                        <td>Crushed Coal ** </td>
                                                                      <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" ></td>
                                                                      <td class="secondTd"><input type="text" class="form-control text-center secVal"  ></td>
                                                                      <td class="thirdTd"><input type="text" class="form-control text-center"  disabled="true"></td>
                                                                      <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" ></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>8</td>
                                                                        <td>Sand </td>
                                                                      <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" ></td>
                                                                      <td class="secondTd"><input type="text" class="form-control text-center secVal"  ></td>
                                                                      <td class="thirdTd"><input type="text" class="form-control text-center"  disabled="true"></td>
                                                                      <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" ></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>9</td>
                                                                        <td>Carosine Oil </td>
                                                                      <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" value="" ></td>
                                                                      <td class="secondTd"><input type="text" class="form-control text-center secVal" ></td>
                                                                      <td class="thirdTd"><input type="text" class="form-control text-center" value="" disabled="true" ></td>
                                                                      <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" ></td>
                                                                      
                                                                    </tr>
                                                                    <tr>
                                                                        <td>10</td>
                                                                        <td>Diesel </td>
                                                                      <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" ></td>
                                                                      <td class="secondTd"><input type="text" class="form-control text-center secVal"  ></td>
                                                                      <td class="thirdTd"><input type="text" class="form-control text-center"  disabled="true"></td>
                                                                      <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" ></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td>12</td>
                                                                        <td>Medison,Nails,Thread,Other </td>
                                                                      <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" ></td>
                                                                      <td class="secondTd"><input type="text" class="form-control text-center secVal"  ></td>
                                                                      <td class="thirdTd"><input type="text" class="form-control text-center"  disabled="true"></td>
                                                                      <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" ></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td>13</td>
                                                                        <td>Wood Shool + Other </td>
                                                                      <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" ></td>
                                                                      <td class="secondTd"><input type="text" class="form-control text-center secVal"  ></td>
                                                                      <td class="thirdTd"><input type="text" class="form-control text-center"  disabled="true"></td>
                                                                      <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" ></td>
                                                                      
                                                                    </tr>
                                                                    <tr>
                                                                    <td>14</td>
                                                                        <td>Sariya </td>
                                                                      <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" ></td>
                                                                      <td class="secondTd"><input type="text" class="form-control text-center secVal"  ></td>
                                                                      <td class="thirdTd"><input type="text" class="form-control text-center"  disabled="true"></td>
                                                                      <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" ></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td>15</td>
                                                                        <td>Black Led ** </td>
                                                                      <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" ></td>
                                                                      <td class="secondTd"><input type="text" class="form-control text-center secVal" ></td>
                                                                      <td class="thirdTd"><input type="text" class="form-control text-center"  disabled="true"></td>
                                                                      <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" ></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td>16</td>
                                                                        <td>Dhoora </td>
                                                                      <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" ></td>
                                                                      <td class="secondTd"><input type="text" class="form-control text-center secVal"  ></td>
                                                                      <td class="thirdTd"><input type="text" class="form-control text-center"  disabled="true"></td>
                                                                      <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" ></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td>17</td>
                                                                        <td>Clay Trali</td>
                                                                      <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" ></td>
                                                                      <td class="secondTd"><input type="text" class="form-control text-center secVal"  ></td>
                                                                      <td class="thirdTd"><input type="text" class="form-control text-center"  disabled="true"></td>
                                                                      <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" ></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td>18</td>
                                                                        <td>Generator</td>
                                                                      <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" ></td>
                                                                      <td class="secondTd"><input type="text" class="form-control text-center secVal"  ></td>
                                                                      <td class="thirdTd"><input type="text" class="form-control text-center"  disabled="true"></td>
                                                                      <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" ></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td>19</td>
                                                                        <td>Misc Expense</td>
                                                                      <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" ></td>
                                                                      <td class="secondTd"><input type="text" class="form-control text-center secVal"  ></td>
                                                                      <td class="thirdTd"><input type="text" class="form-control text-center"  disabled="true"></td>
                                                                      <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" ></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td>20</td>
                                                                        <td>Labour Moulder</td>
                                                                      <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" ></td>
                                                                      <td class="secondTd"><input type="text" class="form-control text-center secVal"  ></td>
                                                                      <td class="thirdTd"><input type="text" class="form-control text-center"  disabled="true"></td>
                                                                      <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" ></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td>21</td>
                                                                        <td>Labour Dheri</td>
                                                                      <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" ></td>
                                                                      <td class="secondTd"><input type="text" class="form-control text-center secVal"  ></td>
                                                                      <td class="thirdTd"><input type="text" class="form-control text-center"  disabled="true"></td>
                                                                      <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" ></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td>22</td>
                                                                        <td>Waste</td>
                                                                      <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" ></td>
                                                                      <td class="secondTd"><input type="text" class="form-control text-center secVal"  ></td>
                                                                      <td class="thirdTd"><input type="text" class="form-control text-center"  disabled="true"></td>
                                                                      <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" ></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td>23</td>
                                                                        <td>Juth/Hath</td>
                                                                      <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" ></td>
                                                                      <td class="secondTd"><input type="text" class="form-control text-center secVal"  ></td>
                                                                      <td class="thirdTd"><input type="text" class="form-control text-center"  disabled="true"></td>
                                                                      <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" ></td>
                                                                    </tr>
                                                                  </tbody>  
                                                                  <tfoot>
                                                                    <!-- <tr>
                                                                      <td colspan="2"><input type="text" class="form-control text-center" value="GRAND TOTAL" disabled="true"></td>
                                                                      <td><input type="text" class="form-control text-center" id="txtTotal" ></td>
                                                                    </tr> -->
                                                                  </tfoot>
                                                                </table>
                                                            </div><br><br><br><br><br><br><br>
                                                            <div class="col-lg-3">
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <span><b>Total Expenses</b></span>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <span id='totalExpense'></span>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <span><b>Total Weight</b></span>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <span id='totalWeight'></span>
                                                                    </div>
                                                                </div>
                                                                <div class="row" >
                                                                    <div class="col-lg-6"></div>
                                                                    <div class="col-lg-6" style="border:1px solid !important;width:100px !important;">
                                                                        <span id='total'></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="party-lookup" class="modal fade modal-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                        <h3 id="myModalLabel"><i class='fa fa-search'></i> Party Lookup</h3>
                                                    </div>
                                                    <div class="modal-body mrgtop">
                                                        <table class="table table-striped modal-table">
                                                            <thead>
                                                                <tr style="font-size:16px;">
                                                                <th>Id</th>
                                                                <th>Name</th>
                                                                <th>Mobile</th>
                                                                <th>Address</th>
                                                                <th style='width:3px;'>Actions</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php foreach ($parties as $party): ?>
                                                                <tr>
                                                                <td width="14%;">
                                                                <?php echo $party['account_id']; ?>
                                                                <input type="hidden" name="hfModalPartyId" value="<?php echo $party['pid']; ?>">
                                                                </td>
                                                                <td><?php echo $party['name']; ?></td>
                                                                <td><?php echo $party['mobile']; ?></td>
                                                                <td><?php echo $party['address']; ?></td>
                                                                <td><a href="#" data-dismiss="modal" class="btn btn-primary populateAccount"><i class="glyphicon glyphicon-ok"></i></a></td>
                                                                </tr>
                                                                <?php endforeach ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn btn-primary pull-right" data-dismiss="modal">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="item-lookup" class="modal fade modal-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h3 id="myModalLabel">item Lookup</h3>
            </div>

                <div class="modal-body">
                <div style="width:250px; float: right !important;top:-10px; position: relative; bottom:30px;">
                    <label for="">Search</label>
                    <input type="text" class="form-control" id="txtItemSearch">
                </div>
                <table class="table" id="tbItems">
                <!-- <table class="table table-bordered table-striped modal-table"> -->
                <thead>
                <tr style="font-size:16px;">
                    <th>Id</th>
                    <th>Code</th>
                    <th>Description</th>
                    <th>Uom</th>
                    <th style='width:3px;'>Actions</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
                </table>
                <div id="divNoRecord" class="divNoRecord"></div>
                    <div id="paging"></div>
                </div>
                <div class="modal-footer">
                <!-- <button class="btn btn-danger delete-modal-del">Delete</button> -->
                <button class="btn btn-primary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
    </div>
                                    </form> <!-- end of form -->
                                </div>  <!-- end of panel-body -->
                            </div>  <!-- end of panel -->
                            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                     <div class="row">

                                        <div class="col-lg-3">
                                            
                                        </div>
                                         <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon fixwid">Emp Name</span>
                                                <select class="form-control select2" id="Memployee_dropdown">
                                                        <option value="" disabled="" selected="">Choose employee</option>
                                                        <?php foreach ($accountEmployee as $employee): ?>
                                                            <option value="<?php echo $employee['pid']; ?>"><?php echo $employee['name']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                            </div>
                                        </div>
                                       <!--  <div class="col-lg-3">
                                             <div class="input-group">
                                                <span class="input-group-addon fixwid">Mould Paid</span>
                                                <input type="text" class="form-control num" id="txtMPaid">
                                            </div>   
                                        </div> -->
                                         <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon fixwid">Emp Name</span>
                                                <select class="form-control select2" id="Demployee_dropdown">
                                                        <option value="" disabled="" selected="">Choose employee</option>
                                                        <?php foreach ($accountEmployee as $employee): ?>
                                                            <option value="<?php echo $employee['pid']; ?>"><?php echo $employee['name']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">                                    
                                        <div class="col-lg-3">   
                                             <div class="input-group">
                                                 <div class="input-group">
                                                    <span class="input-group-addon id-addon fixwidn">Total Weight</span>
                                                    <input type="text" class="form-control readonly num" id="txtTotalWeight" readonly="true" tabindex="-1">
                                                </div>
                                            </div>                                                 
                                        </div>         
                                        <div class="col-lg-3">   
                                            <div class="input-group">
                                                <span class="input-group-addon fixwid">Mould Amount</span>
                                                <input type="text" class=" form-control num"  id="txtMouldAmount" readonly="true" tabindex="-1">
                                                <input type="hidden" id="txtMouldAmountHidden" >
                                            </div>                                                 
                                        </div>       
                                        <div class="col-lg-3">      
                                            <div class="input-group">
                                                <span class="input-group-addon fixwid">Dhary Amount</span>
                                                <input type="text" class=" form-control num"  id="txtDharyGAmount" readonly="true" tabindex="-1">
                                                <input type="hidden" id="txtDharyGAmountHidden" >
                                            </div>                                              
                                        </div>                                                                       
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon fixwid">Expense Account*</span>
                                                <select class="form-control select2" id="exp_dropdown">
                                                    <option value="" disabled="" selected="">Choose</option>
                                                    <?php foreach ($accountExp as $expensess): ?>
                                                        <option value="<?php echo $expensess['pid']; ?>"><?php echo $expensess['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">   
                                        <div class="col-lg-3">   
                                            <div class="input-group">
                                                <span class="input-group-addon id-addon fixwidn">Total Qty</span>
                                                <input type="text" class="form-control readonly num" id="txtTotalQty" readonly="true" tabindex="-1">
                                            </div>                                                 
                                        </div>                                                                                
                                        <div class="col-lg-3">  
                                            <div class="input-group">
                                                <span class="input-group-addon fixwid">Mould Bonus</span>
                                                <input type="text" class=" form-control num"  id="txtMouldBonus">
                                            </div>                                                  
                                        </div>
                                        <div class="col-lg-3">   
                                            <div class="input-group">
                                                <span class="input-group-addon fixwid">Dhary Bonus</span>
                                                <input type="text" class=" form-control num"  id="txtDharyBonus">
                                            </div>                                                 
                                        </div>   
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon fixwid">Cash Account*</span>
                                                <select class="form-control select2" id="cash_dropdown">
                                                    <option value="" disabled="" selected="">Choose</option>
                                                    <?php foreach ($accountCashs as $accountCash): ?>
                                                        <option value="<?php echo $accountCash['pid']; ?>"><?php echo $accountCash['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>                                             
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3">     
                                            <div class="input-group">
                                                <span class="input-group-addon id-addon fixwidn">Net Amount</span>
                                                <input type="text" class="form-control readonly " id='txtNetAmount' readonly="" tabindex="-1">
                                            </div>                                               
                                        </div>  
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon fixwid">Mould Deduction</span>
                                                <input type="text" class=" form-control num"  id="txtMouldDed">
                                            </div>                                                    
                                        </div> 
                                        <div class="col-lg-3">  
                                            <div class="input-group">
                                                <span class="input-group-addon fixwid">Dhary Deduction</span>
                                                <input type="text" class=" form-control num"  id="txtDharyDed">
                                            </div>
                                        </div>
                                      <!--   <div class="col-lg-3">    
                                            <div class="input-group">
                                                <span class="input-group-addon fixwid">Paid</span>
                                                <input type="text" class="form-control num" id="txtPaid">
                                            </div>                                                
                                        </div> -->
                                    </div>
                                    <div class="row">

                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-3">
                                             <div class="input-group">
                                                <span class="input-group-addon fixwid">Mould Paid</span>
                                                <input type="text" class="form-control num" id="txtMPaid">
                                            </div>   
                                        </div>
                                         <div class="col-lg-3">
                                             <div class="input-group">
                                                <span class="input-group-addon fixwid">Dharry Paid</span>
                                                <input type="text" class="form-control num" id="txtDPaid">
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="row">                                                                                    
                                        <div class="col-lg-9">
                                            <a class="btn btn-sm btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                                            <a class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['headmouldingvoucher']['insert']; ?>' data-updatebtn='<?php echo $vouchers['headmouldingvoucher']['update']; ?>' data-deletebtn='<?php echo $vouchers['headmouldingvoucher']['delete']; ?>' data-printbtn='<?php echo $vouchers['headmouldingvoucher']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
                                            <a class="btn btn-sm btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary btn-sm btnPrint" ><i class="fa fa-save"></i>Print F9</button>
                                                <!-- <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                  <span class="caret"></span>
                                                  <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li ><a href="#" class="btnPrint"> Print with header</li>
                                                    <li ><a href="#" class="btnprintwithOutHeader"> Print with Out header</li>
                                                </ul> -->
                                            </div>
                                            <a href="#party-lookup" data-toggle="modal" class="btn btn-sm btn-default btnsearchparty "><i class="fa fa-search"></i>&nbsp;Account Lookup F1</a>
                                           <!--  <a href="#item-lookup" data-toggle="modal" class="btn btn-sm btn-default btnsearchitem "><i class="fa fa-search"></i>&nbsp;Item Lookup F2</a> -->
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon fixwid">User: </span>
                                                <select class="form-control " disabled="" id="user_dropdown" style="width:137px;">
                                                    <option value="" disabled="" selected="">...</option>
                                                    <?php foreach ($userone as $user): ?>
                                                        <option value="<?php echo $user['uid']; ?>"><?php echo $user['uname']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>                                             
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                            </div>  <!-- end of Main -->

                             <div class="tab-pane fade in Attachmentspanel" id="Attachments">
                                <div class="row">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <input type="number" class="form-control" id="txtIdImg" style="background: #fff;" readonly />
                                        </div>
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-primary" id="viewImages">View Attachments <i class="fa fa-eye"></i></button>
                                            <button type="button" class="btn btn-success" id="uploadFiles" data-insert='<?php echo $vouchers['uploadFiles']['insert']; ?>' data-update='<?php echo $vouchers['uploadFiles']['update']; ?>' data-delete='<?php echo $vouchers['uploadFiles']['delete']; ?>' >Update Files <i class="fa fa-upload"></i></button>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                         <form enctype="multipart/form-data" class="dropzone" id="image-upload" method="POST">
                                                <div>
                                                    <h3>Attach Files to Upload</h3>
                                                    <div class="fallback">
                                                        '<input name="file" type="file" />
                                                    </div>
                                                </div>
                                            </form>
                                            <small style="color: #555;"> Supported Files: (.bmp, .gif, .jpg, .jpeg, .png, .pdf, .doc, .docx, .txt, .xls, .xlsx, .rtf)</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            </div>
                            </div>  <!-- end of tab-content -->
                        </div>  <!-- end of col -->
                    </div>  <!-- end of row -->
                    
                </div>
            </div>
        </div>
    </div>
</div>