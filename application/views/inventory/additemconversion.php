<script id="voucher-item-template" type="text/x-handlebars-template">
  <tr>
     <td>{{{VRNOA}}}</td>
     <td>{{VRDATE}}</td>
     <td>{{GODOWN}}</td>
     <td>{{REMARKS}}</td>
     <td>{{TYPE}}</td>
     <td>{{DESCRIPTION}}</td>
     <td class="text-right printRemove">{{UOM}}</td>
     <td class="text-right">{{QTY}}</td>
     <td class="text-right">{{RATE}}</td>
     <td class="text-right" style="text-align:right !important;">{{NETAMOUNT}}</td>
   
  </tr>
</script>
<?php
    $desc = $this->session->userdata('desc');
    $desc = json_decode($desc);
    $desc = objectToArray($desc);

    $vouchers = $desc['vouchers'];
?>

</script>
<!-- main content -->
<div id="main_wrapper">
    <div id="AccountAddModel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="AccountAddModelLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header" style="background:#2477a4;color:white;padding-bottom:20px;">
                    <button type="button" class="modal-button cellRight modal-close pull-right btn-close" data-dismiss="modal"><span class="fa fa-times" style="font-size:26px; "></span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="AccountAddModelLabel">Add New Account</h4>
                </div>
               <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-lg-9 col-lg-offset-1">
                                <form role="form">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label for="exampleInputEmail1">Name</label>
                                                <input type="text" id="txtAccountName" class="form-control" placeholder="Account Name" maxlength="50" tabindex="1">
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Acc Type3</label>
                                                <select class="form-control "  id="txtLevel3" tabindex="2">
                                                    <option value="" disabled="" selected="">Choose Account Type</option>
                                                    <?php foreach ($l3s as $l3): ?>
                                                        <option value="<?php echo $l3['l3']; ?>" data-level2="<?php echo $l3['level2_name']; ?>" data-level1="<?php echo $l3['level1_name']; ?>"><?php echo $l3['level3_name'] ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-12">
                                                <span><b>Type 2 &rarr; </b><span id="txtselectedLevel2"> </span></span> <span><b>Type 1 &rarr; </b><span id="txtselectedLevel1"> </span></span>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>       
                </div>
                <div class="modal-footer">
                    <div class="pull-right">
                        <a class="btn btn-success btnSaveM btn-sm" data-insertbtn="1"><i class="fa fa-save"></i> Save</a>
                        <a class="btn btn-warning btnResetM btn-sm"><i class="fa fa-refresh"></i> Reset</a>
                        <a class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="ItemAddModel" class="modal hide fade" role="dialog" aria-labelledby="ItemAddModelLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header" style="background:#2477a4;color:white;padding-bottom:20px;">
                    <button type="button" class="modal-button cellRight modal-close pull-right btn-close" data-dismiss="modal"><span class="fa fa-times" style="font-size:26px; "></span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="ItemAddModelLabel">Add New Item</h4>
                </div>
               <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-lg-9 col-lg-offset-1">
                                <form role="form">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label for="exampleInputEmail1">Description</label>
                                                <input type="text" id="txtItemName" class="form-control" placeholder="Account Name" maxlength="50" tabindex="1">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label>Category</label>
                                                <select class="form-control select2" id="category_dropdown" tabindex="2">
                                                    <option value="" disabled="" selected="">Choose Category</option>
                                                    <?php foreach ($categories as $category): ?>
                                                        <option value="<?php echo $category['catid']; ?>"><?php echo $category['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Sub Catgeory</label>
                                                <select class="form-control select2" id="subcategory_dropdown" tabindex="3">
                                                    <option value="" disabled="" selected="">Choose sub category</option>
                                                    <?php foreach ($subcategories as $subcategory): ?>
                                                        <option value="<?php echo $subcategory['subcatid']; ?>"><?php echo $subcategory['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>    
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label>Brand</label>
                                                <select class="form-control select2" id="brand_dropdown" tabindex="4">
                                                    <option value="" disabled="" selected="">Choose brand</option>
                                                    <?php foreach ($brands as $brand): ?>
                                                        <option value="<?php echo $brand['bid']; ?>"><?php echo $brand['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Type</label>
                                                <input type="text" list='type' class="form-control" id="txtBarcode" tabindex="5" />
                                                <datalist id='type'>
                                                    <?php foreach ($types as $type): ?>
                                                        <option value="<?php echo $type['barcode']; ?>"></option>
                                                    <?php endforeach ?>
                                                </datalist>
                                            </div>
                                        </div>
                                         <div class="row">
                                            <div class="col-lg-6">
                                                <label>Short Code</label>
                                                <input class="form-control num" type="text" id="txtShortCode" tabindex="6">
                                            </div>
                                            <div class="col-lg-6">
                                                <label>UOM</label>
                                                <input type="text" class='form-control' tabindex="8" placeholder="Uom" id="uom_dropdown" list='uoms'>
                                                <datalist id="uoms">
                                                    <?php foreach ($uoms as $uom): ?>
                                                        <?php if ($uom['uom'] !== ''): ?>
                                                            <option value="<?php echo $uom['uom']; ?>"></option>
                                                        <?php endif ?>
                                                    <?php endforeach ?>
                                                </datalist>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label>Sale Price</label>
                                                <input class="form-control num" type="text" id="txtSalePrice" tabindex="6">
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Pur Price</label>
                                                <input class="form-control num" type="text" id="txtPurPrice" tabindex="7">
                                            </div>
                                        </div>


                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>       
                </div>
                <div class="modal-footer">
                    <div class="pull-right">
                        <a class="btn btn-success btnSaveMItem btn-sm" data-insertbtn="1" tabindex="8"><i class="fa fa-save"></i> Save</a>
                        <a class="btn btn-warning btnResetMItem btn-sm" tabindex="9"><i class="fa fa-refresh"></i> Reset</a>
                        <a class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="GodownAddModel" class="modal hide fade" role="dialog" aria-labelledby="GodownAddModelLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header" style="background:#2477a4;color:white;padding-bottom:20px;">
                    <button type="button" class="modal-button cellRight modal-close pull-right btn-close" data-dismiss="modal"><span class="fa fa-times" style="font-size:26px; "></span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="GodownAddModelLabel">Add New Stock Location</h4>
                </div>
               <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-lg-9 col-lg-offset-1">
                                <form role="form">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label for="exampleInputEmail1">Description</label>
                                                <input type="text" id="txtNameGodownAdd" class="form-control" placeholder="Department Name" maxlength="50" tabindex="1">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>       
                </div>
                <div class="modal-footer">
                    <div class="pull-right">
                        <a class="btn btn-success btnSaveMGodown btn-sm" data-insertbtn="1" tabindex="8"><i class="fa fa-save"></i> Save</a>
                        <a class="btn btn-warning btnResetMGodown btn-sm" tabindex="9"><i class="fa fa-refresh"></i> Reset</a>
                        <a class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="party-lookup" class="modal fade modal-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h3 id="myModalLabel"><i class="fa fa-search"></i> Account Lookup</h3>
                </div>

                    <div class="modal-body mrgtop">
                    <table class="table table-striped modal-table">
                    <!-- <table class="table table-bordered table-striped modal-table"> -->
                    <thead>
                    <tr style="font-size:16px;">
                    <th>Id</th>
                    <th>Name</th>
                    <th>Mobile</th>
                    <th>Address</th>
                    <th style='width:3px;'>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($parties as $party): ?>
                    <tr>
                    <td width="14%;">
                    <?php echo $party['account_id']; ?>
                    <input type="hidden" name="hfModalPartyId" value="<?php echo $party['pid']; ?>">
                    </td>
                    <td><?php echo $party['name']; ?></td>
                    <td><?php echo $party['mobile']; ?></td>
                    <td><?php echo $party['address']; ?></td>
                    <td><a href="#" data-dismiss="modal" class="btn btn-primary populateAccount"><i class="fa fa-search"></i></a></td>
                    </tr>
                    <?php endforeach ?>
                    </tbody>
                    </table>
                    </div>
                    <div class="modal-footer">
                    <!-- <button class="btn btn-danger delete-modal-del">Delete</button> -->
                    <button class="btn btn-primary pull-right" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div id="item-lookup" class="modal fade modal-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h3 id="myModalLabel">item Lookup</h3>
            </div>

                <div class="modal-body">
                <div style="width:250px; float: right !important;top:-10px; position: relative; bottom:30px;">
                    <label for="">Search</label>
                    <input type="text" class="form-control" id="txtItemSearch">
                </div>
                <table class="table" id="tbItems">
                <!-- <table class="table table-bordered table-striped modal-table"> -->
                <thead>
                <tr style="font-size:16px;">
                    <th>Id</th>
                    <th>Code</th>
                    <th>Description</th>
                    <th>Uom</th>
                    <th style='width:3px;'>Actions</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
                </table>
                <div id="divNoRecord" class="divNoRecord"></div>
                    <div id="paging"></div>
                </div>
                <div class="modal-footer">
                <!-- <button class="btn btn-danger delete-modal-del">Delete</button> -->
                <button class="btn btn-primary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

    <div class="page_bar">
        <div class="row">
            <div class="col-md-6">
                <h1 class="page_title"><span class="badge badge-primary badge_style"><i class="fa fa-list"></i></span> Item Conversion Voucher</h1>
            </div>
            <div class="col-lg-6">
                <div class="pull-right">
                    <a class="btn btn-sm btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                    <a class="btn btn-sm btn-default btnSave" data-savegodownbtn='<?php echo $vouchers['warehouse']['insert']; ?>' data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['itemconversion']['insert']; ?>' data-updatebtn='<?php echo $vouchers['itemconversion']['update']; ?>' data-deletebtn='<?php echo $vouchers['itemconversion']['delete']; ?>' data-printbtn='<?php echo $vouchers['itemconversion']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
                    <a class="btn btn-sm btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>
                    
                    <div class="btn-group">
                          <button type="button" class="btn btn-primary btn-sm btnPrint btnPrints" ><i class="fa fa-save"></i>Print F9</button>
                         <!--  <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                           <span class="caret"></span>
                           <span class="sr-only">Toggle Dropdown</span>
                         </button>
                         <ul class="dropdown-menu" role="menu">
                           <li ><a href="#" class="btnprintHeader">With header</a></li>
                           <li ><a href="#" class="btnprintwithOutHeader">With out header</a></li>
                           <li ><a href="#" class="btnprint_sm">Small</a></li>
                           <li ><a href="#" class="btnprint_sm_withOutHeader">Small with out header </a></li>
                           <li ><a href="#" class="btnprint_sm_rate">Small with out rate</a></li>
                           <li ><a href="#" class="btnprint_sm_withOutHeader_rate">Small with out header and rate </a></li>
                           <li ><a href="#" class="btnPrints">Print</a></li>
                         </ul> -->
                    </div>
                    <a href="#item-lookup" data-toggle="modal" class="btn btn-sm btn-info btnsearchitem sr-only"><i class="fa fa-search"></i>&nbsp;Item Lookup F2</a>
                </div>
            </div>
        </div>
    </div>
    <div class="page_content pcontent_style">
        <div class="container-fluid">
            <div class="row ">
                <div class="col-lg-12">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#Main" data-toggle="tab"><i class="fa fa-list"></i> Main</a></li>
                        <li><a href="#Others" data-toggle="tab"><i class="fa fa-cogs"></i> Others</a></li>
                        <li><a href="#Search" data-toggle="tab"><i class="fa fa-search"></i> Search</a></li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="tab-content">
                            <div class="tab-pane active fade in" id="Main">
                                 <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-default" style="margin-top:-31px;">
                                            <div class="panel-body">
                                                <form action="">
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <div class="input-group">
                                                                <span class="input-group-addon id-addon">Sr#</span>
                                                                <input type="number" class="form-control " id="txtVrnoa" >
                                                                <input type="hidden" id="txtMaxVrnoaHidden">
                                                                <input type="hidden" id="txtVrnoaHidden">
                                                                <input type="hidden" id="voucher_type_hidden">
                                                                <input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                                                                <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                                                                <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
                                                                <input type="hidden" id="purchaseid" value="<?php echo $setting_configur[0]['purchase']; ?>">
                                                                <input type="hidden" id="discountid" value="<?php echo $setting_configur[0]['discount']; ?>">
                                                                <input type="hidden" id="itemdiscountid" value="<?php echo isset($setting_configur[0]['itemdiscount'])? $setting_configur[0]['itemdiscount']:""; ?>">
                                                                <input type="hidden" id="expenseid" value="<?php echo $setting_configur[0]['expenses']; ?>">
                                                                <input type="hidden" id="taxid" value="<?php echo $setting_configur[0]['tax']; ?>">
                                                                <input type="hidden" id="cashid" value="<?php echo $setting_configur[0]['cash']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-2">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">Vr#</span>
                                                                <input type="text" class="form-control " id="txtVrno" readonly='true'>
                                                                <input type="hidden" id="txtMaxVrnoHidden">
                                                                <input type="hidden" id="txtVrnoHidden">
                                                            </div>
                                                        </div>                                               
                                                        <div class="col-lg-3">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">Date</span>
                                                                <input class="form-control ts_datepicker" type="text" id="current_date">
                                                                <input class="form-control ts_datepicker" type="hidden" id="vrdate">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">Posting Date</span>
                                                                <input class="form-control " readonly="" type="text" id="posting_date">
                                                            </div>
                                                        </div>
                                                    </div>

                                       
                                                    <div class="row-fluid">
                                                        <h4>Stock Out</h4>
                                                    </div>
                                                    <div class="container-wrap">
                                                       
                                                        <div class="row">
                                                            <div class="col-lg-2">
                                                                <label for="">Item Code</label>
                                                                
                                                                    
                                                                    <!-- <span class="input-group-addon" style='min-width: 0px;'><span class="fa fa-barcode"></span></span> -->
                                                                    <select class="form-control select2" id="itemid_dropdownItemC">
                                                                        <option value="" disabled="" selected="">Item Code</option>
                                                                        <?php foreach ($items as $item): ?>
                                                                            <option value="<?php echo $item['item_id']; ?>" data-size="<?php echo $item['size']; ?>" data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?> " data-stqty="<?php echo $item['stqty']; ?>" data-stweight="<?php echo $item['stweight']; ?>" ><?php echo $item['item_id']; ?></option>
                                                                        <?php endforeach ?>
                                                                    </select>
                                                               
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <label for="" id="stqty_lblItemC">Item</label>
                                                                <div class="input-group" >
                                                                <select class="form-control select2" id="item_dropdownItemC">
                                                                    <option value="" disabled="" selected="">Item description</option>
                                                                    <?php foreach ($items as $item): ?>
                                                                        <option value="<?php echo $item['item_id']; ?>" data-size="<?php echo $item['size']; ?>" data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>" data-stqty="<?php echo $item['stqty']; ?>" data-stweight="<?php echo $item['stweight']; ?>" ><?php echo $item['item_des']; ?></option>
                                                                    <?php endforeach ?>
                                                                </select>
                                                                <input type='hidden' id="txtSizehidden" class="">

                                                                <a class="input-group-addon btn btn-primary active"   tabindex="-1" style="min-width:40px !important;" id="A3" data-target="#ItemAddModel" data-toggle="modal" href="#addItem" rel="tooltip"
                                                                data-placement="top" data-original-title="Add Item" data-toggle="tooltip" data-placement="bottom" title="Add New Account Quick (F3)"><i class="fa fa-plus"></i></a>
                                                                <a id="it_add" href="#item-lookup" data-toggle="modal" class="input-group-addon btn btn-primary active btnsearchitem" style="min-width:40px !important;"><i class="fa fa-search"></i></a>
                                                                <input type="hidden" id="hiddenFieldValue" >
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3">                                                
                                                                <label>Stock Location</label>
                                                                <div class="input-group" >
                                                                <select class="form-control select2" id="dept_dropdownitemc">
                                                                    <option value="" selected="" disabled="">Choose Stock Location</option>
                                                                    <?php foreach ($departments as $department): ?>
                                                                        <option value="<?php echo $department['did']; ?>"><?php echo $department['name']; ?></option>
                                                                    <?php endforeach ?>
                                                                </select>
                                                                <a  tabindex="-1" class="input-group-addon btn btn-primary active" style="min-width:40px !important;" id="A2" data-target="#GodownAddModel" data-toggle="modal" href="#addCategory" rel="tooltip"
                                                                data-placement="top" data-original-title="Add Department" data-toggle="tooltip" data-placement="bottom" title="Add New Department Quick"><i class="fa fa-plus"></i></a>
                                                                </div>                            
                                                            </div>
                                                          <!--   <div class="col-lg-1">
                                                                <label for="">Qty</label>                                                    
                                                                <input type="text" class="form-control num" id="txtQty">                                                    
                                                            </div> -->
                                                            
                                                            <div class="col-lg-2">
                                                                <label for="">Qty</label>                                                    
                                                                <input type="text" class="form-control num" id="txtQtyItemC">                                                    
                                                            </div>
                                                            <div class="col-lg-2">
                                                                <label for="">Weight</label>                                                    
                                                                <input type="text" class="form-control num" id="txtWeightItemC">                                                    
                                                            </div>
                                                            <div class="col-lg-1 hide">
                                                                <label for="">Rate</label>                                                    
                                                                <input type="text" class="form-control num" id="txtPRateItemC">                                                    
                                                            </div>
                                                        </div>
                                                        <div class="row hide">    
                                                            <div class="col-lg-2">
                                                                <label for="">Amount</label>                                                    
                                                                <input type="text" class="form-control readonly num" id="txtAmountItemC" readonly="true" tabindex="-1">                                                    
                                                            </div>
                                                                                                          
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="row-fluid">
                                                        <h4>Stock In</h4>
                                                    </div>

                                                    <div class="container-wrap">
                                                       
                                                        <div class="row">
                                                            <div class="col-lg-2">
                                                                <label for="">Item Code</label>
                                                               
                                                                    
                                                                    <!-- <span class="input-group-addon" style='min-width: 0px;'><span class="fa fa-barcode"></span></span> -->
                                                                    <select class="form-control select2" id="itemid_dropdown">
                                                                        <option value="" disabled="" selected="">Item Code</option>
                                                                        <?php foreach ($items as $item): ?>
                                                                            <option value="<?php echo $item['item_id']; ?>" data-size="<?php echo $item['size']; ?>" data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?> " data-stqty="<?php echo $item['stqty']; ?>" data-stweight="<?php echo $item['stweight']; ?>" ><?php echo $item['item_id']; ?></option>
                                                                        <?php endforeach ?>
                                                                    </select>
                                                                
                                                            </div>
                                                            <div class="col-lg-5">
                                                                <label for="" id="stqty_lbl">Item</label>
                                                                <div class="input-group" >
                                                                <select class="form-control select2" id="item_dropdown">
                                                                    <option value="" disabled="" selected="">Item description</option>
                                                                    <?php foreach ($items as $item): ?>
                                                                        <option value="<?php echo $item['item_id']; ?>" data-size="<?php echo $item['size']; ?>" data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>" data-stqty="<?php echo $item['stqty']; ?>" data-stweight="<?php echo $item['stweight']; ?>" ><?php echo $item['item_des']; ?></option>
                                                                    <?php endforeach ?>
                                                                </select>
                                                                <input type='hidden' id="txtSizehidden" class="">

                                                                <a class="input-group-addon btn btn-primary active"   tabindex="-1" style="min-width:40px !important;" id="A3" data-target="#ItemAddModel" data-toggle="modal" href="#addItem" rel="tooltip"
                                                                data-placement="top" data-original-title="Add Item" data-toggle="tooltip" data-placement="bottom" title="Add New Account Quick (F3)"><i class="fa fa-plus"></i></a>
                                                                <!-- <a href="#item-lookup" data-toggle="modal" class="input-group-addon btn btn-primary"><i class="fa fa-search"></i></a> -->
                                                                <a id="it_less" href="#item-lookup" data-toggle="modal" class="input-group-addon btn btn-primary active btnsearchitem" style="min-width:40px !important;"><i class="fa fa-search"></i></a>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4">                                                
                                                                <label>Stock Location</label>
                                                                <div class="input-group" >
                                                                <select class="form-control select2" id="dept_dropdown">
                                                                    <option value="" selected="" disabled="">Choose Stock Location</option>
                                                                    <?php foreach ($departments as $department): ?>
                                                                        <option value="<?php echo $department['did']; ?>"><?php echo $department['name']; ?></option>
                                                                    <?php endforeach ?>
                                                                </select>
                                                                <a  tabindex="-1" class="input-group-addon btn btn-primary active" style="min-width:40px !important;" id="A2" data-target="#GodownAddModel" data-toggle="modal" href="#addCategory" rel="tooltip"
                                                                data-placement="top" data-original-title="Add Department" data-toggle="tooltip" data-placement="bottom" title="Add New Department Quick"><i class="fa fa-plus"></i></a>
                                                                </div>                            
                                                            </div>
                                                          <!--   <div class="col-lg-1">
                                                                <label for="">Qty</label>                                                    
                                                                <input type="text" class="form-control num" id="txtQty">                                                    
                                                            </div> -->
                                                            <div class="col-lg-1">
                                                                <label for="">Uom</label>                                                    
                                                                <input type="text" class="form-control readonly num" id="txtUom" readonly="" tabindex="-1">                                                    
                                                            </div>
                                                           
                                                                                                          
                                                        </div>
                                                        <div class="row">
                                                             <div class="col-lg-2">
                                                                <label for="">Qty</label>                                                    
                                                                <input type="text" class="form-control num" id="txtQty">                                                    
                                                            </div>
                                                            <div class="col-lg-2">
                                                                <label for="">Weight</label>                                                    
                                                                <input type="text" class="form-control num" id="txtWeight">                                                    
                                                            </div>
                                                             <div class="col-lg-1 hide">
                                                                <label for="">Rate</label>                                                    
                                                                <input type="text" class="form-control num" id="txtPRate">                                                    
                                                            </div>
                                                            <div class="col-lg-2 hide">
                                                                <label for="">Amount</label>                                                    
                                                                <input type="text" class="form-control readonly num" id="txtAmount" readonly="true" tabindex="-1">                                                    
                                                            </div>

                                                            <div class="col-lg-1" style='margin-top: 29px;'>                                                    
                                                                <a href="" class="btn btn-primary" id="btnAdd"><i class="fa fa-plus"></i></a>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <!-- <div class="row"></div> -->
                                                    <!-- <div class="row"></div> -->
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            
                                                            <div id="no-more-tables">
                                                            <table class="col-lg-12 table table-bordered table-striped table-condensed cf" id="purchase_table">
                                                                <thead class="cf tbl_thead">
                                                                    <tr>
                                                                        <th class="text-center hsrno">Sr#</th>
                                                                        <th class="text-center hitem">Item Detail</th>
                                                                        <th class="text-center hgdown">Warehouse</th>
                                                                        <th>Uom</th>
                                                                        <th class="numeric text-right hqty">Qty</th>
                                                                        <th class="numeric text-right hweight">Weight</th>
                                                                        <th class="numeric text-right hide">Rate</th>
                                                                        <th class="numeric text-right hide">Amount</th>
                                                                        <th class="text-center">Action</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>

                                                                </tbody>
                                                                <tfoot class="tfoot_tbl">
                                                                    <tr>
                                                                        <td class="text-right" colspan="4" style="color:red;">Totals</td>
                                                                        <td class="text-right txtTotalQty" style="color:red;"></td>
                                                                        <td class="text-right txtTotalWeight" style="color:red;"></td>
                                                                        <td class="text-right txtTotalRate hide" style="color:red;"></td>
                                                                        <td class="text-right txtTotalAmount hide" style="color:red;"></td>
                                                                        <td></td>
                                                                    </tr>
                                                                </tfoot>
                                                               <!--  <tfoot style='border-top:1px solid !important;'>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td style='color:red !important; text-align:right;'>Total:</td>                                                            
                                                                        <td style='color:red !important;'></td>
                                                                        <td style='color:red !important;'></td>
                                                                        <td></td>                                                                  
                                                                        <td style='color:red !important;'></td>
                                                                        <td></td>
                                                                    </tr>
                                                                </tfoot> -->

                                                            </table>
                                                            </div>
                                                        </div>
                                                    </div>



                                                </form> <!-- end of form -->

                                            </div>  <!-- end of panel-body -->
                                        </div>  <!-- end of panel -->
                                    </div>  <!-- end of col -->
                                 </div>  <!-- end of row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                           
                                           <!--  <div class="row">                                           
                                                <div class="col-lg-1">                                                    
                                                    <label>Discount%</label>
                                                    <input type="text" class=" form-control num"  id="txtDiscount">
                                                </div>                                                
                                                <div class="col-lg-2">                                                    
                                                    <label>DiscAmount</label>
                                                    <input type="text" class=" form-control num"  id="txtDiscAmount">
                                                </div>
                                                <div class="col-lg-1">                                                    
                                                    <label>Expense%</label>
                                                    <input type="text" class=" form-control num"  id="txtExpense">
                                                </div>                                                
                                                <div class="col-lg-2">                                                    
                                                    <label>Exp Amount</label>
                                                    <input type="text" class=" form-control num"  id="txtExpAmount">
                                                </div>
                                                <div class="col-lg-1">                                                    
                                                    <label>Tax%</label>
                                                    <input type="text" class=" form-control num"  id="txtTax">
                                                </div>                                                
                                                <div class="col-lg-2">                                                    
                                                    <label>TaxAmount</label>
                                                    <input type="text" class=" form-control num"  id="txtTaxAmount">
                                                </div>
                                                 <div class="col-lg-3">                                                    
                                                    <label>Paid</label>
                                                    <input type="text" class="form-control num" id="txtPaid">
                                                </div> 
                                            </div> -->
                                            <div class="row">
                                                <div class="col-lg-3">
                                                            <div class="form-group" style="margin-top:20px;">                                                                
                                                                <div class="input-group">
                                                                  <span class="switch-addon">Pre Bal?</span>
                                                                  <input type="checkbox" checked="" class="bs_switch" id="switchPreBal">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <div class="input-group" style="margin-top:20px;">
                                                                <span class="input-group-addon">User: </span>
                                                                <select class="form-control " disabled="" id="user_dropdown">
                                                                    <option value="" disabled="" selected="">...</option>
                                                                    <?php foreach ($userone as $user): ?>
                                                                        <option value="<?php echo $user['uid']; ?>"><?php echo $user['uname']; ?></option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                <div class="col-lg-2 col-lg-offset-4">                                                    
                                                    <label>Net Amount</label>
                                                    <input type="text" class="form-control readonly " id='txtNetAmount' readonly="" tabindex="-1">
                                                    <!-- input type="text" class="form-control readonly num" id="txtUom" >                                                     -->
                                                </div>
                                                
                                            </div>
                                                
                                                <div class="row">                                                                                    
                                                    <div class="col-lg-12">
                                                    <div class="pull-right">
                                                        <a class="btn btn-sm btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                                                        <a class="btn btn-sm btn-default btnSave" data-savegodownbtn='<?php echo $vouchers['warehouse']['insert']; ?>' data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['itemconversion']['insert']; ?>' data-updatebtn='<?php echo $vouchers['itemconversion']['update']; ?>' data-deletebtn='<?php echo $vouchers['itemconversion']['delete']; ?>' data-printbtn='<?php echo $vouchers['itemconversion']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
                                                        <a class="btn btn-sm btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>
                                                        
                                                        <div class="btn-group">
                                                              <button type="button" class="btn btn-primary btn-sm btnPrint btnPrints" ><i class="fa fa-save"></i>Print F9</button>
                                                              <!-- <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                <span class="caret"></span>
                                                                <span class="sr-only">Toggle Dropdown</span>
                                                              </button>
                                                              <ul class="dropdown-menu" role="menu">
                                                                <li ><a href="#" class="btnprintHeader">With header</li>
                                                                <li ><a href="#" class="btnprintwithOutHeader">With out header</li>
                                                                <li ><a href="#" class="btnprint_sm">Small</li>
                                                                <li ><a href="#" class="btnprint_sm_withOutHeader">Small with out header </li>
                                                                <li ><a href="#" class="btnprint_sm_rate">Small with out rate</li>
                                                                <li ><a href="#" class="btnprint_sm_withOutHeader_rate">Small with out header and rate </li>
                                                                <li ><a href="#" class="btnPrints">Print</a></li>
                                                              </ul> -->
                                                        </div>
                                                        <a href="#item-lookup" data-toggle="modal" class="btn btn-sm btn-info btnsearchitem sr-only"><i class="fa fa-search"></i>&nbsp;Item Lookup F2</a>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>  <!-- end of row -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <!-- </div> -->
                        <div class="tab-pane fade" id="Others">
                                <div class="panel panel-default" style="margin-top:-31px;">
                                    <div class="panel-body">
                                        <div class="row">
                                                <div class="col-lg-2">                                                
                                                   <label>Received By</label>
                                                   <input class='form-control ' type='text' list="receivers" id='receivers_list'>
                                                   <datalist id='receivers'>
                                                       <?php foreach ($receivers as $receiver): ?>
                                                           <option value="<?php echo $receiver['received_by']; ?>">
                                                       <?php endforeach ?>
                                                   </datalist>                                                
                                                </div>                                    
                                                <div class="col-lg-3">                                                
                                                    <label>Through</label>
                                                    <select class="form-control select2" id="transporter_dropdown">
                                                        <option value="" disabled="" selected="">Choose transporter</option>
                                                        <?php foreach ($transporters as $transporter): ?>
                                                            <option value="<?php echo $transporter['transporter_id']; ?>"><?php echo $transporter['name']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>  
                                                </div>
                                                                                    
                                                
                                                <div class="col-lg-4">
                                                    <label for="">Customer Name</label>                                                    
                                                    <input type="text" class="form-control" id="txtcustomerName" >                                                    
                                                </div>
                                                <div class="col-lg-3">
                                                    <label for="">Mobile</label>                                                    
                                                    <input type="text" class="form-control" id="txtcustomerMob" >                                                    
                                                </div>


                                        </div>
                                        <div class="row">
                                           <div class="col-lg-2">                                                
                                               <label>Inv#</label>
                                               <input type="text" class="form-control num" id="txtInvNo">                                                
                                           </div>
                                           <div class="col-lg-3">                                                
                                               <label>Due Date</label>
                                               <input class="form-control ts_datepicker" type="text" id="due_date">                                                
                                           </div>                                           
                                           <div class="col-lg-2">                                                
                                               <label>PO#</label>
                                               <input type="text" class="form-control" id="txtOrderNo">    
                                           </div>                                                                                
                                           <div class="col-lg-5">                                            
                                               <label>Remarks</label>
                                               <input type="text" class="form-control" id="txtRemarks">                                                                                                                                                                             
                                           </div>
                                       </div>
                                    </div>
                                </div>
                        </div>
                        <div class="tab-pane fade" id="Search">
                            <div class="panel panel-default" style="margin-top:-31px;">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon">From</span>
                                                <input class="form-control ts_datepicker" type="text" id="from_date">
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon">To</span>
                                                <input class="form-control ts_datepicker" type="text" id="to_date">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="pull-right">
                                                <a href='' class="btn btn-sm btn-success btnSearch" id="btnSearch" ><i class="fa fa-search"></i> Search</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            
                                            <div id="no-more-tables">
                                                <table class="col-lg-12 table table-bordered table-striped table-condensed cf" id="purchase_tableReport">
                                                    <thead class=" tbl_thead">
                                                        <tr>
                                                            <th>Vr#</th>
                                                            <th>Date</th>
                                                            <th class="numeric ">Stock Location</th>
                                                            <th class="numeric ">Remarks</th>
                                                            <th class="numeric ">Type</th>
                                                            <th class="numeric ">Item</th>
                                                            <th class="numeric text-right">UOM</th>
                                                            <th class="numeric text-right">Qty</th>
                                                            <th class="numeric text-right">Rate </th>
                                                            <th class="numeric text-right">Amount</th>
                                                            <!-- <th>Action</th> -->
                                                        </tr>
                                                    </thead>
                                                    <tbody id="saleRows" class="report-rows saleRows">

                                                    </tbody>
                                                 <!--    <tfoot class="tfoot_tbl">
                                                        <tr>
                                                            <td class="text-right" colspan="2">Totals</td>
                                                            <td class="text-right txtTotalQty"></td>
                                                            <td class="text-right txtTotalRate"></td>
                                                            <td class="text-right txtTotalDiscp"></td>
                                                            <td class="text-right txtTotalDiscount"></td>
                                                            <td class="text-right txtTotalAmount"></td>
                                                            <td></td>
                                                        </tr>
                                                    </tfoot> -->
                                                   <!--  <tfoot style='border-top:1px solid !important;'>
                                                        <tr>
                                                            <td></td>
                                                            <td style='color:red !important; text-align:right;'>Total:</td>                                                            
                                                            <td style='color:red !important;'></td>
                                                            <td style='color:red !important;'></td>
                                                            <td></td>                                                                  
                                                            <td style='color:red !important;'></td>
                                                            <td></td>
                                                        </tr>
                                                    </tfoot> -->

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
        
                <!-- </div>   end of level 1 -->
                <div class="col-md-2">
                 <div class="panel panel-default dtheight" style="margin-top:-21px; background-color: #E7F0EF !important;">
                            <div class="panel-body zero-padding"><br>
                            <!-- <div class="row"> -->
                          <label class="label label-primary side_info"><i class="fa fa-list"></i> Other information</label>
                                <div class="row l5ratesdisp disp ten-padding">
                                        <label>Last 5 Purchase Rates</label>
                                            <table class="lastrate_table " id="lastrate_table">
                                                    <thead>
                                                        <tr>
                                                            <th style='width:30px; font-size: 12px;'>Vr#</th>
                                                            <th style='width:90px;  font-size: 12px;'>Date</th>
                                                            <th style='width:60px;  font-size: 12px;'>Rate</th>
                                                            <th style='width:60px;  font-size: 12px;'>Qty</th>
                                                        </tr>
                                                    </thead>
                                                    
                                                    <tbody class="rate_tbody" style="font-size: 11px !important;">
                                                        
                                                    </tbody>
                                            </table>
                                </div>
                                <div class="row laststockLocation_tabledisp disp ten-padding">
                                    <label>Last Stock Locations</label>
                                    <table class="laststockLocation_table" id="laststockLocation_table">
                                            <thead>
                                                <tr>
                                                    <th style='width:90px;  font-size: 12px;'>Location</th>
                                                    <th style='width:60px;  font-size: 12px;'>Qty</th>
                                                </tr>
                                            </thead>
                                            
                                            <tbody class="rate_tbody" style="font-size: 11px !important;">
                                                
                                            </tbody>
                                    </table>
                                </div>
                                    <div class="partydisp disp">
                                        <label>Account Detail </label>
                                          <div id="party_p"></div>
                                    </div>
                                           <!--   <table class="lastparty_table" id="lastparty_table">
                                                 <thead>
                                                     <tr>
                                                         <th style='width:30px; font-size: 12px;'>Vendor Balance</th>
                                                         <th style='width:90px;  font-size: 12px;'>Credit Limit</th>
                                                         <th style='width:60px;  font-size: 12px;'>City</th>
                                                         <th style='width:60px;  font-size: 12px;'>Address</th>
                                                         <th style='width:60px;  font-size: 12px;'>Area</th>
                                                         <th style='width:60px;  font-size: 12px;'>Contact</th>
                                                     </tr>
                                                 </thead>
                                                 
                                                 <tbody class="party_tbody" style="font-size: 11px !important;">
                                                     
                                                 </tbody>
                                             </table> -->
                           </div>
                         </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>