<?php
    $desc = $this->session->userdata('desc');
    $desc = json_decode($desc);
    $desc = objectToArray($desc);
    $vouchers = $desc['vouchers'];
?>
<!-- main content -->
<div id="main_wrapper">

    <div class="page_bar">
        <div class="row">
            <div class="col-md-4">
                <h1 class="page_title">Stock Navigation Voucher</h1>
            </div>
            <div class="col-md-8">
                <div class="pull-right">
                    <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                    <a class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['navigationvoucher']['insert']; ?>' data-updatebtn='<?php echo $vouchers['navigationvoucher']['update']; ?>' data-deletebtn='<?php echo $vouchers['navigationvoucher']['delete']; ?>' data-printbtn='<?php echo $vouchers['navigationvoucher']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
                    <a class="btn btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary btnPrintThermal" ><i class="fa fa-save"></i>Print F9</button>
                        <!-- <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                          <span class="caret"></span>
                          <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li ><a href="#" class="btnPrint"> Print with header</li>
                            <li ><a href="#" class="btnprintwithOutHeader"> Print with Out header</li>
                            <li ><a href="#" class="btnPrintThermal"> Print Thermal </a></li>
                        </ul> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page_content">
        <div class="container-fluid">
            <div class="row ">
                <div class="col-lg-12">
                    <ul class="nav nav-pills">
                        <li class="active"><a href="#Main" data-toggle="tab">Main</a></li>
                        <li><a href="#Attachments" data-toggle="tab">Attachments</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div class="row">
                        <div class="col-lg-10">
                            <div class="tab-content">
                                <div class="tab-pane active fade in" id="Main">
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <form action="">

                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon id-addon">Nav#</span>
                                                    <input type="number" class="form-control" id="txtVrnoa" >
                                                    <input type="hidden" id="txtMaxVrnoaHidden">
                                                    <input type="hidden" id="txtVrnoaHidden">
                                                    <input type="hidden" id="voucher_type_hidden">

                                                    <input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                                                    <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                                                    <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">

                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="input-group">
                                                    <span class="input-group-addon txt-addon">Date</span>
                                                    <input class="form-control ts_datepicker" type="text" id="current_date">
                                                    <input class="form-control ts_datepicker" type="hidden" id="vrdate">
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="input-group">
                                                    <span class="input-group-addon txt-addon min-width-50">Vr#</span>
                                                    <input type="text" class="form-control" id="txtVrno" readonly='true'>
                                                    <input type="hidden" id="txtMaxVrnoHidden">
                                                    <input type="hidden" id="txtVrnoHidden">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label>Issued By</label>
                                                <input class='form-control' type='text' list="issuers" id='issuers_list'>
                                                <datalist id='issuers'>
                                                    <?php foreach ($issuers as $issuer): ?>
                                                    <option value="<?php echo $issuer['issued_by']; ?>">
                                                        <?php endforeach ?>
                                                </datalist>
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Received By</label>
                                                <input class='form-control' type='text' list="receivers" id='receivers_list'>
                                                <datalist id='receivers'>
                                                    <?php foreach ($receivers as $receiver): ?>
                                                        <option value="<?php echo $receiver['received_by']; ?>">
                                                    <?php endforeach ?>
                                                </datalist>
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Remarks</label>
                                                <input type="text" class="form-control" id="txtRemarks">
                                            </div>
                                        </div>

                                        <div class="row"></div>

                                        <div class="container-wrap">
                                            <div class="row">
                                                <!-- <div class="col-lg-2">
                                                    <label>Item Code</label>
                                                    <select class="form-control select2" id="itemid_dropdown">
                                                        <option value="" disabled="" selected="">Item Code</option>
                                                        <?php foreach ($items as $item): ?>
                                                            <option value="<?php echo $item['item_id']; ?>" data-uom="<?php echo $item['uom']; ?>" data-stqty="<?php echo $item['stqty']; ?>" data-stweight="<?php echo $item['stweight']; ?>"><?php echo $item['item_id']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label id ="stqty_lbl">Item Description</label>
                                                    <div class="input-group" >
                                                    <select class="form-control select2" id="item_dropdown">
                                                        <option value="" disabled="" selected="">Item description</option>
                                                        <?php foreach ($items as $item): ?>
                                                            <option value="<?php echo $item['item_id']; ?>" data-uom="<?php echo $item['uom']; ?>" data-stqty="<?php echo $item['stqty']; ?>" data-stweight="<?php echo $item['stweight']; ?>"><?php echo $item['item_des']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                     <a class="input-group-addon btn btn-primary active btnsearchitem" href="#item-lookup" data-toggle="modal"  style="min-width:40px !important;"><i class="fa fa-search"></i></a>
                                                    </div>
                                                </div> -->
                                                <div class="col-lg-4" >
                                                    <label for="">Item Code List <img id="imgItemLoader" class="hide" src="<?php echo base_url(); ?>/assets/img/loader.gif"></label>
                                                    <input type="text" class="form-control" id="txtItemId">
                                                    <input id="hfItemId" type="hidden" value="" />
                                                    <input id="hfItemSize" type="hidden" value="" />
                                                    <input id="hfItemBid" type="hidden" value="" />
                                                    <input id="hfItemUom" type="hidden" value="" />
                                                    <input id="hfItemPrate" type="hidden" value="" />
                                                    <input id="hfItemGrWeight" type="hidden" value="" />
                                                    <input id="hfItemStQty" type="hidden" value="" />
                                                    <input id="hfItemStWeight" type="hidden" value="" />
                                                    <input id="hfItemLength" type="hidden" value="" />
                                                    <input id="hfItemCatId" type="hidden" value="" />
                                                    <input id="hfItemSubCatId" type="hidden" value="" />
                                                    <input id="hfItemDesc" type="hidden" value="" />
                                                    <input id="hfItemUname" type="hidden" value="" />
                                                    <input id="hfItemShortCode" type="hidden" value="" />
                                                </div>
                                                <div class="col-lg-4">
                                                    <label for="" id="stqty_lbl">Item</label>
                                                    <input type="text" class="form-control" id="itemDesc" readonly="" tabindex="-1">
                                                </div>
                                                <div class="col-lg-2">
                                                    <label>Location From</label>
                                                    <select class="form-control select11" id="deptfrom_dropdown">
                                                        <option value="" disabled="" selected="">Location From</option>
                                                        <?php foreach ($departments as $department): ?>
                                                            <option value="<?php echo $department['did']; ?>"><?php echo $department['name']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                                <div class="col-lg-1" >
                                                    <label>Uom</label>
                                                    <input type="text" class="form-control num" id="txtUOM" readonly="true" tabindex="-1">
                                                </div>
                                                <div class="col-lg-1">
                                                    <label>Qty</label>
                                                    <input type="text" class="form-control num" id="txtSQty">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-1">
                                                    <label>GW</label>
                                                    <input type="text" class="form-control num" readonly="true" id="txtGrWeight">
                                                </div>
                                                <div class="col-lg-1">
                                                    <label>Weight</label>
                                                    <input type="text" class="form-control num" id="txtWeight">
                                                </div>

                                                <div class="col-lg-2">
                                                    <label>Location To</label>
                                                    <select class="form-control select11" id="deptto_dropdown">
                                                        <option value="" disabled="" selected="">Location To</option>
                                                        <?php foreach ($departments as $department): ?>
                                                            <option value="<?php echo $department['did']; ?>"><?php echo $department['name']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>

                                                <div class="col-lg-2">
                                                    <label>Exp. Weight</label>
                                                    <input type="text" class="form-control num" id="txtExpWeight">
                                                </div>

                                                <div class="col-lg-2">
                                                    <label>Route</label>
                                                    <select class="form-control select11" id="route_dropdown">
                                                        <option value="" disabled="" selected="">Choose Route</option>
                                                        <?php foreach ($routes as $route): ?>
                                                            <option value="<?php echo $route['route_id']; ?>" data-rpm="<?php echo $route['rate_month']; ?>">
                                                                <?php echo $route['name']; ?>
                                                            </option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>

                                                <div class="col-lg-1">
                                                    <label>Rate</label>
                                                    <input type="text" class="form-control num" id="txtrpm" >
                                                </div>

                                                <div class="col-lg-2">
                                                    <label>Amount</label>
                                                    <input type="text" class="form-control num readonly disabled" id="txtAmount" readonly="true" tabindex="-1">
                                                </div>

                                                <div class="col-lg-1" style="margin-top:12px;">
                                                    <!-- <label>Add</label> --><br>
                                                    <a href="" class="btn btn-primary" id="btnAdd"><i class="fa fa-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row"></div>
                                        <div class="row"></div>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <table class="table table-striped" id="purchase_table">
                                                    <thead>
                                                        <tr>
                                                            <th>Sr#</th>
                                                            <th>Item Name</th>
                                                            <th>Location From</th>
                                                            <th>UOM</th>
                                                            <th>Qty</th>
                                                            <th>Weight</th>
                                                            <th>Location To</th>
                                                            <th>Exp. Weight</th>
                                                            <th>Route</th>
                                                            <th class="text-right">Rate</th>
                                                            <th class="text-right">Amount</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="saleRows">

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </form> <!-- end of form -->

                                </div>  <!-- end of panel-body -->
                            </div>  <!-- end of panel -->
                            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class='col-lg-3'>
                                          <label>Expense</label>
                                            <select class="form-control select11" id="expense_dropdown" >
                                                <option value="" disabled="" selected="">Choose Expense</option>
                                                <?php foreach ($parties as $party): ?>
                                                    <option value="<?php echo $party['pid']; ?>" data-address="<?php echo $party['address']; ?>" data-uaddress="<?php echo $party['uaddress']; ?>" data-uname="<?php echo $party['uname']; ?>" data-pbalance="<?php echo $party['pbalance']; ?>"><?php echo $party['name']; ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                        <div class='col-lg-3'>
                                          <label>Cost</label>
                                            <select class="form-control select11" id="cost_dropdown" >
                                                <option value="" disabled="" selected="">Choose Cost</option>
                                                <?php foreach ($parties as $party): ?>
                                                    <option value="<?php echo $party['pid']; ?>" data-address="<?php echo $party['address']; ?>" data-uaddress="<?php echo $party['uaddress']; ?>" data-uname="<?php echo $party['uname']; ?>" data-pbalance="<?php echo $party['pbalance']; ?>"><?php echo $party['name']; ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                        <div class='col-lg-3'>
                                          <label>Transporter</label>
                                            <select class="form-control select11" id="transport_dropdown" >
                                                <option value="" disabled="" selected="">Choose Transporter</option>
                                                <?php foreach ($transporter as $transport): ?>
                                                    <option value="<?php echo $transport['transporter_id']; ?>" data-rate="<?php echo $transport['area_covers']; ?>"><?php echo $transport['name']; ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                        <div class="col-lg-1">
                                            <label>Rate</label>
                                            <input type="text" class="form-control" id="txtrate">
                                        </div>
                                        <div class="col-lg-2" >
                                            <label>Cash Account</label>
                                            <select class="form-control select11" id="cash_dropdown">
                                                <option value="" disabled="" selected="">Choose Cash Account</option>
                                                <?php foreach ($accountCashs as $accountCash): ?>
                                                    <option value="<?php echo $accountCash['pid']; ?>"><?php echo $accountCash['name']; ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon id-addon" >Qty</span>
                                                <input type="text" class="form-control num" id="txtGQty" readonly="true" tabindex="-1">
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon id-addon" >Weight</span>
                                                <input type="text" class="form-control num" id="txtGWeight" readonly="true" tabindex="-1">
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon id-addon" >Exp. Weight</span>
                                                <input type="text" class="form-control num" id="txtGExpWeight" readonly="true" tabindex="-1">
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon id-addon" > Freight Amount</span>
                                                <input type="text" class="form-control num" id="txtGAmount" readonly="true" tabindex="-1">
                                            </div>
                                        </div>
                                    </div>  <!-- end of row -->

                                    <div class="row">
                                        <div class="col-lg-6"></div>
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                                                    <a class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['navigationvoucher']['insert']; ?>' data-updatebtn='<?php echo $vouchers['navigationvoucher']['update']; ?>' data-deletebtn='<?php echo $vouchers['navigationvoucher']['delete']; ?>' data-printbtn='<?php echo $vouchers['navigationvoucher']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
                                                    <a class="btn btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary btnPrintThermal" ><i class="fa fa-save"></i>Print F9</button>
                                                        <!-- <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                          <span class="caret"></span>
                                                          <span class="sr-only">Toggle Dropdown</span>
                                                        </button> 
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li ><a href="#" class="btnPrint"> Print with header</li>
                                                            <li ><a href="#" class="btnprintwithOutHeader"> Print with Out header</li> 
                                                            <li ><a href="#" class="btnPrintThermal"> Print Thermal </a></li>
                                                        </ul>-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  <!-- end of col -->
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-3" >
                                            <div class="input-group">
                                                <span class="input-group-addon">User: </span>
                                                <select class="form-control " disabled="" id="user_dropdown">
                                                    <option value="" disabled="" selected="">...</option>
                                                    <?php foreach ($userone as $user): ?>
                                                        <option value="<?php echo $user['uid']; ?>"><?php echo $user['uname']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                            </div>  <!-- end of main -->


                            <div class="tab-pane fade in Attachmentspanel" id="Attachments">
                                <div class="row">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <input type="number" class="form-control" id="txtIdImg" style="background: #fff;" readonly />
                                        </div>
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-primary" id="viewImages">View Attachments <i class="fa fa-eye"></i></button>
                                            <button type="button" class="btn btn-success" id="uploadFiles" data-insert='<?php echo $vouchers['uploadFiles']['insert']; ?>' data-update='<?php echo $vouchers['uploadFiles']['update']; ?>' data-delete='<?php echo $vouchers['uploadFiles']['delete']; ?>' >Update Files <i class="fa fa-upload"></i></button>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                         <form enctype="multipart/form-data" class="dropzone" id="image-upload" method="POST">
                                                <div>
                                                    <h3>Attach Files to Upload</h3>
                                                    <div class="fallback">
                                                        '<input name="file" type="file" />
                                                    </div>
                                                </div>
                                            </form>
                                            <small style="color: #555;"> Supported Files: (.bmp, .gif, .jpg, .jpeg, .png, .pdf, .doc, .docx, .txt, .xls, .xlsx, .rtf)</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            </div>
                            </div>  <!-- end of tab -->
                        </div>  <!-- end of col -->

                        <div class="row col-lg-2" style="background-color: #E7F0EF;">
                            <div class="row">
                                <label>Stock Detail</label>
                                <table class="laststockLocation_table table-striped" id="laststockLocation_table" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th>Location</th>
                                                <th>Qty</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                </table>
                            </div>
                        </div>

                    </div>  <!-- end of row -->

                    

                </div>  <!-- end of level 1-->
            </div>
        </div>
    </div>
</div>

<div id="item-lookup" class="modal fade modal-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h3 id="myModalLabel">item Lookup</h3>
            </div>

                <div class="modal-body">
                <div style="width:250px; float: right !important;top:-10px; position: relative; bottom:30px;">
                    <label for="">Search</label>
                    <input type="text" class="form-control" id="txtItemSearch">
                </div>
                <table class="table" id="tbItems">
                <!-- <table class="table table-bordered table-striped modal-table"> -->
                <thead>
                <tr style="font-size:16px;">
                    <th>Id</th>
                    <th>Code</th>
                    <th>Description</th>
                    <th>Uom</th>
                    <th style='width:3px;'>Actions</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
                </table>
                <div id="divNoRecord" class="divNoRecord"></div>
                    <div id="paging"></div>
                </div>
                <div class="modal-footer">
                <!-- <button class="btn btn-danger delete-modal-del">Delete</button> -->
                <button class="btn btn-primary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<style>
    .select2-container-active {
        outline: 1px solid rgba(10,10,255,0.4);
    }
</style>