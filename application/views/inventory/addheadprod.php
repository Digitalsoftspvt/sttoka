<?php

$desc = $this->session->userdata('desc');
$desc = json_decode($desc);
$desc = objectToArray($desc);

$vouchers = $desc['vouchers'];
?>
<style>
  .select2-container .select2-choice {
    width : 100% !important;
  }
</style>
<!-- main content -->
<div id="main_wrapper">
  <div class="page_bar">
    <div class="row">
      <div class="col-md-3">
        <h1 class="page_title">Head Production Voucher</h1>
      </div>
    </div>
  </div>
  <div class="page_content">
    <div class="container-fluid">
      <div class="row ">
        <div class="col-lg-12">
          <ul class="nav nav-pills">
            <li class="active"><a href="#Main" data-toggle="tab">Main</a></li>
            <li><a href="#Attachments" data-toggle="tab">Attachments</a></li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-lg-12">
              <div class="tab-content">
                <div class="tab-pane active fade in" id="Main">
              <div class="panel panel-default">
                <div class="panel-body">
                  <form action="">
                    <div class="form-group">
                      <div class="row">
                        <div class="col-lg-1">
                          <label>ID#</label>
                          <input type="number" class="form-control" id="txtVrnoa" >
                          <input type="hidden" id="txtMaxVrnoaHidden">
                          <input type="hidden" id="txtVrnoaHidden">
                          <input type="hidden" id="voucher_type_hidden">
                          <input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                          <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                          <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
                        </div>
                        <div class="col-lg-2">
                          <label>Date</label>
                          <input class="form-control ts_datepicker" type="text" id="current_date">
                          <input class="form-control ts_datepicker" type="hidden" id="vrdate">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-lg-3">
                          <label>Godown</label>
                          <select class="form-control select2" id="dept_dropdown">
                            <option value="" disabled="" selected="">Choose Godown </option>
                            <?php foreach ($departments as $department): ?>
                              <option value="<?php echo $department['did']; ?>"><?php echo $department['name']; ?></option>
                            <?php endforeach ?>
                          </select>
                        </div>
                        <div class="col-lg-6">
                          <label>Remarks</label>
                          <input type="text" class="form-control" id="txtRemarks">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-10">
                        <div>
                          <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#phead" aria-controls="phead" role="tab" tabindex="-1" data-toggle="tab">Production Head</a></li>
                              <li role="presentation"><a href="#fiting" aria-controls="fiting" role="tab" tabindex="-1" data-toggle="tab">Fitting</a></li>
                              <li  role="presentation"><a href="#helping1" aria-controls="helping1" role="tab" tabindex="-1" data-toggle="tab">Helping 1</a></li>
                              <li  role="presentation"><a href="#helping2" aria-controls="helping2" role="tab" tabindex="-1" data-toggle="tab">Helping 2</a></li>
                            <li   role="presentation"><a href="#cleaning" aria-controls="cleaning" role="tab" tabindex="-1" data-toggle="tab">Cleaning</a></li>
                            <li   role="presentation"><a href="#grinding" aria-controls="grinding" role="tab" tabindex="-1" data-toggle="tab">Grinding</a></li>
                            <li   role="presentation"><a href="#warma" aria-controls="warma" role="tab" tabindex="-1" data-toggle="tab">Warma</a></li>
                            <li   role="presentation"><a href="#machine1" aria-controls="machine1" role="tab" tabindex="-1" data-toggle="tab">Machine 1</a></li>
                            <li   role="presentation"><a href="#machine2" aria-controls="machine2" role="tab" tabindex="-1" data-toggle="tab">Machine 2</a></li>
                            <li   role="presentation"><a href="#machine3" aria-controls="machine3" role="tab" tabindex="-1" data-toggle="tab">Machine 3</a></li>
                            <li   role="presentation"><a href="#machine4" aria-controls="machine4" role="tab" tabindex="-1" data-toggle="tab">Machine 4</a></li>
                            <li   role="presentation"><a href="#machine5" aria-controls="machine5" role="tab" tabindex="-1" data-toggle="tab">Machine 5</a></li>
                            <li  role="presentation"><a href="#machine6" aria-controls="machine6" role="tab" tabindex="-1" data-toggle="tab">Machine 6</a></li>

                          </ul>
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="phead">
                              <div class="container-wrap">
                                <div class="form-group">
                                  <div class="row">
                             <!--        <div class="col-lg-2">
                                      <label>Item Code</label>
                                      <select class="form-control select2" id="itemid_dropdown">
                                        <option value="" disabled="" selected="">Item Id</option>
                                        <?php foreach ($items as $item): ?>
                                          <option value="<?php echo $item['item_id']; ?>" data-uom="<?php echo $item['uom']; ?>" data-stqty="<?php echo $item['stqty']; ?>" data-stweight="<?php echo $item['stweight']; ?>"><?php echo $item['item_code']; ?></option>
                                        <?php endforeach ?>
                                      </select>
                                    </div>
                                    <div class="col-lg-4">
                                      <label>Item Description</label>
                                      <select class="form-control select2" id="item_dropdown">
                                        <option value="" disabled="" selected="">Item description</option>
                                        <?php foreach ($items as $item): ?>
                                          <option value="<?php echo $item['item_id']; ?>" data-uom="<?php echo $item['uom']; ?>" data-stqty="<?php echo $item['stqty']; ?>" data-stweight="<?php echo $item['stweight']; ?>"><?php echo $item['item_des']; ?></option>
                                        <?php endforeach ?>
                                      </select>
                                    </div> -->
                                    <div class="col-lg-5" >
                                        <label for="">Item Code List <img id="imgItemLoader" class="hide" src="<?php echo base_url(); ?>/assets/img/loader.gif"></label>
                                        <input type="text" class="form-control" id="txtItemId">
                                        <input id="hfItemId" type="hidden" value="" />
                                        <input id="hfItemSize" type="hidden" value="" />
                                        <input id="hfItemBid" type="hidden" value="" />
                                        <input id="hfItemUom" type="hidden" value="" />
                                        <input id="hfItemPrate" type="hidden" value="" />
                                        <input id="hfItemGrWeight" type="hidden" value="" />
                                        <input id="hfItemStQty" type="hidden" value="" />
                                        <input id="hfItemStWeight" type="hidden" value="" />
                                        <input id="hfItemLength" type="hidden" value="" />
                                        <input id="hfItemCatId" type="hidden" value="" />
                                        <input id="hfItemSubCatId" type="hidden" value="" />
                                        <input id="hfItemDesc" type="hidden" value="" />
                                        <input id="hfItemUname" type="hidden" value="" />
                                        <input id="hfItemShortCode" type="hidden" value="" />
                                    </div>
                                    <div class="col-lg-5">
                                        <label for="" id="stqty_lbl">Item</label>
                                        <input type="text" class="form-control" id="itemDesc" readonly="" tabindex="-1">
                                    </div>
                                      <div class="col-lg-1">
                                          <label>Qty</label>
                                          <input type="text" class="form-control num" id="txtSQty">
                                      </div>
                                      <div class="col-lg-1">
                                          <label>Weight</label>
                                          <input type="text" class="form-control num" id="txtWeight">
                                      </div>
                                    <div class="col-lg-1">
                                      <label>Add</label>                                                    
                                      <a href="" class="btn btn-primary" id="btnAddItem">+</a>
                                    </div>
                                  </div>
                                    <h3>Production</h3>
                                </div>
                                <div class="row">
                                  <div class="col-lg-12">
                                    <table class="table table-striped st" id="item_table">
                                      <thead>
                                        <tr>
                                          <th>Sr#</th>
                                          <th>Item Code</th>
                                          <th>Description</th>    
                                          <th>Qty</th>
                                            <th>Weight</th>
                                          <th>Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="fiting">
                              <div class="container-wrap">
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-lg-3">
                                      <label>Department</label>
                                      <select class="form-control select2" id="dept_dropdown">
                                        <option value="" disabled="" selected="">Choose Department </option>
                                        <?php foreach ($staffdepartments as $staffdepartment): ?>
                                          <option value="<?php echo $staffdepartment['deptid']; ?>" data-phaseid = '1'><?php echo $staffdepartment['name']; ?></option>
                                        <?php endforeach ?>
                                      </select>
                                    </div>
                                    <div class="col-lg-1">
                                      <label>Add</label>                                                    
                                      <a href="" class="btn btn-primary" id="btnAddF">+</a>
                                    </div>
                                  </div>
                                </div>
                                  <h3>Fitting</h3>
                                <div class="row">
                                  <div class="col-lg-12">
                                    <table class="table table-striped st" id="staff_tableF">
                                      <thead>
                                        <tr>
                                          <th>Sr#</th>
                                          <th>Item</th>
                                          <th style='width:200px !important;'>Employee</th>
                                          <th>Qty</th>
                                            <th>Weight</th>
                                          <th>Rate</th>
                                          <th>Amount</th>
                                          <th>Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="cleaning">
                              <div class="container-wrap">
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-lg-3">
                                      <label>Department</label>
                                      <select class="form-control select2" id="dept_dropdown">
                                        <option value="" disabled="" selected="">Choose Department </option>
                                        <?php foreach ($staffdepartments as $staffdepartment): ?>
                                          <option value="<?php echo $staffdepartment['deptid']; ?>" data-phaseid = '2'><?php echo $staffdepartment['name']; ?></option>
                                        <?php endforeach ?>
                                      </select>
                                    </div>
                                    <div class="col-lg-1">
                                      <label>Add</label>                                                    
                                      <a href="" class="btn btn-primary" id="btnAddC">+</a>
                                    </div>
                                  </div>
                                </div>
                                  <h3>Cleaning</h3>
                                <div class="row">
                                  <div class="col-lg-12">
                                    <table class="table table-striped st" id="staff_tableC">
                                      <thead>
                                        <tr>
                                          <th>Sr#</th>
                                            <th>Item</th>
                                          <th>Employee</th>
                                          <th>Qty</th>
                                            <th>Weight</th>
                                          <th>Rate</th>
                                          <th>Amount</th>
                                          <th>Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="grinding">
                              <div class="container-wrap">
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-lg-3">
                                      <label>Department</label>
                                      <select class="form-control select2" id="dept_dropdown">
                                        <option value="" disabled="" selected="">Choose Department </option>
                                        <?php foreach ($staffdepartments as $staffdepartment): ?>
                                          <option value="<?php echo $staffdepartment['deptid']; ?>" data-phaseid = '3'><?php echo $staffdepartment['name']; ?></option>
                                        <?php endforeach ?>
                                      </select>
                                    </div>
                                    <div class="col-lg-1">
                                      <label>Add</label>                                                    
                                      <a href="" class="btn btn-primary" id="btnAddG">+</a>
                                    </div>
                                  </div>
                                </div>
                                  <h3>Grinding</h3>
                                <div class="row">
                                  <div class="col-lg-12">
                                    <table class="table table-striped st" id="staff_tableG">
                                      <thead>
                                        <tr>
                                          <th>Sr#</th>
                                            <th>Item</th>
                                          <th>Employee</th>
                                          <th>Qty</th>
                                            <th>Weight</th>
                                          <th>Rate</th>
                                          <th>Amount</th>
                                          <th>Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="warma">
                              <div class="container-wrap">
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-lg-3">
                                      <label>Department</label>
                                      <select class="form-control select2" id="dept_dropdown">
                                        <option value="" disabled="" selected="">Choose Department </option>
                                        <?php foreach ($staffdepartments as $staffdepartment): ?>
                                          <option value="<?php echo $staffdepartment['deptid']; ?>" data-phaseid = '4'><?php echo $staffdepartment['name']; ?></option>
                                        <?php endforeach ?>
                                      </select>
                                    </div>
                                    <div class="col-lg-1">
                                      <label>Add</label>                                                    
                                      <a href="" class="btn btn-primary" id="btnAddW">+</a>
                                    </div>
                                  </div>
                                </div>
                                  <h3>Warma</h3>
                                <div class="row">
                                  <div class="col-lg-12">
                                    <table class="table table-striped st" id="staff_tableW">
                                      <thead>
                                        <tr>
                                          <th>Sr#</th>
                                            <th>Item</th>
                                          <th>Employee</th>
                                          <th>Qty</th>
                                            <th>Weight</th>
                                          <th>Rate</th>
                                          <th>Amount</th>
                                          <th>Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="machine1">
                              <div class="container-wrap">
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-lg-3">
                                      <label>Department</label>
                                      <select class="form-control select2" id="dept_dropdown">
                                        <option value="" disabled="" selected="">Choose Department </option>
                                        <?php foreach ($staffdepartments as $staffdepartment): ?>
                                          <option value="<?php echo $staffdepartment['deptid']; ?>" data-phaseid = '5'><?php echo $staffdepartment['name']; ?></option>
                                        <?php endforeach ?>
                                      </select>
                                    </div>
                                    <div class="col-lg-1">
                                      <label>Add</label>                                                    
                                      <a href="" class="btn btn-primary" id="btnAddM1">+</a>
                                    </div>
                                  </div>
                                </div>
                                  <h3>Machine1</h3>
                                <div class="row">
                                  <div class="col-lg-12">
                                    <table class="table table-striped st" id="staff_tableM1">
                                      <thead>
                                        <tr>
                                          <th>Sr#</th>
                                            <th>Item</th>
                                          <th>Employee</th>
                                          <th>Qty</th>
                                            <th>Weight</th>
                                          <th>Rate</th>
                                          <th>Amount</th>
                                          <th>Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="machine2">
                              <div class="container-wrap">
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-lg-3">
                                      <label>Department</label>
                                      <select class="form-control select2" id="dept_dropdown">
                                        <option value="" disabled="" selected="">Choose Department </option>
                                        <?php foreach ($staffdepartments as $staffdepartment): ?>
                                          <option value="<?php echo $staffdepartment['deptid']; ?>" data-phaseid = '6'><?php echo $staffdepartment['name']; ?></option>
                                        <?php endforeach ?>
                                      </select>
                                    </div>
                                    <div class="col-lg-1">
                                      <label>Add</label>                                                    
                                      <a href="" class="btn btn-primary" id="btnAddM2">+</a>
                                    </div>
                                  </div>
                                </div>
                                  <h3>Machine 2</h3>
                                <div class="row">
                                  <div class="col-lg-12">
                                    <table class="table table-striped st" id="staff_tableM2">
                                      <thead>
                                        <tr>
                                          <th>Sr#</th>
                                            <th>Item</th>
                                          <th>Employee</th>
                                          <th>Qty</th>
                                            <th>Weight</th>
                                          <th>Rate</th>
                                          <th>Amount</th>
                                          <th>Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="machine3">
                              <div class="container-wrap">
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-lg-3">
                                      <label>Department</label>
                                      <select class="form-control select2" id="dept_dropdown">
                                        <option value="" disabled="" selected="">Choose Department </option>
                                        <?php foreach ($staffdepartments as $staffdepartment): ?>
                                          <option value="<?php echo $staffdepartment['deptid']; ?>" data-phaseid = '7'><?php echo $staffdepartment['name']; ?></option>
                                        <?php endforeach ?>
                                      </select>
                                    </div>
                                    <div class="col-lg-1">
                                      <label>Add</label>                                                    
                                      <a href="" class="btn btn-primary" id="btnAddM3">+</a>
                                    </div>
                                  </div>
                                </div>
                                  <h3>Machine 3</h3>
                                <div class="row">
                                  <div class="col-lg-12">
                                    <table class="table table-striped st" id="staff_tableM3">
                                      <thead>
                                        <tr>
                                          <th>Sr#</th>
                                            <th>Item</th>
                                          <th>Employee</th>
                                          <th>Qty</th>
                                            <th>Weight</th>
                                          <th>Rate</th>
                                          <th>Amount</th>
                                          <th>Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="machine4">
                              <div class="container-wrap">
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-lg-3">
                                      <label>Department</label>
                                      <select class="form-control select2" id="dept_dropdown">
                                        <option value="" disabled="" selected="">Choose Department </option>
                                        <?php foreach ($staffdepartments as $staffdepartment): ?>
                                          <option value="<?php echo $staffdepartment['deptid']; ?>" data-phaseid = '8'><?php echo $staffdepartment['name']; ?></option>
                                        <?php endforeach ?>
                                      </select>
                                    </div>
                                    <div class="col-lg-1">
                                      <label>Add</label>                                                    
                                      <a href="" class="btn btn-primary" id="btnAddM4">+</a>
                                    </div>
                                  </div>
                                </div>
                                  <h3>Machine 4</h3>
                                <div class="row">
                                  <div class="col-lg-12">
                                    <table class="table table-striped st" id="staff_tableM4">
                                      <thead>
                                        <tr>
                                          <th>Sr#</th>
                                            <th>Item</th>
                                          <th>Employee</th>
                                          <th>Qty</th>
                                            <th>Weight</th>
                                          <th>Rate</th>
                                          <th>Amount</th>
                                          <th>Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="machine5">
                              <div class="container-wrap">
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-lg-3">
                                      <label>Department</label>
                                      <select class="form-control select2" id="dept_dropdown">
                                        <option value="" disabled="" selected="">Choose Department </option>
                                        <?php foreach ($staffdepartments as $staffdepartment): ?>
                                          <option value="<?php echo $staffdepartment['deptid']; ?>" data-phaseid = '9'><?php echo $staffdepartment['name']; ?></option>
                                        <?php endforeach ?>
                                      </select>
                                    </div>
                                    <div class="col-lg-1">
                                      <label>Add</label>                                                    
                                      <a href="" class="btn btn-primary" id="btnAddM5">+</a>
                                    </div>
                                  </div>
                                </div>
                                  <h3>Machine 5</h3>
                                <div class="row">
                                  <div class="col-lg-12">
                                    <table class="table table-striped st" id="staff_tableM5">
                                      <thead>
                                        <tr>
                                          <th>Sr#</th>
                                            <th>Item</th>
                                          <th>Employee</th>
                                          <th>Qty</th>
                                            <th>Weight</th>
                                          <th>Rate</th>
                                          <th>Amount</th>
                                          <th>Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="machine6">
                              <div class="container-wrap">
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-lg-3">
                                      <label>Department</label>
                                      <select class="form-control select2" id="dept_dropdown">
                                        <option value="" disabled="" selected="">Choose Department </option>
                                        <?php foreach ($staffdepartments as $staffdepartment): ?>
                                          <option value="<?php echo $staffdepartment['deptid']; ?>" data-phaseid = '10'><?php echo $staffdepartment['name']; ?></option>
                                        <?php endforeach ?>
                                      </select>
                                    </div>
                                    <div class="col-lg-1">
                                      <label>Add</label>                                                    
                                      <a href="" class="btn btn-primary" id="btnAddM6">+</a>
                                    </div>
                                  </div>
                                </div>
                                  <h3>Machine 6</h3>
                                <div class="row">
                                  <div class="col-lg-12">
                                    <table class="table table-striped st" id="staff_tableM6">
                                      <thead>
                                        <tr>
                                          <th>Sr#</th>
                                            <th>Item</th>
                                          <th>Employee</th>
                                          <th>Qty</th>
                                            <th>Weight</th>
                                          <th>Rate</th>
                                          <th>Amount</th>
                                          <th>Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="helping1">
                              <div class="container-wrap">
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-lg-3">
                                      <label>Department</label>
                                      <select class="form-control select2" id="dept_dropdown">
                                        <option value="" disabled="" selected="">Choose Department </option>
                                        <?php foreach ($staffdepartments as $staffdepartment): ?>
                                          <option value="<?php echo $staffdepartment['deptid']; ?>" data-phaseid = '11'><?php echo $staffdepartment['name']; ?></option>
                                        <?php endforeach ?>
                                      </select>
                                    </div>
                                    <div class="col-lg-1">
                                      <label>Add</label>                                                    
                                      <a href="" class="btn btn-primary" id="btnAddM7">+</a>
                                    </div>
                                  </div>
                                </div>
                                  <h4>Helping 1</h4>
                                <div class="row">
                                  <div class="col-lg-12">
                                    <table class="table table-striped st" id="staff_tableM7">
                                      <thead>
                                        <tr>
                                          <th>Sr#</th>
                                          <th>Item</th>
                                          <th>Employee</th>
                                          <th>Qty</th>
                                            <th>Weight</th>
                                          <th>Rate</th>
                                          <th>Amount</th>
                                          <th>Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="helping2">
                              <div class="container-wrap">
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-lg-3">
                                      <label>Department</label>
                                      <select class="form-control select2" id="dept_dropdown">
                                        <option value="" disabled="" selected="">Choose Department </option>
                                        <?php foreach ($staffdepartments as $staffdepartment): ?>
                                          <option value="<?php echo $staffdepartment['deptid']; ?>" data-phaseid = '12'><?php echo $staffdepartment['name']; ?></option>
                                        <?php endforeach ?>
                                      </select>
                                    </div>
                                    <div class="col-lg-1">
                                      <label>Add</label>                                                    
                                      <a href="" class="btn btn-primary" id="btnAddM8">+</a>
                                    </div>
                                  </div>
                                </div>
                                  <h4>Helping 2</h4>
                                <div class="row">
                                  <div class="col-lg-12">
                                    <table class="table table-striped st" id="staff_tableM8">
                                      <thead>
                                        <tr>
                                          <th>Sr#</th>
                                          <th>Item</th>
                                          <th>Employee</th>
                                          <th>Qty</th>
                                            <th>Weight</th>
                                          <th>Rate</th>
                                          <th>Amount</th>
                                          <th>Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                          <div class="row-fluid">
                              <div class="col-lg-12">
                                  <div class="row">
                                      <div class="col-lg-2">
                                          <label>Total Qty</label>
                                          <input type="text" class="form-control readonly num" id="txtTotalQty" readonly="true" tabindex="-1">
                                      </div>
                                      <div class="col-lg-2">
                                          <label>Total Weight</label>
                                          <input type="text" class="form-control readonly num" id="txtTotalWeight" readonly="true" tabindex="-1">
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="col-lg-2">
                        <div class="row">
                          <div class="col-lg-12">
                            <table class="table table-striped" id="purchase_table">
                              <thead>
                                <tr>
                                  <th>Phase</th>
                                  <th>Amount</th>    
                                </tr>
                              </thead>
                              <tbody>

                              </tbody>
                            </table>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-12">
                            <div class="form-group">
                              <div class="row">

                                  <div class="col-lg-12">
                                      <label>Fitting Amount</label>
                                      <input type="text" class="form-control" id="txtFittingAmnt" readonly="true">
                                      <input class ="hide" type="text" class="form-control" id="txtFittingqty" hidden="true" readonly="true">
                                  </div>
                              </div>
                                  <div class="row">
                                  <div class="col-lg-12">
                                      <label>Helping1 Amount</label>
                                      <input type="text" class="form-control" id="txtHelping1Amnt" readonly="true">
                                      <input class ="hide" type="text" class="form-control" id="txtHelping1qty" hidden="true" readonly="true">
                                  </div>
                                  </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label>Helping2 Amount</label>
                                        <input type="text" class="form-control" id="txtHelping2Amnt" readonly="true">
                                        <input class ="hide" type="text" class="form-control" id="txtHelping2qty" hidden="true" readonly="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label>Clearning Amount</label>
                                        <input type="text" class="form-control" id="txtClearningAmnt" readonly="true">
                                        <input class ="hide" type="text" class="form-control" id="txtClearningqty" hidden="true" readonly="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label>Grinding Amount</label>
                                        <input type="text" class="form-control" id="txtGrindingAmnt" readonly="true">
                                        <input class ="hide" type="text" class="form-control" id="txtGrindingqty" hidden="true" readonly="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label>Warma Amount</label>
                                        <input type="text" class="form-control" id="txtWarmaAmnt" readonly="true">
                                        <input class ="hide" type="text" class="form-control" id="txtWarmaqty" hidden="true" readonly="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label>Machine 1 Amount</label>
                                        <input type="text" class="form-control" id="txtMachine1Amnt" readonly="true">
                                        <input class ="hide" type="text" class="form-control" id="txtMachine1qty" hidden="true" readonly="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label>Machine 2 Amount</label>
                                        <input type="text" class="form-control" id="txtMachine2Amnt" readonly="true">
                                        <input class ="hide" type="text" class="form-control" id="txtMachine2qty" hidden="true" readonly="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label>Machine 3 Amount</label>
                                        <input type="text" class="form-control" id="txtMachine3Amnt" readonly="true">
                                        <input class ="hide" type="text" class="form-control" id="txtMachine3qty" hidden="true" readonly="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label>Machine 4 Amount</label>
                                        <input type="text" class="form-control" id="txtMachine4Amnt" readonly="true">
                                        <input class ="hide" type="text" class="form-control" id="txtMachine4qty" hidden="true" readonly="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label>Machine 5 Amount</label>
                                        <input type="text" class="form-control" id="txtMachine5Amnt" readonly="true">
                                        <input class ="hide" type="text" class="form-control" id="txtMachine5qty" hidden="true" readonly="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label>Machine6 Amount</label>
                                        <input type="text" class="form-control" id="txtMachine6Amnt" readonly="true">
                                        <input class ="hide" type="text" class="form-control" id="txtMachine6qty" hidden="true" readonly="true">
                                    </div>
                                </div>
                                  <div class="row">
                                  <div class="col-lg-12">
                                      <label>Total Amount</label>
                                      <input type="text" class="form-control" id="txtTotAmnt" readonly="true">
                                      <input class ="hide" type="text" class="form-control" id="txtTotqty" hidden="true" readonly="true">
                                  </div>
                                  </div>
                                <!-- <div class="col-lg-6">
                                  <label>Expense Amount</label>
                                  <input type="text" class="form-control" id="txtExpAmnt">
                                </div> -->

                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <select class="form-control select2" id="party_dropdown">
                              <option value="" disabled="" selected="">Choose Party</option>
                              <?php foreach ($accounts as $account): ?>
                                <option value="<?php echo $account['pid']; ?>"><?php echo $account['name']; ?></option>
                              <?php endforeach ?>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form> <!-- end of form -->
                </div>  <!-- end of panel-body -->
              </div>  <!-- end of panel -->
              </div>  <!-- end of Main -->
               <div class="tab-pane fade in Attachmentspanel" id="Attachments">
                                <div class="row">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <input type="number" class="form-control" id="txtIdImg" style="background: #fff;" readonly />
                                        </div>
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-primary" id="viewImages">View Attachments <i class="fa fa-eye"></i></button>
                                            <button type="button" class="btn btn-success" id="uploadFiles" data-insert='<?php echo $vouchers['uploadFiles']['insert']; ?>' data-update='<?php echo $vouchers['uploadFiles']['update']; ?>' data-delete='<?php echo $vouchers['uploadFiles']['delete']; ?>' >Update Files <i class="fa fa-upload"></i></button>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                         <form enctype="multipart/form-data" class="dropzone" id="image-upload" method="POST">
                                                <div>
                                                    <h3>Attach Files to Upload</h3>
                                                    <div class="fallback">
                                                        '<input name="file" type="file" />
                                                    </div>
                                                </div>
                                            </form>
                                            <small style="color: #555;"> Supported Files: (.bmp, .gif, .jpg, .jpeg, .png, .pdf, .doc, .docx, .txt, .xls, .xlsx, .rtf)</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            </div>
              </div>  <!-- end of Tab-content -->
            </div>  <!-- end of col -->
          </div>  <!-- end of row -->
          <div class="row">
            <div class="col-lg-12">
              <div class="panel panel-default">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="pull-right">
                        <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                        <a class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['headproduction']['insert']; ?>' data-updatebtn='<?php echo $vouchers['headproduction']['update']; ?>' data-deletebtn='<?php echo $vouchers['headproduction']['delete']; ?>' data-printbtn='<?php echo $vouchers['headproduction']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
                        <a class="btn btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>
                        <div class="btn-group">
                          <button type="button" class="btn btn-primary btn-sm btnPrint" ><i class="fa fa-save"></i>Print F9</button>
                          <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                          </button>
                          <ul class="dropdown-menu" role="menu">
                            <li><a href="#" class="btnPrint"> Print with header</a></li>
                            <li><a href="#" class="btnprintwithOutHeader"> Print with Out header</a></li>
                          </ul>
                        </div>
                      </div>  
                    </div>             
                  </div><!-- end of row -->
                </div>
              </div>
            </div>
          </div>
        </div>  <!-- end of level 1-->
      </div>
    </div>
  </div>
</div>