<?php

$desc = $this->session->userdata('desc');
$desc = json_decode($desc);
$desc = objectToArray($desc);

$vouchers = $desc['vouchers'];
?>
<!-- main content -->
<div id="main_wrapper">

    <div class="page_bar">
        <div class="row">
            <div class="col-md-5">
                <h1 class="page_title"><i class='fa fa-align-justify'></i> Spare Part Production</h1>
            </div>
            <div class="col-lg-7">
                <div class="pull-right">
                    <div class="col-lg-12">
                        <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                        <a class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['sparepartproduction']['insert']; ?>' data-updatebtn='<?php echo $vouchers['sparepartproduction']['update']; ?>' data-deletebtn='<?php echo $vouchers['sparepartproduction']['delete']; ?>' data-printbtn='<?php echo $vouchers['sparepartproduction']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
                        <a class="btn btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary btn-sm btnPrint" ><i class="fa fa-save"></i>Print F9</button>
                            <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <!-- <li ><a href="#" class="btnPrint"> Print with header </a></li>
                                <li ><a href="#" class="btnprintwithOutHeader"> Print with Out header </a></li> -->
                                <li ><a href="#" class="btnPrints"> Print Production Voucher </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>  <!-- end of col -->
        </div>
    </div>

    <div class="page_content">
        <div class="container-fluid">
<div class="row ">
                <div class="col-lg-12">
                    <ul class="nav nav-pills">
                        <li class="active"><a href="#Main" data-toggle="tab">Main</a></li>
                        <li><a href="#Attachments" data-toggle="tab">Attachments</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="tab-content">
                                <div class="tab-pane active fade in" id="Main">
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <form action="">

                                        <div class="row">
                                            <div class="col-lg-2">
                                                <div class="input-group">
                                                    <span class="input-group-addon id-addon">Sr#</span>
                                                    <input type="number" class="form-control" id="txtVrnoa" >
                                                    <input type="hidden" id="txtMaxVrnoaHidden">
                                                    <input type="hidden" id="txtVrnoaHidden">
                                                    <input type="hidden" id="voucher_type_hidden">

                                                    <input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                                                    <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                                                    <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-lg-offset-1">
                                                <div class="input-group">
                                                    <span class="input-group-addon txt-addon">Date</span>
                                                    <input class="form-control ts_datepicker" type="text" id="current_date">
                                                    <input class="form-control ts_datepicker" type="hidden" id="vrdate">
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="input-group">
                                                    <span class="input-group-addon txt-addon">Vr#</span>
                                                    <input type="text" class="form-control" id="txtVrno" readonly='true'>
                                                    <input type="hidden" id="txtMaxVrnoHidden">
                                                    <input type="hidden" id="txtVrnoHidden">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-2">
                                                <label>Employee Scan Id</label>
                                                <input class='form-control num' type='text' id='employeeScanId'>
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Employee Name</label>
                                                <select class="form-control select2" id="employee_dropdown11">
                                                    <option value="" disabled="" selected="">Choose Name</option>
                                                    <?php foreach ($employees as $employee): ?>
                                                        <option value="<?php echo $employee['pid']; ?>"><?php echo $employee['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Received By</label>
                                                <input class='form-control' type='text' list="receivers" id='receivers_list'>
                                                <datalist id='receivers'>
                                                    <?php foreach ($receivers as $receiver): ?>
                                                    <option value="<?php echo $receiver['received_by']; ?>">
                                                        <?php endforeach ?>
                                                </datalist>
                                            </div>
                                            <div class="col-lg-3" style="display: none;">
                                                <label>Through</label>
                                                <select class="form-control select2" id="through">
                                                    <option value="" disabled="" selected="">Choose</option>
                                                    <?php foreach ($departments as $department): ?>
                                                        <option value="<?php echo $department['did']; ?>"><?php echo $department['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Godown</label>
                                                <select class="form-control select2" id="dept_dropdown">
                                                    <option value="" disabled="" selected="">Choose Godown</option>
                                                    <?php foreach ($departments as $department): ?>
                                                        <option value="<?php echo $department['did']; ?>"><?php echo $department['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-9">
                                                <label>Remarks</label>
                                                <input type="text" class="form-control" id="txtRemarks">
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Expense A/C</label>
                                                <select class="form-control select2" id="expenseParty_dropdown" >
                                                    <option value="" disabled="" selected="">Choose Expense Account</option>
                                                    <?php foreach ($expenseAccount as $party): ?>
                                                        <option value="<?php echo $party['pid']; ?>" data-address="<?php echo $party['address']; ?>" data-uaddress="<?php echo $party['uaddress']; ?>" data-uname="<?php echo $party['uname']; ?>" data-pbalance="<?php echo $party['pbalance']; ?>"><?php echo $party['name']; ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="row"></div>
                                        <div class="container-wrap">
                                            <div class="row">
                                                <div class="col-lg-1">
                                                    <label>Code</label>
                                                    <select class="form-control select2" id="itemid_dropdown">
                                                        <option value="" disabled="" selected="">Code</option>
                                                        <?php foreach ($items as $item): ?>
                                                            <option value="<?php echo $item['item_id']; ?>" data-uom="<?php echo $item['uom']; ?>" data-stqty="<?php echo $item['stqty']; ?>" data-stweight="<?php echo $item['stweight']; ?>" data-grweight="<?php echo $item['grweight']; ?>"><?php echo $item['item_id']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label id="stqty_lbl" >Item Description</label>
                                                    <div class="input-group" >
                                                        <select class="form-control select2" id="item_dropdown">
                                                            <option value="" disabled="" selected="">Item description</option>
                                                            <?php foreach ($items as $item): ?>
                                                                <option value="<?php echo $item['item_id']; ?>" data-uom="<?php echo $item['uom']; ?>" data-stqty="<?php echo $item['stqty']; ?>" data-stweight="<?php echo $item['stweight']; ?>" data-grweight="<?php echo $item['grweight']; ?>"><?php echo $item['item_des']; ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                        <a class="input-group-addon btn btn-primary active btnsearchitem" href="#item-lookup" data-toggle="modal"  style="min-width:40px !important;"><i class="fa fa-search"></i></a>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <label>Phase</label>
                                                    <select class="form-control select2" id="phase_dropdown">
                                                        <option value="" disabled="" selected="">Choose Phase</option>
                                                        <?php foreach ($phases as $phase): ?>
                                                            <option value="<?php echo $phase['id']; ?>"><?php echo $phase['name']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>

                                                <div class="col-lg-1" >
                                                    <label>Qty</label>
                                                    <input type="text" class="form-control num" id="txtSQty">
                                                </div>
                                                <div class="col-lg-1" >
                                                    <label>GW</label>
                                                    <input type="text" class="form-control num" id="txtGW" readonly="" tabindex="-1">
                                                </div>
                                                <div class="col-lg-1" >
                                                    <label>Weight</label>
                                                    <input type="text" class="form-control num" id="txtWeight">
                                                </div>
                                                <div class="col-lg-1" >
                                                    <label>Rate</label>
                                                    <input type="text" class="form-control num" id="txtRate">
                                                </div>
                                                <div class="col-lg-1" >
                                                    <label>Amount</label>
                                                    <input type="text" class="form-control num" id="txtAmount">
                                                </div>
                                                <div class="col-lg-1" >
                                                    <input type="hidden" class="form-control num" id="txtperday">
                                                </div>
                                                <div class="col-lg-1" >
                                                    <input type="hidden" class="form-control num" id="txtproduced"  >
                                                </div>
                                                <div class="col-lg-1">
                                                    <label>Add</label>
                                                    <a href="" class="btn btn-primary" id="btnAdd"><i class="fa fa-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row"></div>
                                        <div class="row"></div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <table class="table table-striped" id="purchase_table">
                                                    <thead>
                                                    <tr>
                                                        <th>Sr#</th>
                                                        <th>Item Name</th>
                                                        <th>Phase</th>
                                                        <th>GW</th>
                                                        <th>Qty</th>
                                                        <th>Weight</th>
                                                        <th>Rate</th>
                                                        <th>Amount</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </form> <!-- end of form -->

                                </div>  <!-- end of panel-body -->
                            </div>  <!-- end of panel -->
                            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-lg-5">
                                            <div class="col-lg-12">
                                                <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                                                <a class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['sparepartproduction']['insert']; ?>' data-updatebtn='<?php echo $vouchers['sparepartproduction']['update']; ?>' data-deletebtn='<?php echo $vouchers['sparepartproduction']['delete']; ?>' data-printbtn='<?php echo $vouchers['sparepartproduction']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
                                                <a class="btn btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary btn-sm btnPrint" ><i class="fa fa-save"></i>Print F9</button>
                                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <!-- <li ><a href="#" class="btnPrint"> Print with header</a></li>
                                                        <li ><a href="#" class="btnprintwithOutHeader"> Print with Out header </a></li> -->
                                                        <li ><a href="#" class="btnPrints"> Print Production Voucher</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>  <!-- end of col -->
                                        <div class="col-lg-2">
                                            <div class="input-group">
                                                <span class="input-group-addon id-addon" >Weight</span>
                                                <input type="text" class="form-control num" id="txtGWeight" readonly="true">
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="input-group">
                                                <span class="input-group-addon id-addon" >Qty</span>
                                                <input type="text" class="form-control num" id="txtGQty" readonly="true">
                                            </div>
                                        </div>
                                        <div class="col-lg-2">


                                            <div class="input-group">
                                                <span class="input-group-addon id-addon" >Amount</span>
                                                <input type="text" class="form-control num" id="txtGAmount" readonly="true">
                                            </div>

                                        </div>
                                    </div>  <!-- end of row -->

                                </div>
                            </div>
                        </div>
                    </div>      
                            </div>  <!-- end of Main -->
                            <div class="tab-pane fade in Attachmentspanel" id="Attachments">
                                <div class="row">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <input type="number" class="form-control" id="txtIdImg" style="background: #fff;" readonly />
                                        </div>
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-primary" id="viewImages">View Attachments <i class="fa fa-eye"></i></button>
                                            <button type="button" class="btn btn-success" id="uploadFiles" data-insert='<?php echo $vouchers['uploadFiles']['insert']; ?>' data-update='<?php echo $vouchers['uploadFiles']['update']; ?>' data-delete='<?php echo $vouchers['uploadFiles']['delete']; ?>' >Update Files <i class="fa fa-upload"></i></button>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                         <form enctype="multipart/form-data" class="dropzone" id="image-upload" method="POST">
                                                <div>
                                                    <h3>Attach Files to Upload</h3>
                                                    <div class="fallback">
                                                        '<input name="file" type="file" />
                                                    </div>
                                                </div>
                                            </form>
                                            <small style="color: #555;"> Supported Files: (.bmp, .gif, .jpg, .jpeg, .png, .pdf, .doc, .docx, .txt, .xls, .xlsx, .rtf)</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            </div>
                            </div>  <!-- end of tab-content -->
                        </div>  <!-- end of col -->
                    </div>  <!-- end of row -->

                    


                </div>  <!-- end of level 1-->
                <div class="col-md-2">
                </div>
            </div>
        </div>
    </div>
</div>

<div id="item-lookup" class="modal fade modal-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h3 id="myModalLabel">item Lookup</h3>
            </div>

            <div class="modal-body">
                <div style="width:250px; float: right !important;top:-10px; position: relative; bottom:30px;">
                    <label for="">Search</label>
                    <input type="text" class="form-control" id="txtItemSearch">
                </div>
                <table class="table" id="tbItems">
                    <!-- <table class="table table-bordered table-striped modal-table"> -->
                    <thead>
                    <tr style="font-size:16px;">
                        <th>Id</th>
                        <th>Code</th>
                        <th>Description</th>
                        <th>Uom</th>
                        <th style='width:3px;'>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <div id="divNoRecord" class="divNoRecord"></div>
                <div id="paging"></div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-danger delete-modal-del">Delete</button> -->
                <button class="btn btn-primary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>