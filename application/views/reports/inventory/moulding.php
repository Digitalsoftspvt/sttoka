<script id="general-head-template" type="text/x-handlebars-template">
    <tr>
        <th class="no_sort">Sr# </th>
        <th class="no_sort" style="width: 100px;">Date </th>
        <th class="no_sort">Vr# </th>
        <th class="no_sort">Name </th>
        <th class="no_sort" style="width:400px;">Item </th>
        <th class="no_sort">UOM </th>
        <th class="no_sort text-right">Qty </th>
        <th class="no_sort text-right">Weight </th>
        <th class="no_sort" style="display:none !important">Rate </th>
        <th class="no_sort" style="display:none !important">Amount </th>
    </tr>
</script>
<script id="summary-voucher-template" type="text/x-handlebars-template">
    <tr>
        <th class="no_sort">Sr# </th>
        <th class="no_sort">Vr# </th>
        <th class="no_sort">Name </th>
        <th class="no_sort">Qty </th>
        <th class="no_sort">Rate </th>
        <th class="no_sort">Amount </th>
    </tr>
</script>
<script id="summary-person-template" type="text/x-handlebars-template">
    <tr>
        <th class="no_sort">Sr# </th>
        <th class="no_sort">Name </th>
        <th class="no_sort">Qty </th>
        <th class="no_sort">Rate </th>
        <th class="no_sort">Amount </th>
    </tr>
</script>
<script id="summary-date-template" type="text/x-handlebars-template">
    <tr>
        <th class="no_sort">Sr# </th>
        <th class="no_sort">Date </th>
        <th class="no_sort">Qty </th>
        <th class="no_sort">Rate </th>
        <th class="no_sort">Amount </th>
    </tr>
</script>
<script id="summary-godownhead-template" type="text/x-handlebars-template">
    <tr>
        <th class="no_sort">Sr# </th>
        <th class="no_sort">Name</th>
        <th class="no_sort">Qty </th>
        <th class="no_sort">Rate </th>
        <th class="no_sort">Amount </th>
    </tr>
</script>
<script id="summary-godownitem-template" type="text/x-handlebars-template">
  <tr>
     <td>{{SERIAL}}</td>
     <td>{{NAME}}</td>
     <td>{{QTY}}</td>
     <td class="text-right" style="text-align:right !important;">{{RATE}}</td>
     <td class="text-right" style="text-align:right !important;">{{NETAMOUNT}}</td>
  </tr>
</script>
<script id="voucher-item-template" type="text/x-handlebars-template">
  <tr>
     <td>{{SERIAL}}</td>
     <td>{{VRDATE}}</td>
     <td>{{{VRNOA}}}</td>
     <td>{{NAME}}</td>
     <td>{{DESCRIPTION}}</td>
     <td >{{UOM}}</td>
     <td class="text-right">{{QTY}}</td>
     <td class="text-right">{{WEIGHT}}</td>
     <td class="text-right" style="text-align:right !important; display:none !important;" >{{RATE}}</td>
     <td class="text-right" style="text-align:right !important; display:none !important;" >{{NETAMOUNT}}</td>
  </tr>
</script>
<script id="voucher-phead-template" type="text/x-handlebars-template">
  <tr class="hightlight_tr">
     <td></td>
     <td></td>
     <td class="tblInvoice"></td>
     <td></td>
     <td>{{NAME}}</td>
     <td></td>
     <td></td>
     <td></td>
     <td class="text-right" style="text-align:right !important;"></td>
  </tr>
</script>
<script id="voucher-ihead-template" type="text/x-handlebars-template">
  <tr class="hightlight_tr">
     <td></td>
     <td></td>
     <td class="tblInvoice"></td>
     <td></td>
     <td>{{DESCRIPTION}}</td>
     <td></td>
     <td></td>
     <td></td>
     <td class="text-right" style="text-align:right !important;"></td>
  </tr>
</script>
<script id="summary-dhead-template" type="text/x-handlebars-template">
  <tr class="hightlight_tr">
     <td></td>
     <td></td>
     <td class="tblInvoice"></td>
     <td></td>
     <td>{{VRDATE}}</td>
     <td></td>
     <td></td>
     <td></td>
     <td class="text-right" style="text-align:right !important;"></td>
  </tr>
</script>
<script id="voucher-dhead-template" type="text/x-handlebars-template">
  <tr class="hightlight_tr">
      <th class="no_sort">Sr# </th>
      <th class="no_sort">Date</th>
      <th class="no_sort">Qty </th>
      <th class="no_sort">Rate </th>
      <th class="no_sort">Amount </th>
  </tr>
</script>
<script id="summary-dateitem-template" type="text/x-handlebars-template">
  <tr>
     <td>{{SERIAL}}</td>
     <td>{{DATE}}</td>
     <td>{{QTY}}</td>
     <td class="text-right" style="text-align:right !important;">{{RATE}}</td>
     <td class="text-right" style="text-align:right !important;">{{NETAMOUNT}}</td>
  </tr>
</script>
<script id="voucher-vhead-template" type="text/x-handlebars-template">
  <tr class="hightlight_tr">
     <td></td>
     <td></td>
     <td>{{{VRNOA}}}</td>
     <td class="tblInvoice"></td>
     <td></td>
     <td></td>
     <td></td>
     <td></td>
     <td style="display:none !important;"></td>
     <td style="display:none !important;"></td>
  </tr>
</script>

<script id="voucher-sum-template" type="text/x-handlebars-template">
  <tr class="finalsum">
     <td></td>
     <td></td>
     <td></td>
     <td class="tblInvoice"></td>
     <td></td>
     <td>{{TOTAL}}</td>
     <td class="text-right">{{ VOUCHER_QTY_SUM }}</td>
     <td class="text-right">{{ VOUCHER_WEIGHT_SUM }}</td>
     <td style="text-align:right !important; display:none !important;"></td>
     <td class="text-right" style="text-align:right !important; display:none !important;" >{{ VOUCHER_SUM }}</td>
  </tr>
</script>
<script id="summary-body-template" type="text/x-handlebars-template">
    <tr>
        <td class="no_sort">{{SERIAL}}</td>
        <td>{{{VRNOA}}}</td>
        <td class="no_sort" style="width:400px;">{{NAME}}</td>
        <td class="no_sort text-right" style="text-align:right !important;">{{QTY}}</td>
        <td class="no_sort text-right" style="text-align:right !important;">{{WEIGHT}}</td>
        <td class="no_sort text-right" style="text-align:right !important;">{{RATE}}</td>
        <td class="no_sort text-right" style="text-align:right !important;">{{NETAMOUNT}}</td>
    </tr>
</script>
<!-- main content -->
<div id="main_wrapper">

    <div class="page_bar">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page_title">Moulding Report</h1>
            </div>
        </div>
    </div>

    <div class="page_content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-lg-12">


                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon">From</span>
                                                <input class="form-control ts_datepicker" type="text" id="from_date">
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon">To</span>
                                                <input class="form-control ts_datepicker" type="text" id="to_date">
                                            </div>
                                        </div>
                                        <div class="col-lg-2"></div>
                                        <div class="col-lg-4">
                                            <div class="pull-right">
                                                <a href='' class="btn btn-primary btn-sm btnSearch">Show Report</a>
                                                <a href='' class="btn btn-success btn-sm btnReset">Reset Filters</a>
                                                <a href='' class="btn btn-success btn-sm btnPrint">Print Report</a>
                                            </div>
                                        </div>
                                    </div>

                                    <legend style='margin-top: 30px;'>Selection Criteria</legend>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <a href='' class="btn btn-primary btn-sm btnSelCre">Voucher Wise</a>
                                            <a href='' class="btn btn-default btn-sm btnSelCre">Person Wise</a>
                                            <a href='' class="btn btn-default btn-sm btnSelCre">Location Wise</a>
                                            <a href='' class="btn btn-default btn-sm btnSelCre">Item Wise</a>
                                            <a href='' class="btn btn-default btn-sm btnSelCre">Date Wise</a>
                                            <a href='' class="btn btn-default btn-sm btnSelCre">Week Wise</a>
                                            <a href='' class="btn btn-default btn-sm btnSelCre">Month Wise</a>
                                            <a href='' class="btn btn-default btn-sm btnSelCre">Year Wise</a>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label class="radio-inline">
                                                    <input type="radio" name="rbRpt" id="Radio1" value="detailed" checked="checked">
                                                    Detailed
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="rbRpt" id="Radio2" value="summary">
                                                    Summary
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="container-fluid">
                                                <div class="pull-right">
                                                    <ul class="stats">
                                                        <li class='blue'>
                                                            <i class="fa fa-money"></i>
                                                            <div class="details">
                                                                <span class="big grand-total">0.00</span>
                                                                <span>Grand Total</span>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Advanced Panels -->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <button type="button" class="btn btnAdvaced">Advanced</button>
                                                </div>
                                            </div>
                                            <div class="panel-group panelDisplay" id="accordion" role="tablist" aria-multiselectable="true">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingOne">
                                                      <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                          General
                                                        </a>
                                                      </h4>
                                                    </div>
                                                    <div id="collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                                                      <div class="panel-body">
                                                        <form class="form-group">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                  
                                                                    
                                                                    <div class="col-lg-3">
                                                                        <label >Choose WareHouse
                                                                        </label>                    
                                                                        <select  class="form-control input-sm select2" multiple="true" id="drpdepartId" data-placeholder="Choose Item....">
                                                                           <!-- <option></option> -->
                                                                            <?php   foreach( $departments as $department):         ?>
                                                                               <option value=<?php echo $department['did']?>><span><?php echo $department['name'];?></span></option>
                                                                            <?php   endforeach                ?>
                                                                        </select>    
                                                                    </div>
                                                                
                                                                    <div class="col-lg-3" >
                                                                        <label >Choose User
                                                                        </label>                    
                                                                        <select  class="form-control input-sm select2" multiple="true" id="drpuserId" data-placeholder="Choose User....">
                                                                           <!-- <option></option> -->
                                                                            <?php   foreach( $userone as $user):         ?>
                                                                               <option value=<?php echo $user['uid']?>><span><?php echo $user['uname'];?></span></option>
                                                                            <?php   endforeach                ?>  
                                                                        </select>   
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                        <div class="row">
                                                            <!-- <div class="col-lg-3 col-lg-offset-9"> -->
                                                                <button class="btn btn-success col-lg-2 col-lg-offset-10" id="reset_criteria">Reset Criteria</button>
                                                            <!-- </div> -->
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingOne">
                                                      <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsetwo" aria-expanded="false" aria-controls="collapsetwo">
                                                          Item
                                                        </a>
                                                      </h4>
                                                    </div>
                                                    <div id="collapsetwo" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                                                      <div class="panel-body">
                                                        <form class="form-group">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="col-lg-3">
                                                                        <label >Item Name
                                                                        </label>                    
                                                                        <select  class="form-control input-sm select2" multiple="true" id="drpitemID" data-placeholder="Choose Item....">
                                                                           <!-- <option></option> -->
                                                                            <?php   foreach( $items as $item):         ?>
                                                                               <option value=<?php echo $item['item_id']?>><span><?php echo $item['item_des'];?></span></option>
                                                                            <?php   endforeach                ?>
                                                                        </select>           
                                                                    </div>
                                                                    <div class="col-lg-3" >
                                                                        <label >Brand
                                                                        </label>        
                                                                        <select  class="form-control input-sm select2 " multiple="true" id="drpbrandID" data-placeholder="Choose Party....">
                                                                           <!-- <option></option> -->
                                                                            <?php   foreach( $brands as $brand):       ?>
                                                                               <option value=<?php echo $brand['bid']?>><span><?php echo $brand['name'];?></span></option>
                                                                            <?php   endforeach  ?>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label >Catogeory
                                                                        </label>                    
                                                                        <select  class="form-control input-sm select2" multiple="true" id="drpCatogeoryid" data-placeholder="Choose Item....">
                                                                           <!-- <option></option> -->
                                                                            <?php   foreach( $categories as $categorie):         ?>
                                                                               <option value=<?php echo $categorie['catid']?>><span><?php echo $categorie['name'];?></span></option>
                                                                            <?php   endforeach                ?>
                                                                        </select>           
                                                                    </div>
                                                                    <div class="col-lg-2">
                                                                        <label >Sub Catogeory
                                                                        </label>                    
                                                                        <select  class="form-control input-sm select2" multiple="true" id="drpSubCat" data-placeholder="Choose Item....">
                                                                           <!-- <option></option> -->
                                                                            <?php   foreach( $subcategories as $subcategori):         ?>
                                                                               <option value=<?php echo $subcategori['subcatid']?>><span><?php echo $subcategori['name'];?></span></option>
                                                                            <?php   endforeach                ?>
                                                                        </select>    
                                                                    </div>
                                                                
                                                                    <div class="col-lg-1" >
                                                                        <label >UOM
                                                                        </label>                    
                                                                        <select  class="form-control input-sm select2" multiple="true" id="drpUom" data-placeholder="Choose User....">
                                                                           <!-- <option></option> -->
                                                                            <?php   foreach( $uoms as $uom):         ?>
                                                                               <option value=<?php echo $uom['uom']?>><span><?php echo $uom['uom'];?></span></option>
                                                                            <?php   endforeach                ?>  
                                                                        </select>   
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                       
                                                      </div>
                                                    </div>

                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingOne">
                                                      <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsethree" aria-expanded="false" aria-controls="collapsethree">
                                                          Account
                                                        </a>
                                                      </h4>
                                                    </div>
                                                    <div id="collapsethree" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                                                      <div class="panel-body">
                                                        <form class="form-group">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="col-lg-2" >
                                                                        <label >Account Name
                                                                        </label>        
                                                                        <select  class="form-control input-sm select2 " multiple="true" id="drpAccountID" data-placeholder="Choose Party....">
                                                                           <!-- <option></option> -->
                                                                            <?php   foreach( $parties as $party):       ?>
                                                                               <option value=<?php echo $party['pid']?>><span><?php echo $party['name'];?></span></option>
                                                                            <?php   endforeach                ?>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-2" >
                                                                        <label >City
                                                                        </label>        
                                                                        <select  class="form-control input-sm select2 " multiple="true" id="drpCity" data-placeholder="Choose Party....">
                                                                           <!-- <option></option> -->
                                                                            <?php   foreach( $cities as $citiy):       ?>
                                                                               <option value=<?php echo $citiy['city']?>><span><?php echo $citiy['city'];?></span></option>
                                                                            <?php   endforeach                ?>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-2">
                                                                        <label >Area
                                                                        </label>                    
                                                                        <select  class="form-control input-sm select2" multiple="true" id="drpCityArea" data-placeholder="Choose Item....">
                                                                           <!-- <option></option> -->
                                                                            <?php   foreach( $cityareas as $cityarea):         ?>
                                                                               <option value=<?php echo $cityarea['cityarea']?>><span><?php echo $cityarea['cityarea'];?></span></option>
                                                                            <?php   endforeach                ?>
                                                                        </select>           
                                                                    </div>
                                                                    <div class="col-lg-2">
                                                                        <label >Level 1
                                                                        </label>                    
                                                                        <select  class="form-control input-sm select2" multiple="true" id="drpl1Id" data-placeholder="Choose Item....">
                                                                           <!-- <option></option> -->
                                                                            <?php   foreach( $l1s as $l1):         ?>
                                                                               <option value=<?php echo $l1['l1']?>><span><?php echo $l1['name'];?></span></option>
                                                                            <?php   endforeach                ?>
                                                                        </select>    
                                                                    </div>
                                                                
                                                                    <div class="col-lg-2" >
                                                                        <label >Level 2
                                                                        </label>                    
                                                                        <select  class="form-control input-sm select2" multiple="true" id="drpl2Id" data-placeholder="Choose User....">
                                                                           <!-- <option></option> -->
                                                                            <?php   foreach( $l2s as $l2):         ?>
                                                                               <option value=<?php echo $l2['l2']?>><span><?php echo $l2['level2_name'];?></span></option>
                                                                            <?php   endforeach                ?>  
                                                                        </select>   
                                                                    </div>
                                                                    <div class="col-lg-2" >
                                                                        <label >Level 3
                                                                        </label>                    
                                                                        <select  class="form-control input-sm select2" multiple="true" id="drpl3Id" data-placeholder="Choose User....">
                                                                           <!-- <option></option> -->
                                                                            <?php   foreach( $l3s as $l3):         ?>
                                                                               <option value=<?php echo $l3['l3']?>><span><?php echo $l3['level3_name'];?></span></option>
                                                                            <?php   endforeach                ?>  
                                                                        </select>   
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                       
                                                      </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- End Advanced Panels -->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <table id="datatable_example" class="table table-striped full table-bordered">
                                                <thead class='dthead'>
                                                </thead>
                                                <tbody id="saleRows" class="report-rows">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>  <!-- end of panel-body -->
                            </div>  <!-- end of panel -->



                        </div>  <!-- end of col -->
                    </div>  <!-- end of row -->

                </div>  <!-- end of level 1-->
            </div>
        </div>
    </div>
</div>