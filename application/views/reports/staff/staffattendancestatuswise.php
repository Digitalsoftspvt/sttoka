<?php

    $desc = $this->session->userdata('desc');
    $desc = json_decode($desc);
    $desc = objectToArray($desc);

    $vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page_title"><span class="badge badge-primary badge_style"></span> Staff Attendance Report</h1>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">

							<div class="row">
								<div class="col-lg-3">
	                                <div class="input-group">
	                                    <span class="input-group-addon txt-addon">From</span>
	                                    <input class="form-control ts_datepicker datepicker" type="text" id="from_date">
	                                </div>
	                            </div>

	                            <div class="col-lg-3">
	                                <div class="input-group">
	                                    <span class="input-group-addon txt-addon">To</span>
	                                    <input class="form-control ts_datepicker datepicker" type="text" id="to_date">
	                                </div>
	                            </div>
								<div class="col-lg-3">
									<div class="input-group">
										<span class="input-group-addon">Stock Location</span>
										<select class="form-control select2" id="dept_dropdown">
		                                  	<option value="-1" selected="">All</option>
		                                  	<?php foreach ($departments as $department): ?>
		                                      	<option value="<?php echo $department['did']; ?>"><?php echo $department['name']; ?></option>
		                                  	<?php endforeach ?>
		                                </select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-3">
									<div class="input-group">
										<span class="input-group-addon">staff Id</span>
										<select class="form-control select2" id="staff_dropdown">
		                                    <option value="" disabled="" selected="">Choose staff Id</option>
		                                    <?php foreach ($staffs as $staff): ?>
		                                    	<option value="<?php echo $staff['staid']; ?>"><?php echo $staff['staid'] . " - " . $staff['name']; ?></option>
		                                    <?php endforeach ?>
		                                </select>
									</div>
								</div>
								<div class="col-lg-3">
									<div class="input-group">
										<span class="input-group-addon">Status</span>
										<select class="form-control select2" id="status_dropdown">
		                                    <option value="-1" selected="">All</option>
		                                    <option value="Present">Present</option>
											<option value="Absent">Absent</option>
											<option value="Paid Leave">Paid Leave</option>
											<option value="Unpaid Leave">Unpaid Leave</option>
											<option value="Rest Day">Rest</option>
											<option value="Gusted Holiday">Gusted Leave</option>
											<option value="Short Leave">Short Leave</option>
											<option value="Outdoor">Outdoor</option>
		                                </select>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="pull-right">
										<a class="btn btn-default btnPrint"><i class="fa fa-print"></i> Print</a>
										<a href='' class="btn btn-primary btnSearch">
	              							<i class="fa fa-search"></i>
	            						Show</a>
	            						<a href="" class="btn btn-danger btnReset">
					                      	<i class="fa fa-refresh"></i>
					                    Reset</a>
				                    </div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-body">

							<table class="table table-striped table-hover" id="atnd-table">
								<thead>
									<tr>
										<th>Sr#</th>
										<th>Vr#</th>
										<th>Date</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>