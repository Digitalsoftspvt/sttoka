<style>
    .switch>div>span {
        text-align: center;
        z-index: 1;
        width: 33.333333% !important;
    }

    .switch {
        min-width: 110px !important;
    }
</style>
<!-- main content -->
<div id="main_wrapper">
    <div class="page_bar">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page_title report_title" style="text-transform:capitalize;"> <?php echo $title; ?> </h1>
            </div>
        </div>
    </div>
    <div class="page_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="from">

                                            </div>
                                            <input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                                            <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                                            <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
                                            <input type="hidden" name="fn_id" id="fn_id" value="<?php echo $this->session->userdata('fn_id'); ?>">
                                            <input type="hidden" name="requestname" id="requestname" value="<?php echo $requestname ?>">
                                            <input type="hidden" name="report_title" id="report_title" value="<?php echo $title ?>">
                                            <input type="hidden" name="requestprint" id="requestprint" value="<?php echo $requestprint ?>">
                                            <input type="hidden" name="company_name" id="company_name" value="<?php echo $this->session->userdata('company_name'); ?>">
                                            <input type="hidden" id="etype" value="<?php echo $etype; ?>">
                                            <input type="hidden" id="detailrecordno">
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="to">

                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="pull-right">
                                                <a href='' class="btn btn-sm btn-success btnSearch" id="btnSearch"><i class="fa fa-search"></i> Search F6</a>
                                                <a href='' class="btn btn-sm btn-success btnReset" id="btnReset"> <i class="fa fa-refresh"></i>Reset F5</a>
                                                <a href='' class="btn btn-sm btn-success btnPrint" id="btnPrint2"><i class="fa fa-print"></i> Print F9</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="Today">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="reporttypes">

                                            </div>
                                            <div class="col-lg-12">
                                                <legend style='margin-top: 30px;'>Selection Criteria</legend>
                                            </div>

                                        </div>
                                        <div class="AllGroups">
                                        </div>
                                        <div class="AllGroups2">
                                        </div>
                                        <?php if ($title == 'Restaurant Cash Flow') : ?>
                                            <div class="row">
                                                <br>
                                                <div class="col-lg-3">
                                                    <label for="">POS Location</label>
                                                    <select class="form-control input-sm select2 filter posid" multiple="ture" data-id='posid' id="posid" data-placeholder="Choose POS Location">
                                                        <?php foreach ($poss as $pos) : ?>
                                                            <option value="<?php echo $pos['posid']; ?>"><?php echo $pos['name']; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                                <div class="col-lg-6 col-lg-offset-3">
                                                    <div class="container-fluid">
                                                        <div class="pull-right">
                                                            <ul class="stats">
                                                                <li class='blue'>
                                                                    <i class="fa fa-money"></i>
                                                                    <div class="details">
                                                                        <span class="big grand-opening" readonly="">0</span>
                                                                        <span>Opening Balance</span>
                                                                    </div>
                                                                </li>
                                                                <li class='blue'>
                                                                    <i class="fa fa-money"></i>
                                                                    <div class="details">
                                                                        <span class="big grand-closing">0</span>
                                                                        <span>Closing Balance</span>
                                                                    </div>
                                                                </li>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <div class="row"></div>

                                        <div class="row hide">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label class="radio-inline">
                                                        <input type="radio" name="types" id="Radio1" value="Wait List" checked="checked">
                                                        Wait List
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="types" id="Radio2" value="Tentative">
                                                        Tentative
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="types" id="Radio2" value="Confirmed">
                                                        Confirmed
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="types" id="Radio2" value="Cancelled">
                                                        Cancelled
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="types" id="Radio2" value="All">
                                                        All
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Advanced Panels -->
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <button type="button" class="btn btnAdvaced">Advanced</button>
                                                    </div>
                                                </div>
                                                <div class="panel-group panelDisplay" id="accordion" role="tablist" aria-multiselectable="true">
                                                    <div class="advancedgenenral">

                                                    </div>
                                                    <div class="advanceditem">

                                                    </div>
                                                    <div class="advancedaccount">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Advanced Panels -->
                                        <div class="row">
                                            <div class="col-lg-12 hide">
                                                <div id="htmlexportPDF" style="padding: 0px 14px;">
                                                    <nav class="btn-bar nav-light">
                                                        <a href="javascript:;" data-action="1" class="btn copy5 btn-glass tool-action btn-success">
                                                            <i class="fa fa-copy fa-lg"></i> F3 Copy
                                                        </a>
                                                        <a href="javascript:;" data-action="3" class="btn excel8 btn-glass tool-action btn-warning">
                                                            <i class="fa fa-file-text-o fa-lg"></i> F8 Excel
                                                        </a>
                                                    </nav>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div id="htmlexportPDF">
                                                <div class="col-lg-12">
                                                    <div class="table-responsive" id="tablesdiv" style="overflow-x: auto;">
                                                        <table id="datatable_example1" class="table table-striped full table-bordered ">
                                                            <tbody id="tbl_body1" class="report-rows saleRows">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" id="chart_tabs" class="disp">
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li id="line_list" role="presentation" class="active"><a href="#area_chart" aria-controls="line_chart" role="tab" data-toggle="tab" data-identifier="area">Area Chart</a></li>
                                                <li role="presentation"><a href="#line_chart" aria-controls="area_chart" role="tab" data-toggle="tab" data-identifier="line">Line Chart</a></li>
                                                <li role="presentation"><a href="#bar_chart" aria-controls="bar_chart" role="tab" data-toggle="tab" data-identifier="bar">Bar Chart</a></li>
                                                <li role="presentation"><a href="#donut_chart" aria-controls="donut_chart" role="tab" data-toggle="tab" data-identifier="donut">Donut Chart</a></li>
                                            </ul>
                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="area_chart">
                                                    <p>
                                                        <h4 align="center"> Area Chart</h4>
                                                        <div id="myfirstareachart" style="height: 200px;"></div>
                                                    </p>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="line_chart">
                                                    <p>
                                                        <h4 align="center"> Line Chart</h4>
                                                        <div id="myfirstlinechart" style="height: 200px;"></div>
                                                    </p>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="bar_chart">
                                                    <p>
                                                        <h4 align="center"> Bar Chart</h4>
                                                        <div id="myfirstbarchart" style="height: 200px;"></div>
                                                    </p>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="donut_chart">
                                                    <p>
                                                        <h4 align="center"> Donut Chart</h4>
                                                        <div id="myfirstdonutchart" style="height: 200px;"></div>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!-- end of panel-body -->
                                </div> <!-- end of panel -->
                            </div> <!-- end of col -->
                        </div> <!-- end of row -->
                    </div> <!-- end of level 1-->
                </div>
            </div>
        </div>
    </div>
    <div id="addEmail" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header" style="background:#2477a4;color:white;padding-bottom:20px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×</button>
                    <h3 id="myModalLabel">Email</h3>
                </div>
                <div class="modal-body">
                    <div style="padding: 10px;">
                        <div class="form-row control-group row-fluid">
                            <label>Enter email address here:</label>
                            <input id="txtAddEmail" type="text" style="width: 80%;">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal">
                        Close</button>
                    <button id="btnSendEmail" class="btn btn-primary">
                        Send</button>
                </div>
            </div>
        </div>
    </div>