
<script id="general-head-template" type="text/x-handlebars-template">
<tr style="border-bottom: 1px solid black !important;border-top: 1px solid black !important;border-collapse: collapse !important;">
    <th class="no_sort">Sr# </th>
    <th class="no_sort" style="min-width: 60px;">Date </th>
    <th class="no_sort text-center vrnoa1" width="70px">Vr# </th>
    <th class="no_sort hidden " style="width: 200px;" >City </th>
    <th class="no_sort hidden " style="width: 200px;" >City Area</th>
    <?php if($etype == "Cash Sale"){
    echo '<th class="no_sort" style="width: 300px;">Customer </th>';
    }
    if($etype !== "Cash Sale"){
    echo '<th class="no_sort" style="width: 300px;">Account </th>';
    }?>
    <th class="no_sort" style="width:400px;">Item </th>
    <?php if($etype == 'Sale' || $etype == 'Sale Order' || $etype == 'Sale Return'){ ?>
    <th class="no_sort" style="width:200px;">Sale Person </th>
    <?php } ?>
    <th class="no_sort" style="width: 100px;">Qty </th>
    <th class="no_sort" style="width: 70px;">Rate </th>
    <th class="no_sort" style="width: 70px;">Gross Amount </th>
    <th class="no_sort" style="width: 60px;">Discount% </th>
    <th class="no_sort" style="width: 60px;">Discount</th>
    <th class="no_sort" style="width: 100px;">Net Amount </th>
</tr>
</script>
<script id="summary-head-template" type="text/x-handlebars-template">
<tr>
    <th class="no_sort">Sr# </th>
    <th class="no_sort">Vr# </th>
    <th class="no_sort">Account </th>
    <th class="no_sort">Qty </th>
    <th class="no_sort">Rate </th>
    <th class="no_sort">Amount </th>
</tr>
</script>
<script id="summary-godownhead-template" type="text/x-handlebars-template">
<tr>
    <th class="no_sort" style="width: 70px;">Sr# </th>
    <th class="no_sort" style="width: 400px;">Description</th>
    <th class="text-right" style="width: 150px;" >Qty </th>
    <th class="text-right" style="width: 100px; display:none;">Rate </th>
    <th class="text-right" style="width: 150px;" >Discount</th>
    <th class="text-right" style="width: 150px;">Amount </th>
</tr>
</script>
<script id="summary-godownitem-template" type="text/x-handlebars-template">
<tr>
    <td>{{SERIAL}}</td>
    <td>{{NAME}}</td>
    <td>{{QTY}}</td>
    <td class="text-right" style="text-align:right !important;">{{RATE}}</td>
    <td class="text-right" style="text-align:right !important;">{{NETAMOUNT}}</td>
</tr>
</script>
<script id="voucher-item-template" type="text/x-handlebars-template">
<tr>
    <td class="text-right">{{SERIAL}}</td>
    <td>{{CATEGORY_NAME}}</td>
    <td>{{SUBCATEGORY_NAME}}</td>
    <td>{{PREMONTH}}</td>
    <td class="text-right">{{THISMONTH}}</td>
    <td class="text-right" style="text-align:right !important;">{{PRVYEARS}}</td>
    <td class="text-right" style="text-align:right !important;">{{PRVMONTHSR}}</td>
    <td class="text-right">{{NETSALEPRVMONTH}}</td>
    <td class="text-right">{{NETSALERTURNEPRVMONTH}}</td>
    <td class="text-right" style="text-align:right !important;">{{NETSALERTURNEPRVYEARS}}</td>
</tr>
</script>
<script id="voucher-itemsummary-template" type="text/x-handlebars-template">
<tr>
    <td>{{SERIAL}}</td>
    <td>{{DESCRIPTION}}</td>
    <td class="text-right">{{QTY}}</td>
    <td class="text-right">{{DISCOUNTA}}</td>
    <td class="text-right" style="text-align:right !important;display:none;">{{RATE}}</td>
    <td class="text-right" style="text-align:right !important;">{{NETAMOUNT}}</td>
</tr>
</script>
<script id="voucher-phead-template" type="text/x-handlebars-template">
<tr class="hightlight_tr">
    <td></td>
    <td></td>
    <td class="tblInvoice"></td>
    <td></td>
    <td>{{NAME}}</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td class="text-right" style="text-align:right !important;"></td>
</tr>
</script>
<script id="voucher-ihead-template" type="text/x-handlebars-template">
<tr class="hightlight_tr">
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td class="tblInvoice"></td>
    <td></td>
    <td>{{DESCRIPTION}}</td>
    <td class="printRemove"></td>
    <td></td>
    <td></td>
    <td class="text-right" style="text-align:right !important;"></td>
</tr>
</script>
<script id="summary-dhead-template" type="text/x-handlebars-template">
<tr class="hightlight_tr">
    <td></td>
    <td></td>
    <td></td>
    <td class="tblInvoice"></td>
    <td></td>
    <td>{{VRDATE}}</td>
    <td></td>
    <td></td>
    <td></td>
    <td class="text-right" style="text-align:right !important;"></td>
</tr>
</script>
<script id="voucher-dhead-template" type="text/x-handlebars-template">
<tr class="hightlight_tr">
    <th class="no_sort">Sr# </th>
    <th class="no_sort">Date</th>
    <th class="no_sort">Qty </th>
    <th class="no_sort">Rate </th>
    <th class="no_sort">Amount </th>
</tr>
</script>
<script id="summary-dateitem-template" type="text/x-handlebars-template">
<tr>
    <td>{{SERIAL}}</td>
    <td>{{DATE}}</td>
    <td>{{QTY}}</td>
    <td class="text-right" style="text-align:right !important;">{{RATE}}</td>
    <td class="text-right" style="text-align:right !important;">{{NETAMOUNT}}</td>
</tr>
</script>
<script id="voucher-vhead-template" type="text/x-handlebars-template">
<tr class="hightlight_tr">
    <td></td>
    <td></td>
    <td class="hidden"></td>
    <td class="hidden"></td>
    <td class="tblInvoice"></td>
    <td ><b>{{{VRNOA1}}}</b></td>
    <td></td>
    <?php if($etype == 'Sale' || $etype == 'Sale Order' || $etype == 'Sale Return'){ ?>
    <td></td>
    <?php } ?>
    <td ></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
</script>
<script id="voucher-sum-template" type="text/x-handlebars-template">
<tr class="finalsum">
    <td></td>
    <td class="tblInvoice"></td>
    <td></td>
    <td class="hidden"></td>
    <td class="hidden"></td>
    <td ></td>
    <?php if($etype == 'Sale' || $etype == 'Sale Order' || $etype == 'Sale Return'){ ?>
    <td></td>
    <?php } ?>
    <td class="text-right txtbold">{{ TOTAL_HEAD }}</td>
    <td class="text-right txtbold">{{ VOUCHER_QTY_SUM }}</td>
    <td style="text-align:right !important;"></td>
    <td style="text-align:right !important;" class="txtbold">{{ VOUCHER_GROSSSUM }}</td>
    <td class="text-right txtbold"></td>
    <td class="text-right txtbold">{{ VOUCHER_DISCOUNTA_SUM }}</td>
    <td class="text-right txtbold" style="text-align:right !important;">{{ VOUCHER_SUM }}</td>
</tr>
</script>
<script id="voucher-sum_summary-template" type="text/x-handlebars-template">
<tr class="finalsum">
    <td ></td>
    <td class="text-right txtbold">{{ TOTAL_HEAD }}</td>
    <td class="text-right txtbold">{{ VOUCHER_QTY_SUM }}</td>
    <td style="text-align:right !important; display:none;"></td>
    <td class="text-right txtbold">{{ VOUCHER_DISCOUNTA_SUM }}</td>
    <td class="text-right txtbold" style="text-align:right !important;">{{ VOUCHER_SUM }}</td>
</tr>
</script>
<script id="summary-body-template" type="text/x-handlebars-template">
<tr>
    <td class="no_sort">{{SERIAL}}</td>
    <td>{{{VRNOA}}}</td>
    <td class="no_sort" style="width:400px;">{{NAME}}</td>
    <td class="no_sort text-right" style="text-align:right !important;">{{QTY}}</td>
    <td class="no_sort text-right" style="text-align:right !important;">{{RATE}}</td>
    <td class="no_sort text-right" style="text-align:right !important;">{{NETAMOUNT}}</td>
</tr>
</script>
<!-- main content -->
<div id="main_wrapper">
    <div class="page_bar">
        <div class="row">
            <div class="col-md-4">
                <h1 class="page_title"><span class=""><i class="ion-document-text"></i></span> <?php echo $title; ?></h1>
            </div>
            <div class="col-lg-8">
                <div class="pull-right">
                    <a href='' class="btn btn-sm btn-success btnSearch" id="btnChart" ><i class="fa fa-bar-chart-o"></i> Show Chart F4</a>
                    <a href='' class="btn btn-sm btn-success btnSearch" id="btnSearch" ><i class="fa fa-search"></i> Show Report F6</a>
                    <a href='' class="btn btn-sm btn-success btnReset" id="btnReset"> <i class="fa fa-refresh"></i>Reset Filters F5</a>
                    <!-- <a href='' class="btn btn-sm btn-success btnSearch" id="sendEmail" data-target="#sendReportEmail" data-toggle="modal"><i class="fa fa-envelope"></i> Email</a> -->
                    <!-- <a href='' class="btn btn-sm btn-success " id="btnPrint"><i class="fa fa-print"></i>Print F9</a>
                    <a href='' class="btn btn-sm btn-success " id="btnPrint2"><i class="fa fa-print"></i>Pdf F8</a> -->
                    <!-- <div class="btn-group"> -->
                    <!-- <a href="" class="btn btn-primary btn-lg btnPrint" ><i class="fa fa-save"></i>Print F9</a> -->
                    <!--                                                   <button type="button" class="btn btn-primary btn-lg dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li ><a href="#" class="btnPrint">Print F9</a></li>
                        <li ><a href="#" class="btnPrint2">Pdf F8</a></li>
                    </ul>
                </div>
            -->
        </div>
    </div>
</div>
</div>
<div class="page_content pcontent_style">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-3 hide">
                                        <div class="input-group">
                                            <span class="input-group-addon">From</span>
                                            <input class="form-control ts_datepicker" type="text" id="from_date">
                                        </div>
                                       
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">Til Date</span>
                                            <input class="form-control ts_datepicker" type="text" id="to_date">
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                                <legend style='margin-top: 30px;'>Selection Criteria</legend>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label >Choose WareHouse
                                        </label>
                                        <select  class="form-control input-sm select2" id="drpdepartId" data-placeholder="Choose WareHouse...."  multiple="true">
                                            <!-- <option></option> -->
                                            <?php   foreach( $departments as $department):         ?>
                                            <option data-id="<?php echo $department['did']?>" value="<?php echo $department['did']?>" ><span><?php echo $department['name'];?></span></option>
                                            <?php   endforeach                ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                        <label >Choose Category
                                        </label>
                                        <select  class="form-control input-sm select2" id="drpCatId" data-placeholder="Choose Category...."  multiple="true">
                                            <!-- <option></option> -->
                                            <?php   foreach( $categories as $category):         ?>
                                            <option value="<?php echo $category['catid']?>" ><span><?php echo $category['name'];?></span></option>
                                            <?php   endforeach                ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                        <label >Choose Subcategory
                                        </label>
                                        <select  class="form-control input-sm select2" id="drpSubCatId" data-placeholder="Choose Subcategory...."  multiple="true">
                                            <!-- <option></option> -->
                                            <?php   foreach( $subcategories as $subcategory):         ?>
                                            <option value="<?php echo $subcategory['subcatid']?>" ><span><?php echo $subcategory['name'];?></span></option>
                                            <?php   endforeach                ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                        <label >Item Types
                                        </label>
                                        <select  class="form-control input-sm select2" id="drpSubCatId" data-placeholder="Choose Item Types...."  multiple="true">
                                            <!-- <option></option> -->
                                            <?php   foreach( $itemtype as $item):         ?>
                                            <option value="<?php echo $item['type']?>" ><span><?php echo $item['type'];?></span></option>
                                            <?php   endforeach                ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row hide">
                                    <div class="col-lg-12">
                                        <a href='' class="btn btn-primary btn-sm btnSelCre">Voucher Wise</a>
                                        <?php if($etype == "Cash Sale"){
                                        echo '<a href="" class="btn btn-default btn-sm btnSelCre">Customer Wise</a>';
                                        }
                                        if($etype !== "Cash Sale"){
                                        echo '<a href="" class="btn btn-default btn-sm btnSelCre">Account Wise</a>';
                                        }?>
                                        <a href='' class="btn btn-default btn-sm btnSelCre">Godown Wise</a>
                                        <a href='' class="btn btn-default btn-sm btnSelCre">Item Wise</a>
                                        
                                        <a href='' class="btn btn-default btn-sm btnSelCre">User Wise</a>
                                        <a href='' class="btn btn-default btn-sm btnSelCre">Year Wise</a>
                                        <a href='' class="btn btn-default btn-sm btnSelCre">Month Wise</a>
                                        <a href='' class="btn btn-default btn-sm btnSelCre">WeekDay Wise</a>
                                        <a href='' class="btn btn-default btn-sm btnSelCre">Date Wise</a>
                                        <a href='' class="btn btn-default btn-sm btnSelCre">Rate Wise</a>
                                        <a href='' class="btn btn-default btn-sm btnSelCre">City Wise</a>
                                        <a href='' class="btn btn-default btn-sm btnSelCre">Area Wise</a>
                                        <a href='' class="btn btn-default btn-sm btnSelCre">Region Wise</a>
                                        <a href='' class="btn btn-default btn-sm btnSelCre">Province Wise</a>
                                        <?php if($etype == 'Sale' || $etype == 'Sale Order' || $etype == 'Sale Return' ) {?>
                                        <a href='' class="btn btn-default btn-sm btnSelCre">Sale Person Wise</a>
                                        <a href='' class="btn btn-default btn-sm btnSelCre">Status Wise</a>
                                        <a href='' class="btn btn-default btn-sm btnSelCre">Route Wise</a>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="radio-inline">
                                                <input type="radio" name="rbRpt" id="Radio1" value="qty" checked="checked">
                                                Qty Wise
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="rbRpt" id="Radio2" value="amount">
                                                Amount Wise
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="container-fluid">
                                            <div class="pull-right">
                                                <ul class="stats">
                                                    <li class='blue'>
                                                        <i class="fa fa-money"></i>
                                                        <div class="details">
                                                            <span class="big grand-total">0.00</span>
                                                            <span>Grand Total</span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Advanced Panels -->
                                <div class="row hide">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <button type="button" class="btn btnAdvaced">Advanced</button>
                                            </div>
                                        </div>
                                        <div class="panel-group panelDisplay" id="accordion" role="tablist" aria-multiselectable="true">
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                    <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                        General
                                                    </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                                                    <div class="panel-body">
                                                        <form class="form-group">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    
                                                                    
                                                                    <div class="col-lg-3">
                                                                        <label >Choose WareHouse
                                                                        </label>
                                                                        <select  class="form-control input-sm select2" multiple="true" id="drpdepartId" data-placeholder="Choose Item....">
                                                                            <!-- <option></option> -->
                                                                            <?php   foreach( $departments as $department):         ?>
                                                                            <option value=<?php echo $department['did']?>><span><?php echo $department['name'];?></span></option>
                                                                            <?php   endforeach                ?>
                                                                        </select>
                                                                    </div>
                                                                    
                                                                    <div class="col-lg-3" >
                                                                        <label >Choose User
                                                                        </label>
                                                                        <select  class="form-control input-sm select2" multiple="true" id="drpuserId" data-placeholder="Choose User....">
                                                                            <!-- <option></option> -->
                                                                            <?php   foreach( $userone as $user):         ?>
                                                                            <option value=<?php echo $user['uid']?>><span><?php echo $user['uname'];?></span></option>
                                                                            <?php   endforeach                ?>
                                                                        </select>
                                                                    </div>
                                                                    <?php if($etype !== "Cash Sale"){
                                                                    echo '<div class="col-lg-3 hide" >';
                                                                        }
                                                                        if($etype == "Cash Sale"){
                                                                        echo '<div class="col-lg-3" >';
                                                                            }?>
                                                                            
                                                                            <label >Choose Customer
                                                                            </label>
                                                                            <select  class="form-control input-sm select2" multiple="true" id="drpCustomerName" data-placeholder="Choose User....">
                                                                                <!-- <option></option> -->
                                                                                <?php   foreach( $Customers as $Customer):         ?>
                                                                                <option value='<?php echo $Customer['customer_name'];?>'><span><?php echo $Customer['customer_name'];?></span></option>
                                                                                <?php   endforeach                ?>
                                                                            </select>
                                                                        </div>
                                                                        <?php if($etype == 'Sale Order'):?>
                                                                        <div class="col-lg-3" >
                                                                            <label >Choose status
                                                                            </label>
                                                                            <select  class="form-control input-sm select2" multiple="true" id="drpstatusId" data-placeholder="Choose User....">
                                                                                <!-- <option></option> -->
                                                                                
                                                                                <option value="'pending'"><span>Pending</span></option>
                                                                                <option value="'approved'"><span>approved</span></option>
                                                                                <option value="'cancel'"><span>cancel</span></option>
                                                                                
                                                                            </select>
                                                                        </div>
                                                                        <?php endif?>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                            <div class="row">
                                                                <!-- <div class="col-lg-3 col-lg-offset-9"> -->
                                                                <button class="btn btn-success col-lg-2 col-lg-offset-10" id="reset_criteria">Reset Criteria</button>
                                                                <!-- </div> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingOne">
                                                        <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsetwo" aria-expanded="false" aria-controls="collapsetwo">
                                                            Item
                                                        </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapsetwo" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                                                        <div class="panel-body">
                                                            <form class="form-group">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="col-lg-3">
                                                                            <label >Item Name
                                                                            </label>
                                                                            <select  class="form-control input-sm select2" multiple="true" id="drpitemID" data-placeholder="Choose Item....">
                                                                                <!-- <option></option> -->
                                                                                <?php   foreach( $items as $item):         ?>
                                                                                <option value=<?php echo $item['item_id']?>><span><?php echo $item['item_des'];?></span></option>
                                                                                <?php   endforeach                ?>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-lg-3" >
                                                                            <label >Brand
                                                                            </label>
                                                                            <select  class="form-control input-sm select2 " multiple="true" id="drpbrandID" data-placeholder="Choose Party....">
                                                                                <!-- <option></option> -->
                                                                                <?php   foreach( $brands as $brand):       ?>
                                                                                <option value=<?php echo $brand['bid']?>><span><?php echo $brand['name'];?></span></option>
                                                                                <?php   endforeach  ?>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-lg-3">
                                                                            <label >Catogeory
                                                                            </label>
                                                                            <select  class="form-control input-sm select2" multiple="true" id="drpCatogeoryid" data-placeholder="Choose Item....">
                                                                                <!-- <option></option> -->
                                                                                <?php   foreach( $categories as $categorie):         ?>
                                                                                <option value=<?php echo $categorie['catid']?>><span><?php echo $categorie['name'];?></span></option>
                                                                                <?php   endforeach                ?>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-lg-2">
                                                                            <label >Sub Catogeory
                                                                            </label>
                                                                            <select  class="form-control input-sm select2" multiple="true" id="drpSubCat" data-placeholder="Choose Item....">
                                                                                <!-- <option></option> -->
                                                                                <?php   foreach( $subcategories as $subcategori):         ?>
                                                                                <option value=<?php echo $subcategori['subcatid']?>><span><?php echo $subcategori['name'];?></span></option>
                                                                                <?php   endforeach                ?>
                                                                            </select>
                                                                        </div>
                                                                        
                                                                        <div class="col-lg-1" >
                                                                            <label >UOM
                                                                            </label>
                                                                            <select  class="form-control input-sm select2" multiple="true" id="drpUom" data-placeholder="Choose User....">
                                                                                <!-- <option></option> -->
                                                                                <?php   foreach( $uoms as $uom):         ?>
                                                                                <option value=<?php echo $uom['uom']?>><span><?php echo $uom['uom'];?></span></option>
                                                                                <?php   endforeach                ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                            
                                                        </div>
                                                    </div>
                                                </div><?php if($etype == "Cash Sale"){
                                                echo '<div class="panel panel-default hide">';
                                                    }
                                                    if($etype !== "Cash Sale"){
                                                    echo '<div class="panel panel-default ">';
                                                        }?>
                                                        
                                                        <div class="panel-heading" role="tab" id="headingOne">
                                                            <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapsethree" aria-expanded="false" aria-controls="collapsethree">
                                                                Account
                                                            </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapsethree" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                                                            <div class="panel-body">
                                                                <form class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-lg-12">
                                                                            <div class="col-lg-2" >
                                                                                <label >Account Name
                                                                                </label>
                                                                                <select  class="form-control input-sm select2 " multiple="true" id="drpAccountID" data-placeholder="Choose Party....">
                                                                                    <!-- <option></option> -->
                                                                                    <?php   foreach( $parties as $party):       ?>
                                                                                    <option value=<?php echo $party['pid']?>><span><?php echo $party['name'];?></span></option>
                                                                                    <?php   endforeach                ?>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-lg-2" >
                                                                                <label >Province
                                                                                </label>
                                                                                <select  class="form-control input-sm select2 " multiple="true" id="drpProvince" data-placeholder="Choose Province....">
                                                                                    <!-- <option></option> -->
                                                                                    <?php   foreach( $province as $provinc):       ?>
                                                                                    <option value=<?php echo $provinc['pvcid']?>><span><?php echo $provinc['name'];?></span></option>
                                                                                    <?php   endforeach                ?>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-lg-2">
                                                                                <label >Region
                                                                                </label>
                                                                                <select  class="form-control input-sm select2" multiple="true" id="drpRegion" data-placeholder="Choose Region....">
                                                                                    <!-- <option></option> -->
                                                                                    <?php   foreach( $region as $regions):         ?>
                                                                                    <option value=<?php echo $regions['rid']?>><span><?php echo $regions['regionname'];?></span></option>
                                                                                    <?php   endforeach                ?>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-lg-2">
                                                                                <label >Level 1
                                                                                </label>
                                                                                <select  class="form-control input-sm select2" multiple="true" id="drpl1Id" data-placeholder="Choose Item....">
                                                                                    <!-- <option></option> -->
                                                                                    <?php   foreach( $l1s as $l1):         ?>
                                                                                    <option value=<?php echo $l1['l1']?>><span><?php echo $l1['name'];?></span></option>
                                                                                    <?php   endforeach                ?>
                                                                                </select>
                                                                            </div>
                                                                            
                                                                            <div class="col-lg-2" >
                                                                                <label >Level 2
                                                                                </label>
                                                                                <select  class="form-control input-sm select2" multiple="true" id="drpl2Id" data-placeholder="Choose User....">
                                                                                    <!-- <option></option> -->
                                                                                    <?php   foreach( $l2s as $l2):         ?>
                                                                                    <option value=<?php echo $l2['l2']?>><span><?php echo $l2['level2_name'];?></span></option>
                                                                                    <?php   endforeach                ?>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-lg-2" >
                                                                                <label >Level 3
                                                                                </label>
                                                                                <select  class="form-control input-sm select2" multiple="true" id="drpl3Id" data-placeholder="Choose User....">
                                                                                    <!-- <option></option> -->
                                                                                    <?php   foreach( $l3s as $l3):         ?>
                                                                                    <option value=<?php echo $l3['l3']?>><span><?php echo $l3['level3_name'];?></span></option>
                                                                                    <?php   endforeach                ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class='row'>
                                                                        <div class='col-lg-12'>
                                                                            <div class="col-lg-2" >
                                                                                <label >City
                                                                                </label>
                                                                                <select  class="form-control input-sm select2 " multiple="true" id="drpCity" data-placeholder="Choose Party....">
                                                                                    <!-- <option></option> -->
                                                                                    <?php   foreach( $cities as $citiy):       ?>
                                                                                    <option value=<?php echo $citiy['cid']?>><span><?php echo $citiy['name'];?></span></option>
                                                                                    <?php   endforeach                ?>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-lg-2">
                                                                                <label >Area
                                                                                </label>
                                                                                <select  class="form-control input-sm select2" multiple="true" id="drpArea" data-placeholder="Choose Item....">
                                                                                    <!-- <option></option> -->
                                                                                    <?php   foreach( $areas as $area):         ?>
                                                                                    <option value=<?php echo $area['area_id']?>><span><?php echo $area['name'];?></span></option>
                                                                                    <?php   endforeach                ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                                
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="row">
                                            <div class="col-lg-12 hide">
                                                <div id="htmlexportPDF" style="padding: 0px 14px;">
                                                    <nav class="btn-bar nav-light">
                                                        <!-- <a href="javascript:;" data-action="0" class="btn print3 btn-glass tool-action btn-primary">
                                                            <i class="fa fa-print fa-lg"></i> F2 Print
                                                        </a> -->
                                                        <a href="javascript:;" data-action="1" class="btn copy5 btn-glass tool-action btn-success">
                                                            <i class="fa fa-copy fa-lg"></i> F3 Copy
                                                        </a>
                                                        <!-- <a href="javascript:;" data-action="2" class="btn pdf7 btn-glass tool-action btn-danger">
                                                            <i class="fa fa-file fa-lg"></i> F7 PDF
                                                        </a> -->
                                                        <a href="javascript:;" data-action="3" class="btn excel8 btn-glass tool-action btn-warning">
                                                            <i class="fa fa-file-text-o fa-lg"></i> F8 Excel
                                                        </a>
                                                        <a href="javascript:;" data-action="4" class="btn csv10 btn-glass tool-action btn-primary">
                                                            <i class="fa fa-file-text fa-lg"></i> F10 CSV
                                                        </a>
                                                        <a href="javascript:;" data-action="5" class="btn btn-glass tool-action btn-default buttons-colvis">
                                                            <i class="fa fa-eye fa-lg"></i> Columns Visible
                                                        </a>
                                                    </nav>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Advanced Panels -->
                                        <div class="row">
                                            <div id="htmlexportPDF">
                                                <div class="col-lg-12 tableDate ">
                                                    <table id="datatable_example" class="table table-striped full table-bordered " >
                                                        
                                                     
                                                        <tbody id="tbl_body" class="report-rows tbl_body saleRows" style="border-collapse:collapse !important;">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" id="chart_tabs" class="disp" >
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li id="line_list" role="presentation" class="active"><a href="#area_chart" aria-controls="line_chart" role="tab" data-toggle="tab" data-identifier="area">Area Chart</a></li>
                                                <li role="presentation"><a href="#line_chart" aria-controls="area_chart" role="tab" data-toggle="tab" data-identifier="line">Line Chart</a></li>
                                                <li role="presentation"><a href="#bar_chart" aria-controls="bar_chart" role="tab" data-toggle="tab" data-identifier="bar">Bar Chart</a></li>
                                                <li role="presentation"><a href="#donut_chart" aria-controls="donut_chart" role="tab" data-toggle="tab" data-identifier="donut">Donut Chart</a></li>
                                            </ul>
                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="area_chart">
                                                    <p>
                                                        <h4 align="center"> Area Chart</h4>
                                                        <div id="myfirstareachart" style="height: 200px;"></div>
                                                    </p>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="line_chart">
                                                    <p>
                                                        <h4 align="center">  Line Chart</h4>
                                                        <div id="myfirstlinechart" style="height: 200px;"></div>
                                                    </p>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="bar_chart">
                                                    <p>
                                                        <h4 align="center">  Bar Chart</h4>
                                                        <div id="myfirstbarchart" style="height: 200px;"></div>
                                                    </p>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="donut_chart">
                                                    <p>
                                                        <h4 align="center">  Donut Chart</h4>
                                                        <div id="myfirstdonutchart" style="height: 200px;"></div>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        </div>  <!-- end of panel-body -->
                                        </div>  <!-- end of panel -->
                                        </div>  <!-- end of col -->
                                        </div>  <!-- end of row -->
                                        </div>  <!-- end of level 1-->
                                    </div>
                                </div>
                            </div>
                        </div>