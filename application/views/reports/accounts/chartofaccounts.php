<script id="ledger-level1-template" type="text/x-handlebars-template">
    <tr class='level1head active'>
        <td class='headAlign'>{{ACCOUNT_ID}}</td>
        <td class='headAlign'>{{L1NAME}}</td>
        <td></td>
        <td></td>
    </tr>
</script>
<script id="ledger-level2-template" type="text/x-handlebars-template">
    <tr class='level2head success'>
        <td class='headAlign'>{{ACCOUNT_ID}}</td>
        <td class='headAlign'>{{L2NAME}}</td>
        <td></td>
        <td></td>
    </tr>
</script>
<script id="ledger-level3-template" type="text/x-handlebars-template">
    <tr class='level3head info'>
        <td class='headAlign'>{{ACCOUNT_ID}}</td>
        <td class='headAlign'>{{L3NAME}}</td>
        <td></td>
        <td></td>
    </tr>
</script>
<script id="chartOfAccountRow-template" type="text/x-handlebars-template">
<tr>
     <td class="hiddenRep">{{ACCOUNT_ID}}</td>
     <td class="hide"><span class='reportShow' style='visibility: hidden;'>{{MOB}}</span></td>
     <td class="hiddenRep">{{PARTY_NAME}}</td>
     <td class="hide"><span class='reportShow' style='visibility: hidden; font-family: Jameel Noori Nastaleeq !important; font-size: 16px'>{{UADDRESS}}</span></td>
     <td class='textRight hiddenRep'>{{TYPE}}</td>
     <td class="hide" ><span class='reportShow' style='visibility: hidden; font-family: Jameel Noori Nastaleeq !important; font-size: 16px'>{{UNAME}}</span></td>
     <td class="hiddenRep">{{STATUS}}</td>
     <td class="hide"><span class='reportShow' style='visibility: hidden; font-family: Jameel Noori Nastaleeq !important; font-size: 16px'>{{SERIAL}}</span></td>
</tr>
</script>

<!-- main content -->
<div id="main_wrapper">

    <div class="page_bar">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page_title">Chart of Accounts</h1>
            </div>
        </div>
    </div>

    <div class="page_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <!-- <div class="btn-group">
                                  <button type="button" class="btn btn-primary btn-lg " id ="btnPrint" ><i class="fa fa-save"></i>Print F9</button>
                                  <button type="button" class="btn btn-primary btn-lg dropdown-toggle1" data-toggle="dropdown" aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                  </button>
                                  <ul class="dropdown-menu" role="menu">
                                    <li ><a href="#" class="" id="btnPrint" > Print F9</li>
                                  </ul>
                    </div> -->

                                    <legend style='margin-top: 30px;'>Group By</legend>
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <label for="catagory" class="radio cpvRadio">
                                                <input type="radio" id='date' name="grouping" value="party.city" checked="checked" />
                                                City Wise
                                            </label>
                                        </div>

                                        <div class="col-lg-2">
                                            <label for="subcatagory" class="radio crvRadio">
                                                <input type="radio" id='subcatagory' name="grouping" value="party.cityarea" />
                                                Area Wise
                                            </label>
                                        </div>

                                        <div class="col-lg-2">
                                            <label for="brand" class="radio crvRadio">
                                                <input type="radio" id='brand' name="grouping" value="party.level3" />
                                                Type
                                            </label>
                                        </div>
                                        <div class="col-lg-2">
                                            <label for="typee" class="radio crvRadio">
                                                <input type="radio" id='Type' name="grouping" value="party.account_id" />
                                                Code Wise
                                            </label>
                                        </div>
                                        <div class="col-lg-2">
                                            <label for="uom" class="radio crvRadio">
                                                <input type="radio" id='uom' name="grouping" value="party.pid" />
                                                Party Id Wise
                                            </label>
                                        </div>                                        
                                    </div>
                                    <legend style='margin-top: 30px;'>Selection Criteria</legend>
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <label>Level 1</label>
                                            <!--  -->
                                            <select class="form-control" id="txtLevel1" >
                                                <option value="" disabled="" selected="">Choose Level1</option>
                                                <?php foreach ($l1s as $l1): ?>
                                                    <option value="<?php echo $l1['l1']; ?>"><?php echo $l1['name']; ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                        <div class="col-lg-2">
                                            <label>Level 2</label>
                                            <select class="form-control"  id="txtLevel2">
                                                <option value="" disabled="" selected="">Choose Level2</option>
                                                <?php   foreach( $l2s as $l2):         ?>
                                                <option value="<?php echo $l2['l2']?>"><?php echo $l2['level2_name'];?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                        <div class="col-lg-2">
                                            <label>Type</label>
                                            <select class="form-control"  id="txtLevel3">
                                                <option value="" disabled="" selected="">Choose Type</option>
                                                <?php   foreach( $l3s as $l3):         ?>
                                                <option value="<?php echo $l3['l3']?>"><?php echo $l3['level3_name'];?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                        <div class="col-lg-2">
                                            <label>City</label>
                                            <select class="form-control"  id="txtCity">
                                                <option value="" disabled="" selected="">Choose City</option>
                                                <?php   foreach( $cities as $citiy):       ?>
                                                <option value="<?php echo $citiy['city']?>"><?php echo $citiy['city'];?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                        <div class="col-lg-2">
                                            <label>Area</label>
                                            <select class="form-control"  id="txtArea">
                                                <option value="" disabled="" selected="">Choose Area</option>
                                                <?php   foreach( $cityareas as $cityarea):         ?>
                                                <option value="<?php echo $cityarea['cityarea']?>"><?php echo $cityarea['cityarea'];?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>

                                    </div>
                                    <legend style='margin-top: 30px;'>Party Status</legend>
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <label for="active" class="radio cpvRadio">
                                                <input type="radio" id="active_item" name="etype" value="party.active=1" checked="checked" />
                                                Active
                                            </label>
                                        </div>

                                        <div class="col-lg-2">
                                            <label for="active" class="radio crvRadio">
                                                <input type="radio" id="inactive_item" name="etype" value="party.active=0" />
                                                In Active
                                            </label>
                                        </div>

                                        <div class="col-lg-2">
                                            <label for="all_item" class="radio crvRadio">
                                                <input type="radio" id="all_item" name="etype" value="party.pid<>0" />
                                                All
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <a href="" class="btn btn-primary show-rept">Show Report</a>
                                            <a href="" class="btn btn-danger reset-rept">Reset Filters</a>
                                            <a href="" class="printCpvCrvBtn btn btn-primary" id="btnPrint">Print Report</a>
                                            <a href="" class="printCpvCrvBtnUr btn btn-primary" id="btnPrintUr">Urdu Print Report</a>
                                            <a href="" class="printPayRcvBtn btn btn-primary" style="display:none;">Print Report</a>
                                            <a href="" class="printDayBook btn btn-primary" style="display:none;">Print Report</a>
                                        </div>
                                    </div>

                    <div class="panel panel-default">
                        
                        <div class="panel-body">
                            

                            <table id="datatable_example" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="hidden-phone" data-hide="phone,tablet">Account Id
                                        </th>
                                        <th class="no_sort" data-class="expand">Account Name
                                        </th>
                                        <th class="no_sort">Type</th>
                                        <th class="no_sort">Status</th>
                                    </tr>
                                </thead>
                                <tbody id="chartOfAccountRows">
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>