<script id="cheque-template" type="text/x-handlebars-template">
  <tr>
     <td class="ucase">{{DCNO}}</td>
     <td class="ucase">{{VRDATE}}</td>
     <td class="ucase">{{ACCOUNT}}</td>
     <td class="ucase">{{BANK_NAME}}</td>
     <td class="ucase">{{CHEQUE_NO}}</td>
     <td class="ucase">{{CHEQUE_DATE}}</td>
     <td class="ucase text-right">{{AMOUNT}}</td>
     <td class="ucase">{{STATUS}}</td>
     <td class="ucase">{{PARTY_NAME}}</td>
     <td class="ucase">{{POST}}</td>
  </tr>
</script>

<!-- main content -->
<div id="main_wrapper">

    <div class="page_bar">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page_title">Cheque Reports</h1>                
            </div>
        </div>
    </div>

    <div class="page_content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon txt-addon">From</span>
                                        <input class="form-control ts_datepicker" type="text" id="from_date">
                                        <input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                                        <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                                        <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon txt-addon">To</span>
                                        <input class="form-control ts_datepicker" type="text" id="to_date">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <label for="pdci" class="radio cpvRadio">
                                                <input type="radio" id='pdci' name="report_type" value="pd_issue" checked="checked" /> 
                                                Post dated Cheque Issue
                                            </label>
                                        </div>

                                        <div class="col-lg-4">
                                            <label for="pdcr" class="radio crvRadio">
                                                <input type="radio" id='pdcr' name="report_type" value="pd_receive" /> 
                                                Post dated Cheque Receive
                                            </label>
                                        </div>                                    
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="pull-right">
                                        <a class="btn btn-default btnPrint"><i class="fa fa-print"></i> Print F9</a>
                                        <a class="btn btn-default btnSearch"><i class="fa fa-search"></i> Show F6</a>
                                        <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="container-fluid">
                                        <div class="pull-right">
                                            <ul class="stats">
                                                <li class="blue">
                                                    <i class="fa fa-money"></i>
                                                    <div class="details">
                                                        <span class="big grand-total">0.00</span>
                                                        <span>Grand Total</span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-lg-12">
                                    
                                    <table id="datatable_inventory" class="table table-striped full table-bordered dthead">
                                        <thead>
                                            <tr>
                                                <th style="">Vr#
                                                </th>
                                                <th class="no_sort">Date
                                                </th>
                                                <th class="no_sort">Account
                                                </th>
                                                <th class="">Bank Name
                                                </th>
                                                <th class="">Cheque#
                                                </th>
                                                <th class="">ChqDate
                                                </th>
                                                <th class="">Amount
                                                </th>
                                                <th class="">Status
                                                </th>
                                                <th class="">Party Name
                                                </th>
                                                <th class="">Led Post
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody id="inventoryRows" class="saleRows">
                                        </tbody>
                                    </table>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>