<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);

	$vouchers = $desc['vouchers'];
?>
<script id="prow-template" type="text/x-handlebars-template">
<tr>
	<td>{{PARTY_ID}}</td>
	<td>
		<div class="row-fluid">
			<div class="span12">
				<strong>Name: </strong>
				<span class="party-name-filter">{{NAME}}</span>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span12">
				<strong>Address: </strong>
				<span class="party-address-filter">{{ADDRESS}}</span>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span12">
				<strong>Join Date: </strong>
				<span>{{PDATE}}</span>
			</div>
		</div>
	</td>
	<td>
		<div class="row-fluid">
			<div class="span12"><strong>Phone Office: </strong><span class="">{{PH_OFF}}</span></div>
		</div>
		<div class="row-fluid">
			<div class="span12"><strong>Phone Residence: </strong><span class="">{{PH_RES}}</span></div>
		</div>
		<div class="row-fluid">
			<div class="span12"><strong>Mobile: </strong><span class="">{{MOBILE}}</span></div>
		</div>
		<div class="row-fluid">
			<div class="span12"><strong>Fax: </strong><span class="">{{FAX}}</span></div>
		</div>
		<div class="row-fluid">
			<div class="span12"><strong>Email: </strong><span class="">{{EMAIL}}</span></div>
		</div>
	</td>
	<td>
		<div class="row-fluid">
			<div class="span12"><strong>Level 1: </strong><span class="level1-name-filter">{{L1NAME}}</span></div>
		</div>
		<div class="row-fluid">
			<div class="span12"><strong>Level 2: </strong><span  class="level2-name-filter">{{L2NAME}}</span></div>
		</div>
		<div class="row-fluid">
			<div class="span12"><strong>Level 3: </strong><span  class="level3-name-filter">{{L3NAME}}</span></div>
		</div>
	</td>
	<td>
		<a class="btn btn-primary editTblItem"><i class="icon icon-edit"></i><input type='hidden' class='hfParty_id' name='party_id' value={{PARTY_ID}} /></a>
	</td>
</tr>
</script>
<!-- main content -->
<div id="main_wrapper">
<div id="AccountAddModel" class="modal hide fade"  role="dialog" aria-labelledby="AccountAddModelLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header" style="background:#2477a4;color:white;padding-bottom:20px;">
                    <button type="button" class="modal-button cellRight modal-close pull-right btn-close" data-dismiss="modal"><span class="fa fa-times" style="font-size:26px; "></span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="AccountAddModelLabel">Add New Account</h4>
                </div>
               <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-lg-9 col-lg-offset-1">
                                <form role="form">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label for="exampleInputEmail1">Name</label>
                                                <input type="text" id="txtAccountName" class="form-control" placeholder="Account Name" maxlength="50" tabindex="101">
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Acc Type3</label>
                                                <select class="form-control select2"  id="txtLevel3" tabindex="102">
                                                    <option value="" disabled="" selected="">Choose Account Type</option>
                                                    <?php foreach ($l3s as $l3): ?>
                                                        <option value="<?php echo $l3['l3']; ?>" data-level2="<?php echo $l3['level2_name']; ?>" data-level1="<?php echo $l3['level1_name']; ?>"><?php echo $l3['level3_name'] ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-12">
                                                <span><b>Type 2 &rarr; </b><span id="txtselectedLevel2"> </span></span> <span><b>Type 1 &rarr; </b><span id="txtselectedLevel1"> </span></span>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>       
                </div>
                <div class="modal-footer">
                    <div class="pull-right">
                        <a class="btn btn-success btnSaveM btn-sm" tabindex="103" data-insertbtn="1"><i class="fa fa-save"></i> Save</a>
                        <a class="btn btn-warning btnResetM btn-sm" tabindex="104"><i class="fa fa-refresh"></i> Reset</a>
                        <a class="btn btn-danger btn-sm" data-dismiss="modal" tabindex="105" ><i class="fa fa-times"></i> Close</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="page_bar">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page_title">Cash Payment/Reciept</h1>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="row">
				<div class="col-md-12">

					<div class="row">
						<div class="col-lg-12">

							<ul class="nav nav-pills">
					            <li class="active"><a href="#addupdateCash" data-toggle="tab">Add/Update Cash Vouchern</a></li>
					            <li><a href="#searchcash" data-toggle="tab">Search Cash Voucher</a></li>
					             <li><a href="#Attachments" data-toggle="tab">Attachments</a></li>
				          	</ul>

				          	<div class="tab-content">
								<div class="tab-pane active" id="addupdateCash">
									<div class="panel panel-default">
										<div class="panel-body">

											<form action="">

												<div class="row">
													<div class="col-lg-2">
														<div class="input-group">
															<span class="input-group-addon id-addon">Sr#</span>
															<input type="number" id="txtId" class="form-control num txtidupdate" data-txtidupdate=<?php echo $vouchers['cash_payment_receipt']['update']; ?>/>
															<input type="hidden" id="voucher_type_hidden"/>
															<input type="hidden" id="vrnoa_all_hidden"/>
															<input type="hidden" id="txtMaxIdHidden"/>
															<input type="hidden" id="txtIdHidden"/>
											
															<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
															<input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                                                            <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                                                            <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">

														</div>
													</div>

													<div class="col-lg-1"></div>

													<div class="col-lg-3">
					                                    <div class="input-group">
					                                        <span class="input-group-addon txt-addon">Date</span>
					                                        <input class="form-control ts_datepicker" type="text" id="cur_date">
					                                        <input class="form-control ts_datepicker" type="hidden" id="vrdate">
					                                    </div>
					                                </div>

					                            </div>

					                            <div class="row">
	                                                <div class="col-lg-2">
                                                        <label for="cpv" class="radio cpvRadio">
                                                            <input type="radio" id="cpv" name="vrEtype" value="cpv" checked="checked">
                                                            Cash Payment
                                                        </label>
	                                                </div>

	                                                <div class="col-lg-2">
                                                        <label for="crv" class="radio crvRadio">
                                                            <input type="radio" id="crv" name="vrEtype" value="crv">
                                                            Cash Receipt
                                                        </label>
	                                                </div>
					                            </div>

												<div class="container-wrap">
													<div class="row">
							                            <div class="col-lg-1" style="width:140px;">
						                                  	<label>Id</label>
						                                  	<select class="form-control" id="pid_dropdown" tabindex="1">
						                                  		
						                                      	<option value="" disabled="" selected="">Choose</option>
						                                      	<?php foreach ($accounts as $account): ?>
						                                          	<option value="<?php echo $account['pid']; ?>"><?php echo $account['pid']; ?></option>
						                                      	<?php endforeach ?>
						                                  	</select>
							                            </div>
							                            <div class="col-lg-4" >
						                                  	<label id="balance_lbl">Account</label>
						                                  	<div class="input-group" >
						                                  	<select class="form-control" id="name_dropdown" tabindex="2">
						                                  		<option value="" disabled="" selected="">Choose Account</option>
						                                      	<?php foreach ($accounts as $account): ?>
						                                          	<option value="<?php echo $account['pid']; ?>"><?php echo $account['name']; ?></option>
						                                      	<?php endforeach ?>
						                                  	</select>
						                                  	<a  tabindex="-1" class="input-group-addon btn btn-primary active" style="min-width:40px !important;" id="A2" data-target="#AccountAddModel" data-toggle="modal" href="#addCategory" rel="tooltip"
			                                                data-placement="top" data-original-title="Add Category" data-toggle="tooltip" data-placement="bottom" title="Add New Account Quick (F3)"><i class="fa fa-plus"></i></a>
			                                                </div>
							                            </div>

							                            <div class="col-lg-3">
					                                      	<label>Remarks</label>
					                                        <input type="text" id="txtRemarks" class="form-control" placeholder="Remarks" tabindex="3"/>
						                                </div>

						                                <div class="col-lg-1">
					                                      	<label>Inv#</label>				                            
				                                        	<input type="text" id="txtInvNo" placeholder='Inv#' class="form-control" tabindex="4"/>						                                    
						                                </div>

						                                <div class="col-lg-1" style="width:140px;">
					                                      	<label>Amount</label>
					                                        <input type="text" id="txtAmount" placeholder='Amount' class="form-control num" tabindex="5" />						                                   
						                                </div>
							                            
								                        <!-- <div class="col-lg-1">								                            
								                            <a href="" class="btn btn-primary" id="btnAddCash">+</a>															
								                        </div> -->
								                        <div class="col-lg-1" style="margin-top:30px;">
								                        	<!-- <label>Add</label> -->
															<!-- <a class="btn btn-primary" id="btnAddCash" tabindex="6"></span></a> -->
															<a href="" class="btn btn-primary" id="btnAddCash"><i class="fa fa-plus"></i></a>
															
														</div>
							                    	</div>
												</div>
												<div class="row"></div>


												<div class="row">
						                            <div class="col-lg-12">
						                                <table class="table table-striped" id="cash_table">
						                                    <thead>
						                                        <tr>
						                                            <th>AccId</th>
						                                            <th>Account Name</th>
						                                            <th>Remarks</th>
						                                            <th>Inv#</th>
						                                            <th>Amount</th>
						                                            <th class='text-center'>Actions</th>
						                                        </tr>
						                                    </thead>
						                                    <tbody>

						                                    </tbody>
						                                </table>
						                            </div>
						                        </div>
												<div id="party-lookup" class="modal fade modal-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
													<div class="modal-dialog modal-lg">
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
																<h3 id="myModalLabel"><i class="fa fa-search"></i> Party Lookup</h3>
															</div>

																<div class="modal-body mrgtop">
																<table class="table table-striped modal-table">
																<!-- <table class="table table-bordered table-striped modal-table"> -->
																<thead>
																<tr style="font-size:16px;">
																<th>Id</th>
																<th>Name</th>
																<th>Mobile</th>
																<th>Address</th>
																<th>Actions</th>
																</tr>
																</thead>
																<tbody>
																<?php foreach ($accounts as $party): ?>
																<tr>
																<td width="13%;">
																<?php echo $party['account_id']; ?>
																<input type="hidden" name="hfModalPartyId" value="<?php echo $party['pid']; ?>">
																</td>
																<td><?php echo $party['name']; ?></td>
																<td><?php echo $party['mobile']; ?></td>
																<td><?php echo $party['address']; ?></td>
																<td><a href="#" data-dismiss="modal" class="btn btn-primary populateAccount"><i class="glyphicon glyphicon-ok"></i></a></td>
																</tr>
																<?php endforeach ?>
																</tbody>
																</table>
																</div>
																<div class="modal-footer">
																<!-- <button class="btn btn-danger delete-modal-del">Delete</button> -->
																<button class="btn btn-primary pull-right" data-dismiss="modal">Cancel</button>
															</div>
														</div>
													</div>
												</div>
						                        <div class="row">
						                        <div class="col-lg-9"></div>

						                        	<div class="col-lg-9">
						                        	<div class="col-lg-3" style="width:30%">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">User: </span>
                                                            <select class="form-control " disabled="" id="user_dropdown" tabindex="-1">
                                                                <option value="" disabled="" selected="">...</option>
                                                                <?php foreach ($userone as $user): ?>
                                                                    <option value="<?php echo $user['uid']; ?>"><?php echo $user['uname']; ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                		                            <div class="col-lg-3" style="width:30%" >
                	                                  	<select class="form-control select2" id="cash_dropdown" tabindex="6">
                	                                  		<option value="" disabled="" selected="">Choose Cash Account</option>
                	                                      	<?php foreach ($accountCashs as $accountCash): ?>
                	                                          	<option value="<?php echo $accountCash['pid']; ?>"><?php echo $accountCash['name']; ?></option>
                	                                      	<?php endforeach ?>
                	                                  	</select>
                		                            </div>
						                        	<div class="col-lg-3">
						                        		<div class="input-group">
				                                        	<span class="input-group-addon">Net Amount</span>
				                                        	<input type="text" id="txtNetAmount" class="form-control" readonly="true" tabindex="-1"/>
					                                    </div>
						                        	</div>
						                        	</div>
						                        </div>

												<div class="row">
													<div class="col-lg-12">
														<div class="pull-right">
															<!-- <a href=''  class="btn btn-default btnSave" data-insertbtn='<?php echo $vouchers['cash_payment_receipt']['insert']; ?>'><i class="fa fa-save"></i> Save F10</a> -->
															<a href='' tabindex="10" class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
															<a href='' tabindex="7" class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-insertbtn='<?php echo $vouchers['cash_payment_receipt']['insert']; ?>' data-updatebtn='<?php echo $vouchers['cash_payment_receipt']['update']; ?>' data-deletebtn='<?php echo $vouchers['cash_payment_receipt']['delete']; ?>' data-printbtn='<?php echo $vouchers['cash_payment_receipt']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
															<a href='' tabindex="8" class="btn btn-default btnDelete" data-deletetbtn='<?php echo $vouchers['cash_payment_receipt']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete F12</a>
															<!-- <a href='' tabindex="9" class="btn btn-default btnPrint" data-printbtn='<?php echo $vouchers['cash_payment_receipt']['print']; ?>'><i class="fa fa-print"></i> Print F9</a> -->
															<div class="btn-group">
				                                                  <button type="button" class="btn btn-primary btn-sm btnPrint" ><i class="fa fa-save"></i>Print F9</button>
				                                                  <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
				                                                    <span class="caret"></span>
				                                                    <span class="sr-only">Toggle Dropdown</span>
				                                                  </button>
				                                                  <ul class="dropdown-menu" role="menu">
				                                                    <li ><a href="#" class="envelopePrint btn"> Envelope Print</a></li>
				                                                  </ul>
				                                            </div>
															<a href="#party-lookup" tabindex="11" data-toggle="modal" class="btn btn-default fat-btn btnsearchparty "><i class="fa fa-search"></i>&nbsp;Account Lookup F1</a>
														</div>
													</div> 	<!-- end of col -->
												</div>	<!-- end of row -->


											</form>	<!-- end of form -->

										</div>	<!-- end of panel-body -->
									</div>	<!-- end of panel -->
								</div>

								<div class="tab-pane" id="searchcash">
									<div class="panel panel-default">
										<div class="panel-body">
											
											<div class="row">
												<div class="col-lg-3">
				                                    <div class="input-group">
				                                        <span class="input-group-addon">From</span>
				                                        <input class="form-control ts_datepicker" type="text" id="from_date">
				                                    </div>
				                                </div>
				                                <div class="col-lg-3">
				                                    <div class="input-group">
				                                        <span class="input-group-addon">To</span>
				                                        <input class="form-control ts_datepicker" type="text" id="to_date">
				                                    </div>
				                                </div>

				                                <div class="col-lg-2">
				                                	<a href='' class="btn btn-default btnSearch"><i class="fa fa-search"></i> Search</a>
				                                </div>
											</div>

											<div class="row">
	                                            <div class="col-lg-2">
	                                                <label for="scpv" class="radio cpvRadio">
	                                                    <input type="radio" id="scpv" name="vrEtype" value="scpv" checked="checked">
	                                                    Cash Payment
	                                                </label>
	                                            </div>

	                                            <div class="col-lg-2">
	                                                <label for="scrv" class="radio crvRadio">
	                                                    <input type="radio" id="scrv" name="vrEtype" value="scrv">
	                                                    Cash Receipt
	                                                </label>
	                                            </div>
				                            </div>

				                            <div class="row">
					                            <div class="col-lg-12">
					                                <table class="table table-striped" id="search_cash_table">
					                                    <thead>
					                                        <tr>
					                                        	<th>Vr#</th>
					                                            <th>VrDate</th>
					                                            <th>Party Name</th>
					                                            <th>Amount</th>
					                                            <th>Remarks</th>
					                                            <th class='text-center'>Actions</th>
					                                        </tr>
					                                    </thead>
					                                    <tbody>
					                                    </tbody>
					                                </table>
					                            </div>
					                        </div>

										</div>	<!-- end of panel-body -->
									</div>	<!-- end of panel -->
								</div>
								 <div class="tab-pane fade in Attachmentspanel" id="Attachments">
                                <div class="row">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <input type="number" class="form-control" id="txtIdImg" style="background: #fff;" readonly />
                                        </div>
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-primary" id="viewImages">View Attachments <i class="fa fa-eye"></i></button>
                                            <button type="button" class="btn btn-success" id="uploadFiles" data-insert='<?php echo $vouchers['uploadFiles']['insert']; ?>' data-update='<?php echo $vouchers['uploadFiles']['update']; ?>' data-delete='<?php echo $vouchers['uploadFiles']['delete']; ?>' >Update Files <i class="fa fa-upload"></i></button>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                         <form enctype="multipart/form-data" class="dropzone" id="image-upload" method="POST">
                                                <div>
                                                    <h3>Attach Files to Upload</h3>
                                                    <div class="fallback">
                                                        '<input name="file" type="file" />
                                                    </div>
                                                </div>
                                            </form>
                                            <small style="color: #555;"> Supported Files: (.bmp, .gif, .jpg, .jpeg, .png, .pdf, .doc, .docx, .txt, .xls, .xlsx, .rtf)</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            </div>
				          	</div>

						</div>  <!-- end of col -->
					</div>	<!-- end of row -->

				</div>	<!-- end of level 1-->
			</div>
		</div>
	</div>
</div>