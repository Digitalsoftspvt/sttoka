<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);

	$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">

	<div class="page_bar">
		<div class="row">
			<div class="col-lg-3">
				<h1 class="page_title">Advance Voucher</h1>
			</div>
			<div class="col-lg-9">
				<div class="pull-right">
					<a href='' class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset</a>
					<a href='' class="btn btn-default btnSave" data-insertbtn='<?php echo $vouchers['advance']['insert']; ?>' data-updatebtn='<?php echo $vouchers['advance']['update']; ?>' data-deletebtn='<?php echo $vouchers['advance']['delete']; ?>' data-printbtn='<?php echo $vouchers['advance']['print']; ?>'><i class="fa fa-save"></i> Save</a>
					<a href='' class="btn btn-default btnDelete" data-deletetbtn='<?php echo $vouchers['advance']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete</a>
					<a class="btn btn-default btnPrint"><i class="fa fa-print"></i> Print</a>
					
				</div>
			</div>
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="row">
				<div class="col-md-12">

					<div class="row">
						<div class="col-lg-12">

							<ul class="nav nav-pills">
					            <li class="active"><a href="#addupdateAdvance" data-toggle="tab">Add/Update Advance</a></li>
					            <li><a href="#searchcash" data-toggle="tab">Search Advance</a></li>
				          	</ul>

				          	<div class="tab-content">
								<div class="tab-pane active" id="addupdateAdvance">
									<div class="panel panel-default">
										<div class="panel-body">

											<form action="">

												<div class="row">
													<div class="col-lg-2">
														<div class="input-group">
															<span class="input-group-addon id-addon">Vr#</span>
															<input type="number" id="txtId" class="form-control num txtidupdate" data-txtidupdate='<?php echo $vouchers['advance']['update']; ?>'>
															<input type="hidden" id="txtMaxIdHidden"/>
															<input type="hidden" id="txtIdHidden"/>
															<input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
															<input type="hidden" id="voucher_type_hidden">
															<input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                                                            <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
														</div>
													</div>

													<div class="col-lg-1"></div>

													<div class="col-lg-3">
					                                    <div class="input-group">
					                                        <span class="input-group-addon txt-addon">Date</span>
					                                        <input class="form-control " type="date" id="cur_date" value="<?php echo date('Y-m-d'); ?>">
					                                    </div>
					                                </div>

					                            </div>
												
												<div class="row">
													<div class="container-wrap">
														<div class="row">
								                            <div class="col-lg-2" >
							                                  	<select class="form-control select2" id="pid_dropdown">
							                                      	<option value="" disabled="" selected="">Choose Id</option>
							                                      	<?php foreach ($accounts1 as $account): ?>
							                                          	<option value="<?php echo $account['pid']; ?>"><?php echo $account['pid']; ?></option>
							                                      	<?php endforeach ?>
							                                  	</select>
								                            </div>
								                            <div class="col-lg-3" style=''>
							                                  	<select class="form-control select2" id="name_dropdown">
							                                  		<option value="" disabled="" selected="">Choose Account</option>
							                                      	<?php foreach ($accounts1 as $account): ?>
							                                          	<option value="<?php echo $account['pid']; ?>"><?php echo $account['name']; ?></option>
							                                      	<?php endforeach ?>
							                                  	</select>
								                            </div>

								                            <div class="col-lg-4" style=''>
						                                      	
						                                        <input type="text" id="txtRemarks" class="form-control" placeholder="Remarks"/>
							                                </div>

							                                <div class="col-lg-3">
						                                      	
						                                        <input type="text" id="txtInvNo" placeholder='Inv#' class="form-control"/>
							                                    
							                                </div>
							                            </div>
							                            <div class="row">
							                                <div class="col-lg-3">
						                                      	<div class="input-group">
						                                        	<span class="input-group-addon amnt-addon"><span class="fa fa-money"></span></span>
						                                        	<input type="text" id="txtAmount" placeholder='Amount' class="form-control num" maxlength="5" />
							                                    </div>
							                                </div>
								                            
									                        <div class="col-lg-1">
									                            <div class="input-group">
									                                <a href="" class="btn btn-primary" id="btnAddCash"><i class="fa fa-plus"></i></a>
																</div>
									                        </div>
								                    	</div>
													</div>
												</div>
												<div class="row"></div>


												<div class="row">
						                            <div class="col-lg-12">
						                                <table class="table table-striped report-table" id="advance_table">
						                                    <thead>
						                                        <tr>
						                                            <th>AccId</th>
						                                            <th>Account Name</th>
						                                            <th>Remarks</th>
						                                            <th>Inv#</th>
						                                            <th>Amount</th>
						                                            <th class='text-center'>Actions</th>
						                                        </tr>
						                                    </thead>
						                                    <tbody>

						                                    </tbody>
						                                </table>
						                            </div>
						                        </div>

						                        <div class="row">
						                        	<div class="col-lg-9"></div>
						                        	<div class="col-lg-3">
						                        		<div class="input-group">
				                                        	<span class="input-group-addon">Net Amount</span>
				                                        	<input type="text" id="txtNetAmount" class="form-control" readonly/>
					                                    </div>
						                        	</div>
						                        </div>

												<div class="row">
                                                    <div class="col-lg-4" style='width: 24%;'>
                                                        <select class="form-control select2" id="name_dropdown2">
                                                            <option value="" disabled="" selected="">Choose Account</option>
                                                            <?php foreach ($accounts as $account){
                                                                $selected = $payrollSetting;
                                                                ?>
                                                                <option value="<?php echo $account['pid']; ?>" <?php echo (($selected == $account['pid'])?'selected':''); ?>><?php echo $account['name']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
													<div class="col-lg-9">
														<div class="pull-right">
															<a href='' class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset</a>
															<a href='' class="btn btn-default btnSave" data-insertbtn='<?php echo $vouchers['advance']['insert']; ?>' data-updatebtn='<?php echo $vouchers['advance']['update']; ?>' data-deletebtn='<?php echo $vouchers['advance']['delete']; ?>' data-printbtn='<?php echo $vouchers['advance']['print']; ?>'><i class="fa fa-save"></i> Save</a>
															<a href='' class="btn btn-default btnDelete" data-deletetbtn='<?php echo $vouchers['advance']['delete']; ?>'><i class="fa fa-trash-o"></i> Delete</a>
															<a class="btn btn-default btnPrint"><i class="fa fa-print"></i> Print</a>
															
														</div>
													</div> 	<!-- end of col -->
												</div>	<!-- end of row -->


											</form>	<!-- end of form -->

										</div>	<!-- end of panel-body -->
									</div>	<!-- end of panel -->
								</div>

								<div class="tab-pane" id="searchcash">
									<div class="panel panel-default">
										<div class="panel-body">
											
											<div class="row">
												<div class="col-lg-3">
				                                    <div class="input-group">
				                                        <span class="input-group-addon">From</span>
				                                        <input class="form-control " type="date" id="from_date" value="<?php echo date('Y-m-d'); ?>">
				                                    </div>
				                                </div>
				                                <div class="col-lg-3">
				                                    <div class="input-group">
				                                        <span class="input-group-addon">To</span>
				                                        <input class="form-control " type="date" id="to_date" value="<?php echo date('Y-m-d'); ?>">
				                                    </div>
				                                </div>

				                                <div class="col-lg-2">
				                                	<a href='' class="btn btn-default btnSearch"><i class="fa fa-search"></i> Search</a>
				                                </div>
											</div>

				                            <div class="row">
					                            <div class="col-lg-12">
					                                <table class="table table-striped" id="search_advance_table">
					                                    <thead>
					                                        <tr>
					                                        	<th>Vr#</th>
					                                            <th>VrDate</th>
					                                            <th>Party Name</th>
					                                            <th>Amount</th>
					                                            <th>Remarks</th>
					                                            <th class='text-center'>Actions</th>
					                                        </tr>
					                                    </thead>
					                                    <tbody>
					                                    </tbody>
					                                </table>
					                            </div>
					                        </div>

										</div>	<!-- end of panel-body -->
									</div>	<!-- end of panel -->
								</div>
				          	</div>

						</div>  <!-- end of col -->
					</div>	<!-- end of row -->

				</div>	<!-- end of level 1-->
			</div>
		</div>
	</div>
</div>