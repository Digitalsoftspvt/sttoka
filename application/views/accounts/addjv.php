<?php

	$desc = $this->session->userdata('desc');
	$desc = json_decode($desc);
	$desc = objectToArray($desc);

	$vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">
	<div id="AccountAddModel" class="modal hide fade"  role="dialog" aria-labelledby="AccountAddModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header" style="background:#2477a4;color:white;padding-bottom:20px;">
                    <button type="button" class="modal-button cellRight modal-close pull-right btn-close" data-dismiss="modal"><span class="fa fa-times" style="font-size:26px; "></span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="AccountAddModelLabel">Add New Account</h4>
                </div>
               <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="col-lg-9 col-lg-offset-1">
                                <form role="form">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label for="exampleInputEmail1">Name</label>
                                                <input type="text" id="txtAccountName" class="form-control" placeholder="Account Name" maxlength="50" tabindex="101">
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Acc Type3</label>
                                                <select class="form-control select2"  id="txtLevel3" tabindex="102">
                                                    <option value="" disabled="" selected="">Choose Account Type</option>
                                                    <?php foreach ($l3s as $l3): ?>
                                                        <option value="<?php echo $l3['l3']; ?>" data-level2="<?php echo $l3['level2_name']; ?>" data-level1="<?php echo $l3['level1_name']; ?>"><?php echo $l3['level3_name'] ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-12">
                                                <span><b>Type 2 &rarr; </b><span id="txtselectedLevel2"> </span></span> <span><b>Type 1 &rarr; </b><span id="txtselectedLevel1"> </span></span>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>       
                </div>
                <div class="modal-footer">
                    <div class="pull-right">
                        <a class="btn btn-success btnSaveM btn-sm" tabindex="103" data-insertbtn="1"><i class="fa fa-save"></i> Save</a>
                        <a class="btn btn-warning btnResetM btn-sm" tabindex="104"><i class="fa fa-refresh"></i> Reset</a>
                        <a class="btn btn-danger btn-sm" data-dismiss="modal" tabindex="105" ><i class="fa fa-times"></i> Close</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<div class="page_bar">
		<div class="row">
			<div class="col-md-4">
				<h1 class="page_title">Journal Voucher</h1>
			</div>
			<!-- <div class="col-md-18">
				<div class="pull-right">
					<a href='' class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
					<a href='' class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php //echo $vouchers['account']['insert']; ?>' data-insertbtn='<?php //echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php //echo $vouchers['jvvoucher']['insert']; ?>' data-updatebtn='<?php //echo $vouchers['jvvoucher']['update']; ?>' data-deletebtn='<?php //echo $vouchers['jvvoucher']['delete']; ?>' data-printbtn='<?php //echo $vouchers['jvvoucher']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
					<a href='' class="btn btn-default btnDelete" ><i class="fa fa-trash-o"></i> Delete F12</a>
					<a href='' class="btn btn-default btnPrint" ><i class="fa fa-print"></i> Print F9</a>
					
					<a href='' class="btn btn-default btnPartySearch"><i class="fa fa-search"></i> Account Search F1</a>
					<a href="#party-lookup" data-toggle="modal" class="btn btn-default fat-btn"><i class="fa fa-search"></i>&nbsp;Account Lookup F1</a>
				</div>
			</div> 	end of col -->
		</div>
	</div>

	<div class="page_content">
		<div class="container-fluid">

			<div class="row">
				<div class="col-md-12">

					<div class="row">
						<div class="col-lg-12">

							<ul class="nav nav-pills">
					            <li class="active"><a href="#addupdateJV" data-toggle="tab">Add/Update JV</a></li>
					            <li><a href="#searchcash" data-toggle="tab">Search JV</a></li>
				          	</ul>

				          	<div class="tab-content">
								<div class="tab-pane active" id="addupdateJV">
									<div class="panel panel-default">
										<div class="panel-body">

											<form action="">

												<div class="row">
													<div class="col-lg-2">
														<div class="input-group">
															<span class="input-group-addon id-addon">Sr#</span>
															<input type="number" id="txtId" class="form-control  num txtidupdate" data-txtidupdate=<?php //echo $vouchers['jvvoucher']['update']; ?>/>
															<input type="hidden" id="txtMaxIdHidden"/>
															<input type="hidden" id="txtIdHidden"/>
															<input type="hidden" id="voucher_type_hidden"/>

															<input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                                                            <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                                                            <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">

														</div>
													</div>

													<div class="col-lg-1"></div>

													<div class="col-lg-3">
					                                    <div class="input-group">
					                                        <span class="input-group-addon txt-addon">Date</span>
					                                        <input class="form-control  ts_datepicker" type="text" id="cur_date">
					                                        <input class="form-control  ts_datepicker" type="hidden" id="vrdate">
					                                    </div>
					                                </div>

					                            </div>

												<div class="row"></div>
												<div class="container-wrap">
													<div class="row">
							                            <div class="col-lg-1" style="width:120px;">
							                            	<label>Code</label>
						                                  	<select class="form-control " id="pid_dropdown">
						                                      	<option value="" disabled="" selected="">Choose</option>
						                                      	<?php foreach ($accounts as $account): ?>
						                                          	<option value="<?php echo $account['pid']; ?>"><?php echo $account['pid']; ?></option>
						                                      	<?php endforeach ?>
						                                  	</select>
							                            </div>
							                            <div class="col-lg-3" >
							                            	<label id="balance_lbl">Account</label>
						                                  	<div class="input-group" >
						                                  	<select class="form-control " id="name_dropdown">
						                                  		<option value="" disabled="" selected="">Choose Account</option>
						                                      	<?php foreach ($accounts as $account): ?>
						                                          	<option value="<?php echo $account['pid']; ?>"><?php echo $account['name']; ?></option>
						                                      	<?php endforeach ?>
						                                  	</select>
						                                  	<a  tabindex="-1" class="input-group-addon btn btn-primary active" style="min-width:40px !important;" id="A2" data-target="#AccountAddModel" data-toggle="modal" href="#addCategory" rel="tooltip"
			                                                data-placement="top" data-original-title="Add Category" data-toggle="tooltip" data-placement="bottom" title="Add New Account Quick (F3)"><i class="fa fa-plus"></i></a>
			                                                </div>
							                            </div>

							                            <div class="col-lg-3" >
					                                      	<label>Remarks</label>
					                                        <input type="text" id="txtRemarks" class="form-control " placeholder="Remarks"/>
						                                </div>

						                                <div class="col-lg-1" >
					                                      	<label>Inv/Chq#</label>
					                                        <input type="text" id="txtInvNo" placeholder='Inv#' class="form-control "/>
						                                </div>

						                                <div class="col-lg-1" style="width:130px;">
					                                      	<label>Debit</label>
					                                        <input type="text" id="txtDebit" placeholder='Debit' class="form-control  num"/>
						                                </div>

						                                <div class="col-lg-1" style="width:130px;">
					                                      	<label>Credit</label>
					                                        <input type="text" id="txtCredit" placeholder='Credit' class="form-control  num"/>
						                                </div>
							                            
								                        <div class="col-lg-1" style="margin-top:30px;">
								                            <!-- <label>Add</label> -->
								                            <a href="" class="btn btn-sm btn-primary" id="btnAddCash"><i class="fa fa-plus"></i></a>
								                        </div>
							                    	</div>
												</div>
												<div class="row"></div>


												<div class="row">
						                            <div class="col-lg-12">
						                                <table class="table table-striped" id="cash_table">
						                                    <thead>
						                                        <tr>
						                                            <th>Acc Code</th>
						                                            <th>Account Name</th>
						                                            <th>Remarks</th>
						                                            <th>Inv#</th>
						                                            <th>Debit</th>
						                                            <th>Debit</th>
						                                            <th class='text-center'>Actions</th>
						                                        </tr>
						                                    </thead>
						                                    <tbody>

						                                    </tbody>
						                                </table>
						                            </div>
						                        </div>
												<div id="party-lookup" class="modal fade modal-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
													<div class="modal-dialog modal-lg">
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
																<h3 id="myModalLabel"><i class='fa fa-search'></i> Party Lookup</h3>
															</div>

																<div class="modal-body mrgtop">
																<table class="table table-striped modal-table">
																<!-- <table class="table table-bordered table-striped modal-table"> -->
																<thead>
																<tr style="font-size:16px;">
																<th>Id</th>
																<th>Name</th>
																<th>Mobile</th>
																<th>Address</th>
																<th>Actions</th>
																</tr>
																</thead>
																<tbody>
																<?php foreach ($accounts as $party): ?>
																<tr>
																<td width="13%;">
																<?php echo $party['account_id']; ?>
																<input type="hidden" name="hfModalPartyId" value="<?php echo $party['pid']; ?>">
																</td>
																<td><?php echo $party['name']; ?></td>
																<td><?php echo $party['mobile']; ?></td>
																<td><?php echo $party['address']; ?></td>
																<td><a href="#" data-dismiss="modal" class="btn btn-primary populateAccount"><i class="glyphicon glyphicon-ok"></i></a></td>
																</tr>
																<?php endforeach ?>
																</tbody>
																</table>
																</div>
																<div class="modal-footer">
																<!-- <button class="btn btn-danger delete-modal-del">Delete</button> -->
																<button class="btn btn-primary pull-right" data-dismiss="modal">Cancel</button>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-lg-9"></div>
													<div class="col-lg-9">
														<div class="col-lg-3" style="width:30%">
															<div class="input-group">
																<span class="input-group-addon">User: </span>
																<select class="form-control " disabled="" id="user_dropdown">
																	<option value="" disabled="" selected="">...</option>
																	<?php foreach ($userone as $user): ?>
																	<option value="<?php echo $user['uid']; ?>"><?php echo $user['uname']; ?></option>
																	<?php endforeach; ?>
																</select>
															</div>
														</div>

														<div class="col-lg-3">
															<div class="input-group">
																<span class="input-group-addon">Total Debit</span>
																<input type="text" id="txtNetDebit" class="form-control " readonly/>
															</div>
														</div>

														<div class="col-lg-3">
															<div class="input-group">
																<span class="input-group-addon">Total Credit</span>
																<input type="text" id="txtNetCredit" class="form-control " readonly/>
															</div>
														</div>
													</div>
												</div>

												<div class="row">
													<div class="col-lg-12">
														<div class="pull-right">
															<a href='' class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
															<a href='' class="btn btn-sm btn-default btnSave" data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['jvvoucher']['insert']; ?>' data-updatebtn='<?php echo $vouchers['jvvoucher']['update']; ?>' data-deletebtn='<?php echo $vouchers['jvvoucher']['delete']; ?>' data-printbtn='<?php echo $vouchers['jvvoucher']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
															<a href='' class="btn btn-default btnDelete" ><i class="fa fa-trash-o"></i> Delete F12</a>
															<a href='' class="btn btn-default btnPrint" ><i class="fa fa-print"></i> Print F9</a>
															
															<!-- <a href='' class="btn btn-default btnPartySearch"><i class="fa fa-search"></i> Account Search F1</a> -->
															<a href="#party-lookup" data-toggle="modal" class="btn btn-default fat-btn"><i class="fa fa-search"></i>&nbsp;Account Lookup F1</a>
														</div>
													</div> 	<!-- end of col -->
												</div>	<!-- end of row -->


											</form>	<!-- end of form -->

										</div>	<!-- end of panel-body -->
									</div>	<!-- end of panel -->
								</div>

								<div class="tab-pane" id="searchcash">
									<div class="panel panel-default">
										<div class="panel-body">
											
											<div class="row">
												<div class="col-lg-3">
				                                    <div class="input-group">
				                                        <span class="input-group-addon">From</span>
				                                        <input class="form-control  ts_datepicker" type="text" id="from_date">
				                                    </div>
				                                </div>
				                                <div class="col-lg-3">
				                                    <div class="input-group">
				                                        <span class="input-group-addon">To</span>
				                                        <input class="form-control  ts_datepicker" type="text" id="to_date">
				                                    </div>
				                                </div>

				                                <div class="col-lg-2">
				                                	<a href='' class="btn btn-sm btn-default btnSearch"><i class="fa fa-search"></i> Search</a>
				                                </div>
											</div>

				                            <div class="row">
					                            <div class="col-lg-12">
					                                <table class="table table-striped" id="search_cash_table">
					                                    <thead>
					                                        <tr>
					                                        	<th>Vr#</th>
					                                            <th>VrDate</th>
					                                            <th>Party Name</th>
					                                            <th>Debit</th>
					                                            <th>Credit</th>
					                                            <th>Remarks</th>
					                                            <th class='text-center'>Actions</th>
					                                        </tr>
					                                    </thead>
					                                    <tbody>
					                                    </tbody>
					                                </table>
					                            </div>
					                        </div>

										</div>	<!-- end of panel-body -->
									</div>	<!-- end of panel -->
								</div>
				          	</div>

						</div>  <!-- end of col -->
					</div>	<!-- end of row -->

				</div>	<!-- end of level 1-->
			</div>
		</div>
	</div>
</div>