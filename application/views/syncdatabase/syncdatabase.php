<!-- main content -->
<div id="main_wrapper">

    <div class="page_bar">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page_title"><i class="ion-android-storage"></i> Sync Database</h1>
            </div>
        </div>
    </div>

    <div class="page_content">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="col-lg-12">
                    <a class="btn btn-sm btn-primary btnLocalToServer"><i class="ion-android-storage"></i> Local To Server F3</a>
                    <a class="btn btn-sm btn-primary btnServerToLocal"><i class="ion-android-storage"></i> Server To Local F4</a>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12" style="text-align:center;">
                    <img src="<?php echo base_url('assets/img/db-icon.png'); ?>" alt="">
                </div>

            </div>
        </div>
    </div>
</div>  