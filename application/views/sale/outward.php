<script id="voucher-item-template" type="text/x-handlebars-template">
  <tr>
     <td>{{{VRNOA}}}</td>
     <td>{{VRDATE}}</td>
     <td>{{PARTYNAME}}</td>
     <td>{{REMARKS}}</td>
     <td class="text-right">{{DOZEN}}</td>
     <td class="text-right">{{BAG}}</td>
     <td class="text-right">{{QTY}}</td>
     <td class="text-right">{{WEIGHT}}</td>
  </tr>
</script>
<script id="voucher-sum-template" type="text/x-handlebars-template">
  <tr class="finalsum">
     
     <td class="tblInvoice"></td>
     <td class="tblInvoice"></td>
     <td class="tblInvoice"></td>
     <td class="text-right txtbold">{{ TOTAL_HEAD }}</td>
     <td class="text-right txtbold" style="text-align:right !important;">{{ VOUCHER_DOZEN_SUM }}</td>
     <td class="text-right txtbold" style="text-align:right !important;">{{ VOUCHER_BAG_SUM }}</td>
     <td class="text-right txtbold" style="text-align:right !important;">{{ VOUCHER_QTY_SUM }}</td>
     <td class="text-right txtbold" style="text-align:right !important;">{{ VOUCHER_WEIGHT_SUM }}</td>
  </tr>
</script>
<?php

    $desc = $this->session->userdata('desc');
    $desc = json_decode($desc);
    $desc = objectToArray($desc);
    $vouchers = $desc['vouchers'];
?>

<!-- main content -->
<div id="main_wrapper">
  <div id="AccountAddModel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="AccountAddModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header" style="background:#2477a4;color:white;padding-bottom:20px;">
          <button type="button" class="modal-button cellRight modal-close pull-right btn-close" data-dismiss="modal"><span class="fa fa-times" style="font-size:26px; "></span><span class="sr-only">Close</span></button>
          <h4 class="modal-title" id="AccountAddModelLabel">Add New Account</h4>
        </div>
        <div class="modal-body">
          <div class="container-fluid">
            <div class="row-fluid">
              <div class="col-lg-9 col-lg-offset-1">
                <form role="form">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-lg-6">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" id="txtAccountName" class="form-control" placeholder="Account Name" maxlength="50" tabindex="1">
                      </div>
                      <div class="col-lg-6">
                        <label>Acc Type3</label>
                        <select class="form-control "  id="txtLevel3" tabindex="2">
                          <option value="" disabled="" selected="">Choose Account Type</option>
                          <?php foreach ($l3s as $l3): ?>
                          <option value="<?php echo $l3['l3']; ?>" data-level2="<?php echo $l3['level2_name']; ?>" data-level1="<?php echo $l3['level1_name']; ?>"><?php echo $l3['level3_name'] ?></option>
                          <?php endforeach ?>
                        </select>
                      </div>
                      <div class="col-lg-12">
                        <span><b>Type 2 &rarr; </b><span id="txtselectedLevel2"> </span></span> <span><b>Type 1 &rarr; </b><span id="txtselectedLevel1"> </span></span>
                      </div><!-- end of col -->
                    </div><!-- end of row -->
                  </div><!-- form-group -->
                </form><!-- end of form -->
              </div><!-- end of col -->
            </div><!--row -fluid -->
          </div><!-- container-fluid -->       
        </div><!-- model-body -->
        <div class="modal-footer">
          <div class="pull-right">
            <a class="btn btn-success btnSaveM btn-sm" data-insertbtn="1"><i class="fa fa-save"></i> Save</a>
            <a class="btn btn-warning btnResetM btn-sm"><i class="fa fa-refresh"></i> Reset</a>
            <a class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
          </div><!-- pull-right -->
        </div><!-- model-footer -->
      </div><!-- model-content -->
    </div><!-- model-dialgue -->
  </div><!-- end of model -->
  <div id="ItemAddModel" class="modal hide fade" role="dialog" aria-labelledby="ItemAddModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header" style="background:#2477a4;color:white;padding-bottom:20px;">
          <button type="button" class="modal-button cellRight modal-close pull-right btn-close" data-dismiss="modal"><span class="fa fa-times" style="font-size:26px; "></span><span class="sr-only">Close</span></button>
          <h4 class="modal-title" id="ItemAddModelLabel">Add New Item</h4>
        </div>
        <div class="modal-body">
          <div class="container-fluid">
            <div class="row-fluid">
              <div class="col-lg-9 col-lg-offset-1">
                <form role="form">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-lg-12">
                        <label for="exampleInputEmail1">Description</label>
                        <input type="text" id="txtItemName" class="form-control" placeholder="Account Name" maxlength="50" tabindex="1">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-6">
                        <label>Category</label>
                        <select class="form-control select2" id="category_dropdown" tabindex="2">
                          <option value="" disabled="" selected="">Choose Category</option>
                          <?php foreach ($categories as $category): ?>
                          <option value="<?php echo $category['catid']; ?>"><?php echo $category['name']; ?></option>
                          <?php endforeach ?>
                        </select>
                      </div>
                      <div class="col-lg-6">
                        <label>Sub Catgeory</label>
                        <select class="form-control select2" id="subcategory_dropdown" tabindex="3">
                          <option value="" disabled="" selected="">Choose sub category</option>
                          <?php foreach ($subcategories as $subcategory): ?>
                          <option value="<?php echo $subcategory['subcatid']; ?>"><?php echo $subcategory['name']; ?></option>
                          <?php endforeach ?>
                        </select>
                      </div>
                    </div>    
                    <div class="row">
                      <div class="col-lg-6">
                        <label>Brand</label>
                        <select class="form-control select2" id="brand_dropdown" tabindex="4">
                          <option value="" disabled="" selected="">Choose brand</option>
                          <?php foreach ($brands as $brand): ?>
                          <option value="<?php echo $brand['bid']; ?>"><?php echo $brand['name']; ?></option>
                          <?php endforeach ?>
                        </select>
                      </div>
                      <div class="col-lg-6">
                        <label>Type</label>
                        <input type="text" list='type' class="form-control" id="txtBarcode" tabindex="5" />
                        <datalist id='type'>
                          <?php foreach ($types as $type): ?>
                          <option value="<?php echo $type['barcode']; ?>"></option>
                          <?php endforeach ?>
                        </datalist>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-3">
                        <label>Sale Price</label>
                        <input class="form-control num" type="text" id="txtSalePrice" tabindex="6">
                      </div>
                      <div class="col-lg-3">
                        <label>Pur Price</label>
                        <input class="form-control num" type="text" id="txtPurPrice" tabindex="7">
                      </div>
                      <div class="col-lg-6">
                        <label>UOM</label>
                        <input type="text" class='form-control' tabindex="8" placeholder="Uom" id="uom_dropdown" list='uoms'>
                        <datalist id="uoms">
                          <?php foreach ($uoms as $uom): ?>
                          <?php if ($uom['uom'] !== ''): ?>
                          <option value="<?php echo $uom['uom']; ?>"></option>
                          <?php endif ?>
                          <?php endforeach ?>
                        </datalist>
                      </div><!-- end of col -->
                    </div><!-- end of row -->
                  </div><!-- form-group -->
                </form><!-- end of form -->
              </div><!-- end of col -->
            </div><!-- end of row -->
          </div><!-- container-fluid -->      
        </div><!-- end of model -->
        <div class="modal-footer">
          <div class="pull-right">
            <a class="btn btn-success btnSaveMItem btn-sm" data-insertbtn="1" tabindex="8"><i class="fa fa-save"></i> Save</a>
            <a class="btn btn-warning btnResetMItem btn-sm" tabindex="9"><i class="fa fa-refresh"></i> Reset</a>
            <a class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Close</a>
          </div><!-- pull-right -->
        </div><!-- model-footer -->
      </div><!-- model-content -->
    </div><!-- model-dialogue -->
  </div><!-- end of model -->
  <div class="page_bar">
    <div class="row">
      <div class="col-md-5">
        <h1 class="page_title">Outward Service Gate Pass</h1>
      </div>
      <div class="col-md-7">
        <div class="pull-right">
          <a class="btn btn-sm btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
          <a class="btn btn-sm btn-default btnSave" data-savegodownbtn='<?php echo $vouchers['warehouse']['insert']; ?>' data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['outwardservice']['insert']; ?>' data-updatebtn='<?php echo $vouchers['outwardservice']['update']; ?>' data-deletebtn='<?php echo $vouchers['outwardservice']['delete']; ?>' data-printbtn='<?php echo $vouchers['outwardservice']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
          <a class="btn btn-sm btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>  
          <div class="btn-group">
            <button type="button" class="btn btn-primary btn-sm btnPrint" ><i class="fa fa-save"></i>Print F9</button>
            <!-- <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              <span class="caret"></span>
              <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu" role="menu">
              <li ><a href="#" class="btnprintHeader"> Print with header</li>
              <li ><a href="#" class="btnprintwithOutHeader"> Print with Out header</li>
            </ul> -->
          </div>
        </div>
          <!--<a href="#party-lookup" data-toggle="modal" class="btn btn-sm btn-default btnsearchparty "><i class="fa fa-search"></i>&nbsp;Account Lookup F1</a>
          <a href="#item-lookup" data-toggle="modal" class="btn btn-sm btn-info btnsearchitem "><i class="fa fa-search"></i>&nbsp;Item Lookup F2</a>-->
      </div>
    </div>
  </div>
  <div class="page_content">
    <div class="container-fluid">
      <!--<div class="row ">
        <div class="col-lg-12">
          <ul class="nav nav-pills">
            <li class="active"><a href="#Main" data-toggle="tab">Main</a></li>
            <li><a href="#Search" data-toggle="tab">Search</a></li>
          </ul>
        </div>
      </div>-->
      <div class="row">
        <div class="col-md-12">
          <div class="tab-content">
            <div class="tab-pane active fade in" id="Main">  
              <div class="row">
                <div class="col-lg-12">
                  <div class="panel panel-default" style="  margin: -46px 0px 0px 0px;">
                    <div class="panel-body">
                      <form action="">
                        <div class="row">
                          <div class="col-lg-1" >
                            <!-- <div class="input-group"> -->
                            <span class="">GPO#</span>
                            <input type="number" class="form-control " id="txtVrnoa" >
                            <input type="hidden" id="txtMaxVrnoaHidden">
                            <input type="hidden" id="txtVrnoaHidden">
                            <input type="hidden" id="voucher_type_hidden">
                            <input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                            <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                            <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
                            <input type="hidden" id="purchaseid" value="<?php echo $setting_configur[0]['purchase']; ?>">
                            <input type="hidden" id="discountid" value="<?php echo $setting_configur[0]['discount']; ?>">
                            <input type="hidden" id="expenseid" value="<?php echo $setting_configur[0]['expenses']; ?>">
                            <input type="hidden" id="taxid" value="<?php echo $setting_configur[0]['tax']; ?>">
                            <input type="hidden" id="cashid" value="<?php echo $setting_configur[0]['cash']; ?>">
                            <input type="hidden" id="saleid" value="<?php echo $setting_configur[0]['sale']; ?>">
                            <input type="hidden" name="edit_qty" id="edit_qty" value="0">
                            <input type="hidden" name="edit_weight" id="edit_weight" value="0">
                            <!-- </div> -->
                          </div>
                          <div class="col-lg-2 col-lg-offset-2">
                            <!-- <div class="input-group"> -->
                            <label class="">Date</label>
                            <input class="form-control ts_datepicker" type="text" id="current_date">
                            <input class="form-control ts_datepicker" type="hidden" id="vrdate">
                            <!-- </div> -->
                          </div>
                          <div class="col-lg-1">
                            <!-- <div class="input-group"> -->
                            <span class="">Vr#</span>
                            <input type="text" class="form-control " id="txtVrno" readonly='true'>
                            <input type="hidden" id="txtMaxVrnoHidden">
                            <input type="hidden" id="txtVrnoHidden">
                            <!-- </div> -->
                          </div>                                               
                          
                        </div>
                        <div class="row">
                          <div class="col-lg-3">
                            <div class="form-group">
                              <label id="balance_lbl">Party Name</label>
                              <div class="input-group" >
                                <select class="form-control  select2" id="party_dropdown" >
                                  <option value="" disabled="" selected="">Choose party</option>
                                  <?php foreach ($parties as $party): ?>
                                  <option value="<?php echo $party['pid']; ?>"><?php echo $party['name']; ?></option>
                                  <?php endforeach ?>
                                </select>
                                <a  tabindex="-1" class="input-group-addon btn btn-primary active" style="min-width:40px !important;" id="A2" data-target="#AccountAddModel" data-toggle="modal" href="#addCategory" rel="tooltip"
                                data-placement="top" data-original-title="Add Category" data-toggle="tooltip" data-placement="bottom" title="Add New Account Quick (F3)"><i class="fa fa-plus"></i></a>
                              </div>
                            </div>      
                          </div>                          
                          <!-- <div class="col-lg-3">                                                
                            <label>Warehouse</label>
                            <select class="form-control select2" id="dept_dropdown">
                              <option value="" selected="" disabled="">Choose Warehouse</option>
                              <?php foreach ($departments as $department): ?>
                              <option value="<?php echo $department['did']; ?>"><?php echo $department['name']; ?></option>
                              <?php endforeach ?>
                            </select>                            
                          </div> -->
                          <div class="col-lg-2">                                                
                            <label>Received By</label>
                            <input class='form-control' type='text' list="receivers" id='receivers_list'>
                            <datalist id='receivers'>
                              <?php foreach ($receivers as $receiver): ?>
                              <option value="<?php echo $receiver['received_by']; ?>"></option>
                              <?php endforeach ?>
                            </datalist>                                                
                          </div>                                                                           
                          <!-- <div class="col-lg-3">                                                
                            <label>Through</label>
                            <select class="form-control select2" id="transporter_dropdown">
                              <option value="" disabled="" selected="">Choose transporter</option>
                              <?php foreach ($transporters as $transporter): ?>
                              <option value="<?php echo $transporter['transporter_id']; ?>"><?php echo $transporter['name']; ?></option>
                              <?php endforeach ?>
                            </select>                                               
                          </div> -->
                          <div class="col-lg-3">
                          <label>Godown</label>
                          <select class="form-control select2" id="dept_dropdown">
                            <option value="" disabled="" selected="">Choose Godown </option>
                            <?php foreach ($departments as $department): ?>
                              <option value="<?php echo $department['did']; ?>"><?php echo $department['name']; ?></option>
                            <?php endforeach ?>
                          </select>
                        </div>
                      </div>
                      <div class="row" style="margin-top:0px;">
                          <div class="col-lg-8">                                            
                            <label>Remarks</label>
                            <input type="text" class="form-control " id="txtRemarks">
                          </div>
                        </div>                                        
                        <div class="row" style="margin-top:0px;">
                          <div class="col-lg-12">                        
                            <div class="tab-content">
                              <div class="tab-pane active fade in" id="itemadd">
                                <div class="container-wrap">
                                  <div class="row">
                                    <!-- <div class="col-lg-1" style="width:120px;">
                                      <label for="">Item Code</label>
                                      <div class="input-group">                                                
                                        
                                        <select class="form-control select2" id="itemid_dropdown">
                                          <option value="" disabled="" selected="">Item Code</option>
                                          <?php foreach ($items as $item): ?>
                                          <option value="<?php echo $item['item_id']; ?>" data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>" data-stqty="<?php echo $item['stqty']; ?>" data-stweight="<?php echo $item['stweight']; ?>" ><?php echo $item['item_id']; ?></option>
                                          <?php endforeach ?>
                                        </select>
                                      </div>
                                    </div>
                                    <div class="col-lg-4">
                                      <label for="" id="" >Item Description</label>
                                      <div class="input-group" >
                                      <select class="form-control select2" id="item_dropdown">
                                        <option value="" disabled="" selected="">Item description</option>
                                        <?php foreach ($items as $item): ?>
                                        <option value="<?php echo $item['item_id']; ?>" data-uom_item="<?php echo $item['uom']; ?>" data-prate="<?php echo $item['cost_price']; ?>" data-grweight="<?php echo $item['grweight']; ?>" data-stqty="<?php echo $item['stqty']; ?>" data-stweight="<?php echo $item['stweight']; ?>"><?php echo $item['item_des']; ?></option>
                                        <?php endforeach ?>
                                      </select>
                                      <a class="input-group-addon btn btn-primary active btnsearchitem" href="#item-lookup" data-toggle="modal"  style="min-width:40px !important;"><i class="fa fa-search"></i></a>
                                      </div>
                                      
                                    </div> -->
                                    <div class="col-lg-4" >
                                        <label for="">Item Code List <img id="imgItemLoader" class="hide" src="<?php echo base_url(); ?>/assets/img/loader.gif"></label>
                                        <input type="text" class="form-control" id="txtItemId">
                                        <input id="hfItemId" type="hidden" value="" />
                                        <input id="hfItemSize" type="hidden" value="" />
                                        <input id="hfItemBid" type="hidden" value="" />
                                        <input id="hfItemUom" type="hidden" value="" />
                                        <input id="hfItemPrate" type="hidden" value="" />
                                        <input id="hfItemGrWeight" type="hidden" value="" />
                                        <input id="hfItemStQty" type="hidden" value="" />
                                        <input id="hfItemStWeight" type="hidden" value="" />
                                        <input id="hfItemLength" type="hidden" value="" />
                                        <input id="hfItemCatId" type="hidden" value="" />
                                        <input id="hfItemSubCatId" type="hidden" value="" />
                                        <input id="hfItemDesc" type="hidden" value="" />
                                        <input id="hfItemUname" type="hidden" value="" />
                                        <input id="hfItemShortCode" type="hidden" value="" />
                                    </div>
                                    <div class="col-lg-4">
                                        <label for="" id="stqty_lbl">Item</label>
                                        <input type="text" class="form-control" id="itemDesc" readonly="" tabindex="-1">
                                    </div>
                                    <!-- <div class="col-lg-1">
                                      <label for="">Dozen</label>                                                    
                                      <input type="text" class="form-control num" id="txtDozenQty">                                                    
                                    </div> -->
                                    <!-- <div class="col-lg-1">
                                      <label for="">Bag</label>                                                    
                                      <input type="text" class="form-control num" id="txtBag">                                                    
                                    </div> -->
                                    <div class="col-lg-1">
                                      <label for="">Qty</label>                                                    
                                      <input type="text" class="form-control num" id="txtQty">                                                    
                                    </div>
                                    <div class="col-lg-1 hide">
                                      <label for="">GW</label>                                                    
                                      <input type="text" class="form-control readonly num" id="txtGWeight" readonly="" tabindex="-1">                                                    
                                    </div> 
                                     <div class="col-lg-1">
                                      <label for="">Uom</label>                                                    
                                      <input type="text" class="form-control readonly num" id="txtUom" readonly="" tabindex="-1">                                                    
                                    </div>
                                    <div class="col-lg-1">
                                      <label for="">Weight</label>                                                    
                                      <input type="text" class="form-control num" id="txtWeight">                                                    
                                    </div>
                                    <div class="col-lg-1">
                                      <label for="">Rate</label>                                                    
                                      <input type="text" class="form-control num" id="txtPRate">                                                    
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-lg-1" style="width:150px;">
                                      <label for="">Amount</label>                                                    
                                      <input type="text" class="form-control readonly num" id="txtAmount" readonly="true" tabindex="-1">                                                    
                                    </div>
                                    <div class="col-lg-1" style='margin-top: 30px;'>                                                    
                                      <a href="" class="btn btn-primary" id="btnAdd"><i class='fa fa-plus'></i></a>
                                    </div>                                                
                                  </div>
                                </div><br>
                                <div class="row">
                                  <div class="col-lg-12">
                                    <div id="no-more-tables">
                                      <table class="col-lg-12 table table-bordered table-striped table-condensed cf" id="purchase_table">
                                        <thead class="cf tbl_thead">
                                          <tr>
                                            <th class="numeric">Sr#</th>
                                            <th class="numeric">Code</th>
                                            <th>Item Description</th>
                                            <th class="numeric hidden text-right">Dozen</th>
                                            <th class="numeric hidden text-right">Bag</th>
                                            <th class="numeric text-right">Qty</th>
                                            <th class="numeric text-right">Weight</th>
                                            <th class="numeric">Rate</th>
                                            <th class="numeric">Amount</th>
                                            <!-- <th class="numeric">Type</th> -->
                                            <th>Action</th>
                                          </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                        <tfoot class="tfoot_tbl">
                                          <tr>
                                            <td data-title="" class="numeric"></td>
                                            <td data-title="" class="numeric"></td>
                                            <td data-title="" class="numeric" style="color:red;">Total:</td>
                                            <td data-title="" class="numeric hidden" id="txtTotalDozen"></td>
                                            <td data-title="" class="numeric hidden" id="txtTotalBag"></td> 
                                            <td data-title="" class="numeric text-right" style="color:red;" id="txtTotalQty"></td>
                                            <td data-title="" class="numeric text-right" style="color:red;" id="txtTotalWeight"></td>                                   
                                            <td data-title="" class="numeric" class="numeric"></td>
                                            <td data-title="" class="numeric" class="numeric" style="color:red;" id="txtTotalAmount"></td>
                                            <td data-title="" class="numeric" class="numeric"></td> 
                                            <!-- <td data-title="" class="numeric"></td> -->
                                          </tr>
                                        </tfoot>
                                      </table>
                                    </div><!-- no-more-table -->
                                  </div><!-- end of col -->
                                </div><!-- end of row -->
                              </div><!-- tab-pane -->
                            </div><!-- tab-content -->
                          </div><!-- end of col -->
                        </div><!-- end of row -->
                      </div><!-- panel-body -->
                    </div><!-- panel -->  
                  </form> <!-- end of form -->
                </div>  <!-- end of panel-body -->
              </div>  <!-- end of panel -->
            </div>  <!-- end of col -->
            <div class="row">
              <div class="col-lg-12">
                <div class="panel panel-default">
                  <div class="panel-body">
                    <div class="row">                                    
                      <!-- <div class="col-lg-3">                                                    
                        <label>Total Weight</label>
                        <input type="text" class="form-control readonly num" id="txtTotalWeight" readonly="true" tabindex="-1">
                      </div>                                                
                      <div class="col-lg-3">                                                    
                        <label>Total Qty</label>
                        <input type="text" class="form-control readonly num" id="txtTotalQty" readonly="true" tabindex="-1">
                      </div> -->                                                
                      <div class="col-lg-3 hidden">                                                    
                        <label class="hidden">Total Amount</label>
                        <input type="text" class="form-control readonly num hidden" id="txtTotalAmount" readonly="true" tabindex="-1">
                      </div>
                      <div class="col-lg-3 hidden">                                                    
                        <label class="hidden">Less Amount</label>
                        <input type="text" class="form-control num hidden" id="txtPaid">
                      </div>                                                
                    </div>
                    <div class="row">                                           
                      <div class="col-lg-1 hidden">                                                    
                        <label class="hidden">Discount%</label>
                        <input type="text" class=" form-control num hidden"  id="txtDiscount">
                      </div>                                                
                      <div class="col-lg-2 hidden">                                                    
                        <label class="hidden">DiscAmount</label>
                        <input type="text" class=" form-control num hidden"  id="txtDiscAmount">
                      </div>
                      <div class="col-lg-1 hidden">                                                    
                        <label class="hidden">Expense%</label>
                        <input type="text" class=" form-control num hidden"  id="txtExpense">
                      </div>                                                
                      <div class="col-lg-2 hidden">                                                    
                        <label class="hidden">Exp Amount</label>
                        <input type="text" class=" form-control num hidden"  id="txtExpAmount">
                      </div>
                      <div class="col-lg-1 hidden">                                                    
                        <label class="hidden">Tax%</label>
                        <input type="text" class=" form-control num hidden"  id="txtTax">
                      </div>                                                
                      <div class="col-lg-2 hidden">                                                    
                        <label class="hidden">TaxAmount</label>
                        <input type="text" class=" form-control num hidden"  id="txtTaxAmount">
                      </div>
                      <div class="col-lg-3 hidden">                                                    
                        <label class="hidden">Net Amount</label>
                        <input type="text" class="form-control readonly hidden" id='txtNetAmount' readonly="" tabindex="-1">
                        <!-- input type="text" class="form-control readonly num" id="txtUom" >                                                     -->
                      </div>
                    </div>
                    <div class="row">                                                                                    
                      <div class="col-lg-6">
                        <div class="pull-left">
                          <a class="btn btn-sm btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                          <a class="btn btn-sm btn-default btnSave" data-savegodownbtn='<?php echo $vouchers['warehouse']['insert']; ?>' data-saveaccountbtn='<?php echo $vouchers['account']['insert']; ?>' data-saveitembtn='<?php echo $vouchers['item']['insert']; ?>' data-insertbtn='<?php echo $vouchers['outwardservice']['insert']; ?>' data-updatebtn='<?php echo $vouchers['outwardservice']['update']; ?>' data-deletebtn='<?php echo $vouchers['outwardservice']['delete']; ?>' data-printbtn='<?php echo $vouchers['outwardservice']['print']; ?>' ><i class="fa fa-save"></i> Save F10</a>
                          <a class="btn btn-sm btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>  
                          <div class="btn-group">
                            <button type="button" class="btn btn-primary btn-sm btnPrint" ><i class="fa fa-save"></i>Print F9</button>
                            <!-- <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                              <li ><a href="#" class="btnprintHeader"> Print with header</li>
                              <li ><a href="#" class="btnprintwithOutHeader"> Print with Out header</li>
                            </ul> -->
                          </div>
                        </div>
                          <!--<a href="#party-lookup" data-toggle="modal" class="btn btn-sm btn-default btnsearchparty "><i class="fa fa-search"></i>&nbsp;Account Lookup F1</a>
                          <a href="#item-lookup" data-toggle="modal" class="btn btn-sm btn-info btnsearchitem "><i class="fa fa-search"></i>&nbsp;Item Lookup F2</a>-->
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">                                                                
                          <div class="input-group">
                            <span class="switch-addon input-group-addon">Print Header?</span>
                            <input type="checkbox" checked="" class="bs_switch" id="switchHeader">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">                                                                
                          <div class="input-group">
                            <span class="switch-addon input-group-addon">User:</span>
                            <select class="form-control " disabled="" id="user_dropdown">
                              <option value="" disabled="" selected="">...</option>
                              <?php foreach ($userone as $user): ?>
                              <option value="<?php echo $user['uid']; ?>"><?php echo $user['uname']; ?></option>
                              <?php endforeach; ?>
                            </select>
                          </div>
                        </div>
                      </div><!-- end of col -->
                      <div class="col-lg-2 hidden">
                        <div class="form-group">                                                                
                          <div class="input-group">
                            <span class="switch-addon input-group-addon">Pre Bal?</span>
                            <input type="checkbox" checked="" class="bs_switch" id="switchPreBal">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><!-- panel-body -->
                </div><!-- end of row -->
              </div><!-- end of col -->
            </div><!-- end of row -->
          </div>
          <div class="tab-pane fade" id="Search">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="row">
                  <div class="col-lg-3">
                    <div class="input-group">
                      <span class="input-group-addon">From</span>
                      <input class="form-control " type="date" id="from_date" value="<?php echo date('Y-m-d'); ?>">
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <div class="input-group">
                      <span class="input-group-addon">To</span>
                      <input class="form-control " type="date" id="to_date" value="<?php echo date('Y-m-d'); ?>">
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="pull-right">
                      <a href='' class="btn btn-sm btn-success btnSearch" id="btnSearch" ><i class="fa fa-search"></i> Search</a>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <div id="no-more-tables">
                      <table class="col-lg-12 table-bordered table-striped table-condensed cf" id="purchase_tableReport">
                        <thead class="cf tbl_thead">
                          <tr>
                            <th>Vr#</th>
                            <th>Date</th>
                            <th class="numeric text-left">Party</th>
                            <th class="numeric text-left">Remarks</th>
                            <th class="numeric text-right">Dozen</th>
                            <th class="numeric text-right">Bag</th>
                            <th class="numeric text-right">Qty</th>
                            <th class="numeric text-right">Weight</th>
                            <!-- <th>Action</th> -->
                          </tr>
                        </thead>
                        <tbody id="saleRows" class="report-rows saleRows">

                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>                  
  </div>
</div>  <!-- end of level 1-->

<div id="item-lookup" class="modal fade modal-lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h3 id="myModalLabel">item Lookup</h3>
            </div>

                <div class="modal-body">
                <div style="width:250px; float: right !important;top:-10px; position: relative; bottom:30px;">
                    <label for="">Search</label>
                    <input type="text" class="form-control" id="txtItemSearch">
                </div>
                <table class="table" id="tbItems">
                <!-- <table class="table table-bordered table-striped modal-table"> -->
                <thead>
                <tr style="font-size:16px;">
                    <th>Id</th>
                    <th>Code</th>
                    <th>Description</th>
                    <th>Uom</th>
                    <th style='width:3px;'>Actions</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
                </table>
                <div id="divNoRecord" class="divNoRecord"></div>
                    <div id="paging"></div>
                </div>
                <div class="modal-footer">
                <!-- <button class="btn btn-danger delete-modal-del">Delete</button> -->
                <button class="btn btn-primary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>