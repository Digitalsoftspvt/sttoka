<?php

    $desc = $this->session->userdata('desc');
    $desc = json_decode($desc);
    $desc = objectToArray($desc);

    $vouchers = $desc['vouchers'];
?>
<style type="text/css">
  .select2-container .select2-choice{
    width:100% !important;
  }
</style>
<!-- main content -->
<div id="main_wrapper" style="  margin: 24px 0 0 80px;">
  <div class="page_bar" style="   margin: -22px 0px 0px 0px !important;  padding: 9px 23px 39px;">
    <div class="row-fluid">
      <div class="col-md-7">
        <h1 class="page_title"><i class="fa fa-money"></i> Cash Slip</h1>
      </div>
      <div class="col-lg-5">
        <div class="pull-right">
          <div class="col-lg-12">
            <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
            <a class="btn btn-primary btnSave" data-insertbtn='<?php echo $vouchers['cashslip']['insert']; ?>' data-updatebtn='<?php echo $vouchers['cashslip']['update']; ?>' data-deletebtn='<?php echo $vouchers['cashslip']['delete']; ?>' data-printbtn='<?php echo $vouchers['cashslip']['print']; ?>'><i class="fa fa-save"></i> Save F10</a>
           <!--  <a class="btn btn-primary btnPrint"><i class="fa fa-print"></i> Print</a> -->
            <a class="btn btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>
            <div class="btn-group">
              <button type="button" class="btn btn-primary btn-sm btnPrint btnPrintThermal" ><i class="fa fa-save"></i>Print F9</button>
              <!-- <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
              </button>
              <ul class="dropdown-menu" role="menu">
                <li ><a href="#" class="btnPrintThermal"> Print Thermal</a></li>
              </ul> -->
            </div>
          </div><!-- end of col -->
        </div><!-- pull-right -->
      </div><!-- end of col -->
    </div><!-- end of row -->
  </div><!-- pagebar -->

  <div class="page_content">
    <div class="container-fluid">
      <div class="row ">
                <div class="col-lg-12">
                    <ul class="nav nav-pills">
                        <li class="active"><a href="#Main" data-toggle="tab">Main</a></li>
                        <li><a href="#Attachments" data-toggle="tab">Attachments</a></li>
                    </ul>
                </div>
            </div>
      <div class="col-md-12">
        <form action="">
          <div class="row">
            <div class="col-lg-12">            
              <div class="panel-body">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="tab-content">
                                <div class="tab-pane active fade in" id="Main">
                    <div class="row">
                      <div class="col-lg-8">
                        <div class="row">
                          <div class="col-lg-2">
                            <label>Vr#</label> 
                            <input type="number" class="form-control " id="txtVrnoa" >
                            <input type="hidden" id="txtMaxVrnoaHidden">
                            <input type="hidden" id="txtVrnoaHidden">
                            

                            <input type="hidden" name="uid" id="uid" value="<?php echo $this->session->userdata('uid'); ?>">
                            <input type="hidden" name="uname" id="uname" value="<?php echo $this->session->userdata('uname'); ?>">
                            <input type="hidden" name="cid" id="cid" value="<?php echo $this->session->userdata('company_id'); ?>">
                            <input type="hidden" id="voucher_type_hidden">

                            <input type="hidden" id="purchaseid" value="<?php echo $setting_configur[0]['purchase']; ?>">
                            <input type="hidden" id="discountid" value="<?php echo $setting_configur[0]['discount']; ?>">
                            <input type="hidden" id="expenseid" value="<?php echo $setting_configur[0]['expenses']; ?>">
                            <input type="hidden" id="taxid" value="<?php echo $setting_configur[0]['tax']; ?>">
                            <input type="hidden" id="cashid" value="<?php echo $setting_configur[0]['cash']; ?>">
                          </div>
                          <div class="col-lg-3">  
                            <label>Date</label>
                            <input class="form-control ts_datepicker" type="text" id="current_date">
                            <input class="form-control ts_datepicker" type="hidden" id="vrdate">
                          </div>
                          <div class='col-lg-4'>
                            <label>Customer Name</label>
                              <select class="form-control select2" id="cus_dropdown" >
                                  <option value="" disabled="" selected="">Choose party</option>
                                  <?php foreach ($parties as $party): ?>
                                      <option value="<?php echo $party['pid']; ?>" data-address="<?php echo $party['address']; ?>" data-uaddress="<?php echo $party['uaddress']; ?>" data-uname="<?php echo $party['uname']; ?>" data-pbalance="<?php echo $party['pbalance']; ?>"><?php echo $party['name']; ?></option>
                                  <?php endforeach ?>
                              </select>
                          </div>
                          <div class="col-lg-3">
                              <label>Send Msg</label>     
                              <select class="form-control" id="msgStatus">
                                  <option disabled="" value="">Choose Status</option>
                                  <option value="1" selected="">Yes</option>
                                  <option value="0">No</option>
                              </select>                                           
                          </div>       
                        </div>
                        <div class="row">
                          <div class="col-lg-3">
                            <label for="crv" class="radio crvRadio">
                                <input type="radio" id="crv" name="vrEtype" value="crv" checked="checked">
                                Cash Receipt
                            </label>
                          </div>
                          <div class="col-lg-3">
                            <label for="cpv" class="radio cpvRadio">
                                <input type="radio" id="cpv" name="vrEtype" value="cpv" >
                                Cash Payment
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="row">
                        <div class="row">      
                          <div class="col-sm-6">
                            <label>Account Ledger</label>
                          </div>
                          <div class="col-sm-6 text-right">
                            <label>Current Balance: <span class="pbalance" id="pbalance"></span>

                            </label>
                            
                          </div>
                      </div>
                            
                            <table class="lastLedgerTable table-striped" id="lastLedgerTable" style="width: 100%;">
                              <thead>
                                <tr>
                                  <th>Date</th>
                                  <th>Debit</th>
                                  <th>Credit</th>
                                </tr>
                              </thead>
                              <tbody>
                              </tbody>
                            </table>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-12">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-lg-8" style="background:white;"><br>
                              <table class="table table_trans" id="conversionTable">
                              <tbody>
                                <tr>
                                  <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" placeholder="0"></td>
                                  <td class="secondTd"><input type="text" class="form-control text-center" value="10" disabled="true"></td>
                                  <td class="thirdTd"><input type="text" class="form-control text-center" placeholder="0" disabled="true" ></td>
                                  <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" placeholder="0"></td>
                                </tr>
                                <tr>
                                  <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" placeholder="0"></td>
                                  <td class="secondTd"><input type="text" class="form-control text-center" value="20" disabled="true"></td>
                                  <td class="thirdTd"><input type="text" class="form-control text-center" placeholder="0" disabled="true"></td>
                                  <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" placeholder="0"></td>
                                </tr>
                                <tr>
                                  <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" placeholder="0"></td>
                                  <td class="secondTd"><input type="text" class="form-control text-center" value="50" disabled="true"></td>
                                  <td class="thirdTd"><input type="text" class="form-control text-center" placeholder="0" disabled="true"></td>
                                  <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" placeholder="0"></td>
                                </tr>
                                <tr>
                                  <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" placeholder="0"></td>
                                  <td class="secondTd"><input type="text" class="form-control text-center" value="100" disabled="true"></td>
                                  <td class="thirdTd"><input type="text" class="form-control text-center" placeholder="0" disabled="true" disabled="true"></td>
                                  <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" placeholder="0"></td>
                                </tr>
                                <tr>
                                  <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" placeholder="0"></td>
                                  <td class="secondTd"><input type="text" class="form-control text-center" value="500" disabled="true"></td>
                                  <td class="thirdTd"><input type="text" class="form-control text-center" placeholder="0" disabled="true"></td>
                                  <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" placeholder="0"></td>
                                </tr>
                                <tr>
                                  <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" placeholder="0"></td>
                                  <td class="secondTd"><input type="text" class="form-control text-center" value="1000" disabled="true"></td>
                                  <td class="thirdTd"><input type="text" class="form-control text-center" placeholder="0" disabled="true"></td>
                                  <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" placeholder="0"></td>
                                </tr>
                                <tr>
                                  <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" placeholder="0"></td>
                                  <td class="secondTd"><input type="text" class="form-control text-center" value="5000" disabled="true"></td>
                                  <td class="thirdTd"><input type="text" class="form-control text-center" placeholder="0" disabled="true"></td>
                                  <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" placeholder="0"></td>
                                </tr>
                                <tr>
                                  <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" placeholder="0"></td>
                                  <td class="secondTd"><input type="text" class="form-control text-center" value="DD" disabled="true"></td>
                                  <td class="thirdTd"><input type="text" class="form-control text-center" placeholder="0" disabled="true"></td>
                                  <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" placeholder="0"></td>
                                </tr>
                                <tr>
                                  <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" value="" placeholder="0"></td>
                                  <td class="secondTd"><input type="text" class="form-control text-center" value="TT" id="txtTTAmount" disabled="true"></td>
                                  <td class="thirdTd"><input type="text" class="form-control text-center" value="" id="txtTTAmountVal" disabled="true" placeholder="0"></td>
                                  <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" placeholder="0"></td>
                                  <td colspan="2">
                                    <select class="form-control select2" id="field_dropdown">
                                      <option value="" disabled="" selected="" ></option>
                                      <?php foreach ($fields as $party): ?>
                                      <option value="<?php echo $party['pid']; ?>"><?php echo $party['name']; ?></option>
                                      <?php endforeach ?>
                                    </select>
                                  </td>
                                </tr>
                                <tr>
                                  <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" placeholder="0"></td>
                                  <td class="secondTd"><input type="text" class="form-control text-center" value="CHEQUE" disabled="true"></td>
                                  <td class="thirdTd"><input type="text" class="form-control text-center" placeholder="0" disabled="true"></td>
                                  <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" placeholder="0"></td>
                                </tr>
                                <tr>
                                  <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" placeholder="0"></td>
                                  <td class="secondTd"><input type="text" class="form-control text-center" value="TC" disabled="true"></td>
                                  <td class="thirdTd"><input type="text" class="form-control text-center" placeholder="0" disabled="true"></td>
                                  <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" placeholder="0"></td>
                                </tr>
                                <tr>
                                  <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" placeholder="0"></td>
                                  <td class="secondTd"><input type="text" class="form-control text-center" value="DOLLAR" disabled="true"></td>
                                  <td class="thirdTd"><input type="text" class="form-control text-center" placeholder="0" disabled="true"></td>
                                  <td class="exchangeRate"><input type="text" class="form-control exchangeRate" placeholder="0"></td>
                                  <td class="text-primary" style="width: 100px;">Exchange Rate</td>
                                </tr>
                                <tr>
                                  <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" placeholder="0"></td>
                                  <td class="secondTd"><input type="text" class="form-control text-center" value="POUND" disabled="true"></td>
                                  <td class="thirdTd"><input type="text" class="form-control text-center" placeholder="0" disabled="true"></td>
                                  <td class="exchangeRate"><input type="text" class="form-control exchangeRate" placeholder="0"></td>
                                  <td class="text-primary" style="width: 100px !important;">Exchange Rate</td>
                                </tr>
                                <tr>
                                  <td class="firsrtTd"><input type="text" class="form-control text-center firstVal" placeholder="0"></td>
                                  <td class="secondTd"><input type="text" class="form-control text-center" value="OTHER" disabled="true"></td>
                                  <td class="thirdTd"><input type="text" class="form-control text-center" placeholder="0" disabled="true"></td>
                                  <td class="exchangeRate hidden"><input type="text" class="form-control exchangeRate" placeholder="0"></td>
                                </tr>
                              </tbody>  
                              <tfoot>
                                <tr>
                                  <td colspan="2"><input type="text" class="form-control text-center" value="GRAND TOTAL" disabled="true"></td>
                                  <td><input type="text" class="form-control text-center" disabled="" id="txtTotal" placeholder="0"></td>
                                </tr>
                              </tfoot>
                              </table>
                            </div>
                            <div class="col-lg-4">
                              <div class="form-group">
                                <div class="row"> 
                                  <div class="col-lg-12">
                                    <label>Bank Name</label>
                                    <!--<input class="form-control" type='text'  list="banks" id="bank_dropdown" >
                                        <detaillist id='banks'>
                                        <?php /*foreach ($banks as $bank): */?>
                                           <option value="<?php /*echo $bank['bank_id']; */?>">
                                        <?php /*endforeach */?>
                                        </detaillist>
                                    </input>-->
                                      <input class='form-control' type='text' list="banks" id='bank_dropdown'>
                                      <datalist id='banks'>
                                          <?php foreach ($banks as $bank): ?>
                                          <option value="<?php echo $bank['bank_id']; ?>">
                                              <?php endforeach ?>
                                      </datalist>
                                  </div>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="comment">Remarks</label>
                                <textarea class="form-control" rows="5" id="txtRemarks"></textarea>
                              </div>
                              <div class="form-group">
                                <div class="row">
                                  <div class='col-lg-12'>
                                    <label>Cash Account</label>
                                      <select class="form-control select2" id="cash_dropdown" >
                                          <option value="" disabled="" selected="">Choose Account</option>
                                          <?php foreach ($cashes as $party): ?>
                                              <option value="<?php echo $party['pid']; ?>" data-address="<?php echo $party['address']; ?>" data-uaddress="<?php echo $party['uaddress']; ?>" data-uname="<?php echo $party['uname']; ?>" data-pbalance="<?php echo $party['pbalance']; ?>"><?php echo $party['name']; ?></option>
                                          <?php endforeach ?>
                                      </select>
                                  </div>
                                </div>
                              </div>
                              <!-- <div class="form-group">
                                <div class="row"> 
                                  <div class="col-lg-12">
                                    <label>Device</label>
                                    <select class="form-control select2" id="party_dropdown">
                                      <option value="" disabled="" selected="">Choose Device</option>
                                      <?php foreach ($parties as $party): ?>
                                      <option value="<?php echo $party['pid']; ?>"><?php echo $party['name']; ?></option>
                                      <?php endforeach ?>
                                    </select>
                                  </div>
                                </div>
                              </div> -->
                              
                              <div class="row">
                                <div class="form-group">
                                <label for="comment">TT Remarks</label>
                                <textarea class="form-control" rows="3" id="txtRemarks2"></textarea>
                              </div>
                                <div class="col-lg-1">
                                  <a class="btn btn-primary">Send Message</a>
                                </div>
                              </div><!-- end of row -->
                            </div><!-- end of col -->
                          </div><!-- end of row -->
                        </div><!-- panel-body -->
                      </div><!-- end of col -->
                      </form><!-- end of form -->
                    </div><!-- end of row -->
                  </div><!-- end of col -->   
                  <div class="tab-pane fade in Attachmentspanel" id="Attachments">
                                <div class="row">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <input type="number" class="form-control" id="txtIdImg" style="background: #fff;" readonly />
                                        </div>
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-primary" id="viewImages">View Attachments <i class="fa fa-eye"></i></button>
                                            <button type="button" class="btn btn-success" id="uploadFiles" data-insert='<?php echo $vouchers['uploadFiles']['insert']; ?>' data-update='<?php echo $vouchers['uploadFiles']['update']; ?>' data-delete='<?php echo $vouchers['uploadFiles']['delete']; ?>' >Update Files <i class="fa fa-upload"></i></button>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                         <form enctype="multipart/form-data" class="dropzone" id="image-upload" method="POST">
                                                <div>
                                                    <h3>Attach Files to Upload</h3>
                                                    <div class="fallback">
                                                        '<input name="file" type="file" />
                                                    </div>
                                                </div>
                                            </form>
                                            <small style="color: #555;"> Supported Files: (.bmp, .gif, .jpg, .jpeg, .png, .pdf, .doc, .docx, .txt, .xls, .xlsx, .rtf)</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            </div>     
                  </div><!-- end of MAin -->          
                  </div><!-- end of Tab-content -->          
                  <div class="row">
                    <div class="pull-right">
                      <div class="col-lg-12">
                        <a class="btn btn-default btnReset"><i class="fa fa-refresh"></i> Reset F5</a>
                        <a class="btn btn-primary btnSave1" data-insertbtn='<?php echo $vouchers['cashslip']['insert']; ?>' data-updatebtn='<?php echo $vouchers['cashslip']['update']; ?>' data-deletebtn='<?php echo $vouchers['cashslip']['delete']; ?>' data-printbtn='<?php echo $vouchers['cashslip']['print']; ?>'><i class="fa fa-save"></i> Save F10</a>
                       <!--  <a class="btn btn-primary btnPrint"><i class="fa fa-print"></i> Print</a> -->
                        <a class="btn btn-default btnDelete"><i class="fa fa-trash-o"></i> Delete F12</a>
                        <div class="btn-group">
                        <button type="button" class="btn btn-primary btn-sm btnPrint" ><i class="fa fa-save"></i>Print F9</button>
                        <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                          <span class="caret"></span>
                          <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                          <li ><a href="#" class="btnPrintThermal"> Print Thermal</a></li>
                        </ul>
                    </div>
                      </div><!-- end of col -->
                    </div><!-- pull-right -->     
                  </div><!-- end of row -->
                </div><!-- panel-body -->
              </div><!-- end of col -->
          </div><!-- end of col -->
        </div><!-- contaier-fluid -->
      </div><!-- page-content --> 
    </div><!-- main-wrapper -->
  </div>
</div>