<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

 

class My_mpdf {

    

    function __construct()

    {

        $CI = & get_instance();

        log_message('Debug', 'mPDF class is loaded.');

    }

 

    function load($param=NULL)

    {

        include_once APPPATH.'/third_party/mpdf/mpdf.php';

         

        if ($params == NULL)

        {

            // $param = '"en-GB-x","A4","","",10,10,10,10,6,3';          



            // $param = '"en-GB-x","A4","","",10,10,10,10,6,3'; // Defaults to portrait

            // $param = '"en-GB-x","A4","","",10,10,10,10,6,3,"P"'; // Portrait

            $param = '"en-GB-x","A4","","",10,10,10,10,6,3,"L"'; // Landscape



        }



        return new mPDF($param);

    }

}