<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FileUploaders extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
	}

	public function isVoucherExist($vrnoa, $etype, $tablename)
	{
        if ($tablename==='pledger')
        {
            $clvrnoa = 'dcno';
        }
        else
        {
            $clvrnoa = 'vrnoa';
        }

        $this->db->where(array($clvrnoa => $vrnoa, 'etype' => $etype ));

        $result = $this->db->get($tablename);
        
        if ($result->num_rows() > 0)
        {
			return true;
		} 
        else
        {
			return false;
		}
	}

	public function uploading($etype)
	{

        $config['upload_path']        = FCPATH . 'assets/uploads/' . $etype . '/';
		$config['allowed_types']      = '*';
		$config['max_size']           = 2048;
		$config['remove_spaces']      = FALSE;


		$response = array();
		
        $this->load->library('upload');
		
        $this->upload->initialize($config);
		// Creating Directory
		if( ! is_dir($config['upload_path']))
        {
            @mkdir($config['upload_path'], 0755, true);
		}

        if ( ! $this->upload->do_upload('file'))
        {
            $error = array('error' => $this->upload->display_errors());                
            $response['result'] = $error;
            header("HTTP/1.0 400 Bad Request");
			die(print_r($error['error']));
        }
        else
        {
            $data = array('upload_data' => $this->upload->data('file'));
            $error = array('error' => $this->upload->display_errors());
            $response['error'] = $error;
        }
        
		return $response;
	}

	public function uploadImages($imgData) 
	{
        $dataRow = $this->db->get_where('imagestock', array('vrnoa' => $imgData['vrnoa'], 'etype' => $imgData['etype'], 'photo' => $imgData['photo']));

        if($dataRow->num_rows() == 0)
        {
        	$result = $this->uploading($_POST['etype']);
        	if($result['error']['error'] == '') {
				$this->db->insert('imagestock', $imgData);
	            $affect = $this->db->affected_rows(); 
	            if ($affect === 0) {
	                return false;
	            } else {
	                return $affect;
	            }
        	}else {
        		 header("HTTP/1.0 400 Bad Request");
				die("Failed to Upload File!");
        	}            
        }else {
        	header("HTTP/1.0 400 Bad Request");
			die('File with this name already exist!');
            return false;
        }
    }

    public function updateFilesRemarks($vrnoa, $etype, $details)
    {
    	// die(print_r($details));
    	$result = $this->db->get_where('imagestock', array('vrnoa' => $vrnoa, 'etype' => $etype, 'photo' => $details['photo']));
    	$img_id = $result->row_array();
  
    	$data = array(
    		'vrnoa' => $vrnoa,
    		'etype' => $etype,
    		'photo' => $details['photo'],
    		'remarks' => $details['remarks'],
    	);
    	if ($result->num_rows() > 0) {
    		$img_id = $img_id['img_id'];
    		$this->db->where('img_id', $img_id);
			$row = $this->db->update('imagestock', $data);
			return $row;
    	} else {
    		return false;
    	}
    }

    public function fetchSearchedFile($searchedTxt)
    {

    	$this->db->or_like('vrnoa', $searchedTxt);
		$this->db->or_like('etype', $searchedTxt);
		$this->db->or_like('photo', $searchedTxt);
		$this->db->or_like('remarks', $searchedTxt);
        $this->db->or_like('datetime', $searchedTxt);
    	$result = $this->db->get('imagestock');
    	// $query = "SELECT * FROM imagestock WHERE CONCAT(vrnoa, etype, photo, remarks) LIKE '%TRIM(".$searchedTxt.")%'";
  
    	if ($result->num_rows() > 0) {
			return $result->result_array();
    	} else {
    		return false;
    	}
    }

    public function fetchImages($vrnoa, $etype)
    {

        $result = $this->db->get_where('imagestock', array('vrnoa' => $vrnoa, 'etype' => $etype));

        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

     public function deleteImage($vrnoa, $etype, $imgName, $path)
    {

        $affect = 0;
        try {
        	unlink($path . $imgName);
        	$affect = 1;
        } catch (Exception $e) {
        	echo $e->errorMessage();
        	$affect = 0;
        }
        // if(unlink($path . $imgName)){
        //     $affect = 1;
        // }else{
        // 	$affect = 0;
        // }
        
        $this->db->delete('imagestock', array('vrnoa' => $vrnoa, 'etype' => $etype, 'photo' => $imgName));
        $affect += $this->db->affected_rows(); 
        if ($affect == 0) {
            return false;
        } else {
            return $imgName;
        }
    }

}

/* End of file orders.php */
/* Location: ./application/models/orders.php */