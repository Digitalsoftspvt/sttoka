<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Purchases extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function getMaxVrno($etype,$company_id) {

		$result = $this->db->query("SELECT MAX(vrno) vrno FROM stockmain WHERE etype = '". $etype ."' AND company_id=". $company_id ."  AND DATE(vrdate) = DATE(NOW())");
		$row = $result->row_array();
		$maxId = $row['vrno'];

		return $maxId;
	}
 	
 	public function fetchAllPurchases( $company_id,$etype ) {

        $result = $this->db->query("SELECT round(stockmain.discount,0) as discount,round(stockmain.expense,0) as expense,round(stockmain.tax,0) as tax,stockmain.vrnoa, DATE_FORMAT(stockmain.vrdate,'%d %b %y') AS DATE,p.name AS party_name,stockmain.remarks,round(stockmain.taxpercent,1) as taxpercent,round(stockmain.exppercent,1) as exppercent,round(stockmain.discp,1)as discp,stockmain.paid,round(stockmain.namount,0) as namount,user.uname as user_name,TIME(stockmain.date_time) as date_time,stockmain.vrnoa
                                    FROM stockmain stockmain
                                    INNER JOIN party p ON p.pid = stockmain.party_id
                                    INNER JOIN user ON user.uid = stockmain.uid
                                    INNER JOIN company c ON c.company_id = stockmain.company_id
                                    WHERE stockmain.company_id= '".$company_id."' AND stockmain.etype= '".$etype."' AND stockmain.vrdate = CURDATE()
                                    ORDER BY stockmain.vrnoa DESC
                                    LIMIT 10");
        // if ( $result->num_rows() > 0 ) {
            return $result->result_array();
        // } else {
        //  return false;
        // }
    }

	public function sendMessage( $mobile, $message )
	{

		$ptn = "/^[0-9]/";  // Regex

		$rpltxt = "92";  // Replacement string

		$mobile = preg_replace($ptn, $rpltxt, $mobile);




		// Create the SoapClient instance 

		$url = ZONG_API_SERVICE_URL; 

		// $soapClient = new SoapClient( $url);

		// var_dump($soapClient->__getFunctions());



		$post_string = '<?xml version="1.0" encoding="utf-8"?>'.

		'<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' .

		  '<soap:Body>'.

		    '<SendSingleSMS xmlns="http://tempuri.org/">'.

		      '<Src_nbr>' . ZONG_API_MOB . '</Src_nbr>'.

		      '<Password>' . ZONG_API_PASS . '</Password>'.

		      '<Dst_nbr>' . $mobile . '</Dst_nbr>'.

		      '<Mask>' . ZONG_API_MASK . '</Mask>'.

		      '<Message>' . $message . '</Message>'.

		    '</SendSingleSMS>'.

		  '</soap:Body>'.

		'</soap:Envelope>';



		$soap_do = curl_init(); 

		curl_setopt($soap_do, CURLOPT_URL,            $url );   

		curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10); 

		curl_setopt($soap_do, CURLOPT_TIMEOUT,        10); 

		curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );

		curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);  

		curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false); 

		curl_setopt($soap_do, CURLOPT_POST,           true ); 

		curl_setopt($soap_do, CURLOPT_POSTFIELDS,    $post_string); 

		curl_setopt($soap_do, CURLOPT_HTTPHEADER,     array('Content-Type: text/xml; charset=utf-8', 'Content-Length: '.strlen($post_string) ));



		$result = curl_exec($soap_do);

		$err = curl_error($soap_do);



		return $result;
	}

	public function fetchPurchaseReportData($startDate, $endDate, $what, $type, $company_id, $etype,$field,$crit,$orderBy,$groupBy,$name)
	{

			if ($type == 'detailed') {
                $temp=("SELECT $field as voucher,$name, dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username,stockmain.vrdate, stockmain.vrnoa, stockmain.remarks,  stockdetail.qty, stockdetail.weight, stockdetail.rate, stockdetail.netamount, item.item_des as 'item_des',item.uom FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.stid INNER JOIN party party ON stockmain.party_id = party.pid              INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3  INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2 INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1 INNER JOIN item item ON stockdetail.item_id = item.item_id left JOIN user user ON user.uid = stockmain.uid  INNER JOIN department dept  ON  stockdetail.godown_id = dept.did WHERE  stockmain.vrdate BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' $crit  order by $orderBy");
			    $query = $this->db->query("SELECT $field as voucher,$name, dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username,stockmain.vrdate, stockmain.vrnoa, stockmain.remarks,  stockdetail.qty, stockdetail.weight, stockdetail.rate, stockdetail.netamount, item.item_des as 'item_des',item.uom FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.stid INNER JOIN party party ON stockmain.party_id = party.pid              INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3  INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2 INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1 INNER JOIN item item ON stockdetail.item_id = item.item_id left JOIN user user ON user.uid = stockmain.uid  INNER JOIN department dept  ON  stockdetail.godown_id = dept.did WHERE  stockmain.vrdate BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' $crit  order by $orderBy");
				return $query->result_array();
			} else {
				$query = $this->db->query("SELECT $field as voucher,$name,date(stockmain.vrdate) as DATE,dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username, date(stockmain.VRDATE) VRDATE, stockmain.vrnoa, round(SUM(stockdetail.qty)) qty, round(SUM(stockdetail.weight)) weight, round(SUM(stockdetail.rate)) rate, round(SUM(stockdetail.amount)) amount, round(sum(stockdetail.netamount)) netamount, stockmain.remarks FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.stid = stockdetail.stid INNER JOIN party party ON stockmain.party_id = party.pid              INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3  INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2 INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1 INNER JOIN item item ON stockdetail.item_id = item.item_id INNER JOIN user user ON user.uid = stockmain.uid  INNER JOIN department dept  ON  stockdetail.godown_id = dept.did WHERE  stockmain.vrdate between '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' $crit group by $groupBy order by $orderBy");
				return $query->result_array();
			}	
		
	}

	public function fetchContractPurchaseReportData($startDate, $endDate, $what, $type, $company_id, $etype,$field,$crit,$orderBy,$groupBy,$name)
	{	
		if ($type == 'complete') {
			$query = $this->db->query("SELECT m.vrnoa,i.item_des,i.uom,d.rate, stk.rate smrate, IFNULL(d.qty,0) o_qty, IFNULL(st_qty,0) s_qty,stk1.st1_qty, IFNULL(d.weight,0) o_weight, IFNULL(st_weight,0) m_weight,stk1.st1_weight,m.vrdate , stk.vrdate smvrdate
										FROM ordermain AS m
										INNER JOIN orderdetail AS d ON m.oid = d.oid
										INNER JOIN item AS i ON i.item_id = d.item_id
										LEFT JOIN (
										SELECT sd.inv_no,sd.item_id, IFNULL(sd.weight,0) AS st_weight, IFNULL(sd.qty,0) AS st_qty,sm.etype AS smetype,sm.vrdate,sd.rate
										FROM stockmain AS sm
										INNER JOIN stockdetail AS sd ON sm.stid = sd.stid
										INNER JOIN item AS i ON i.item_id = sd.item_id
										WHERE sm.etype='purchase' AND sm.company_id=1
																				) AS stk ON stk.inv_no=m.vrnoa AND stk.item_id=d.item_id
										LEFT JOIN (
										SELECT sd1.inv_no,sd1.item_id, IFNULL(SUM(sd1.weight),0) AS st1_weight, IFNULL(SUM(sd1.qty),0) AS st1_qty
										FROM stockmain AS sm
										INNER JOIN stockdetail AS sd1 ON sm.stid = sd1.stid
										INNER JOIN item AS i ON i.item_id = sd1.item_id
										WHERE sm.etype='purchase' AND sm.company_id=1
										GROUP BY sd1.inv_no,sd1.item_id
																				) AS stk1 ON stk1.inv_no=m.vrnoa AND stk1.item_id=d.item_id
										WHERE m.company_id = 1 AND m.etype = 'purchasecontract' AND m.company_id=1 and m.vrdate BETWEEN '". $startDate ."' AND '". $endDate ."' $crit
										HAVING CASE WHEN (i.uom = 'kg' || i.uom = 'kgs') THEN  o_weight-stk1.st1_weight = 0 ELSE o_qty - stk1.st1_qty = 0 END
										ORDER BY m.vrnoa");
			return $query->result_array();
		} else {
			$query = $this->db->query("SELECT m.vrnoa,i.item_des,i.uom,d.rate, stk.rate smrate, IFNULL(d.qty,0) o_qty, IFNULL(st_qty,0) s_qty,stk1.st1_qty, IFNULL(d.weight,0) o_weight, IFNULL(st_weight,0) m_weight,stk1.st1_weight,m.vrdate , stk.vrdate smvrdate
										FROM ordermain AS m
										INNER JOIN orderdetail AS d ON m.oid = d.oid
										INNER JOIN item AS i ON i.item_id = d.item_id
										LEFT JOIN (
										SELECT sd.inv_no,sd.item_id, IFNULL(sd.weight,0) AS st_weight, IFNULL(sd.qty,0) AS st_qty,sm.etype AS smetype,sm.vrdate,sd.rate
										FROM stockmain AS sm
										INNER JOIN stockdetail AS sd ON sm.stid = sd.stid
										INNER JOIN item AS i ON i.item_id = sd.item_id
										WHERE sm.etype='purchase' AND sm.company_id=1
																				) AS stk ON stk.inv_no=m.vrnoa AND stk.item_id=d.item_id
										LEFT JOIN (
										SELECT sd1.inv_no,sd1.item_id, IFNULL(SUM(sd1.weight),0) AS st1_weight, IFNULL(SUM(sd1.qty),0) AS st1_qty
										FROM stockmain AS sm
										INNER JOIN stockdetail AS sd1 ON sm.stid = sd1.stid
										INNER JOIN item AS i ON i.item_id = sd1.item_id
										WHERE sm.etype='purchase' AND sm.company_id=1
										GROUP BY sd1.inv_no,sd1.item_id
																				) AS stk1 ON stk1.inv_no=m.vrnoa AND stk1.item_id=d.item_id
										WHERE m.company_id = 1 AND m.etype = 'purchasecontract' AND m.company_id=1 and m.vrdate BETWEEN '". $startDate ."' AND '". $endDate ."' $crit
										HAVING CASE WHEN (i.uom = 'kg' || i.uom = 'kgs') THEN  o_weight-stk1.st1_weight > 0 ELSE o_qty - stk1.st1_qty > 0 END
										ORDER BY m.vrnoa");
			return $query->result_array();
		}	
		
	}
	
	public function fetchChartData($period, $company_id,$etype)
	{
		$period = strtolower($period);

		$query = '';

		if ($period === 'daily') {
			$query = "SELECT VRNOA, party.name AS ACCOUNT, NAMOUNT  FROM stockmain as stockmain INNER JOIN party as party  ON party.pid = stockmain.party_id  WHERE stockmain.etype='$etype' AND stockmain.vrdate = CURDATE() AND stockmain.company_id=$company_id order by stockmain.vrdate desc LIMIT 10";
		}
		else if ( $period === 'weekly' ) {
			$query = "SELECT sum(case when date_format(vrdate, '%W') = 'Monday' then namount else 0 end) as 'Monday', sum(case when date_format(vrdate, '%W') = 'Tuesday' then namount else 0 end) as 'Tuesday', sum(case when date_format(vrdate, '%W') = 'Wednesday' then namount else 0 end) as 'Wednesday', sum(case when date_format(vrdate, '%W') = 'Thursday' then namount else 0 end) as 'Thursday', sum(case when date_format(vrdate, '%W') = 'Friday' then namount else 0 end) as 'Friday', sum(case when date_format(vrdate, '%W') = 'Saturday' then namount else 0 end) as 'Saturday', sum(case when date_format(vrdate, '%W') = 'Sunday' then namount else 0 end) as 'Sunday' from stockmain where    etype = '$etype' and vrdate between DATE_SUB(VRDATE, INTERVAL 7 DAY) and CURDATE() AND stockmain.company_id=$company_id group by WEEK(VRDATE) order by WEEK(VRDATE) desc LIMIT 1 ";
		}
		else if( $period === 'monthly' ) {
			$query = "SELECT   sum(case when date_format(vrdate, '%b') = 'Jan' then namount else 0 end) as 'Jan', sum(case when date_format(vrdate, '%b') = 'Feb' then namount else 0 end) as 'Feb', sum(case when date_format(vrdate, '%b') = 'Mar' then namount else 0 end) as 'Mar', sum(case when date_format(vrdate, '%b') = 'Apr' then namount else 0 end) as 'Apr',sum(case when date_format(vrdate, '%b') = 'May' then namount else 0 end) as 'May', sum(case when date_format(vrdate, '%b') = 'Jun' then namount else 0 end) as 'Jun',sum(case when date_format(vrdate, '%b') = 'Jul' then namount else 0 end) as 'Jul', sum(case when date_format(vrdate, '%b') = 'Aug' then namount else 0 end) as 'Aug', sum(case when date_format(vrdate, '%b') = 'Sep' then namount else 0 end) as 'Sep', sum(case when date_format(vrdate, '%b') = 'Oct' then namount else 0 end) as 'Oct' , sum(case when date_format(vrdate, '%b') = 'Nov' then namount else 0 end) as 'Nov' , sum(case when date_format(vrdate, '%b') = 'Dec' then namount else 0 end) as 'Dec' from stockmain where    etype = 'purchase' and MONTH(VRDATE) = MONTH(CURDATE()) AND stockmain.company_id=$company_id group by month(VRDATE) order by month(VRDATE)";
		}
		else if ( $period === 'yearly' ) {
			$query = "SELECT YEAR(vrdate) as 'Year', MONTHNAME(STR_TO_DATE(MONTH(VRDATE), '%m')) as Month, sum(namount) AS TotalAmount FROM stockmain where  etype = 'purchase' and YEAR(VRDATE) = YEAR(CURDATE()) and stockmain.company_id=$company_id GROUP BY YEAR(vrdate), MONTH(vrdate) ORDER BY YEAR(vrdate), MONTH(vrdate)";
		}

		$query = $this->db->query($query);
		return $query->result_array();
	}

public function fetchNetSum( $company_id, $etype )
	{
		// if ($etype=='purchase' || $etype='salereturn'){
		// 	$query = "SELECT IFNULL(SUM(DEBIT),0) as 'PRETURNS_TOTAL' FROM pledger WHERE pledger.etype='$etype' AND pledger.company_id=$company_id";
		// }else{
		// 	$query = "SELECT IFNULL(SUM(CREDIT), 0) as 'PRETURNS_TOTAL' FROM pledger WHERE pledger.etype='$etype' AND pledger.company_id=$company_id";
		// }
		if($etype=='sale'){
			$query="SELECT IFNULL(SUM(NAMOUNT),0) as 'PRETURNS_TOTAL' FROM ordermain WHERE ordermain.etype='$etype' AND ordermain.company_id=$company_id";
		}else{
			$query="SELECT IFNULL(SUM(NAMOUNT),0) as 'PRETURNS_TOTAL' FROM stockmain WHERE stockmain.etype='$etype' AND stockmain.company_id=$company_id";	
		}
		
		$result = $this->db->query($query);
		
		return $result->result_array();
	}

	public function getMaxVrnoa($etype,$company_id) {

		$result = $this->db->query("SELECT MAX(vrnoa) vrnoa FROM stockmain WHERE etype = '". $etype ."' and company_id=". $company_id ." ");
		$row = $result->row_array();
		$maxId = $row['vrnoa'];
		return $maxId;
	}

	public function save( $stockmain, $stockdetail, $vrnoa, $etype )
    {
        $company_id = $stockmain['company_id'];
        $godown_id = $stockdetail[0]['godown_id'];
        unset($stockmain['type']);
		$this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype,'company_id'=>$stockmain['company_id'] ));
		$result = $this->db->get('stockmain');

		$stid = "";
		$this->db->trans_start(); # Starting Transaction
		if ($result->num_rows() > 0) {
			$result = $result->row_array();
			$stid = $result['stid'];
			$this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype ,'company_id'=>$stockmain['company_id']));
			$this->db->update('stockmain', $stockmain);

			$this->db->where(array('stid' => $stid ));
			$this->db->delete('stockdetail');
		} else {
			$this->db->insert('stockmain', $stockmain);
			$stid = $this->db->insert_id();
		}

		$narration = "";
		foreach ($stockdetail as $detail) {
			$detail['stid'] = $stid;
			$this->db->insert('stockdetail', $detail);

			if($etype == 'purchase' && $stockmain['msg_status'] == 1){
				
				$itemName = $this->db->query("select item_des from item where item_id=".$detail['item_id']);
				$row = $itemName->row_array();
				$itemName = $row['item_des'];

				$narration .= "item Name:'".$itemName."' Qty.".$detail['qty']." Weight.".$detail['weight']." @.".$detail['rate']." Amount:".$detail['amount'].", ";
			}
		}

		if($narration != ''){
				
			$this->load->model('accounts');
			$previous = $this->accounts->fetchRunningTotal( $stockmain['vrdate'] , $stockmain['party_id'] );
            $previousBalance = $previous[0]['RTotal'];
                   
            $query = 'select name as party_name, mobile from party where pid ='.$stockmain['party_id'];
            $mobile = $this->db->query( $query );
            $row = $mobile->row_array();
			$mobile = $row['mobile'];
			$partyName = $row['party_name'];

			$message = $partyName." ".$narration." Remarks:".$stockmain['remarks']." ".$previousBalance;

			$this->load->model('sendsms');
            $this->sendsms->sendMessage( $mobile, $message);
		}

        if($etype =='production')
        {		   
		   $result = $this->db->query("
		   SELECT m.stid,i.item_id,-(ifnull(sum(d.qty *(rcp.qty/ IFNULL(rcp.fqty,1))),0)) AS qty,
		   i.cost_price as rate ,
		   ifnull(sum(d.qty *(rcp.qty/ IFNULL(rcp.fqty,1))),0)* i.cost_price  as amount,'less' as type,
		   " .$stockdetail[0]['godown_id'] ." as godown_id,-ifnull(sum(d.qty)*(rcp.weight),0) as 'weight'
		   FROM stockmain AS m
		   INNER JOIN stockdetail AS d ON m.stid = d.stid
		   left join (select rd.item_id,rd.qty,rd.weight,rm.item_id as fitem,rm.fqty from recipemain rm inner join recipedetail rd on rm.rid=rd.rid where rm.item_id in (select item_id from stockmain m1 inner join stockdetail d1 on m1.stid = d1.stid where m1.vrnoa =  $vrnoa and m1.etype = 'production')) as rcp on rcp.fitem=d.item_id
		   INNER JOIN item i ON i.item_id = rcp.item_id
		   where m.etype='production' and m.vrnoa= $vrnoa and m.company_id=1
		   group by m.stid,i.item_id");
		   $result=$result->result_array();

		   foreach ($result as $detail) {
			$this->db->insert('stockdetail', $detail);
			}
        }
        if($etype =='employeeproduction')
        {
            $query = $this->db->query("call spw_EployeeProductionLess('" . $stockdetail[0]['godown_id'] .  "', '" . $vrnoa . "', '" . $company_id . "')");
        }
        if($etype =='headmoulding')
        {
		   $result = $this->db->query("
		   select stid,item_id,qty,rate,amount,'less' as 'type'," .$stockdetail[0]['godown_id'] ." as godown_id, sum(weight) as 'weight' from (
		   SELECT m.stid,i.item_id,-(ifnull(sum(d.qty *(rcp.qty/ IFNULL(rcp.fqty,1))),0)) AS qty, i.cost_price as rate ,ifnull(sum(d.qty *(rcp.qty/ IFNULL(rcp.fqty,1))),0)
		   * i.cost_price as amount,'less' as 'type'," .$stockdetail[0]['godown_id'] ." as godown_id,-ifnull(sum(d.qty)*(rcp.weight),0) as 'weight'
		   FROM stockmain AS m
		   INNER JOIN stockdetail AS d ON m.stid = d.stid
		   left join (select rd.item_id,rd.qty,rd.weight,rm.item_id as fitem,rm.fqty from recipemain rm inner join recipedetail rd on rm.rid=rd.rid where rm.item_id in(select item_id from stockdetail d1 inner join stockmain m1 on d1.stid = m1.stid where m1.etype = 'headmoulding' and m1.vrnoa = $vrnoa )) as rcp on rcp.fitem=d.item_id
		   INNER JOIN item i ON i.item_id = rcp.item_id
		   where m.etype='headmoulding' and m.vrnoa=$vrnoa and m.company_id=$company_id
		   group by m.stid,i.item_id,d.item_id) x group by stid,item_id ;");
		   $result=$result->result_array();
		   foreach ($result as $detail) {
			$this->db->insert('stockdetail', $detail);
			}

        }
        if($etype =='moulding')
        {
            		   
		   $result = $this->db->query("select stid,item_id,qty,lprate rate,amount,'less' as 'type','" . $stockdetail[0]['godown_id'] .  "' as godown_id, sum(weight) as weight from (
			SELECT m.stid,i.item_id,-(ifnull(sum(d.qty *(rcp.qty/ IFNULL(rcp.fqty,1))),0)) AS qty, i.cost_price as lprate ,ifnull(sum(d.qty *(rcp.qty/ IFNULL(rcp.fqty,1))),0)
			* i.cost_price as amount,'less',-ifnull(sum(d.qty)*(rcp.weight),0) as 'weight'
			FROM stockmain AS m
			INNER JOIN stockdetail AS d ON m.stid = d.stid
			left join (select rd.item_id,rd.qty,rd.weight,rm.item_id as fitem,rm.fqty from recipemain rm inner join recipedetail rd on rm.rid=rd.rid where rm.item_id in (select item_id from stockdetail d1 inner join stockmain m1 on d1.stid = m1.stid where m1.etype = 'moulding' and m1.vrnoa = $vrnoa)) as rcp on rcp.fitem=d.item_id
			INNER JOIN item i ON i.item_id = rcp.item_id
			where m.etype='moulding' and m.vrnoa = $vrnoa
			group by m.stid,i.item_id,d.item_id) x group by stid,item_id ;");

			$result=$result->result_array();
		   foreach ($result as $detail) {
			$this->db->insert('stockdetail', $detail);
			}

        }
        if($etype == 'head_production'){

        	$query = $this->db->query("call spw_headproduction_less('" . $godown_id .  "', '" . $vrnoa . "', '" . $company_id . "')");
            //mysqli_next_result($this->db->conn_id);
        }

        $this->db->trans_complete(); # Completing transaction

        if ($this->db->trans_status() === FALSE) {
            # Something went wrong.
            $this->db->trans_rollback();
            return FALSE;
        } 
        else {
            # Everything is Perfect. 
            # Committing data to the database.
            $this->db->trans_commit();
            return $stid;
        }
	}
	public function fetchmonthlysalecomparison($til, $dept_id, $type, $cat_id, $subcat_id,$itemtype){

        if ($dept_id == '')
        {
        $result = $this->db->query("select group_concat(did) as dept from department");
        $result=$result->result_array();
        $dept_id=$result[0]['dept'];
		}
		if ($cat_id == '')
        {
        $result = $this->db->query("select group_concat(catid) as catid from category");
        $result=$result->result_array();
        $cat_id=$result[0]['catid'];
		}
		if ($subcat_id == '')
        {
        $result = $this->db->query("select group_concat(subcatid) as subcatid from subcategory");
        $result=$result->result_array();
        $subcat_id=$result[0]['subcatid'];
		}
		if ($itemtype == '')
        {
        $result = $this->db->query("select  group_concat(distinct case when type = '' then '-' else type end) as itemtype from item");
        $result=$result->result_array();
        $itemtype=$result[0]['itemtype'];
        }
        if ($type=='qty')
        {
            $result = $this->db->query("call spw_salescomparisonreport('".$til."','".$dept_id."','".$cat_id."','".$subcat_id."','".$itemtype."')");    
        }
        else
        {
            $result = $this->db->query("call spw_salescomparisonreportamount('".$til."','".$dept_id."','".$cat_id."','".$subcat_id."','".$itemtype."')");
		}        
//		$temp =$result->result_array(); 
        return $result->result_array();
    }
	public function saveSpareParts( $stockmain, $stockdetail, $vrnoa, $etype )
    {
        $company_id = $stockmain['company_id'];
        //$godown_id = $stockdetail[0]['godown_id'];
		$this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype,'company_id'=>$stockmain['company_id'] ));
		$result = $this->db->get('stockmain');

		$stid = "";
		if ($result->num_rows() > 0) {
			$result = $result->row_array();
			$stid = $result['stid'];
			$this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype ,'company_id'=>$stockmain['company_id']));
			$this->db->update('stockmain', $stockmain);

			$this->db->where(array('stid' => $stid ));
			$this->db->delete('stockdetail');
		} else {
			$this->db->insert('stockmain', $stockmain);
			$stid = $this->db->insert_id();
		}

		foreach ($stockdetail as $detail) {
			$detail['stid'] = $stid;
			$this->db->insert('stockdetail', $detail);
		}

        if($etype =='production')
        {
            $query = $this->db->query("call spw_ProductionLess('" . $stockdetail[0]['godown_id'] .  "', '" . $vrnoa . "', '" . $company_id . "')");
            mysqli_next_result($this->db->conn_id);
        }
        if($etype =='employeeproduction')
        {
            $query = $this->db->query("call spw_EployeeProductionLess('" . $stockdetail[0]['godown_id'] .  "', '" . $vrnoa . "', '" . $company_id . "')");
            mysqli_next_result($this->db->conn_id);
        }
        if($etype =='headmoulding')
        {
            $query = $this->db->query("call spw_HeadMouldingLess('" . $vrnoa . "', '" . $company_id . "','" . $stockdetail[0]['godown_id'] .  "')");
            mysqli_next_result($this->db->conn_id);
        }
        if($etype =='moulding')
        {
            $query = $this->db->query("call spw_MouldingLess('" . $vrnoa . "', '" . $company_id . "','" . $stockdetail[0]['godown_id'] .  "')");
            mysqli_next_result($this->db->conn_id);
        }
		return $stid;
	}

	public function fetch( $vrnoa, $etype,$company_id ) {

		$result = $this->db->query("SELECT m.expense_id,m.msg_status,m.dharrypaid,m.f_rate,m.employee_id,m.f_amount,m.l_rate,m.l_amount,m.ul_rate,m.ul_amount,m.scale_charges,d.tot_weight,m.godown_id as godown,m.date_time,m.othercharges,i.item_code,i.uitem_des,m.party_id_co,m.currency_id,
                                    m.received_by as 'received',d.workdetail,g.name as dept_name,m.vrno,m.uid,m.paid,
                                    m.vrnoa, m.vrdate,m.taxpercent,m.exppercent,m.tax,m.discp,m.discount, m.party_id,
                                    m.bilty_no AS 'inv_no', m.bilty_date AS 'due_date', m.received_by, m.transporter_id,
                                    m.remarks, ROUND(m.namount, 2) namount, m.order_vrno AS 'order_no',
                                    ROUND(m.freight, 2) freight, ROUND(m.discp, 2) discp, ROUND(m.discount, 2) discount,
                                    ROUND(m.expense, 2) expense, m.vehicle_id AS 'amnt_paid', m.officer_id,
                                    ROUND(m.ddays) AS 'due_days', d.item_id, d.godown_id, ROUND(d.qty, 2) AS 's_qty',
                                    ROUND(d.qtyf, 2) AS s_qtyf, ROUND(d.rate, 2) AS 's_rate',
                                    ROUND(d.amount, 2) AS 's_amount', ROUND(d.damount, 2) AS 's_damount',
                                    ROUND(d.discount, 2) AS 's_discount', ROUND(d.netamount, 2) AS 's_net',d.type,
                                    i.item_des AS 'item_name',i.uom , d.weight,d.mould_qty,d.waste_qty,d.waste_weight,d.godown_id as godownid ,g.name as godownname,d.inv_no,d.bal_qty_weight,d.bal_qty_weight_hide
                                    FROM stockmain AS m
                                    INNER JOIN stockdetail AS d ON m.stid = d.stid
                                    INNER JOIN item AS i ON i.item_id = d.item_id
                                    left JOIN department AS g ON g.did = d.godown_id
                                    WHERE m.vrnoa = $vrnoa AND m.company_id = $company_id AND m.etype = '". $etype ."' ");
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchSparParts( $vrnoa, $etype,$company_id ) {

		$result = $this->db->query("SELECT m.msg_status,m.f_rate,m.f_amount,m.l_rate,m.l_amount,m.ul_rate,m.ul_amount,m.scale_charges,d.tot_weight,m.godown_id as godown,m.date_time,m.othercharges,i.item_code,i.uitem_des,m.party_id_co,m.currency_id,
                                    m.received_by as 'received',d.workdetail,g.name as dept_name,m.vrno,m.uid,m.paid,
                                    m.vrnoa, m.vrdate,m.taxpercent,m.exppercent,m.tax,m.discp,m.discount, m.party_id,
                                    m.bilty_no AS 'inv_no', m.bilty_date AS 'due_date', m.received_by, m.transporter_id,
                                    m.remarks, ROUND(m.namount, 2) namount, m.order_vrno AS 'order_no',
                                    ROUND(m.freight, 2) freight, ROUND(m.discp, 2) discp, ROUND(m.discount, 2) discount,
                                    ROUND(m.expense, 2) expense, m.vehicle_id AS 'amnt_paid', m.officer_id,
                                    ROUND(m.ddays) AS 'due_days', d.item_id, d.godown_id, ROUND(d.qty, 2) AS 's_qty',
                                    ROUND(d.qtyf, 2) AS s_qtyf, ROUND(d.rate, 2) AS 's_rate',
                                    ROUND(d.amount, 2) AS 's_amount', ROUND(d.damount, 2) AS 's_damount',
                                    ROUND(d.discount, 2) AS 's_discount', ROUND(d.netamount, 2) AS 's_net',d.type,
                                    i.item_des AS 'item_name',i.uom , d.weight,d.mould_qty,d.waste_qty,d.waste_weight,d.godown_id as godownid ,g.name as godownname,d.inv_no,d.bal_qty_weight,d.bal_qty_weight_hide, p.name as phase_name, p.id as phase_id, i.grweight, m.expense_id, m.employee_id, m.godown_id as main_godown_id
                                    FROM stockmain AS m
                                    INNER JOIN stockdetail AS d ON m.stid = d.stid
                                    INNER JOIN item AS i ON i.item_id = d.item_id
                                    left JOIN department AS g ON g.did = d.godown_id
                                    left JOIN phase AS p ON p.id = d.phase_id
                                    WHERE m.vrnoa = $vrnoa AND m.company_id = $company_id AND m.etype = '". $etype ."' ");
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchVoucher($vrnoa, $etype, $company_id)
	{
	    $crit = "";
	     if( $etype==='headmoulding' )
	    {
	        $crit=" if(ifnull(d.type,'add')='add','add',d.type)='add' and ";
	    }
	    
	    $query = "SELECT p.name as party_name,pd.name as employee_id,d.type,m.vrno,m.paid, m.vrnoa, m.vrdate,m.taxpercent,m.paid,m.dharrypaid,m.exppercent,m.tax,m.discp,m.discount, m.party_id, m.bilty_no AS 'inv_no', m.bilty_date AS 'due_date', m.received_by, m.transporter_id, m.remarks, ROUND(m.namount, 2) namount, m.order_vrno AS 'order_no', ROUND(m.freight, 2) freight, ROUND(m.discp, 2) discp, ROUND(m.discount, 2) discount, ROUND(m.expense, 2) expense, m.vehicle_id AS 'amnt_paid', m.officer_id, ROUND(m.ddays) AS 'due_days', d.item_id, d.godown_id, d.qty, ROUND(d.qtyf, 2) AS s_qtyf, ROUND(d.rate, 2) AS 's_rate', ROUND(d.amount, 2) AS 's_amount', ROUND(d.damount, 2) AS 's_damount', ROUND(d.discount, 2) AS 's_discount', ROUND(d.netamount, 2) AS 's_net', i.item_des AS 'item_name',i.item_code AS 'item_code', i.uom, d.weight, dep.name AS 'dept_name' FROM stockmain AS m INNER JOIN stockdetail AS d ON m.stid = d.stid 
		LEFT OUTER JOIN party as pd ON pd.pid=m.employee_id
	    INNER JOIN item AS i ON i.item_id = d.item_id  LEFT OUTER JOIN department dep ON dep.did = d.godown_id INNER JOIN party as p on p.pid=m.party_id WHERE $crit m.vrnoa = $vrnoa AND m.company_id = $company_id AND m.etype = '". $etype ."' ";
	   
		$query = $this->db->query($query);
		 
		return $query->result_array();
	}


	public function fetchNavigation( $vrnoa, $etype, $company_id ) {

		$result = $this->db->query("SELECT m.rate,m.cash_id,m.transport_id,m.cost_id,m.expense_id,m.uid,m.vrno, m.vrnoa, DATE(m.vrdate) vrdate, m.received_by,route.name route_name, d.route_id, IFNULL(d.exp_weight,0) as exp_weight, IFNULL(d.rpm, 0) as rpm, IFNULL(d.amount, 0) as amount, m.remarks, m.etype, d.item_id, d.godown_id, d.godown_id2, ROUND(d.qty, 2) qty, ROUND(d.weight, 2) weight, d.uom, dep.name AS 'dept_to', i.item_des AS 'item_name', dep2.name AS 'dept_from',m.issued_by FROM stockmain m INNER JOIN stockdetail d ON m.stid = d.stid INNER JOIN item i ON i.item_id = d.item_id INNER JOIN department dep ON dep.did = d.godown_id INNER JOIN department dep2 ON dep2.did = d.godown_id2 LEFT JOIN route ON route.route_id = d.route_id WHERE m.vrnoa = $vrnoa AND m.etype = '". $etype ."' AND m.company_id = ". $company_id ." AND qty > 0");
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchByCol($col) {

		$result = $this->db->query("SELECT DISTINCT $col FROM stockmain");
		return $result->result_array();
	}
	public function fetchByCol_inward($col) {

		$result = $this->db->query("SELECT DISTINCT $col FROM stockmain where etype in ('inward','outward') ");
		return $result->result_array();
	}

	public function fetchByColFromStkDetail($col) {

		$result = $this->db->query("SELECT DISTINCT $col FROM stockdetail");
		return $result->result_array();
	}

	public function delete( $vrnoa, $etype , $company_id) {

		$this->db->where(array('etype' => $etype, 'dcno' => $vrnoa , 'company_id' => $company_id ));
		$result = $this->db->delete('pledger');

		$this->db->where(array('etype' => $etype, 'vrnoa' => $vrnoa , 'company_id' => $company_id ));
		$result = $this->db->get('stockmain');

		if ($result->num_rows() == 0) {
			return false;
		} else {

			$result = $result->row_array();
			$stid = $result['stid'];

			$this->db->where(array('etype' => $etype, 'vrnoa' => $vrnoa ));
			$this->db->delete('stockmain');
			$this->db->where(array('stid' => $stid ));
			$this->db->delete('stockdetail');

			if($etype === 'headmoulding'){
				
				$this->db->where(array('stid' => $stid ));
				$this->db->delete('orderdetail');
			}

			return true;
		}
	}

	// public function fetchPurchaseReportData($startDate, $endDate, $what, $type, $company_id, $etype)
	// {
	// 	// sr# date vr# partyname description  quantity rate amount
	// 	if ($what === 'voucher') {
	// 		if ($type == 'detailed') {
	// 			$query = $this->db->query("SELECT stockmain.VRDATE, stockmain.REMARKS, stockmain.VRNOA, stockmain.REMARKS, party.NAME, stockdetail.QTY, stockdetail.WEIGHT, stockdetail.RATE, stockdetail.NETAMOUNT, item.item_des as 'item_des',item.uom FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE  stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id AND stockmain.etype='$etype'  ORDER BY stockmain.VRNOA");
	// 			return $query->result_array();
	// 		} else {
	// 			$query = $this->db->query("SELECT party.NAME, date(stockmain.VRDATE) VRDATE, stockmain.VRNOA, round(SUM(stockdetail.QTY)) QTY, round(SUM(stockdetail.WEIGHT)) WEIGHT, round(SUM(stockdetail.RATE)) RATE, round(sum(stockdetail.NETAMOUNT)) NETAMOUNT, stockmain.REMARKS FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE  stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' Group by stockmain.VRNOA ORDER BY stockmain.VRNOA");
	// 			return $query->result_array();
	// 		}
	// 	}

	// 	else if ($what == 'account') {
	// 		if ($type == 'detailed') {
	// 			$query = $this->db->query("SELECT stockmain.VRDATE, stockmain.REMARKS, stockmain.VRNOA, stockmain.REMARKS, party.NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.NETAMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE  stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' ORDER BY stockmain.party_id");
	// 			return $query->result_array();
	// 		} else {
	// 			$query = $this->db->query("SELECT party.NAME, date(stockmain.VRDATE) VRDATE, stockmain.VRNOA, round(SUM(stockdetail.QTY)) QTY, round(SUM(stockdetail.WEIGHT)) WEIGHT, round(SUM(stockdetail.RATE)) RATE, round(sum(stockdetail.NETAMOUNT)) NETAMOUNT, stockmain.REMARKS FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE  stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' Group by stockmain.party_id ORDER BY stockmain.party_id");
	// 			return $query->result_array();
	// 		}
	// 	}

	// 	else if ($what == 'godown') {
	// 		if ($type == 'detailed') {
	// 			$query = $this->db->query("SELECT stockmain.VRDATE, stockmain.REMARKS, stockmain.VRNOA, stockmain.REMARKS, dept.name AS NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.NETAMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department dept  ON stockdetail.godown_id = dept.did WHERE  stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' ORDER BY stockdetail.godown_id");
	// 			return $query->result_array();
	// 		} else {
	// 			$query = $this->db->query("SELECT dept.NAME, ROUND(SUM(stockdetail.QTY)) QTY, ROUND(SUM(stockdetail.WEIGHT)) WEIGHT, ROUND(SUM(stockdetail.RATE)) RATE, ROUND(SUM(stockdetail.NETAMOUNT)) NETAMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department dept ON dept.did = stockdetail.godown_id WHERE  stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' GROUP BY stockdetail.godown_id AND stockmain.company_id=$company_id AND stockmain.etype='$etype' ORDER BY stockdetail.godown_id");
	// 			return $query->result_array();
	// 		}
	// 	}

	// 	else if ($what == 'item') {
	// 		if ($type == 'detailed') {
	// 			$query = $this->db->query("SELECT stockmain.VRDATE, stockmain.REMARKS, stockmain.VRNOA, stockmain.REMARKS, party.NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.NETAMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE  stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' ORDER BY stockdetail.item_id");
	// 			return $query->result_array();
	// 		} else {
	// 			$query = $this->db->query("SELECT item.item_des as NAME, ROUND(SUM(stockdetail.QTY)) QTY, ROUND(SUM(stockdetail.WEIGHT)) WEIGHT, ROUND(SUM(stockdetail.RATE)) RATE, ROUND(SUM(stockdetail.NETAMOUNT)) NETAMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE  stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' GROUP BY stockdetail.item_id ORDER BY stockdetail.item_id");
	// 			return $query->result_array();
	// 		}
	// 	}

	// 	else if ($what == 'date') {
	// 		if ($type == 'detailed') {
	// 			$query = $this->db->query("SELECT stockmain.VRDATE, stockmain.REMARKS, stockmain.VRNOA, stockmain.REMARKS, party.NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.NETAMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE  stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' ORDER BY stockmain.vrdate");
	// 			return $query->result_array();
	// 		} else {
	// 			$query = $this->db->query("SELECT date(stockmain.vrdate) as DATE, ROUND(SUM(stockdetail.QTY)) QTY, ROUND(SUM(stockdetail.WEIGHT)) WEIGHT, ROUND(SUM(stockdetail.RATE)) RATE, ROUND(SUM(stockdetail.NETAMOUNT)) NETAMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE  stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' GROUP BY stockmain.vrdate ORDER BY stockmain.vrdate");
	// 			return $query->result_array();
	// 		}
	// 	}
	// }

	public function fetchPurchaseReturnReportData($startDate, $endDate, $what, $type)
	{
		$company_id = 1;
		// sr# date vr# partyname description  quantity rate amount
		if ($what === 'voucher') {
			if ($type == 'detailed') {
				$query = $this->db->query("SELECT stockmain.VRDATE, stockmain.REMARKS, stockmain.VRNOA, stockmain.REMARKS, party.NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.NETAMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='purchase return' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id ORDER BY stockmain.VRNOA");
				return $query->result_array();
			} else {
				$query = $this->db->query("SELECT party.NAME, date(stockmain.VRDATE) VRDATE, stockmain.VRNOA, round(SUM(stockdetail.QTY)) QTY, round(SUM(stockdetail.RATE)) RATE, round(sum(stockdetail.NETAMOUNT)) NETAMOUNT, stockmain.REMARKS FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='purchase return' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id Group by stockmain.VRNOA ORDER BY stockmain.VRNOA");
				return $query->result_array();
			}
		}

		else if ($what == 'account') {
			if ($type == 'detailed') {
				$query = $this->db->query("SELECT stockmain.VRDATE, stockmain.REMARKS, stockmain.VRNOA, stockmain.REMARKS, party.NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.NETAMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='purchase return' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id ORDER BY stockmain.party_id");
				return $query->result_array();
			} else {
				$query = $this->db->query("SELECT party.NAME, date(stockmain.VRDATE) VRDATE, stockmain.VRNOA, round(SUM(stockdetail.QTY)) QTY, round(SUM(stockdetail.RATE)) RATE, round(sum(stockdetail.NETAMOUNT)) NETAMOUNT, stockmain.REMARKS FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='purchase return' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id Group by stockmain.party_id ORDER BY stockmain.party_id");
				return $query->result_array();
			}
		}

		else if ($what == 'godown') {
			if ($type == 'detailed') {
				$query = $this->db->query("SELECT stockmain.VRDATE, stockmain.REMARKS, stockmain.VRNOA, stockmain.REMARKS, dept.name AS NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.NETAMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department dept ON stockdetail.godown_id = dept.did WHERE stockmain.ETYPE='purchase return' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id ORDER BY stockdetail.godown_id");
				return $query->result_array();
			} else {
				$query = $this->db->query("SELECT dept.NAME, ROUND(SUM(stockdetail.QTY)) QTY, ROUND(SUM(stockdetail.RATE)) RATE, ROUND(SUM(stockdetail.NETAMOUNT)) NETAMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department dept ON dept.did = stockdetail.godown_id WHERE stockmain.ETYPE='purchase return' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id GROUP BY stockdetail.godown_id ORDER BY stockdetail.godown_id");
				return $query->result_array();
			}
		}

		else if ($what == 'item') {
			if ($type == 'detailed') {
				$query = $this->db->query("SELECT stockmain.VRDATE, stockmain.REMARKS, stockmain.VRNOA, stockmain.REMARKS, party.NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.NETAMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='purchase return' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' ORDER BY stockdetail.item_id");
				return $query->result_array();
			} else {
				$query = $this->db->query("SELECT item.item_des as NAME, ROUND(SUM(stockdetail.QTY)) QTY, ROUND(SUM(stockdetail.RATE)) RATE, ROUND(SUM(stockdetail.NETAMOUNT)) NETAMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='purchase return' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' GROUP BY stockdetail.item_id ORDER BY stockdetail.item_id");
				return $query->result_array();
			}
		}

		else if ($what == 'date') {
			if ($type == 'detailed') {
				$query = $this->db->query("SELECT stockmain.VRDATE, stockmain.REMARKS, stockmain.VRNOA, stockmain.REMARKS, party.NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.NETAMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='purchase return' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' ORDER BY stockmain.vrdate");
				return $query->result_array();
			} else {
				$query = $this->db->query("SELECT date(stockmain.vrdate) as DATE, ROUND(SUM(stockdetail.QTY)) QTY, ROUND(SUM(stockdetail.RATE)) RATE, ROUND(SUM(stockdetail.NETAMOUNT)) NETAMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN party party ON stockmain.PARTY_ID = party.pid INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='purchase return' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' GROUP BY stockmain.vrdate ORDER BY stockmain.vrdate");
				return $query->result_array();
			}
		}
	}

	public function fetchStockNavigationData($startDate, $endDate, $what, $type, $crit)
	{
		// sr# date vr# partyname description  quantity rate amount
		if ($what === 'voucher') {
			if ($type == 'detailed') {
				$query = $this->db->query("SELECT stockmain.VRDATE,stockmain.remarks, stockmain.VRNOA, stockdetail.UOM, stockdetail.QTY, stockdetail.weight, item.item_des as DESCRIPTION, frm.name 'from_dept', _to.name 'to_dept' FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department frm ON stockdetail.godown_id = frm.did INNER JOIN department _to ON stockdetail.godown_id2 = _to.did WHERE stockmain.ETYPE='navigation' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockdetail.QTY > 0 $crit ORDER BY stockmain.VRNOA");
				return $query->result_array();
			}
		}

		else if ($what == 'location') {
			if ($type == 'detailed') {
				$query = $this->db->query("SELECT stockmain.VRDATE,stockmain.remarks, stockmain.VRNOA, stockdetail.UOM, stockdetail.QTY, stockdetail.weight, item.item_des as DESCRIPTION, frm.name 'from_dept', _to.name 'to_dept' FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department frm ON stockdetail.godown_id = frm.did INNER JOIN department _to ON stockdetail.godown_id2 = _to.did WHERE stockmain.ETYPE='navigation' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockdetail.QTY > 0 $crit ORDER BY frm.name");
				return $query->result_array();
			}
		}

		else if ($what == 'item') {
			if ($type == 'detailed') {
				$query = $this->db->query("SELECT stockmain.VRDATE,stockmain.remarks, stockmain.VRNOA, stockdetail.UOM, stockdetail.QTY, stockdetail.weight, item.item_des as DESCRIPTION, frm.name 'from_dept', _to.name 'to_dept' FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department frm ON stockdetail.godown_id = frm.did INNER JOIN department _to ON stockdetail.godown_id2 = _to.did WHERE stockmain.ETYPE='navigation' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockdetail.QTY > 0 $crit ORDER BY item.item_des");
				return $query->result_array();
			}
		}

		else if ($what == 'date') {
			if ($type == 'detailed') {
				$query = $this->db->query("SELECT stockmain.VRDATE,stockmain.remarks, stockmain.VRNOA, stockdetail.UOM, stockdetail.QTY, stockdetail.weight, item.item_des as DESCRIPTION, frm.name 'from_dept', _to.name 'to_dept' FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department frm ON stockdetail.godown_id = frm.did INNER JOIN department _to ON stockdetail.godown_id2 = _to.did WHERE stockmain.ETYPE='navigation' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockdetail.QTY > 0 $crit ORDER BY stockmain.VRDATE");
				return $query->result_array();
			}
		}
	}

	public function fetchMaterialReturnData($startDate, $endDate, $what, $type)
	{
		// sr# date vr# partyname description  quantity rate amount
		if ($what === 'voucher') {
			if ($type == 'detailed') {
				$query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, stockmain.received_by NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.AMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption_return' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' ORDER BY stockmain.VRNOA");
				return $query->result_array();
			} else {
				$query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, stockmain.received_by NAME, sum(stockdetail.QTY) QTY, sum(stockdetail.RATE) RATE, sum(stockdetail.AMOUNT) AMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption_return' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' GROUP BY stockmain.VRNOA ORDER BY stockmain.VRNOA");
				return $query->result_array();
			}
		}

		else if ($what == 'person') {
			if ($type == 'detailed') {
				$query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, stockmain.received_by NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.AMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption_return' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' ORDER BY stockmain.received_by");
				return $query->result_array();
			} else {
				$query = $this->db->query("SELECT stockmain.received_by NAME, SUM(stockdetail.QTY) QTY, SUM(stockdetail.RATE) RATE, SUM(stockdetail.AMOUNT) AMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption_return' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' GROUP BY stockmain.received_by ORDER BY stockmain.received_by");
				return $query->result_array();
			}
		}

		else if ($what == 'location') {
			if ($type == 'detailed') {
				$query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, dept.NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.AMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department dept ON stockdetail.godown_id = dept.did WHERE stockmain.ETYPE='consumption_return' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' ORDER BY stockdetail.godown_id");
				return $query->result_array();
			} else {
				$query = $this->db->query("SELECT dept.NAME, SUM(stockdetail.QTY) QTY, SUM(stockdetail.RATE) RATE, SUM(stockdetail.AMOUNT) AMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department dept ON dept.did = stockdetail.godown_id WHERE stockmain.ETYPE='consumption_return' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' GROUP BY stockdetail.godown_id ORDER BY stockdetail.godown_id");
				return $query->result_array();
			}
		}

		else if ($what == 'item') {
			if ($type == 'detailed') {
				$query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, stockmain.received_by NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.AMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption_return' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' ORDER BY stockdetail.item_id");
				return $query->result_array();
			} else {
				$query = $this->db->query("SELECT item.item_des NAME, (stockdetail.QTY) QTY, SUM(stockdetail.RATE) RATE, SUM(stockdetail.AMOUNT) AMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption_return' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' GROUP BY stockdetail.item_id ORDER BY stockdetail.item_id");
				return $query->result_array();
			}
		}

		else if ($what == 'date') {
			if ($type == 'detailed') {
				$query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, stockmain.received_by NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.AMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption_return' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' ORDER BY stockmain.vrdate desc");
				return $query->result_array();
			} else {
				$query = $this->db->query("SELECT stockmain.VRDATE NAME, (stockdetail.QTY) QTY, SUM(stockdetail.RATE) RATE, SUM(stockdetail.AMOUNT) AMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption_return' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' GROUP BY stockmain.VRDATE ORDER BY stockmain.VRDATE");
				return $query->result_array();
			}
		}
	}


	public function fetchConsumptionData($startDate, $endDate, $what, $type)
	{
		// sr# date vr# partyname description  quantity rate amount
		if ($what === 'voucher') {
			if ($type == 'detailed') {
				$query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, stockmain.received_by NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.AMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' ORDER BY stockmain.VRNOA");
				return $query->result_array();
			} else {
				$query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, stockmain.received_by NAME, sum(stockdetail.QTY) QTY, sum(stockdetail.RATE) RATE, sum(stockdetail.AMOUNT) AMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' GROUP BY stockmain.VRNOA ORDER BY stockmain.VRNOA");
				return $query->result_array();
			}
		}

		else if ($what == 'person') {
			if ($type == 'detailed') {
				$query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, stockmain.received_by NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.AMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' ORDER BY stockmain.received_by");
				return $query->result_array();
			} else {
				$query = $this->db->query("SELECT stockmain.received_by NAME, SUM(stockdetail.QTY) QTY, SUM(stockdetail.RATE) RATE, SUM(stockdetail.AMOUNT) AMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' GROUP BY stockmain.received_by ORDER BY stockmain.received_by");
				return $query->result_array();
			}
		}

		else if ($what == 'location') {
			if ($type == 'detailed') {
				$query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, dept.NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.AMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department dept ON stockdetail.godown_id = dept.did WHERE stockmain.ETYPE='consumption' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' ORDER BY stockdetail.godown_id");
				return $query->result_array();
			} else {
				$query = $this->db->query("SELECT dept.NAME, SUM(stockdetail.QTY) QTY, SUM(stockdetail.RATE) RATE, SUM(stockdetail.AMOUNT) AMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID INNER JOIN department dept ON dept.did = stockdetail.godown_id WHERE stockmain.ETYPE='consumption' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' GROUP BY stockdetail.godown_id ORDER BY stockdetail.godown_id");
				return $query->result_array();
			}
		}

		else if ($what == 'item') {
			if ($type == 'detailed') {
				$query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, stockmain.received_by NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.AMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' ORDER BY stockdetail.item_id");
				return $query->result_array();
			} else {
				$query = $this->db->query("SELECT item.item_des NAME, (stockdetail.QTY) QTY, SUM(stockdetail.RATE) RATE, SUM(stockdetail.AMOUNT) AMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' GROUP BY stockdetail.item_id ORDER BY stockdetail.item_id");
				return $query->result_array();
			}
		}

		else if ($what == 'date') {
			if ($type == 'detailed') {
				$query = $this->db->query("SELECT stockmain.VRNOA, stockmain.VRDATE, stockmain.received_by NAME, stockdetail.QTY, stockdetail.RATE, stockdetail.AMOUNT, item.item_des FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' ORDER BY stockmain.vrdate desc");
				return $query->result_array();
			} else {
				$query = $this->db->query("SELECT stockmain.VRDATE NAME, (stockdetail.QTY) QTY, SUM(stockdetail.RATE) RATE, SUM(stockdetail.AMOUNT) AMOUNT FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.STID INNER JOIN item item ON stockdetail.ITEM_ID = item.ITEM_ID WHERE stockmain.ETYPE='consumption' AND stockmain.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' GROUP BY stockmain.VRDATE ORDER BY stockmain.VRDATE");
				return $query->result_array();
			}
		}
	}

	public function fetchPRRangeSum( $from ,$to )
	{
		$query = "SELECT IFNULL(SUM(CREDIT), 0)-IFNULL(SUM(DEBIT),0) as 'PRETURNS_TOTAL' FROM pledger pledger WHERE pid IN (SELECT pid FROM party party WHERE NAME='purchase return') AND date between '{$from}' AND '{$to}'";
		$result = $this->db->query($query);

		return $result->result_array();
	}

	public function fetchImportRangeSum( $from ,$to )
	{
		$query = "SELECT IFNULL(SUM(DEBIT), 0)- IFNULL(SUM(CREDIT),0) AS 'PIMPORTS_TOTAL' FROM pledger pledger WHERE pid IN ( SELECT pid FROM party party WHERE NAME='purchase import') AND date BETWEEN '{$from}' AND '{$to}'";
		$result = $this->db->query($query);

		return $result->result_array();
	}

	public function fetchRangeSum( $from ,$to )
	{
		$query = "SELECT IFNULL(SUM(DEBIT), 0)- IFNULL(SUM(CREDIT),0) AS 'PURCHASES_TOTAL' FROM pledger pledger WHERE pid IN ( SELECT pid FROM party party WHERE NAME='purchase') AND date BETWEEN '{$from}' AND '{$to}'";
		$result = $this->db->query($query);

		return $result->result_array();
	}
	public function fetchPOrders() {

		
		//$result = $this->db->query("SELECT oid,ordermain.status, vrnoa, DATE(vrdate) vrdate, party.name, city, party.cityarea, ordermain.remarks FROM ordermain INNER JOIN party ON party.pid = ordermain.party_id WHERE ordermain.etype = 'pur_order'");
		$result = $this->db->query("SELECT oid,ordermain.status, vrnoa, DATE(vrdate) vrdate, party.name, city, party.cityarea, ordermain.remarks FROM ordermain INNER JOIN party ON party.pid = ordermain.party_id WHERE ordermain.etype = 'pur_order' and ordermain.vrnoa not in (select order_vrno from stockmain where etype='purchase' and order_vrno <> 0)");
		return $result->result_array();
	}
	public function fetchitemc($vrnoa, $etype, $company_id)
    {
        $result = $this->db->query("SELECT m.date_time,m.customer_name,m.mobile,d.discount as discount1,d.damount ,d.uom,d.etype as detype,d.pcs,d.packing ,d.type, m.party_id_co,m.currency_id, d.received_by as 'received',d.workdetail,g.name as dept_name,m.vrno,m.uid,m.paid, m.vrnoa, m.vrdate,m.taxpercent,m.exppercent,m.tax,m.discp,m.discount, m.party_id, m.bilty_no AS 'inv_no', m.bilty_date AS 'due_date', m.received_by, m.transporter_id, m.remarks, ROUND(m.namount, 2) namount, m.order_vrno AS 'order_no', ROUND(m.freight, 2) freight, ROUND(m.discp, 2) discp, ROUND(m.discount, 2) discount, ROUND(m.expense, 2) expense, m.vehicle_id AS 'amnt_paid', m.officer_id, ROUND(m.ddays) AS 'due_days', d.item_id, d.godown_id, ROUND(d.qty, 2) AS 's_qty', ROUND(d.qtyf, 2) AS s_qtyf, ROUND(d.rate, 2) AS 's_rate', ROUND(d.amount, 2) AS 's_amount', ROUND(d.damount, 2) AS 's_damount', ROUND(d.discount, 2) AS 's_discount', ROUND(d.netamount, 2) AS 's_net', i.item_des AS 'item_name',i.uom , d.weight  FROM stockmain AS m INNER JOIN stockdetail AS d ON m.stid = d.stid INNER JOIN item AS i ON i.item_id = d.item_id  LEFT JOIN department AS g ON g.did = d.godown_id WHERE m.vrnoa = '" . $vrnoa . "' AND m.company_id = '" . $company_id . "' AND m.etype = '" . $etype . "' ");
        if ($result->num_rows() > 0)
        {
            return $result->result_array();
        }
        else
        {
            return false;
        }
    }
    public function fetchReportDataItemcMain($startDate, $endDate, $company_id, $etype, $uid)
    {
        $query = $this->db->query("SELECT stockdetail.etype as detype,g.name as dept_name,item.uom,item.item_des,stockdetail.qty,stockdetail.rate,stockdetail.amount,stockmain.vrnoa,stockmain.vrdate,p.name as party_name,stockmain.remarks,stockmain.taxpercent,stockmain.exppercent,stockmain.discp,stockmain.paid,stockmain.namount from
                                      stockmain stockmain
                                    INNER JOIN stockdetail stockdetail ON stockmain.stid = stockdetail.stid
                                    INNER JOIN item item ON stockdetail.item_id = item.item_id
                                    LEFT JOIN party p ON p.pid = stockmain.party_id
                                    INNER JOIN user ON user.uid = stockmain.uid
                                    INNER JOIN company c ON c.company_id = stockmain.company_id
                                    LEFT JOIN department AS g ON g.did = stockdetail.godown_id
                                    WHERE stockmain.vrdate BETWEEN  '" . $startDate . "' AND '" . $endDate . "' AND stockmain.company_id= '" . $company_id . "' AND stockmain.etype= '" . $etype . "'
                                     and stockmain.uid = $uid order by stockmain.vrnoa");
       //return $query->result_arrayoduction;
		return $query->result_array();

    }

    public function fetchProduction( $vrnoa, $etype,$company_id ) {

    if($etype=='head_production')
    {
   //     $crit = " and d.type='add' ";
        $crit=" ";
    }
    else{
        $crit = " ";
    }
        $result = $this->db->query("SELECT m.othercharges,i.item_code,i.uitem_des,m.party_id_co,m.currency_id,d.perday,
                                    m.received_by as 'received',d.workdetail,g.name as dept_name,m.vrno,m.uid,m.paid,
                                    m.vrnoa, m.vrdate,m.taxpercent,m.exppercent,m.tax,m.discp,m.discount, m.party_id,
                                    m.bilty_no AS 'inv_no', m.bilty_date AS 'due_date', m.received_by, m.transporter_id,
                                    m.remarks, ROUND(m.namount, 2) namount, m.order_vrno AS 'order_no',
                                    ROUND(m.freight, 2) freight, ROUND(m.discp, 2) discp, ROUND(m.discount, 2) discount,
                                    ROUND(m.expense, 2) expense, m.vehicle_id AS 'amnt_paid', m.officer_id,
                                    ROUND(m.ddays) AS 'due_days', d.item_id, d.godown_id, ROUND(d.qty, 2) AS 's_qty',
                                    ROUND(d.qtyf, 2) AS s_qtyf, ROUND(d.rate, 2) AS 's_rate',
                                    ROUND(d.amount, 2) AS 's_amount', ROUND(d.damount, 2) AS 's_damount',
                                    ROUND(d.discount, 2) AS 's_discount', ROUND(d.netamount, 2) AS 's_net',
                                    i.item_des AS 'item_name',i.uom , d.weight, d.type, d.emp_id, party.name
                                    FROM stockmain AS m
                                    INNER JOIN stockdetail AS d ON m.stid = d.stid
                                    INNER JOIN item AS i ON i.item_id = d.item_id
                                    INNER JOIN department AS g ON g.did = d.godown_id
                                    LEFT JOIN party ON party.pid = d.emp_id
                                    WHERE m.vrnoa = '" . $vrnoa . "' AND m.company_id = '" . $company_id . "' AND m.etype = '" . $etype . "' $crit ");
        if ( $result->num_rows() > 0 ) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function employeeFetchProduction( $vrnoa, $etype,$company_id ) {

        $result = $this->db->query("SELECT m.othercharges,i.item_code,i.uitem_des,m.party_id_co,m.currency_id,
                                    m.received_by as 'received',d.workdetail,g.name as dept_name,m.vrno,m.uid,m.paid,
                                    m.vrnoa, m.vrdate,m.taxpercent,m.exppercent,m.tax,m.discp,m.discount, m.party_id,
                                    m.bilty_no AS 'inv_no', m.bilty_date AS 'due_date', m.received_by, m.transporter_id,
                                    m.remarks, ROUND(m.namount, 2) namount, m.order_vrno AS 'order_no',
                                    ROUND(m.freight, 2) freight, ROUND(m.discp, 2) discp, ROUND(m.discount, 2) discount,
                                    ROUND(m.expense, 2) expense, m.vehicle_id AS 'amnt_paid', m.officer_id,
                                    ROUND(m.ddays) AS 'due_days', d.item_id, d.godown_id, ROUND(d.qty, 2) AS 's_qty',
                                    ROUND(d.qtyf, 2) AS s_qtyf, ROUND(d.rate, 2) AS 's_rate',
                                    ROUND(d.amount, 2) AS 's_amount', ROUND(d.damount, 2) AS 's_damount',
                                    ROUND(d.discount, 2) AS 's_discount', ROUND(d.netamount, 2) AS 's_net',
                                    i.item_des AS 'item_name',i.uom , d.weight, d.type, d.rate,d.amount,m.employee_id,m.expense_id
                                    FROM stockmain AS m
                                    INNER JOIN stockdetail AS d ON m.stid = d.stid
                                    INNER JOIN item AS i ON i.item_id = d.item_id
                                    INNER JOIN department AS g ON g.did = d.godown_id
                                    WHERE m.vrnoa = '" . $vrnoa . "' AND m.company_id = '" . $company_id . "' AND m.etype = '" . $etype . "' ");
        if ( $result->num_rows() > 0 ) {
            return $result->result_array();
        } else {
            return false;
        }
    }

     public function fetchitemcReportData($startDate, $endDate, $what, $type, $company_id, $etype, $field, $crit, $orderBy, $groupBy, $name)
    {
        if ($type == 'detailed')
        {
            $query = $this->db->query("SELECT " . $field . " as voucher," . $name . ",item.uom,dept.name as dept_name,stockdetail.discount as discountP,stockdetail.damount discountA,stockdetail.netamount as d_netamount,stockdetail.pcs, dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username,stockmain.vrdate, stockmain.remarks, stockmain.vrnoa, stockmain.remarks,  stockdetail.qty, stockdetail.weight, stockdetail.rate,stockdetail.etype as detype, stockdetail.amount, stockdetail.netamount, item.item_des as 'item_des',item.uom FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.STID = stockdetail.stid LEFT JOIN party party ON stockmain.party_id = party.pid              LEFT JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3  LEFT JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2 LEFT JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1 INNER JOIN item item ON stockdetail.item_id = item.item_id INNER JOIN user user ON user.uid = stockmain.uid  LEFT JOIN department dept  ON  stockdetail.godown_id = dept.did WHERE  stockmain.vrdate BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND stockmain.company_id = '" . $company_id . "' AND stockmain.etype='" . $etype . "' " . $crit . "  order by " . $orderBy);
            return $query->result_array();
        }
        else
        {
            $query = $this->db->query("SELECT " . $field . " as voucher, " . $name . ",round(SUM(stockdetail.discount)) discountP,round(SUM(stockdetail.damount)) discountA,round(SUM(stockdetail.netamount)) d_netamount ,stockdetail.pcs,date(stockmain.vrdate) as DATE,dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username, date(stockmain.VRDATE) VRDATE, stockmain.vrnoa, round(SUM(stockdetail.qty)) qty, round(SUM(stockdetail.weight)) weight, round(SUM(stockdetail.rate)) rate, round(SUM(stockdetail.amount)) amount, round(sum(stockdetail.netamount)) netamount, stockmain.remarks FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.stid = stockdetail.stid INNER JOIN party party ON stockmain.party_id = party.pid              INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3  INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2 INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1 INNER JOIN item item ON stockdetail.item_id = item.item_id INNER JOIN user user ON user.uid = stockmain.uid  INNER JOIN department dept  ON  stockdetail.godown_id = dept.did WHERE  stockmain.vrdate between '" . $startDate . "' AND '" . $endDate . "' AND stockmain.company_id = '" . $company_id . "' AND stockmain.etype='" . $etype . "' " . $crit . " group by " . $groupBy . " order by " . $orderBy);
            return $query->result_array();
        }
    }

    public function fetchVoucherRange($from, $to, $etype)
	{
		$query = $this->db->query("SELECT m.date_time,m.othercharges,i.item_code,i.uitem_des,m.party_id_co,m.currency_id,
                                    m.received_by as 'received',d.workdetail,g.name as dept_name,m.vrno,m.uid,m.paid,
                                    m.vrnoa, m.vrdate,m.taxpercent,m.exppercent,m.tax,m.discp,m.discount, m.party_id,
                                    m.bilty_no AS 'inv_no', m.bilty_date AS 'due_date', m.received_by, m.transporter_id,
                                    m.remarks, ROUND(m.namount, 2) namount, m.order_vrno AS 'order_no',
                                    ROUND(m.freight, 2) freight, ROUND(m.discp, 2) discp, ROUND(m.discount, 2) discount,
                                    ROUND(m.expense, 2) expense, m.vehicle_id AS 'amnt_paid', m.officer_id,
                                    ROUND(m.ddays) AS 'due_days', d.item_id, d.godown_id, ROUND(d.qty, 2) AS 's_qty',
                                    ROUND(d.qtyf, 2) AS s_qtyf, ROUND(d.rate, 2) AS 's_rate',
                                    ROUND(d.amount, 2) AS 's_amount', ROUND(d.damount, 2) AS 's_damount',
                                    ROUND(d.discount, 2) AS 's_discount', ROUND(d.netamount, 2) AS 's_net',
                                    i.item_des AS 'item_name',i.uom , d.weight
                                    FROM stockmain AS m
                                    INNER JOIN stockdetail AS d ON m.stid = d.stid
                                    INNER JOIN item AS i ON i.item_id = d.item_id
                                    left JOIN department AS g ON g.did = d.godown_id
                                    WHERE m.vrdate BETWEEN '". $from ."' AND '". $to ."' AND m.etype = '". $etype ."'");
		return $query->result_array();
	}

	// public function fetchAllPurchases($company_id, $etype)
 //    {
 //    	$query = $this->db->query("SELECT ordermain.etype,round(ifnull(ordermain.discount,0),0) as discount,round(ifnull(ordermain.expense,0),0) as expense,round(ifnull(ordermain.tax,0),0) as tax,ordermain.vrnoa, DATE_FORMAT(ordermain.vrdate,'%d %b %y') AS DATE,COALESCE(p.name,'') AS party_name,ordermain.remarks,round(ordermain.taxpercent,1) as taxpercent,round(ordermain.exppercent,1) as exppercent,round(ordermain.discp,1) as discp,round(ordermain.paid,0) as paid,round(ordermain.namount,0) AS namount,user.uname as user_name,time(ordermain.vrdate) as date_time
 //                                    FROM ordermain ordermain
 //                                    INNER JOIN party p ON p.pid = ordermain.party_id
 //                                    INNER JOIN user ON user.uid = ordermain.uid
 //                                    INNER JOIN company c ON c.company_id = ordermain.company_id 
 //                                    WHERE ordermain.company_id= " . $company_id . " AND ordermain.etype= '" . $etype . "' AND ordermain.vrdate = SUBDATE(CURDATE(), INTERVAL 1 DAY)
 //                                    ORDER BY ordermain.vrnoa DESC
 //                                    LIMIT 10");
       
 //        return $query->result_array();
 //    }

    public function fetchTotalPurchases($etype)
    {
    	$query = $this->db->query("select ifnull(sum(namount),0) as totalpurchase from stockmain where etype = '" . $etype . "' AND vrdate = SUBDATE(CURDATE(), INTERVAL 1 DAY)");
        return $query->result_array();
    }

    public function fetchAllTransactions($from, $to, $company_id)
    {
        $result['purchaseorder'] = $this->fetchAllTransactionsbyquery1($from, $to, $company_id, 'pur_order');

        $result['purchase'] = $this->fetchAllTransactionsbyquery2($from, $to, $company_id, 'purchase');

        $result['purchasereturn'] = $this->fetchAllTransactionsbyquery2($from, $to, $company_id, 'purchasereturn');

        $result['openingstock'] = $this->fetchAllTransactionsbyquery2($from, $to, $company_id, 'openingstock');

        $result['quotationorder'] = $this->fetchAllTransactionsbyquery1($from, $to, $company_id, 'quotationorder');

        $result['saleorder'] = $this->fetchAllTransactionsbyquery1($from, $to, $company_id, 'sale_order');

        $result['sale'] = $this->fetchAllTransactionsbyquery1($from, $to, $company_id, 'sale');

        $result['salereturn'] = $this->fetchAllTransactionsbyquery2($from, $to, $company_id, 'salereturn');

        $result['requisition'] = $this->fetchAllTransactionsbyquery1($from, $to, $company_id, 'requisition');

        $result['consumption'] = $this->fetchAllTransactionsbyquery2($from, $to, $company_id, 'consumption');

        $result['materialreturn'] = $this->fetchAllTransactionsbyquery2($from, $to, $company_id, 'materialreturn');

        $result['item_conversion'] = $this->fetchAllTransactionsbyquery2($from, $to, $company_id, 'item_conversion');

        $result['inwardvoucher'] = $this->fetchAllTransactionsbyquery2($from, $to, $company_id, 'inwardvoucher');

        $result['outwardvoucher'] = $this->fetchAllTransactionsbyquery2($from, $to, $company_id, 'order_loading');

        $result['production'] = $this->fetchAllTransactionsbyquery2($from, $to, $company_id, 'production');

        $result['payments'] = $this->fetchAllTransactionsbyquery3($from, $to, $company_id, 'cpv');

        $result['receipts'] = $this->fetchAllTransactionsbyquery3($from, $to, $company_id, 'crv');

        $result['chequeissue'] = $this->fetchAllTransactionsbyquery3($from, $to, $company_id, 'pd_issue');

        $result['chequereceipt'] = $this->fetchAllTransactionsbyquery3($from, $to, $company_id, 'pd_receive');

        $result['jv'] = $this->fetchAllTransactionsbyquery3($from, $to, $company_id, 'jv');

        $result['openingbalance'] = $this->fetchAllTransactionsbyquery3($from, $to, $company_id, 'openingbalance');

        $result['penalty'] = $this->fetchAllTransactionsbyquery4($from, $to, $company_id, 'penalty');

        $result['advance'] = $this->fetchAllTransactionsbyquery4($from, $to, $company_id, 'advance');

        $result['loan'] = $this->fetchAllTransactionsbyquery4($from, $to, $company_id, 'loan');

        $result['incentive'] = $this->fetchAllTransactionsbyquery4($from, $to, $company_id, 'incentive');

        $totalofalltransactionss = array(
            'totalpurchases' => $this->getStockMainTotalAmount("purchase", $from, $to, $company_id),
            'totalpurchasereturns' => $this->getStockMainTotalAmount("purchasereturn", $from, $to, $company_id),
            'totalopeningstocks' => $this->getStockMainTotalAmount("openingstock", $from, $to, $company_id),
            'totalquotation' => $this->getOrderMainTotalAmount("quotationorder", $from, $to, $company_id),
            'totalsaleorder' => $this->getOrderMainTotalAmount("sale_order", $from, $to, $company_id),
            'totalsale' => $this->getOrderMainTotalAmount("sale", $from, $to, $company_id),
            'totalsalereturn' => $this->getOrderMainTotalAmount("salereturn", $from, $to, $company_id),
            'totalpayment' => $this->getPledgerTotalAmount("cpv", $from, $to, $company_id),
            'totalreceipt' => $this->getPledgerTotalAmount("crv", $from, $to, $company_id),
            'totalbankpayment' => $this->getPledgerTotalAmount("pd_issue", $from, $to, $company_id),
            'totalbankreceipt' => $this->getPledgerTotalAmount("pd_receive", $from, $to, $company_id),
            'totaljv' => $this->getPledgerTotalAmount("jv", $from, $to, $company_id),
            'totalop' => $this->getPledgerTotalAmount("openingbalance", $from, $to, $company_id),
            'totalpenalty' => $this->getPledgerTotalAmount("penalty", $from, $to, $company_id),
            'totalloan' => $this->getPledgerTotalAmount("loan", $from, $to, $company_id),
            'totaladvance' => $this->getPledgerTotalAmount("advance", $from, $to, $company_id),
            'totalincentive' => $this->getPledgerTotalAmount("incentive", $from, $to, $company_id)
        );

        $result['totalofalltransaction'] = $totalofalltransactionss;
        return $result;
    }

    public function fetchAllTransactionsbyquery1($from, $to, $company_id, $etype)
    {
        $query = $this->db->query("SELECT ordermain.etype, ROUND(ifnull(ordermain.discount,0),0) AS discount, ROUND(ifnull(ordermain.expense,0),0) AS expense, ROUND(ifnull(ordermain.tax,0),0) AS tax,ordermain.vrnoa, DATE_FORMAT(ordermain.vrdate,'%d %b %y') AS DATE,COALESCE(p.name,'') AS party_name,ordermain.remarks, ROUND(ordermain.taxpercent,1) AS taxpercent, ROUND(ordermain.exppercent,1) AS exppercent, ROUND(ordermain.discp,1) AS discp, ROUND(ordermain.paid,0) AS paid, ROUND(ordermain.namount,0) AS namount,user.uname AS user_name, TIME(ordermain.vrdate) AS date_time
                        FROM ordermain ordermain
                        LEFT JOIN party p ON p.pid = ordermain.party_id
                        LEFT JOIN user ON user.uid = ordermain.uid
                        LEFT JOIN company c ON c.company_id = ordermain.company_id
                        WHERE ordermain.company_id= '" . $company_id . "' AND ordermain.etype= '" . $etype . "' AND ordermain.vrdate between '" . $from . "' and '" . $to . "'
                        ORDER BY ordermain.vrnoa DESC
                        LIMIT 10");
        return $query->result_array();
    }

    public function fetchAllTransactionsbyquery2($from, $to, $company_id, $etype)
    {
        $query = $this->db->query("SELECT stockmain.etype, ROUND(ifnull(stockmain.discount,0),0) AS discount, ROUND(ifnull(stockmain.expense,0),0) AS expense, ROUND(ifnull(ifnull(stockmain.tax,0),0),0) AS tax,stockmain.vrnoa, DATE_FORMAT(stockmain.vrdate,'%d %b %y') AS DATE,COALESCE(p.name,'') AS party_name,stockmain.remarks, ROUND(stockmain.taxpercent,1) AS taxpercent, ROUND(stockmain.exppercent,1) AS exppercent, ROUND(stockmain.discp,1) AS discp, ROUND(stockmain.paid,0) AS paid, ROUND(stockmain.namount,0) AS namount,user.uname AS user_name, TIME(stockmain.date_time) AS date_time
                            FROM stockmain stockmain
                            LEFT JOIN party p ON p.pid = stockmain.party_id
                            LEFT JOIN user ON user.uid = stockmain.uid
                            LEFT JOIN company c ON c.company_id = stockmain.company_id
                            WHERE stockmain.company_id= '" . $company_id . "' AND stockmain.etype= '" . $etype . "' AND stockmain.vrdate between '" . $from . "' and '" . $to . "'
                            ORDER BY stockmain.vrnoa DESC
                            LIMIT 10");
        return $query->result_array();
    }

    public function fetchAllTransactionsbyquery3($from, $to, $company_id, $etype)
    {
        $query = $this->db->query("SELECT pledger.etype,COALESCE(p.name,'') AS party_name,pledger.description,pledger.debit,pledger.credit,pledger.deduction,COALESCE(pledger.invoice,'') as invoice, TIME(date_time) AS date_time,user.uname AS user_name,pledger.dcno as vrnoa
                            FROM pledger pledger
                            LEFT JOIN party p ON p.pid = pledger.pid
                            LEFT JOIN user ON user.uid = pledger.uid
                            LEFT JOIN company c ON c.company_id = pledger.company_id
                            WHERE pledger.company_id= '" . $company_id . "' AND pledger.etype= '" . $etype . "' AND pledger.date between '" . $from . "' and '" . $to . "'
                            ORDER BY pledger.pledid DESC
                            LIMIT 10");
        return $query->result_array();
    }

    public function fetchAllTransactionsbyquery4($from, $to, $company_id, $etype)
    {
        $query = $this->db->query("SELECT pledger.etype,COALESCE(p.name,'') AS party_name,pledger.description,pledger.debit,pledger.credit,pledger.deduction,COALESCE(pledger.invoice,'') as invoice, TIME(pledger.date) AS date_time,user.uname AS user_name,pledger.dcno as vrnoa
                            FROM pledger pledger
                            LEFT JOIN party p ON p.pid = pledger.pid
                            LEFT JOIN user ON user.uid = pledger.uid
                            LEFT JOIN company c ON c.company_id = pledger.company_id
                            WHERE pledger.company_id= '" . $company_id . "' AND pledger.etype= '" . $etype . "' AND pledger.date between '" . $from . "' and '" . $to . "' and pledger.debit > 0
                            ORDER BY pledger.pledid DESC
                            LIMIT 10");
        return $query->result_array();
    }

    public function getOrderMainTotalAmount($etype, $fromDate, $toDate, $companyId)
    {
        $query = $this->db->query("select ifnull(sum(namount),0) as total
                                from ordermain
                                where etype = '" . $etype . "' AND vrdate between '" . $fromDate . "' and '" . $toDate . "'
                                and company_id = '" . $companyId . "' ");
        $resultArray = $query->result_array();
        return number_format((float)$resultArray[0]['total'], 0, '.', '');
    }

    public function getStockMainTotalAmount($etype, $fromDate, $toDate, $companyId)
    {
        $query = $this->db->query("select ifnull(sum(namount),0) as total
                                from stockmain
                                where etype = '" . $etype . "' AND vrdate between '" . $fromDate . "' and '" . $toDate . "'
                                and company_id = '" . $companyId . "' ");
        $resultArray = $query->result_array();
        return number_format((float)$resultArray[0]['total'], 0, '.', '');
    }

    public function getPledgerTotalAmount($etype, $fromDate, $toDate, $companyId)
    {
        $query = $this->db->query("select ifnull(sum(debit),0) as total
                              from pledger
                              where etype = '" . $etype . "' AND date between '" . $fromDate . "' and '" . $toDate . "'
                              and company_id = '" . $companyId . "' ");
        $resultArray = $query->result_array();
        return number_format((float)$resultArray[0]['total'], 0, '.', '');
    }

    public function checkCostiong( $stockdetail, $itemId )
    {	
		$result = $this->db->query("SELECT ROUND(recipedetail.qty, 5) qty, ROUND(recipedetail.weight, 5) weight, recipedetail.item_id AS 'item_id', CONCAT(item.item_code,' ',item.item_des) item_des,recipemain.fweight
									FROM recipemain
									INNER JOIN recipedetail ON recipemain.rid = recipedetail.rid
									INNER JOIN item item ON item.item_id = recipedetail.item_id
									where recipemain.item_id=".$itemId);

		if ( $result->num_rows() > 0 ) {

			$finalQty = 0;
			$finalWeight = 0;
			$spareQty = 0;
			$index = 0;
			$totalItemQtySum = 0;
			$totalReceipeItemQtySum = 0;
			$totalItemWeightSum = 0;
			$totalReceipeItemWeightSum = 0;

			$data =  $result->result_array();
			
			foreach ($data as $key => $detail) {
				
				if($key == 0){

					$finishWeight = $detail['fweight'];
				}
				$itemID = $detail['item_id'];
				$qty = $detail['qty'];
				$weight = $detail['weight'];

				$totalReceipeItemQtySum += $qty;
				$totalReceipeItemWeightSum += $weight;

				$result = $this->searchForId($itemID , $stockdetail);

				if($result != false){

					if($result == 'zero'){

						$result = 0;
					}
					
					$qty1 = $stockdetail[$result]['qty'];
					$weight1 = $stockdetail[$result]['weight'];

					$totalItemQtySum += $qty1;
					$totalItemWeightSum += $weight1;

					$qtyResult = floor($qty1/$qty);
					$weightResult = floor($weight1/$weight);

					if($qtyResult < $finalQty || $index == 0){

						$finalQty = $qtyResult;
						$finalWeight = $weightResult;
						$index++;
					}
				}
				else{
					
					return false;
				}
			}

			foreach ($stockdetail as $key => $detail ) {
			
				$itemID = $detail['item_id'];
			
				$result = $this->searchForId($itemID , $data);

				if($result != false){

					if($result == 'zero'){

						$result = 0;
					}
					
					$qty2 = $data[$result]['qty'] * $finalQty;
					$weight2 = $data[$result]['weight'];

					$qty3 = $detail['qty'] - $qty2 ;
					$spareQty += $qty3;

					if($qty3 > 0){
						$stockdetail[$key]['qty'] = $qty3;
					}
					else{
						unset($stockdetail[$key]);
					}
				}
				else{

					$spareQty += $detail['qty'];
				}
			}	

			$allReceipeQty = $finalQty * $totalReceipeItemQtySum;
			$finalWeight = $finalQty * $finishWeight;

			//$spareQty = $totalItemQtySum - $allReceipeQty;
			$spareWeight = $totalItemWeightSum - $finalWeight;

			$datas = array(
							"final_qty" => $finalQty,
							"final_weight" => $finalWeight,
							"spare_qty" => $spareQty,
							"spare_weight" => $spareWeight,
							"stockdetail" => json_encode($stockdetail)
						   );
			return $datas;
		} else {

			return false;
		}
	}

	public function searchForId($id, $array) {

   		foreach ($array as $key => $val) {

       		if ($val['item_id'] === $id) {

       			if($key == 0){
       				
       				return 'zero';
       			}else{

       				return $key;
       			}
       		}
   		}
   		return false;
	}

	public function fetchDetail($vrnoa, $etype,$company_id){

		$result = $this->db->query("SELECT i.item_code,i.uitem_des,d.amount,g.name AS dept_name,d.item_id, d.godown_id, ROUND(d.qty, 2) AS 's_qty',
								i.item_des AS 'item_name',i.uom, d.weight,d.mould_rate,d.mould_amount,d.dharry_rate,d.dharry_amount
                            FROM stockmain AS m
                            INNER JOIN orderdetail AS d ON m.stid = d.stid
                            INNER JOIN item AS i ON i.item_id = d.item_id
                            left JOIN department AS g ON g.did = d.godown_id
                            WHERE m.vrnoa = $vrnoa AND m.company_id = $company_id AND m.etype = '". $etype ."' ");
		
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function saveDetail($orderdetail , $stid){

		$this->db->where(array('stid' => $stid ));
		$this->db->delete('orderdetail');
		
		foreach ($orderdetail as $detail) {
			$detail['stid'] = $stid;
			$this->db->insert('orderdetail', $detail);
		}

		return true;
	}
}

/* End of file purchases.php */
/* Location: ./application/models/purchases.php */