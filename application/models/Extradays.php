<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Extradays extends CI_Model {

	public function __construct() {
		parent::__construct();
	}	

	public function getMaxVrnoa($etype,$company_id) {

		$result = $this->db->query("SELECT MAX(vrnoa) vrnoa FROM extradays_main WHERE etype = '". $etype ."' and company_id=". $company_id ." ");
		$row = $result->row_array();
		$maxId = $row['vrnoa'];

		return $maxId;
	}

	public function save( $stockmain, $stockdetail, $vrnoa, $etype )
    {
        $company_id = $stockmain['company_id'];
        //$godown_id = $stockdetail[0]['godown_id'];
		$this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype,'company_id'=>$stockmain['company_id'] ));
		$result = $this->db->get('extradays_main');

		$extradays_id = "";
		if ($result->num_rows() > 0) {
			$result = $result->row_array();
			$extradays_id = $result['extradays_id'];
			$this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype ,'company_id'=>$stockmain['company_id']));
			$affect = $this->db->update('extradays_main', $stockmain);

			$this->db->where(array('extradays_id' => $extradays_id ));
			$this->db->delete('extradays_detail');
		} else {
			$this->db->insert('extradays_main', $stockmain);
			$extradays_id = $this->db->insert_id();
			$affect = $this->db->affected_rows();
		}

		foreach ($stockdetail as $detail) {
			$detail['extradays_id'] = $extradays_id;
			$this->db->insert('extradays_detail', $detail);
		}

        if ($affect === 0) {
			return false;
		} else {
			return true;
		}
	}


	public function fetch( $vrnoa, $etype,$company_id ) {

		$result = $this->db->query("SELECT *,employee.name as emp_name
									FROM extradays_main AS m
									INNER JOIN extradays_detail AS d ON m.extradays_id = d.extradays_id
									INNER JOIN party as employee on d.employee_id = employee.pid
									WHERE m.vrnoa='".$vrnoa."' AND m.etype='".$etype."' AND m.company_id='".$company_id."'");
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function delete( $vrnoa, $etype , $company_id) {

		$this->db->where(array('etype' => $etype, 'dcno' => $vrnoa , 'company_id' => $company_id ));
		$result = $this->db->delete('pledger');
		
		$this->db->where(array('etype' => $etype, 'vrnoa' => $vrnoa , 'company_id' => $company_id ));
		$result = $this->db->get('extradays_main');

		if ($result->num_rows() == 0) {
			return false;
		} else {

			$result = $result->row_array();
			$extradays_id = $result['extradays_id'];

			$this->db->where(array('etype' => $etype, 'vrnoa' => $vrnoa ));
			$this->db->delete('extradays_main');
			$this->db->where(array('extradays_id' => $extradays_id ));
			$this->db->delete('extradays_detail');

			return true;
		}
	}
}

/* End of file salesmen.php */
/* Location: ./application/models/salesmen.php */