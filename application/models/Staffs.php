<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staffs extends CI_Model {

	public function __construct() {

		parent::__construct();
	}

	public function getMaxId() {

		$this->db->select_max('staid');
		$result = $this->db->get('staff');

		$row = $result->row_array();
		$maxId = $row['staid'];

		return $maxId;
	}

	public function getMaxOvertimeId() {

		$this->db->select_max('dcno');
		$result = $this->db->get('overtime');

		$row = $result->row_array();
		$maxId = $row['dcno'];

		return $maxId;
	}

	public function getMaxSalaryId($etype)
	 {
		$this->db->select_max('dcno');
		$this->db->where(array('etype' => $etype ));
		$result = $this->db->get('salarysheet');

		$row = $result->row_array();
		$maxId = $row['dcno'];

		return $maxId;
	}

	public function getAllTypes() {

		$types = $this->db->query("SELECT DISTINCT type FROM staff WHERE type <> '' ORDER BY type DESC");
		return $types->result_array();
	}

	public function getAllAgreements() {

		$agreements = $this->db->query("SELECT DISTINCT agreement FROM staff WHERE agreement <> '' ORDER BY agreement DESC");
		return $agreements->result_array();
	}

	public function getAllReligions() {

		$religions = $this->db->query("SELECT DISTINCT religion FROM staff WHERE religion <> '' ORDER BY religion DESC");
		return $religions->result_array();
	}

	public function getAllBankNames() {

		$banks = $this->db->query("SELECT DISTINCT bankname FROM salary WHERE bankname <> '' ORDER BY bankname DESC");
		return $banks->result_array();
	}

	public function distincts_fields($fd) {

		$banks = $this->db->query("SELECT DISTINCT $fd FROM salary WHERE $fd <> '' ORDER BY $fd ");
		return $banks->result_array();
	}


	public function save( $staff, $pid ) {

		$staff = json_decode(html_entity_decode($staff['staff'], true));
		$staff = (array)$staff;
		$staff['pid'] = $pid;

		if ( isset($_FILES['photo']) && $_FILES['photo']['size'] > 0) {
			$staff['photo'] = $this->upload_photo( $staff );
		} else {
			unset($staff['photo']);
		}


		$this->db->where(array(
			'staid' => $staff['staid']
			));
		$result = $this->db->get('staff');

		$affect = 0;
		$staid = "";
		if ($result->num_rows() > 0 ) {

			$this->db->where('staid', $staff['staid']);
			$affect = $this->db->update('staff', $staff);

			$staid = $staff['staid'];
		} else {

			unset($staff['staid']);
			$this->db->insert('staff', $staff);
			$affect = $this->db->affected_rows();

			$staid = $this->db->insert_id();
		}

		if ( $affect === 0 ) {
			return false;
		} else {
			return $staid;
		}
	}

	public function saveOvertime( $overtime ) {

		// check if the fee category is already saved or not
		$this->db->where(array('dcno' => $overtime['dcno'] ));
		$result = $this->db->get('overtime');

		$affect = 0;
		if ($result->num_rows() > 0 ) {
			$this->db->where(array('dcno' => $overtime['dcno'] ));
			$affect = $this->db->update('overtime', $overtime);
		} else {
			$this->db->insert('overtime', $overtime);
			$affect = $this->db->affected_rows();
		}

		if ( $affect === 0 ) {
			return false;
		} else {
			return true;
		}
	}

	public function saveSalarySheet( $salarysheet, $dcno ) {
     $etype=($salarysheet[0]['etype']);

		$this->db->where(array('dcno' => $dcno , 'etype' => $etype));
		$affect = $this->db->delete('salarysheet');

		$affect = 0;
		foreach ($salarysheet as $ss) {

			$ss['dcno'] = $dcno;
			$this->db->insert('salarysheet', $ss);
			$affect = $this->db->affected_rows();
		}


		if ( $affect === 0 ) {
			return false;
		} else {
			return true;
		}
	}

	public function upload_photo( $photo ){
		// $uploadsDirectory = ($_SERVER['DOCUMENT_ROOT'] . '/AfaqTraders/application/assets/uploads/item_imgs/');
		$uploadsDirectory = ($_SERVER['DOCUMENT_ROOT'] . '/toka/assets/uploads/staff/');


		// die($uploadsDirectory);
		// die(realpath(dirname(__FILE__)));

		$errors = array(1 => 'php.ini max file size exceeded',
			2 => 'html form max file size exceeded',
			3 => 'file upload was only partial',
			4 => 'no file was attached');

		($_FILES['photo']['error'] == 0)
		or die($errors[$_FILES['photo']['error']]);

		@is_uploaded_file($_FILES['photo']['tmp_name'] )
		or die('Not an HTTP upload');

		@getimagesize($_FILES['photo']['tmp_name'])
		or die('Only image uploads are allowed');

		$now = time();
		while(file_exists($uploadFilename = $uploadsDirectory.$now.'-'.$_FILES['photo']['name']))
		{
			$now++;
		}

		// now let's move the file to its final location and allocate the new filename to it
		move_uploaded_file($_FILES['photo']['tmp_name'], $uploadFilename) or die('Error uploading file');

		$path_parts = explode('/', $uploadFilename);
		$uploadFilename = $path_parts[count($path_parts) - 1];
		return $uploadFilename;
	}

	public function saveSalary( $salary ) {

		$this->db->where(array(
			'staid' => $salary['staid']
			));
		$result = $this->db->delete('salary');


		$this->db->insert('salary', $salary);
	}

	public function saveQualification( $qualifications, $staid ) {

		$this->db->where(array(
			'staid' => $staid
			));
		$result = $this->db->delete('qualification');


		foreach ($qualifications as $qualification) {

			$qualification = (array)$qualification;
			$this->db->insert('qualification', $qualification);
		}
	}

	public function saveExperience( $experiences, $staid ) {

		$this->db->where(array(
			'staid' => $staid
			));
		$result = $this->db->delete('experience');


		foreach ($experiences as $experience) {

			$experience = (array)$experience;
			$this->db->insert('experience', $experience);
		}
	}

	public function savePhases( $phases, $staid ) {

		$this->db->where(array(
			'staid' => $staid
			));
		$result = $this->db->delete('staff_phases');


		foreach ($phases as $phase) {

			$phase = (array)$phase;
			$this->db->insert('staff_phases', $phase);
		}
	}

	public function fetchStaff( $staid ) {

		$this->db->where(array(
			'staid' => $staid
			));
		$result = $this->db->get('staff');

		if ( $result->num_rows() > 0 ) {
			$result = $this->db->query("select staid, deptid, name, fname, religion, photo, address, phone, birthdate, jdate, type, salary, date, mobile, agreement, gender, mstatus, active, cnic, pid, did, gid, gdate, otallowed, otrate, paidleave, unpaidleave, medleave,perdayproduction from staff where staid = $staid");
			return $result->result_array();
		} else {
			return false;
		}
	}
    public function fetchStaffPhaseInfo( $employeeId,$item_id ) {

        $result = $this->db->query("Select staid  from staff where pid =".$employeeId);
        if($result->num_rows() > 0){

            $result = $result->row_array();
            $result1 = $this->db->query("SELECT Distinct p.id,p.name,phases.`cal_method`,phases.rate,phases.perday as perday
				FROM `staff_phases` phases
				inner join item i on i.item_id = phases.item_id
				left join phase p on p.id = phases.phase_id
				WHERE phases.staid =".$result['staid'] ." and phases.item_id=". $item_id );
            if($result1->num_rows() > 0){

                return $result1->result_array();
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
        public function fetch_present_staff( $itemIds, $itemData, $dept_id, $phase_id, $date ) {

		$itemIds = implode(",",$itemIds);

    	// echo "<script>console.log( 'Debug Object: " . $itemIds . "' );</script>";

		$result = $this->db->query("SELECT st.pid, st.name AS 'name', i.item_id, i.item_des, st.deptid, IFNULL(stp.rate,0) AS 'rate',stp.perday as 'perday',ifnull(sts.status,'Absent') as status
									FROM staff AS st
									INNER JOIN staff_phases AS stp ON st.staid = stp.staid AND stp.item_id IN ($itemIds) 
									INNER JOIN item AS i ON i.item_id =stp.item_id
									LEFT JOIN (select staid,status from staffatndetail where date='". $date ."') as sts on sts.staid=st.staid	
									WHERE st.deptid = $dept_id
									ORDER BY item_id");
		
		if ($result->num_rows() == 0 ) {
			return false;
		} else {

			$employeeData = $result->result_array();
			$dbQuery = "SELECT COUNT(st.pid) AS total_employee, i.item_id,stp.perday as perday
						FROM staff AS st
						INNER JOIN staff_phases AS stp ON st.staid = stp.staid AND stp.item_id IN ($itemIds) 
						INNER JOIN item AS i ON i.item_id =stp.item_id
						WHERE st.deptid = $dept_id 
						GROUP BY item_id
						ORDER BY item_id";

			$result = $this->db->query($dbQuery)->result_array();
			$data = array();
			foreach ($employeeData as $key => $value) {
				
				$qty = $this->searchByValue($itemData, $value['item_id'], 'qty');
				$weight = $this->searchByValue($itemData, $value['item_id'], 'weight');
				$totalEmployee = $this->searchByValue($result, $value['item_id'], 'employee');
                
				$emp_qty = round($qty/$totalEmployee,2);
				if ($weight!="0.0000")
                {
                    $emp_weight = $weight/$totalEmployee;
                }
                else{$emp_weight=0;}




				$data[] = array(
					'pid' => $value['pid'],
					'name' => $value['name'],
					'item_id' => $value['item_id'],
					'item_des' => $value['item_des'],
					'deptid' => $value['deptid'],
					'rate' => $value['rate'],
					'qty' => $emp_qty,
                    "weight"=>$emp_weight,
                    'perday' => $value['perday'],
                    'status' => $value['status'],
				);
			}
			
			return $data;
		}
	}

	public function searchByValue($itemData, $value, $type)
	{
	    foreach ($itemData as $data)
	    {
	        if ($data['item_id'] == $value){

                if($type == 'qty'){

                    return $data['qty'];
                }
                else if($type == 'weight'){
                    return $data['weight'];
                }else{

	        		return $data['total_employee'];
	        	}
	        }
	    }
	}

	public function fetchByDepartment($did) {

		$result = "";
		if ($did == '-1') {
			$this->db->where(array('active' => '1'));
			$result = $this->db->get('staff');
		} else {
			$this->db->where(array('did' => $did, 'active' => '1'));
			$result = $this->db->get('staff');
		}

		if ($result->num_rows() == 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}

	public function fetchSalarySheet( $dcno,$etype ) {

		$this->db->where(array(
								'dcno' => $dcno
							));
		$result = $this->db->get('salarysheet');

		if ( $result->num_rows() > 0 ) {
			$result = $this->db->query("SELECT ss.dcno, ss.etype, ss.dts, ss.dte, ss.staid,
                                    ss.did, ss.pid, ss.shid, ROUND(ss.bsalary) bsalary,
                                    ss.absent, ss.leave_wp, ss.rest_days,
                                    ss.work_days,
									ss.leave_gholiday as 'gusted_holiday',
                                    ss.leave_outdoor as 'outdoor',
                                    (ss.leave_sleave / 2) as 'short_leave',ss.perdayproduction,
                                    /*(ss.work_days + ss.leave_wp + ss.rest_days + ss.leave_gholiday + ss.leave_outdoor + (ss.leave_sleave / 2)) as paid_days,*/
                                    paid_days as paid_days,
                                    ROUND(ss.gross_salary) gross_salary,
                                    ROUND(ss.otrate) otrate, ss.othour, ss.overtime overtime,
                                    ROUND(ss.advance) advance, ROUND(ss.loan_deduction) loan_deduction, ROUND(ss.loan_deduction) + round(ss.balance) as loan,
                                    ROUND(ss.balance) balance, ROUND(ss.incentive) incentive,
                                    ROUND(ss.penalty) penalty, ROUND(ss.eobi) eobi,
                                    ROUND(ss.insurance) insurance, ROUND(ss.socialsec) socialsec,
                                    ROUND(ss.net_salary) net_salary, d.name AS 'department_name',
                                    s.name, sal.designation AS 'designation',ss.spid as salary_account_id,headqty,headamount,spqty,spamount
                                    FROM salarysheet AS ss
                                    INNER JOIN department AS d ON ss.did = d.did
                                    INNER JOIN staff AS s ON ss.staid = s.staid
                                    inner join staffatndetail as a on ss.dcno = a.dcno
									inner join salary as sal on sal.staid = s.staid 
                                    WHERE ss.dcno ='".$dcno."' and ss.etype='".$etype."' group by ss.pid");
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchOvertime( $dcno ) {

		$this->db->where(array('dcno' => $dcno ));
		$result = $this->db->get('overtime');

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchStaffSalary( $staid ) {

		$result = $this->db->query("select bs, designation, bpay, inipay, hrent, convallow, medallow, entertain, charge, bankname, acno, netpay, househ, scall, publicsall, saall, dearness, adhoc1, adhoc2, arrears, pfund, income, hostel, pessi, scont, recovery, totalpay, tdeduc, loan, eobi, socialsec, insurance, per_day from salary where staid = $staid");
		return $result->result_array();
	}

	public function fetchStaffQualification( $staid ) {

		$result = $this->db->query("select quali, grade, year, subject, institute from qualification where staid = $staid");
		return $result->result_array();
	}

	public function fetchStaffExperience( $staid ) {

		$result = $this->db->query("select `job`, `from`, `to`, `pd1` from `experience` where staid = $staid");
		return $result->result_array();
	}

	public function fetchStaffPhase( $staid ) {

		$result = $this->db->query("SELECT phases.`item_id`, phases.`phase_id`, phases.`rate`, phases.`cal_method`,i.item_des,p.name as phase_name,phases.perday as perday,phases.type as type
			FROM `staff_phases` phases
			inner join item i on i.item_id = phases.item_id
			left join phase p on p.id = phases.phase_id
			WHERE phases.staid =".$staid);
		return $result->result_array();
	}

    public function fetchStaffPhaseData( $employeeId ) {

        $result = $this->db->query("Select staid  from staff where pid =".$employeeId);
        if($result->num_rows() > 0){

            $result = $result->row_array();
            $result1 = $this->db->query("SELECT Distinct phases.`item_id`, i.uom, phases.`cal_method`,sd.stqty as stqty,i.item_des,i.grweight,sd.stweight as stweight 
				FROM `staff_phases` phases
				inner join item i on i.item_id = phases.item_id
				left join phase p on p.id = phases.phase_id
					LEFT JOIN (
									SELECT stockdetail.item_id, IFNULL(SUM(stockdetail.qty),0) stqty, IFNULL(SUM(stockdetail.weight),0) stweight
									FROM stockdetail
									INNER JOIN department ON department.did = stockdetail.godown_id
									inner join stockmain on stockmain.stid =  stockdetail.stid
									GROUP BY stockdetail.item_id) sd ON sd.item_id=i.item_id
				WHERE phases.staid =".$result['staid']);
            if($result1->num_rows() > 0){

                return $result1->result_array();
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

	public function fetchAllTeachers() {

		$result = $this->db->query("select `staid`, `name` from `staff` where type = 'teacher' and active = 1");
		return $result->result_array();
	}

	public function fetchStaffReportByStatus($status, $did) {

		$query = "";
		if ($status == '1' || $status == '0') {
			if ($did == "all")
				$query = "SELECT stf.staid, d.name AS 'dept_name', stf.name, stf.mobile, stf.phone, d.did, stf.address, stf.type, stf.active, stf.type, s.name AS 'shift_name', s.shid, s.tin, s.tout, d.name as 'dept_name', sal.designation, round(sal.totalpay, 2) totalpay FROM department AS d INNER JOIN staff AS stf ON d.did=stf.did INNER JOIN salary AS sal ON stf.staid=sal.staid INNER JOIN (allot_shift AS `as` INNER JOIN shift AS s ON s.shid = as.shid) ON stf.gid = as.gid WHERE stf.active = $status ORDER BY d.name, stf.type, stf.name";
			else
				$query = "SELECT stf.staid, d.name AS 'dept_name', stf.name, stf.mobile, stf.phone, d.did, stf.address, stf.type, stf.active, stf.type, s.name AS 'shift_name', s.shid, s.tin, s.tout, d.name as 'dept_name', sal.designation, round(sal.totalpay, 2) totalpay FROM department AS d INNER JOIN staff AS stf ON d.did=stf.did INNER JOIN salary AS sal ON stf.staid=sal.staid INNER JOIN (allot_shift AS `as` INNER JOIN shift AS s ON s.shid = as.shid) ON stf.gid = as.gid WHERE stf.active = $status AND d.did = $did ORDER BY d.name, stf.type, stf.name";
		} else {
			if ($did == "all")
				$query = "SELECT stf.staid, d.name AS 'dept_name', stf.name, stf.mobile, stf.phone, d.did, stf.address, stf.type, stf.active, stf.type, s.name AS 'shift_name', s.shid, s.tin, s.tout FROM department AS d INNER JOIN staff AS stf ON d.did=stf.did INNER JOIN (allot_shift AS `as` INNER JOIN shift AS s ON s.shid = as.shid) ON stf.gid = as.gid ORDER BY d.name, stf.type, stf.name";
			else
				$query = "SELECT stf.staid, d.name AS 'dept_name', stf.name, stf.mobile, stf.phone, d.did, stf.address, stf.type, stf.active, stf.type, s.name AS 'shift_name', s.shid, s.tin, s.tout FROM department AS d INNER JOIN staff AS stf ON d.did=stf.did INNER JOIN (allot_shift AS `as` INNER JOIN shift AS s ON s.shid = as.shid) ON stf.gid = as.gid WHERE stf.did = $did ORDER BY d.name, stf.type, stf.name";
		}

		$result = $this->db->query($query);
		if ($result->num_rows() == 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}

	public function fetchAll() {

		$result = $this->db->query("SELECT stf.staid, d.name AS 'dept_name', stf.name, stf.fname, stf.mobile, stf.phone, d.did, stf.pid, stf.address, stf.type, stf.active, stf.type, s.name AS 'shift_name', s.shid, sal.designation, ROUND(sal.bpay) bpay
			FROM department AS d
			INNER JOIN staff AS stf ON d.did=stf.did
			INNER JOIN salary AS sal ON stf.staid=sal.staid
			INNER JOIN (allot_shift AS `as`
			INNER JOIN shift AS s ON s.shid = as.shid) ON stf.gid = as.gid
			ORDER BY stf.name");
		if ($result->num_rows() == 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}

	public function fetchAllOvertime() {

		$result = $this->db->query("SELECT dcno, DATE(o.date) AS date, s.name, s.fname, o.approved_by, o.reason, o.remarks, o.othour FROM overtime AS o INNER JOIN staff AS s ON o.staid = s.staid");
		if ($result->num_rows() == 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}

	public function getSalary($from, $to, $did) {

        $result = "call salarysheet('". $from ."', '". $to ."','". $did ."')";
		$result = $this->db->query($result);
        mysqli_next_result($this->db->conn_id);

		if ($result->num_rows() == 0 ) {
			return false;
		} else {
            $data = array();
            $result = $result->result_array();
            foreach($result as $row)
            {
                $loadDeduction = $this->getLoanDeduction($row['pid']);
                $loanBalance = $this->getLoanBalance($from, $to, $row['pid']);
                $data[] = array(
                    "staid" => $row['staid'],
                    "did" => $row['did'],
                    "department_name" => $row['department_name'],
                    "salary_account_id" => $row['spid'],
                    "pid" => $row['pid'],
                    "shid" => $row['shid'],
                    "name" => $row['name'],
                    "designation" => $row['designation'],
                    "bsalary" => $row['bsalary'],
                    "absent" => $row['absent'],
                    "leave_wp" => $row['leave_wp'],
                    "leave_wop" => $row['leave_wop'],
                    "gusted_holiday" => $row['gusted_holiday'],
                    "outdoor" => $row['outdoor'],
                    "short_leave" => $row['short_leave'],
                    "rest_days" => $row['rest_days'],
                    "work_days" => $row['work_days'],
                    "paid_days" => $row['paid_days'],
                    "gross_salary" => $row['gross_salary'],
                    "otrate" => $row['otrate'],
                    "othour" => $row['othour'],
                    "overtime" => $row['overtime'],
                    "advance" => $row['advance'],
                    "loan_deduction" => $loadDeduction,
                    "balance" => $loanBalance,
                    "incentive" => $row['incentive'],
                    "penalty" => $row['penalty'],
                    "eobi" => $row['eobi'],
                    "insurance" => $row['insurance'],
                    "socialsec" => $row['socialsec'],
                    "net_salary" => $row['net_salary']
                );
            }
            return $data;
		}
	}

	public function getLoanDeduction($pid)
	{
		$res = $this->db->query("select deduction from pledger WHERE pid = '" . $pid . "' AND etype = 'loan' order by pledid desc limit 0, 1 ");
		$res = $res->result_array();
		if(isset($res[0]['deduction'])) {
			return ($res[0]['deduction'] != '') ? $res[0]['deduction'] : '0.00';
		}
		else{
			return "0.00";
		}
	}

	public function getLoanBalance($from, $to, $pid)
	{	
		$result = $this->db->query("SELECT SUM(debit) - (SELECT IFNULL(SUM(deduction), 0) FROM pledger WHERE etype = 'salary'
			AND pid = '" . $pid . "' AND date <= '" . str_replace('/', '-', $to) . "') as deduction
			FROM pledger
			WHERE pid = '" . $pid . "' AND etype = 'loan' AND date <= '" . str_replace('/', '-', $to) . "' ");
		$result = $result->result_array();
		$amout = ($result[0]['deduction'] != '')? $result[0]['deduction']:'0.00';
		$deduction = $this->getLoanDeduction($pid);
		$totalRemaining = "";
		if($amout < $deduction)
		{
			$totalRemaining = $amout;
		}
		else
		{
			$totalRemaining = $amout - $this->getLoanDeduction($pid);
		}
		return $totalRemaining;
	}

	public function deleteOvertime($dcno) {

		$this->db->where(array('dcno' => $dcno ));
		$result = $this->db->get('overtime');

		if ($result->num_rows() > 0) {
			$this->db->where(array('dcno' => $dcno ));
			$result = $this->db->delete('overtime');
		} else {
			return false;
		}
	}

	public function deleteSalarySheet($dcno,$etype) {

		$this->db->where(array('dcno' => $dcno,'etype'=>$etype));
		$result = $this->db->get('salarysheet');

		if ($result->num_rows() > 0) {
			$this->db->where(array('dcno' => $dcno ));
			$result = $this->db->delete('salarysheet');
		} else {
			return false;
		}
	}
	public  function fetchOvertimeReport($from, $to, $pid, $did)
	{
		$query = "SELECT o.dcno,o.date,o.staid,s.name as staff_name,d.name as dept_name,o.approved_by,o.reason,o.remarks,o.othour FROM overtime AS o INNER JOIN staff AS s ON s.staid = o.staid INNER JOIN department AS d ON d.did = o.did WHERE o.date >= '". $from ."' AND o.date <= '". $to ."'";
		if ($pid != '-1') {
			$query .= " AND o.staid = (select staid from staff where pid = '".$pid."' )";
		}
		if ($did != '-1') {
			$query .= " AND o.did = $did";
		}
		// die(print ($query));
		$result = $this->db->query($query);
		if ($result->num_rows() > 0)
		{
			return $result->result_array();
		}
		else
		{
			return false;
		}
	}

	public function getSalary_contract($from, $to, $did) {

        $result = "call salarysheetcontract('". $from ."', '". $to ."','". $did ."')";
		$result = $this->db->query($result);
        mysqli_next_result($this->db->conn_id);

		if ($result->num_rows() == 0 ) {
			return false;
		} else {
            $data = array();
            $result = $result->result_array();
            foreach($result as $row)
            {
                $loadDeduction = $this->getLoanDeduction($row['emp_id']);
                $loanBalance = $this->getLoanBalance($from,$to, $row['emp_id']);
                $data[] = array(
                    "staid" => $row['staid'],
                    "did" => $row['did'],
                    "department_name" => $row['department_name'],
                    "pid" => $row['emp_id'],
                    "name" => $row['name'],
                    "bsalary" => $row['basicsalary'],
                    "headqty" => $row['headqty'],
                    "headamount" => $row['headamount'],
					"sparepartqty" => $row['sparepartqty'],
                    "sparepartamount" => $row['sparepartamount'],
                    "presentdays" => $row['presentdays'],
                    "extradays" => $row['extradays'],
                    "totaldays" => $row['totaldays'],
                    "loan" => $row['loan'],
                    "deduction" => $row['deduction'],
                    "balance" => $row['balance'],
                    "incentive" => $row['incentive'],
                    "advance" => $row['advance'],
                    "netsalary" => $row['netsalary'],
                    "perdayproduction" => $row['perdayproduction'],
                    "per_day" => $row['per_day']

                );
            }
            return $data;
		}
	}
}

/* End of file staffs.php 
/* Location: ./application/models/staffs.php */