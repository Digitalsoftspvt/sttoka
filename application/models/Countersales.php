<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Countersales extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getMaxVrno($etype, $company_id)
    {   
        $result = $this->db->query("SELECT MAX(vrno) vrno FROM stockmain WHERE etype = '" . $etype . "' and company_id=$company_id AND DATE(vrdate) = DATE(NOW())");
        $row = $result->row_array();
        $maxId = $row['vrno'];

        return $maxId;
    }

    public function fetchAllFilterItems($categoryId, $subCategoryId, $brandId){

        $where = 'where item_id <> 0';
        if($categoryId){

            $where .= ' and catid ='.$categoryId;
        }
        if($subCategoryId){

            $where .= ' and subcatid ='.$subCategoryId;
        }
        if($brandId){

            $where .= ' and item.bid ='.$brandId;
        }

        $result = $this->db->query("SELECT item.item_id, item.item_des, item.uom, brand.name AS brand_name, item.grweight, item.size AS sale_price, is_selected
            FROM item
            INNER JOIN brand ON brand.bid = item.bid ".$where);

        return $result->result_array();
    }

    public function Validate_Order($etype, $company_id, $order_no, $status)
    {
        $result = $this->db->query("SELECT vrnoa FROM stockmain WHERE etype = '" . $etype . "' and company_id=$company_id AND vrnoa =$order_no  and status='" . $status . "' and vrnoa not in (select order_vrno from stockmain where order_vrno=$order_no and etype='order_parts') ");
        // $result = $this->db->query("SELECT max(vrnoa) vrnoa FROM stockmain WHERE etype = 'sale_order' and company_id=1 AND vrnoa =1  and status='running' ");
        $row = $result->row_array();
        if ($result->num_rows() > 0) {
            $chk_flag = $row['vrnoa'];
            return $chk_flag;
        } else {
            return false;
        }
    }

    public function Validate_Order_Loading($etype, $company_id, $order_no, $status)
    {
        $result = $this->db->query("SELECT vrnoa FROM stockmain WHERE etype = 'order_parts' and company_id=$company_id AND order_vrno =$order_no  ");
        $row = $result->row_array();
        if ($result->num_rows() > 0) {
            $chk_flag = $row['vrnoa'];
            return $chk_flag;
        } else {
            return false;
        }
    }

    public function fetchOrderReportData($startDate, $endDate, $what, $type, $company_id, $etype, $field, $crit, $orderBy, $groupBy, $name)
    {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT  $field as voucher,$name,dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username,stockdetail.amount,stockmain.vrdate, stockmain.remarks, stockmain.vrnoa, stockmain.remarks,  abs(stockdetail.qty) qty, stockdetail.weight, stockdetail.rate, stockdetail.netamount, item.item_des as 'item_des',item.uom FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.stid = stockdetail.stid INNER JOIN party party ON stockmain.party_id = party.pid              INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3 INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2 INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1 INNER JOIN department dept  ON stockdetail.godown_id = dept.did  INNER JOIN item item ON stockdetail.item_id = item.item_id INNER JOIN user user ON user.uid = stockmain.uid WHERE  stockmain.vrdate between '" . $startDate . "' AND '" . $endDate . "' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' $crit  order by $orderBy");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT  $field as voucher,$name,dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username,date(stockmain.vrdate) as DATE, stockdetail.amount, date(stockmain.VRDATE) VRDATE, stockmain.vrnoa, ABS(round(SUM(stockdetail.qty))) qty, round(SUM(stockdetail.weight)) weight, round(SUM(stockdetail.rate)) rate, round(sum(stockdetail.netamount)) netamount, stockmain.remarks FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.stid = stockdetail.stid INNER JOIN party party ON stockmain.party_id = party.pid              INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3 INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2 INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1 INNER JOIN department dept  ON stockdetail.godown_id = dept.did  INNER JOIN item item ON stockdetail.item_id = item.item_id INNER JOIN user user ON user.uid = stockmain.uid WHERE  stockmain.vrdate between '" . $startDate . "' AND '" . $endDate . "' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' $crit group by $groupBy order by $orderBy");
            return $query->result_array();
        }
    }

    public function fetchOrderReportDataParts($startDate, $endDate, $what, $type, $company_id, $etype, $field, $crit, $orderBy, $groupBy, $name)
    {
        if ($type == 'detailed') {
            $query = $this->db->query("SELECT  $field as voucher,$name, dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username,stockmain.vrdate,date(stockmain.vrdate) as date, stockmain.remarks, stockmain.vrnoa, stockmain.remarks, abs(stockdetail.qty) qty, stockdetail.weight, stockdetail.netamount, item.item_des as 'item_des',item.uom FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.stid = stockdetail.stid INNER JOIN party party ON stockmain.party_id = party.pid              INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3 INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2 INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1 INNER JOIN item item ON stockdetail.item_id = item.item_id INNER JOIN department dept  ON stockdetail.godown_id = dept.did INNER JOIN user user ON user.uid = stockmain.uid  WHERE  stockmain.vrdate between '" . $startDate . "' and '" . $endDate . "' and stockmain.company_id=$company_id and stockmain.etype='$etype' $crit  order by $orderBy");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT  $field as voucher,$name, dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username,date(stockmain.vrdate) as date, date(stockmain.vrdate) vrdate, stockmain.vrnoa, ABS(round(SUM(stockdetail.qty))) qty, round(SUM(stockdetail.weight)) weight, round(SUM(stockdetail.RATE)) RATE, round(sum(stockdetail.netamount)) netamount, stockmain.remarks FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.stid = stockdetail.stid INNER JOIN party party ON stockmain.party_id = party.pid              INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3 INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2 INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1 INNER JOIN item item ON stockdetail.item_id = item.item_id INNER JOIN user user ON user.uid = stockmain.uid  INNER JOIN department dept  ON stockdetail.godown_id = dept.did WHERE  stockmain.VRDATE between '" . $startDate . "' and '" . $endDate . "' and stockmain.company_id=$company_id and stockmain.etype='$etype' $crit group by $groupBy order by $orderBy");
            return $query->result_array();
        }
    }

    public function getMaxVrnoa($etype, $company_id)
    {
        $result = $this->db->query("SELECT MAX(vrnoa) vrnoa FROM stockmain WHERE etype = '" . $etype . "' and company_id=$company_id");
        $row = $result->row_array();
        $maxId = $row['vrnoa'];

        return $maxId;
    }
    
    public function saveRequest($data)
    {
        $this->db->trans_start(); # Starting Transaction
        
        $this->db->insert('counter_requst_log', $data);
        
        $this->db->trans_complete(); # Completing transaction

        if ($this->db->trans_status() === FALSE) {
            # Something went wrong.
            $this->db->trans_rollback();
            return FALSE;
        } 
        else {
            # Everything is Perfect. 
            # Committing data to the database.
            $this->db->trans_commit();
            return TRUE;
        }
        //return true;
    }

    public function save($stockmain, $stockdetail, $vrnoa, $etype)
    {
        ini_set('max_execution_time', 300);
        $companyId=1;
        $this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype ,'company_id'=>$stockmain['company_id']));
        $result = $this->db->get('stockmain');

        $stid = "";
        
        if ($result->num_rows() > 0 && $stockmain['voucher_type'] != 'new') {

            unset($stockmain['voucher_type']);
            $result = $result->row_array();
            $stid = $result['stid'];
            $this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype,'company_id'=>$stockmain['company_id']));
            $this->db->update('stockmain', $stockmain);

            $this->db->where(array('stid' => $stid));
            $this->db->delete('stockdetail');


        } else {
            
            unset($stockmain['voucher_type']);
            $this->db->trans_start(); # Starting Transaction
            $vrnoa = $this->getMaxVrnoa($etype , $stockmain['company_id']) + 1;
            $stockmain['vrnoa'] = $vrnoa;

            $this->db->insert('stockmain', $stockmain);
            $stid = $this->db->insert_id();
            $this->db->trans_complete(); # Completing transaction
        }

        foreach ($stockdetail as $detail) {
            $detail['stid'] = $stid;
            $this->db->insert('stockdetail', $detail);
        }


        if($etype == 'counter_sale')
        {

            $resSaleStockLess = $this->db->query("call sale_invoice_stock_less_stockmain('" . $stockdetail[0]['godown_id'] .  "', '" . $vrnoa . "', '" . $companyId . "','". $etype ."')");

            mysqli_next_result($this->db->conn_id);

           if((int)$resSaleStockLess->num_rows() > 0)
            {
                $resSaleStockLessArray = $resSaleStockLess->result_array();

               foreach($resSaleStockLessArray as $row)
                {

                    $qtyLess = $row['qty'];
                    if($qtyLess <> 0)
                    {
                        $this->db->query("insert into stockdetail(stid,item_id,qty,rate,amount,`type`,godown_id, weight, netamount)
                                       values
                                       ('" . $row['stid'] . "', '" . $row['item_id'] . "', '" . $qtyLess . "', '" . $row['lprate'] . "', '" . $row['amount'] . "', '" . $row['less'] . "', '" . $row['godown_id'] . "', '-". $row['weight'] ."', '0')");
                    }
                }
            }
        }
        

        if ($this->db->trans_status() === FALSE) {
            # Something went wrong.
            $this->db->trans_rollback();
            return FALSE;
        } 
        else {
            # Everything is Perfect. 
            # Committing data to the database.
            $this->db->trans_commit();
            return $vrnoa;
        }
        //return true;
    }

    public function fetchAllNotedBy()
    {
        $result = $this->db->query("SELECT DISTINCT noted_by FROM stockmain WHERE noted_by <> ''");
        return $result->result_array();
    }

    public function fetchAllApprovedBy()
    {
        $result = $this->db->query("SELECT DISTINCT approved_by FROM stockmain WHERE approved_by <> ''");
        return $result->result_array();
    }

    public function fetchAllreferBy()
    {
        $result = $this->db->query("SELECT DISTINCT order_by FROM stockmain WHERE order_by <> ''");
        return $result->result_array();
    }

    public function fetchAllPayment()
    {
        $result = $this->db->query("SELECT DISTINCT payment FROM stockmain WHERE payment <> ''");
        return $result->result_array();
    }

    public function fetch($vrnoa, $etype, $company_id)
    {
        $result = $this->db->query("SELECT m.customer_name,m.mobile customer_mobile,m.prepared_by, m.cash_receive,m.received_by,party.name party_name,m.vrno, m.vrnoa, m.order_vrno,
                                      m.transporter_id,m.discount,m.discp,m.tax,m.taxpercent,m.expense,m.exppercent,m.paid, m.uid,
                                      m.party_id, DATE(m.vrdate) vrdate, m.remarks, m.etype, m.namount,
                                      d.item_id, d.godown_id, ROUND(d.qty, 2) qty,d.rate,d.amount, ROUND(d.bundle, 2) bundle,
                                      ROUND(d.weight, 3) weight, d.type, i.item_des AS item_name, i.uom, dep.name AS dept_name ,
                                      rcp.item_id as finish_item,ifnull(rcp.rid,0) recipeid, m.cash_id
                                      FROM stockmain AS m INNER JOIN stockdetail AS d ON m.stid = d.stid
                                      INNER JOIN item i ON i.item_id = d.item_id
                                      left JOIN department dep ON dep.did = d.godown_id
                                      left JOIN party party on party.pid=m.party_id
                                      left join recipemain rcp on rcp.item_id=d.item_id
                                      WHERE m.vrnoa = $vrnoa AND m.etype = '" . $etype . "' and m.company_id=$company_id  order by d.type,d.stdid");

        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function last_5_srate($pid, $item_id, $company_id)
    {
        $result = $this->db->query("SELECT  m.vrnoa, DATE(m.vrdate) vrdate, ROUND(d.qty, 2) qty,d.rate, ROUND(d.weight, 2) as weight
                                    FROM stockmain AS m
                                    INNER JOIN stockdetail AS d ON m.stid = d.stid
                                    INNER JOIN item i ON i.item_id = d.item_id
                                    INNER JOIN department dep ON dep.did = d.godown_id
                                    INNER JOIN party party ON party.pid=m.party_id
                                    WHERE m.party_id = $pid AND m.etype = 'sale' AND m.company_id=$company_id and i.item_id=$item_id
                                    ORDER BY m.vrdate  desc
                                    limit 5");
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function fetch_order_stock($vrnoa, $etype, $company_id)
    {
        $result = $this->db->query("SELECT party.name party_name,m.vrno, m.vrnoa, m.order_vrno, m.transporter_id,m.discount,m.discp,m.tax,m.taxpercent,m.expense,m.exppercent,m.paid, m.uid, m.party_id, DATE(m.vrdate) vrdate, m.remarks, m.etype, m.noted_by, m.namount, m.pub_add, d.item_id, d.godown_id, ROUND(d.qty, 2) tot_qty, ROUND(d.weight, 2) tot_weight, ROUND(stk.qty, 2) qty, ROUND(stk.weight, 2) weight,d.rate,d.amount, ROUND(d.bundle, 2) bundle, ROUND(d.weight, 2) weight, d.type, i.item_des AS item_name, i.uom, dep.name AS dept_name , rcp.item_id as finish_item,ifnull(rcp.rid,0) recipeid FROM stockmain AS m INNER JOIN stockdetail AS d ON m.stid = d.stid INNER JOIN item i ON i.item_id = d.item_id INNER JOIN department dep ON dep.did = d.godown_id INNER JOIN party party on party.pid=m.party_id left join recipemain rcp on rcp.item_id=d.item_id
        inner join(SELECT ifnull(sum(d.qty),0) as qty,ifnull(sum(d.weight),0) as weight,i.item_id,i.item_des,d.type  FROM stockmain AS m INNER JOIN stockdetail AS d ON m.stid = d.stid INNER JOIN item i ON i.item_id = d.item_id INNER JOIN department dep ON dep.did = d.godown_id INNER JOIN party party on party.pid=m.party_id left join recipemain rcp on rcp.item_id=d.item_id WHERE m.order_vrno =$vrnoa and  m.etype in('sale','sale_order') and m.company_id=$company_id  group by i.item_id,d.`type`) as stk on stk.item_id=d.item_id and stk.`type`=d.`type`
        WHERE m.vrnoa =$vrnoa AND m.etype = '" . $etype . "' and m.company_id=$company_id
        order by d.type,d.odid");

        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function fetchPartsOrder($vrnoa, $etype, $company_id, $type)
    {
        if ($type == 'parts') {
            $result = $this->db->query("SELECT i.item_id,rcpd.qty rqty ,rcp.fqty,d.qty dqty, d.qty * rcpd.qty as qty, d.weight *(rcpd.weight/ifnull(rcp.fweight,1)) as weight, i.item_des AS item_name, i.uom, rcp.item_id as finish_item,ifnull(rcp.rid,0) recipeid FROM stockmain AS m
            INNER JOIN stockdetail AS d ON m.stid = d.stid  
            inner join recipemain rcp on rcp.item_id=d.item_id 
            inner join recipedetail rcpd on rcpd.rid=rcp.rid 
            INNER JOIN item i ON i.item_id = rcpd.item_id 
            WHERE m.vrnoa =$vrnoa AND m.etype = '" . $etype . "' and m.company_id=$company_id and d.`type`='add'
            ");
        } else if ($type == 'spare_parts') {
            $result = $this->db->query("SELECT party.name party_name,m.vrno, m.vrnoa, m.transporter_id,m.discount,m.discp,m.tax,m.taxpercent,m.expense,m.exppercent,m.paid, m.uid, m.party_id, DATE(m.vrdate) vrdate, m.remarks, m.etype, m.noted_by, m.namount, m.pub_add, d.item_id, d.godown_id, ROUND(d.qty, 2) qty,d.rate,d.amount, ROUND(d.bundle, 2) bundle, ROUND(d.weight, 2) weight, d.type, i.item_des AS item_name, i.uom, dep.name AS dept_name , rcp.item_id as finish_item,ifnull(rcp.rid,0) recipeid FROM stockmain AS m INNER JOIN stockdetail AS d ON m.stid = d.stid INNER JOIN item i ON i.item_id = d.item_id INNER JOIN department dep ON dep.did = d.godown_id INNER JOIN party party on party.pid=m.party_id left join recipemain rcp on rcp.item_id=d.item_id WHERE m.vrnoa = $vrnoa AND m.etype = '" . $etype . "' and m.company_id=$company_id and i.item_id not in (select item_id from recipemain)  and d.`type`='add'  order by d.type,d.odid");
        } else {
            $result = $this->db->query("SELECT party.name party_name,m.vrno, m.vrnoa, m.transporter_id,m.discount,m.discp,m.tax,m.taxpercent,m.expense,m.exppercent,m.paid, m.uid, m.party_id, DATE(m.vrdate) vrdate, m.remarks, m.etype, m.noted_by, m.namount, m.pub_add, d.item_id, d.godown_id, ROUND(d.qty, 2) qty,d.rate,d.amount, ROUND(d.bundle, 2) bundle, ROUND(d.weight, 2) weight, d.type, i.item_des AS item_name, i.uom, dep.name AS dept_name  FROM stockmain AS m INNER JOIN stockdetail AS d ON m.stid = d.stid INNER JOIN item i ON i.item_id = d.item_id INNER JOIN department dep ON dep.did = d.godown_id INNER JOIN party party on party.pid=m.party_id WHERE m.vrnoa = $vrnoa AND m.etype = '" . $etype . "' and m.company_id=$company_id   and d.`type`='less'  order by d.type,d.odid");
            // $result = $this->db->query("SELECT party.name party_name,m.vrno, m.vrnoa, m.transporter_id,m.discount,m.discp,m.tax,m.taxpercent,m.expense,m.exppercent,m.paid, m.uid, m.party_id, DATE(m.vrdate) vrdate, m.remarks, m.etype, m.noted_by, m.namount, m.pub_add, d.item_id, d.godown_id, ROUND(d.qty, 2) qty,d.rate,d.amount, ROUND(d.bundle, 2) bundle, ROUND(d.weight, 2) weight, d.type, i.item_des AS item_name, i.uom, dep.name AS dept_name , rcp.item_id as finish_item,ifnull(rcp.rid,0) recipeid FROM stockmain AS m INNER JOIN stockdetail AS d ON m.stid = d.stid INNER JOIN item i ON i.item_id = d.item_id INNER JOIN department dep ON dep.did = d.godown_id INNER JOIN party party on party.pid=m.party_id left join recipemain rcp on rcp.item_id=d.item_id WHERE m.vrnoa = 6 AND m.etype = 'sale_order' and m.company_id=1 and i.item_id not in (select item_id from recipemain)  and d.`type`='less'  order by d.type,d.odid");
        }
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function fetch_loading_Stock($order_no, $company_id)
    {
        $result = $this->db->query("select d.type,i.item_id,i.item_des item_name,ifnull(sum(d.qty),0) qty,ifnull(sum(d.weight),0) weight, ifnull(sd.stqty,0) stqty,ifnull(sd.stweight,0) stweight from stockdetail d
            inner join item i on i.item_id=d.item_id
            inner join stockmain m on m.stid=d.stid 
            left join  (select item_id,ifnull(sum(qty),0) stqty,ifnull(sum(weight),0) stweight from stockdetail group by item_id) sd on sd.item_id=i.item_id
            where m.order_vrno=$order_no and m.company_id=$company_id and m.etype in('order_parts','order_loading') and d.type<>'less'
            group by d.type,i.item_id,i.item_des having ifnull(sum(d.qty),0)<>0");

        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }


    public function fetch_parts($vrnoa, $etype, $company_id)
    {
        $result = $this->db->query("SELECT party.name party_name,m.vrno, m.vrnoa, m.transporter_id,m.discount,m.discp,m.tax,m.taxpercent,m.expense,m.exppercent,m.paid, m.uid, m.party_id, DATE(m.vrdate) vrdate, m.remarks, m.etype, m.noted_by, m.namount, m.pub_add, d.item_id, d.godown_id, ROUND(d.qty, 2) qty,d.rate,d.amount, ROUND(d.bundle, 2) bundle, ROUND(d.weight, 2) weight, d.type, i.item_des AS item_name, i.uom, dep.name AS dept_name FROM stockmain AS m INNER JOIN stockdetail AS d ON m.stid = d.stid INNER JOIN item i ON i.item_id = d.item_id INNER JOIN department dep ON dep.did = d.godown_id INNER JOIN party party on party.pid=m.party_id WHERE m.vrnoa = $vrnoa AND m.etype = '" . $etype . "' and m.company_id=$company_id  order by d.type,d.odid");
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function fetchPurchaseOrder($vrnoa, $etype)
    {

        $result = $this->db->query("SELECT m.vrno, m.vrnoa, m.vrdate, m.party_id, m.remarks, m.payment, m.order_by, m.noted_by, m.shade_card, m.approved_by, m.etype, ROUND(m.amount, 2) amount, ROUND(m.discountp, 2) discountp, ROUND(m.discount, 2) discount, ROUND(m.namount, 2) namount, d.item_id, d.godown_id, ROUND(d.qty, 2) item_qty, ROUND(d.rate, 2) item_rate, ROUND(d.amount, 2) item_amount, ROUND(d.gstrate, 2) item_gstrate, ROUND(d.gstamount, 2) item_gstamount, ROUND(d.netamount, 2) item_netamount, i.item_des item_name, ROUND(d.weight, 2) weight, d.type status FROM stockmain AS m INNER JOIN stockdetail AS d ON m.stid = d.stid INNER JOIN item i ON i.item_id = d.item_id WHERE m.vrnoa = $vrnoa AND etype = '" . $etype . "'");
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function fetchInwardGatePass($vrnoa, $etype)
    {
        $result = $this->db->query("SELECT m.vrno, m.vrnoa, m.vrdate, m.party_id, m.remarks, m.payment, m.order_by, m.noted_by, m.approved_by, m.etype, m.party_id_co, d.item_id, d.godown_id, ROUND(d.qty) qty, d.uom, dep.name AS 'dept_name', i.description AS 'item_name' FROM stockmain m INNER JOIN stockdetail d ON m.stid = d.stid LEFT JOIN department dep ON dep.did = d.godown_id INNER JOIN item i ON i.item_id = d.item_id WHERE m.vrnoa = $vrnoa AND etype = '" . $etype . "'");
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function delete($vrnoa, $etype, $company_id)
    {
        if ($etype == 'sale' || $etype == 'counter_sale') {
            $this->db->where(array('etype' => $etype, 'dcno' => $vrnoa, 'company_id' => $company_id));
            $result = $this->db->delete('pledger');
        }
        $this->db->where(array('etype' => $etype, 'vrnoa' => $vrnoa, 'company_id' => $company_id));
        $result = $this->db->get('stockmain');

        if ($result->num_rows() == 0) {
            return false;
        } else {

            $result = $result->row_array();
            $stid = $result['stid'];

            $this->db->where(array('etype' => $etype, 'vrnoa' => $vrnoa));
            $this->db->delete('stockmain');
            $this->db->where(array('stid' => $stid));
            $this->db->delete('stockdetail');

            return true;
        }
    }

    public function fetchCountersales($from, $to, $type)
    {
        if ($type == 'new') {
            $result = $this->db->query("SELECT stid,stockmain.status, vrnoa, date(vrdate) vrdate, party.name, city, party.cityarea, stockmain.remarks FROM stockmain INNER JOIN party ON party.pid = stockmain.party_id WHERE STATUS = 'new' AND stockmain.etype = 'sale_order'");
            return $result->result_array();
        } else if ($type == 'running') {
            $result = $this->db->query("SELECT stid,stockmain.status, vrnoa, date(vrdate) vrdate, party.name, city, party.cityarea, stockmain.remarks FROM stockmain INNER JOIN party ON party.pid = stockmain.party_id WHERE STATUS = 'running' AND vrnoa not in (select order_vrno from stockmain where etype='order_parts' ) AND stockmain.etype = 'sale_order' order by stockmain.vrnoa");
            return $result->result_array();
        } else if ($type == 'running_loading') {
            $result = $this->db->query("SELECT stid,stockmain.status, vrnoa, date(vrdate) vrdate, party.name, city, party.cityarea, stockmain.remarks FROM stockmain INNER JOIN party ON party.pid = stockmain.party_id WHERE STATUS = 'running' AND vrnoa  in (select order_vrno from stockmain where etype='order_parts' ) AND stockmain.etype = 'sale_order' order by stockmain.vrnoa");
            return $result->result_array();
        } else if ($type == 'sale') {
            $result = $this->db->query("SELECT stid,stockmain.status, vrnoa, date(vrdate) vrdate, party.name, city, party.cityarea, stockmain.remarks FROM stockmain INNER JOIN party ON party.pid = stockmain.party_id WHERE vrnoa not in (select order_vrno from stockmain where etype='sale' ) AND stockmain.etype = 'sale_order' order by stockmain.vrnoa");
            return $result->result_array();
        } else {
            $result = $this->db->query("SELECT stid,stockmain.status, vrnoa, date(vrdate) vrdate, party.name, city, party.cityarea, stockmain.remarks FROM stockmain INNER JOIN party ON party.pid = stockmain.party_id WHERE stockmain.etype = 'sale_order' order by stockmain.vrnoa");
            return $result->result_array();
        }
    }

    public function Loading_Stock($company_id, $order_no)
    {
        $result = $this->db->query("select type,item_id,ifnull(sum(qty),0) qty,ifnull(sum(weight),0) weight from stockdetail where stid in (select stid from stockmain where etype in('order_parts','order_loading') and order_vrno=$order_no ) group by type,item_id");
        return $result->result_array();
    }


    public function updateOrderStatus($stids)
    {
        foreach ($stids as $stid) {
            $this->db->query("UPDATE stockmain SET status = 'running' WHERE etype = 'sale_order' AND stid = $stid");
        }
        return true;
    }
}