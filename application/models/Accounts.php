<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accounts extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function getMaxId() {

		$this->db->select_max('pid');
		$result = $this->db->get('party');

		$row = $result->row_array();
		$maxId = $row['pid'];
		return $maxId;
	}

	public function fetchNetChequeSum( $etype, $company_id )
	{			
		$query= $this->db->query("SELECT IFNULL(SUM(amount), 0) as NETTOTAL FROM pd_cheque WHERE etype='$etype' AND post='unpost' AND company_id={$company_id}");
		$result = $query->result_array();

		return $result;
	}

	public function fetchdailytransactionsales($fromdate, $todate)
	{			
		$query= $this->db->query("SELECT 'Counter Sale' as 'Entrytype',g.name as 'Warehouse',round(sum(namount)) 'Amount_sum',0 as 'Remaining Bal_sum' from 
		stockmain m 
		inner join stockdetail d on m.stid = d.stid 
		inner join department g on g.did = d.godown_id
		where m.vrdate between '" .$fromdate. "' and '" .$todate. "' and m.etype in ('counter_sale')
		group by g.name,m.etype
		
		union all
		
		SELECT 'Credit Sale' as 'Entrytype',p.name as 'Account Titles',round(sum(namount)) 'Orders_sum',(select round(sum(debit) - sum(credit)) from pledger where date <= '" .$todate. "' and pid = p.pid) as 'Remaining Bal_sum' from 
		stockmain m 
		inner join stockdetail d on m.stid = d.stid 
		left join department g on g.did = d.godown_id
		inner join party p on p.pid = m.party_id
		where m.vrdate between '" .$fromdate. "' and '" .$todate. "' and m.etype in ('sale')
		group by p.name,m.etype order by Entrytype,Warehouse;");
		$result = $query->result_array();

		return $result;
	}

	public function fetchdailytransactionpurchases($fromdate, $todate)
	{			
		$query= $this->db->query("SELECT 'Purchase' as 'Entrytype',p.name as 'Account Titles',m.remarks as 'Remarks',round(sum(namount)) 'Amount_sum',(select round(sum(debit) - sum(credit)) from pledger where date <= '" .$todate. "' and pid = p.pid) as 'Remaining Bal_sum' from 
		stockmain m 
		inner join stockdetail d on m.stid = d.stid 
		left join department g on g.did = d.godown_id
		inner join party p on p.pid = m.party_id
		where m.vrdate between '" .$fromdate. "' and '" .$todate. "' and m.etype in ('purchase')
		group by p.name,m.etype having round(sum(namount)) <> 0 order by p.name;;");
		$result = $query->result_array();

		return $result;
	}

	public function fetchdailytransactionproduction($fromdate, $todate)
	{			
		$query= $this->db->query("SELECT 'Production' as 'Entrytype',i.item_des,g.name as 'Warehouse',round(sum(d.qty)) 'Quantity_sum',round(sum(weight)) as 'Weight_sum' from 
		stockmain m 
		inner join stockdetail d on m.stid = d.stid 
		inner join department g on g.did = d.godown_id
		inner join item i on i.item_id = d.item_id
		where m.vrdate between '" .$fromdate. "' and '" .$todate. "' and m.etype in ('head_production') and d.qty > 0
		group by g.name,m.etype,i.item_id;");
		$result = $query->result_array();

		return $result;
	}

	public function fetchdailytransactionexpense($fromdate, $todate)
	{			
		$query= $this->db->query("SELECT 'Expenses' as 'Entrytype',p.name as 'Account Titles',round(sum(debit)) 'Amount_sum' from 
		pledger l
		inner join party p on p.pid = l.pid
		inner join level3 l3 on p.level3 = l3.l3
		where l.date between '" .$fromdate. "' and '" .$todate. "' and l3.l3 in (select l3 from level3 where name like '%expense%')
		group by p.name having sum(debit) <> 0 order by p.name;");
		$result = $query->result_array();

		return $result;
	}

	public function fetchAllCashAndBd() { 

		$result = $this->db->query("SELECT `party`.pid, `party`.name, `party`.limit, `party`.account_id, `party`.mobile, `party`.address, `party`.level3, level3.name AS level3_name FROM `party` INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3` WHERE `level3`.name like '%cash and bd%' ");
		return $result->result_array();
	}

	public function checkAccountType($pid, $level3){

		$query= $this->db->query("SELECT level3
								FROM party
								WHERE pid = ".$pid." AND level3 = ".$level3);
		$result = $query->result_array();
		if($result){

			return false;
		}

		return true;
	}

	function fetchTrialBalanceData6($startDate, $endDate,$company_id ,$l1,$l2,$l3) 
	{

		$query = $this->db->query("call spw_trial_six('$startDate', '$endDate', $company_id,$l1,$l2,$l3)");
        mysqli_next_result($this->db->conn_id);
		return $query->result_array();
	}



	public function sendMessage( $mobile, $message )

	{

		$ptn = "/^[0-9]/";  // Regex

		$rpltxt = "92";  // Replacement string

		$mobile = preg_replace($ptn, $rpltxt, $mobile);



		// Create the SoapClient instance 

		$url = ZONG_API_SERVICE_URL; 

		// $soapClient = new SoapClient( $url);

		// var_dump($soapClient->__getFunctions());



		$post_string = '<?xml version="1.0" encoding="utf-8"?>'.

		'<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' .

		  '<soap:Body>'.

		    '<SendSingleSMS xmlns="http://tempuri.org/">'.

		      '<Src_nbr>' . ZONG_API_MOB . '</Src_nbr>'.

		      '<Password>' . ZONG_API_PASS . '</Password>'.

		      '<Dst_nbr>' . $mobile . '</Dst_nbr>'.

		      '<Mask>' . ZONG_API_MASK . '</Mask>'.

		      '<Message>' . $message . '</Message>'.

		    '</SendSingleSMS>'.

		  '</soap:Body>'.

		'</soap:Envelope>';



		$soap_do = curl_init(); 

		curl_setopt($soap_do, CURLOPT_URL,            $url );   

		curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10); 

		curl_setopt($soap_do, CURLOPT_TIMEOUT,        10); 

		curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );

		curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);  

		curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false); 

		curl_setopt($soap_do, CURLOPT_POST,           true ); 

		curl_setopt($soap_do, CURLOPT_POSTFIELDS,    $post_string); 

		curl_setopt($soap_do, CURLOPT_HTTPHEADER,     array('Content-Type: text/xml; charset=utf-8', 'Content-Length: '.strlen($post_string) ));



		$result = curl_exec($soap_do);

		$err = curl_error($soap_do);



		return $result;

	}
		

	public function getsetting_configur()
	{
		$query = "SELECT * FROM setting_configuration";
		$result = $this->db->query($query);
		return $result->result_array();
	}
	public function getMaxChequeId( $etype,$company_id )
	{
		$this->db->select_max('dcno');
		$query = $this->db->get_where('pd_cheque', array('etype' => $etype,'company_id'	=>	$company_id));

		$result = $query->row_array();
		return $result['dcno'];
	}

	public function getDistinctFields($field) {

		$result = $this->db->query("SELECT DISTINCT $field FROM `party`");
		return $result->result_array();
	}

	public function save( $accountDetail ) {
		$this->db->where(array(
								'pid' => $accountDetail['pid']
							));
		$result = $this->db->get('party');

		$affect = 0;
		$pid = "";
		if ($result->num_rows() > 0) {

			$this->db->where(array(
									'pid' => $accountDetail['pid']
								));
			$result = $this->db->update('party', $accountDetail);
			$affect = $this->db->affected_rows();

			$pid = $accountDetail['pid'];
		} else {	// if less than or equal to 0 then insert it

			unset($accountDetail['pid']);
			$result = $this->db->insert('party', $accountDetail);
			$affect = $this->db->affected_rows();

			$pid = $this->db->insert_id();
		}

		if ($affect === 0 && $pid == "") {
			return false;
		} else {
			return $pid;
		}
	}

	public function fetchAccount($pid) {

		$this->db->where(array(
								'pid' => $pid
							));
		$result = $this->db->get('party');

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchAccountByName($name) {

		$this->db->where(array(
								'name' => $name
							));
		$result = $this->db->get('party');

		if ( $result->num_rows() > 0 ) {
			return $result->row_array();
		} else {
			return false;
		}
	}


	public function fetchmultiaccountdata($pid){

		$result = $this->db->query("SELECT * from party where pid IN(".$pid.")");
			if ( $result->num_rows() > 0 ) {
				return $result->result_array();
			}
			else {
				return false;
			}
	}
	public function fetchAll($activee=-1,$typee="all") {
		$crit="";
		if ($typee=='cashslip'){
			$crit=" and level2.name in ('CASH & BD','TRADE DEBITORS/RECEIVABLES','PURCHASES') and level3.name not in ('BD')";		
		}
		else if ($typee=='sale order' || $typee=='sale' || $typee=='sale return' || $typee=='order_parts'){
			$crit=" and level2.name in ('TRADE DEBITORS/RECEIVABLES','customers')";		
		
		}else if ($typee=='purchase order' || $typee=='purchase return' || $typee=='purchasecontract'){
			$crit=" and level3.name like '%purchases%' or level3.name like '%creditors%' or level3.name like '%manufacturing expenses 1%'";
		}else if ($typee=='purchase'){
            $crit=" and level3.name like '%purchases%' or level3.name like '%creditors%' or level3.name like '%manufacturing expenses 1%'";
        }else if ($typee=='cashslip_bank'){
			$crit=" and level3.name in ('BD-OFFICE NEW')";		
		}else if ($typee=='cashslip_cash' || $typee == 'countersale'){
		
			/*$crit=" and (level3.name in ('CASH & BD') || party.name in ('HSAC'))";	*/	
			$crit=" and (level3.name like 'cash in hand%')";		
		}else if ($typee=='fields'){
			$crit=" and (level1.name in ('assets') or level3.name like '%purchases%' or level3.name like '%creditors%' )";		
 		}else if ($typee=='cashslip'){
			$crit="";		
		}else if ($typee=='salereturn_expense'){
            $crit=" and level1.name like '%expenses%'";
        }else if ($typee=='loan'){
            $crit=" and (level1.name like '%expenses%' || level3.name like '%cash in hand%')";
        }else if ($typee == 'advance'){
            //$crit=" and (level3.name like '%BD%' || level3.name like '%CASH and BD%')";
            	$crit=" and (level3.name like 'cash in hand%')";
        }else if ($typee=='loan1'){
            $crit=" and (level3.name LIKE '%asset employee%' OR level3.name LIKE '%assets%')";
        }else if ($typee == 'advance1'){
            $crit=" and (level3.name LIKE '%asset employee%')";
        }else if ($typee=='salary'){
            $crit=" and level3.name in ('Advance Employee','Loan Employee','Asset Employees','EMPLOYEE','Salary & Wages')";
        }
        else if ($typee!='all' && $typee!='ledger'){
			$crit=" and level3.name in ('debitors','creditors','customers','suppliers')";		
		}



        $level2Access = $this->getLevel2Access();
        $level2 = "";

        /*if($level2Access != "" and $typee!="sale order" and $typee!="cashslip" and $typee!="cashslip_cash")*/
        //if($level2Access != "" and $typee!="sale order")
        if($level2Access != "" && ($typee=="cashslip_cash"  || $typee == 'countersale' || $typee == 'ledger'))
        {
            $level2 = " and level3.l3 in (" . $level2Access . ")";
        }
        $qry='';
		if ($activee==-1){
			$qry ="SELECT  ifnull(bal.pbalance,0)pbalance,
                                        `party`.pid, `party`.name, party.city,party.country,`party`.uname, `party`.limit,
                                        `party`.account_id, `party`.mobile, `party`.address,`party`.uaddress, `party`.level3,
                                        level3.name AS level3_name
                                        FROM `party`
                                        INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3`
                                        INNER JOIN level2 ON level2.l2 = level3.l2
                                        INNER JOIN level1 ON level1.l1 = level2.l1
										left join (SELECT pid,SUM(debit) - SUM(credit) as pbalance
										FROM pledger
										group by pid ) as bal on bal.pid=party.pid
                                        where party.name <> '' " . $crit . " " . $level2 . " ";

			$result = $this->db->query($qry);
		}else{
			$qry="SELECT ifnull(bal.pbalance,0)pbalance,
                                        `party`.pid, `party`.name, party.city,party.country,`party`.uname,`party`.limit,
                                        `party`.account_id, `party`.mobile, `party`.address,`party`.uaddress, `party`.level3,
                                        level3.name AS level3_name
                                        FROM `party`
                                        INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3`
                                        INNER JOIN level2 ON level2.l2 = level3.l2
                                        INNER JOIN level1 ON level1.l1 = level2.l1
										left join (SELECT pid,SUM(debit) - SUM(credit) as pbalance
										FROM pledger
										group by pid ) as bal on bal.pid=party.pid
                                        WHERE `party`.`active` = " . $activee . "  " . $crit . " " . $level2 . "";
			$result = $this->db->query($qry);
		}
		// echo $qry;
		return $result->result_array();
	}
	
	public function fetchAllAccountWithUserRights($uid) {
		$crit="";
		if ($typee!='all'){
			$crit=" and level3.name in ('debitors','creditors','customers','suppliers')";		
		}

        $level2Access = $this->getLevel2Access();
        $level2 = "";
        if($level2Access != "")
        {
            $level2 = " and level3.l2 in (" . $level2Access . ")";
        }

		if ($activee==-1){
			$result = $this->db->query("SELECT ifnull(bal.balance,0) as pbalance,
                                        `party`.pid, `party`.name, party.city,party.country,`party`.uname, `party`.limit,
                                        `party`.account_id, `party`.mobile, `party`.address,`party`.uaddress, `party`.level3,
                                        level3.name AS level3_name
                                        FROM `party`
                                        INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3`
                                        INNER JOIN level2 ON level2.l2 = level3.l2
										left join (
										SELECT SUM(debit) - SUM(credit) as balance,pid
										FROM pledger
										group by pid) as bal on `party`.pid = bal.pid
                                        where party.name <> '' " . $crit . " " . $level2 . " ");
		}else{
			$result = $this->db->query("SELECT ifnull(bal.balance,0) as pbalance,
                                        `party`.pid, `party`.name, party.city,party.country,`party`.uname,`party`.limit,
                                        `party`.account_id, `party`.mobile, `party`.address,`party`.uaddress, `party`.level3,
                                        level3.name AS level3_name
                                        FROM `party`
                                        INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3`
                                        INNER JOIN level2 ON level2.l2 = level3.l2
										left join (
										SELECT SUM(debit) - SUM(credit) as balance,pid
										FROM pledger
										group by pid) as bal on `party`.pid = bal.pid
                                        WHERE `party`.`active` = " . $activee . "  " . $crit . " " . $level2 . " ");
		}
		
		return $result->result_array();
	}


	public function fetchAllEmployee(){

		$result = $this->db->query("SELECT (
									SELECT SUM(debit) - SUM(credit)
									FROM pledger
									WHERE pid = party.pid) AS pbalance,
									 `party`.pid, `party`.name, party.city,party.country,`party`.uname,`party`.limit,
									 `party`.account_id, `party`.mobile, `party`.address,`party`.uaddress, `party`.level3,
									 level3.name AS level3_name, salary.per_day AS per_day_rate
									FROM `party`
									INNER JOIN staff ON staff.pid = party.pid
									INNER JOIN salary ON salary.staid = staff.staid
									INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3`
									INNER JOIN level2 ON level2.l2 = level3.l2
									WHERE `party`.etype='staff'");

		return $result->result_array();
	}

	public function fetchAllEmployeeByDepAndGodown($crit){

		$result = $this->db->query("SELECT (
									SELECT SUM(debit) - SUM(credit)
									FROM pledger
									WHERE pid = party.pid) AS pbalance,
									 `party`.pid, `party`.name, party.city,party.country,`party`.uname,`party`.limit,
									 `party`.account_id, `party`.mobile, `party`.address,`party`.uaddress, `party`.level3,
									 level3.name AS level3_name, salary.per_day AS per_day_rate
									FROM `party`
									INNER JOIN staff ON staff.pid = party.pid
									INNER JOIN salary ON salary.staid = staff.staid
									INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3`
									INNER JOIN level2 ON level2.l2 = level3.l2
									WHERE `party`.etype='staff' $crit");

		return $result->result_array();
	}

	public function fetchAllAssetEmployee(){

		$result = $this->db->query("SELECT (
									SELECT SUM(debit) - SUM(credit)
									FROM pledger
									WHERE pid = party.pid) AS pbalance,
									 `party`.pid, `party`.name, party.city,party.country,`party`.uname,`party`.limit,
									 `party`.account_id, `party`.mobile, `party`.address,`party`.uaddress, `party`.level3,
									 level3.name AS level3_name
									FROM `party`
									INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3`
									INNER JOIN level2 ON level2.l2 = level3.l2
									WHERE `party`.etype='staff' and level3.name='ASSET EMPLOYEES'");

		return $result->result_array();
	}

	public function fetchAllStaff(){

		$result = $this->db->query("SELECT ifnull(bal.balance,0) as pbalance,
                                    `party`.pid, `party`.name, party.city,party.country,`party`.uname, `party`.limit,
                                    `party`.account_id, `party`.mobile, `party`.address,`party`.uaddress, `party`.level3,
                                    level3.name AS level3_name
                                    FROM `party`
                                    INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3`
                                    INNER JOIN level2 ON level2.l2 = level3.l2
									left join (
									SELECT SUM(debit) - SUM(credit) as balance,pid
									FROM pledger
									group by pid) as bal on `party`.pid = bal.pid
                                    where party.name <> '' and level3.name='Employee' ");
	
		return $result->result_array();
	}

	public function fetchAllBank(){

		$result = $this->db->query("SELECT ifnull(bal.balance,0) as pbalance,
                                    `party`.pid, `party`.name, party.city,party.country,`party`.uname, `party`.limit,
                                    `party`.account_id, `party`.mobile, `party`.address,`party`.uaddress, `party`.level3,
                                    level3.name AS level3_name
                                    FROM `party`
                                    INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3`
                                    INNER JOIN level2 ON level2.l2 = level3.l2
									left join (
									SELECT SUM(debit) - SUM(credit) as balance,pid
									FROM pledger
									group by pid) as bal on `party`.pid = bal.pid
                                    where party.name <> '' and level3.name='CASH AT BANK' ");
	
		return $result->result_array();
	}

    public function getLevel2Access()
    {
        $result = $this->db->query("select * from user_level2_rights where uid = '" . $this->session->userdata('uid') . "'");
        $result = $result->result_array();
        $l2Id = "";
        foreach($result as $row)
        {
            $l2Id .= $row['l2_id'] . ',';
        }
        $l2Id = rtrim($l2Id, ',');
        return $l2Id;
    }

	public function fetchAll_Users() {

		$result = $this->db->query("select * from user");
		return $result->result_array();
	}

	public function fetchAll_CashAccount() { 

		$result = $this->db->query("SELECT `party`.pid, `party`.name, `party`.limit, `party`.account_id, `party`.mobile, `party`.address, `party`.level3, level3.name AS level3_name
			FROM `party`
			INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3`
			WHERE `party`.name LIKE '%cash%' and party.pid in (SELECT pid FROM user_level2_rights INNER JOIN party ON party.level3 = l2_id WHERE user_level2_rights.uid = ".$this->session->userdata('uid').") ");
		return $result->result_array();
	}
	public function fetchAll_ExpAccount() { 

		$result = $this->db->query("SELECT `party`.pid, `party`.name, `party`.limit, `party`.account_id, `party`.mobile, `party`.address, `party`.level3, level3.name AS level3_name FROM `party` INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3` WHERE `level3`.name like '%expense%' ");
		return $result->result_array();
	}

	public function fetchAll_ExpAccountByLevelOne() { 

		$result = $this->db->query("SELECT ifnull(bal.balance,0) AS pbalance, `party`.pid, `party`.name, `party`.limit, `party`.account_id, `party`.mobile, `party`.address, `party`.level3, level1.name AS level3_name,`party`.uaddress, `party`.level3,level3.name AS level3_name,`party`.uname
									FROM `party`
									INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3`
									INNER JOIN `level2` ON `level2`.`l2` = `level3`.`l2`
									INNER JOIN `level1` ON `level1`.`l1` = `level2`.`l1`
									left join (
									SELECT SUM(debit) - SUM(credit) as balance,pid
									FROM pledger
									group by pid) as bal on `party`.pid = bal.pid
									WHERE `level1`.name LIKE '%expense%' ");
									return $result->result_array();
	}

	public function fetchAll_EmployeeAccount() { 

		$result = $this->db->query("SELECT `party`.pid, `party`.name, `party`.limit, `party`.account_id, `party`.mobile, `party`.address, `party`.level3, level3.name AS level3_name FROM `party` INNER JOIN `level3` ON `level3`.`l3` = `party`.`level3` WHERE `level3`.name like '%employee%' ");
		return $result->result_array();
	}

	public function getAllParties($etype) {

		if ($etype === '') {

			$result = $this->db->query("SELECT `name`, `pid` FROM `party`");
			return $result->result_array();
		} else {

			$this->db->where(array(
								'etype' => $etype
							));
			$result = $this->db->get('party');
			return $result->result_array();
		}
	}

	public function fetchBanks() {
		$query = 'SELECT DISTINCT bank_name FROM pd_cheque ORDER BY bank_name';
		$result = $this->db->query( $query );

		return $result->result_array();
	}

	public function saveCheque( $data )
	{
		$this->db->where(array(
			'dcno' => $data['dcno'],
			'etype' => $data['etype']
		));
		$q = $this->db->get('pd_cheque');

		if ( $q->num_rows() > 0 ) {
			$this->db->where(array(
				'dcno' => $data['dcno'],
				'etype' => $data['etype']
			));
			$this->db->update('pd_cheque',$data);
			$rowCount = $this->db->affected_rows();

			if ( $rowCount !== 0 ) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			$this->db->insert('pd_cheque',$data);
			$rowCount = $this->db->affected_rows();
			if ( $rowCount !== 0 ) {
				return true;
			}
			else {
				return false;
			}
		}
	}

public function fetchCheques($etype, $date, $company_id)
	{
		$query = "SELECT pd_cheque.DCNO, pd_cheque.VRDATE, pd_cheque.CHEQUE_NO, pd_cheque.MATURE_DATE, party.NAME 'PARTY', pd_cheque.BANK_NAME 'BANK', pd_cheque.AMOUNT FROM pd_cheque INNER JOIN party on pd_cheque.party_id_cr = party.pid WHERE pd_cheque.etype='{$etype}' AND pd_cheque.vrdate <= '{$date}' AND pd_cheque.post='unpost' AND pd_cheque.company_id={$company_id} ORDER BY pd_cheque.DCNO, pd_cheque.VRDATE";
		$result = $this->db->query($query);

		return $result->result_array();
	}


	public function fetchChequeVoucher( $dcno, $etype,$company_id )
	{
		$query = $this->db->query("select 	pd_cheque.*, DATE(pd_cheque.cheque_date) as cheque_date, DATE(pd_cheque.vrdate) as vrdate, DATE(pd_cheque.mature_date) as mature_date,  p1.name as partyName, p2.name as partyName2, u.uname as user_name,c.company_name from pd_cheque INNER JOIN party as p1 ON pd_cheque.party_id = p1.pid INNER JOIN party as p2 ON pd_cheque.party_id_cr = p2.pid inner join user as u on pd_cheque.uid=u.uid inner join company c on c.company_id=pd_cheque.company_id where pd_cheque.etype='{$etype}' AND pd_cheque.company_id ={$company_id} AND pd_cheque.dcno={$dcno}");	
		return $query->result_array();
		// $query = $this->db->get_where('pd_cheque', array(
		// 	'etype' => $etype,
		// 	'dcno' => $dcno
		// ));

		// if ($query->num_rows() > 0) {
		// 	return $query->row_array();
		// }else {
		// 	return false;
		// }

	}

	public function removeChequeVoucher( $dcno, $etype ,$company_id )
	{
		$this->db->where(array(
			'etype' => $etype,
			'dcno' => $dcno,
			'company_id' => $company_id
		));

		$this->db->delete('pd_cheque');

		if ($this->db->affected_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function getAllCheques($startDate, $endDate, $etype)
	{
		$query = "SELECT pd_cheque.DCNO, pcr.name 'ACCOUNT', p.name 'PARTY_NAME', BANK_NAME, CHEQUE_NO, CHEQUE_DATE, AMOUNT, STATUS, POST, VRDATE FROM pd_cheque INNER JOIN party AS pcr ON pd_cheque.party_id_cr = pcr.pid LEFT JOIN party AS p ON pd_cheque.party_id = p.pid WHERE pd_cheque.etype='{$etype}' AND VRDATE BETWEEN '{$startDate}' AND '{$endDate}'";
		$result = $this->db->query($query);

		return $result->result_array();
	}

	public function fetchPartyOpeningBalance( $to, $party_id )
	{
		$query = "SELECT IFNULL(SUM(DEBIT), 0)- IFNULL(SUM(CREDIT),0) AS 'OPENING_BALANCE' FROM pledger WHERE pledger.pid={$party_id} AND date < '{$to}'";
		$result = $this->db->query($query);
		return $result->result_array();
	}

	public function fetchRunningTotal($endDate, $party_id)
	{
		$query = "SELECT SUM(DEBIT) - SUM(CREDIT) 'RTotal' FROM pledger WHERE DATE(DATE) <= '$endDate' AND pid =$party_id";
		$result = $this->db->query($query);
		return $result->result_array();
	}

	public function fetchTurnOver($fromDate, $endDate, $party_id)
	{
		$query = "SELECT (
				SELECT IFNULL(SUM(ABS(debit)),0)
				FROM pledger
				WHERE etype = 'sale' AND pid = ".$party_id." AND (DATE(DATE) >= '$fromDate' AND DATE(DATE) <= '$endDate')) AS sale,(
				SELECT IFNULL(SUM(credit),0)
				FROM pledger
				WHERE etype = 'salereturn' AND pid = ".$party_id." AND (DATE(DATE) >= '$fromDate' AND DATE(DATE) <= '$endDate')) AS salereturn";
		$result = $this->db->query($query);
		return $result->result_array();
	}

	function fetchTrialBalanceData($startDate, $endDate,$company_id) 
	{

		$query = $this->db->query("call Trial_Balance('$startDate', '$endDate', $company_id)");
        mysqli_next_result($this->db->conn_id);
		return $query->result_array();
	}
	// function fetchTrialBalanceData6($startDate, $endDate,$company_id) 
	// {

	// 	$query = $this->db->query("call spw_trial_six('$startDate', '$endDate', $company_id)");
	// 	return $query->result_array();
	// }

	function Account_Flow($startDate, $endDate, $party_id ,$company_id) 
	{

		$query = $this->db->query("call spw_account_flow('$startDate', '$endDate', $party_id, $company_id)");
        mysqli_next_result($this->db->conn_id);
		return $query->result_array();
	}


	public function getChartOfAccounts($status , $what , $crit)
	{
		$query = $this->db->query("SELECT party.ACCOUNT_ID, party.name AS 'PARTY_NAME', party.uname as UNAME, party.uaddress as UADDRESS, party.mobile AS MOB, party.level3, level1.l1, level1.name AS 'L1NAME', level2.l2, level2.name AS 'L2NAME', level3.l3, level3.name AS 'L3NAME',party.level3 as TYPE,party.active as STATUS
									FROM party party
									INNER JOIN level3 level3 ON party.level3 = level3.l3
									INNER JOIN level2 level2 ON level3.l2 = level2.l2
									INNER JOIN level1 level1 ON level1.l1 = level2.l1
									where $status $crit
									group by $what
									ORDER BY account_id");
		return $query->result_array();
	}

	public function getChequeReportData($startDate, $endDate, $etype,$company_id)
	{
		$query = "SELECT pd_cheque.DCNO, pcr.name 'ACCOUNT', party.name 'PARTY_NAME', BANK_NAME, CHEQUE_NO, CHEQUE_DATE, AMOUNT, STATUS,POST,VRDATE FROM (SELECT * FROM party party) pcr INNER JOIN pd_cheque pd_cheque ON pcr.pid =pd_cheque.party_id_cr INNER JOIN party party ON party.pid=pd_cheque.party_id AND pd_cheque.etype='{$etype}' AND vrdate BETWEEN '{$startDate}' AND '{$endDate}' AND pd_cheque.company_id=$company_id ORDER BY vrdate";
		$result = $this->db->query($query);

		return $result->result_array();
	}

	public function fetchPayRecvReportData($startDate, $endDate, $etype,$company_id)
	{
		if ($etype === 'payable') {
			$query = "SELECT IFNULL(SUM(L.DEBIT),0)- IFNULL(SUM(L.CREDIT),0) 'BALANCE', P.NAME 'ACCOUNT_NAME', P.MOBILE, P.PHONE AS PHONE_OFF, P.ADDRESS, P.EMAIL FROM pledger L INNER JOIN party P ON L.pid = P.pid INNER JOIN level3 l3 ON l3.l3 = P.level3 WHERE L.DATE BETWEEN '$startDate' AND '$endDate' AND l3.name='CREDITORS' AND L.company_id=$company_id GROUP BY P.NAME HAVING IFNULL(SUM(L.DEBIT),0)- IFNULL(SUM(L.CREDIT),0)<>0";
			$query = $this->db->query($query);
			return $query->result_array();
		} else if ( $etype === 'receiveable') {
			$query = "SELECT IFNULL(SUM(L.DEBIT),0)- IFNULL(SUM(L.CREDIT),0) 'BALANCE', P.NAME 'ACCOUNT_NAME', P.MOBILE, P.PHONE AS PHONE_OFF, P.ADDRESS, P.EMAIL FROM pledger L INNER JOIN party P ON L.pid = P.pid INNER JOIN level3 l3 ON l3.l3 = P.level3 WHERE L.DATE BETWEEN '$startDate' AND '$endDate' AND l3.name='DEBITORS' AND L.company_id=$company_id GROUP BY  P.NAME HAVING IFNULL(SUM(L.DEBIT),0)-IFNULL(SUM(L.CREDIT),0)<>0";
			$query = $this->db->query($query);
			return $query->result_array();
		}
	}

	public function fetchClosingBalance( $to )
	{
		$query = "SELECT IFNULL(SUM(DEBIT), 0)-IFNULL(SUM(CREDIT), 0) as 'CLOSING_BALANCE' FROM pledger pledger WHERE pledger.pid=(SELECT pid FROM party party WHERE name='cash') AND date <= '{$to}'";
		$result = $this->db->query($query);
		return $result->result_array();
	}

	public function fetchOpeningBalance( $to )
	{
		$query = "SELECT IFNULL(SUM(DEBIT), 0)-IFNULL(SUM(CREDIT),0) as 'OPENING_BALANCE' FROM pledger pledger WHERE pledger.pid=(SELECT pid FROM party party WHERE name='cash') AND date < '{$to}'";
		$result = $this->db->query($query);
		return $result->result_array();
	}

	public function fetchOpeningBalance_Accounts( $from,$pid,$company_id )
	{
		$query = "SELECT IFNULL(SUM(DEBIT), 0)-IFNULL(SUM(CREDIT),0) as 'OPENING_BALANCE' FROM pledger pledger WHERE pledger.pid=$pid AND date < '{$from}' ";
		$result = $this->db->query($query);
		return $result->result_array();
	}


	public function fetchDayBookReportData ($startDate, $endDate, $what, $etype,$company_id)
	{
		// if ($what == 'date') {
		// 	$query = "SELECT pledger.DCNO AS `VRNOA`,party.NAME AS `PARTY`,pledger.DEBIT AS `DEBIT`, pledger.DESCRIPTION AS `REMARKS`, pledger.CREDIT AS `CREDIT`, pledger.DATE, pledger.ETYPE AS `ETYPE` FROM pledger pledger INNER JOIN party party ON pledger.pid = party.pid WHERE pledger.date BETWEEN '$startDate' AND '$endDate' ORDER BY pledger.date";
		// 	$result = $this->db->query($query);
		// 	return $result->result_array();
		// }
		// else if ($what == 'invoice') {
		// 	$query = "SELECT pledger.DCNO AS `VRNOA`,party.NAME AS `PARTY`,pledger.DEBIT AS `DEBIT`, pledger.DESCRIPTION AS `REMARKS`, pledger.CREDIT AS `CREDIT`, pledger.DATE, pledger.ETYPE AS `ETYPE` FROM pledger pledger INNER JOIN party party ON pledger.pid = party.pid WHERE pledger.date BETWEEN '$startDate' AND '$endDate' ORDER BY pledger.DCNO";
		// 	$result = $this->db->query($query);
		// 	return $result->result_array();
		// }
		// else if ($what == 'party') {
		// 	$query = "SELECT pledger.DCNO AS `VRNOA`,party.NAME AS `PARTY`,pledger.DEBIT AS `DEBIT`, pledger.DESCRIPTION AS `REMARKS`, pledger.CREDIT AS `CREDIT`, pledger.DATE, pledger.ETYPE AS `ETYPE` FROM pledger pledger INNER JOIN party party ON pledger.pid = party.pid WHERE pledger.date BETWEEN '$startDate' AND '$endDate' ORDER BY pledger.pid";
		// 	$result = $this->db->query($query);
		// 	return $result->result_array();
		// }

		$ord='';
		if ($what == 'date') {
				$ord='pledger.date';
		}
		else if ($what == 'invoice') {
				$ord='pledger.dcno';
		}
		else if ($what == 'party') {
			$ord='party.name';		
		}
		else if ($what == 'user') {
				$ord='user.uname';
		}

		// $query = "SELECT pledger.ETYPE, pledger.DCNO AS VRNOA,party.NAME AS PARTY,pledger.DEBIT AS 'AMOUNT',pledger.DESCRIPTION AS REMARKS, pledger.DATE FROM pledger pledger INNER JOIN party party ON pledger.pid = party.pid WHERE pledger.DEBIT <> 0 AND pledger.ETYPE='CPV' AND pledger.date BETWEEN '$startDate' AND '$endDate' ORDER BY party.NAME, pledger.DCNO";
		$query = "SELECT pledger.ETYPE, pledger.DCNO AS VRNOA,party.NAME AS PARTY,pledger.DEBIT as DEBIT ,pledger.credit  as 'CREDIT' ,pledger.DESCRIPTION AS REMARKS, pledger.DATE, user.uname, company.company_name  FROM pledger pledger INNER JOIN party party ON pledger.pid = party.pid inner join user as user on user.uid=pledger.uid inner join company on company.company_id=pledger.company_id   WHERE  pledger.date BETWEEN '$startDate' AND '$endDate'  and pledger.company_id=$company_id  ORDER BY  $ord  ,pledger.date";
		// $query = "SELECT pledger.ETYPE, pledger.DCNO AS VRNOA,party.NAME AS PARTY,(case when (pledger.etype='cpv') then pledger.DEBIT else pledger.credit END) as 'AMOUNT' ,pledger.DESCRIPTION AS REMARKS, pledger.DATE, user.uname, company.company_name  FROM pledger pledger INNER JOIN party party ON pledger.pid = party.pid inner join user as user on user.uid=pledger.uid inner join company on company.company_id=pledger.company_id   WHERE (case when (pledger.etype='cpv') then pledger.DEBIT<>0 else pledger.credit<>0 END)    ORDER BY  pledger.date";
		$result = $this->db->query($query);
		return $result->result_array();

	}

	public function fetchExpenseReportData ($startDate, $endDate, $what, $etype,$company_id)
	{

	// if ($what == 'date') {
	// 	$query = "SELECT pledger.ETYPE, pledger.DCNO AS `VRNOA`, pledger.DATE, party.name AS `PARTY`, pledger.DESCRIPTION AS `REMARKS`, pledger.DEBIT AS `DEBIT` FROM pledger pledger INNER JOIN party party ON pledger.pid = party.pid WHERE pledger.pid IN (SELECT pid AS PARTY_ID FROM party party INNER JOIN level3 level3 ON party.level3=level3.l3 INNER JOIN level2 level2 ON level2.l2 = level3.l2 INNER JOIN level1 level1 ON level2.l1 = level1.l1 WHERE level1.NAME IN ('expense', 'expenses')) AND party.name NOT IN ('sale return', 'purchase', 'purchase import') AND pledger.date BETWEEN '$startDate' AND '$endDate' ORDER BY pledger.date";
	// 	$result = $this->db->query($query);
	// 	return $result->result_array();
	// }
	// else if ($what == 'invoice') {
	// 	$query = "SELECT pledger.ETYPE, pledger.DCNO AS `VRNOA`, pledger.DATE, party.name AS `PARTY`, pledger.DESCRIPTION AS `REMARKS`, pledger.DEBIT AS `DEBIT` FROM pledger pledger INNER JOIN party party ON pledger.pid = party.pid WHERE pledger.pid IN (SELECT pid AS PARTY_ID FROM party party INNER JOIN level3 level3 ON party.level3=level3.l3 INNER JOIN level2 level2 ON level2.l2 = level3.l2 INNER JOIN level1 level1 ON level2.l1 = level1.l1 WHERE level1.NAME IN ('expense', 'expenses')) AND party.name NOT IN ('sale return', 'purchase', 'purchase import') AND pledger.date BETWEEN '$startDate' AND '$endDate' ORDER BY pledger.DCNO";
	// 	$result = $this->db->query($query);
	// 	return $result->result_array();
	// }
	// else if ($what == 'party') {
	// 	$query = "SELECT pledger.ETYPE, pledger.DCNO AS `VRNOA`, pledger.DATE, party.name AS `PARTY`, pledger.DESCRIPTION AS `REMARKS`, pledger.DEBIT AS `DEBIT` FROM pledger pledger INNER JOIN party party ON pledger.pid = party.pid WHERE pledger.pid IN (SELECT pid AS PARTY_ID FROM party party INNER JOIN level3 level3 ON party.level3=level3.l3 INNER JOIN level2 level2 ON level2.l2 = level3.l2 INNER JOIN level1 level1 ON level2.l1 = level1.l1 WHERE level1.NAME IN ('expense', 'expenses')) AND party.name NOT IN ('sale return', 'purchase', 'purchase import') AND pledger.date BETWEEN '$startDate' AND '$endDate' ORDER BY pledger.pid";
	// 	$result = $this->db->query($query);
	// 	return $result->result_array();
		// }

		$ord='';
		if ($what == 'date') {
				$ord='pledger.date';
		}
		else if ($what == 'invoice') {
				$ord='pledger.dcno';
		}
		else if ($what == 'party') {
			$ord='party.name';		
		}
		else if ($what == 'user') {
				$ord='user.uname';
		}

		// $query = "SELECT pledger.ETYPE, pledger.DCNO AS VRNOA,party.NAME AS PARTY,pledger.DEBIT AS 'AMOUNT',pledger.DESCRIPTION AS REMARKS, pledger.DATE FROM pledger pledger INNER JOIN party party ON pledger.pid = party.pid WHERE pledger.DEBIT <> 0 AND pledger.ETYPE='CPV' AND pledger.date BETWEEN '$startDate' AND '$endDate' ORDER BY party.NAME, pledger.DCNO";
		$query = "SELECT pledger.ETYPE, pledger.DCNO AS VRNOA,party.NAME AS PARTY,pledger.DEBIT as DEBIT ,pledger.credit  as 'CREDIT' ,pledger.DESCRIPTION AS REMARKS, pledger.DATE, user.uname, company.company_name  FROM pledger pledger INNER JOIN party party ON pledger.pid = party.pid inner join user as user on user.uid=pledger.uid inner join company on company.company_id=pledger.company_id   WHERE pledger.pid IN (SELECT pid AS PARTY_ID FROM party party INNER JOIN level3 level3 ON party.level3=level3.l3 INNER JOIN level2 level2 ON level2.l2 = level3.l2 INNER JOIN level1 level1 ON level2.l1 = level1.l1 WHERE level1.NAME IN ('expense', 'expenses')) AND party.name NOT IN ('sale return', 'purchase', 'purchase import') AND pledger.date BETWEEN '$startDate' AND '$endDate'  and pledger.company_id=$company_id  ORDER BY  $ord  ,pledger.date";
		// $query = "SELECT pledger.ETYPE, pledger.DCNO AS VRNOA,party.NAME AS PARTY,(case when (pledger.etype='cpv') then pledger.DEBIT else pledger.credit END) as 'AMOUNT' ,pledger.DESCRIPTION AS REMARKS, pledger.DATE, user.uname, company.company_name  FROM pledger pledger INNER JOIN party party ON pledger.pid = party.pid inner join user as user on user.uid=pledger.uid inner join company on company.company_id=pledger.company_id   WHERE (case when (pledger.etype='cpv') then pledger.DEBIT<>0 else pledger.credit<>0 END)    ORDER BY  pledger.date";
		$result = $this->db->query($query);
		return $result->result_array();

	}
	public function fetchPayRecvCount( $startDate, $endDate, $company_id, $etype)
	{
		if ($etype == 'payable') {
			$query = "SELECT COUNT(*) as NET_COUNT FROM (SELECT a.dcno, a.date, party.name as account, IFNULL(a.date, '-') as due_date, IFNULL(DATEDIFF(a.date, CURDATE()), '-') as days_passed, sum(a.credit) invoice_amount, (SELECT IFNULL(SUM(b.debit),0) FROM pledger  b WHERE b.invoice = a.dcno and b.pid =a.pid and a.date = CURDATE() AND b.etype='CPV' AND b.company_id = $company_id ) paid FROM pledger  a INNER JOIN party ON a.pid = party.pid WHERE a.etype='PURCHASE' AND a.date = CURDATE() AND a.company_id = $company_id GROUP BY a.dcno,a.date ORDER BY a.dcno ) as aging";
			$query = $this->db->query($query);

			$result = $query->row_array();
			return $result['NET_COUNT'];

		} else if ( $etype === 'receiveable') {
			$query = "SELECT COUNT(*) as NET_COUNT FROM (SELECT a.dcno, a.date, party.name as account, IFNULL(a.date, '-') as due_date, IFNULL(DATEDIFF(a.date, CURDATE()), '-') as days_passed, sum(a.debit) invoice_amount, (SELECT IFNULL(SUM(b.credit),0) FROM pledger  b WHERE b.invoice = a.dcno and b.pid =a.pid and a.date = CURDATE() AND b.etype='CPV' AND b.company_id = $company_id ) paid FROM pledger  a INNER JOIN party ON a.pid = party.pid WHERE a.etype='SALE' AND a.date = CURDATE() AND a.company_id = $company_id GROUP BY a.dcno,a.date ORDER BY a.dcno ) as aging";
			$query = $this->db->query($query);

			$result = $query->row_array();
			return $result['NET_COUNT'];
		}
	}
	public function fetchInvoiceAgingData( $from, $to, $reptType, $party_id, $company_id)
	{
		$query = "";
		if ( $reptType === 'payables' ) {
			if ( $party_id ) {
				// $query = "SELECT a.dcno, party.name as account, a.vrdate, IFNULL(a.due_date, '-') as due_date, IFNULL(DATEDIFF(a.due_date, CURDATE()), '-') as days_passed, sum(a.credit) invoice_amount, (SELECT IFNULL(SUM(b.debit),0) FROM pledger  b WHERE b.invoice = a.dcno and b.party_id =a.party_id and a.vrdate BETWEEN '$from' AND '$to' AND b.etype='CPV' AND b.company_id = $company_id ) paid FROM pledger  a INNER JOIN party ON a.party_id = party.party_id WHERE a.etype='PURCHASE' AND a.vrdate BETWEEN '$from' AND '$to' AND a.company_id = $company_id GROUP BY a.dcno,a.vrdate ORDER BY a.dcno ";
				$query = "SELECT *, invoice_amount-paid as balance FROM (SELECT a.dcno, a.date, party.name as account, IFNULL(a.due_date, '-') as due_date, IFNULL(DATEDIFF(a.due_date, CURDATE()), '-') as days_passed, sum(a.credit) invoice_amount, (SELECT IFNULL(SUM(b.debit),0) FROM pledger  b WHERE b.invoice = a.dcno and b.pid =a.pid and a.date BETWEEN '$from' AND '$to' AND b.etype='CPV' AND b.company_id = $company_id ) paid FROM pledger  a INNER JOIN party ON a.pid = party.pid WHERE a.etype='PURCHASE' AND a.date BETWEEN '$from' AND '$to' AND a.company_id = $company_id AND a.pid = $pid GROUP BY a.dcno,a.date ORDER BY a.dcno ) as aging";
			} else {
				$query = "SELECT *, invoice_amount-paid AS balance, aging.address, aging.email, aging.mobile FROM (SELECT  a.dcno, a.date, party.address, party.email, party.mobile, party.name AS account, IFNULL(a.due_date, '-') AS due_date, IFNULL(DATEDIFF(a.due_date, CURDATE()), '-') AS days_passed, SUM(a.credit) invoice_amount, (SELECT IFNULL(SUM(b.debit),0) FROM pledger b WHERE b.invoice = a.dcno AND b.pid =a.pid AND a.date BETWEEN '$from' AND '$to' AND b.etype='CPV' AND b.company_id = $company_id ) paid FROM pledger a INNER JOIN party ON a.pid = party.pid WHERE a.etype='PURCHASE' AND a.date BETWEEN '$from' AND '$to' AND a.company_id = $company_id GROUP BY a.dcno,a.date ORDER BY a.dcno) AS aging";
			}
		} else if ( $reptType === 'receiveables' ) {
			if ($party_id) {
				$query = "SELECT *, invoice_amount-paid as balance FROM (SELECT a.dcno, a.date, party.name as account, IFNULL(a.due_date, '-') as due_date, IFNULL(DATEDIFF(a.due_date, CURDATE()), '-') as days_passed, sum(a.debit) invoice_amount, (SELECT IFNULL(SUM(b.credit),0) FROM pledger  b WHERE b.invoice = a.dcno and b.pid =a.pid and a.date BETWEEN '$from' AND '$to' AND b.etype='CRV' AND b.company_id = $company_id ) paid FROM pledger  a INNER JOIN party ON a.pid = party.pid WHERE a.etype='SALE' AND a.date BETWEEN '$from' AND '$to' AND a.company_id = $company_id AND a.pid = $party_id GROUP BY a.dcno,a.date ORDER BY a.dcno ) as aging";
			} else {
				$query = "SELECT *, invoice_amount-paid AS balance, aging.address, aging.email, aging.mobile FROM (SELECT a.dcno, a.date, party.address, party.email, party.mobile, party.name AS account, IFNULL(a.due_date, '-') AS due_date, IFNULL(DATEDIFF(a.due_date, CURDATE()), '-') AS days_passed, SUM(a.debit) invoice_amount, (SELECT IFNULL(SUM(b.credit),0) FROM pledger b WHERE b.invoice = a.dcno AND b.pid =a.pid AND a.date BETWEEN '$from' AND '$to' AND b.etype='CRV' AND b.company_id = $company_id) paid FROM pledger a INNER JOIN party ON a.pid = party.pid WHERE a.etype='SALE' AND a.date BETWEEN '$from' AND '$to' AND a.company_id = $company_id GROUP BY a.dcno,a.date ORDER BY a.dcno) AS aging";
			}		
		}

		$result = $this->db->query( $query );
		return $result->result_array();
	}

	public function fetchPreviousBalance( $date, $party_id , $dcno , $etype)
	{
		
        $balance=0;
		$result = $this->db->query("call spw_PreviousBalance('2007/01/01','". $date ."',".$party_id.",'".$etype."',".$dcno.")");
        mysqli_next_result($this->db->conn_id);
        $balance = $result->row_array();
		//=$result['balance'];
		return $balance['balance'];

 	}

 	public function fetchPartyWiseRate($partyId){

 		$sql = 'SELECT party_id, item.item_id, item_des, rate
			FROM item
			INNER JOIN party_rate ON party_rate.item_id = item.item_id
			WHERE party_id = '.$partyId;
 		$result = $this->db->query($sql);

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
 	}

	public function fetchPartyPurRate($itemId, $partyId){

 		$sql = 'SELECT rate
				FROM party_rate
				WHERE party_id = '.$partyId.' AND item_id = '.$itemId;
 		$result = $this->db->query($sql);

		if ( $result->num_rows() > 0 ) {
			$result = $result->row_array();
			return $result['rate'];
		} else {
			return false;
		}
 	}

 	public function savePartyRate($partyRate)
    {
 		$this->db->where(array(
			'party_id' => $partyRate[0]['party_id']
		));

		$this->db->delete('party_rate');
        
        foreach ($partyRate as $rate) {
            
            $result = $this->db->insert('party_rate', $rate);
        } 

        return true;  
	} 
}

/* End of file accounts.php */
/* Location: ./application/models/accounts.php */