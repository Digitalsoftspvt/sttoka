<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sync_database extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function sync_db()
    {
        $host = $this->db->hostname;
        $username = $this->db->username;
        $password = $this->db->password;
        $database = $this->db->database;

        $return = '';
        $tab = array();
        $query = $this->db->query("show tables");
        $result = $query->result_array();

        foreach ($result as $value)
        {
            foreach ($value as $values)
            {
                $tab[] = $values;
            }
        }

        $md5 = (md5(implode(',', $tab)));
        $link = mysqli_connect($host, $username, $password, $database);
        foreach ($tab as $value)
        {
            $resultss = mysqli_query($link, 'SELECT * FROM ' . '`' . $value . '`');
            $num_fields = mysqli_num_fields($resultss);

            for ($i = 0; $i < $num_fields; $i++)
            {
                while ($row = mysqli_fetch_row($resultss))
                {
                    $return .= 'INSERT INTO ' . $value . ' VALUES(';
                    for ($j = 0; $j < $num_fields; $j++)
                    {
                        $row[$j] = addslashes($row[$j]);
                        $row[$j] = preg_replace("/[\n]/", "\\n", $row[$j]);

                        if (isset($row[$j]))
                        {
                            $return .= '"' . $row[$j] . '"';
                        }
                        else
                        {
                            $return .= '""';
                        }

                        if ($j < ($num_fields - 1))
                        {
                            $return .= ',';
                        }
                    }
                    $return .= ");\n";
                }
            }
            $return .= "\n\n\n";
        }

        $path = $_SERVER['DOCUMENT_ROOT'] . '/foundry/syncdatabase/' . 'db-backup-';
        $date = date('Y-M-d');
        $sqlFilename = $path . $date . '-' . $md5 . '.sql';

        $handle = fopen($sqlFilename, 'w+');
        fwrite($handle, $return);
        fclose($handle);

        /*$data = 'helo';
        $optional_headers = 'asd';
        $url = $sqlFilename;
        $params = array('http' => array(
            'method' => 'POST',
            'content' => $data
        ));
        if ($optional_headers !== null) {
            $params['http']['header'] = $optional_headers;
        }
        $ctx = stream_context_create($params);
        $fp = @fopen($url, 'rb', false, $ctx);
        if (!$fp) {
            throw new Exception("Problem with $url, $php_errormsg");
        }
        $response = @stream_get_contents($fp);
        if ($response === false) {
            throw new Exception("Problem reading data from $url, $php_errormsg");
        }*/
        die(print('ssss'));
        // return $result = 'Database has been Synced..!!!';
    }
}