<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('purchases');
	}

	public function getMaxVrno($etype, $company_id)
	{
		if ($etype == 'gp_in' || $etype === 'gp_out') {
			$result = $this->db->query("SELECT MAX(vrno) vrno FROM stockmain WHERE etype = '" . $etype . "' and company_id=$company_id AND DATE(vrdate) = DATE(NOW())");
		} else {
			$result = $this->db->query("SELECT MAX(vrno) vrno FROM ordermain WHERE etype = '" . $etype . "' and company_id=$company_id AND DATE(vrdate) = DATE(NOW())");
		}
		$row = $result->row_array();
		$maxId = $row['vrno'];

		return $maxId;
	}

	public function Validate_Order($etype, $company_id, $order_no, $status)
	{

		$result = $this->db->query("SELECT vrnoa FROM ordermain WHERE etype = '" . $etype . "' and company_id=$company_id AND vrnoa =$order_no  and status='" . $status . "' and vrnoa not in (select ordno from ordermain where ordno=$order_no and etype='order_parts') ");
		// $result = $this->db->query("SELECT max(vrnoa) vrnoa FROM ordermain WHERE etype = 'sale_order' and company_id=1 AND vrnoa =1  and status='running' ");
		$row = $result->row_array();
		if ($result->num_rows() > 0) {
			$chk_flag = $row['vrnoa'];
			return $chk_flag;
		} else {
			return false;
		}
	}

	public function Validate_Order_Loading($etype, $company_id, $order_no, $status)
	{

		$result = $this->db->query("SELECT vrnoa FROM ordermain WHERE etype = 'order_parts' and company_id=$company_id AND ordno =$order_no  ");
		$row = $result->row_array();
		if ($result->num_rows() > 0) {
			$chk_flag = $row['vrnoa'];
			return $chk_flag;
		} else {
			return false;
		}
	}


	public function fetchOrderReportData($startDate, $endDate, $what, $type, $company_id, $etype, $field, $crit, $orderBy, $groupBy, $name)
	{
		if ($type == 'detailed') {
			$query = $this->db->query("SELECT  $field as voucher,$name,bal.balance as pbalance,clo.balance AS cbalance,dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username,orderdetail.amount,ordermain.vrdate, ordermain.remarks, ordermain.vrnoa, ordermain.remarks,  abs(orderdetail.qty) qty, orderdetail.weight, orderdetail.rate, orderdetail.netamount, concat(item.item_code , ' ' , item.item_des) AS 'item_des',item.uom,orderdetail.`type` as detailentrytype FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetail.oid INNER JOIN party party ON ordermain.party_id = party.pid              INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3 INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2 INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1 INNER JOIN department dept  ON orderdetail.godown_id = dept.did  INNER JOIN item item ON orderdetail.item_id = item.item_id INNER JOIN user user ON user.uid = ordermain.uid 
			left join (select pid,sum(debit) - sum(credit) balance from pledger group by pid ) as bal on bal.pid = party.pid
			left join (SELECT pid,SUM(debit) - SUM(credit) balance FROM pledger WHERE date<='" . $endDate . "' group by pid)  as clo on party.pid = clo.pid
			 WHERE  ordermain.vrdate between '" . $startDate . "' AND '" . $endDate . "' AND ordermain.company_id=$company_id AND ordermain.etype='$etype' $crit  order by $orderBy");
			return $query->result_array();
		} else {
			if ($etype == 'sale') {
				$query = $this->db->query("SELECT $ FIELD AS voucher,$name,bal.balance AS pbalance,clo.balance AS cbalance, DAYNAME(vrdate) AS weekdate, MONTH(vrdate) AS monthdate, YEAR(vrdate) AS yeardate,user.uname AS username, DATE(ordermain.vrdate) AS DATE, orderdetail.amount, DATE(ordermain.VRDATE) VRDATE, ordermain.vrnoa, ABS(ROUND(SUM(orderdetail.qty))) qty, ROUND(SUM(orderdetail.weight)) weight, ROUND(SUM(orderdetail.rate)) rate, ROUND(SUM(CASE WHEN orderdetail.`type`='add' THEN orderdetail.netamount WHEN orderdetail.`type`='less' THEN -orderdetail.netamount ELSE 0 END)) netamount, ordermain.remarks
					FROM ordermain ordermain
					INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetail.oid
					INNER JOIN party party ON ordermain.party_id = party.pid
					INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3
					INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2
					INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1
					INNER JOIN department dept ON orderdetail.godown_id = dept.did
					INNER JOIN item item ON orderdetail.item_id = item.item_id
					INNER JOIN user user ON user.uid = ordermain.uid
					LEFT JOIN (
					SELECT pid, SUM(debit) - SUM(credit) balance
					FROM pledger
					GROUP BY pid) AS bal ON bal.pid = party.pid
					LEFT JOIN (
					SELECT pid,SUM(debit) - SUM(credit) as balance
					FROM pledger
					WHERE date<='" . $endDate . "') as clo on clo.pid = party.pid
					WHERE ordermain.vrdate BETWEEN '" . $startDate . "' AND '" . $endDate . "' AND ordermain.company_id=$company_id AND ordermain.etype='$etype' $crit
					GROUP BY $groupBy
					ORDER BY $orderBy");
			} else {
				$query = $this->db->query("SELECT  $field as voucher,$name,bal.balance as pbalance,dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username,date(ordermain.vrdate) as DATE, orderdetail.amount, date(ordermain.VRDATE) VRDATE, ordermain.vrnoa, ABS(round(SUM(orderdetail.qty))) qty, round(SUM(orderdetail.weight)) weight, round(SUM(orderdetail.rate)) rate, round(sum(orderdetail.netamount)) netamount, ordermain.remarks FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetail.oid INNER JOIN party party ON ordermain.party_id = party.pid              INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3 INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2 INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1 INNER JOIN department dept  ON orderdetail.godown_id = dept.did  INNER JOIN item item ON orderdetail.item_id = item.item_id INNER JOIN user user ON user.uid = ordermain.uid left join (select pid,sum(debit) - sum(credit) balance from pledger group by pid ) as bal on bal.pid = party.pid WHERE  ordermain.vrdate between '" . $startDate . "' AND '" . $endDate . "' AND ordermain.company_id=$company_id AND ordermain.etype='$etype' $crit group by $groupBy order by $orderBy");
			}

			return $query->result_array();
		}
	}

	public function fetchOrderReportDataParts($startDate, $endDate, $what, $type, $company_id, $etype, $field, $crit, $orderBy, $groupBy, $name)
	{
		if ($type == 'detailed') {
			$query = $this->db->query("SELECT  $field as voucher,$name, dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username,ordermain.vrdate,date(ordermain.vrdate) as date, ordermain.remarks, ordermain.vrnoa, ordermain.remarks, abs(orderdetail.qty) qty, orderdetail.weight, orderdetail.netamount, item.item_des as 'item_des',item.uom FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetail.oid INNER JOIN party party ON ordermain.party_id = party.pid              INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3 INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2 INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1 INNER JOIN item item ON orderdetail.item_id = item.item_id INNER JOIN department dept  ON orderdetail.godown_id = dept.did INNER JOIN user user ON user.uid = ordermain.uid  WHERE  ordermain.vrdate between '" . $startDate . "' and '" . $endDate . "' and ordermain.company_id=$company_id and ordermain.etype='$etype' $crit  order by $orderBy");
			return $query->result_array();
		} else {
			$query = $this->db->query("SELECT  $field as voucher,$name, dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username,date(ordermain.vrdate) as date, date(ordermain.vrdate) vrdate, ordermain.vrnoa, ABS(round(SUM(orderdetail.qty))) qty, round(SUM(orderdetail.weight)) weight, round(SUM(orderdetail.RATE)) RATE, round(sum(orderdetail.netamount)) netamount, ordermain.remarks FROM ordermain ordermain INNER JOIN orderdetail orderdetail ON ordermain.oid = orderdetail.oid INNER JOIN party party ON ordermain.party_id = party.pid              INNER JOIN level3 leveltbl3 ON leveltbl3.l3 = party.level3 INNER JOIN level2 leveltbl2 ON leveltbl2.l2 = leveltbl3.l2 INNER JOIN level1 leveltbl1 ON leveltbl1.l1 = leveltbl2.l1 INNER JOIN item item ON orderdetail.item_id = item.item_id INNER JOIN user user ON user.uid = ordermain.uid  INNER JOIN department dept  ON orderdetail.godown_id = dept.did WHERE  ordermain.VRDATE between '" . $startDate . "' and '" . $endDate . "' and ordermain.company_id=$company_id and ordermain.etype='$etype' $crit group by $groupBy order by $orderBy");
			return $query->result_array();
		}
	}

	public function fetchByCol($col)
	{

		$result = $this->db->query("SELECT DISTINCT $col FROM ordermain");
		return $result->result_array();
	}

	public function getMaxVrnoa($etype, $company_id)
	{
		if ($etype == 'gp_in' || $etype === 'gp_out') {
			$result = $this->db->query("SELECT MAX(vrnoa) vrnoa FROM stockmain WHERE etype = '" . $etype . "' and company_id=$company_id");
		} else {
			$result = $this->db->query("SELECT MAX(vrnoa) vrnoa FROM ordermain WHERE etype = '" . $etype . "' and company_id=$company_id");
		}

		$row = $result->row_array();
		$maxId = $row['vrnoa'];

		return $maxId;
	}

	/**
	 * @param $ordermain
	 * @param $orderdetail
	 * @param $vrnoa
	 * @param $etype
	 * @param string $companyId
	 * @return bool
	 */
	// this function is using in inward and outward gatepass voucher.......
	public function save_inward($stockmain, $stockdetail, $vrnoa, $etype, $company_id)
	{
		$this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype));
		$result = $this->db->get('stockmain');

		$oid = "";


		if ($result->num_rows() > 0) {
			$result = $result->row_array();
			$stid = $result['stid'];
			$this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype));
			$this->db->update('stockmain', $stockmain);

			$this->db->where(array('stid' => $stid));
			$this->db->delete('stockdetail');
		} else {

			$this->db->insert('stockmain', $stockmain);
			$stid = $this->db->insert_id();
		}
		foreach ($stockdetail as $detail) {

			$detail['stid'] = $stid;
			$this->db->insert('stockdetail', $detail);
		}
		return true;
	}
	public function save($ordermain, $orderdetail, $vrnoa, $etype, $companyId = "")
	{

		$this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype));
		$result = $this->db->get('ordermain');

		$oid = "";
		if ($result->num_rows() > 0) {
			$result = $result->row_array();
			$oid = $result['oid'];
			$this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype));
			$this->db->update('ordermain', $ordermain);

			$this->db->where(array('oid' => $oid));
			$this->db->delete('orderdetail');
		} else {
			$this->db->insert('ordermain', $ordermain);
			$oid = $this->db->insert_id();
		}

		$stockDetails = array();
		foreach ($orderdetail as $detail) {
			$detail['oid'] = $oid;
			$this->db->insert('orderdetail', $detail);
			if ($etype == 'spare_parts') {

				$query = "SELECT phase_id
						FROM recipemain
						INNER JOIN recipedetail ON recipedetail.rid = recipemain.rid
						WHERE recipemain.itemto = $detail[item_id]
						ORDER BY rdid DESC
						LIMIT 1";

				$result = $this->db->query($query);
				if ($result->num_rows() > 0) {

					$result = $result->result_array();
					$phaseId = $result[0]['phase_id'];
					if ($phaseId == $detail['phase_id']) {
						unset($detail['oid']);
						$stockDetails[] = $detail;
					}
				}
			}
		}

		$company_id = $ordermain['company_id'];


		if (count($stockDetails) > 0) {

			$this->db->where(array('order_vrno' => $vrnoa, 'etype' => 'spare_parts', 'company_id' => $company_id));
			$result = $this->db->get('stockmain');

			$stid = "";
			if ($result->num_rows() > 0) {
				$result = $result->row_array();
				$stid = $result['stid'];
				$sVrnoa = $result['vrnoa'];
				/*$this->db->where(array('order_vrno' => $vrnoa, 'etype' => 'spare_parts' ,'company_id'=>$stockmain['company_id']));
				$this->db->update('stockmain', $stockmain);*/

				$this->db->where(array('stid' => $stid));
				$this->db->delete('stockdetail');
			} else {

				$sVrnoa = $this->purchases->getMaxVrnoa('spare_parts', $company_id) + 1;
				$stockMain = array('vrnoa' => $sVrnoa, 'etype' => $etype, 'order_vrno' => $vrnoa, 'vrdate' => date('Y-m-d'), 'company_id' => $company_id);

				//$result = $this->purchases->save($stockMain, $stockDetails, $sVrnoa, $company_id);
				$this->db->insert('stockmain', $stockMain);
				$stid = $this->db->insert_id();
			}

			foreach ($stockDetails as $detail) {

				$detail['stid'] = $stid;
				$this->db->insert('stockdetail', $detail);
			}

			$query = $this->db->query("call spw_SparePartsLess('" . $ordermain['godown_id'] .  "', '" . $sVrnoa . "', '" . $company_id . "')");
		} else {

			$this->deleteFromStockmain($vrnoa, 'spare_parts', $company_id);
		}

		return true;
	}

	public function deleteFromStockmain($vrnoa, $etype, $company_id)
	{

		$this->db->where(array('etype' => $etype, 'order_vrno' => $vrnoa, 'company_id' => $company_id));
		$result = $this->db->get('stockmain');

		if ($result->num_rows() == 0) {
			return false;
		} else {

			$result = $result->row_array();
			$stid = $result['stid'];

			$this->db->where(array('etype' => $etype, 'order_vrno' => $vrnoa));
			$this->db->delete('stockmain');
			$this->db->where(array('stid' => $stid));
			$this->db->delete('stockdetail');

			return true;
		}
	}

	public function saveSaleInvoice($stockmain, $stockdetail, $stockless, $ordermain, $orderdetail, $vrnoa, $etype, $companyId = "")
	{

		$this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype, 'company_id' => $companyId));
		$result = $this->db->get('ordermain');

		$oid = "";
		if ($result->num_rows() > 0) {
			$result = $result->row_array();
			$oid = $result['oid'];
			$this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype));
			$this->db->update('ordermain', $ordermain);

			$this->db->where(array('oid' => $oid));
			$this->db->delete('orderdetail');
		} else {
			$this->db->insert('ordermain', $ordermain);
			$oid = $this->db->insert_id();
		}

		foreach ($orderdetail as $detail) {
			$detail['oid'] = $oid;
			$this->db->insert('orderdetail', $detail);
		}

		$this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype, 'company_id' => $companyId));
		$resultStock = $this->db->get('stockmain');

		$stid = "";
		if ($resultStock->num_rows() > 0) {
			$resultStock = $resultStock->row_array();

			$stid = $resultStock['stid'];
			$this->db->where(array('vrnoa' => $vrnoa, 'etype' => $etype));
			$this->db->update('stockmain', $stockmain);

			$this->db->where(array('stid' => $stid));
			$this->db->delete('stockdetail');
		} else {
			$this->db->insert('stockmain', $stockmain);
			$stid = $this->db->insert_id();
		}

		//         if($etype == 'sale'){

		//         	$this->load->model('accounts');
		//          $previous = $this->accounts->fetchRunningTotal( $stockmain['vrdate'] , $stockmain['party_id'] );
		//          $previousBalance = $previous[0]['RTotal'];
		//          $amount = $stockmain['namount'];
		//          $message = 'Your Account has been Debited Rs:'.$amount.' against Invoice # '.$vrnoa.' and your Previous Balance is Rs:'.$previousBalance;
		//          $query = 'select mobile from party where pid ='.$stockmain['party_id'];
		//          $mobile = $this->db->query( $query );
		//          $row = $mobile->row_array();
		// $mobile = $row['mobile'];

		//          $this->sendsms->sendS( $mobile, $message);
		//         }

		//     }

		// foreach ($stockdetail as $detail) {
		//     $detail['stid'] = $stid;
		//     $this->db->insert('stockdetail', $detail);
		// }

		if ($etype == 'sale') {


			$resSaleStockLess = $this->db->query("call sale_invoice_stock_less('" . $orderdetail[0]['godown_id'] .  "', '" . $vrnoa . "', '" . $companyId . "')");

			if ((int) $resSaleStockLess->num_rows() > 0) {
				$resSaleStockLessArray = $resSaleStockLess->result_array();
				mysqli_next_result($this->db->conn_id);
				foreach ($resSaleStockLessArray as $row) {
					$qtyLess = $row['qty'];
					$itemId = $row['item_id'];
					$less = $row['less'];
					/*  if($stockless != "")
                     {
                         foreach ($stockless as $detail)
                         {
                             if ($itemId == $detail['item_id'])
                             {
                                 $remainingQty = (float)$qtyLess - abs((float)$detail['qty']);
                                 $qtyLess = $remainingQty;
                             }
                         }
                     }*/
					if ($less == 'add') {
						$this->db->query("insert into stockdetail(stid,item_id,qty,rate,amount,`type`,godown_id, weight, netamount)
                                       values
                                       ('" . $row['stid'] . "', '" . $row['item_id'] . "', '" . $qtyLess . "', '" . $row['lprate'] . "', '" . $row['amount'] . "', '" . $row['less'] . "', '" . $row['godown_id'] . "', '-" . $row['weight'] . "', '0')");
					} else {
						$this->db->query("insert into stockdetail(stid,item_id,qty,rate,amount,`type`,godown_id, weight, netamount)
                                       values
                                       ('" . $row['stid'] . "', '" . $row['item_id'] . "', '-" . $qtyLess . "', '" . $row['lprate'] . "', '" . $row['amount'] . "', '" . $row['less'] . "', '" . $row['godown_id'] . "', '-" . $row['weight'] . "', '0')");
					}
				}
			}
		}
		return true;
	}

	public function fetchAllNotedBy()
	{
		$result = $this->db->query("SELECT DISTINCT noted_by FROM ordermain WHERE noted_by <> ''");
		return $result->result_array();
	}

	public function fetchAllApprovedBy()
	{
		$result = $this->db->query("SELECT DISTINCT approved_by FROM ordermain WHERE approved_by <> ''");
		return $result->result_array();
	}

	public function fetchAllPreparedBy()
	{
		$result = $this->db->query("SELECT DISTINCT prepared_by FROM ordermain WHERE prepared_by <> ''");
		return $result->result_array();
	}

	public function fetchAllreferBy()
	{
		$result = $this->db->query("SELECT DISTINCT order_by FROM ordermain WHERE order_by <> ''");
		return $result->result_array();
	}

	public function fetchAllPayment()
	{
		$result = $this->db->query("SELECT DISTINCT payment FROM ordermain WHERE payment <> ''");
		return $result->result_array();
	}

	public function fetch($vrnoa, $etype, $company_id)
	{
		$result = $this->db->query("SELECT i.uitem_des,m.pre_balance,i.item_code, 
			d.fedrate,d.fedamount, m.received_by,party.name party_name,m.vrno, 
			m.vrnoa, m.ordno, m.transporter_id,m.discount,m.discp,m.tax,m.taxpercent,
			m.expense,m.exppercent,m.paid, m.uid, m.party_id, DATE(m.vrdate) vrdate, 
			m.remarks, m.etype, m.noted_by, m.namount, m.pub_add, d.item_id, d.godown_id, 
			ROUND(d.qty, 2) qty,d.rate,d.amount, ROUND(d.bundle, 2) bundle, ROUND(d.weight, 3) weight,
			d.type, i.item_des AS item_name, i.uom, dep.name AS dept_name, rcp.item_id AS finish_item,
			IFNULL(rcp.rid,0) recipeid,party.address,i.item_code,party.account_id,sd.stqty,m.officer_id,
			m.prepared_by,m.approved_by,m.dispatch_address,m.payment_term,m.delivery_term,m.bilty_no,m.payment
			FROM ordermain AS m
			INNER JOIN orderdetail AS d ON m.oid = d.oid
			INNER JOIN item i ON i.item_id = d.item_id
			left JOIN department dep ON dep.did = d.godown_id
			LEFT JOIN party party ON party.pid=m.party_id
			LEFT JOIN recipemain rcp ON rcp.item_id=d.item_id
			LEFT JOIN (
				SELECT item_id, IFNULL(SUM(qty),0) stqty, IFNULL(SUM(weight),0) stweight
				FROM stockdetail
				GROUP BY item_id) sd ON sd.item_id=i.item_id 
WHERE m.vrnoa = $vrnoa AND m.etype = '" . $etype . "' and m.company_id=$company_id  order by d.type,d.odid");
		if ($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchforsaleprint($vrnoa, $etype, $company_id)
	{
		$result = $this->db->query("SELECT i.uitem_des,m.pre_balance,i.item_code, 
		d.fedrate,d.fedamount, m.received_by,party.name party_name,m.vrno, 
		m.vrnoa, m.ordno, m.transporter_id,m.discount,m.discp,m.tax,m.taxpercent,
		m.expense,m.exppercent,m.paid, m.uid, m.party_id, DATE(m.vrdate) vrdate, 
		m.remarks, m.etype, m.noted_by, m.namount, m.pub_add, d.item_id, d.godown_id, 
		ROUND(d.qty, 2) qty,d.rate,d.amount, ROUND(d.bundle, 2) bundle, ROUND(d.weight, 3) weight,
		d.type, i.item_des AS item_name, i.uom, dep.name AS dept_name, rcp.item_id AS finish_item,
		IFNULL(rcp.rid,0) recipeid,party.address,i.item_code,party.account_id,sd.stqty,m.officer_id,
		m.prepared_by,m.approved_by,m.dispatch_address,m.payment_term,m.delivery_term,m.bilty_no,m.payment,i.item_des
		FROM ordermain AS m
		INNER JOIN orderdetail AS d ON m.oid = d.oid
		INNER JOIN item i ON i.item_id = d.item_id
		left JOIN department dep ON dep.did = d.godown_id
		LEFT JOIN party party ON party.pid=m.party_id
		LEFT JOIN recipemain rcp ON rcp.item_id=d.item_id
		LEFT JOIN (
			SELECT item_id, IFNULL(SUM(qty),0) stqty, IFNULL(SUM(weight),0) stweight
			FROM stockdetail
			GROUP BY item_id) sd ON sd.item_id=i.item_id WHERE m.vrnoa = $vrnoa AND m.etype = '" . $etype . "' and m.company_id=$company_id  order by d.type,d.odid");
		if ($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchSparParts($vrnoa, $etype, $company_id)
	{
		$result = $this->db->query("SELECT i.uitem_des,i.item_code, d.fedrate,d.fedamount, m.received_by,party.name party_name,m.vrno, m.vrnoa, m.ordno, m.transporter_id,m.discount,m.discp,m.tax,m.taxpercent,m.expense,m.exppercent,m.paid, m.uid, m.party_id, DATE(m.vrdate) vrdate, m.remarks, m.etype, m.noted_by, m.namount, m.pub_add, d.item_id, d.godown_id, ROUND(d.qty, 2) qty,d.rate,d.amount, ROUND(d.bundle, 2) bundle, ROUND(d.weight, 2) weight, d.type, i.item_des AS item_name, i.uom, dep.name AS dept_name, rcp.item_id AS finish_item, IFNULL(rcp.rid,0) recipeid,party.address,i.item_code,party.account_id,sd.stqty,m.officer_id,m.prepared_by,m.approved_by,m.dispatch_address,m.payment_term,m.delivery_term,m.bilty_no,m.payment, p.name as phase_name, p.id as phase_id, i.grweight, m.expense_id, m.employee_id, m.godown_id as main_godown_id,d.perday,d.produced
			FROM ordermain AS m
			INNER JOIN orderdetail AS d ON m.oid = d.oid
			INNER JOIN item i ON i.item_id = d.item_id
			left JOIN department dep ON dep.did = d.godown_id
			LEFT JOIN party party ON party.pid=m.party_id
			LEFT JOIN recipemain rcp ON rcp.item_id=d.item_id
			left JOIN phase AS p ON p.id = d.phase_id
			LEFT JOIN (
				SELECT item_id, IFNULL(SUM(qty),0) stqty, IFNULL(SUM(weight),0) stweight
				FROM stockdetail
				GROUP BY item_id) sd ON sd.item_id=i.item_id 
			WHERE m.vrnoa = $vrnoa AND m.etype = '" . $etype . "' and m.company_id=$company_id  order by d.type,d.odid");
		if ($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchthroughPO($vrnoa, $etype, $company_id)
	{
		$result = $this->db->query("SELECT m.received_by,party.name party_name,m.vrno, m.vrnoa, m.ordno, m.transporter_id,m.discount,m.discp,m.tax,m.taxpercent,m.expense,m.exppercent,m.paid, m.uid, m.party_id, DATE(m.vrdate) vrdate, m.remarks, m.etype, m.noted_by, m.namount, m.pub_add, d.item_id, d.godown_id, ROUND(d.qty, 2) qty,d.rate,d.amount, ROUND(d.bundle, 2) bundle, ROUND(d.weight, 2) weight, d.type, i.item_des AS item_name, i.uom, dep.name AS dept_name, rcp.item_id AS finish_item, IFNULL(rcp.rid,0) recipeid,party.address,i.item_code,party.account_id FROM ordermain AS m INNER JOIN orderdetail AS d ON m.oid = d.oid INNER JOIN item i ON i.item_id = d.item_id INNER JOIN department dep ON dep.did = d.godown_id LEFT JOIN party party ON party.pid=m.party_id LEFT JOIN recipemain rcp ON rcp.item_id=d.item_id WHERE m.vrnoa =$vrnoa AND m.etype = '" . $etype . "' AND m.company_id=$company_id and m.vrnoa not in (select order_vrno from stockmain where etype = 'purchase') ORDER BY d.type,d.odid");
		if ($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function last_5_srate($company_id)
	{
		$result = $this->db->query("SELECT  m.vrnoa, DATE(m.vrdate) vrdate, ROUND(d.qty, 2) qty,d.rate, ROUND(d.weight, 2) as weight
		FROM stockmain AS m
		INNER JOIN stockdetail AS d ON m.stid = d.stid
		INNER JOIN item i ON i.item_id = d.item_id
		INNER JOIN department dep ON dep.did = d.godown_id
		INNER JOIN party party ON party.pid=m.party_id
		WHERE m.etype = 'purchase' AND m.company_id=$company_id
		ORDER BY m.vrdate  desc
		limit 5");
		if ($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function last_stockLocatons($item_id, $company_id, $etype)
	{
		$result = $this->db->query("SELECT g.name as location, ifnull(sum(d.qty),0) as qty
		from stockdetail d
		inner join department g
		on g.did=d.godown_id
		where d.item_id= '" . $item_id . "' group by g.name");

		if ($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetch_order_stock($vrnoa, $etype, $company_id)
	{

		$result = $this->db->query("SELECT i.uom,i.item_code,i.uitem_des, party.name party_name,m.vrno, m.vrnoa, m.ordno, m.transporter_id,m.discount,m.discp,m.tax,m.taxpercent,m.expense,m.exppercent,m.paid, m.uid, m.party_id, DATE(m.vrdate) vrdate, m.remarks, m.etype, m.noted_by, m.namount, m.pub_add, d.item_id, d.godown_id, ROUND(d.qty, 2) tot_qty, ROUND(d.weight, 2) tot_weight, ROUND(stk.qty, 2) qty, ROUND(stk.weight, 2) weight,i.srate rate,i.srate * ROUND(stk.qty, 2) as amount, ROUND(d.bundle, 2) bundle, ROUND(d.weight, 2) weight, d.type, i.item_des AS item_name, i.uom, dep.name AS dept_name, rcp.item_id AS finish_item, IFNULL(rcp.rid,0) recipeid,d.fedrate,d.fedamount
		FROM ordermain AS m
		INNER JOIN orderdetail AS d ON m.oid = d.oid
		INNER JOIN item i ON i.item_id = d.item_id
		INNER JOIN department dep ON dep.did = d.godown_id
		INNER JOIN party party ON party.pid=m.party_id
		LEFT JOIN recipemain rcp ON rcp.item_id=d.item_id
		INNER JOIN(
			SELECT IFNULL(SUM(d.qty),0) AS qty, IFNULL(SUM(d.weight),0) AS weight,i.item_id,i.item_des,d.type
			FROM ordermain AS m
			INNER JOIN orderdetail AS d ON m.oid = d.oid
			INNER JOIN item i ON i.item_id = d.item_id
			INNER JOIN department dep ON dep.did = d.godown_id
			INNER JOIN party party ON party.pid=m.party_id
			LEFT JOIN recipemain rcp ON rcp.item_id=d.item_id
			WHERE m.ordno =$vrnoa AND m.etype IN('sale','sale_order') AND m.company_id=$company_id
			GROUP BY i.item_id,d.`type`) AS stk ON stk.item_id=d.item_id AND stk.`type`=d.`type`
WHERE m.vrnoa =$vrnoa AND m.etype = '" . $etype . "' AND m.company_id=$company_id AND m.vrnoa NOT IN (
	SELECT ordno
	FROM ordermain
	WHERE etype='sale')
ORDER BY d.type,d.odid");

		if ($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchPartsOrder($vrnoa, $etype, $company_id, $type)
	{
		if ($type == 'parts') {

			$result = $this->db->query("SELECT i.uitem_des, i.item_id,rcp.qty rqty ,rcp.fqty,d.qty dqty, d.qty * rcp.qty as qty, d.weight *(rcp.weight/ifnull(rcp.fweight,1)) as weight, i.item_des AS item_name, i.uom, 
            rcp.item_id as finish_item,
            ifnull(rcp.rid,0) recipeid,ifnull(stk.qty,0) as stkqty,ifnull(stk.weight,0) as stkweight FROM ordermain AS m
			INNER JOIN orderdetail AS d ON m.oid = d.oid  
			left join
			(
			select rcp.item_id,rcp.fqty,rcpd.qty,rcpd.weight,rcp.fweight,rcp.rid,rcpd.item_id as ritem from recipemain rcp inner join recipedetail rcpd on rcpd.rid=rcp.rid
			where rcp.item_id in (select item_id from ordermain m inner join orderdetail d on m.oid = d.oid where m.vrnoa =$vrnoa AND m.etype = '" . $etype . "' and m.company_id=$company_id and d.`type`='add')
			) rcp on d.item_id=rcp.item_id
			INNER JOIN item i ON i.item_id = rcp.ritem
			INNER JOIN item ic ON ic.item_id = rcp.item_id 
			INNER JOIN category c ON c.catid = ic.catid and c.name ='FINISHED PRODUCTS'
			left join(
				select ifnull(sum(qty),0) as qty,ifnull(sum(weight),0) as weight,item_id
				from stockdetail
				group by item_id
				) as stk on stk.item_id=i.item_id
		WHERE m.vrnoa =$vrnoa AND m.etype = '" . $etype . "' and m.company_id=$company_id and d.`type`='add' ;");
		} else if ($type == 'spare_parts') {
			$result = $this->db->query("SELECT i.uitem_des,party.name party_name,m.vrno, m.vrnoa, m.transporter_id,m.discount,m.discp,m.tax,m.taxpercent,m.expense,m.exppercent,m.paid, m.uid, m.party_id, DATE(m.vrdate) vrdate, m.remarks, m.etype, m.noted_by, m.namount, m.pub_add, d.item_id, d.godown_id, ROUND(d.qty, 2) qty,d.rate,d.amount, ROUND(d.bundle, 2) bundle, ROUND(d.weight, 2) weight, d.type, i.item_des AS item_name, i.uom, dep.name AS dept_name, rcp.item_id AS finish_item, IFNULL(rcp.rid,0) recipeid,ifnull(stk.qty,0) as stkqty,ifnull(stk.weight,0) as stkweight
			FROM ordermain AS m
			INNER JOIN orderdetail AS d ON m.oid = d.oid
			INNER JOIN item i ON i.item_id = d.item_id
			INNER JOIN department dep ON dep.did = d.godown_id
			INNER JOIN party party ON party.pid=m.party_id
			LEFT JOIN recipemain rcp ON rcp.item_id=d.item_id
			left join(
				select ifnull(sum(qty),0) as qty,ifnull(sum(weight),0) as weight,item_id
				from stockdetail
				group by item_id
				) as stk on stk.item_id=i.item_id
		WHERE m.vrnoa =$vrnoa AND m.etype = '" . $etype . "' and m.company_id=$company_id AND i.item_id NOT IN (
			SELECT rcp.item_id
			FROM recipemain rcp
			INNER JOIN item ic ON ic.item_id = rcp.item_id
			INNER JOIN category c ON c.catid = ic.catid and c.name ='FINISHED PRODUCTS'
			) AND d.`type`='add'
            ORDER BY d.type,d.odid");
		} else {
			$result = $this->db->query("SELECT i.uitem_des,party.name party_name,m.vrno, m.vrnoa, m.transporter_id,m.discount,m.discp,m.tax,m.taxpercent,m.expense,m.exppercent,m.paid, m.uid, m.party_id, DATE(m.vrdate) vrdate, m.remarks, m.etype, m.noted_by, m.namount, m.pub_add, d.item_id, d.godown_id, ROUND(d.qty, 2) qty,d.rate,d.amount, ROUND(d.bundle, 2) bundle, ROUND(d.weight, 2) weight, d.type, i.item_des AS item_name, i.uom, dep.name AS dept_name,ifnull(stk.qty,0) as stkqty,ifnull(stk.weight,0) as stkweight
		FROM ordermain AS m
		INNER JOIN orderdetail AS d ON m.oid = d.oid
		INNER JOIN item i ON i.item_id = d.item_id
		INNER JOIN department dep ON dep.did = d.godown_id
		INNER JOIN party party ON party.pid=m.party_id
		left join(
			select ifnull(sum(qty),0) as qty,ifnull(sum(weight),0) as weight,item_id
			from stockdetail
			group by item_id
			) as stk on stk.item_id=i.item_id
	WHERE m.vrnoa = $vrnoa AND m.etype = '" . $etype . "' AND m.company_id=$company_id AND d.`type`='less'
	ORDER BY d.type,d.odid");
		}
		if ($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetch_loading_Stock($order_no, $company_id)
	{

		$result = $this->db->query("select d.type,i.item_id,i.item_des item_name,ifnull(sum(d.qty),0) qty,ifnull(sum(d.weight),0) weight, ifnull(sd.stqty,0) stqty,ifnull(sd.stweight,0) stweight from orderdetail d
		inner join item i on i.item_id=d.item_id
		inner join ordermain m on m.oid=d.oid 
		left join  (select item_id,ifnull(sum(qty),0) stqty,ifnull(sum(weight),0) stweight from stockdetail group by item_id) sd on sd.item_id=i.item_id
		where m.ordno=$order_no and m.company_id=$company_id and m.etype in('order_parts','order_loading') and d.type<>'less'
		group by d.type,i.item_id,i.item_des having ifnull(sum(d.qty),0)<>0");

		if ($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}




	public function fetch_parts($vrnoa, $etype, $company_id)
	{
		$result = $this->db->query("SELECT party.name party_name,m.vrno, m.vrnoa, m.transporter_id,m.discount,m.discp,m.tax,m.taxpercent,m.expense,m.exppercent,m.paid, m.uid, m.party_id, DATE(m.vrdate) vrdate, m.remarks, m.etype, m.noted_by, m.namount, m.pub_add, d.item_id, d.godown_id, ROUND(d.qty, 2) qty,d.rate,d.amount, ROUND(d.bundle, 2) bundle, ROUND(d.weight, 2) weight, d.type, i.item_des AS item_name, i.uom, dep.name AS dept_name FROM ordermain AS m INNER JOIN orderdetail AS d ON m.oid = d.oid INNER JOIN item i ON i.item_id = d.item_id INNER JOIN department dep ON dep.did = d.godown_id INNER JOIN party party on party.pid=m.party_id WHERE m.vrnoa = $vrnoa AND m.etype = '" . $etype . "' and m.company_id=$company_id  order by d.type,d.odid");
		if ($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}


	public function fetchPurchaseOrder($vrnoa, $etype)
	{

		$result = $this->db->query("SELECT m.vrno, m.vrnoa, m.vrdate, m.party_id, m.remarks, m.payment, m.order_by, m.noted_by, m.shade_card, m.approved_by, m.etype, ROUND(m.amount, 2) amount, ROUND(m.discountp, 2) discountp, ROUND(m.discount, 2) discount, ROUND(m.namount, 2) namount, d.item_id, d.godown_id, ROUND(d.qty, 2) item_qty, ROUND(d.rate, 2) item_rate, ROUND(d.amount, 2) item_amount, ROUND(d.gstrate, 2) item_gstrate, ROUND(d.gstamount, 2) item_gstamount, ROUND(d.netamount, 2) item_netamount, i.item_des item_name, ROUND(d.weight, 2) weight, d.type status FROM ordermain AS m INNER JOIN orderdetail AS d ON m.oid = d.oid INNER JOIN item i ON i.item_id = d.item_id WHERE m.vrnoa = $vrnoa AND etype = '" . $etype . "'");
		if ($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchInwardGatePass($vrnoa, $etype)
	{

		$result = $this->db->query("SELECT m.vrno, m.vrnoa, m.vrdate, m.party_id, m.remarks, m.payment, m.order_by, m.noted_by, m.approved_by, m.etype, m.party_id_co, d.item_id, d.godown_id, ROUND(d.qty) qty, d.uom, dep.name AS 'dept_name', i.description AS 'item_name' FROM ordermain m INNER JOIN orderdetail d ON m.oid = d.oid LEFT JOIN department dep ON dep.did = d.godown_id INNER JOIN item i ON i.item_id = d.item_id WHERE m.vrnoa = $vrnoa AND etype = '" . $etype . "'");
		if ($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}
	public function fetchInwardGatePass22($vrnoa, $etype, $company_id)
	{

		$result = $this->db->query("SELECT m.vrno, m.vrnoa, m.vrdate, m.party_id,d.weight, m.remarks, m.etype, d.item_id, d.godown_id, ROUND(d.qty) qty, d.uom, dep.name AS 'dept_name', i.item_des AS 'item_name' FROM stockmain m INNER JOIN stockdetail d ON m.stid = d.stid LEFT JOIN department dep ON dep.did = d.godown_id INNER JOIN item i ON i.item_id = d.item_id WHERE m.vrnoa = $vrnoa AND m.etype = '" . $etype . "' and m.company_id='$company_id'");
		if ($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}
	// this function is also using in inward and outward gatepass voucher.......
	public function delete($vrnoa, $etype, $company_id)
	{

		if ($etype == 'sale') {
			$this->db->where(array('etype' => $etype, 'dcno' => $vrnoa, 'company_id' => $company_id));
			$result = $this->db->delete('pledger');

			$this->db->where(array('etype' => $etype, 'vrnoa' => $vrnoa, 'company_id' => $company_id));
			$resultStock = $this->db->get('stockmain');

			if ($resultStock->num_rows() != 0) {
				$resultStock = $resultStock->row_array();
				$stid = $result['stid'];

				$this->db->where(array('etype' => $etype, 'vrnoa' => $vrnoa));
				$this->db->delete('stockmain');
				$this->db->where(array('stid' => $stid));
				$this->db->delete('stockdetail');

				return true;
			}
		} else if ($etype == 'spare_parts') {

			$this->db->where(array('etype' => $etype, 'dcno' => $vrnoa, 'company_id' => $company_id));
			$result = $this->db->delete('pledger');
			$this->db->where(array('etype' => $etype, 'order_vrno' => $vrnoa, 'company_id' => $company_id));
			$result = $this->db->get('stockmain');

			if ($result->num_rows() == 0) { } else {

				$result = $result->row_array();
				$stid = $result['stid'];

				$this->db->where(array('etype' => $etype, 'order_vrno' => $vrnoa));
				$this->db->delete('stockmain');
				$this->db->where(array('stid' => $stid));
				$this->db->delete('stockdetail');
			}
		} else if ($etype == 'order_loading') {

			$this->db->where(array('etype' => $etype, 'vrnoa' => $vrnoa, 'company_id' => $company_id));
			$resultStock = $this->db->get('stockmain');

			if ($resultStock->num_rows() != 0) {
				$resultStock = $resultStock->row_array();
				$stid = $resultStock['stid'];

				$this->db->where(array('etype' => $etype, 'vrnoa' => $vrnoa));
				$this->db->delete('stockmain');
				$this->db->where(array('stid' => $stid));
				$this->db->delete('stockdetail');

				return true;
			}
		} else if ($etype == 'gp_in' || $etype == 'gp_out') {

			$this->db->where(array('etype' => $etype, 'vrnoa' => $vrnoa, 'company_id' => $company_id));
			$resultStock = $this->db->get('stockmain');

			if ($resultStock->num_rows() != 0) {
				$resultStock = $resultStock->row_array();
				$stid = $resultStock['stid'];

				$this->db->where(array('etype' => $etype, 'vrnoa' => $vrnoa));
				$this->db->delete('stockmain');
				$this->db->where(array('stid' => $stid));
				$this->db->delete('stockdetail');

				return true;
			}
		}

		$this->db->where(array('etype' => $etype, 'vrnoa' => $vrnoa, 'company_id' => $company_id));
		$result = $this->db->get('ordermain');

		if ($result->num_rows() == 0) {
			return false;
		} else {

			$result = $result->row_array();
			$oid = $result['oid'];

			$this->db->where(array('etype' => $etype, 'vrnoa' => $vrnoa));
			$this->db->delete('ordermain');
			$this->db->where(array('oid' => $oid));
			$this->db->delete('orderdetail');

			return true;
		}
	}

	public function fetchOrders($from, $to, $type)
	{

		if ($type == 'new') {
			$result = $this->db->query("SELECT oid,ordermain.status, vrnoa, date(vrdate) vrdate, party.name, city, party.cityarea, ordermain.remarks FROM ordermain INNER JOIN party ON party.pid = ordermain.party_id WHERE STATUS = 'new' AND ordermain.etype = 'sale_order' order by vrnoa DESC ");
			return $result->result_array();
		} else if ($type == 'running') {
			$result = $this->db->query("SELECT oid,ordermain.status, vrnoa, date(vrdate) vrdate, party.name, city, party.cityarea, ordermain.remarks FROM ordermain INNER JOIN party ON party.pid = ordermain.party_id WHERE STATUS = 'running' AND vrnoa not in (select ordno from ordermain where etype='order_parts' ) AND ordermain.etype = 'sale_order' order by ordermain.vrnoa");
			return $result->result_array();
		} else if ($type == 'running_loading') {
			$result = $this->db->query("SELECT oid,ordermain.status, vrnoa, date(vrdate) vrdate, party.name, city, party.cityarea, ordermain.remarks FROM ordermain INNER JOIN party ON party.pid = ordermain.party_id WHERE STATUS = 'running' AND vrnoa  in (select ordno from ordermain where etype='order_parts' ) AND ordermain.etype = 'sale_order' order by ordermain.vrnoa");
			return $result->result_array();
		} else if ($type == 'sale') {
			$result = $this->db->query("SELECT oid,ordermain.status, vrnoa, date(vrdate) vrdate, party.name, city, party.cityarea, ordermain.remarks FROM ordermain INNER JOIN party ON party.pid = ordermain.party_id WHERE vrnoa not in (select ordno from ordermain where etype='sale' ) AND ordermain.etype = 'sale_order' order by ordermain.vrnoa");
			return $result->result_array();
		} else {
			$result = $this->db->query("SELECT oid,ordermain.status, vrnoa, date(vrdate) vrdate, party.name, city, party.cityarea, ordermain.remarks FROM ordermain INNER JOIN party ON party.pid = ordermain.party_id WHERE ordermain.etype = 'sale_order' order by ordermain.vrnoa");
			return $result->result_array();
		}
	}
	public function Loading_Stock($company_id, $order_no)
	{

		$result = $this->db->query("select type,item_id,ifnull(sum(qty),0) qty,ifnull(sum(weight),0) weight from orderdetail where oid in (select oid from ordermain where etype in('order_parts','order_loading') and ordno=$order_no ) group by type,item_id");
		return $result->result_array();
	}


	public function updateOrderStatus($oids)
	{
		foreach ($oids as $oid) {
			$this->db->query("UPDATE ordermain SET status = 'running' WHERE etype = 'sale_order' AND oid = $oid");
		}
		return true;
	}

	public function fetchAllDistinctMain($field)
	{
		$result = $this->db->query("SELECT DISTINCT $field FROM ordermain WHERE $field <> '' ");
		return $result->result_array();
	}

	public function fetchRemaining($vrnoa, $etype, $company_id)
	{


		// $sql = "SELECT  d.type,m.party_id_co,g.name AS dept_name,m.vrno,m.uid,m.paid, m.vrnoa, m.vrdate,m.taxpercent,m.exppercent,m.tax,m.discp,m.discount, m.party_id, m.bilty_no, m.bilty_date, m.received_by, m.transporter_id, m.remarks, ROUND(m.namount, 2) namount, ROUND(m.discp, 2) discp, ROUND(m.discount, 2) discount, ROUND(m.expense, 2) expense, m.officer_id, d.item_id, d.godown_id, ROUND(d.qty, 2) AS 's_qty', ROUND(d.qtyf, 2) AS s_qtyf, ROUND(d.rate, 2) AS 's_rate', ROUND(d.amount, 2) AS 's_amount', ROUND(d.damount, 2) AS 's_damount', ROUND(d.discount, 2) AS 's_discount', ROUND(d.netamount, 2) AS 's_net', i.item_des AS 'item_name',i.uom, d.weight, IFNULL(d.weight,0)- IFNULL(stk.st_weight,0) AS st_weight, IFNULL(d.qty,0)- IFNULL(stk.st_qty,0) AS st_qty, IFNULL(stk.st_weight,0) AS st_weight1, IFNULL(stk.st_qty,0) AS st_qty1,i.item_code,i.uitem_des
		// 		FROM ordermain AS m
		// 		INNER JOIN orderdetail AS d ON m.oid = d.oid
		// 		INNER JOIN item AS i ON i.item_id = d.item_id
		// 		LEFT JOIN department AS g ON g.did = d.godown_id
		// 		LEFT JOIN (
		// 		SELECT d.inv_no,d.item_id, IFNULL(SUM(d.weight),0) AS st_weight, IFNULL(SUM(d.qty),0) AS st_qty
		// 		FROM stockmain AS m
		// 		INNER JOIN stockdetail AS d ON m.stid = d.stid
		// 		INNER JOIN item AS i ON i.item_id = d.item_id
		// 		WHERE m.etype='purchase' AND d.inv_no=$vrnoa AND m.company_id=$company_id
		// 		GROUP BY d.inv_no,d.item_id
		// 				) AS stk ON stk.inv_no=m.vrnoa AND stk.item_id=d.item_id
		// 		WHERE m.company_id = $company_id AND m.etype = '$etype' AND vrnoa=$vrnoa AND IF(i.uom='kg' || i.uom='kgs', IFNULL(d.weight,0)- IFNULL(stk.st_weight,0)>=0, IFNULL(d.qty,0)- IFNULL(stk.st_qty,0)>0)";
		$sql = "SELECT *, o_weight-m_weight AS balance_weight , o_qty - m_qty as balance_qty
	FROM (
		SELECT i.uom,m.party_id, d.rate,IFNULL(SUM(d.qty),0) o_qty, SUM(d.weight) o_weight , d.item_id,(
			SELECT IFNULL(SUM(sd.weight),0)
			FROM stockmain sm
			INNER JOIN stockdetail sd ON sm.stid=sd.stid
			WHERE sm.etype = 'purchase' and sd.inv_no = m.vrnoa and sd.item_id = d.item_id and sm.company_id=" . $company_id . ") m_weight
,(
	SELECT IFNULL(SUM(sd.qty),0)
	FROM stockmain sm
	INNER JOIN stockdetail sd ON sm.stid=sd.stid
	WHERE sm.etype = 'purchase' and sd.inv_no = m.vrnoa and sd.item_id = d.item_id and sm.company_id=" . $company_id . ") m_qty
FROM ordermain m
INNER JOIN orderdetail d ON m.oid=d.oid
inner join item i on i.item_id = d.item_id
WHERE m.etype = '$etype' and m.vrnoa =" . $vrnoa . " and m.company_id=" . $company_id . "
GROUP BY d.item_id,m.vrnoa) as datas";
		// having balance_weight <> '(NULL)'";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchPendingContracts($companyId)
	{

		$sql = "SELECT *, o_weight-m_weight AS balance_weight, o_qty - m_qty AS balance_qty
	FROM (
		SELECT m.vrnoa,p.name,i.item_des,i.uom, d.rate, d.amount,SUM(d.qty) o_qty, SUM(d.weight) o_weight, d.item_id,(
			SELECT IFNULL(SUM(sd.weight),0)
			FROM stockmain sm
			INNER JOIN stockdetail sd ON sm.stid=sd.stid
			WHERE sm.etype = 'purchase' AND sd.inv_no = m.vrnoa AND sd.item_id = d.item_id AND sm.company_id=" . $companyId . ") m_weight
,(
	SELECT IFNULL(SUM(sd.qty),0)
	FROM stockmain sm
	INNER JOIN stockdetail sd ON sm.stid=sd.stid
	WHERE sm.etype = 'purchase' AND sd.inv_no = m.vrnoa AND sd.item_id = d.item_id AND sm.company_id=" . $companyId . ") m_qty
FROM ordermain m
INNER JOIN orderdetail d ON m.oid=d.oid
INNER JOIN item i ON i.item_id = d.item_id
inner join party p on p.pid = m.party_id
WHERE m.etype = 'purchasecontract' AND  m.company_id=" . $companyId . "
GROUP BY d.item_id , m.vrnoa) AS datas";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}
}

/* End of file orders.php */
/* Location: ./application/models/orders.php */
