<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Items extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function getMaxId() {

		$this->db->select_max('item_id');
		$result = $this->db->get('item');
		$result = $result->row_array();
		return $result['item_id'];
	}

	public function getMaxCatId() {
		$this->db->select_max('catid');
		$result = $this->db->get('category');
		$result = $result->row_array();
		return $result['catid'];
	}

	public function getMaxSubCatId() {
		$this->db->select_max('subcatid');
		$result = $this->db->get('subcategory');
		$result = $result->row_array();
		return $result['subcatid'];
	}

	public function getMaxBrandId() {
		$this->db->select_max('bid');
		$result = $this->db->get('brand');
		$result = $result->row_array();
		return $result['bid'];
	}

	public function getMaxRouteId() {
		$this->db->select_max('route_id');
		$result = $this->db->get('route');
		$result = $result->row_array();
		return $result['route_id'];
	}

	public function getMaxRecipeId() {
		$this->db->select_max('rid');
		$result = $this->db->get('recipemain');
		$result = $result->row_array();
		return $result['rid'];
	}

	public function fetchByCol($col) {

		$result = $this->db->query("SELECT DISTINCT $col FROM item");
		return $result->result_array();
	}
	public function fetchItemByBrand($crit)
    {
    	
        $result = $this->db->query("SELECT i.item_id,i.item_code,i.item_des,i.grweight,i.cost_price,i.srate,i.srate1,i.srate2,i.srate3,i.srate4,i.self_freight_rate,i.bilty_freight_rate,i.gari_freight_rate,i.discount
									from item i
                            		WHERE 1=1 $crit ;");
        // if ( $result->num_rows() > 0 )
        // {
            return $result->result_array();
        // }
        // else
        // {
        //     return false;
        // }
    }

    public function updateItemsByIds($itemDetail, $status, $updateItemHistory)
    {
  //   	foreach ($recipedetail as $detail) {
		// 	$detail['rid'] = $rid;
		// 	$this->db->insert('recipedetail', $detail);
		// }

        foreach ($itemDetail as $detail)
        {
            $itemId = $detail['item_id'];
            unset($detail['item_id']);

            // DB::table('item')
            //     ->where(array('item_id' => $itemId))
            //     ->update($detail);
            
            $this->db->where('item_id', $itemId);
			$affect = $this->db->update('item', $detail);

        }

        if($status == 1){
           // DB::table('update_item_history')->insert($updateItemHistory);
           $this->db->insert('update_item_history', $updateItemHistory);
        }
        return true;
    }


	public function save( $item ) {

		if ( isset($_FILES['photo']) && $_FILES['photo']['size'] > 0) {
			$item['photo'] = $this->upload_photo( $item );
		} else {
			unset($item['photo']);
		}

		$item['item_code'] = $this->genItemStr($item['subcatid'],$item['item_id']);

		$this->db->where(array(
								'item_id' => $item['item_id']
							));
		$result = $this->db->get('item');

		$affect = 0;
		$item_id = "";
		if ($result->num_rows() > 0 ) {

			$this->db->where('item_id', $item['item_id']);
			$affect = $this->db->update('item', $item);

			$item_id = $item['item_id'];
		} else {

			unset($item['item_id']);
			$this->db->insert('item', $item);
			$affect = $this->db->affected_rows();

			$item_id = $this->db->insert_id();
		}

		if ( $affect === 0 ) {
			return false;
		} else {
			return $item_id;
		}
	}

	public function upload_photo( $photo ){
		$uploadsDirectory = ($_SERVER['DOCUMENT_ROOT'] . '/sttoka/assets/uploads/items/');

		$errors = array(1 => 'php.ini max file size exceeded',
		                2 => 'html form max file size exceeded',
		                3 => 'file upload was only partial',
		                4 => 'no file was attached');

		($_FILES['photo']['error'] == 0)
		    		or die($errors[$_FILES['photo']['error']]);

		@is_uploaded_file($_FILES['photo']['tmp_name'] )
					or die('Not an HTTP upload');

		@getimagesize($_FILES['photo']['tmp_name'])
		    or die('Only image uploads are allowed');

		$now = time();
		while(file_exists($uploadFilename = $uploadsDirectory.$now.'-'.$_FILES['photo']['name']))
		{
		    $now++;
		}

		// now let's move the file to its final location and allocate the new filename to it
		move_uploaded_file($_FILES['photo']['tmp_name'], $uploadFilename) or die('Error uploading file');

		$path_parts = explode('/', $uploadFilename);
		$uploadFilename = $path_parts[count($path_parts) - 1];
		return $uploadFilename;
	}

	public function fetch( $item_id ) {

		$this->db->where(array(
								'item_id' => $item_id
							));
		$result = $this->db->get('item');

		if ( $result->num_rows() > 0 ) {
			return $result->row_array();
		} else {
			return false;
		}
	}

	public function fetchCategory( $catid ) {

		$this->db->where(array('catid' => $catid));
		$result = $this->db->get('category');

		if ( $result->num_rows() > 0 ) {
			return $result->row_array();
		} else {
			return false;
		}
	}

	public function fetchSubCategory( $subcatid ) {

		$this->db->where(array('subcatid' => $subcatid));
		$result = $this->db->get('subcategory');

		if ( $result->num_rows() > 0 ) {
			return $result->row_array();
		} else {
			return false;
		}
	}

	public function fetchBrand( $bid ) {

		$this->db->where(array('bid' => $bid));
		$result = $this->db->get('brand');

		if ( $result->num_rows() > 0 ) {
			return $result->row_array();
		} else {
			return false;
		}
	}

	public function fetchRoute( $route_id ) {

		$this->db->where(array('route_id' => $route_id));
		$result = $this->db->get('route');

		if ( $result->num_rows() > 0 ) {
			return $result->row_array();
		} else {
			return false;
		}
	}

    public function fetchAllItems() {

        $result = $this->db->query("SELECT * from item");

        return $result->result_array();
    }

	public function fetchAll($activee =-1) {
		$sts='';
		if($activee==-1){
			$sts='';
		}else{
			$sts=' where item.active=1 ';
		}

		// $result = $this->db->query("SELECT item.*, category.name AS 'category_name', subcategory.name AS 'subcategory_name', brand.name AS 'brand_name', ifnull(sd.stqty,0) as stqty, ifnull(sd.stweight,0) as stweight FROM item
		// 		INNER JOIN category ON item.catid = category.catid 
		// 		INNER JOIN subcategory ON item.subcatid = subcategory.subcatid 
		// 		INNER JOIN brand ON item.bid = brand.bid
		// 		left join  (select item_id,ifnull(sum(qty),0) stqty,ifnull(sum(weight),0) stweight from stockdetail group by item_id) sd on sd.item_id=item.item_id $sts");

		// $result = $this->db->query("SELECT item.*, category.name AS 'category_name', subcategory.name AS 'subcategory_name', brand.name AS 'brand_name' FROM item
		// 		INNER JOIN category ON item.catid = category.catid 
		// 		INNER JOIN subcategory ON item.subcatid = subcategory.subcatid 
		// 		INNER JOIN brand ON item.bid = brand.bid
		// 		 $sts");

		$result  = $this->db->query("SELECT item.*, category.name AS 'category_name', subcategory.name AS 'subcategory_name', brand.name AS 'brand_name', IFNULL(sd.stqty,0) AS stqty, IFNULL(sd.stweight,0) AS stweight
									FROM item
									INNER JOIN category ON item.catid = category.catid
									INNER JOIN subcategory ON item.subcatid = subcategory.subcatid
									INNER JOIN brand ON item.bid = brand.bid
									LEFT JOIN (
									SELECT stockdetail.item_id, IFNULL(SUM(stockdetail.qty),0) stqty, IFNULL(SUM(stockdetail.weight),0) stweight
									FROM stockdetail
									INNER JOIN department ON department.did = stockdetail.godown_id
									inner join stockmain on stockmain.stid =  stockdetail.stid
									GROUP BY stockdetail.item_id) sd ON sd.item_id=item.item_id $sts
									order by item.item_id");

		
		return $result->result_array();
	}
	public function fetchAllItem($crit){
		$result = $this->db->query("SELECT item_id,item_des from item where item_des like '%".$crit."%' and active='1'");
		return $result->result_array();

	}
	public function fetchAll_report($from,$to,$orderby,$status) {
		
		if ($status=='all_item'){
			$result = $this->db->query("SELECT item.*, category.name AS 'category_name', subcategory.name AS 'subcategory_name', brand.name AS 'brand_name' FROM item INNER JOIN category ON item.catid = category.catid INNER JOIN subcategory ON item.subcatid = subcategory.subcatid INNER JOIN brand ON item.bid = brand.bid order by $orderby ");
		}else{
			$result = $this->db->query("SELECT item.*, category.name AS 'category_name', subcategory.name AS 'subcategory_name', brand.name AS 'brand_name' FROM item INNER JOIN category ON item.catid = category.catid INNER JOIN subcategory ON item.subcatid = subcategory.subcatid INNER JOIN brand ON item.bid = brand.bid where item.active=$status order by $orderby ");
		}
		return $result->result_array();
	}

	public function fetchItemsByStock() {
		$result = $this->db->query("SELECT i.item_code,i.item_id, i.item_des as description, round(SUM(d.qty)) stock, round(d.prate) prate FROM stockmain m INNER JOIN stockdetail d ON m.stid = d.stid INNER JOIN item i on d.item_id = i.item_id GROUP BY d.item_id");
		return $result->result_array();
	}

	public function fetchItemLedgerReport($from, $to, $item_id, $company_id , $godown_id) {
		$result = $this->db->query("SELECT  party.name as party_name, m.vrnoa, m.etype, DATE(m.vrdate) date, i.description, i.item_des, m.remarks, dep.name, d.qty, d.weight FROM stockmain m INNER JOIN stockdetail d ON m.stid = d.stid INNER JOIN item i ON i.item_id = d.item_id LEFT JOIN department dep ON dep.did = d.godown_id LEFT JOIN party as party on party.pid=m.party_id WHERE d.item_id = $item_id and d.godown_id=$godown_id AND m.company_id = $company_id  AND DATE(m.vrdate) >= '". $from ."' AND DATE(m.vrdate) <= '". $to ."' and (ifnull(d.type,'') not in ('add_fitting','less_fitting','less_helping1','add_helping1','add_helping2','less_helping2') or m.etype='navigation') ORDER BY m.vrdate, m.vrnoa, dep.name, i.description");

		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchItemOpeningStock($to, $item_id, $company_id , $godown_id)
	{
		$q = "SELECT IFNULL(SUM(qty), 0) as 'OPENING_QTY' ,IFNULL(SUM(weight), 0) as 'OPENING_WEIGHT' FROM stockmain as stockmain INNER JOIN stockdetail as stockdetail ON stockmain.stid = stockdetail.stid WHERE stockdetail.item_id = {$item_id} and stockdetail.godown_id = {$godown_id} AND DATE(stockmain.VRDATE) < '{$to}' AND stockmain.company_id={$company_id}";
		$query = $this->db->query( $q );

		return $query->result_array();
	}


	public function saveCategory($category) {

		$this->db->where(array('catid' => $category['catid']));
		$result = $this->db->get('category');

		if ($result->num_rows() > 0) {

			$this->db->where(array('catid' => $category['catid']));
			$this->db->update('category', $category);
		} else {
			unset($category['catid']);
			$this->db->insert('category', $category);
		}

		return true;
	}

	public function saveSubCategory($category) {

		$this->db->where(array('subcatid' => $category['subcatid']));
		$result = $this->db->get('subcategory');

		if ($result->num_rows() > 0) {
			$this->db->where(array('subcatid' => $category['subcatid']));
			$this->db->update('subcategory', $category);
		} else {
			unset($category['subcatid']);
			$this->db->insert('subcategory', $category);
		}

		return true;
	}

	public function saveRecipe( $recipe, $recipedetail, $rid ) {

		$this->db->where(array('rid' => $rid ));
		$result = $this->db->get('recipemain');

		$rid = "";
		if ($result->num_rows() > 0) {
		
			$result = $result->row_array();
			$rid = $result['rid'];

			$this->db->where(array('rid' => $rid));
			$this->db->update('recipemain', $recipe);

			// delete old items
			$this->db->where(array('rid' => $rid ));
			$this->db->delete('recipedetail');
		} else {
			$this->db->insert('recipemain', $recipe);
			$rid = $this->db->insert_id();
		}

		foreach ($recipedetail as $detail) {
			$detail['rid'] = $rid;
			$this->db->insert('recipedetail', $detail);
		}
		return true;
	}

	public function saveBrand($brand) {

		$this->db->where(array('bid' => $brand['bid']));
		$result = $this->db->get('brand');

		if ($result->num_rows() > 0) {

			$this->db->where(array('bid' => $brand['bid']));
			$this->db->update('brand', $brand);
		} else {
			unset($brand['bid']);
			$this->db->insert('brand', $brand);
		}

		return true;
	}

	public function saveRoute($route) {

		$this->db->where(array('route_id' => $route['route_id']));
		$result = $this->db->get('route');

		if ($result->num_rows() > 0) {

			$this->db->where(array('route_id' => $route['route_id']));
			$this->db->update('route', $route);
		} else {
			unset($route['route_id']);
			$this->db->insert('route', $route);
		}

		return true;
	}

	public function isCategoryAlreadySaved($category) {
		$result = $this->db->query("SELECT * FROM category WHERE catid <> ". $category['catid'] ." AND name = '". $category['name'] ."'");
		if ($result->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function isSubCategoryAlreadySaved($subcategory) {
		$result = $this->db->query("SELECT * FROM subcategory WHERE subcatid <> ". $subcategory['subcatid'] ." AND name = '". $subcategory['name'] ."'");
		if ($result->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function isBrandAlreadySaved($brand) {
		$result = $this->db->query("SELECT * FROM brand WHERE bid <> ". $brand['bid'] ." AND name = '". $brand['name'] ."'");
		if ($result->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function isRouteAlreadySaved($route) {
		$result = $this->db->query("SELECT * FROM route WHERE route_id <> ". $route['route_id'] ." AND name = '". $route['name'] ."'");
		if ($result->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function fetchAllCategories() {
		$result = $this->db->get("category");
		return $result->result_array();
	}
	public function fetchAllPhase() {
		$result = $this->db->get("phase");
		return $result->result_array();
	}


	public function fetchAllSubCategories() {
		$result = $this->db->query("SELECT subcategory.*, category.name AS 'category_name' FROM subcategory INNER JOIN category ON subcategory.catid = category.catid");
		return $result->result_array();
	}

	public function fetchAllBrands() {
		$result = $this->db->get("brand");
		return $result->result_array();
	}
	public function fetchAllRoutes() {
		$result = $this->db->get("route");
		return $result->result_array();
	}

	public function genItemStr($subcatid,$itemId)
	{
		$query = $this->db->query("SELECT codeid,category.catid, subcatid FROM category INNER JOIN subcategory ON category.catid = subcategory.catid WHERE subcategory.subcatid = $subcatid");
		if ($query->num_rows() > 0) {
			$result = $query->result_array();

			$catid = str_pad($result[0]['catid'], 2, '0', STR_PAD_LEFT);
			$subcatid = str_pad($result[0]['subcatid'], 2, '0', STR_PAD_LEFT);
			$codeid=$result[0]['codeid'];
			//$items_count = $this->count_items($subcatid);
			$items_count = $this->generateItemId($codeid,$itemId);
			$code = $items_count;
			$code = str_pad($code, 3, '0', STR_PAD_LEFT);

			return $codeid . '-' . $code;
		}
	}

	public function count_items($subcatid)
	{
		$query = $this->db->get_where('item', array('subcatid' => $subcatid));
		return $query->num_rows();
	}

	public function generateItemId($subCatId,$itemId)
	{
		$query = $this->db->query("select count(item_id) counter from item where left(item.item_code,5) ='".$subCatId."' and item_id =".$itemId);
		$counter = $query->row_array();

		if($counter['counter'] == 0){

			$query = "select max(right(item_code,3))+1 item_idcode  from item where left(item_code,5)='".$subCatId."'";//$this->db->get_where('item', array('left(item_code,5)' => $subCatId));
			$query=$this->db->query($query);
			$item_id=$query->row_array();
			return $item_id['item_idcode'];
		}

		
		$query = $this->db->query("select item_code from item where item_id=".$itemId);
		$itemId = $query->row_array();
		return substr($itemId['item_code'],6,3);
	}

	public function recipeExists($rid, $item_id) {
		
		$result = $this->db->query("SELECT * FROM recipemain WHERE item_id = $item_id AND rid <> $rid");

		if ($result->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function fetchRecipe( $rid ) {
		$result = $this->db->query("SELECT recipemain.*, recipedetail.uom, ROUND(recipedetail.qty, 5) qty, ROUND(recipedetail.weight, 5) weight, recipedetail.item_id AS 'ditem_id', concat(item.item_code,' ',item.item_des) item_des  FROM recipemain INNER JOIN recipedetail ON recipemain.rid = recipedetail.rid INNER JOIN item item ON item.item_id = recipedetail.item_id WHERE recipemain.rid = $rid");
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function deleteRecipe( $rid ) {

		$this->db->where(array('rid' => $rid ));
		$result = $this->db->get('recipemain');

		if ($result->num_rows() == 0) {
			return false;
		} else {
			$this->db->where(array('rid' => $rid ));
			$result = $this->db->delete('recipemain');
			$this->db->where(array('rid' => $rid ));
			$result = $this->db->delete('recipedetail');

			return true;
		}
	}
	public function getMinStockNotifs( $company_id )
	{
			// $query = "SELECT item.description, curr_stock, stock_notifs.min_level, order_value , (SELECT prate FROM stockmain INNER JOIN stockdetail ON stockmain.stid = stockdetail.stid WHERE item_id = item.item_id AND stockmain.company_id = $company_id ORDER BY stockmain.stid DESC LIMIT 1 ) as prate from stock_notifs INNER JOIN item ON stock_notifs.item_id = item.item_id WHERE stock_notifs.company_id = $company_id";
			$query = "SELECT item.description, curr_stock, stock_notifs.min_level, order_value, ( SELECT prate FROM stockmain INNER JOIN stockdetail ON stockmain.stid = stockdetail.stid WHERE item_id = item.item_id AND stockmain.company_id = $company_id ORDER BY stockmain.stid DESC LIMIT 1) AS prate, order_value, ( SELECT party.name
						FROM stockmain
						INNER JOIN stockdetail ON stockmain.stid = stockdetail.stid
						INNER JOIN party ON stockmain.party_id = party.pid
						WHERE item_id = item.item_id AND stockmain.company_id = $company_id
						ORDER BY stockmain.stid DESC
						LIMIT 1) AS supplier
						FROM stock_notifs
						INNER JOIN item ON stock_notifs.item_id = item.item_id
						WHERE stock_notifs.company_id = $company_id
					";
			$result = $this->db->query( $query );
			return $result->result_array();
	}
public function fetchStockOrderCount( $company_id )
{
	$query = "SELECT COUNT(*) as ORDERCOUNT FROM stock_notifs WHERE company_id = $company_id";
	$result = $this->db->query( $query );
	$row = $result->row_array();
	
	return $row['ORDERCOUNT'];
}

    public function paging($page_number, $search)
    {
        $pageSize = 10;
        $start = ($page_number - 1) * $pageSize;
        $sql = "SELECT item.*, category.name AS 'category_name', subcategory.name AS 'subcategory_name', brand.name AS 'brand_name',
                    ifnull(sd.stqty,0) as stqty, ifnull(sd.stweight,0) as stweight FROM item
                    INNER JOIN category ON item.catid = category.catid
                    INNER JOIN subcategory ON item.subcatid = subcategory.subcatid
                    INNER JOIN brand ON item.bid = brand.bid
                    left join  (
                        SELECT stockdetail.item_id, IFNULL(SUM(stockdetail.qty),0) stqty, IFNULL(SUM(stockdetail.weight),0) stweight
						FROM stockdetail
						INNER JOIN department ON department.did = stockdetail.godown_id
						inner join stockmain on stockmain.stid =  stockdetail.stid
						GROUP BY stockdetail.item_id) sd on sd.item_id=item.item_id
                    where item.active='1' and
                    (
                      item.item_id like '%" . $search . "%' or
                      item.item_des like '%" . $search . "%' or
                      item.item_code like '%" . $search . "%' or
                      item.uom like '%" . $search . "%'
                    )
                    limit " . $start . ", " . $pageSize . " ";

        $result = $this->db->query($sql);

		$data = array();
        if($result->num_rows() > 0)
        {
			foreach($result->result_array() as $row)
			{
				$data[] = array(
					"item_id" => $this->htmlspecialchars($row['item_id']),
					"op_stock" => $this->htmlspecialchars($row['op_stock']),
					"min_level" => $this->htmlspecialchars($row['min_level']),
					"max_level" => $this->htmlspecialchars($row['max_level']),
					"order_level" => $this->htmlspecialchars($row['order_level']),
					"open_date" => $this->htmlspecialchars($row['open_date']),
					"active" => $this->htmlspecialchars($row['active']),
					"srate" => $this->htmlspecialchars($row['srate']),
					"srate1" => $this->htmlspecialchars($row['srate1']),
					"srate2" => $this->htmlspecialchars($row['srate2']),
					"cost_price" => $this->htmlspecialchars($row['cost_price']),
					"discount" => $this->htmlspecialchars($row['discount']),
					"status" => $this->htmlspecialchars($row['status']),
					"item_code" => $this->htmlspecialchars($row['item_code']),
					"length" => $this->htmlspecialchars($row['length']),
					"fitting" => $this->htmlspecialchars($row['fitting']),
					"uom" => $this->htmlspecialchars($row['uom']),
					"item_des" => $this->htmlspecialchars($row['item_des']),
					"uitem_des" => $this->htmlspecialchars($row['uitem_des']),
					"uname" => $this->htmlspecialchars($row['uname']),
					"description" => $this->htmlspecialchars($row['description']),
					"model" => $this->htmlspecialchars($row['model']),
					"photo" => $this->htmlspecialchars($row['photo']),
					"size" => $this->htmlspecialchars($row['size']),
					"catid" => $this->htmlspecialchars($row['catid']),
					"item_detail_id" => $this->htmlspecialchars($row['item_detail_id']),
					"barcode" => $this->htmlspecialchars($row['barcode']),
					"department" => $this->htmlspecialchars($row['department']),
					"bid" => $this->htmlspecialchars($row['bid']),
					"made" => $this->htmlspecialchars($row['made']),
					"subcatid" => $this->htmlspecialchars($row['subcatid']),
					"paryt_id" => $this->htmlspecialchars($row['paryt_id']),
					"party_id_cr" => $this->htmlspecialchars($row['party_id_cr']),
					"netweight" => $this->htmlspecialchars($row['netweight']),
					"grweight" => $this->htmlspecialchars($row['grweight']),
					"uid" => $this->htmlspecialchars($row['uid']),
					"datetime1" => $this->htmlspecialchars($row['datetime1']),
					"company_id" => $this->htmlspecialchars($row['company_id']),
					"srate3" => $this->htmlspecialchars($row['srate3']),
					"srate4" => $this->htmlspecialchars($row['srate4']),
					"category_name" => $this->htmlspecialchars($row['category_name']),
					"subcategory_name" => $this->htmlspecialchars($row['subcategory_name']),
					"brand_name" => $this->htmlspecialchars($row['brand_name']),
					"stqty" => $this->htmlspecialchars($row['stqty']),
					"stweight" => $this->htmlspecialchars($row['stweight'])
				);
			}

			return $data;
        }
        else
        {
            return false;
        }
    }

	public function htmlspecialchars($value)
	{
		return htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
	}

    public function total_records($search)
    {
        $sql = "SELECT count(item.item_id) as total FROM item
                    INNER JOIN category ON item.catid = category.catid
                    INNER JOIN subcategory ON item.subcatid = subcategory.subcatid
                    INNER JOIN brand ON item.bid = brand.bid
                    left join
                      (
                        SELECT stockdetail.item_id, IFNULL(SUM(stockdetail.qty),0) stqty, IFNULL(SUM(stockdetail.weight),0) stweight
						FROM stockdetail
						INNER JOIN department ON department.did = stockdetail.godown_id
						inner join stockmain on stockmain.stid =  stockdetail.stid
						GROUP BY stockdetail.item_id) sd on sd.item_id=item.item_id
                    where item.active=1 and
                    (
                      item.item_id like '%" . $search . "%' or
                      item.item_des like '%" . $search . "%' or
                      item.item_code like '%" . $search . "%' or
                      item.uom like '%" . $search . "%'
                    )";

        $result = $this->db->query($sql);
        if($result->num_rows() > 0)
        {
            $result= $result->row_array();
            return $result['total'];
        }
        else
        {
            return false;
        }
    }

    public function getItemInfo($itemId)
    {
        $sql = "SELECT item.*, category.name AS 'category_name', subcategory.name AS 'subcategory_name', brand.name AS 'brand_name',
                    ifnull(sd.stqty,0) as stqty, ifnull(sd.stweight,0) as stweight FROM item
                    INNER JOIN category ON item.catid = category.catid
                    INNER JOIN subcategory ON item.subcatid = subcategory.subcatid
                    INNER JOIN brand ON item.bid = brand.bid
                    left join
                      (
                        SELECT stockdetail.item_id, IFNULL(SUM(stockdetail.qty),0) stqty, IFNULL(SUM(stockdetail.weight),0) stweight
						FROM stockdetail
						INNER JOIN department ON department.did = stockdetail.godown_id
						inner join stockmain on stockmain.stid =  stockdetail.stid
						GROUP BY stockdetail.item_id) sd on sd.item_id = item.item_id
                    where item.active=1 and item.item_id = '" . $itemId . "' ";

        $result = $this->db->query($sql);
        if($result->num_rows() > 0)
        {
            return $result->result_array();
        }
        else
        {
            return false;
        }
    }

    public function searchItem($search)
    {
        $result = $this->db->query("SELECT item.*, category.name AS 'category_name', subcategory.name AS 'subcategory_name', brand.name AS 'brand_name',
                    	ifnull(sd.stqty,0) as stqty, ifnull(sd.stweight,0) as stweight FROM item
                        left JOIN category ON item.catid = category.catid
                        left JOIN subcategory ON item.subcatid = subcategory.subcatid
                        left JOIN brand ON item.bid = brand.bid
                        left join  (select item_id,ifnull(sum(qty),0) stqty,ifnull(sum(weight),0) stweight
                        from stockdetail group by item_id) sd on sd.item_id = item.item_id
                        where item.active = 1 and  (
								                      item.item_id like '" . $search . "%' or
								                      item.item_des like '" . $search . "%' or
								                      item.item_code like '" . $search . "%'
								                    ) order by item.item_des
                        limit 0, 50");

        return $result->result_array();
    }

    public function getItemInfoById($id)
    {
        $result = $this->db->query("SELECT item.*, category.name AS 'category_name', subcategory.name AS 'subcategory_name', brand.name AS 'brand_name',
                    	ifnull(sd.stqty,0) as stqty, ifnull(sd.stweight,0) as stweight FROM item
                        left JOIN category ON item.catid = category.catid
                        left JOIN subcategory ON item.subcatid = subcategory.subcatid
                        left JOIN brand ON item.bid = brand.bid
                        left join  (select item_id,ifnull(sum(qty),0) stqty,ifnull(sum(weight),0) stweight
                        from stockdetail group by item_id) sd on sd.item_id = item.item_id
                        where item.active = 1 and item.item_id = '" . $id . "' ");

        return $result->result_array();
    }

    public function fetchreceipereportdata($crit)
    {
        $result = $this->db->query("SELECT receipecosting_main.rid as vrno,items.item_des voucher,receipecosting_detail.weight,item.item_des receipe_item,receipecosting_detail.uom ,receipecosting_detail.qty ,item.cost_price ,ROUND((receipecosting_detail.qty)*item.cost_price , 2) amount
                    FROM recipemain receipecosting_main
                    INNER JOIN recipedetail receipecosting_detail ON receipecosting_main.rid = receipecosting_detail.rid
                    INNER JOIN item AS item ON item.item_id = receipecosting_detail.item_id
                    INNER JOIN item AS items ON items.item_id = receipecosting_main.item_id
                    where 1=1 $crit
                    ORDER BY receipecosting_main.rid");
        return $result->result_array();        
    }
}

/* End of file items.php */
/* Location: ./application/models/items.php */