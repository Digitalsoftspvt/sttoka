<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class finanicalyears extends CI_Model {

	public function getMaxId() {
		
		$cid=$this->session->userdata('company_id');
		$this->db->select_max('fn_id')->where(array('company_id'  => $cid));
		$result = $this->db->get('finanicalyear');

		$row = $result->row_array();
		$maxId = $row['fn_id'];

		return $maxId;
	}
	public function save( $finanicalyear ) {
		$cid=$this->session->userdata('company_id');
		$finanicalyear['company_id']=$this->session->userdata('company_id');
		$this->db->where(array('fn_id' => $finanicalyear['fn_id'],'company_id'  => $cid ));
		$result = $this->db->get('finanicalyear');

		$affect = 0;
		if ($result->num_rows() > 0) {

			$this->db->where(array('fn_id' => $finanicalyear['fn_id'],'company_id'  => $cid ));
			$result = $this->db->update('finanicalyear', $finanicalyear);
			$affect = $this->db->affected_rows();
		} else {

			unset($finanicalyear['fn_id']);
			$result = $this->db->insert('finanicalyear', $finanicalyear);
			$affect = $this->db->affected_rows();
		}

		if ($affect === 0) {
			return false;
		} else {
			return true;
		}
	}
	public function fetch( $fn_id ) {
		$cid=$this->session->userdata('company_id');
		$this->db->where(array('fn_id' => $fn_id ,'company_id'  => $cid));
		$result = $this->db->get('finanicalyear');
		if ( $result->num_rows() > 0 ) {
			return $result->row_array();
		} else {
			return false;
		}
	}
	public function fetchAll() {
		$cid=$this->session->userdata('company_id');
		
		//$this->db->where(array('company_id'  => $cid));
		$result = $this->db->get('finanicalyear');
		
		if ( $result->num_rows() > 0 ) {
		return $result->result_array();
		} else {
			return false;
		}

	}

	public function delete($fn_id) {
		$cid=$this->session->userdata('company_id');
		$this->db->where(array('fn_id' => $fn_id,'company_id'  => $cid));
		$result = $this->db->get('finanicalyear');

		if ( $result->num_rows() > 0 ) {
			$this->db->where(array('fn_id' => $fn_id));
			$this->db->delete('finanicalyear');

			return true;
		} else {
			return false;
		}
	}
}
/* End of file finanicalyears.php */
/* Location: ./application/models/transporters.php */