<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Filters extends CI_Model {

public function fetchfilters($groupname)
	{
		
		$result = $this->db->query("select * from filters where reportgroup = '". $groupname ."'");
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function fetchfilterdata($tablename,$id,$display,$condition)
	{
		$result = $this->db->query("select $display as 'displayvalue',$id as 'id' from $tablename $condition");
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}
	public function getreportdatafromdatabase($name)
    {
        $result = $this->db->query("SELECT *  FROM reports where reports_name = '".$name."'");
        if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
    }
    public function fetchAllForComponents($name)
    {
    	$result = $this->db->query("SELECT * FROM component WHERE report_name = '". $name ."'");
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
    }


}