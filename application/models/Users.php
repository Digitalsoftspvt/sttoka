<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function getMaxRoleGroupId() {

		$this->db->select_max('rgid');
		$result = $this->db->get('rolegroup');
		$result = $result->row_array();
		return $result['rgid'];
	}
	

	public function sendMessage( $mobile, $message )

	{

		$ptn = "/^[0-9]/";  // Regex

		$rpltxt = "92";  // Replacement string

		$mobile = preg_replace($ptn, $rpltxt, $mobile);



		// Create the SoapClient instance 

		$url = ZONG_API_SERVICE_URL; 

		// $soapClient = new SoapClient( $url);

		// var_dump($soapClient->__getFunctions());



		$post_string = '<?xml version="1.0" encoding="utf-8"?>'.

		'<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' .

		  '<soap:Body>'.

		    '<SendSingleSMS xmlns="http://tempuri.org/">'.

		      '<Src_nbr>' . ZONG_API_MOB . '</Src_nbr>'.

		      '<Password>' . ZONG_API_PASS . '</Password>'.

		      '<Dst_nbr>' . $mobile . '</Dst_nbr>'.

		      '<Mask>' . ZONG_API_MASK . '</Mask>'.

		      '<Message>' . $message . '</Message>'.

		    '</SendSingleSMS>'.

		  '</soap:Body>'.

		'</soap:Envelope>';



		$soap_do = curl_init(); 

		curl_setopt($soap_do, CURLOPT_URL,            $url );   

		curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10); 

		curl_setopt($soap_do, CURLOPT_TIMEOUT,        10); 

		curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );

		curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);  

		curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false); 

		curl_setopt($soap_do, CURLOPT_POST,           true ); 

		curl_setopt($soap_do, CURLOPT_POSTFIELDS,    $post_string); 

		curl_setopt($soap_do, CURLOPT_HTTPHEADER,     array('Content-Type: text/xml; charset=utf-8', 'Content-Length: '.strlen($post_string) ));



		$result = curl_exec($soap_do);

		$err = curl_error($soap_do);

		return $result;

	}



	public function getMaxId() {

		$this->db->select_max('uid');
		$result = $this->db->get('user');
		$result = $result->row_array();
		return $result['uid'];
	}

	public function saveRoleGroup( $rolegroup ) {

		$this->db->where(array('rgid' => $rolegroup['rgid']));
		$result = $this->db->get('rolegroup');

		$affect = 0;
		if ($result->num_rows() > 0) {

			$this->db->where(array('rgid' => $rolegroup['rgid'] ));
			$result = $this->db->update('rolegroup', $rolegroup);
			$affect = $this->db->affected_rows();
		} else {

			unset($rolegroup['rgid']);
			$result = $this->db->insert('rolegroup', $rolegroup);
			$affect = $this->db->affected_rows();
		}

		if ($affect === 0) {
			return false;
		} else {
			return true;
		}
	}
	
	// public function saveRoleGroup( $rolegroup ) {

	// 	$this->db->where(array('rgid' => $rolegroup['rgid']));
	// 	$result = $this->db->get('rolegroup');

	// 	$affect = 0;
	// 	if ($result->num_rows() > 0) {

	// 		$this->db->where(array('rgid' => $rolegroup['rgid'] ));
	// 		$result = $this->db->update('rolegroup', $rolegroup);
	// 		$affect = $this->db->affected_rows();
	// 	} else {

	// 		unset($rolegroup['rgid']);
	// 		$result = $this->db->insert('rolegroup', $rolegroup);
	// 		$affect = $this->db->affected_rows();
	// 	}

	// 	if ($affect === 0) {
	// 		return false;
	// 	} else {
	// 		return true;
	// 	}
	// }

	public function save( $user ) {

		$this->db->where(array('uid' => $user['uid']));
		$result = $this->db->get('user');

		$affect = 0;
		if ($result->num_rows() > 0) {

			$this->db->where(array('uid' => $user['uid'] ));
			$result = $this->db->update('user', $user);
			$affect = $this->db->affected_rows();
		} else {

			unset($user['uid']);
			$user['date'] = date('Y-m-d H:i:s');
			$result = $this->db->insert('user', $user);
			$affect = $this->db->affected_rows();
		}

		/*if ($affect === 0) {
			return false;
		} else {
			return true;
		}*/
		return true;
	}

	public function openDate( $user, $date) {

		$this->db->where(array('date_cl' => $date));
		$result = $this->db->delete('dateclose');

		$affect = 0;
		if ($this->db->affected_rows() > 0) {
			return true;
		} else{
			return false;
		}
	}

	public function closeDate( $data) {
		// die(print_r($data));
		$this->db->where(array('date_cl' => $data['date_cl']));
		$result = $this->db->get('dateclose');

		if ($result->num_rows() > 0) {
			$affect = 0;
		} else {

			$result = $this->db->insert('dateclose', $data);
			$affect = $this->db->affected_rows();
		}
		if ($affect > 0) {
			return true;
		} else{
			return false;
		}
	}

	

	public function fetchRoleGroup( $rgid ) {

		$this->db->where(array('rgid' => $rgid));
		$result = $this->db->get('rolegroup');
		if ( $result->num_rows() > 0 ) {
			return $result->row_array();
		} else {
			return false;
		}
	}

	public function fetch( $uid ) {

		$this->db->where(array('uid' => $uid));
		$result = $this->db->get('user');
		if ( $result->num_rows() > 0 ) {
			return $result->row_array();
		} else {
			return false;
		}
	}

	public function fetch_mobile($uname ) {

		$this->db->where(array('uname' => $uname));
		$result = $this->db->get('user');
		if ( $result->num_rows() > 0 ) {
			 return $result->row_array();
		} else {
			return false;
		}
	}

	public function fetchAll() {

		// $this->db->where(array('uid' => $uid));
		$result = $this->db->get('user');
			return $result->result_array();
		
	}

	public function fetchAllDateClose() {

		// $this->db->where(array('uid' => $uid));
		$result = $this->db->get('dateclose');
			return $result->result_array();
		
	}
	


	public function fetchAllRoleGroup() {
		$result = $this->db->get('rolegroup');
		if ( $result->num_rows() > 0 ) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function privillagesAssigned() {

		$result = $this->db->query("SELECT u.uid, u.fullname, r.name, r.desc, DATE(u.date) AS date FROM user AS u INNER JOIN rolegroup AS r ON u.rgid = r.rgid");
		return $result->result_array();
	}

	public function login($uname, $pass) {

		$result = $this->db->query("SELECT u.uid, u.fullname, r.rgid,u.company_id, r.name AS 'rolegroup_name', r.desc FROM user AS u INNER JOIN rolegroup AS r ON r.rgid = u.rgid WHERE u.uname = '". $uname ."' AND u.pass = '". $pass ."'");

		if ($result->num_rows() == 0) {
			return false;
		} else {
			return $result->row_array();
		}
	}
	public function has_match($username, $password)
			{
				$query = $this->db->query("SELECT * FROM user WHERE BINARY uname='{$username}' AND BINARY pass='{$password}'");
				// $query = $this->db->get_where('login', array(
				// 									  	    	'uname' => $username, 
				// 							  		  	    	'pass' => $password
				// 							 				));
				return ($query->num_rows() > 0);
			}
	public function login_user($data)
		{
			unset($data['submit']);

			$username = $this->db->escape_str($data['uname']);
			$password = $this->db->escape_str($data['pass']);
			$mob_code = $this->db->escape_str($data['mob_code']);
			$fn_dropdown =$this->db->escape_str($data['fn_dropdown']);
			$fn_name =$this->db->escape_str($data['fn_name']);
			$fn_sdate =$this->db->escape_str($data['fn_sdate']);
			$fn_edate =$this->db->escape_str($data['fn_edate']);
			$mob_code = $this->db->escape_str($data['mob_code']);

			if ($this->has_match($username, $password)) {

				//$query = $this->db->get_where('login', $data);
				// $query = $this->db->query("SELECT * FROM login INNER JOIN rolegroup ON login.pgroup_id=rolegroup.pgroup_id INNER JOIN company ON login.company_id=company.company_id WHERE BINARY uname='{$data['uname']}' AND BINARY pass='{$data['pass']}'");
				// $q = "SELECT company.company_id, company.company_name, user.uid, user.uname, user.pass, user.email, user.company_id, user.user_type, user.pgroup_id, user.full_name, user.user_mobile, pgroup_name, partyAll, addParty, updateParty, itemAll, addItem, updateItem, attachItemSpecsAll, updateAttachedItemSpecs, attachItemSpecs, specItemsAll, addSpecItems, updateSpecItems, userAll, addUser, updteUser, priviligeGroupAll, addPrivligeGroup, updatePrivligeGroup, companyAll, addCompany, updteCompany, currencyAll, addCurrency, updteCurrency, removeCurrency, accountLevelAll, addAccountLevel, updteAccountLevel, godownAll, addGodown, updteGodown, removeGodown, transporterAll, addTransporter, updteTransporter, removeTransporter, salesmanAll, addSalesman, updteSalesMan, removeSalesman, saleOrderAll, addSaleOrder, updateSaleOrder, removeSaleOrder, printSaleOrderRcpt, saleAll, addSale, updateSale, removeSale, printSaleRcpt, saleReturnAll, addSaleReturn, updateSaleReturn, removeSaleReturn, printSaleReturnRcpt, purchaseAll, addPurchase, updatePurchase, removePurchase, printPurchaseRcpt, purchaseOrderAll, dashboard, addPurchaseOrder, updatePurchaseOrder, removePurchaseOrder, printPurchaseOrdeRcpt, purchaseImportAll, addPurchaseImport, updatePurchaseImport, removePurchaseImport, printPurchaseImportRcpt, purchaseReturnAll, addPurchaseReturn, updatePurchaseReturn, removePurchaseReturn, printPurchaseReturnRcpt, stockNavigationAll, addStockNavigation, updateStockNavigation, removeStockNavigation, printStocknavigationRcpt, CPVAll, addCPV, updateCPV, removeCPV, printCpvReceipt, CRVAll, addCRV, updateCRV, removeCRV, printCrvRcpt, chequeIssueAll, addChequeIssue, updateChequeIssue, removeChequeIssue, printChequeIssueRcpt, chequeReceiveAll, addChequeReceive, updateChequeReceive, removeChequeReceive, printPdCrvRcpt, journalAll, addJournal, updateJournal, removeJournal, printJournalRcpt, assemblingAll, addAssembling, updateAssembling, removeAssembling, printAssemblingRcpt, finalAll, viewAccountLedger, viewItemLedger, viewTrialBalance, viewChartOfAccounts, viewProfitLoss, reportsAll, serialSearchReport, purchaseReport, purchaseImportReport, purchaseReturnReport, saleReport, saleReturnReport, chequeReport, stockNavigationReport, inventoryReport, accountReportsAll, cpvReport, crvReport, jvReport, expenseReport, payableReport, receivableReport, daybookReport, partyReport, categoryReport, subCategoryReport, godownReport, salesmanReport, itemReport, letterPad, stockOrderReport, closingStockValuesReport, cashFlowReport, saleOrderReport, agingSheetReport FROM user INNER JOIN rolegroup ON user.pgroup_id = rolegroup.pgroup_id INNER JOIN company ON user.company_id = company.company_id WHERE BINARY uname =  '{$username}' AND BINARY pass =  '{$password}'"; 
				$q="SELECT company.company_id, company.company_name, user.uid, user.uname,company.company_uname,
				user.pass, user.email, user.company_id, user.user_type,
				user.rgid, user.fullname,
				user.mobile, user.rgid, user.mob_code, rolegroup.name as rolegroup_name,rolegroup.isadmin,rolegroup.desc FROM user INNER JOIN rolegroup ON user.rgid = rolegroup.rgid INNER JOIN company ON user.company_id = company.company_id WHERE BINARY uname =  '{$username}' AND BINARY pass =  '{$password}' AND BINARY mob_code =  '{$mob_code}' ";

				$query = $this->db->query($q);
				$result = $query->result_array();

				// Date Close Save
				if($query->num_rows() > 0) 
				{
					$todayDate = date("Y-m-d", strtotime("-2 days"));

	                $uid = $result[0]['uid'];
	                $due = date('Y-m-d', strtotime('-10 year'));
	                $dateclose = array(
	                	'date_cl' => $due,
	                	'remarks' => "auto close",
	                	'uid' =>  $uid
	                ); 
	                    // die(print_r($dateclose));            
	                $res =  $this->saveDateClose($dateclose);
	                
	                if($res == true){
	                    $this->session->set_userdata('date_close', $due);
	                }else{
	                    $this->session->set_userdata('date_close', "");
	                }
				}
				
				$this->session->set_userdata($result[0]);
				$this->session->set_userdata('fn_id', $fn_dropdown);
				$this->session->set_userdata('fn_name', $fn_name);
				$this->session->set_userdata('fn_sdate', $fn_sdate);
				$this->session->set_userdata('fn_edate', $fn_edate);
				$this->session->set_userdata('user_time', microtime(true));
			}
			else{
				return false;
			}
		}

		public function saveDateclose( $dateclose ) {

		
			$query = $this->db->get_where('dateclose', array('date_cl' =>  $dateclose['date_cl']));

			$affect = 0;

			if ($query->num_rows() > 0)
	        {
				return "already Saved";
			} else {

				$result =$this->db->insert('dateclose', $dateclose);
				$affect = $this->db->affected_rows();
			}

			if ($affect === 0) {
				return false;
			} else {
				return true;
			}
		}

		public function login_user_code($uname , $password ,$mob_code)
		{
			if ($this->has_match($uname, $password)) {
				$data = array(
               		'mob_code' => $mob_code
	            	);
				$this->db->where('uname', $uname);
				$this->db->update('user', $data);
				unset($password);
				unset($uname);
				unset($mob_code);
			}
			else{
				return false;
			}
		}

		public function login_user_code_logout($uname)
		{
			$data = array(
           		'mob_code' => 'SOUT'
            	);
			$this->db->where('uname', $uname);
			$this->db->update('user', $data);
			unset($password);
			unset($uname);
			unset($mob_code);
		}
		public function getNotifications($company_id ) {

			$result = $this->db->query("SELECT log.log_id,log.vrnoa,log.vrdate,log.etype,log.party_id,log.namount,log.is_seen from transaction_log log INNER JOIN user u on u.uid = log.uid INNER JOIN company c on c.company_id = log.company_id where c.company_id = 1 and u.uid = 1 and is_seen = 1");
			if ( $result->num_rows() > 0 ) {
				return $result->result_array();
			} else {
				return false;
			}
		}


}

/* End of file users.php */
/* Location: ./application/models/users.php */