<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ledgers extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function getMaxId($etype,$company_id) {

		$this->db->where(array('etype' => $etype,'company_id'=>$company_id));
		$this->db->select_max('dcno');
		$result = $this->db->get('pledger');

		$row = $result->row_array();
		$maxId = $row['dcno'];

		return $maxId;
	}
	public function fetchAllLedgersPayments( $company_id,$etype ) {

		$result = $this->db->query("SELECT p.name AS party_name,pledger.description,pledger.debit,pledger.credit,pledger.deduction,pledger.invoice,time(date_time) as date_time,user.uname as user_name,pd.cheque_no as chq_no,pd.cheque_date as chq_date,pledger.dcno
										FROM pledger pledger
										INNER JOIN party p ON p.pid = pledger.pid
										INNER JOIN user ON user.uid = pledger.uid
										INNER JOIN company c ON c.company_id = pledger.company_id
										INNER JOIN pd_cheque pd ON pd.dcno = pledger.dcno and pd.etype=pledger.etype
										WHERE pledger.company_id= '".$company_id."' AND pledger.etype= '".$etype."'  AND pledger.date = CURDATE()
										ORDER BY pledger.pledid desc limit 10;");
		
			return $result->result_array();
		
	}
	public function fetchAllExpenses( $company_id ) {

		$result = $this->db->query("SELECT party.NAME, SUM(DEBIT) AS 'AMOUNT'
									FROM pledger
									INNER JOIN party ON pledger.pid = party.pid
									INNER JOIN level3 ON party.level3 = level3.l3
									INNER JOIN level2 ON level2.l2 = level3.l2
									INNER JOIN level1 ON level1.l1 = level2.l1
									WHERE level1.name ='EXPENSES' AND level2.name <> 'COST OF GOODS SOLD' and pledger.company_id=$company_id 
									AND pledger.DATE = CURDATE()
									GROUP BY pledger.pid
									HAVING IFNULL(SUM(DEBIT),0)<>0;");
		
			return $result->result_array();
		
	}
	public function save($ledgers, $dcno, $etype,$voucher_type_hidden) {

		//if ($voucher_type_hidden!='new'){
		$this->db->where(array(
							'dcno' => $dcno,
							'etype' => $etype,
						));
		$affect = $this->db->delete('pledger');
		//}
		$affect = 0;
		$i = 0;
		# Starting Transaction
		$this->db->trans_start(); 
		foreach ($ledgers as $ledger) {

			$ledger['dcno'] = $dcno;
		    unset($ledger['pledid']);

			$this->db->insert('pledger', $ledger);
			$affect = $this->db->affected_rows();

			if($i != 0){
				if(($etype == 'crv' && $voucher_type_hidden == 'new') || ($etype == 'cpv' && $voucher_type_hidden == 'new')){
				
					$this->load->model('accounts');
					$previous = $this->accounts->fetchRunningTotal( $ledger['date'] , $ledger['pid'] );
		            $previousBalance = $previous[0]['RTotal'];
		            $amount = ($etype === 'cpv') ?  $ledger['debit'] : $ledger['credit'];
		            $currentBalance = ($etype === 'cpv') ? $previousBalance - $amount: $previousBalance + $amount;
		            $chequeType = ($etype === 'cpv') ?  'paid' : 'received';
		            $message = 'Cash '. $chequeType .' Rs:'.$amount.' against Invoice # '.$ledger['dcno'].' and your Current Balance is Rs:'.$currentBalance;
		            $query = 'select mobile from party where pid ='.$ledger['pid'];
		            $mobile = $this->db->query( $query );
		            $row = $mobile->row_array();
					$mobile = $row['mobile'];
					
		            $this->sendsms->sendS( $mobile, $message);
				}
			}
			$i++;
			
		}
		$this->db->trans_complete(); # Completing transaction

		if ($this->db->trans_status() === FALSE) {
            # Something went wrong.
            $this->db->trans_rollback();
            return FALSE;
        } 
        else {
            # Everything is Perfect. 
            # Committing data to the database.
            $this->db->trans_commit();
            if($etype == "penalty" || $etype == "loan" || $etype == "advance" || $etype == "incentive")
            {
                $this->partyEffect($ledgers[0]['pid'], $etype);
            }
            return TRUE;
        }

		/*if ( $affect === 0 ) {
			return false;
		} else {
            if($etype == "penalty" || $etype == "loan" || $etype == "advance" || $etype == "incentive")
            {
                $this->partyEffect($ledgers[0]['pid'], $etype);
            }
			return true;
		}*/
	}

	public function deleteVoucher($dcno, $etype,$company_id) {

		$this->db->where(array(
								'dcno' => $dcno,
								'etype' => $etype,
								'company_id'=> $company_id
							));
		$result = $this->db->get('pledger');

		if ($result->num_rows() > 0) {

			$this->db->where(array(
								'dcno' => $dcno,
								'etype' => $etype
							));
			$result = $this->db->delete('pledger');

		} else {
			return false;
		}
	}

	public function fetch($dcno, $etype,$company_id) {

		$result = $this->db->query("SELECT ldgr.cid,ldgr.date_time,cc.name as currency,ldgr.frate,abs(ldgr.famount)famount,ldgr.pledid,ldgr.remarks,
ldgr.uid, ldgr.pid, ldgr.description, ldgr.deduction, ldgr.date, ldgr.debit, ldgr.credit, ldgr.invoice, ldgr.dcno, ldgr.etype, cid, chid, rose, secid, pid_key, p.name AS 'party_name',
u.uname as user_name,ldgr.mode_of_payment,ldgr.checque_date,ldgr.checque_no,ldgr.`status`,ldgr.is_post,cash.name cashaccount FROM pledger AS ldgr
		LEFT OUTER JOIN party AS p ON ldgr.pid = p.pid
		left JOIN currencey AS cc ON cc.id = ldgr.cid
		inner join user as u on u.uid=ldgr.uid
		left join party as cash on cash.pid = ldgr.pid_key WHERE ldgr.etype = '". $etype ."' AND ldgr.dcno = $dcno AND ldgr.company_id = $company_id" );

		if ( $result->num_rows() === 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}
	public function savePenalty($ledgers, $dcno, $etype) {

		$this->db->where(array(
								'dcno' => $dcno,
								'etype' => $etype,
							));
		$affect = $this->db->delete('pledger');

		$affect = 0;
		foreach ($ledgers as $ledger) {
			$this->db->insert('pledger', $ledger);
			$affect = $this->db->affected_rows();
		}

		if ( $affect === 0 ) {
			return false;
		} else {
			return true;
		}
	}
	public function fetchpenalty($dcno, $etype,$company_id) {

				$result = $this->db->query("SELECT pledid, ldgr.pid, description, deduction, date(ldgr.DATE) DATE, debit, credit, invoice, ldgr.dcno, ldgr.etype, cid, chid, rose, secid, pid_key, p.name AS 'party_name'
	FROM pledger AS ldgr
	LEFT OUTER
	JOIN party AS p ON ldgr.pid = p.pid
	INNER JOIN user AS u ON u.uid=ldgr.uid
	WHERE ldgr.etype = '". $etype ."' AND ldgr.dcno = $dcno AND ldgr.company_id = $company_id;

	");

				if ( $result->num_rows() === 0 ) {
					return false;
				} else {
					return $result->result_array();
				}
			}

	public function fetchVoucherRange($from, $to, $etype)
	{
		$query = $this->db->query("SELECT pledid, ldgr.pid, description, date, (debit+credit) AS amount, invoice, ldgr.dcno, ldgr.etype, cid, chid, rose, secid, pid_key, p.name AS 'party_name', round(ldgr.debit, 2) debit, round(ldgr.credit, 2) credit FROM pledger AS ldgr LEFT OUTER JOIN party AS p ON ldgr.pid = p.pid WHERE ldgr.date BETWEEN '". $from ."' AND '". $to ."' AND ldgr.etype = '". $etype ."'");
		return $query->result_array();
	}

	public function getAccLedgerReport($from, $to, $pid) {

		$result = $this->db->query("CALL acc_ledger('". $from ."', '". $to ."', $pid)");
		$result = $result->result_array();
		mysqli_next_result($this->db->conn_id);
		if (count($result) > 0) {
			return $result;//->result_array();
		} else {
			return false;
		}
	}

	public function getCashFlowStatement($from, $to, $pid) {

		$result = $this->db->query("CALL cash_flow_statment('". $from ."', '". $to ."', $pid)");
		$result = $result->result_array();
		mysqli_next_result($this->db->conn_id);

		if (count($result) > 0) {
			return $result;//->result_array();
		} else {
			return false;
		}
	}

	public function getAccLedgerReports($from, $to, $pid) {

		$result = $this->db->query("CALL acc_ledger_foreign('". $from ."', '". $to ."', $pid)");
        mysqli_next_result($this->db->conn_id);
      		if (count($result) > 0) {
			return $result->result_array();
		} else {
			return false;
		}
	}

	public function getAccLedgerReport22($from, $to, $pid, $company_id) {

		$result = $this->db->query("SELECT ldgr.pledid,ldgr.etype,ldgr.uid, ldgr.pid, ldgr.description, ldgr.deduction, ldgr.date, ldgr.debit, ldgr.credit, ldgr.invoice, ldgr.dcno, ldgr.etype, ldgr.cid, ldgr.chid, ldgr.r  ose, ldgr.secid, ldgr.pid_key, p.name AS 'party_name', p.mobile + '. ' + p.phone AS 'contact', p.address AS 'address',p.account_id as 'acc_id',u.uname as user_name FROM pledger AS ldgr LEFT OUTER JOIN party AS p ON ldgr.pid = p.pid inner join user as u on u.uid=ldgr.uid WHERE ldgr.pid = $pid and ldgr.date between '". $from ."' and '". $to ."' and ldgr.company_id =$company_id ");

		if ( $result->num_rows() === 0 ) {
			return false;
		} else {
			return $result->result_array();
		}
	}

    public function partyEffect($partyId, $etype)
    {
        if($etype == "penalty")
        {
            $this->db->query("update payroll_setting set penalty_party_id_effect = '" . $partyId . "' ");
        }
        else if($etype == "loan")
        {
            $this->db->query("update payroll_setting set loan_party_id_effect = '" . $partyId . "' ");
        }
        else if($etype == "advance")
        {
            $this->db->query("update payroll_setting set advance_party_id_effect = '" . $partyId . "' ");
        }
        else if($etype == "incentive")
        {
            $this->db->query("update payroll_setting set incentive_party_id_effect = '" . $partyId . "' ");
        }
    }

    // public function fetchAllLedgersPayments( $company_id,$etype ) {

    //     $query = $this->db->query("SELECT pledger.etype,COALESCE(p.name,'') AS party_name,pledger.description,ifnull(pledger.debit,0) as debit,ifnull(pledger.credit,0) as credit,pledger.deduction,COALESCE(pledger.invoice,'') as invoice,time(date_time) as date_time,user.uname as user_name,pledger.dcno
    //                                     FROM pledger pledger
    //                                     INNER JOIN party p ON p.pid = pledger.pid
    //                                     INNER JOIN user ON user.uid = pledger.uid
    //                                     INNER JOIN company c ON c.company_id = pledger.company_id
    //                                     WHERE pledger.company_id= '".$company_id."' AND pledger.etype= '".$etype."'  AND pledger.date = SUBDATE(CURDATE(), INTERVAL 1 DAY) 
    //                                     ORDER BY pledger.pledid desc limit 10;");
        
    //     return $query->result_array();
        
    // }

    public function fetchAllPenaltys( $company_id,$etype ) {

        $query = $this->db->query("SELECT pledger.etype,COALESCE(p.name,'') AS party_name,pledger.description,ifnull(pledger.debit,0) as namount,ifnull(pledger.credit,0) as credit,pledger.deduction,COALESCE(pledger.invoice,'') as invoice,time(date_time) as date_time,user.uname as user_name,pledger.dcno as vrnoa
                                        FROM pledger pledger
                                        INNER JOIN party p ON p.pid = pledger.pid
                                        INNER JOIN user ON user.uid = pledger.uid
                                        INNER JOIN company c ON c.company_id = pledger.company_id
                                        WHERE pledger.company_id= '".$company_id."' AND pledger.etype= '".$etype."'  AND pledger.date = SUBDATE(CURDATE(), INTERVAL 1 DAY) and pledger.debit > 0
                                        ORDER BY pledger.pledid desc limit 10;");
        
        return $query->result_array();
        
    }

    public function fetchTotalPayments($etype)
    {
        $query = $this->db->query("select ifnull(sum(debit),0) as totalaccount from pledger where etype = '".$etype."' AND date = SUBDATE(CURDATE(), INTERVAL 1 DAY)");
        return $query->result_array();
    }

}

/* End of file ledgers.php */
/* Location: ./application/models/ledgers.php */