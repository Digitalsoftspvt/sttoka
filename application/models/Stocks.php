<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit','100M');
class Stocks extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function getMaxVrno($etype) {

		$result = $this->db->query("SELECT MAX(vrno) vrno FROM stockmain WHERE etype = '". $etype ."' AND DATE(vrdate) = DATE(NOW())");
		$row = $result->row_array();
		$maxId = $row['vrno'];

		return $maxId;
	}

	public function getMaxVrnoa($etype) {

		$result = $this->db->query("SELECT MAX(vrnoa) vrnoa FROM stockmain WHERE etype = '". $etype ."'");
		$row = $result->row_array();
		$maxId = $row['vrnoa'];

		return $maxId;
	}

	public function save( $stockmain, $stockdetail, $vrnoa, $etype ) {

		$this->db->where(array('salebillno' => $vrnoa, 'etype' => $etype ));
		$result = $this->db->get('stockmain');

		if ($result->num_rows() > 0) {

			$result = $result->row_array();
			$stid = $result['stid'];

			////////////////////////////////
			// delete from stock main     //
			////////////////////////////////
			$this->db->where(array('salebillno' => $vrnoa, 'etype' => $etype ));
			$result = $this->db->delete('stockmain');
			//////////////////////////////
			// delete from stock detail //
			//////////////////////////////
			$this->db->where(array('stid' => $stid));
			$result = $this->db->delete('stockdetail');
		}

		$this->db->insert('stockmain', $stockmain);
		$stid = $this->db->insert_id();

		$affect = 0;
		foreach ($stockdetail as $detail) {

			$detail['stid'] = $stid;
			$this->db->insert('stockdetail', $detail);
			$affect = $this->db->affected_rows();
		}

		if ( $affect == 0 ) {
			return false;
		} else {
			return true;
		}
	}

	public function delete( $vrnoa, $etype ) {

		$this->db->where(array('salebillno' => $vrnoa, 'etype' => $etype ));
		$result = $this->db->get('stockmain');

		if ($result->num_rows() > 0) {

			$result = $result->row_array();
			$stid = $result['stid'];

			  ////////////////////////////
			 // delete from stock main //
			////////////////////////////
			$this->db->where(array('salebillno' => $vrnoa, 'etype' => $etype ));
			$result = $this->db->delete('stockmain');
			  //////////////////////////////
			 // delete from stock detail //
			//////////////////////////////
			$this->db->where(array('stid' => $stid));
			$result = $this->db->delete('stockdetail');
		}

		return true;
	}


	public function fetchDailyVoucherReport($startDate, $endDate) {

		$query = $this->db->query("SELECT m.VRNOA, p.NAME, m.REMARKS, ROUND(d.QTY, 2) QTY, ROUND(d.NETAMOUNT, 2) NETAMOUNT, m.ETYPE , i.item_des FROM stockmain m INNER JOIN stockdetail d ON m.stid = d.stid INNER JOIN party p ON p.pid = m.party_id inner join item i on i.item_id = d.item_id WHERE m.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' ORDER BY m.ETYPE");
		return $query->result_array();
	}

	public function fetchStockReport($startDate, $endDate, $what , $company_id) {

		if ($what == 'location') {
			$query = $this->db->query("SELECT i.item_des as DESCRIPTION, dep.NAME,i.cost_price as PRATE,i.grweight as GWEIGHT, round(SUM(qty), 2) QTY, round(SUM(weight), 2) WEIGHT FROM stockmain m INNER JOIN stockdetail d ON m.stid = d.stid INNER JOIN item i ON i.item_id = d.item_id INNER JOIN department dep ON dep.did = d.godown_id WHERE m.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND m.company_id=". $company_id ." GROUP BY d.item_id, dep.did ORDER BY dep.name");
			return $query->result_array();
		} else if ($what == 'item') {
			$query = $this->db->query("SELECT i.item_des as DESCRIPTION,i.cost_price as PRATE,i.grweight as GWEIGHT, dep.NAME, round(SUM(qty), 2) QTY , round(SUM(weight), 2) WEIGHT FROM stockmain m INNER JOIN stockdetail d ON m.stid = d.stid INNER JOIN item i ON i.item_id = d.item_id INNER JOIN department dep ON dep.did = d.godown_id WHERE m.VRDATE BETWEEN '". $startDate ."' AND '". $endDate ."' AND m.company_id=". $company_id ." GROUP BY d.item_id, dep.did ORDER BY i.item_id");
			return $query->result_array();
		}
	}

	public function fetchClosingStockReport($startDate, $endDate, $what , $company_id , $crit, $godownIds="") {

		$sql = "SELECT * FROM (SELECT m.party_id,m.vrnoa, m.etype,m.vrdate, d.godown_id,p.name AS partyname, g.name AS godown,d.qty,d.weight,detail.*
				FROM stockdetail d
				INNER JOIN stockmain m ON m.stid=d.stid
				LEFT JOIN party p ON p.pid = m.party_id
				LEFT JOIN department g ON g.did = d.godown_id
				inner join item items ON d.item_id=items.item_id
				INNER JOIN(
				SELECT i.item_id,i.item_des as voucher,i.item_des,i.item_code,i.uom, IFNULL(op.qty,0) AS opqty, IFNULL(op.weight,0) AS opweight, IFNULL(cl1.inqty,0) AS inqty, IFNULL(cl1.inweight,0) AS inweight, IFNULL (cl.outqty,0) outqty, IFNULL (cl.outweight,0) outweight, IFNULL(op.qty,0)+ IFNULL(cl1.inqty,0)- IFNULL(cl.outqty,0) balanceqty, IFNULL(op.weight,0)+ IFNULL(cl1.inweight,0)- IFNULL(cl.outweight,0) balanceweight, IFNULL(lp.Rate,0) AS cost_rate,(IFNULL(op.qty,0)+ IFNULL(cl1.inqty,0)- IFNULL(cl.outqty,0))* IFNULL(lp.Rate,0) AS value
				FROM item i
				LEFT JOIN(
				SELECT IFNULL(SUM(qty),0) AS qty, IFNULL(SUM(weight),0) AS weight,item_id
				FROM stockdetail d
				INNER JOIN stockmain m ON m.stid=d.stid
				WHERE m.vrdate < '". $startDate ."' $godownIds
				GROUP BY item_id
				) AS op ON op.item_id=i.item_id
				LEFT JOIN(
				SELECT d.item_id, ABS(IFNULL(SUM(d.qty),0)) AS outqty, ABS(IFNULL(SUM(d.weight),0)) AS outweight
				FROM stockdetail d
				INNER JOIN stockmain m ON m.stid=d.stid
				WHERE m.vrdate BETWEEN '". $startDate ."' AND '". $endDate ."' AND qty<0 $godownIds
				GROUP BY d.item_id 
				) AS cl ON cl.item_id=i.item_id
				LEFT JOIN(
				SELECT d.item_id, IFNULL(SUM(d.qty),0) AS inqty, IFNULL(SUM(d.weight),0) AS inweight
				FROM stockdetail d
				INNER JOIN stockmain m ON m.stid=d.stid
				WHERE m.vrdate BETWEEN '". $startDate ."' AND '". $endDate ."' AND qty>0 $godownIds
				GROUP BY d.item_id 
				) AS cl1 ON cl1.item_id=i.item_id
				LEFT JOIN (
				SELECT t1.item_id, t1.rate,t1.stid
				FROM stockdetail t1
				JOIN (
				SELECT item_id, MAX(stid) stid
				FROM stockdetail
				WHERE stid IN (
				SELECT stid
				FROM stockmain
				WHERE vrdate <='". $endDate ."')
				GROUP BY item_id) t2 ON t1.item_id = t2.item_id AND t1.stid = t2.stid
				GROUP BY t1.item_id,t1.rate,t1.stid
				) AS lp ON lp.item_id=i.item_id
				ORDER BY i.item_id
				) AS detail ON detail.item_id = d.item_id
				WHERE m.vrdate between  '". $startDate ."' AND '". $endDate ."' AND m.company_id = ". $company_id ."
				$crit 
				
				Union All
				
				SELECT '','','','','','','','','',i.item_id,i.item_des as voucher,i.item_des,i.item_code,i.uom, IFNULL(op.qty,0) AS opqty, IFNULL(op.weight,0) AS opweight, IFNULL(cl1.inqty,0) AS inqty, IFNULL(cl1.inweight,0) AS inweight, IFNULL (cl.outqty,0) outqty, IFNULL (cl.outweight,0) outweight, IFNULL(op.qty,0)+ IFNULL(cl1.inqty,0)- IFNULL(cl.outqty,0) balanceqty, IFNULL(op.weight,0)+ IFNULL(cl1.inweight,0)- IFNULL(cl.outweight,0) balanceweight, IFNULL(lp.Rate,0) AS cost_rate,(IFNULL(op.qty,0)+ IFNULL(cl1.inqty,0)- IFNULL(cl.outqty,0))* IFNULL(lp.Rate,0) AS value
				FROM item i
				LEFT JOIN(
				SELECT IFNULL(SUM(qty),0) AS qty, IFNULL(SUM(weight),0) AS weight,item_id
				FROM stockdetail d
				INNER JOIN stockmain m ON m.stid=d.stid
				WHERE m.vrdate < '". $startDate ."' $godownIds
				GROUP BY item_id
				) AS op ON op.item_id=i.item_id
				LEFT JOIN(
				SELECT d.item_id, ABS(IFNULL(SUM(d.qty),0)) AS outqty, ABS(IFNULL(SUM(d.weight),0)) AS outweight
				FROM stockdetail d
				INNER JOIN stockmain m ON m.stid=d.stid
				WHERE m.vrdate BETWEEN '". $startDate ."' AND '". $endDate ."' AND qty<0 $godownIds
				GROUP BY d.item_id 
				) AS cl ON cl.item_id=i.item_id
				LEFT JOIN(
				SELECT d.item_id, IFNULL(SUM(d.qty),0) AS inqty, IFNULL(SUM(d.weight),0) AS inweight
				FROM stockdetail d
				INNER JOIN stockmain m ON m.stid=d.stid
				WHERE m.vrdate BETWEEN '". $startDate ."' AND '". $endDate ."' AND qty>0 $godownIds
				GROUP BY d.item_id 
				) AS cl1 ON cl1.item_id=i.item_id
				LEFT JOIN (
				SELECT t1.item_id, t1.rate,t1.stid
				FROM stockdetail t1
				JOIN (
				SELECT item_id, MAX(stid) stid
				FROM stockdetail
				WHERE stid IN (
				SELECT stid
				FROM stockmain
				WHERE vrdate <='". $endDate ."')
				GROUP BY item_id) t2 ON t1.item_id = t2.item_id AND t1.stid = t2.stid
				GROUP BY t1.item_id,t1.rate,t1.stid
				) AS lp ON lp.item_id=i.item_id
				where i.item_id not in (select distinct item_id from stockdetail d inner join stockmain m on d.stid=m.stid where vrdate between '". $startDate ."' AND '". $endDate ."' $godownIds)) AS X WHERE (X.balanceqty<>0 or X.balanceweight<>0 or inqty<>0 or inweight<>0 or outqty<>0 or outweight<>0) order by X.item_code";
				//die(print($sql));
		$query = $this->db->query( $sql );
		return $query->result_array();
	}
    public function fetchPurchaseReportData($startDate, $endDate, $what, $type, $company_id, $etype,$field,$crit,$orderBy,$groupBy,$name)
    {



        if ($type == 'detailed') {
            $query = $this->db->query("SELECT $field as voucher,item.item_des,dept.name as dname, party.name as name, dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username,stockmain.vrdate, stockmain.vrnoa, stockmain.remarks, stockdetail.qty, stockdetail.weight, stockdetail.rate, stockdetail.netamount, item.item_des as 'item_des',item.uom FROM stockmain stockmain inner JOIN stockdetail ON stockmain.STID = stockdetail.stid inner JOIN party ON stockmain.party_id = party.pid inner JOIN item ON stockdetail.item_id = item.item_id left JOIN user ON user.uid = stockmain.uid inner JOIN department dept ON stockdetail.godown_id = dept.did WHERE stockmain.vrdate BETWEEN '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=1 AND stockmain.etype='$etype' $crit order by $orderBy");
            return $query->result_array();
        } else {
            $query = $this->db->query("SELECT $field as voucher,$name,date(stockmain.vrdate) as DATE,dayname(vrdate) as weekdate, month(vrdate) as monthdate,year(vrdate) as yeardate,user.uname as username, date(stockmain.VRDATE) VRDATE, stockmain.vrnoa, round(SUM(stockdetail.qty)) qty, round(SUM(stockdetail.weight)) weight, round(SUM(stockdetail.rate)) rate, round(SUM(stockdetail.amount)) amount, round(sum(stockdetail.netamount)) netamount, stockmain.remarks FROM stockmain stockmain INNER JOIN stockdetail stockdetail ON stockmain.stid = stockdetail.stid INNER JOIN party party ON stockmain.party_id = party.pid      INNER JOIN item item ON stockdetail.item_id = item.item_id LEFT JOIN user user ON user.uid = stockmain.uid  INNER JOIN department dept  ON  stockdetail.godown_id = dept.did WHERE  stockmain.vrdate between '". $startDate ."' AND '". $endDate ."' AND stockmain.company_id=$company_id AND stockmain.etype='$etype' $crit group by $groupBy order by $orderBy");
            return $query->result_array();
        }

    }
}

/* End of file stocks.php */
/* Location: ./application/models/stocks.php */